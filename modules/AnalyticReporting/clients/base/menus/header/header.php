<?php

$module_name = 'AnalyticReporting';
$viewdefs[$module_name]['base']['menu']['header'] = array(
    array(
        'route'=>'#bwc/index.php?module=AnalyticReporting&action=index',
        'label' => 'LBL_ALL_REPORTS',
        'acl_module'=> $module_name,
        'acl_action'=> 'list',
        'icon' => 'fa-bars',
    ),
);
