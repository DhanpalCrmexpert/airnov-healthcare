<?php

require_once dirname(__FILE__) . '/PreInstallBasic.php';

class PreInstall extends PreInstallBasic
{
    protected function configKeys()
    {
        return array(
            'plan' => 'BASIC',
            'new_install' => $this->params['new_install'],
            'version' => 'sugarcrmlite',
        );
    }

    // Add 'm' prefixes to all field defintions
    public function patchReports() {
        // Check if pre-installed version has already patch included
        $sql = "SELECT * FROM advancedreports_config WHERE id = 'prefixes_updated';";
        $result = $this->adb->query($sql);

        if ($result->num_rows) return;

        $sql = "SELECT * FROM advancedreports;";
        $result = $this->adb->query($sql);
        $binaryFields = array("calculatedColumns",
                                    "columnstate",
                                    "calcFields",
                                    "combinedfields",
                                    "chart",
                                    "labels",
                                    "options",
                                    "totalAggregates",
                                    "aggregates",
                                    "grouping",
                                    "filters",
                                    "fields",
                                    "related_data");

        while($row = $this->adb->fetchRow($result)) {
            $row = self::addPrefixToColumns($row);
            $id = $row["id"];
            $values = array();
            foreach ($row as $key => $value) {
                if(in_array($key, $binaryFields) && !empty($value)){
                    $value = self::toBinary($value);
                    $values[] = "{$key} = {$value}";
                }
            }
            $values = implode(", ", $values);

            $this->adb->query("UPDATE advancedreports SET {$values} WHERE id = {$id}");
        }

        $sql = "INSERT INTO advancedreports_config (value, id) VALUES (1, 'prefixes_updated')";
        $this->adb->query($sql);
    }

    static public function toBinary($data) {
        return '0x'.strtoupper(bin2hex($data));
    }

    protected static function addPrefixToColumns($object) {
        foreach($object as $index => &$item) {
            if($index === 'calculatedColumns'){
                continue;
            }

            if (!is_string($item)) continue;

            // Special case for combined reports
            $item = str_replace('"00_', '"m00_', $item);

            foreach(range(0,9) as $i) {
                // Replace "9_ to "m9_
                $item = str_replace("\"{$i}_", "\"m{$i}_", $item);

                // Replace aggregates
                foreach(array("cnt", "count", "countDistinct", "avg", "min", "max", "sum") as $aggregate) {
                    $item = str_replace("\"{$aggregate}_{$i}_", "\"{$aggregate}_m{$i}_", $item);
                }
            }
        }

        return $object;
    }

    public function run()
    {
        $tableName = 'advancedreports_config';

        foreach ($this->configKeys() as $key => $value) {
            $query = null;
            $found = $this->adb->getOne("SELECT COUNT(*) AS count FROM {$tableName} WHERE id = '{$key}'");

            if ($found > 0 && $key === 'new_install') continue;

            if ($found < 1) {
                $query = "INSERT INTO {$tableName} (id, value) VALUES ('{$key}', '{$value}')";
            } else {
                $query = "UPDATE {$tableName} SET value = '{$value}' WHERE id = '{$key}'";
            }

            $this->adb->query($query);
        }

        $this->patchReports();

        echo "Add env. configuration.\n<br />";
    }
}