<?php

array_push($job_strings, 'hours_missing_on_projects');

function Hours_Missing_On_Projects(){
	global $db;

	$sql = "select id, reminder_time_limit, users_to_get_reminder from users where deleted = 0 and reminder_to_enter_time = '1'";
	$res = $db->query($sql);
	while($row = $db->fetchByAssoc($res)){
		$reminder_time_limit = $row['reminder_time_limit'];
		$weekStart = date('Y-m-d', strtotime('this week'));
		$weekEnd = date('Y-m-d', strtotime('this week +6 day'));
		$timesheetSql = "select id, hours, name from sp_timesheet where deleted = 0 and user_id_c = '{$row['id']}' and track_date_from = '{$weekStart}' and track_date_to = '{$weekEnd}'";
		$sendReminder = false;
		$timesheetRes = $db->query($timesheetSql);
		if($timesheetRes->num_rows == 0){
			$sendReminder = true;
		}
		
		while($timesheetRow = $db->fetchByAssoc($timesheetRes)){
			if((float)$timesheetRow['hours'] <= (float)$row['reminder_time_limit']){
				$sendReminder = true;
			}
		}
		if($sendReminder){
			$users = explode('^,^', $row['users_to_get_reminder']);
			$managerEmails = [];
			foreach($users as $user){
				$user_obj = BeanFactory::getBean("Users", ltrim(rtrim($user, '^'), '^'));
				$managerEmails[] = $user_obj->email1;		
			}
			notifyUser($row['id'], $managerEmails, "TIMESHEET: {$weekStart} - {$weekEnd}");
		}
	}
	return true;
}


function notifyUser($user_id, $managerEmails, $timesheetName){
	global $sugar_config;
	require_once('modules/EmailTemplates/EmailTemplate.php');
	
	$user_obj = BeanFactory::getBean("Users", $user_id);
	$email_address = $user_obj->email1;
	if(empty($email_address)){
		return;
	}
	
	$employee_name = $user_obj->first_name." ".$user_obj->last_name;
			
	$objTemplate = new EmailTemplate();
	$objTemplate->retrieve_by_string_fields(array('name' => 'Timesheet - Time logging missing reminder'));
	
	if (!empty($objTemplate->id)) {		
		$subject = $objTemplate->subject;
		$subject = str_replace('::employee_name::', $employee_name, $subject);
		$subject = str_replace('::timesheet_name::', $timesheetName, $subject);
		
		$body_html = $objTemplate->body_html;
		$body = $objTemplate->body;
	
		$body_html = str_replace('::employee_name::', $employee_name, $body_html);
		$body_html = str_replace('::timesheet_name::', $timesheetName, $body_html);
		
		$body = str_replace('::employee_name::', $employee_name, $body);
		$body = str_replace('::timesheet_name::', $timesheetName, $body);
	}
	else{
		$subject = "{$employee_name} time logs for $timesheetName are Blank";
		
		$body_html = "Hello {$employee_name},

					Your timesheet is blank for {$timesheetName}. This is a reminder to enter your time into the timesheet system by the end of day. The timesheet will lock and you will not be able to enter time for the past week once the timesheet is submitted by the end of day. 

					Thank you!.<br/>
					Service Push.<br/> <br/>";
		$body = $body_html;
	}
	
	require_once("include/SugarPHPMailer.php");
	$note_msg=new SugarPHPMailer();
	$admin = new Administration();
	$admin->retrieveSettings();
	$note_msg->Subject = $subject;
	$note_msg->prepForOutbound();
	$note_msg->setMailerForSystem();
	
	$note_msg->IsHTML(true);
	$note_msg->From     = $admin->settings['notify_fromaddress'];
	$note_msg->FromName = $admin->settings['notify_fromname'];
	$note_msg->AddAddress($email_address);
	if(!empty($managerEmails)) {
		foreach($managerEmails as $email) {
			$note_msg->AddCC($email);
		}
	}
	$note_msg->Body = from_html($body_html);
	$note_msg->AltBody = from_html($body);
	
	if (!$note_msg->Send()){
		 $GLOBALS['log']->info("Could not send email: " . $note_msg->ErrorInfo);
	}
}

?>