<?php
// created: 2020-11-18 06:37:33
$mod_strings = array (
  'LBL_ACCOUNT_NAME' => 'Customer Name:',
  'LBL_ACCOUNT_ID' => 'Customer ID',
  'LBL_ACCOUNT' => 'Customer',
  'LBL_LIST_ACCOUNT_NAME' => 'Customer Name',
  'LNK_NEW_CASE' => 'Create QNS',
  'LNK_CASE_LIST' => 'View QNS',
  'LNK_IMPORT_CASES' => 'Import QNS',
  'LBL_LIST_FORM_TITLE' => 'QNS List',
  'LBL_SEARCH_FORM_TITLE' => 'QNS Search',
  'LBL_LIST_MY_CASES' => 'My Open QNS',
  'LBL_MODULE_NAME' => 'QNS',
);