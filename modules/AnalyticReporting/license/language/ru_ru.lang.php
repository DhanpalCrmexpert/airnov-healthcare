﻿<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
$license_strings = array (
'LBL_STEPS_TO_LOCATE_KEY_TITLE' => 'Чтобы найти ключ',
'LBL_STEPS_TO_LOCATE_KEY_1' => '1. Введите логин и пароль, чтобы',
'LBL_STEPS_TO_LOCATE_KEY_2' => '2. к Account->Purchases',
'LBL_STEPS_TO_LOCATE_KEY_3' => '3. Найдите ключ на покупку этого дополнения',
'LBL_STEPS_TO_LOCATE_KEY_4' => '4. Вставить в окне License Key ниже',
'LBL_STEPS_TO_LOCATE_KEY_5' => '5. Нажмите на "Validate"',
'LBL_LICENSE_KEY' => 'Лицензионный ключ',
'LBL_CURRENT_USERS' => 'Текущий Граф Пользователь',
'LBL_LICENSED_USERS' => 'Лицензия Граф Пользователь',
'LBL_VALIDATE_LABEL' => 'утверждать',
'LBL_VALIDATED_LABEL' => 'Утвержденные',
);
