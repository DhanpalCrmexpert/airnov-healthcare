<?php
 // created: 2020-09-01 11:04:27
$dictionary['Account']['fields']['npr_c'] = array(
    'name' => 'npr_c',
    'vname' => 'LBL_NPR',
    'type' => 'enum',
    'options' => 'yes_no_list',
    'required' => false,
    'audited' => true,
    'reportable' => true,
    'default' => '',
);

$dictionary['Account']['fields']['nda_c'] = array(
    'name' => 'nda_c',
    'vname' => 'LBL_NDA',
    'type' => 'enum',
    'options' => 'yes_no_list',
    'required' => false,
    'audited' => true,
    'reportable' => true,
    'default' => '',
);

$dictionary['Account']['fields']['potential_launch_c'] = array(
    'name' => 'potential_launch_c',
    'vname' => 'LBL_POTENTIAL_LAUNCH',
    'type' => 'date',
    'required' => false,
    'audited' => true,
    'reportable' => true,
);

$dictionary['Account']['fields']['success_rate_c'] = array(
    'name' => 'success_rate_c',
    'vname' => 'LBL_SUCCESS_RATE',
    'type' => 'float',
    'required' => false,
    'audited' => true,
    'reportable' => true,
);

$dictionary['Account']['fields']['projects_won_c'] = array(
    'name' => 'projects_won_c',
    'vname' => 'LBL_PROJECTS_WON',
    'type' => 'float',
    'required' => false,
    'audited' => true,
    'reportable' => true,
);

$dictionary['Account']['fields']['sales_c'] = array(
    'name' => 'sales_c',
    'vname' => 'LBL_SALES',
    'type' => 'float',
    'required' => false,
    'audited' => true,
    'reportable' => true,
);

$dictionary['Account']['fields']['production_site_c'] = array(
    'name' => 'production_site_c',
    'vname' => 'LBL_PRODUCTION_SITE',
    'type' => 'varchar',
    'required' => false,
    'audited' => true,
    'reportable' => true,
);

$dictionary['Account']['fields']['potential_impact_usd_budget_nineteen_c'] = array(
    'name' => 'potential_impact_usd_budget_nineteen_c',
    'vname' => 'LBL_POTENTIAL_IMPACT_USD_BUDGET_NINETEEN',
    'type' => 'currency',
    'precision' => 2,
    'required' => false,
    'audited' => true,
    'reportable' => true,
);

$dictionary['Account']['fields']['potential_impact_usd_budget_twenty_c'] = array(
    'name' => 'potential_impact_usd_budget_twenty_c',
    'vname' => 'LBL_POTENTIAL_IMPACT_USD_BUDGET_TWENTY',
    'type' => 'currency',
    'precision' => 2,
    'required' => false,
    'audited' => true,
    'reportable' => true,
);

$dictionary['Account']['fields']['potential_impact_usd_budget_twentyone_c'] = array(
    'name' => 'potential_impact_usd_budget_twentyone_c',
    'vname' => 'LBL_POTENTIAL_IMPACT_USD_BUDGET_TWENTYONE',
    'type' => 'currency',
    'required' => false,
    'audited' => true,
    'reportable' => true,
    'precision' => 2,
);

?>