<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * This file is part of package Price Books.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Price Books is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
require_once('custom/VIPriceBooks/VIPriceBooksFunction.php');
class VIPriceBooksConfigEnable{
    public function __construct(){
    	$this->priceBooksConfig();
    } 
    //Add Price Book Config Value 
    public function priceBooksConfig(){
    	$enablePriceBooks = $_REQUEST['val'];

        $tableName = "vi_enable_pricebooks";
        $fieldNames = array('enable');
        $where = array();

        $selData = getPriceBooksRecord($tableName,$fieldNames,$where);
        $resultData = $GLOBALS['db']->fetchOne($selData);

        $fielddata = array('enable'=>$enablePriceBooks);
        if(empty($resultData)) {
            $resultEnablePriceBooks = insertPriceBooksRecord($tableName,$fielddata);         
        }else{
            $resultUpdatePriceBooks = updatePriceBooksRecord($tableName,$fielddata,$where);                
        } 
    }//end of function
}//end of class
new VIPriceBooksConfigEnable();
?>