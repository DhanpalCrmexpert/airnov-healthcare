<div class="moduleTitle">
<h2>TimeSheet Configuration</h2>
<div class="clear"></div>
<div>
<form name="frmTimesheet" id="frmTimesheet" action="index.php" method="POST">
<input type="hidden" name="module" id="module" value="sp_Timesheet">
<input type="hidden" name="action" id="action" value="TimesheetSettings">
<input type="hidden" name="saveForm" id="saveForm" value="1">


<!-- Discount Configuration -->
<table width="100%" border="0" cellspacing="1" cellpadding="0" class="edit view">
<tbody>
        <tr>
                <th align="left" scope="row" colspan="4">
                        <h4>Module Settings</h4>
                </th>
        </tr>
        <tr>
                <td scope="row">Module</td>
                <td>{$moduleValueList}</td>
                <td scope="row">Related Module</td>
                <td>{$relatedModuleValueList}</td>
        </tr>
</tbody>

<!-- Action buttons  -->
<table width="100%" cellpadding="0" cellspacing="1" border="0" class="actionsContainer">
        <tbody>
                <tr>
                        <td>
                        <input type="submit" class="button primary" onclick="return validateForm();" name="Save" id="Save" value="Save" />
                        <a href="index.php?module=sp_Timesheet&action=EditView"><input type="button" class="button" name="Back" id="Back" value="Back" /></a>
                        </td>
                </tr>
        </tbody>
</table>
</form>

{literal}
<script type="text/javascript">
$(document).ready(function() {
	if(document.getElementById('selected_module')){
		var selected_module = document.getElementById('selected_module');
		selected_module.onchange = function(){populateRelatedModules(selected_module);};
	}
});


function populateRelatedModules(module){
	var selectedModuleValue = module.value;
	var relatedModuleFieldName = 'related_module';
	
	var callback = {
		success:function(o){
			var options = JSON.parse(o.responseText);
			if(document.getElementById(relatedModuleFieldName)){
				var relatedModuleField = document.getElementById(relatedModuleFieldName);
				var relatedModuleFieldValue = relatedModuleField.value;
				relatedModuleField.innerHTML = '';
				relatedModuleField.innerHTML = options;
				
				for (var i=0; i<relatedModuleField.length; i++){
					if(relatedModuleField.options[i].value == relatedModuleFieldValue){
						relatedModuleField.selectedIndex = i;
						break;
					}
				}
			}else{
				alert ('Related Module field not found. Please Contact your Administrator.');
			}
			 
		},
		failure:function(o){
			alert(SUGAR.language.get('app_strings','LBL_AJAX_FAILURE'));
		}
	}
	postData = '&sugar_body_only=true&selectedModule=' + selectedModuleValue;

	YAHOO.util.Connect.asyncRequest('POST', 'index.php?module=sp_Timesheet&action=getRelatedModulesList', callback, postData);
}
</script>
{/literal}