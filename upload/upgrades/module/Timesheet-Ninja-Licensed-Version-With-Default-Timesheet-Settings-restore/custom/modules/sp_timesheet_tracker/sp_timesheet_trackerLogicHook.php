<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

class sp_timesheet_trackerLogicHook {

    function sp_timesheet_trackerLogicHookFunc(&$bean, $event, $arguments) {
		$bean->related_record_url = htmlspecialchars_decode($bean->related_record_url);
	}
	function saveProjectFunc(&$bean, $event, $arguments) {
		global $db;
		if(empty($bean->project_id_c) && $bean->parent_type=='Project'){
			$qry_upd = "update sp_timesheet_tracker set project_id_c=parent_id where id='".$bean->id."'";
			$db->query($qry_upd);
		}
	}
}
