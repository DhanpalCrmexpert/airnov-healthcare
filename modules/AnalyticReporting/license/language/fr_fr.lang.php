<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

$license_strings = array (
    'LBL_STEPS_TO_LOCATE_KEY_TITLE' => 'Pour retrouver la cl�',
    'LBL_STEPS_TO_LOCATE_KEY_1' => '1. Connectez-vous �',
    'LBL_STEPS_TO_LOCATE_KEY_2' => '2. Allez � Account->Purchases',
    'LBL_STEPS_TO_LOCATE_KEY_3' => '3. Recherchez la cl� pour l\'achat de cet add-on',
    'LBL_STEPS_TO_LOCATE_KEY_4' => '4. Coller dans la bo�te de cl� de licence ci-dessous',
    'LBL_STEPS_TO_LOCATE_KEY_5' => '5. Cliquez sur "Validate"',
    'LBL_LICENSE_KEY' => 'Cl� de licence',
    'LBL_CURRENT_USERS' => 'Nombre actuel de l\'utilisateur',
    'LBL_LICENSED_USERS' => 'Licence Nombre d\'utilisateurs',
    'LBL_VALIDATE_LABEL' => 'Valider',
    'LBL_VALIDATED_LABEL' => 'Valid�',
);

