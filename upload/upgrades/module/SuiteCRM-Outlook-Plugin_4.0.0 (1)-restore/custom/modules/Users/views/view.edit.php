<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once('modules/Users/views/view.edit.php');

class CustomUsersViewEdit extends UsersViewEdit {
    function display() {
		global $current_user;
		if(file_exists){
			unlink('cache/themes/SuiteP/modules/Users/EditView.tpl');
		}
		if(!is_admin($current_user)){
			unset($this->ev->defs['panels']['LBL_TIMESHEET_NINJA_USER_CONFIGURATION']);
		}
		parent::display();
    }
}