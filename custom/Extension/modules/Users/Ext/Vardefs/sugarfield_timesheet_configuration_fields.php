<?php
$dictionary['User']['fields']['timesheet_lock'] =  array (
      'name' => 'timesheet_lock',
      'vname' => 'LBL_TIMESHEET_LOCK',
      'type' => 'enum',
      'audited' => 1,
	  'default' => 'none',
	  'len' => 20,
      'options' => 'timesheet_lock_dom',
    );
	
// $dictionary['User']['fields']['reminder_for_time_log'] =  array (
      // 'name' => 'reminder_for_time_log',
      // 'vname' => 'Reminder to Enter Time',
      // 'label' => 'Reminder to Enter Time',
      // 'type' => 'bool',
	  // 'default' => false,
    // );
	
$dictionary['User']['fields']['require_description'] =  array (
      'name' => 'require_description',
      'vname' => 'Description Required',
      'label' => 'Description Required',
      'type' => 'bool',
	  'default' => false,
    );
	
$dictionary['User']['fields']['reminder_to_enter_time'] =  array (
      'name' => 'reminder_to_enter_time',
      'vname' => 'Reminder to enter time',
      'label' => 'Reminder to enter time',
      'type' => 'bool',
	  'default' => false,
    );	
	
$dictionary['User']['fields']['reminder_time_limit'] =  array (
      'name' => 'reminder_time_limit',
      'vname' => 'Reminder time limit',
      'label' => 'Reminder time limit',
      'type' => 'decimal',
	  'default' => 0.0,
	  'len' => '18',
	  'size' => '10',
	  'precision' => '2',
    );	
	
$dictionary['User']['fields']['users_to_get_reminder'] = array (
	'name' => 'users_to_get_reminder',
	'vname' => 'Users to get reminder',
	'label' => 'Users to get reminder',
	'function' => 'getUsersList',
	'type' => 'multienum',
	'dbType' => 'text',
	'comment' => 'This is multi-select field for related users.',
);	
	
$dictionary['User']['fields']['sp_timesheet_module'] = array (
	'name' => 'sp_timesheet_module',
	'vname' => 'Module',
	'label' => 'Module',
	'function' => 'getModulesList',
	'type' => 'enum',
	'dbType' => 'text',
	'comment' => 'Display list of modules.',
);	
	
$dictionary['User']['fields']['sp_timesheet_related_module'] = array (
	'name' => 'sp_timesheet_related_module',
	'vname' => 'Related Module',
	'label' => 'Related Module',
	'function' => 'getRelatedModulesList',
	'type' => 'dynamicenum',
	'parentenum' => 'sp_timesheet_module',
	'dbType' => 'text',
	'comment' => 'Display list of realted modules.',
);


$dictionary['User']['fields']['user_preferred_timesheet_view'] = array (
	'name' => 'user_preferred_timesheet_view',
	'vname' => 'User preferred view',
	'label' => 'User preferred view',
	'type' => 'multienum',
	'options' => 'user_preferred_view_list',
	'isMultiSelect' => true,
	'default' => '^Daily^,^Weekly^',
	'dbType' => 'text',
	'comment' => 'This is preferred user view for timesheet set by admin.',
);
?>