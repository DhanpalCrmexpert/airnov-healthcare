{php}
global $sugar_config;
{/php}

<div class="moduleTitle">
    <h2 class="module-title-text">
        <a href="#">SendGrid</a><span class="pointer">»</span> CONFIGURATION
    </h2>
    <div class="clear"></div>
</div>

<form id="sendgrid" name="sendgrid" method="POST" action="">
    <table width="100%" cellpadding="0" cellspacing="0" border="0" class="dcQuickEdit">
        <tr>
            <td style="padding-bottom: 2px;" width="100%">
                <input title="SAVE" name="save_sendgrid"  class="button" type="submit" value="SAVE">
                <input title="CANCEL" onclick="document.location.href = 'index.php?module=Administration&action=index'" class="button" type="button" value="CANCEL">
            </td>
        </tr>
    </table>

   

    <div class="clear"></div>
    <div class="clear"></div>
    <br>
    <div name="twilio" id="twilio">
        <div class="panel panel-default">
            <div class="panel-heading ">
                <a class="" role="button" data-toggle="collapse-edit" aria-expanded="false">
                    <div class="col-xs-10 col-sm-11 col-md-11">
                       Configure the SendGrid
                    </div>
                </a>
            </div>

            <div class="panel-body panel-collapse collapse in" id="detailpanel_-1">
                <div class="tab-content">
                    <!-- tab_panel_content.tpl -->
                    <br>
                    
                    <div class="row edit-view-row">    
                        <div>
                            <div class="col-xs-12 col-sm-4 label" style="color: black;">
                            SendGrid API Key :
                            </div>
                            <div class="col-xs-12 col-sm-8 edit-view-field " type="varchar">
                                <input type="text" name="sendgrid_api_key" id="sendgrid_api_key" size="50" maxlength="255" value="{$sendgrid_api_key}">
                            </div>
                            <!-- [/hide] -->
                        </div>
                    </div>
                    <br>
                    {if !empty($sendgrid_api_key)}
                    <div class="row edit-view-row">
                         <div>
                            <div class="col-xs-12 col-sm-4 label" style="color: black;">
                            Use In Campaign : 
                            </div>
                            <div class="col-xs-12 col-sm-8 edit-view-field " type="varchar">
                                <input type="checkbox" name="use_sendgrid_as_default" id="use_sendgrid_as_default" size="50" maxlength="255" {$use_sendgrid_as_default}>
                            </div>
                            <!-- [/hide] -->
                        </div>
                    </div>  
                    {/if}
                     <div class="clear"></div>
                     <div class="clear"></div>
                </div>
            </div>
        </div>

    </div>

</form>
<script type="text/javascript">
    {literal}
    //var dreampb = new PromptBoxes();
    {/literal}
</script>

