<script src="modules/{$moduleName}/assets/js/jquery/jquery-1.8.3.min.js"></script>
<script src="modules/{$moduleName}/assets/js/legacy/startwith.js"></script>
<script src="modules/{$moduleName}/assets/js/jquery-ui/jquery-ui.min.js"></script>
<script src="modules/{$moduleName}/assets/js/jquery/jquery.select2.min.js"></script>
<script type="text/javascript">
    jQuery.noConflict();
</script>


<script src="modules/{$moduleName}/assets/js/ractive/Ractive.min.js"></script>
<script src="modules/{$moduleName}/assets/js/ractive/ractive-decorators-select2.min.js"></script>

<link href="modules/{$moduleName}/assets/css/{$moduleName}.css" rel="stylesheet" type="text/css" media="all" />
<link href="modules/{$moduleName}/assets/css/ReportBuilder.css" rel="stylesheet" type="text/css" media="all" />
<link href="modules/{$moduleName}/assets/css/jquery-ui/redmond/jquery-ui.css" rel="stylesheet" type="text/css" media="all" />
<link href="modules/{$moduleName}/assets/css/jquery.select2.css" rel="stylesheet" type="text/css" media="all" />

<script>
    var translated_labels = {$DICTIONARY};
    var urls = {$urlsJSON};
</script>

{include file="modules/$moduleName/templates/templates.tpl"}

<script src="modules/{$moduleName}/assets/js/ractive/ractive-decorators-select2.min.js"></script>
<!-- env:dev --#>
<script src="modules/{$moduleName}/assets/js/components/reports/shared.js"></script>
<script src="modules/{$moduleName}/assets/js/components/shared.js"></script>
<script src="modules/{$moduleName}/assets/js/components/reportManager.js"></script>
<script src="modules/{$moduleName}/assets/js/components/manager/accessManager.js"></script>
<script src="modules/{$moduleName}/assets/js/components/manager/scheduleManager.js"></script>
<!-- env:dev:end -->
<!-- env:prod -->
<script src="modules/{$moduleName}/assets/js/main.min.js?v=2.1.327"></script>
<!-- env:prod:end -->


<h1>{$MOD.reportingToolSettings}</h1>

<a class="ar-button" style="margin:5px;" href="{$urls.index}">Back to reports</a>

{* If reportID is passed, then offer to return to current report *}
{if $REPORTID neq false}
    <a class="ar-button green" style="margin:5px;" href="{$urls.report}{$REPORTID}">Back to report</a>
{/if}

<div class="settings-wrap">
    <div id="ar-rv-editor" class="ar-rv-section">
        <div id="ar-rv-editor-tabs" class="ar-rv-tabs">
            <ul>
                {if $ISADMIN}
                <li><a href="#ar-rv-editor-license-managment">{$MOD.licenseManagment}</a></li>
                <li><a href="#ar-rv-editor-format-settings">{$MOD.formatSettings}</a></li>
                <li><a href="#ar-rv-editor-report-builder">{$MOD.permissions}</a></li>
                <li><a href="#ar-rv-editor-section-states">{$MOD.sectionStates}</a></li>
                {/if}

                <li><a href="#ar-rv-editor-templates">{$MOD.templates}</a></li>

                {if $ISADMIN}
                <li><a href="#ar-rv-editor-processes">{$MOD.processes}</a></li>
                <li><a href="#ar-rv-editor-scheduler-debug">Test Scheduler</a></li>
                <li><a href="#ar-rv-editor-dashboard-scheduler-debug">Test Dashboard Scheduler</a></li>
                <li><a href="#ar-rv-editor-logs">Logs</a></li>
                {/if}
            </ul>
            {if $ISADMIN}
            <div id="ar-rv-editor-license-managment">
                {include file="modules/$moduleName/templates/settings/license.tpl"}
            </div>
            <div id="ar-rv-editor-format-settings">
                {include file="modules/$moduleName/templates/settings/format.tpl"}
            </div>
            <div id="ar-rv-editor-report-builder">
                {include file="modules/$moduleName/templates/settings/permissions.tpl"}
            </div>
            <div id="ar-rv-editor-section-states">
                {include file="modules/$moduleName/templates/settings/states.tpl"}
            </div>
            {/if}

            <div id="ar-rv-editor-templates">
                {include file="modules/$moduleName/templates/settings/templates.tpl"}
            </div>

            {if $ISADMIN}
            <div id="ar-rv-editor-processes">
                {include file="modules/$moduleName/templates/settings/processes.tpl"}
            </div>
            <div id="ar-rv-editor-scheduler-debug">
                {include file="modules/$moduleName/templates/settings/scheduler.tpl"}
            </div>
            <div id="ar-rv-editor-dashboard-scheduler-debug">
                {include file="modules/$moduleName/templates/settings/dashboardScheduler.tpl"}
            </div>
            <div id="ar-rv-editor-logs">
                {include file="modules/$moduleName/templates/settings/logs.tpl"}
            </div>
            {/if}
        </div>
    </div>
</div>
<script>
    //TODO: move to external script
    {literal}
    jQuery("#ar-rv-editor-tabs").tabs({
        {/literal}
        active: {$currentTab}
        {literal}
    });
    {/literal}
</script>
