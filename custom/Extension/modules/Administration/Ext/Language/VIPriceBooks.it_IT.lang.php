<?php
/*********************************************************************************
 * This file is part of package Price Books.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Price Books is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
$mod_strings['LBL_PRICE_BOOKS'] = "Libri dei prezzi";
$mod_strings['LBL_PRICE_BOOKS_DESCRIPTION'] = "Gestisci i listini prezzi dei prodotti per particolari contatti / account.";
$mod_strings['LBL_ENABLE_PRICE_BOOKS'] = "Abilita funzionalità libri prezzi";
$mod_strings['LBL_PRICE_BOOKS_ACTIVATED'] = "Funzione libri prezzi attivata correttamente.";
$mod_strings['LBL_PRICE_BOOKS_DEACTIVATED'] = "Funzione libri a prezzo disattivata correttamente.";
$mod_strings['LBL_CLICK_HERE'] = "Fai clic qui";
$mod_strings['LBL_MANAGE_PRICEBOOK'] = "Gestire i libri dei prezzi";
//license
$mod_strings['LBL_LICENCE_ACTIVE_LABEL'] = "VIPriceBooksLicenseAddon non è più attivo a causa del seguente motivo:";
$mod_strings['LBL_LICENCE_ISSUES'] = "Gli utenti non avranno accesso illimitato fino a quando il problema non sarà stato risolto";
$mod_strings['LBL_CLICK_HERE'] = "Fai clic qui";
$mod_strings['LBL_LICENCE_ACTIVE'] = "VIPriceBooksLicenseAddon non è più attivo";
$mod_strings['LBL_RENEW_LICENCE'] = "Rinnova l'abbonamento o verifica la configurazione della licenza.";
$mod_strings['LBL_UPDATE_LICENSE'] = "Aggiorna licenza";
?>