<?php
/**
 * Copyright (c) SalesAgility Ltd.  All Rights Reserved.
 * Licensed under the GPL3 License.  See License in the project root
 * for license information.
 *
 * @package Outlook Addin for SuiteCRM
 * @copyright 2020 SalesAgility Ltd http://www.salesagility.com
 * @author SalesAgility <info@salesagility.com
 *
 */

namespace SalesAgility\Outlook\Sync;

use BeanFactory;
use Exception;
use Microsoft\Graph\Model;
use Microsoft\Graph\Model\FileAttachment;
use SugarBean;
use SalesAgility\Outlook\Outlook;


class OutlookSyncService implements SyncService
{
    /**
     * @param SyncAPI $syncAPI
     * @return bool
     * @throws Exception
     */
    public function run(SyncAPI $syncAPI)
    {
        $outlook = new Outlook();
        $config = $outlook->getSettings();

        if (empty($config)) {
            throw new Exception('No Outlook Plugin Config');
        }
        $success = $syncAPI->connect();
        if (!$success) {
            throw new Exception('Failed to connect to sync API');
        }
        $users = $this->getSyncUsers();
        $res = true;
        foreach ($users as $user) {
            $res = $res && $this->syncUser($user, $syncAPI);
        }

        return $res;
    }

    /**
     * @return SugarBean[]
     */
    private function getSyncUsers()
    {
        $userBean = BeanFactory::getBean("Users");

        return $userBean->get_full_list(
            "users.sa_outlook_sync_date",
            "users.sa_outlook_enable AND users.sa_outlook_is_licensed"
        );
    }

    /**
     * @param SugarBean $user
     * @param SyncAPI $syncAPI
     * @return bool
     */
    private function syncUser(SugarBean $user, SyncAPI $syncAPI)
    {
        global $timedate;
        $userEmail = $user->emailAddress->getPrimaryAddress($user);
        if (empty($userEmail)) {
//            return true - if user has no address, no mail sync is expected
            return true;
        }
        $res = true;
        $folders = unencodeMultienum($user->sa_outlook_folders);
        $since = null;

        if($user->sa_outlook_sync_date){
            $since = $timedate->fromUser($user->sa_outlook_sync_date);
        }
        $now = $timedate->nowDb();
        foreach($folders as $folder) {
            $emails = $syncAPI->getEmails($userEmail, $folder,$since ?: null);
            foreach ($emails as $email) {
                $syncRes = $this->syncEmail($user->id, $userEmail, $email, $syncAPI);
                $res = $res && $syncRes;
            }
        }
        $now = $user->db->quoted($now);
        $user->db->query("UPDATE users SET sa_outlook_sync_date = $now WHERE id = ".$user->db->quoted($user->id));
        return $res;
    }

    /**
     * @param $l
     * @param bool $addrOnly
     * @return string
     */
    private function emailListToString($l,$addrOnly = false)
    {
        $bits = [];
        foreach ($l as $email) {
            if($addrOnly){
                $bits[] = $email['emailAddress']['address'];
            }else{
                $bits[] = $email['emailAddress']['name'] . " <" . $email['emailAddress']['address'] . ">";
            }
        }
        return implode(",", $bits);
    }

    /**
     * @param $userId
     * @param $userEmail
     * @param Model\Message $email
     * @param SyncAPI $syncApi
     * @return bool|string
     */
    private function syncEmail($userId, $userEmail, $email, SyncAPI $syncApi)
    {
        global $timedate;
        $crmEmail = BeanFactory::newBean("Emails");

        $isDupe = $crmEmail->retrieve_by_string_fields(['uid' => $email->getId()]);
        if ($isDupe) {
            return true;
        }

        $crmEmail->name = $email->getSubject();
        $crmEmail->created_by = $userId;
        $crmEmail->assigned_user_id = $userId;
        $crmEmail->last_synced = $timedate->nowDb();
        $crmEmail->date_sent_received = $timedate->asDb($email->getReceivedDateTime());
        $from = false;
        if($email->getFrom()) {
            $from = $email->getFrom()->getEmailAddress();
        }
        if($from) {
            $crmEmail->from_addr_name = $from->getName() . " <" . $from->getAddress() . ">";
            $crmEmail->from_addr = $from->getAddress();
        }
        $body = $email->getBody();
        $crmEmail->description = $body->getContent();
        $crmEmail->description_html = $body->getContent();

        $crmEmail->to_addrs_names = $this->emailListToString($email->getToRecipients());
        $crmEmail->to_addrs = $this->emailListToString($email->getToRecipients(),true);

        $crmEmail->cc_addrs_names = $this->emailListToString($email->getCcRecipients());
        $crmEmail->cc_addrs = $this->emailListToString($email->getCcRecipients(),true);

        $crmEmail->bcc_addrs_names = $this->emailListToString($email->getBccRecipients());
        $crmEmail->bcc_addrs = $this->emailListToString($email->getBccRecipients(),true);

        $inbound = $from && $from->getAddress() !== $userEmail;
        if ($email->getIsDraft()) {
            $crmEmail->from_addr_name = "<" . $userEmail . ">";
            $crmEmail->from_addr = $userEmail;

            $crmEmail->status = 'draft';
            $crmEmail->type = 'draft';
        } else if ($inbound) {
            $crmEmail->status = $email->getIsRead() ? "read" : "unread";
            $crmEmail->type = 'inbound';
        } else {
            $crmEmail->status = 'sent';
            $crmEmail->type = 'out';
        }

        $crmEmail->mailbox_id = $userEmail;
        $crmEmail->message_id = $email->getInternetMessageId();
        $crmEmail->uid = $email->getId();
        $result = $crmEmail->save();

        if($email->getHasAttachments()) {
            $result = $result && $this->syncAttachments($userEmail, $crmEmail->uid, $crmEmail->id, $syncApi);
        }

        return $result;
    }

    /**
     * @param $userEmail
     * @param $outlookEmailId
     * @param $suiteEmailId
     * @param SyncAPI $syncApi
     * @return bool
     */
    private function syncAttachments($userEmail, $outlookEmailId, $suiteEmailId, SyncAPI $syncApi)
    {
        global $sugar_config;
        /** @var FileAttachment[] $attachments */
        $attachments = $syncApi->getAttachments($userEmail, $outlookEmailId);
        foreach ($attachments as $attachment) {
            $extPtr = strrpos($attachment->getName(), '.');
            $extension = substr($attachment->getName(), ($extPtr+1));
            if (!$extPtr || !$extension || in_array($extension, $sugar_config['upload_badext'], true)) {
                $GLOBALS['log']->error('[Outlook Graph API] Attachment sync failed - file extension "' . $extension . '" not permitted');
                continue;
            }
            if ($attachment->getSize() > $sugar_config['upload_maxsize']) {
                $GLOBALS['log']->error('[Outlook Graph API] Attachment sync failed - file size exceeds max upload size');
                continue;
            }

            $note = BeanFactory::newBean("Notes");
            $note->name = $attachment->getName();
            $note->save();

            $fileName = $note->id;
            $b64Contents = $attachment->getContentBytes();
            $filePath = $sugar_config['upload_dir'] . $fileName;
            $contents = base64_decode($b64Contents);
            $file = fopen($filePath, 'wb');
            fwrite($file, $contents);
            fclose($file);

            $mimeType = mime_content_type($filePath);
            $note->filename = $attachment->getName();
            $note->uploadfile = $attachment->getName();
            $note->file_mime_type = $mimeType;
            $note->parent_type = 'Emails';
            $note->parent_id = $suiteEmailId;
            $note->save();
        }
        return true;
    }
}
