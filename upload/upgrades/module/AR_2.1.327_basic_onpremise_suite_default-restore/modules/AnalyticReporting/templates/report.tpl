<script src="modules/{$moduleName}/assets/js/jquery/jquery-1.8.3.min.js"></script>
<script src="modules/{$moduleName}/assets/js/legacy/startwith.js"></script>
<script type="text/javascript">
    jQuery.noConflict();
    jQuery('link[rel="stylesheet"][href*="nvd3.css"]').attr('disabled', 'disabled');
    jQuery('link[rel="stylesheet"][href*="nvd3.css"]').remove();

    // Add EventSource polyfill for Internet Explorer
    {literal}
    if(!window.EventSource){
        document.write('<script type="text/javascript" src="modules/{/literal}{$moduleName}{literal}/assets/js/EventSource.js"><\/script>');
    }
</script>
<script data-pace-options='{ "ajax": false }' src="modules/{/literal}{$moduleName}{literal}/assets/js/pace.js"></script>
{/literal}
<link href="modules/{$moduleName}/assets/css/pace.css" rel="stylesheet" type="text/css" media="all" />

<script src="modules/{$moduleName}/assets/js/jquery-ui/jquery-ui.min.js"></script>

<script src="modules/{$moduleName}/assets/js/jquery/jquery.select2.min.js"></script>
<script src="modules/{$moduleName}/assets/js/jquery/jquery.event.drag-2.2.js"></script>
<script src="modules/{$moduleName}/assets/js/spectrum.js"></script>
<script src="modules/{$moduleName}/assets/js/toastr.min.js"></script>
<script src="modules/{$moduleName}/assets/js/jquery.fileDownload.js"></script>

<script src="modules/{$moduleName}/assets/js/moment.min.js"></script>

<script src="modules/{$moduleName}/assets/js/slickgrid/slick.core.js"></script>
<script src="modules/{$moduleName}/assets/js/slickgrid/slick.formatters.js"></script>
<script src="modules/{$moduleName}/assets/js/slickgrid/slick.editors.js"></script>
<script src="modules/{$moduleName}/assets/js/slickgrid/plugins/slick.cellrangedecorator.js"></script>
<script src="modules/{$moduleName}/assets/js/slickgrid/plugins/slick.cellrangeselector.js"></script>
<script src="modules/{$moduleName}/assets/js/slickgrid/plugins/slick.cellselectionmodel.js"></script>
<script src="modules/{$moduleName}/assets/js/slickgrid/slick.grid.js"></script>
<script src="modules/{$moduleName}/assets/js/slickgrid/slick.groupitemmetadataprovider.js"></script>
<script src="modules/{$moduleName}/assets/js/slickgrid/slick.dataview.js"></script>
<script src="modules/{$moduleName}/assets/js/slickgrid/controls/slick.pager.js"></script>
<script src="modules/{$moduleName}/assets/js/slickgrid/controls/slick.columnpicker.js"></script>
<script src="modules/{$moduleName}/assets/js/ractive/Ractive.min.js"></script>
<script src="modules/{$moduleName}/assets/js/ractive/ractive-decorators-select2.min.js"></script>
<script src="modules/{$moduleName}/assets/js/ractive/Ractive-decorators-sortable.js"></script>
<script src="modules/{$moduleName}/assets/js/lodash.min.js"></script>
<script src="modules/{$moduleName}/assets/js/jquery.doubleScroll.js"></script>

<link href="modules/{$moduleName}/assets/css/slickgrid/slick.grid.css" rel="stylesheet" type="text/css">
<link href="modules/{$moduleName}/assets/css/slickgrid/controls/slick.pager.css" rel="stylesheet" type="text/css">
<link href="modules/{$moduleName}/assets/css/slickgrid/controls/slick.columnpicker.css" rel="stylesheet" type="text/css">
<link href="modules/{$moduleName}/assets/css/slickgrid/default-theme.css" rel="stylesheet" type="text/css">
<link href="modules/{$moduleName}/assets/css/jquery.select2.css" rel="stylesheet" type="text/css" media="all" />

<link href="modules/{$moduleName}/assets/css/jquery-ui/redmond/jquery-ui.css" rel="stylesheet" type="text/css" media="all" />
<link href="modules/{$moduleName}/assets/css/leaflet/leaflet.css" rel="stylesheet" type="text/css" media="all" />
<link href="modules/{$moduleName}/assets/css/leaflet/dvf.css" rel="stylesheet" type="text/css" media="all" />
<link href="modules/{$moduleName}/assets/css/spectrum.css" rel="stylesheet" type="text/css">
<link href="modules/{$moduleName}/assets/css/toastr.min.css" rel="stylesheet" type="text/css">
{literal}
<style>
    #ar-rv-data-grid {
        /*max-height:500px;*/
        border:1px solid #ddd;
    }
    #reportDescription{
        margin-left:18px;
        display: none;
    }
    /* Fix for gauge chart */
    .nvd3-svg {
        height:100%;
    }
    /* Remove leaflet legend bars scale "stairs" */
    .scale-bars i {
        height:20px !important
    }
</style>
{/literal}

{*
* Fixed SuiteCRM issue with selects in reporting filters (for example between),
*  where selectboxes have high padding (0 52px 0 5px) and therefore breaks filter lines.
*}
{if $systemType eq 'SUITECRM'}
{literal}
<style>
    body {
        font-size:12px !important;
    }
    #ar-chart-app * {
        font-size:12px !important;
    }
    select {padding:0 32px 0 5px}
</style>
{/literal}
{/if}

<div id="modalWindow" style="display:none;"></div>
<div id="ar-container">
    <div id="reportControls" style="position:relative;margin-left:5px;"></div>
    <h2 style="margin-top:20px;margin-left:5px;color:#333;" id="reportTitleContainer">
    <span id="reportTitle">{$REPORTTITLE}</span>{if $CANEDIT eq true}<a id="ar-rv-editor-title" href="#"><img src="modules/{$moduleName}/assets/img/slickgrid/pencil.gif"></a>{/if}
    </h2>
    <span id="reportDescription">{$REPORTDESCRIPTION}</span>
    <div id="ar-rv-editor" class="ar-rv-section">
        {assign var="sectionName" value="ar-rv-editor"}
        <h5><a href="#" class="toggle {if $REPORTSECTIONSTATES.$sectionName}desc{else}asc{/if}">{$MOD.label_report_editor}</a></h5>
		<div id="ar-rv-editor-tabs" class="ar-rv-tabs" {if $REPORTSECTIONSTATES.$sectionName}style="display:none"{/if}>
            <ul>
                <li><a href="#ar-rv-editor-filters">{$MOD.label_filters}</a></li>
                <li><a href="#ar-rv-editor-fields">{$MOD.label_fields}</a></li>
                {if $REPORTCOMBINED neq true} {* #5098 *}
                <li><a href="#ar-rv-editor-calcfields">{$MOD.label_calc_fields}{if $ISBASIC} {$MOD.label_pro}{/if}</a></li>
                {/if}
                <li><a href="#ar-rv-editor-agregates">{$MOD.label_aggregates}</a></li>
                <li><a href="#ar-rv-editor-groupingsorting">{$MOD.label_grouping_sorting}</a></li>
                <li><a href="#ar-rv-editor-labels">{$MOD.label_labels}</a></li>
                <li><a href="#ar-rv-editor-templates">{$MOD.templates}{if $ISBASIC} {$MOD.label_pro}{/if}</a></li>
                <li><a href="#ar-rv-editor-access">{$MOD.label_access}</a></li>
                {if $debugMode}
                <li><a href="#ar-rv-editor-debug">Debug</a></li>
                {/if}
            </ul>
        <div id="ar-rv-editor-filters"></div>
        <div id="ar-rv-editor-fields"></div>
        {if $REPORTCOMBINED neq true} {* #5098 *}
        <div id="ar-rv-editor-calcfields"></div>
        {/if}
        <div id="ar-rv-editor-agregates"></div>
        <div id="ar-rv-editor-groupingsorting"></div>
        <div id="ar-rv-editor-templates"></div>
        <div id="ar-rv-editor-labels"></div>
        <div id="ar-rv-editor-access">
            <strong>{$MOD.label_sharing}</strong><br>
            <div id="ar-rv-editor-access-sharing"></div><br>
            {if $CANEDIT eq true || $CANDELETE eq true}
            <strong>{$MOD.label_scheduling}</strong><br>
            <div id="ar-rv-editor-access-scheduling"></div>
            {/if}
        </div>
            {if $debugMode}
                <div id="ar-rv-editor-debug">
                    <h2>Request</h2>
                    <textarea>{$debugReport}</textarea>

                    <div id="debugManager"></div>
                </div>
            {/if}
        </div>
    </div>

    <div id="ar-rv-data" class="ar-rv-section">
        {assign var="sectionName" value="ar-rv-data"}
        <h5><a href="#" class="toggle {if $REPORTSECTIONSTATES.$sectionName}desc{else}asc{/if}">{$MOD.label_report}</a></h5>
        <div {if $REPORTSECTIONSTATES.$sectionName}style="display:none"{/if}>
            <div class="pagination" id="pagination-top"></div>

            <div id="displayData">
                <div id="ar-rv-data-grid"></div>
            </div>

            <div class="pagination" id="pagination-bottom"></div>

            <br />

            <span>
                {if $CANEDIT eq true || $CANEXPORT eq true}
                {include file="modules/$moduleName/templates/elements/exportButtonsBottom.tpl"}
                {/if}
            </span>
        </div>
	</div>

    <div id="ar-chart-app"></div>
</div>

{* Include underscore.js and Ractive.js templates *}
{include file="modules/$moduleName/templates/templates.tpl"}


<script>
/**
 * Define defaults for ReportGrid and ReportChart
 */
var ReportData = {$REPORTDATA};
var isBasic = {$ISBASIC} ? true : false;
var wasNewInstall = {$WASNEWINST} ? true : false;
var envConfig = {literal}{{/literal}isBasic:isBasic,wasNewInstall:wasNewInstall{literal}}{/literal};

//#5843 Adding widget to user and role is only allowed for the owner and admin
var current_user = {$CURRENTUSER};
//#3831 [start] - get report folders
var current_folder={$current_report_folder};
var report_folders=[];
{foreach from=$report_folders key=folder_key item=report_folder}
    report_folders[{$folder_key}] = "{$report_folder}";
{/foreach}
//#3831 [end]
var translated_labels = {$DICTIONARY}; // #5286 - Refactored to JSON object
ReportData.dictionary = translated_labels;
var excelTemplates = {$excelTemplates};
var reportNamesForTemplates = {$reportNamesForTemplates};

var userPermissions = {$userPermissions};
var moduleName = "AnalyticReporting";
var urls = {$urlsJSON};

var aggregatesDefinitions = {$aggregatesDefinitionsJSON};
var aggregatesCheckBoxesChecked = {$aggregatesCheckBoxesChecked};
var aggregatesCheckBoxesNotChecked = {$aggregatesCheckBoxesNotChecked};

var currentUser = {$currentUser};

{if $debugMode}
    var reportDebugMode = true;
{/if}

{literal}
// #5141 - When SlickGrid is finally rendered add top scroll
jQuery(document).on("slickRenderCompleted", function(){
    // By default check for double scroll in left (main) pane
    jQuery(".slick-viewport-top.slick-viewport-left").doubleScroll({
        beforeElement: ".slick-header.slick-header-left:first",
        onlyIfScroll: false,
        resetOnWindowResize: true,
        timeToWaitForResize:0,
        scrollCss: {
            'overflow-x': 'scroll',
        },
        contentCss: {
            'overflow-x': 'scroll',
        },
    });
    // If there is some frozen columns, then add double scroll on second pane too
    if(ReportData.options.frozenColumn && ReportData.options.frozenColumn > 0) {
        jQuery(".slick-viewport-top.slick-viewport-right").doubleScroll({
            beforeElement: ".slick-header.slick-header-right:first",
            onlyIfScroll: false,
            resetOnWindowResize: true,
            timeToWaitForResize:0,
            scrollCss: {
                'overflow-x': 'scroll',
            },
            contentCss: {
                'overflow-x': 'scroll',
            },
        });
    }

});
{/literal}

{* #4538 [start] - Resize container by content width, else it will be as width as .slick-header-columns *}
{literal}
function resizeContainer() {
    var newWidth = jQuery("#content").width() - 40;
    jQuery("#ar-container").width(newWidth);
}

jQuery(window).resize(function(){
    resizeContainer();
});
resizeContainer();
{/literal}
{* #4538 [end] *}
</script>

<script src="modules/{$moduleName}/assets/js/multiselect/jquery.multiselect.js"></script>
<link href="modules/{$moduleName}/assets/css/multiselect/jquery.multiselect.css" rel="stylesheet" type="text/css">
{if $REPORTCOMBINED eq true}
    <script type="text/javascript">
    {literal}
    for(var i = 0; i < ReportData.availableFields.length; i++) {
        for(var j = 0; j < ReportData.availableFields[i].fields.length; j++) {
            ReportData.availableFields[i].fields[j].moduleId = ReportData.availableFields[i].fields[j].name.split("_")[0];
        }
    }
    for(var i = 0; i < ReportData.selectableFields.length; i++) {
        for(var j = 0; j < ReportData.selectableFields[i].fields.length; j++) {
            ReportData.selectableFields[i].fields[j].moduleId = ReportData.selectableFields[i].fields[j].name.split("_")[0];
        }
    }

    // #5382 - After saving combined report, remove first level grouping elements from common grouping
    ReportData.fieldGroupingSorting = ReportData.fieldGroupingSorting.filter(function (el) {
        return el.showAggregates !== undefined;
    });
    {/literal}
    </script>
{/if}

<script type="text/javascript">
    var $sugar_flavor = '{$sugar_flavor}';
    var $isSidecar = '{$isSidecar}';
    var $systemType = '{$systemType}';
    var $site_url = '{$site_url}';
</script>
<link href="modules/{$moduleName}/assets/css/{$moduleName}.css" rel="stylesheet" type="text/css" media="all" />
<!-- env:dev --#>
<script src="modules/{$moduleName}/assets/js/components/reports/shared.js"></script>
<script src="modules/{$moduleName}/assets/js/components/reports/standart.js"></script>
<script src="modules/{$moduleName}/assets/js/components/shared.js"></script>
<script src="modules/{$moduleName}/assets/js/components/reportManager.js"></script>
<script src="modules/{$moduleName}/assets/js/components/componentUtils.js"></script>
<script src="modules/{$moduleName}/assets/js/components/manager/debugManager.js"></script>
<script src="modules/{$moduleName}/assets/js/components/manager/filterManager.js"></script>
<script src="modules/{$moduleName}/assets/js/components/manager/fieldManager.js"></script>
<script src="modules/{$moduleName}/assets/js/components/manager/aggregateManager.js"></script>
<script src="modules/{$moduleName}/assets/js/components/manager/TotalTable.js"></script>
<script src="modules/{$moduleName}/assets/js/components/manager/calcColumns.js"></script>
<script src="modules/{$moduleName}/assets/js/components/manager/SortingByField.js"></script>
<script src="modules/{$moduleName}/assets/js/components/manager/SortingByCombinedField.js"></script>
<script src="modules/{$moduleName}/assets/js/components/manager/SortingBy.js"></script>
<script src="modules/{$moduleName}/assets/js/components/manager/SortingByCombined.js"></script>
<script src="modules/{$moduleName}/assets/js/components/manager/groupingManager.js"></script>
<script src="modules/{$moduleName}/assets/js/components/manager/templateManager.js"></script>
<script src="modules/{$moduleName}/assets/js/components/manager/labelManager.js"></script>
<script src="modules/{$moduleName}/assets/js/components/manager/accessManager.js"></script>
<script src="modules/{$moduleName}/assets/js/components/manager/scheduleManager.js"></script>
<script src="modules/{$moduleName}/assets/js/components/manager/calcFieldManager.js"></script>
<script src="modules/{$moduleName}/assets/js/components/manager/paginationManager.js"></script>
<script src="modules/{$moduleName}/assets/js/components/reportViewer.js"></script>
<!-- env:dev:end -->
<!-- env:prod -->
<script src="modules/{$moduleName}/assets/js/{$moduleName}.min.js?v=2.1.327"></script>
<!-- env:prod:end -->

<style media="print">{literal}
    #header, #footer, #bottomLinks, td>p.error {
        display: none;
    }
    #ar-container{
        display: block !important;
    }
    #ar-container > * {
        display: none;
    }
    #reportTitleContainer > *{
        display: none;
    }
    #reportTitleContainer{
        display: block !important;
    }
    #reportTitle{
        display: block !important;
    }

    #ar-rv-data{
        display: block !important;
    }
    #ar-rv-data > * {
        display: none;
    }

    #ar-rv-data > div{
        display: block !important;
    }
    #ar-rv-data > div > *{
        display: none;
    }
    #ar-rv-data > div > #displayData{
        display: block !important;
    }
    #displayData{
        display: block !important;
    }
    #ar-rv-data-grid {
        border: none;
    }
    {/literal}
</style>

<script src="modules/{$moduleName}/assets/js/chart/chartApp.min.js"></script>
<script>
    window.ARChartApp.init("ar-chart-app", window.ReportData, window.envConfig);
</script>