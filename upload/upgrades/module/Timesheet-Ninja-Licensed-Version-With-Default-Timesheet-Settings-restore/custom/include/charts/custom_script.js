$(document).ready(function(){
	function toggleDataSeries(e) {
	if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
		e.dataSeries.visible = false;
	}
	else {
		e.dataSeries.visible = true;
	}
	chart.render();
}

    if($('#date_time_by_day').val()!== "undefined" && $('#user_list_by_day').val()!== "undefined"){
		var search_date = $('#date_time_by_day').val();
		var user_id = $('#user_list_by_day').val();
		$.ajax({
					url:'index.php?entryPoint=timesheet_custom_view&search=true',
					data:{'date':search_date, 'user_id':user_id},
					type:'GET',
					success:function(req_response){
						$("#data-table").html('');
						$("#data-table").html(req_response);
						//$('#main-body-by-day').find('.thead-light').remove();
						//$('#main-body-by-day').find('tbody').remove();
						//$('#main-body-by-day').append(req_response);
					}
				});
	}
	
$(document).on('click','#search_time_by_day',function(e){
	var search_date = $('#date_time_by_day').val();
	var user_id = $('#user_list_by_day').val();
	$.ajax({
				url:'index.php?entryPoint=timesheet_custom_view&search=true',
				data:{'date':search_date, 'user_id':user_id},
				type:'GET',
				success:function(req_response){
					$("#data-table").html('');
					$("#data-table").html(req_response);
					//$('#main-body-by-day').find('.thead-light').remove();
					//$('#main-body-by-day').find('tbody').remove();
					//$('#main-body-by-day').append(req_response);
				}
			});
	
});

		$(document).on('focus','#report_date_start_trigger',function(){
				Calendar.setup ({
					inputField : "report_date_start",
					daFormat : "%m/%d/%Y",
					ifFormat : "%m/%d/%Y",
					showsTime : false,
					button : "report_date_start_trigger",
					singleClick : true,
					step : 1,
					weekNumbers:false
				});
		});
		
		$(document).on('focus','#report_date_end_trigger',function(){
				Calendar.setup ({
					inputField : "report_date_end",
					daFormat : "%m/%d/%Y",
					ifFormat : "%m/%d/%Y",
					showsTime : false,
					button : "report_date_end_trigger",
					singleClick : true,
					step : 1,
					weekNumbers:false
				});
		});
		
		YAHOO.util.Event.addListener('report_date_end', 'change', function(){
		  var start_date = $('#report_date_start').val();
		   var type = '';
		   var report_type_phrase ='';
		  if(start_date!='' && $(this).val()!=''){
			  if($('#report-time').length>0){
			    var report_time = $('#report-time').val();
				type = 'time_by_employee';
				report_type_phrase ='Time By Employee';
			  }else if($('#report-time-project').length>0){
				var report_time = $('#report-time-project').val();
				type = 'time_by_project';
				report_type_phrase ='Time on Projects';
			  }else if($('#report-time-cases').length>0){
				var report_time = $('#report-time-cases').val();
				type = 'time_by_cases';
				report_type_phrase ='Time Spent on Support Cases';
			  }else if($('#report-time-accounts').length>0){
				var report_time = $('#report-time-accounts').val();
				type = 'time_by_account';
				report_type_phrase ='Time Spent on Projects By Account';
			  }else if($('#report-time-payroll-cost').length>0){
				var report_time = $('#report-time-payroll-cost').val();
				type = 'payroll_cost_by_employee';
				report_type_phrase ='Payroll Cost by Employee';
			  }else if($('#report-time-employee-project').length>0){
				var report_time = $('#report-time-employee-project').val();
				type = 'time_by_employee_and_project';
				report_type_phrase ='Time By Employee and Project';
			  }else if($('#report-budget-by-hours-by-project').length>0){
				var report_time = $('#report-budget-by-hours-by-project').val();
				type = 'budget_by_hours_by_project';
				report_type_phrase ='Budgeted vs Actual Hours by Project';
			  }
				var start_date = '';
				var end_date = '';
				if(report_time=='is_between'){
					 start_date = $('#report_date_start').val();
					 end_date = $('#report_date_end').val();
					 $('#report-heading').html('<h3>'+report_type_phrase+' (Between '+start_date+' And '+end_date+')</h3>');
				}
				else{
					$('div.data').hide();
					return;
				}
				call_ajax('./index.php?module=sp_Timesheet&action=visualreports&sugar_body_only=true', 'GET', {'type':type,'time':report_time,'start_date':start_date,'end_date':end_date}, function(response){
					console.log('response----------'+response);
					if(response != undefined && response != null){
						if(type=='payroll_cost_by_employee'){
							drawChartPayroll($('#report-heading').text(), JSON.parse(response),'report-time-payroll-cost');
						}else if(type=='time_by_employee_and_project'){
							//drawChartPayroll($('#report-heading').text(), JSON.parse(response),'report-time-payroll-cost');
								var chart = new CanvasJS.Chart("chart-container", {
									animationEnabled: true,
								title:{
									text: "Time by Employee & Project",
									fontWeight: "normal",
								},	
								axisY: {
									title: "Medals"
								},
												
								toolTip: {
									shared: true
								},
								legend: {
									cursor:"pointer",
									itemclick: toggleDataSeries
								},
								data:JSON.parse(response),
								
									});
								chart.render();
						}else if(type=='budget_by_hours_by_project'){
								var chart = new CanvasJS.Chart("chart-container", {
									animationEnabled: true,
								title:{
									text: "Budgeted vs Actual Hours by Project",
									fontWeight: "normal",
								},	
								axisY: {
									title: "Medals"
								},
												
								toolTip: {
									shared: true
								},
								legend: {
									cursor:"pointer",
									itemclick: toggleDataSeries
								},
								data:JSON.parse(response),
								
									});
								chart.render();
						}else{
						  drawChart($('#report-heading').text(), JSON.parse(response),type);
						}
					}
				});

				$('div.data').show();
		  }
		});

		  YAHOO.util.Event.addListener('report_date_start', 'change', function(){
		  var end_date_report = $('#report_date_end').val();
		  var type = '';
		   var report_type_phrase ='';
		  if(end_date_report!='' && $(this).val()!=''){
			   if($('#report-time').length>0){
			    var report_time = $('#report-time').val();
				type = 'time_by_employee';
				report_type_phrase ='Time By Employee';
			  }else if($('#report-time-project').length>0){
				var report_time = $('#report-time-project').val();
				type = 'time_by_project';
				report_type_phrase ='Time on Projects';
			  }else if($('#report-time-cases').length>0){
				var report_time = $('#report-time-cases').val();
				type = 'time_by_cases';
				report_type_phrase ='Time Spent on Support Cases';
			  }else if($('#report-time-accounts').length>0){
				var report_time = $('#report-time-accounts').val();
				type = 'time_by_account';
				report_type_phrase ='Time Spent on Projects By Account';
			  }else if($('#report-time-payroll-cost').length>0){
				var report_time = $('#report-time-payroll-cost').val();
				type = 'payroll_cost_by_employee';
				report_type_phrase ='Time Payroll Cost by Employee';
			  }else if($('#report-time-employee-project').length>0){
				var report_time = $('#report-time-employee-project').val();
				type = 'time_by_employee_and_project';
				report_type_phrase ='Time By Employee and Project';
			  }else if($('#report-budget-by-hours-by-project').length>0){
				var report_time = $('#report-budget-by-hours-by-project').val();
				type = 'budget_by_hours_by_project';
				report_type_phrase ='Budgeted vs Actual Hours by Project';
			  }
				var start_date = '';
				var end_date = '';
				if(report_time=='is_between'){
					 start_date = $('#report_date_start').val();
					 end_date = $('#report_date_end').val();
					 $('#report-heading').html('<h3>'+report_type_phrase+' (Between '+start_date+' And '+end_date+')</h3>');
				}
				else{
					$('div.data').hide();
					return;
				}
				call_ajax('./index.php?module=sp_Timesheet&action=visualreports&sugar_body_only=true', 'GET', {'type':type,'time':report_time,'start_date':start_date,'end_date':end_date}, function(response){
					console.log('response----------'+response);
					if(response != undefined && response != null){
						if(type=='payroll_cost_by_employee'){
							drawChartPayroll($('#report-heading').text(), JSON.parse(response),'report-time-payroll-cost');
						}else if(type=='time_by_employee_and_project'){
								var chart = new CanvasJS.Chart("chart-container", {
									animationEnabled: true,
								title:{
									text: "Time by Employee & Project",
									fontWeight: "normal",
								},	
								axisY: {
									title: "Medals"
								},			
								toolTip: {
									shared: true
								},
								legend: {
									cursor:"pointer",
									itemclick: toggleDataSeries
								},
								data:JSON.parse(response),
								
									});
								chart.render();
						}else if(type=='budget_by_hours_by_project'){
								var chart = new CanvasJS.Chart("chart-container", {
									animationEnabled: true,
								title:{
									text: "Budgeted vs Actual Hours by Project",
									fontWeight: "normal",
								},	
								axisY: {
									title: "Medals"
								},
												
								toolTip: {
									shared: true
								},
								legend: {
									cursor:"pointer",
									itemclick: toggleDataSeries
								},
								data:JSON.parse(response),
								
									});
								chart.render();
						}else{
						  drawChart($('#report-heading').text(), JSON.parse(response),type);
						}
					}
				});

				$('div.data').show();
		  }
		});
		
		//,#report-time-project,#report-time-cases,#report-time-accounts
	  $(document).on('change','#report-time,#report-time-project,#report-time-cases,#report-time-accounts,#report-time-payroll-cost,#report-time-employee-project,#report-budget-by-hours-by-project', function(){
		  $('#between_dates').remove();
		if($(this).val()=='is_between'){
			var between_dates_html = '<span id="between_dates"><input style="margin-left: 10px;margin-right: 10px;" autocomplete="off" type="text" name="report_date_start" id="report_date_start" value="" title="" size="11" class="datepicker"><button id="report_date_start_trigger" type="button" onclick="return false;" class="btn btn-danger"><span class="suitepicon suitepicon-module-calendar" alt="Enter Date"></span></button><span>And</span><input style="margin-left: 10px;margin-right: 10px;" autocomplete="off" type="text" name="report_date_end" id="report_date_end" value="" title="" size="11" class="datepicker"><button id="report_date_end_trigger" type="button" onclick="return false;" class="btn btn-danger"><span class="suitepicon suitepicon-module-calendar" alt="Enter Date"></span></button></span>';
			$(this).after(between_dates_html);
		}else{
			$('#between_dates').remove();
		}
	});
});