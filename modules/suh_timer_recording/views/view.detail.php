<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

class suh_timer_recordingViewDetail extends ViewDetail
{
	public function __construct()
 	{
        parent::__construct();
	}
	
    function display()
    {
		parent::display();
		global $current_user, $sugar_config, $app_list_strings;
		$user_id = $current_user->id;
		$current_user_id = $current_user->id;
	 
		//////////////////
		
		$siteUrl = $sugar_config['site_url'];
		require_once('modules/TimesheetNinja/license/OutfittersLicense.php');
		$validate_license = OutfittersLicense::isValid('TimesheetNinja');
		if($validate_license !== true) {
		if(is_admin($current_user)) {
			SugarApplication::appendErrorMessage('Timesheet is no longer active due to the following reason: '.$validate_license.' Users will have limited to no access until the issue has been addressed.');
		}
		echo '<p class="error">Timesheet is no longer active Please renew your subscription or check your license configuration.Will be redirect in seconds.....</p>';
		header("refresh:2;url=$siteUrl");
		//functionality may be altered here in response to the key failing to validate
		}
		echo "</br>";
		
		
		//////////////////
		
		parent::display();
	}

}
