<?php

$manifest = array(
    'acceptable_sugar_versions' => array(
        'regex_matches' => array('.+'), // Any version
    ),
    'acceptable_sugar_flavors' => array(
        'CE','PRO', 'CORP', 'ENT', 'ULT'
    ),
    'name' => 'Analytic Reporting',
    'version' => '2.1.327',
    'description' => 'Analytic Reporting for SugarCRM',
    'author' => 'IT Sapiens',
    'published_date' => '2020/09/23',
    'type' => 'module',
    'icon' => '',
    'is_uninstallable' => 'true',
);

$installdefs = array(
    'id'=> 'AnalyticReporting',
    'copy' => array(
        //copy license directory to your module
        array(
            'from' => '<basepath>/license',
            'to' => 'modules/AnalyticReporting',
        ),
        array(
            'from' => '<basepath>/modules/AnalyticReporting',
            'to' => 'modules/AnalyticReporting',
        ),
        // Language file
        array(
            'from' => '<basepath>/custom/Extension/application/Ext/Language/en_us.analyticreporting.php',
            'to' => 'custom/Extension/application/Ext/Language/en_us.analyticreporting.php',
        ),
        // Enable backward compatability
        array(
            'from' => '<basepath>/custom/Extension/application/Ext/Include/AnalyticReportingBWC.php',
            'to' => 'custom/Extension/application/Ext/Include/AnalyticReportingBWC.php',
        ),
        // Scheduler reports
        array(
            'from' => '<basepath>/custom/Extension/modules/Schedulers/Ext/ScheduledTasks/sendScheduledDashboards.php',
            'to' => 'custom/Extension/modules/Schedulers/Ext/ScheduledTasks/sendScheduledDashboards.php',
        ),
        array(
            'from' => '<basepath>/custom/Extension/modules/Schedulers/Ext/ScheduledTasks/sendScheduledReports.php',
            'to' => 'custom/Extension/modules/Schedulers/Ext/ScheduledTasks/sendScheduledReports.php',
        ),
        array(
            'from' => '<basepath>/custom/Extension/modules/Schedulers/Ext/ScheduledTasks/executeARJobs.php',
            'to' => 'custom/Extension/modules/Schedulers/Ext/ScheduledTasks/executeARJobs.php',
        ),
        // Entrypoints
        array(
            'from' => '<basepath>/custom/Extension/application/Ext/EntryPointRegistry/widgetEntryPoint.php',
            'to' => 'custom/Extension/application/Ext/EntryPointRegistry/widgetEntryPoint.php',
        ),
        array(
            'from' => '<basepath>/custom/Extension/application/Ext/EntryPointRegistry/downloadTemplateEntryPoint.php',
            'to' => 'custom/Extension/application/Ext/EntryPointRegistry/downloadTemplateEntryPoint.php',
        ),
        // JSGrouping file
        array(
            'from' => '<basepath>/custom/Extension/application/Ext/JSGroupings/analyticreporting.php',
            'to' => 'custom/Extension/application/Ext/JSGroupings/analyticreporting.php',
        ),

        // Clients/base/ directory
        array(
            'from' => '<basepath>/custom/clients/base/api/AnalyticReportingApi.php',
            'to' => 'custom/clients/base/api/AnalyticReportingApi.php',
        ),
        array(
            'from' => '<basepath>/custom/clients/base/views/analyticreporting/analyticreporting.php',
            'to' => 'custom/clients/base/views/analyticreporting/analyticreporting.php',
        ),
        // CSS
        array(
            'from' => '<basepath>/custom/clients/base/views/analyticreporting/analyticreporting.css',
            'to' => 'custom/clients/base/views/analyticreporting/analyticreporting.css',
        ),
        // JS
        array(
            'from' => '<basepath>/custom/clients/base/views/analyticreporting/analyticreporting.js',
            'to' => 'custom/clients/base/views/analyticreporting/analyticreporting.js',
        ),
        // Templates
        array(
            'from' => '<basepath>/custom/clients/base/views/analyticreporting/analyticreporting.hbs',
            'to' => 'custom/clients/base/views/analyticreporting/analyticreporting.hbs',
        ),
        array(
            'from' => '<basepath>/custom/clients/base/views/analyticreporting/dashlet-config.hbs',
            'to' => 'custom/clients/base/views/analyticreporting/dashlet-config.hbs',
        ),
        // Modules/AnalyticReporting directory
        array (
            'from' => '<basepath>/modules/AnalyticReporting/AnalyticReporting.php',
            'to' => 'modules/AnalyticReporting/AnalyticReporting.php',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/css/AnalyticReporting.css',
            'to' => 'modules/AnalyticReporting/assets/css/AnalyticReporting.css',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/css/jquery-ui/redmond/images/animated-overlay.gif',
            'to' => 'modules/AnalyticReporting/assets/css/jquery-ui/redmond/images/animated-overlay.gif',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/css/jquery-ui/redmond/images/ui-bg_flat_0_aaaaaa_40x100.png',
            'to' => 'modules/AnalyticReporting/assets/css/jquery-ui/redmond/images/ui-bg_flat_0_aaaaaa_40x100.png',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/css/jquery-ui/redmond/images/ui-bg_flat_55_fbec88_40x100.png',
            'to' => 'modules/AnalyticReporting/assets/css/jquery-ui/redmond/images/ui-bg_flat_55_fbec88_40x100.png',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/css/jquery-ui/redmond/images/ui-bg_glass_75_d0e5f5_1x400.png',
            'to' => 'modules/AnalyticReporting/assets/css/jquery-ui/redmond/images/ui-bg_glass_75_d0e5f5_1x400.png',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/css/jquery-ui/redmond/images/ui-bg_glass_85_dfeffc_1x400.png',
            'to' => 'modules/AnalyticReporting/assets/css/jquery-ui/redmond/images/ui-bg_glass_85_dfeffc_1x400.png',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/css/jquery-ui/redmond/images/ui-bg_glass_95_fef1ec_1x400.png',
            'to' => 'modules/AnalyticReporting/assets/css/jquery-ui/redmond/images/ui-bg_glass_95_fef1ec_1x400.png',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/css/jquery-ui/redmond/images/ui-bg_gloss-wave_55_5c9ccc_500x100.png',
            'to' => 'modules/AnalyticReporting/assets/css/jquery-ui/redmond/images/ui-bg_gloss-wave_55_5c9ccc_500x100.png',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/css/jquery-ui/redmond/images/ui-bg_inset-hard_100_f5f8f9_1x100.png',
            'to' => 'modules/AnalyticReporting/assets/css/jquery-ui/redmond/images/ui-bg_inset-hard_100_f5f8f9_1x100.png',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/css/jquery-ui/redmond/images/ui-bg_inset-hard_100_fcfdfd_1x100.png',
            'to' => 'modules/AnalyticReporting/assets/css/jquery-ui/redmond/images/ui-bg_inset-hard_100_fcfdfd_1x100.png',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/css/jquery-ui/redmond/images/ui-icons_217bc0_256x240.png',
            'to' => 'modules/AnalyticReporting/assets/css/jquery-ui/redmond/images/ui-icons_217bc0_256x240.png',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/css/jquery-ui/redmond/images/ui-icons_2e83ff_256x240.png',
            'to' => 'modules/AnalyticReporting/assets/css/jquery-ui/redmond/images/ui-icons_2e83ff_256x240.png',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/css/jquery-ui/redmond/images/ui-icons_469bdd_256x240.png',
            'to' => 'modules/AnalyticReporting/assets/css/jquery-ui/redmond/images/ui-icons_469bdd_256x240.png',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/css/jquery-ui/redmond/images/ui-icons_6da8d5_256x240.png',
            'to' => 'modules/AnalyticReporting/assets/css/jquery-ui/redmond/images/ui-icons_6da8d5_256x240.png',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/css/jquery-ui/redmond/images/ui-icons_cd0a0a_256x240.png',
            'to' => 'modules/AnalyticReporting/assets/css/jquery-ui/redmond/images/ui-icons_cd0a0a_256x240.png',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/css/jquery-ui/redmond/images/ui-icons_d8e7f3_256x240.png',
            'to' => 'modules/AnalyticReporting/assets/css/jquery-ui/redmond/images/ui-icons_d8e7f3_256x240.png',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/css/jquery-ui/redmond/images/ui-icons_f9bd01_256x240.png',
            'to' => 'modules/AnalyticReporting/assets/css/jquery-ui/redmond/images/ui-icons_f9bd01_256x240.png',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/css/jquery-ui/redmond/jquery-ui.css',
            'to' => 'modules/AnalyticReporting/assets/css/jquery-ui/redmond/jquery-ui.css',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/css/jquery.select2.css',
            'to' => 'modules/AnalyticReporting/assets/css/jquery.select2.css',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/css/jstree/32px.png',
            'to' => 'modules/AnalyticReporting/assets/css/jstree/32px.png',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/css/jstree/40px.png',
            'to' => 'modules/AnalyticReporting/assets/css/jstree/40px.png',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/css/jstree/style.min.css',
            'to' => 'modules/AnalyticReporting/assets/css/jstree/style.min.css',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/css/jstree/throbber.gif',
            'to' => 'modules/AnalyticReporting/assets/css/jstree/throbber.gif',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/css/multiselect/jquery.multiselect.css',
            'to' => 'modules/AnalyticReporting/assets/css/multiselect/jquery.multiselect.css',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/css/pace.css',
            'to' => 'modules/AnalyticReporting/assets/css/pace.css',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/css/ReportBuilder.css',
            'to' => 'modules/AnalyticReporting/assets/css/ReportBuilder.css',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/css/select2-spinner.gif',
            'to' => 'modules/AnalyticReporting/assets/css/select2-spinner.gif',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/css/select2.png',
            'to' => 'modules/AnalyticReporting/assets/css/select2.png',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/css/select2x2.png',
            'to' => 'modules/AnalyticReporting/assets/css/select2x2.png',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/css/slickgrid/controls/slick.columnpicker.css',
            'to' => 'modules/AnalyticReporting/assets/css/slickgrid/controls/slick.columnpicker.css',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/css/slickgrid/controls/slick.pager.css',
            'to' => 'modules/AnalyticReporting/assets/css/slickgrid/controls/slick.pager.css',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/css/slickgrid/default-theme.css',
            'to' => 'modules/AnalyticReporting/assets/css/slickgrid/default-theme.css',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/css/slickgrid/slick.grid.css',
            'to' => 'modules/AnalyticReporting/assets/css/slickgrid/slick.grid.css',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/css/spectrum.css',
            'to' => 'modules/AnalyticReporting/assets/css/spectrum.css',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/img/arrow_down.png',
            'to' => 'modules/AnalyticReporting/assets/img/arrow_down.png',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/img/arrow_right.png',
            'to' => 'modules/AnalyticReporting/assets/img/arrow_right.png',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/img/arrow_up.png',
            'to' => 'modules/AnalyticReporting/assets/img/arrow_up.png',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/img/cross.png',
            'to' => 'modules/AnalyticReporting/assets/img/cross.png',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/img/cross_g.png',
            'to' => 'modules/AnalyticReporting/assets/img/cross_g.png',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/img/cross_grey.png',
            'to' => 'modules/AnalyticReporting/assets/img/cross_grey.png',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/img/excel_icon.png',
            'to' => 'modules/AnalyticReporting/assets/img/excel_icon.png',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/img/lookup.png',
            'to' => 'modules/AnalyticReporting/assets/img/lookup.png',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/img/pdf_icon.png',
            'to' => 'modules/AnalyticReporting/assets/img/pdf_icon.png',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/img/slickgrid/actions.gif',
            'to' => 'modules/AnalyticReporting/assets/img/slickgrid/actions.gif',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/img/slickgrid/ajax-loader-small.gif',
            'to' => 'modules/AnalyticReporting/assets/img/slickgrid/ajax-loader-small.gif',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/img/slickgrid/arrow_redo.png',
            'to' => 'modules/AnalyticReporting/assets/img/slickgrid/arrow_redo.png',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/img/slickgrid/arrow_right_peppermint.png',
            'to' => 'modules/AnalyticReporting/assets/img/slickgrid/arrow_right_peppermint.png',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/img/slickgrid/arrow_right_spearmint.png',
            'to' => 'modules/AnalyticReporting/assets/img/slickgrid/arrow_right_spearmint.png',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/img/slickgrid/arrow_undo.png',
            'to' => 'modules/AnalyticReporting/assets/img/slickgrid/arrow_undo.png',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/img/slickgrid/bullet_blue.png',
            'to' => 'modules/AnalyticReporting/assets/img/slickgrid/bullet_blue.png',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/img/slickgrid/bullet_star.png',
            'to' => 'modules/AnalyticReporting/assets/img/slickgrid/bullet_star.png',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/img/slickgrid/bullet_toggle_minus.png',
            'to' => 'modules/AnalyticReporting/assets/img/slickgrid/bullet_toggle_minus.png',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/img/slickgrid/bullet_toggle_plus.png',
            'to' => 'modules/AnalyticReporting/assets/img/slickgrid/bullet_toggle_plus.png',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/img/slickgrid/calendar.gif',
            'to' => 'modules/AnalyticReporting/assets/img/slickgrid/calendar.gif',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/img/slickgrid/collapse.gif',
            'to' => 'modules/AnalyticReporting/assets/img/slickgrid/collapse.gif',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/img/slickgrid/comment_yellow.gif',
            'to' => 'modules/AnalyticReporting/assets/img/slickgrid/comment_yellow.gif',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/img/slickgrid/down.gif',
            'to' => 'modules/AnalyticReporting/assets/img/slickgrid/down.gif',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/img/slickgrid/drag-handle.png',
            'to' => 'modules/AnalyticReporting/assets/img/slickgrid/drag-handle.png',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/img/slickgrid/editor-helper-bg.gif',
            'to' => 'modules/AnalyticReporting/assets/img/slickgrid/editor-helper-bg.gif',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/img/slickgrid/expand.gif',
            'to' => 'modules/AnalyticReporting/assets/img/slickgrid/expand.gif',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/img/slickgrid/header-bg.gif',
            'to' => 'modules/AnalyticReporting/assets/img/slickgrid/header-bg.gif',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/img/slickgrid/header-columns-bg.gif',
            'to' => 'modules/AnalyticReporting/assets/img/slickgrid/header-columns-bg.gif',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/img/slickgrid/header-columns-over-bg.gif',
            'to' => 'modules/AnalyticReporting/assets/img/slickgrid/header-columns-over-bg.gif',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/img/slickgrid/help.png',
            'to' => 'modules/AnalyticReporting/assets/img/slickgrid/help.png',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/img/slickgrid/info.gif',
            'to' => 'modules/AnalyticReporting/assets/img/slickgrid/info.gif',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/img/slickgrid/listview.gif',
            'to' => 'modules/AnalyticReporting/assets/img/slickgrid/listview.gif',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/img/slickgrid/pencil.gif',
            'to' => 'modules/AnalyticReporting/assets/img/slickgrid/pencil.gif',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/img/slickgrid/row-over-bg.gif',
            'to' => 'modules/AnalyticReporting/assets/img/slickgrid/row-over-bg.gif',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/img/slickgrid/sort-asc.gif',
            'to' => 'modules/AnalyticReporting/assets/img/slickgrid/sort-asc.gif',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/img/slickgrid/sort-asc.png',
            'to' => 'modules/AnalyticReporting/assets/img/slickgrid/sort-asc.png',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/img/slickgrid/sort-desc.gif',
            'to' => 'modules/AnalyticReporting/assets/img/slickgrid/sort-desc.gif',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/img/slickgrid/sort-desc.png',
            'to' => 'modules/AnalyticReporting/assets/img/slickgrid/sort-desc.png',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/img/slickgrid/stripes.png',
            'to' => 'modules/AnalyticReporting/assets/img/slickgrid/stripes.png',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/img/slickgrid/tag_red.png',
            'to' => 'modules/AnalyticReporting/assets/img/slickgrid/tag_red.png',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/img/slickgrid/tick.png',
            'to' => 'modules/AnalyticReporting/assets/img/slickgrid/tick.png',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/img/slickgrid/user_identity.gif',
            'to' => 'modules/AnalyticReporting/assets/img/slickgrid/user_identity.gif',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/img/slickgrid/user_identity_plus.gif',
            'to' => 'modules/AnalyticReporting/assets/img/slickgrid/user_identity_plus.gif',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/js/AnalyticReporting.min.js',
            'to' => 'modules/AnalyticReporting/assets/js/AnalyticReporting.min.js',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/js/builder.min.js',
            'to' => 'modules/AnalyticReporting/assets/js/builder.min.js',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/js/jquery/jquery-1.8.3.min.js',
            'to' => 'modules/AnalyticReporting/assets/js/jquery/jquery-1.8.3.min.js',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/js/jquery/jquery.event.drag-2.2.js',
            'to' => 'modules/AnalyticReporting/assets/js/jquery/jquery.event.drag-2.2.js',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/js/jquery/jquery.select2.min.js',
            'to' => 'modules/AnalyticReporting/assets/js/jquery/jquery.select2.min.js',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/js/jquery-ui/jquery-ui.min.js',
            'to' => 'modules/AnalyticReporting/assets/js/jquery-ui/jquery-ui.min.js',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/js/jquery.doubleScroll.js',
            'to' => 'modules/AnalyticReporting/assets/js/jquery.doubleScroll.js',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/js/jstree/jstree.min.js',
            'to' => 'modules/AnalyticReporting/assets/js/jstree/jstree.min.js',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/js/jstree/jstreegrid.min.js',
            'to' => 'modules/AnalyticReporting/assets/js/jstree/jstreegrid.min.js',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/js/legacy/startwith.js',
            'to' => 'modules/AnalyticReporting/assets/js/legacy/startwith.js',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/js/main.min.js',
            'to' => 'modules/AnalyticReporting/assets/js/main.min.js',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/js/moment.min.js',
            'to' => 'modules/AnalyticReporting/assets/js/moment.min.js',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/js/multiselect/jquery.multiselect.js',
            'to' => 'modules/AnalyticReporting/assets/js/multiselect/jquery.multiselect.js',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/js/pace.js',
            'to' => 'modules/AnalyticReporting/assets/js/pace.js',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/js/processes.min.js',
            'to' => 'modules/AnalyticReporting/assets/js/processes.min.js',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/js/ractive/ractive-decorators-select2.min.js',
            'to' => 'modules/AnalyticReporting/assets/js/ractive/ractive-decorators-select2.min.js',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/js/ractive/Ractive-decorators-sortable.js',
            'to' => 'modules/AnalyticReporting/assets/js/ractive/Ractive-decorators-sortable.js',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/js/ractive/Ractive.min.js',
            'to' => 'modules/AnalyticReporting/assets/js/ractive/Ractive.min.js',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/js/scheduler.min.js',
            'to' => 'modules/AnalyticReporting/assets/js/scheduler.min.js',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/js/templateSettings.min.js',
            'to' => 'modules/AnalyticReporting/assets/js/templateSettings.min.js',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/js/licenseUsersTable.min.js',
            'to' => 'modules/AnalyticReporting/assets/js/licenseUsersTable.min.js',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/js/slickgrid/controls/slick.columnpicker.js',
            'to' => 'modules/AnalyticReporting/assets/js/slickgrid/controls/slick.columnpicker.js',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/js/slickgrid/controls/slick.pager.js',
            'to' => 'modules/AnalyticReporting/assets/js/slickgrid/controls/slick.pager.js',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/js/slickgrid/plugins/slick.cellrangedecorator.js',
            'to' => 'modules/AnalyticReporting/assets/js/slickgrid/plugins/slick.cellrangedecorator.js',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/js/slickgrid/plugins/slick.cellrangeselector.js',
            'to' => 'modules/AnalyticReporting/assets/js/slickgrid/plugins/slick.cellrangeselector.js',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/js/slickgrid/plugins/slick.cellselectionmodel.js',
            'to' => 'modules/AnalyticReporting/assets/js/slickgrid/plugins/slick.cellselectionmodel.js',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/js/slickgrid/slick.core.js',
            'to' => 'modules/AnalyticReporting/assets/js/slickgrid/slick.core.js',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/js/slickgrid/slick.dataview.js',
            'to' => 'modules/AnalyticReporting/assets/js/slickgrid/slick.dataview.js',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/js/slickgrid/slick.editors.js',
            'to' => 'modules/AnalyticReporting/assets/js/slickgrid/slick.editors.js',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/js/slickgrid/slick.formatters.js',
            'to' => 'modules/AnalyticReporting/assets/js/slickgrid/slick.formatters.js',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/js/slickgrid/slick.grid.js',
            'to' => 'modules/AnalyticReporting/assets/js/slickgrid/slick.grid.js',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/js/slickgrid/slick.groupitemmetadataprovider.js',
            'to' => 'modules/AnalyticReporting/assets/js/slickgrid/slick.groupitemmetadataprovider.js',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/js/spectrum.js',
            'to' => 'modules/AnalyticReporting/assets/js/spectrum.js',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/assets/js/underscore.js',
            'to' => 'modules/AnalyticReporting/assets/js/underscore.js',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/config/common/commonDefault.php',
            'to' => 'modules/AnalyticReporting/config/common/commonDefault.php',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/config/definitions/fieldsDefault.php',
            'to' => 'modules/AnalyticReporting/config/definitions/fieldsDefault.php',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/config/definitions/modulesDefault.php',
            'to' => 'modules/AnalyticReporting/config/definitions/modulesDefault.php',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/config/hooks/addAuditTableDefault.php',
            'to' => 'modules/AnalyticReporting/config/hooks/addAuditTableDefault.php',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/config/hooks/addCustomTableDefault.php',
            'to' => 'modules/AnalyticReporting/config/hooks/addCustomTableDefault.php',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/config/hooks/reportModuleColumnName.php',
            'to' => 'modules/AnalyticReporting/config/hooks/reportModuleColumnName.php',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/config/platform/sugarDefault.php',
            'to' => 'modules/AnalyticReporting/config/platform/sugarDefault.php',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/config/processes/processesDefault.php',
            'to' => 'modules/AnalyticReporting/config/processes/processesDefault.php',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/config/reportBuilder/commonDefault.php',
            'to' => 'modules/AnalyticReporting/config/reportBuilder/commonDefault.php',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/config/reportBuilder/manyToManyDefault.php',
            'to' => 'modules/AnalyticReporting/config/reportBuilder/manyToManyDefault.php',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/controller.php',
            'to' => 'modules/AnalyticReporting/controller.php',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/controllers/ARController.php',
            'to' => 'modules/AnalyticReporting/controllers/ARController.php',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/controllers/ARDashboardController.php',
            'to' => 'modules/AnalyticReporting/controllers/ARDashboardController.php',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/controllers/ARJobsController.php',
            'to' => 'modules/AnalyticReporting/controllers/ARJobsController.php',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/controllers/ARReportBuilderController.php',
            'to' => 'modules/AnalyticReporting/controllers/ARReportBuilderController.php',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/controllers/ARReportController.php',
            'to' => 'modules/AnalyticReporting/controllers/ARReportController.php',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/controllers/ARReportTreeController.php',
            'to' => 'modules/AnalyticReporting/controllers/ARReportTreeController.php',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/controllers/ARSettingsController.php',
            'to' => 'modules/AnalyticReporting/controllers/ARSettingsController.php',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/controllers/ARConfigController.php',
            'to' => 'modules/AnalyticReporting/controllers/ARConfigController.php',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/controllers/libs.php',
            'to' => 'modules/AnalyticReporting/controllers/libs.php',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/Dashlets/WidgetDashlet/WidgetDashlet.en_us.lang.php',
            'to' => 'modules/AnalyticReporting/Dashlets/WidgetDashlet/WidgetDashlet.en_us.lang.php',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/Dashlets/WidgetDashlet/WidgetDashlet.meta.php',
            'to' => 'modules/AnalyticReporting/Dashlets/WidgetDashlet/WidgetDashlet.meta.php',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/Dashlets/WidgetDashlet/WidgetDashlet.php',
            'to' => 'modules/AnalyticReporting/Dashlets/WidgetDashlet/WidgetDashlet.php',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/Dashlets/WidgetDashlet/WidgetDashlet.tpl',
            'to' => 'modules/AnalyticReporting/Dashlets/WidgetDashlet/WidgetDashlet.tpl',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/Dashlets/WidgetDashlet/WidgetDashletConfigure.tpl',
            'to' => 'modules/AnalyticReporting/Dashlets/WidgetDashlet/WidgetDashletConfigure.tpl',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/entryPoints/widget.php',
            'to' => 'modules/AnalyticReporting/entryPoints/widget.php',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/entryPoints/downloadTemplate.php',
            'to' => 'modules/AnalyticReporting/entryPoints/downloadTemplate.php',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/hooks/ITSapiensReportModuleHooks.php',
            'to' => 'modules/AnalyticReporting/hooks/ITSapiensReportModuleHooks.php',
        ),/*
        array (
            'from' => '<basepath>/modules/AnalyticReporting/key.php',
            'to' => 'modules/AnalyticReporting/key.php',
        ),*/
        array (
            'from' => '<basepath>/modules/AnalyticReporting/language/de_DE.lang.php',
            'to' => 'modules/AnalyticReporting/language/de_DE.lang.php',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/language/en_us.lang.php',
            'to' => 'modules/AnalyticReporting/language/en_us.lang.php',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/language/es_ES.lang.php',
            'to' => 'modules/AnalyticReporting/language/es_ES.lang.php',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/language/fr_FR.lang.php',
            'to' => 'modules/AnalyticReporting/language/fr_FR.lang.php',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/language/ja_JP.lang.php',
            'to' => 'modules/AnalyticReporting/language/ja_JP.lang.php',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/profiles/FunctionWrapper.php',
            'to' => 'modules/AnalyticReporting/profiles/FunctionWrapper.php',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/profiles/PreInstallBasic.php',
            'to' => 'modules/AnalyticReporting/profiles/PreInstallBasic.php',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/profiles/PreInstall.php',
            'to' => 'modules/AnalyticReporting/profiles/PreInstall.php',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/templates/main.tpl',
            'to' => 'modules/AnalyticReporting/templates/main.tpl',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/templates/report.tpl',
            'to' => 'modules/AnalyticReporting/templates/report.tpl',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/templates/ReportBuilder/main.tpl',
            'to' => 'modules/AnalyticReporting/templates/ReportBuilder/main.tpl',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/templates/ReportBuilder/settings.tpl',
            'to' => 'modules/AnalyticReporting/templates/ReportBuilder/settings.tpl',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/templates/ReportBuilder/simple.tpl',
            'to' => 'modules/AnalyticReporting/templates/ReportBuilder/simple.tpl',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/templates/ReportBuilder/templates.tpl',
            'to' => 'modules/AnalyticReporting/templates/ReportBuilder/templates.tpl',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/templates/settings/format.tpl',
            'to' => 'modules/AnalyticReporting/templates/settings/format.tpl',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/templates/settings/license.tpl',
            'to' => 'modules/AnalyticReporting/templates/settings/license.tpl',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/templates/settings/logs.tpl',
            'to' => 'modules/AnalyticReporting/templates/settings/logs.tpl',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/templates/settings/permissions.tpl',
            'to' => 'modules/AnalyticReporting/templates/settings/permissions.tpl',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/templates/settings/processes.tpl',
            'to' => 'modules/AnalyticReporting/templates/settings/processes.tpl',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/templates/settings/scheduler.tpl',
            'to' => 'modules/AnalyticReporting/templates/settings/scheduler.tpl',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/templates/settings/dashboardScheduler.tpl',
            'to' => 'modules/AnalyticReporting/templates/settings/dashboardScheduler.tpl',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/templates/elements/exportButtonsBottom.tpl',
            'to' => 'modules/AnalyticReporting/templates/elements/exportButtonsBottom.tpl',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/templates/elements/reportImportNExport.tpl',
            'to' => 'modules/AnalyticReporting/templates/elements/reportImportNExport.tpl',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/templates/elements/massSchedule.tpl',
            'to' => 'modules/AnalyticReporting/templates/elements/massSchedule.tpl',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/templates/elements/proLink.tpl',
            'to' => 'modules/AnalyticReporting/templates/elements/proLink.tpl',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/templates/settings/states.tpl',
            'to' => 'modules/AnalyticReporting/templates/settings/states.tpl',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/templates/settings/templates.tpl',
            'to' => 'modules/AnalyticReporting/templates/settings/templates.tpl',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/templates/settings.tpl',
            'to' => 'modules/AnalyticReporting/templates/settings.tpl',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/templates/templates.tpl',
            'to' => 'modules/AnalyticReporting/templates/templates.tpl',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/utils/ITSapiensDateTimeUtils.php',
            'to' => 'modules/AnalyticReporting/utils/ITSapiensDateTimeUtils.php',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/vardefs.php',
            'to' => 'modules/AnalyticReporting/vardefs.php',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/views/view.license.php',
            'to' => 'modules/AnalyticReporting/views/view.license.php',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/views/view.list.php',
            'to' => 'modules/AnalyticReporting/views/view.list.php',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/views/view.logs.php',
            'to' => 'modules/AnalyticReporting/views/view.logs.php',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/views/view.report.php',
            'to' => 'modules/AnalyticReporting/views/view.report.php',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/views/view.reportbuilder.php',
            'to' => 'modules/AnalyticReporting/views/view.reportbuilder.php',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/views/view.settings.php',
            'to' => 'modules/AnalyticReporting/views/view.settings.php',
        ),
        array (
            'from' => '<basepath>/modules/language/application/en_us.lang.php',
            'to' => 'modules/language/application/en_us.lang.php',
        ),
        array (
            'from' => '<basepath>/modules/language/modules/Schedulers/en_us.sendScheduledReports.php',
            'to' => 'modules/language/modules/Schedulers/en_us.sendScheduledReports.php',
        ),
        array (
            'from' => '<basepath>/modules/AnalyticReporting/Menu.php',
            'to' => 'modules/AnalyticReporting/Menu.php',
        ),
    ),
    // 'scheduledefs' => array(
    //  array(
    //      'from' => '<basepath>/modules/Schedulers/sendScheduledReports.php',
    //      'to_module' => 'Schedulers',
    //  ),
    // ),
    'language' => array(
        array(
            'from'=> '<basepath>/license_admin/language/en_us.AnalyticReporting.php',
            'to_module'=> 'Administration',
            'language'=>'en_us'
        ),
        array(
            'from' => '<basepath>/modules/language/application/en_us.lang.php',
            'to_module' => 'application',
            'language' => 'en_us',
        ),
        array(
            'from'=> '<basepath>/modules/language/modules/Schedulers/en_us.sendScheduledReports.php',
            'to_module'=> 'Schedulers',
            'language'=>'en_us'
        ),
    ),
    'administration' =>
        array(
            array(
            'from'=>'<basepath>/license_admin/menu/AnalyticReporting_admin.php',
            'to' => 'modules/Administration/AnalyticReporting_admin.php',
        ),
    ),
    'action_view_map' =>
        array(
            array(
            'from'=> '<basepath>/license_admin/actionviewmap/AnalyticReporting_actionviewmap.php',
            'to_module'=> 'AnalyticReporting',
        ),
    ),
    // Dummy module for menu
    // @TODO - Refactor as normal SugarBean to use ACL and other functionality
    'beans'=> array(
        array(
            'module' => 'AnalyticReporting',
            'class' => 'AnalyticReporting',
            'path' => 'modules/AnalyticReporting/AnalyticReporting.php',
            'tab' => true,
        ),
    ),
    'post_uninstall' => array(
        0 => '<basepath>/scripts/post_uninstall.php',
    ),
);
