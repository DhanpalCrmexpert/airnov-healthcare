<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

class sp_Timeoff_TimeCardViewDetail extends ViewDetail
{
	public function __construct() {
		 parent::__construct();
	}
	
    function display()
    {
		parent::display();
		global $current_user, $sugar_config, $app_list_strings;
	 	$user_id = $current_user->id;
	 	$current_user_id = $current_user->id;
		
		//////////////////
		
		$siteUrl = $sugar_config['site_url'];
		require_once('modules/TimesheetNinja/license/OutfittersLicense.php');
		$validate_license = OutfittersLicense::isValid('TimesheetNinja');
		if($validate_license !== true) {
		if(is_admin($current_user)) {
			SugarApplication::appendErrorMessage('Timesheet is no longer active due to the following reason: '.$validate_license.' Users will have limited to no access until the issue has been addressed.');
		}
		echo '<p class="error">Timesheet is no longer active Please renew your subscription or check your license configuration.Will be redirect in seconds.....</p>';
		header("refresh:2;url=$siteUrl");
		//functionality may be altered here in response to the key failing to validate
		}
		echo "</br>";
		
		
		//////////////////
		
		if(!empty($this->bean->id)){
			$timeSheetDates = $this->getTimesheetDates($this->bean->id);
			$week_start_date = $timeSheetDates['track_date_from'];
			$result['start'] = date("Y-m-d",strtotime($timeSheetDates['track_date_from']));
			$result['end'] = date("Y-m-d",strtotime($timeSheetDates['track_date_to']));
			$dates = $this->getWeekDates($result['start'], $result['end']);
		}
		$bean_id = $this->bean->id;
		
		
		$timecard_list = $this->getTimeCardList($result['start'], $result['end'], $user_id);
		//print_r($timecard_list);
		
		$html = '
		<script src="modules/sp_Timesheet/js/timesheet.js"></script>';
		

		$html .= '<br/><br/><br/>
		<div class "detail view  detail508 expanded">
		<table style="border-collapse: separate;  border-spacing: 0 1em;">';
		$html .= '<tr>';
		$html .= '<td>Timecard Type</td>';
		$sum_all_projects = array();
		for($k=0; $k<7; $k++){
			$html .= '<td>'.date("D m/d",strtotime($dates[$k])).'</td>';
			$html .= '<td></td>';
			$sum_all_projects[$dates[$k]] = 0;
		}
		$html .= '<td colspan="2"><strong>Total</strong></td>';
		//$html .= '<td colspan="2"><strong>Project Hours</strong></td>';
		$html .= "</tr>";
		$found = true;
		$per_project_sum = 0;
		for($i=0; $i<count($timecard_list['tracker']); $i++){
			$project_name = $timecard_list['tracker'][$i]['name'];
			$project_id = $projects[$i]['project_id'];
			$quoted_project_id = "'$project_id'";
			$html .= '<tr>';
			$html .= '<td style="width: 275px;"> <b>'.$project_name.'</b></td>';
			$row_project_sum = 0;
			for($k=0; $k<7; $k++){
				$field_name = 'project_'.$dates[$k];
				$class_name = 'class_'.$dates[$k];
				$quoted_date = $dates[$k];
				$quoted_date = "'$quoted_date'";
				$hours="hours".$k;
				$html .= '<td><strong>'.$timecard_list['tracker'][$i][$hours].'<strong></td>';
				$html .= '<td style="width: 30px;"></td>';
			
				$sum_all_projects[$dates[$k]] += $timecard_list['tracker'][$i][$hours];
				$row_project_sum = $row_project_sum + $timecard_list['tracker'][$i][$hours];
				
				$per_project_sum = $per_project_sum + $projects[$i]['project_time_sheet_tracker'][$field_name];
				$total_hours = $total_hours + $timecard_list['tracker'][$i][$hours];
			}
			$project_class = 'project_';
			$field_name = 'sumproject_';
			$html .= '<td><strong><div class="'.$project_class.' "name="'.$field_name.'" id="'.$field_name.'" >'.$row_project_sum.'</div> </strong> </td>';
			$html .= '<td style="width: 30px;"></td>';
			$additional_hours = 0.0;
			
			
			$per_project_sum = 0;
			$found = true;
			$html .= '<tr></tr><tr></tr>';
		}
		$temp_array = array();
		
	
		$html .= "<tr>
		<td style='width: 275px;'><strong>Total</strong></td>";
		for($k=0; $k<7; $k++){
				$field_name = 'sumallprojects_'.$dates[$k];
				$field_val = $sum_all_projects[$dates[$k]];
				$html .= '<td><strong><div class="per_day_hours" name="'.$field_name.'" id="'.$field_name.'">'.$field_val.'</div></strong></td>';
				$html .= '<td style="width: 30px;"></td>';
		}
		$html .= "
		<td style='width: 30px;'><strong><div id='total_hours' name='total_hours' >$total_hours</div></strong></td>
		</tr></table></form>";
		echo $html;
	}
	
	
	function getTimeCardList($start_date, $end_date, $user_id)
	{
		global $db, $current_user;
		$current_user_id = $user_id;
		$sql_get="SELECT * FROM   sp_timeoff_timecard  where track_date_from ='".$start_date."'  AND  track_date_to ='".$end_date."' AND assigned_user_id='".$user_id."' and deleted = '0'";
		$rstimecard= $db->query($sql_get);
		$timecard_list=array();
		$j=0;
		while($rowTimecard = $db->fetchByAssoc($rstimecard))
		{
	
		$sql_timecard_tracker="SELECT  * FROM  sp_timecard_tracker WHERE timecard_id='".$rowTimecard['id']."' AND  track_date0=  '".$rowTimecard['track_date_from']."'  AND  track_date6=  '".$rowTimecard['track_date_to']."'";
		$rs_timecard_tracker= $db->query($sql_timecard_tracker);
		$i=0;
		$timecard_list['timecard_id']=$rowTimecard['id'];
		
		$timecard_tracker=array();
		
		
		 while($row_timecard_tracker = $db->fetchByAssoc($rs_timecard_tracker))
		 {
				$timecard_tracker[$i]['name']=$row_timecard_tracker['name'];
				$timecard_tracker[$i]['tracker_id']=$row_timecard_tracker['id'];
				$timecard_tracker[$i]['position']=$row_timecard_tracker['position'];
				
				for($n=0;$n<7;$n++)
				{
				 	$track_date='track_date'.$n;
					$hours='hours'.$n;
					$timecard_tracker[$i][$track_date]=$row_timecard_tracker[$track_date];
					$timecard_tracker[$i][$hours]=$row_timecard_tracker[$hours];
				}	
				
				$i++;
		 }
		 $timecard_list['tracker']=$timecard_tracker;
		 $j++;
	}
		
		 return $timecard_list;
	
		
	}
	
	function getProjectsList($start_date, $end_date, $user_id, $selected_module, $related_module){
		global $db, $current_user;
		
		$current_user_id = $user_id;
		$selected_module_bean = BeanFactory::getBean($selected_module);
		$module_table_name = $selected_module_bean->table_name;
		
		$status_where = "";
		if($selected_module == "Project" || $selected_module == "ProjectTask"){
			$status_where = "and status not in('Completed', 'Closed', 'On_Hold')";
		}
		
		$i = 0;
		$project_list = array();
		if($selected_module == "Project"){
			// $getProjectQry = "Select p.id, p.name from project p where p.deleted = 0 and status not in('Completed', 'Closed', 'On_Hold') ORDER BY p.date_entered DESC";
			$getProjectQry = "Select DISTINCT p.id, p.name, p.date_entered from project p INNER JOIN project_task pt on pt.project_id = p.id where p.deleted = 0 and pt.deleted = 0 and p.status not in('Completed', 'Closed', 'On_Hold') and pt.assigned_user_id = '$current_user_id' ORDER BY p.date_entered DESC";
		}else if($selected_module == "Accounts"){
			$getProjectQry = "Select p.id, p.name from accounts p INNER JOIN cases c ON p.id = c.account_id where p.deleted = 0 and c.deleted = 0 and c.assigned_user_id = '$current_user_id' $status_where ORDER BY p.date_entered DESC";
		}else{
			$getProjectQry = "Select * from $module_table_name p where p.deleted = 0 and assigned_user_id = '$current_user_id' $status_where ORDER BY p.date_entered DESC";
		}
		$resProject= $db->query($getProjectQry);
		while($rowProjectId = $db->fetchByAssoc($resProject))
		{
			$project_list[$i]['project_id'] = $rowProjectId['id'];
			if(isset($rowProjectId['name'])){
				$project_list[$i]['project_name'] = $rowProjectId['name'];
			}
			else{
				$project_list[$i]['project_name'] = $rowProjectId['first_name']." ".$rowProjectId['last_name'];
			}
			$timeSheetResult = $this->getProjectTimeTracked($rowProjectId['id'], $start_date, $end_date, $user_id);
			$project_list[$i]['project_tasks_list'] = $this->getProjectTasksList($rowProjectId['id'], $selected_module, $related_module, $current_user_id);
			$project_list[$i]['project_time_sheet_tracker'] = $timeSheetResult['project_time_sheet_list'];
			$i++;
		}
		$custom_array = array();
		$timesheet_id_detail = $this->bean->id;
		$getCustomProjectTimeSheetQry = "Select * from sp_timesheet_tracker where deleted = 0 and track_date >= '$start_date' and track_date <= '$end_date' and user_id_c = '$user_id' and timesheet_id = '$timesheet_id_detail' ORDER BY track_date ASC";
		$resCustomProjectTimeSheet = $db->query($getCustomProjectTimeSheetQry);
		while($rowCustomProjectTimeSheetId = $db->fetchByAssoc($resCustomProjectTimeSheet))
		{
			$temp_array = array();
			$project_task_id = $rowCustomProjectTimeSheetId['projecttask_id_c'];
			// if(!empty($rowCustomProjectTimeSheetId['related_module']) && $rowCustomProjectTimeSheetId['related_module']!=='Cases' && $rowCustomProjectTimeSheetId['related_module']!=='ProjectTasks'){ //to capture case and project_task only from timer
			if(!empty($rowCustomProjectTimeSheetId['related_module'])){
				$temp_array = array(
					'hours' => $rowCustomProjectTimeSheetId['hours'],
					'track_date' => $rowCustomProjectTimeSheetId['track_date'],
					'related_module' => $rowCustomProjectTimeSheetId['related_module'],
					'related_module_record' => $project_task_id,
				);
				$custom_array[$project_task_id."_".$rowCustomProjectTimeSheetId['track_date']] = $temp_array;
			}
		}
		return array('project_list' => $project_list, 'custom_array' => $custom_array);
	}
	
	
	
	
	public function validateStatus($related_module_record, $related_module){
		if($related_module == "Project" || $related_module == "ProjectTask")
		{
			if($related_module_record->status == "Completed" || $related_module_record->status == "Closed" || $related_module_record->status == "On_Hold")
			{
				return false;
			}
		}
		return true;
	}
	
	function getDates($date){
	  $week = date("W",strtotime($date));
	  $year = date("Y",strtotime($date));
	  $dto = new DateTime();
	  $result['start'] = $dto->setISODate($year, $week, 1)->format('Y-m-d');
	  $result['end'] = $dto->setISODate($year, $week, 7)->format('Y-m-d');
	  return $result;
	}
	
	public function getWeekDates($start_date, $end_date){
		$start_timestamp = strtotime($start_date);
		$end_timestamp   = strtotime($end_date);
		$dates = array();
		while ($start_timestamp <= $end_timestamp) {
			$dates[] = date('Y-m-d', $start_timestamp);
			$start_timestamp = strtotime('+1 day', $start_timestamp);
		}
		return $dates;
	}
	
	public function getNextPreviousDate($date, $how_much_days){
		$week_start_date = strtotime($date);
		$week_start_date = strtotime($how_much_days, $week_start_date);
		return $week_start_date = date('Y-m-d', $week_start_date);
	}
	
	public function getTimesheetDates($timesheet_id){
		global $db;
		
		$resultTimesheet = array();
		$query = "Select * from sp_timeoff_timecard t where t.deleted = 0 and t.id = '$timesheet_id'";
		
		$result= $db->query($query);
		while($row = $db->fetchByAssoc($result))
		{
			$resultTimesheet["track_date_from"] = $row["track_date_from"];	
			$resultTimesheet["track_date_to"] = $row["track_date_to"];	
		}
		return $resultTimesheet;
	}
}
