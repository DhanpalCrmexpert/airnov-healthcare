<?php
$viewdefs ['Opportunities'] = 
array (
  'EditView' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'javascript' => '{$PROBABILITY_SCRIPT}',
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL1' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_PANEL_ASSIGNMENT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'name',
          ),
          1 => 'account_name',
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'currency_id',
            'label' => 'LBL_CURRENCY',
          ),
          1 => 
          array (
            'name' => 'date_closed',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'amount',
          ),
          1 => 'opportunity_type',
        ),
        3 => 
        array (
          0 => 'lead_source',
          1 => 
          array (
            'name' => 'competitor_c',
            'studio' => 'visible',
            'label' => 'LBL_COMPETITOR',
          ),
        ),
        4 => 
        array (
          0 => 'sales_stage',
          1 => '',
        ),
        5 => 
        array (
          0 => 'probability',
          1 => 'campaign_name',
        ),
        6 => 
        array (
          0 => 'next_step',
          1 => '',
        ),
        7 => 
        array (
          0 => 'description',
        ),
      ),
      'lbl_editview_panel1' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'nda_c',
            'label' => 'LBL_NDA',
          ),
          1 => 
          array (
            'name' => 'npr_c',
            'label' => 'LBL_NPR',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'potential_launch_c',
            'label' => 'LBL_POTENTIAL_LAUNCH',
          ),
          1 => 
          array (
            'name' => 'item_description_c',
            'label' => 'LBL_ITEM_DESCRIPTION',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'quantity_c',
            'label' => 'LBL_QUANTITY_C',
          ),
          1 => 
          array (
            'name' => 'production_site_c',
            'label' => 'LBL_PRODUCTION_SITE',
          ),
        ),
        3 => 
        array (
          0 => '',
          1 => 
          array (
            'name' => 'success_rate_c',
            'label' => 'LBL_SUCCESS_RATE',
          ),
        ),
      ),
      'LBL_PANEL_ASSIGNMENT' => 
      array (
        0 => 
        array (
          0 => 'assigned_user_name',
        ),
      ),
    ),
  ),
);
;
?>
