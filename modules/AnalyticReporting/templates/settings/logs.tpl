{literal}
<style>
.log-table {border:1px solid #efefef;}
.log-table th {padding:5px;text-align:left;}
.log-table td {padding:2px; border:1px solid #efefef;background:#fff;}
.log-table tr:hover td {background:#efefef;}
form {display:inline;}
</style>
{/literal}

<div style="padding:5px">
    <form action="{$urls.settings}" method="post">
        <input type="hidden" name="call" value="enableLogs" />
        <label>
            <span style="display:inline-block">
                <input type="checkbox" {if $logging eq 1}checked="checked"{/if} onclick="this.form.submit();" />
                <input type="hidden" name="logging" value="{if $logging eq 1}0{else}1{/if}" />
            </span>
            Enable logging
        </label>
    </form>

    <span style="margin-left:30px;">Show</span>

    <form action="{$urls.settings}" method="post">
        <input type="hidden" name="call" value="updateLogLimit" />
        {html_options name="logLastLimit" options=$LOG_LIMIT_OPTIONS selected=$logLastLimit onchange="this.form.submit()"}
    </form>
    entries of
    <form action="{$urls.settings}" method="post">
        {html_options name="logClassName" options=$LOG_CLASS_NAME_OPTIONS selected=$logClassName}
        {html_options name="logMethodName" options=$LOG_METHOD_NAME_OPTIONS selected=$logMethodName}

        <input type="hidden" name="call" value="refreshLogs" />
        {html_options name="logLevel" options=$LOG_LEVEL_OPTIONS selected=$logLevel onchange="this.form.submit()"}
        log records
        <label>
            <input type="submit" name="refresh" value="Refresh" />
        </label>
    </form>


    <form action="{$urls.settings}" method="post">
        <input type="hidden" name="call" value="clearLogs" />
        <label>
            <input type="submit" name="clear" value="Clear logs" />
        </label>
    </form>
</div>

{if $data.status}
<table class="log-table" cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th>No.</th>
            <th>Created</th>
            <th>Level</th>
            <th>Memory</th>
            <th>Time</th>
            <th>Message</th>
            <th>Variables</th>
        </tr>
    </thead>
    <tbody>
        {foreach from=$data.entries item=entry name="entries"}
        <tr>
            <td>{$smarty.foreach.entries.iteration}</td>
            <td><span title="Server time {$entry.created}">{$entry.userCreated}</span></td>
            <td>
                {if $entry.level eq 'ERROR'}
                    <span style="color:red">{$entry.level}</span>
                {elseif $entry.level eq 'DEBUG'}
                    <span style="color:blue">{$entry.level}</span>
                {else}
                    {$entry.level}
                {/if}
            </td>
            <td>{$entry.memory_usage}</td>
            <td>{$entry.time_taken}</td>
            <td>{$entry.message}</td>
            <td>
                {if $entry.vars neq ''}
                    <pre>{$entry.vars}</pre>
                {/if}
            </td>
        </tr>
        {/foreach}
    </tbody>
</table>
{else}
    <i>{$data.message}</i>
{/if}