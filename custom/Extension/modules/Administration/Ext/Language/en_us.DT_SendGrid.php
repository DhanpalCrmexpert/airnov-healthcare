<?php

$mod_strings['LBL_DTSENDGRID'] = 'SendGrid Configuration';
$mod_strings['LBL_DTSENDGRID_LICENSE_TITLE'] = 'License Configuration';
$mod_strings['LBL_DTSENDGRID_TITLE'] = 'License Configuration';
$mod_strings['LBL_DTSENDGRID_LICENSE'] = 'Manage and configure the license for this add-on';

$mod_strings['LBL_DTSENDGRID_CONFIGURATION'] = 'SendGrid Settings';
$mod_strings['LBL_DTSENDGRID_MESSAGE'] = 'Configure SendGrid API Keys';

