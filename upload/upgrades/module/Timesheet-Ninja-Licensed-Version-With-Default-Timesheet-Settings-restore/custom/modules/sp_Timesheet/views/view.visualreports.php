<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

//ini_set('display_errors',1);
//error_reporting(E_ALL);
require_once('include/MVC/View/SugarView.php');

class ViewVisualreports extends SugarView
{
	private $reportType;
	public function display()
    {
		
		
		parent::display();
		$this->reportType = $_REQUEST['type'];
		if(isset($this->reportType) && !isset($_GET['time'])){
			echo 	'<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>';
			echo "<script src='custom/include/charts/charts.js?v=1.1'></script>";
			echo "<script src='custom/include/charts/exporting.js?v=1.1'></script>";
			echo "<script src='custom/include/charts/custom_charts.js?v=1.1'></script>";
			echo "<script src='custom/include/charts/custom_script.js?v=1.1'></script>";
		  echo '<script src="https://canvasjs.com/assets/script/canvasjs.min.js?v=1.1"></script>';

		}
		if(isset($_GET['time'])){
			if($this->reportType=='payroll_cost_by_employee' || $this->reportType=='time_by_employee_and_project' || $this->reportType=='budget_by_hours_by_project'){
				echo json_encode($this->getData($this->reportType, $_GET['time'],$_GET['start_date'],$_GET['end_date']),JSON_NUMERIC_CHECK);
			}else {
			echo json_encode($this->getData($this->reportType, $_GET['time'],$_GET['start_date'],$_GET['end_date']));
			}
			exit;
		}
		echo $this->getHtml();
	}
	private function getHtml(){
		$html = "<style>
					.graph-links-main fieldset{margin-top:-30px;}
					.graph-links-main ul{padding-left: 15px;font-size: 15px;}
					.graph-links-main li{margin-bottom: 5px;}
				</style>
				<div id='main' class='graph-links-main'>
					<fieldset>
						<legend><h1>Reports</h1></legend>
						<h3 style='margin-left: 15px;'>Productivity Reports</h3>
						<ul>
							<li><a href='./index.php?module=sp_Timesheet&action=visualreports&type=time_by_employee&initial_time=this_month'>Time By Employee</a></li>
							<li><a href='./index.php?module=sp_Timesheet&action=visualreports&type=time_by_project&initial_time=this_month'>Time on Projects</a></li>
							<li><a href='./index.php?module=sp_Timesheet&action=visualreports&type=time_by_cases&initial_time=this_month'>Time Spent on Support Cases</a></li>
							<li><a href='./index.php?module=sp_Timesheet&action=visualreports&type=time_by_account&initial_time=this_month'>Time Spent on Projects By Account</a></li>
							<li><a href='./index.php?module=sp_Timesheet&action=visualreports&type=time_by_day_by_employee'>Time by Day by Employee</a></li>
							</ul>
							<legend><h1>Financial Reports</h1></legend>
							<ul>
							<li><a href='./index.php?module=sp_Timesheet&action=visualreports&type=payroll_cost_by_employee&initial_time=this_month'>Payroll Cost by Employee</a></li>
							<li><a href='./index.php?module=sp_Timesheet&action=visualreports&type=time_by_employee_and_project&initial_time=this_month'>Time By Employee and Project</a></li>
							<li><a href='./index.php?module=sp_Timesheet&action=visualreports&type=budget_by_hours_by_project&initial_time=this_month'>Budgeted vs Actual Hours by Project</a></li>
						</ul>
					</fieldset>
				</div>";
				
		$display = (!isset($this->reportType)) ? 'none' : '';
		switch ($this->reportType) {
			case 'time_by_employee':
				$charHeader = "Time by Employee";
				$reportHeader = 'Productivity Reports';
				break;
			case 'time_by_project':
				$charHeader = "Time on Projects";
				$reportHeader = 'Productivity Reports';
				break;
			case 'time_by_cases':
				$charHeader = "Time spent on Support Cases (Monthly)";
				$reportHeader = 'Productivity Reports';
				break;
			case 'time_by_account':
				$charHeader = "Time Spent on Projects By Accounts";
				$reportHeader = 'Productivity Reports';
				break;
			case 'time_by_day_by_employee':
				$charHeader = "Time by Day by Employee";
				$reportHeader = 'Productivity Reports';
				break;
			case 'payroll_cost_by_employee':
			    $reportHeader = 'Financial Reports';
				$charHeader = "Payroll Cost by Employee";
				break;
			case 'time_by_employee_and_project':
			    $reportHeader = 'Financial Reports';
				$charHeader = "Time By Employee and Project";
				break;
			case 'budget_by_hours_by_project':
			   $reportHeader = 'Financial Reports';
				$charHeader = "Budgeted vs Actual Hours by Project";
				break;
			default:
				$charHeader = '';
				break;
		}
		$html.= "<div id='charts' class='charts-data' style='display:{$display}'>
					<fieldset>
					<center><h2 id='report-heading-financial'>".$reportHeader."</h2></center>
						<center><h3 id='report-heading'>{$charHeader}</h3></center>
						<div id='filters' class='filter-panel'>".$this->getFilter()."</div>
						<center>
						<div id='data' class='data' style='display:none;'>
							<div id='chart-container' class='chart-container'></div>
							<div id='result-container' class='result-container'></div>
						</div>
						</center>
					</fieldset>
				</div>
				";	
		return $html;
	}
private function getDayHtml($date, $user_id){
	global $db;
	$user = BeanFactory::getBean('Users', $user_id);
	$user_id = $user->id;
	$date_str = strtotime($date);

	$start = Date('Y-m-d', strtotime('monday this week', $date_str));
	$end = Date('Y-m-d', strtotime('sunday this week', $date_str));
	$sql = "select track_date date, sum(hours) hours from sp_timesheet_tracker 
			where user_id_c = '{$user_id}'
			and track_date BETWEEN '{$start}' AND '{$end}'
			group by track_date
			order by track_date";

	$week_hours = array();
	while (strtotime($start) <= strtotime($end)) {
		$week_hours[$start] = array('name' => Date('D', strtotime($start))."</br>".Date('M d', strtotime($start)), 'hours'=>0);
		$start = Date('Y-m-d', strtotime('+1 day', strtotime($start)));
	}

	$result = $db->query($sql, true);
	while ($row = $db->fetchByAssoc($result)) {
		$week_hours[$row['date']]['hours'] = $row['hours'];
	}

	$header_html = "<thead class='thead-light'>
						<tr><th width='20%'>User Name</th>";
	$body_html = "<tbody>
						<tr><td>{$user->full_name}</td>";
	$total_hours = 0.0;
	foreach($week_hours as $day){
		$header_html .= "<th width='11%'>{$day['name']}</th>";
		$body_html .= "<td>{$day['hours']}</td>";
		$total_hours += number_format($day['hours'], 2);
	}

	$header_html .= "<th width='11%'>Total</th></tr></thead>";
	$body_html .= "<td>{$total_hours}</td></tr></tbody>";

	$html = $header_html.$body_html;

	return $html;
}
	private function getFilter(){
		switch ($this->reportType) {
			case 'time_by_employee':
			$selected = '';
			if(isset($_REQUEST['initial_time'])){
				$selected = 'selected';
			}
			//	<option value=''>-None-</option>
				$filterHtml = "<label for='report-time'>Report Time : <label>
					   <select name='report-time' id='report-time'>
					   		<option value='this_month' ".$selected.">This Month</option>
					   		<option value='last_month'>Last Month</option>
							<option value='is_between'>Is Between</option>
					   		<option value='overall_time'>Overall Time</option>
					   </select>
					   ";
				break;	
			case 'time_by_project':
			$selected = '';
			if(isset($_REQUEST['initial_time'])){
				$selected = 'selected';
			}
				$filterHtml = "<label for='report-time'>Report Time : <label>
					   <select name='report-time-project' id='report-time-project'>
					   		<option value='this_month' ".$selected.">This Month</option>
					   		<option value='last_month'>Last Month</option>
							<option value='is_between'>Is Between</option>
							<option value='overall_time'>Overall Time</option>
					   </select>
					   ";
				break;
			case 'time_by_cases':
			$selected = '';
			if(isset($_REQUEST['initial_time'])){
				$selected = 'selected';
			}
				$filterHtml = "<label for='report-time'>Report Time : <label>
					   <select name='report-time-cases' id='report-time-cases'>
					   		<option value='this_month' ".$selected.">This Month</option>
					   		<option value='last_month'>Last Month</option>
							<option value='is_between'>Is Between</option>
							<option value='overall_time'>Overall Time</option>
					   </select>
					   ";
				break;
			case 'time_by_account':
			$selected = '';
			if(isset($_REQUEST['initial_time'])){
				$selected = 'selected';
			}
				$filterHtml = "<label for='report-time'>Report Time : <label>
					   <select name='report-time-accounts' id='report-time-accounts'>
					   		<option value='this_month' ".$selected.">This Month</option>
					   		<option value='last_month'>Last Month</option>
							<option value='is_between'>Is Between</option>
							<option value='overall_time'>Overall Time</option>
					   </select>
					   ";
				break;
			case 'time_by_day_by_employee':
			$filterHtml = '';
				/*$filterHtml = "<label for='report-time'>Report Time : <label>
					   <select name='report-time-time-by-day-by-employee' id='report-time-time-by-day-by-employee'>
					   		<option value=''>-None-</option>
					   		<option value='this_month'>This Month</option>
					   </select>
					   ";*/
					   global $current_user;
					$userBean = BeanFactory::getBean('Users');
					$users = $userBean->get_full_list();

					$user_options = "<option value='#'></option>";
					foreach ($users as $key => $value) {
						if($value->id === $current_user->id){
							$user_options .= "<option value='{$value->id}' selected>{$value->full_name}</option>";
						}
						else{
							$user_options .= "<option value='{$value->id}'>{$value->full_name}</option>";
						}
					}
					$today = Date('Y-m-d',strtotime('today'));
					$table_html = $this->getDayHtml($today,$current_user->id);

					$start = Date('Y-m-d', strtotime('monday this week', $date_str));
					$end = Date('Y-m-d', strtotime('sunday this week', $date_str));
					  $filterHtml = "<style>
					::-webkit-datetime-edit { padding: 1em; }
					::-webkit-datetime-edit-fields-wrapper { background: silver; }
					::-webkit-datetime-edit-text { color: red; padding: 0 0.3em; }
					::-webkit-inner-spin-button { display: none; }
					::-webkit-calendar-picker-indicator { background: #a5e8d6; }

					.data-table.table .thead-light th {
						color: #495057;
						background-color: #e9ecef;
						border-color: #000000;
					}
					.data-table.table th, .data-table.table td {
						text-align: left;
						padding: 0.75rem;
						vertical-align: top;
						border: 1px solid #000000 !important;
					}
					.date{
						border: 1px solid #a5e8d6;
						border-radius: 5px;
						min-height: 20px;
					}
				</style>
					  <div class='main-body-by-day' id='main-body-by-day'>
							<div class='filter-panel'>
								<fieldset>
								<label for='report-time'>Report Time : <label>
									<select name='user_list' id='user_list_by_day'>
										{$user_options}
									</select>
									<input type='date' class='date' style='margin-right:10px;' name='date' id='date_time_by_day' required value='{$today}' />
									<input type='button' name='search' style='padding: 0 8px 0 8px;height:29px;' id='search_time_by_day' value='Search'/>
								</fieldset>
							</div>
					<table class='data-table table' id='data-table'>
					</table>
						 </div>";
				break;
			case 'payroll_cost_by_employee':
			$selected = '';
			if(isset($_REQUEST['initial_time'])){
				$selected = 'selected';
			}
				$filterHtml = "<label for='report-time'>Report Time : <label>
					   <select name='report-time-payroll-cost' id='report-time-payroll-cost'>
					   		<option value='this_month' ".$selected.">This Month</option>
					   		<option value='last_month'>Last Month</option>
							<option value='is_between'>Is Between</option>
							<option value='overall_time'>Overall Time</option>
					   </select>
					   ";
				break;
			case 'time_by_employee_and_project':
			$selected = '';
			if(isset($_REQUEST['initial_time'])){
				$selected = 'selected';
			}
				$filterHtml = "<label for='report-time'>Report Time : <label>
					   <select name='report-time-employee-project' id='report-time-employee-project'>
					   		<option value='this_month' ".$selected.">This Month</option>
					   		<option value='last_month'>Last Month</option>
							<option value='is_between'>Is Between</option>
							<option value='overall_time'>Overall Time</option>
					   </select>
					   ";
				break;
			case 'budget_by_hours_by_project':
			$selected = '';
			if(isset($_REQUEST['initial_time'])){
				$selected = 'selected';
			}
				$filterHtml = "<label for='report-time'>Report Time : <label>
					   <select name='report-budget-by-hours-by-project' id='report-budget-by-hours-by-project'>
					   		<option value='this_month' ".$selected.">This Month</option>
					   		<option value='last_month'>Last Month</option>
							<option value='is_between'>Is Between</option>
							<option value='overall_time'>Overall Time</option>
					   </select>
					   ";
				break;
			default:
				$filterHtml = '';
				break;
		}
		
		return $filterHtml;
	}

	private function getData($type, $time,$start_date,$end_date){
		global $db;
		$data_to_return = array();

		switch ($type) {
			case 'time_by_employee':
				$where = "WHERE users.deleted=0 AND  users.status='Active' and sp_timesheet_tracker.deleted=0 ";
				if($time == 'this_month'){
					$date = Date('Y-m', strtotime('this month'));
					$where .= " AND DATE_FORMAT(sp_timesheet_tracker.track_date, '%Y-%m') = '{$date}'";
				}else if($time == 'last_month'){
					$date = Date('Y-m', strtotime('last month'));
					$where .= " AND DATE_FORMAT(sp_timesheet_tracker.track_date, '%Y-%m') = '{$date}'";
				}else if($time == 'is_between'){
					$date_start = Date('Y-m-d', strtotime($start_date));
					$date_end = Date('Y-m-d', strtotime($end_date));
					$where .= " AND sp_timesheet_tracker.track_date BETWEEN '{$date_start}' AND '{$date_end}'";
				}
				$sql = "SELECT 
						    CONCAT(COALESCE(users.first_name, ''),' ',COALESCE(users.last_name, '')) AS user_name,
						    SUM(sp_timesheet_tracker.hours) AS total_hours
						FROM
						    sp_timesheet_tracker
						    JOIN users ON sp_timesheet_tracker.user_id_c = users.id
						{$where}
						GROUP BY CONCAT(COALESCE(users.first_name, ''),' ',COALESCE(users.last_name, ''))";
				$sql_result = $db->query($sql, true);
				
				while ($row = $db->fetchByAssoc($sql_result)) {
					$data_to_return['categories'][] = $row['user_name'];
					$data_to_return['data'][] = floatval($row['total_hours']);
				}

				# code...
				break;

			case 'time_by_project':
				$where = "WHERE project.deleted=0 AND sp_timesheet_tracker.deleted=0 ";
				if($time == 'this_month'){
					$date = Date('Y-m', strtotime('this month'));
					$where .= " AND DATE_FORMAT(sp_timesheet_tracker.track_date, '%Y-%m') = '{$date}'";
				}else if($time == 'last_month'){
					$date = Date('Y-m', strtotime('last month'));
					$where .= " AND DATE_FORMAT(sp_timesheet_tracker.track_date, '%Y-%m') = '{$date}'";
				}else if($time == 'is_between'){
					$date_start = Date('Y-m-d', strtotime($start_date));
					$date_end = Date('Y-m-d', strtotime($end_date));
					$where .= " AND sp_timesheet_tracker.track_date BETWEEN '{$date_start}' AND '{$date_end}'";
				}
				$sql = "SELECT 
						    project.name AS name,
						    SUM(sp_timesheet_tracker.hours) AS total_hours
						FROM
						    sp_timesheet_tracker
						        JOIN
						    project ON sp_timesheet_tracker.project_id_c = project.id
						{$where}
						GROUP BY project.name";

				$sql_result = $db->query($sql, true);
				
				while ($row = $db->fetchByAssoc($sql_result)) {
					$data_to_return['categories'][] = $row['name'];
					$data_to_return['data'][] = floatval($row['total_hours']);
				}
				break;
			case 'time_by_cases':
				//$date_start = Date('Y-m', strtotime('-3 month', strtotime('now')));
				//$date_end = Date('Y-m', strtotime('+3 month', strtotime('now')));
				$where = "WHERE sp_timesheet_tracker.deleted=0 ";
				if($time == 'this_month'){
					$date = Date('Y-m', strtotime('this month'));
					$where .= " AND DATE_FORMAT(sp_timesheet_tracker.track_date, '%Y-%m') = '{$date}'";
				}else if($time == 'last_month'){
					$date = Date('Y-m', strtotime('last month'));
					$where .= " AND DATE_FORMAT(sp_timesheet_tracker.track_date, '%Y-%m') = '{$date}'";
				}else if($time == 'is_between'){
					$date_start = Date('Y-m-d', strtotime($start_date));
					$date_end = Date('Y-m-d', strtotime($end_date));
					$where .= " AND sp_timesheet_tracker.track_date BETWEEN '{$date_start}' AND '{$date_end}'";
				}
				//AND DATE_FORMAT(sp_timesheet_tracker.track_date, '%Y-%m') BETWEEN '{$date_start}' AND '{$date_end}'
				/*$sql = "SELECT 
						    DATE_FORMAT(sp_timesheet_tracker.track_date, '%Y-%m') AS month,
						    SUM(sp_timesheet_tracker.hours) AS total_hours
						FROM
						    sp_timesheet_tracker
						WHERE sp_timesheet_tracker.deleted = 0
						AND sp_timesheet_tracker.related_record_url like '%module=Cases%'
						{$where}
						GROUP BY month";*/
					/* $sql = "SELECT 
						    CONCAT(COALESCE(users.first_name, ''),' ',COALESCE(users.last_name, '')) AS user_name,
						    SUM(sp_timesheet_tracker.hours) AS total_hours
						FROM
						    sp_timesheet_tracker
						    JOIN users ON sp_timesheet_tracker.user_id_c = users.id
						{$where}
						AND sp_timesheet_tracker.related_record_url like '%module=Cases%'
						GROUP BY CONCAT(COALESCE(users.first_name, ''),' ',COALESCE(users.last_name, ''))";*/
						
						 $sql = "SELECT 
						    sp_timesheet_tracker.parent_id,
						    SUM(sp_timesheet_tracker.hours) AS total_hours
						FROM
						    sp_timesheet_tracker
						    JOIN users ON sp_timesheet_tracker.user_id_c = users.id
						{$where}
						AND sp_timesheet_tracker.related_record_url like '%module=Cases%'
						GROUP BY sp_timesheet_tracker.parent_id";
						
				$sql_result = $db->query($sql, true);
			  $data_to_return = array();
				while ($row = $db->fetchByAssoc($sql_result)) {
				//	$data_to_return['categories'][] = date('F-Y', strtotime($row['month']));
				//$data_to_return['categories'][] = $row['user_name'];
				if(!empty($row['parent_id'])){
					$caseBean = new aCase();
					$caseBean->retrieve($row['parent_id']);
					if($caseBean->id){
					$data_to_return['categories'][] = $caseBean->name;
					$data_to_return['data'][] = floatval($row['total_hours']);
					}
				}
				}
				/*echo '<pre>';
				print_r($data_to_return);
				die();*/
				break;
			case 'time_by_account':
			$where = "WHERE accounts.deleted=0  and sp_timesheet_tracker.deleted=0 and accounts_sp_timesheet_tracker_1_c.deleted=0";
			if($time == 'this_month'){
					$date = Date('Y-m', strtotime('this month'));
					$where .= " AND DATE_FORMAT(sp_timesheet_tracker.track_date, '%Y-%m') = '{$date}'";
				}else if($time == 'last_month'){
					$date = Date('Y-m', strtotime('last month'));
					$where .= " AND DATE_FORMAT(sp_timesheet_tracker.track_date, '%Y-%m') = '{$date}'";
				}else if($time == 'is_between'){
					$date_start = Date('Y-m-d', strtotime($start_date));
					$date_end = Date('Y-m-d', strtotime($end_date));
					$where .= " AND sp_timesheet_tracker.track_date BETWEEN '{$date_start}' AND '{$date_end}'";
				}
				//						Where total_hours_at > 0
				/*$sql = "SELECT 
						    name, total_hours_at as total_hours
						FROM
						    accounts
							{$where}
						and deleted = 0 ";*/
				/* $sql = "SELECT 
						    accounts.name AS name,
						    SUM(sp_timesheet_tracker.hours) AS total_hours
						FROM
						    sp_timesheet_tracker
						        INNER JOIN
						    accounts ON sp_timesheet_tracker.parent_id = accounts.id
						{$where}
						GROUP BY accounts.name";*/
				$sql = "SELECT 
						    accounts.name AS name,
						    SUM(sp_timesheet_tracker.hours) AS total_hours
						FROM
						  accounts inner join accounts_sp_timesheet_tracker_1_c
						  on accounts.id = accounts_sp_timesheet_tracker_1_c.accounts_sp_timesheet_tracker_1accounts_ida
						  inner join sp_timesheet_tracker
						  on accounts_sp_timesheet_tracker_1_c.accounts_sp_timesheet_tracker_1sp_timesheet_tracker_idb = sp_timesheet_tracker.id
						  {$where}
						GROUP BY accounts.name";
				$sql_result = $db->query($sql, true);
				
				while ($row = $db->fetchByAssoc($sql_result)) {
					$data_to_return['categories'][] = $row['name'];
					$data_to_return['data'][] = floatval($row['total_hours']);
				}

				break;
			case 'payroll_cost_by_employee':
				$where = "WHERE users.deleted=0 AND users.status='Active' AND  sp_timesheet_tracker.deleted=0 ";
				if($time == 'this_month'){
					$date = Date('Y-m', strtotime('this month'));
					$where .= " AND DATE_FORMAT(sp_timesheet_tracker.track_date, '%Y-%m') = '{$date}'";
				}else if($time == 'last_month'){
					$date = Date('Y-m', strtotime('last month'));
					$where .= " AND DATE_FORMAT(sp_timesheet_tracker.track_date, '%Y-%m') = '{$date}'";
				}else if($time == 'is_between'){
					$date_start = Date('Y-m-d', strtotime($start_date));
					$date_end = Date('Y-m-d', strtotime($end_date));
					$where .= " AND sp_timesheet_tracker.track_date BETWEEN '{$date_start}' AND '{$date_end}'";
				}
				 $sql = "SELECT 
						    CONCAT(COALESCE(users.first_name, ''),' ',COALESCE(users.last_name, '')) AS user_name,
						    SUM(sp_timesheet_tracker.hours) AS total_hours,SUM(sp_timesheet_tracker.hours) AS total_hours,max(users.hourly_rate) as hourly_rate
						FROM
						    sp_timesheet_tracker
						    JOIN users ON sp_timesheet_tracker.user_id_c = users.id
						{$where}
						GROUP BY CONCAT(COALESCE(users.first_name, ''),' ',COALESCE(users.last_name, ''))";
				$sql_result = $db->query($sql, true);
				
				while ($row = $db->fetchByAssoc($sql_result)) {
					 $total_amount = 0;
					 $total_amount = $row['total_hours']*$row['hourly_rate'];
					 $data_to_return[] = array("y" => $total_amount, "total_hours" => array($total_amount,$row['total_hours']),"label"=>$row['user_name']);
					 
				}
				# code...
				break;
				
				case 'time_by_employee_and_project':
				$where = " WHERE project.deleted=0 AND sp_timesheet_tracker.deleted=0 and users.deleted=0 AND  users.status='Active' ";
				if($time == 'this_month'){
					$date = Date('Y-m', strtotime('this month'));
					$where .= " AND DATE_FORMAT(sp_timesheet_tracker.track_date, '%Y-%m') = '{$date}'";
				}else if($time == 'last_month'){
					$date = Date('Y-m', strtotime('last month'));
					$where .= " AND DATE_FORMAT(sp_timesheet_tracker.track_date, '%Y-%m') = '{$date}'";
				}else if($time == 'is_between'){
					$date_start = Date('Y-m-d', strtotime($start_date));
					$date_end = Date('Y-m-d', strtotime($end_date));
					$where .= " AND sp_timesheet_tracker.track_date BETWEEN '{$date_start}' AND '{$date_end}'";
				}
				$sql = "SELECT 
						    project.id as project_id,project.name AS project_name,CONCAT(COALESCE(users.first_name, ''),' ',COALESCE(users.last_name, '')) AS user_name,users.id as user_id,
						    SUM(sp_timesheet_tracker.hours) AS total_hours
						FROM
						    sp_timesheet_tracker
						        JOIN
						    project ON sp_timesheet_tracker.project_id_c = project.id
							JOIN users on sp_timesheet_tracker.user_id_c = users.id
						{$where}
						GROUP BY project.id,sp_timesheet_tracker.user_id_c
						";
						
				$sql_result = $db->query($sql, true);
				$finalArray =  array();
				while ($row = $db->fetchByAssoc($sql_result)) {
				/*	$finalArray['hours'][] = array(
					  'project_id' => $row['project_id'],
					  'project_name' => $row['project_name'],
					  'user_id' => $row['user_id'],
					  'user_name' => $row['user_name'],
					  'total_hours' => $row['total_hours'],
					);*/
					$finalArray['hours'][$row['project_id']][$row['user_id']]['total_hours'] = $row['total_hours'];
					$finalArray['project_name'][$row['project_id']] = $row['project_name'];
					$finalArray['user_name'][$row['user_id']] = $row['user_name'];
				}
				$projectArr = $datapointsArr = array();
				foreach($finalArray['user_name'] as $userId=>$username){
					$datapointsArr = array();
					foreach($finalArray['project_name'] as $projectId=>$projectName){
						if(isset($finalArray['hours'][$projectId][$userId])){
							$datapointsArr[] = array(
							'y' => $finalArray['hours'][$projectId][$userId]['total_hours'],
							 'label' => $projectName,
							   
							);
					}else{
						$datapointsArr[] = array(
							'y' => '0.0',
							 'label' => $projectName,
							   
							);
					}
					}
						$projectArr[] = array(
								'type' => "column",
								'name' => $username,
								'legendText' => $username,
								'showInLegend' => true, 
								'dataPoints' => $datapointsArr,
						);
				}
				$data_to_return = $projectArr;
				//echo '<pre>';
			//	echo 'heyyyyyyyyyyyyyyy';
				//print_r($projectArr);
				# code...
				break;
				
				case 'budget_by_hours_by_project':
				$where = " WHERE project.deleted=0 AND sp_timesheet_tracker.deleted=0 and users.deleted=0 AND  users.status='Active' ";
				if($time == 'this_month'){
					$date = Date('Y-m', strtotime('this month'));
					$where .= " AND DATE_FORMAT(sp_timesheet_tracker.track_date, '%Y-%m') = '{$date}'";
				}else if($time == 'last_month'){
					$date = Date('Y-m', strtotime('last month'));
					$where .= " AND DATE_FORMAT(sp_timesheet_tracker.track_date, '%Y-%m') = '{$date}'";
				}else if($time == 'is_between'){
					$date_start = Date('Y-m-d', strtotime($start_date));
					$date_end = Date('Y-m-d', strtotime($end_date));
					$where .= " AND sp_timesheet_tracker.track_date BETWEEN '{$date_start}' AND '{$date_end}'";
				}
					$sql = "SELECT 
						    project.id as project_id,project.name AS project_name,project.budget_hours,
						    SUM(sp_timesheet_tracker.hours) AS total_hours
						FROM
						    sp_timesheet_tracker
						        JOIN
						    project ON sp_timesheet_tracker.project_id_c = project.id
							JOIN users on sp_timesheet_tracker.user_id_c = users.id
						{$where}
						GROUP BY project.id
						";
				$sql_result = $db->query($sql, true);
				$projectArr = array();
				$datapointsTotalArr = $datapointsBudgetedArr = array();
				while ($row = $db->fetchByAssoc($sql_result)) {
					$total_hours = '0.0';
					$budget_hours='0.0';
					if($row['total_hours']>0){
						$total_hours = $row['total_hours'];
					}
					if($row['budget_hours']>0){
						$budget_hours = $row['budget_hours'];
					}
					$datapointsTotalArr[] = array(
							 'y' => $total_hours,
							 'label' => $row['project_name'],
							 );
					$datapointsBudgetedArr[] = array(
							'y' => $budget_hours,
							 'label' => $row['project_name'],
							 );
				}
					 $projectArr[] = array(
								'type' => "column",
								'name' => 'Total Hours',
								'legendText' => 'Total Hours',
								'showInLegend' => true, 
								'dataPoints' => $datapointsTotalArr,
						);
						$projectArr[] = array(
								'type' => "column",
								'name' => 'Budgeted',
								'legendText' => 'Budgeted',
								'showInLegend' => true, 
								'dataPoints' => $datapointsBudgetedArr,
						);
						//echo '<pre>';
						//print_r($projectArr);
						$data_to_return = $projectArr;
				# code...
				break;
				
			    default:
				break;
		}
		return $data_to_return;
	}
}
?>