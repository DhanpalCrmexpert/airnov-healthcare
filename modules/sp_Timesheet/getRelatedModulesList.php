<?php
	
	global $app_list_strings;
	$selected_module = $_POST['selectedModule'];
	
	if(empty($selected_module)){
		$value = array(''=>'Select Related Module');
		$value = get_select_options ($value);
		print_r (json_encode($value));
		return json_encode($value); 
		exit();
	}
	else{
		 $module_meta_data_fields = '';
		 $bean = BeanFactory::getBean($selected_module);
		 if(!empty($bean)){
			$module_meta_data_fields = $bean->getFieldDefinitions();
		 }
		 $modules_list = array();
		 // $modules_list[''] = 'Select Related Module';
		 
		if(!empty($module_meta_data_fields)){	
			foreach($module_meta_data_fields as $module_field_key=>$module_field_val){
				$include = true;
				if(is_object($module_field_val) || is_array($module_field_val)){	
					foreach($module_field_val as $key=>$val){
						// Add the types you don't want to send
						if($key == "relationship"){
							$related_module_name = getRelatedModuleNameCustom($bean, $module_field_key);
							if($related_module != "-1" && !empty($related_module_name)){
								$related_module_val = $app_list_strings['moduleList'][$related_module_name];
								if(!empty($related_module_val)){
									$modules_list[$module_field_key] = $related_module_val;
								}
							}
						}
					}
				}
			}	
		}
		
		$value = get_select_options ($modules_list, '');
		print_r (json_encode($value));
		return json_encode($value); 
		exit();
	}
	
	function getRelatedModuleNameCustom($module_obj, $relationship_name){
		if($module_obj->load_relationship($relationship_name)){
			$related_module = $module_obj->$relationship_name->getRelatedModuleName();
			return $related_module;
		}
		return '-1';
	}