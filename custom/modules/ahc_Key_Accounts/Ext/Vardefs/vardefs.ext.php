<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2020-11-18 16:02:23
$dictionary['ahc_Key_Accounts']['fields']['annual_revenue_year_c']['inline_edit']='1';
$dictionary['ahc_Key_Accounts']['fields']['annual_revenue_year_c']['labelValue']='Annual Revenue Year';

 

// created: 2020-11-03 16:17:29
$dictionary["ahc_Key_Accounts"]["fields"]["ahc_key_accounts_accounts_1"] = array (
  'name' => 'ahc_key_accounts_accounts_1',
  'type' => 'link',
  'relationship' => 'ahc_key_accounts_accounts_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'side' => 'right',
  'vname' => 'LBL_AHC_KEY_ACCOUNTS_ACCOUNTS_1_FROM_ACCOUNTS_TITLE',
);

?>