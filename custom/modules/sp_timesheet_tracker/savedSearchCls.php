<?php
//ini_set('display_errors',1);
//error_reporting(E_ALL);
require_once('modules/MySettings/StoreQuery.php');
class savedSearchCls{
	function savedSearchFunc(&$bean, $event, $arguments) {
		global $db,$current_user;

		if(!$current_user->isAdmin()){
		 $squery123= new StoreQuery();
		 $squery123->query=array('user_id_c_advanced'=>$current_user->id,'track_user_advanced'=>$current_user->full_name, 'module'=>'sp_timesheet_tracker','searchFormTab'=>'advanced_search','query'=>'true','action'=>'index');
		 $squery123->SaveQuery('sp_timesheet_tracker'); 
		}

		$current_date=date('m/d/Y');
        $savedBean = new SavedSearch();
        $savedBean->retrieve_by_string_fields(array('name'=>'My Timesheet','assigned_user_id'=>$current_user->id,'search_module'=>'sp_timesheet_tracker'));
		$contents = 'YToyMjp7czoxMzoic2VhcmNoRm9ybVRhYiI7czoxNToiYWR2YW5jZWRfc2VhcmNoIjtzOjU6InF1ZXJ5IjtzOjQ6InRydWUiO3M6MjA6InBhcmVudF90eXBlX2FkdmFuY2VkIjtzOjA6IiI7czoyMDoicGFyZW50X25hbWVfYWR2YW5jZWQiO3M6MDoiIjtzOjk6InBhcmVudF9pZCI7czowOiIiO3M6MzI6InRyYWNrX2RhdGVfYWR2YW5jZWRfcmFuZ2VfY2hvaWNlIjtzOjE6Ij0iO3M6MjU6InJhbmdlX3RyYWNrX2RhdGVfYWR2YW5jZWQiO3M6MDoiIjtzOjMxOiJzdGFydF9yYW5nZV90cmFja19kYXRlX2FkdmFuY2VkIjtzOjA6IiI7czoyOToiZW5kX3JhbmdlX3RyYWNrX2RhdGVfYWR2YW5jZWQiO3M6MDoiIjtzOjI3OiJob3Vyc19hZHZhbmNlZF9yYW5nZV9jaG9pY2UiO3M6MToiPSI7czoyMDoicmFuZ2VfaG91cnNfYWR2YW5jZWQiO3M6MDoiIjtzOjI2OiJzdGFydF9yYW5nZV9ob3Vyc19hZHZhbmNlZCI7czowOiIiO3M6MjQ6ImVuZF9yYW5nZV9ob3Vyc19hZHZhbmNlZCI7czowOiIiO3M6MTk6InRyYWNrX3VzZXJfYWR2YW5jZWQiO3M6MDoiIjtzOjI1OiJhc3NpZ25lZF91c2VyX2lkX2FkdmFuY2VkIjthOjE6e2k6MDtzOjE6IjEiO31zOjEzOiJzZWFyY2hfbW9kdWxlIjtzOjIwOiJzcF90aW1lc2hlZXRfdHJhY2tlciI7czoxOToic2F2ZWRfc2VhcmNoX2FjdGlvbiI7czo0OiJzYXZlIjtzOjE0OiJkaXNwbGF5Q29sdW1ucyI7czo3MDoiUEFSRU5UX05BTUV8UkVMQVRFRF9SRUNPUkRfVVJMfFRSQUNLX0RBVEV8SE9VUlN8VFJBQ0tfVVNFUnxkZXNjcmlwdGlvbiI7czo4OiJoaWRlVGFicyI7czowOiIiO3M6Nzoib3JkZXJCeSI7czoxODoiUkVMQVRFRF9SRUNPUkRfVVJMIjtzOjk6InNvcnRPcmRlciI7czozOiJBU0MiO3M6ODoiYWR2YW5jZWQiO2I6MTt9';
		$contents1 = unserialize(base64_decode($contents));
		$contents1['user_id_c_advanced'][0] = $current_user->id;
		unset($contents1['assigned_user_id_advanced']);
		$formatted_content = base64_encode(serialize($contents1));
		if(!$savedBean->id){
			$savedBean->new_with_id = true;
			$savedBean->id = create_guid();
			$savedBean->name = 'My Timesheet';
			$savedBean->assigned_user_id = $current_user->id;
			$savedBean->search_module = 'sp_timesheet_tracker';
		}
		$savedBean->contents = $formatted_content;
		$savedBean->save();
	}
}