<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2019-02-21 03:39:46
$dictionary['Prospect']['fields']['jjwg_maps_lat_c']['inline_edit']=1;

 

 // created: 2019-02-21 03:39:46
$dictionary['Prospect']['fields']['jjwg_maps_address_c']['inline_edit']=1;

 

 // created: 2020-11-20 15:52:54
$dictionary['Prospect']['fields']['department_ahp_c']['inline_edit']='1';
$dictionary['Prospect']['fields']['department_ahp_c']['labelValue']='Department';

 

 // created: 2019-02-21 03:39:46
$dictionary['Prospect']['fields']['jjwg_maps_geocode_status_c']['inline_edit']=1;

 

 // created: 2020-11-20 15:51:21
$dictionary['Prospect']['fields']['customer_id_c']['inline_edit']='1';
$dictionary['Prospect']['fields']['customer_id_c']['labelValue']='Customer ID';

 

 // created: 2020-11-20 15:51:52
$dictionary['Prospect']['fields']['department']['inline_edit']=true;
$dictionary['Prospect']['fields']['department']['comments']='The department of the contact';
$dictionary['Prospect']['fields']['department']['merge_filter']='disabled';

 

 // created: 2020-11-20 16:04:17
$dictionary['Prospect']['fields']['customer_account_group_c']['inline_edit']='1';
$dictionary['Prospect']['fields']['customer_account_group_c']['labelValue']='Customer Account Group';

 

 // created: 2019-02-21 03:39:46
$dictionary['Prospect']['fields']['jjwg_maps_lng_c']['inline_edit']=1;

 
?>