<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/


class sp_Auto_TimeSheet_Time_TrackerViewEdit extends ViewEdit
{
 	public function __construct()
 	{
 		parent::ViewEdit();
 		$this->useForSubpanel = true;
 		//$this->useModuleQuickCreateTemplate = true;
 	}
	public function display()
 	{
		//////////////////
		global  $sugar_config;
		$siteUrl = $sugar_config['site_url'];
		require_once('modules/TimesheetNinja/license/OutfittersLicense.php');
		$validate_license = OutfittersLicense::isValid('TimesheetNinja');
		if($validate_license !== true) {
		if(is_admin($current_user)) {
			SugarApplication::appendErrorMessage('Timesheet is no longer active due to the following reason: '.$validate_license.' Users will have limited to no access until the issue has been addressed.');
		}
		echo '<p class="error">Timesheet is no longer active Please renew your subscription or check your license configuration.Will be redirect in seconds.....</p>';
		header("refresh:2;url=$siteUrl");
		//functionality may be altered here in response to the key failing to validate
		}
		echo "</br>";
		
		
		//////////////////
		
		$db =  DBManagerFactory::getInstance();
        $this->ev->process();
		
		global $app_list_strings;
		global $timedate;
		
		
		echo '<input type="hidden" name="current_user" id="current_user" value="'.$current_user->id.'"/>';
		echo '<input type="hidden" name="current_user_name" id="current_user_name" value="'.$current_user->name.'"/>';
		echo '<input type="hidden" name="recordid" id="recordid" value="'.$this->bean->id.'"/>';
		echo '<input type="hidden" name="is_admin" id="is_admin" value="'.$current_user->is_admin.'"/>';
		echo '<input type="hidden" name="save_continue_action" id="save_continue_action" value="0"/>';
		
		
		$this->ev->process();
		
		echo $this->ev->display($this->showTitle);	
	
		
//**********************************
	?>

	
		<script type="text/javascript">
		
 $(document).ready(function(){
	 var bean_id = '<?php echo $this->bean->id; ?>';
	 var related_project_name = '';
	 var related_project_id = '';
	 
	 $('#parent_type').after('<br>');
	 $("div[data-label='Project']").parent().hide();
	 $("div[data-label='Project']").parent().next().hide();
	  
	 if(bean_id!=''){
		 var parent_type = '<?php echo $this->bean->parent_type; ?>';
		 if(parent_type=='ProjectTask'){
			  $("div[data-label='Project']").parent().show();
			  $("div[data-label='Project']").parent().next().show();
			  /*$('#related_project').attr('style', 'pointer-events: none;background-color:#DCDCDC;');
			  $('#btn_related_project').attr('style', 'pointer-events: none;');
			  $('#btn_clr_related_project').attr('style', 'pointer-events: none;');
	          document.getElementById('related_project').readOnly = true;*/
		 }
	 }
	  
	  
	 $(document).on('click','#Save_And_Create_New_Entry',function(){
		 if($('.save_continue_action').length){
		    $('.save_continue_action').remove();
		  }
		 $('form').append('<input type="hidden" name="save_continue_action" id="save_continue_action" class="save_continue_action" value="1"/>');
		 var _form = document.getElementById('EditView');
		 _form.action.value='Save';
		 if(check_form('EditView'))
		 SUGAR.ajaxUI.submitForm(_form);
		 return false;
	 });
	  $(document).on('change','#parent_type',function(){
		  if($(this).val()=='ProjectTask'){
			  if($('#related_project').val()==='undefined'){
				  $('#related_project').val('');
				   $('#related_project_id').val('');
			  }
			  $("div[data-label='Project']").parent().show();
			  $("div[data-label='Project']").parent().next().show();
			  $('#parent_name').attr('style', 'pointer-events: none;background-color:#DCDCDC;');
			  $('#btn_parent_name').attr('style', 'pointer-events: none;');
			  $('#btn_clr_parent_name').attr('style', 'pointer-events: none;');
	          document.getElementById('parent_name').readOnly = true;
		  }else{
			 $("div[data-label='Project']").parent().hide();
			  $('#related_project').val('');
			  $('#related_project_id').val('');
			  
			  $('#parent_name').attr('style', 'pointer-events: auto;background-color:white;');
			  $('#btn_parent_name').attr('style', 'pointer-events: auto;');
			  $('#btn_clr_parent_name').attr('style', 'pointer-events: auto;');
	          document.getElementById('parent_name').readOnly = false;
			  
		  }
	  });

$('#parent_name').removeClass('sqsEnabled');
 
 
YAHOO.util.Event.onDOMReady(function(){
    YAHOO.util.Event.addListener('related_project', 'change', function(){
		if($(this).val()!=''){
			
         var project_id = $('#related_project_id').val();
		 var project_task_id = $('#parent_id').val();
		 var url = "index.php?module=sp_Auto_TimeSheet_Time_Tracker&action=geProjectDetails&sugar_body_only=1";
		 var dataString = {"project_id" : project_id,"project_task_id":project_task_id};
		 if(project_task_id!=''){
		 console.log("index.php?module=sp_Auto_TimeSheet_Time_Tracker&action=geProjectDetails&sugar_body_only=1&project_id="+project_id+"&project_task_id="+project_task_id);
		 	$.ajax({
				type : "POST",
				dataType: "json",
				data: dataString,
				url : url,
				success : function(data) {
					if(data.project_task_exists=='0'){
						$('#parent_name').val('');
			            $('#parent_id').val('');
					}
				}
			});
		}
			  $('#parent_name').attr('style', 'pointer-events: auto;background-color:white;');
			  $('#btn_parent_name').attr('style', 'pointer-events: auto;');
			  $('#btn_clr_parent_name').attr('style', 'pointer-events: auto;');
	          document.getElementById('parent_name').readOnly = false;
		}else{
			  $('#parent_name').attr('style', 'pointer-events: none;background-color:#DCDCDC;');
			  $('#btn_parent_name').attr('style', 'pointer-events: none;');
			  $('#btn_clr_parent_name').attr('style', 'pointer-events: none;');
	          document.getElementById('parent_name').readOnly = true;
		}
		/*if($('#parent_type').val()=='ProjectTask'){
         var parent_id = $('#parent_id').val();
		 var url = "index.php?module=sp_Auto_TimeSheet_Time_Tracker&action=geProjectDetails&sugar_body_only=1";
		 var dataString = {"project_task_id" : parent_id};
		 	$.ajax({
				type : "POST",
				dataType: "json",
				data: dataString,
				url : url,
				success : function(data) {
					 $('#related_project').val(data.project_name);
				     $('#related_project_id').val(data.project_id);
				}
			});
		}*/
    });
});
	  
	  $(document).on('click','#SAVE',function(){
		  if($('.save_continue_action').length){
		    $('.save_continue_action').remove();
		  }
		 $('form').append('<input type="hidden" name="save_continue_action" id="save_continue_action" class="save_continue_action" value="0"/>');
		 var _form = document.getElementById('EditView');
		 _form.action.value='Save';
		 if(check_form('EditView'))
		 SUGAR.ajaxUI.submitForm(_form);
		 return false;
	 });
	 
	 
var uid=$('#current_user').val();
var uname=$('#current_user_name').val();
var recordid=$('#recordid').val();
var isadmin=$('#is_admin').val();
$('#manual_entry').val('1');
$('#manual_entry').prop('checked', true);
if(isadmin==0){
$('#employee_id').css('pointer-events','none');
$('#btn_employee_id').css('pointer-events','none');
$('#btn_clr_employee_id').css('pointer-events','none');

}
if(recordid=='')
{
	
$("#employee_id").val(uname);
$("#user_id_c").val(uid);	
}

	
				
});
		</script>
		
		
		
		
		<?php
	
	}
}

