<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

$license_strings = array (
    'LBL_STEPS_TO_LOCATE_KEY_TITLE' => 'Para localizar la clave',
    'LBL_STEPS_TO_LOCATE_KEY_1' => '1. Ingresa para',
    'LBL_STEPS_TO_LOCATE_KEY_2' => '2. Ir a Account->Purchases',
    'LBL_STEPS_TO_LOCATE_KEY_3' => '3. Busque la clave para la compra de este complemento',
    'LBL_STEPS_TO_LOCATE_KEY_4' => '4. Pegar en el cuadro Clave de licencia por debajo de',
    'LBL_STEPS_TO_LOCATE_KEY_5' => '5. Hacer clic en "Validate"',
    'LBL_LICENSE_KEY' => 'clave de licencia',
    'LBL_CURRENT_USERS' => 'Conde Usuario actual',
    'LBL_LICENSED_USERS' => 'N�mero de usuarios con licencia',
    'LBL_VALIDATE_LABEL' => 'Validar',
    'LBL_VALIDATED_LABEL' => 'Validado',
);

