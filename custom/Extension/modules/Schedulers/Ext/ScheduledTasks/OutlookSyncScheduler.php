<?php

$job_strings[] = 'OutlookSyncScheduler';

function OutlookSyncScheduler()
{
    require_once "custom/include/SA_Outlook/Outlook.php";

    $service = new SalesAgility\Outlook\Sync\OutlookSyncService();

    try {
        return $service->run(new SalesAgility\Outlook\Sync\OutlookSyncAPI());
    } catch (Exception $e) {
        return false;
    }

}
