<?php
/**
 * @author SalesAgility <info@salesagility.com>.
 */

class OnUpdateFolders
{
    public function run(SugarBean &$user)
    {
        global $timedate;
        if($user->fetched_row['sa_outlook_folders'] !== $user->sa_outlook_folders) {
            $user->sa_outlook_sync_date = $timedate->asDbDate($timedate->fromTimestamp(1));
        }
    }
}
