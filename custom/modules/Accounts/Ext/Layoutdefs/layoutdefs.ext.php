<?php 
 //WARNING: The contents of this file are auto-generated


/*********************************************************************************
 * This file is part of package Price Books.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Price Books is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
$layout_defs['Accounts']['subpanel_setup']['account_vi_price_books'] = array (
    'order' => 99,
    'module' => 'VI_Price_Books',
    'subpanel_name' => 'VIPriceBooksSubpanelListView',
    'sort_order' => 'asc',
    'sort_by' => 'date_entered',
    'title_key' => 'LBL_PRICE_BOOKS_LABEL',
    'get_subpanel_data' => 'vi_price_books',
    'top_buttons' => array(
        array('widget_class' => 'SubPanelTopCreateButton'),
        array('widget_class' => 'SubPanelTopSelectButton', 'popup_module' => 'VI_Price_Books'),
    ),
);


 // created: 2016-09-26 21:38:56
$layout_defs["Accounts"]["subpanel_setup"]['accounts_sp_timesheet_tracker_1'] = array (
  'order' => 100,
  'module' => 'sp_timesheet_tracker',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_ACCOUNTS_SP_TIMESHEET_TRACKER_1_FROM_SP_TIMESHEET_TRACKER_TITLE',
  'get_subpanel_data' => 'accounts_sp_timesheet_tracker_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>