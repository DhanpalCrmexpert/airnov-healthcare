<?php
/**
 * @author SalesAgility <info@salesagility.com>.
 */

if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

require_once 'custom/include/SA_Outlook/Outlook.php';

$outlook = new SalesAgility\Outlook\Outlook();
$settings = $outlook->getSettings();

$modules = '';

if (!empty($settings['modules'])){
    $modules = json_encode($settings['modules']);
}

header("Access-Control-Allow-Origin: *");
header('Access-Control-Allow-Methods: POST, GET, OPTIONS, PUT, DELETE');
header('Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With');
header('Cache-Control: no-store, no-cache, must-revalidate, max-age=0');
header('Cache-Control: post-check=0, pre-check=0', false);
header('Pragma: no-cache');
header('Content-type: text/json');
header('Content-Disposition: attachment; filename="modules.json";');
header('X-Content-Type-Options: nosniff');
header('Expires: ' . gmdate('D, d M Y H:i:s \G\M\T', time() + 2592000));
set_time_limit(0);
header('Content-Description: File Transfer');
echo($modules);
