
{include file="modules/$moduleName/templates/settings/pro_notification.tpl"}

<div>
    <h2 title="{$DATETIMENOW}">Scheduler settings</h2>
    <div class="ar-container clearfix">
        <div>
        <label>
            <input type="checkbox" disabled />
            <input type="hidden" disabled />
            <span style="display: inline-block; vertical-align: middle; font-weight: normal; margin-left: 10px;">
                Do not adjust time zone for scheduler (set if scheduler server time is already set to user time zone).<br />
                This setting doesn't affect processes, they are run only by server time.
            </span>
        </label>
        </div>
    </div>

    <h2>{$MOD.processes}</h2>
    <div class="ar-container clearfix">
        <div class="jobs-list">
            <table>
                <tr>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Active</th>
                    <th>Status</th>
                    <th>Max Time (sec.)</th>
                    <th>Max Memory (Mb)</th>
                    <th>Interval</th>
                    <th><span title="Next job run time in current user timezone">Next Time</span></th>
                    <th><span title="Job last modified (updated) time in current user timezone">Modified</span></th>
                    <th><span title="When job was first time created (in current user timezone)">Created</span></th>
                    <th>Edit</th>
                    <th>Run</th>
                </tr>
            </table>
            <p>
                <button class="ar-button green" disabled>New process</button>
            </p>
        </div>
    </div>
</div>
