<?php
$app_strings['LBL_SA_OUTLOOK_LICENSE_NO_KEY'] = 'No Valid Key Found';
$app_strings['LBL_SA_OUTLOOK_LICENSE_TOO_MANY_SELECTED'] = 'Selected Users Exceeds Licensed Amount';
$app_strings['LBL_SA_OUTLOOK_LICENSE_USERS_SUCCESS'] = 'Licensed Users Updated Successfully';
$app_strings['LBL_SA_OUTLOOK_LICENSE_VALIDATION_FAIL'] = 'License Key Validation Failed';
$app_strings['LBL_SA_OUTLOOK_LICENSE_VALIDATION_SUCCESS'] = 'License Key Validation Successful';
