<?php

if (!defined('sugarEntry') || !sugarEntry)
{
    die('Not A Valid Entry Point');
}
// echo "abc";
// $GLOBALS['log']->fatal("SendGrid Event is called");
$data = file_get_contents("php://input");
$events = json_decode($data, true);

foreach ($events as $event) {
  // Here, you now have each event and can process them how you like
  //process_event($event);

	if($event['dt_msg_id'])
	{
		// $GLOBALS['log']->fatal(print_r($event,true));
		//Below section is for view message in campaign module
		if($event['campaign']=="true")
		{
			if($event['event'] == "open")
			{				
				$identifier = $event['dt_msg_id'];
				header("Location:index.php?entryPoint=image&identifier={$identifier}");
			}
			die();

		}
		$site_url = $event['site_url'];
		$dt_msg_id = $event['dt_msg_id'];
		$event_name = $event['event'];
		$email = $event['email'];
		$ip = $event['ip'];
		$timestamp = $event['timestamp'];
		$sg_event_id = $event['sg_event_id'];
		$sg_message_id = $event['sg_message_id'];
		$response = $event['response'];
		$reason = $event['reason'];

		$checkasge  = BeanFactory::newBean("sg_SendGrid_Event");
        $searchsge = array(
            'sg_message_id' => $sg_message_id,
            'name' => $event_name,
            'email_address' => $email
        );        

        $resultsge = $checkasge->retrieve_by_string_fields($searchsge);
        $recordid  = $resultsge->fetched_row['id'];

		//if(!$recordid)
		{
			$GLOBALS['log']->fatal("=================sg_message_id={$sg_message_id},name={$event_name},email_address={$email}");
	        $bean = BeanFactory::newBean('sg_SendGrid_Event');
			$bean->name 	= $event_name;
			$bean->crm_url  = $site_url;
			$bean->email_address = $email;
			$bean->ip 		= $ip;
			$bean->sg_timestamp = $timestamp;
			$bean->sg_event_id = $sg_event_id;
			$bean->sg_message_id = $sg_message_id;
			$bean->response = $response;
			$bean->reason = $reason;
			$record_id = $bean->save();
			$bean->load_relationship('sg_sendgrid_event_emails');
			$bean->sg_sendgrid_event_emails->add($dt_msg_id);
		}				
	}
}


