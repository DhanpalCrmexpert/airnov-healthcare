<?php

if(!defined('sugarEntry'))define('sugarEntry', true);

global $sugar_config, $current_user, $app_list_strings;
	
$current_user_id = $current_user->id;

// Form submitted?
if (!empty($_POST['saveForm'])) {
    $conf = new Configurator();
    // Save module settings
    $selected_module = trim($_POST['selected_module']);
    $related_module_relationship = trim($_POST['related_module']);
    $conf->config['timesheet_module_'.$current_user_id] = $selected_module;
    $conf->config['related_module_relationship_'.$current_user_id] = $related_module_relationship;
	
	if(!empty($selected_module) && !empty($related_module_relationship)){
		$module_obj = BeanFactory::getBean($selected_module);
		if($module_obj->load_relationship($related_module_relationship)){
			$related_module = $module_obj->$related_module_relationship->getRelatedModuleName();
			$conf->config['related_module_'.$current_user_id] = $related_module;
		}
				
	    // Save config changes
	    $conf->handleOverride();
	    $conf->clearCache();
	}else{
    	header('Location: index.php?module=sp_Timesheet&action=TimesheetSettings');
	}

}


		//////////////////
		
		$siteUrl = $sugar_config['site_url'];
		require_once('modules/TimesheetNinja/license/OutfittersLicense.php');
		$validate_license = OutfittersLicense::isValid('TimesheetNinja');
		$validate_license = true;
		if($validate_license !== true) {
		if(is_admin($current_user)) {
			SugarApplication::appendErrorMessage('Timesheet is no longer active due to the following reason: '.$validate_license.' Users will have limited to no access until the issue has been addressed.');
		}
		echo '<p class="error">Timesheet is no longer active Please renew your subscription or check your license configuration.Will be redirect in seconds.....</p>';
		header("refresh:2;url=$siteUrl");
		//functionality may be altered here in response to the key failing to validate
		}
		echo "</br>";
		
		
		//////////////////

// Get max discount for your
$selected_module = $sugar_config['timesheet_module_'.$current_user_id];
$related_module = $sugar_config['related_module_'.$current_user_id];
$related_module_relationship = $sugar_config['related_module_relationship_'.$current_user_id];

setDisplay($selected_module, $related_module, $related_module_relationship);

function setDisplay($selected_module, $related_module, $related_module_relationship){
	global $sugar_config, $app_list_strings;
	$module_list = $app_list_strings['moduleList'];

	$moduleValueListHTML = convertToHTML($module_list, $selected_module, "selected_module");	

	$relatedModulesList = getRelatedModules($selected_module);
	
	$relatedModuleValueHTML = convertToHTML($relatedModulesList, $related_module_relationship, "related_module");

	$sugar_smarty = new Sugar_Smarty();
	$sugar_smarty->assign("sugar_config", $sugar_config);
	$sugar_smarty->assign("moduleValueList", $moduleValueListHTML);
	$sugar_smarty->assign("relatedModuleValueList", $relatedModuleValueHTML);

	$sugar_smarty->display("modules/sp_Timesheet/TimesheetSettings.tpl");
}

function getRelatedModules($selected_module){
	global $app_list_strings;
	if(empty($selected_module)){
		return $value = array(''=>'Select Related Module');
	}
	else{
		 $module_meta_data_fields = '';
		 $bean = BeanFactory::getBean($selected_module);
		 if(!empty($bean)){
			$module_meta_data_fields = $bean->getFieldDefinitions();
		 }
		
		 $modules_list = array();
		 // $modules_list[''] = 'Select Related Module';
		 
		
		if(!empty($module_meta_data_fields)){	
			foreach($module_meta_data_fields as $module_field_key=>$module_field_val){
				$include = true;
				if(is_object($module_field_val) || is_array($module_field_val)){	
					foreach($module_field_val as $key=>$val){
						// Add the types you don't want to send
						if($key == "relationship"){
							$related_module_name = getRelatedModuleNameCustom($bean, $module_field_key);
							if($related_module_name != "-1"  && !empty($related_module_name)){
								$related_module_val = $app_list_strings['moduleList'][$related_module_name];
								if(!empty($related_module_val)){
									$modules_list[$module_field_key] = $related_module_val;
								}
							}
						}
					}
				}
			}	
		 }
		return $modules_list;
	}
}

function getRelatedModuleNameCustom($module_obj, $relationship_name){
	if($module_obj->load_relationship($relationship_name)){
		$related_module = $module_obj->$relationship_name->getRelatedModuleName();
		return $related_module;
	}
	return '-1';
}


function convertToHTML($list, $field_value, $field_name){
	$moduleValueList = '<select id="'.$field_name.'" name="'.$field_name.'">';
	
	foreach($list as $key=>$val){
		if($key == $field_value) $selectedKey = 'selected'; else $selectedKey = '';
		$moduleValueList.='<option value="'.$key.'" '.$selectedKey.'>'.$val.'</option>';
	}
	$moduleValueList.='</select>';
	return $moduleValueList;	
}

?>