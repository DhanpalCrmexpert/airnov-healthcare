<?php
	/*********************************************************************************
 * This file is part of package Price Books.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Price Books is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
	$entry_point_registry['VIPriceBooksConfigEnable'] = array(
        'file' => 'custom/VIPriceBooks/VIPriceBooksConfigEnable.php',
        'auth' => true
    );
    
	$entry_point_registry['VIPriceBooksAddProducts'] = array(
        'file' => 'custom/VIPriceBooks/VIPriceBooksAddProducts.php',
        'auth' => true
    );
	
	$entry_point_registry['VIPriceBooksGetData'] = array(
        'file' => 'custom/VIPriceBooks/VIPriceBooksGetData.php',
        'auth' => true
    );
	
	$entry_point_registry['VIPriceBooksGetUpdatedListPrice'] = array(
        'file' => 'custom/VIPriceBooks/VIPriceBooksGetUpdatedListPrice.php',
        'auth' => true
    );
	
    $entry_point_registry['VIPriceBooksGetProductPrice'] = array(
        'file' => 'custom/VIPriceBooks/VIPriceBooksGetProductPrice.php',
        'auth' => true
    );
?>