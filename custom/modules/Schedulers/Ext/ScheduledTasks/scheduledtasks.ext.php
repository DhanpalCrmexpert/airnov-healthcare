<?php 
 //WARNING: The contents of this file are auto-generated



$job_strings[] = 'OutlookSyncScheduler';

function OutlookSyncScheduler()
{
    require_once "custom/include/SA_Outlook/Outlook.php";

    $service = new SalesAgility\Outlook\Sync\OutlookSyncService();

    try {
        return $service->run(new SalesAgility\Outlook\Sync\OutlookSyncAPI());
    } catch (Exception $e) {
        return false;
    }

}


array_push($job_strings, 'calculate_Auto_Track_Timings');

function calculate_Auto_Track_Timings(){
	global $db, $timedate;
	$timesheet_ids = array();
	
	$current_date_time = $timedate->get_gmt_db_datetime();
	
	$getTimeSheetQry = "Select * from sp_auto_timesheet_time_tracker where deleted = 0 and processed = 0";
	$resTimeSheet = $db->query($getTimeSheetQry);
	while($rowTimeSheetId = $db->fetchByAssoc($resTimeSheet))
	{
		$auto_track_id = $rowTimeSheetId['id'];
		$end_date = $rowTimeSheetId['end_date'];
		$parent_type = $rowTimeSheetId['parent_type'];
		$parent_id = $rowTimeSheetId['parent_id'];
		$selected_module_id = $rowTimeSheetId['selected_module_id'];
		$account_id = $selected_module_id;
		
		$timesheet_tracker_id = $rowTimeSheetId['sp_timesheet_tracker_id_c'];
		
		$hours = getHours($end_date, $current_date_time);
		
		$timesheet_tracker_obj = BeanFactory::getBean('sp_timesheet_tracker', $timesheet_tracker_id);
		
		if(!empty($timesheet_tracker_obj->id)){
			$timesheet_ids[$timesheet_tracker_obj->timesheet_id] = $timesheet_tracker_obj->timesheet_id;
		
			$timesheet_tracker_obj->hours = $timesheet_tracker_obj->hours + $hours;
			$timesheet_tracker_obj->save();
		}
		
		if(!empty($account_id)){
			$tt_id = $timesheet_tracker_obj->id;
			$at_id = create_guid();
			$acc_tt_id = "";
			
			$get_acc_tt_qry = "Select id from accounts_sp_timesheet_tracker_1_c where accounts_sp_timesheet_tracker_1accounts_ida = '$account_id' and accounts_sp_timesheet_tracker_1sp_timesheet_tracker_idb = '$tt_id' and deleted = 0";
			$res_acc_tt_qry = $db->query($get_acc_tt_qry);
			while($row_acc_tt_qry = $db->fetchByAssoc($res_acc_tt_qry))
			{
				$acc_tt_id =  $row_acc_tt_qry["id"];
			}
			if(empty($acc_tt_id)){
				$query_account_timesheet = "INSERT into accounts_sp_timesheet_tracker_1_c (id, date_modified, deleted, accounts_sp_timesheet_tracker_1accounts_ida, accounts_sp_timesheet_tracker_1sp_timesheet_tracker_idb) VALUES ('$at_id', '$current_date_time', '0', '$account_id', '$tt_id')";
				$db->query($query_account_timesheet);
			}
		}
		
		$db->query("update sp_auto_timesheet_time_tracker set end_date = '$current_date_time' where id = '$auto_track_id'");
	}
	
	foreach($timesheet_ids as $timesheet_id){
		$getHoursQry = "Select SUM(hours) as total_hours from sp_timesheet_tracker where deleted = 0 and timesheet_id = '$timesheet_id'";
		$resHours = $db->query($getHoursQry);
		while($rowHour= $db->fetchByAssoc($resHours))
		{
			$total_hours = $rowHour['total_hours'];
			$update_hours = "UPDATE sp_timesheet set hours = '$total_hours' where id = '$timesheet_id' and deleted = 0";
			$db->query($update_hours);
		}
	}
	return true;
}
	
	function getHours($start_date, $current_date_time){
		
		$res = get_time_difference( $start_date, $current_date_time );
		
		$hours = ($res['days'] * 8) + ($res['hours']) + ($res['minutes'] / 60);
		
		return $hours = round($hours, 2);
	}
	
	function get_time_difference( $start, $end )
	{
		$uts['start']      =    strtotime( $start );
		$uts['end']        =    strtotime( $end );
		
		if( $uts['start']!==-1 && $uts['end']!==-1 )
		{
			if( $uts['end'] >= $uts['start'] )
			{
				$diff    =    $uts['end'] - $uts['start'];
				if( $days=intval((floor($diff/86400))) )
					$diff = $diff % 86400;
				if( $hours=intval((floor($diff/3600))) )
					$diff = $diff % 3600;
				if( $minutes=intval((floor($diff/60))) )
					$diff = $diff % 60;
				$diff    =    intval( $diff );            
				return( array('days'=>$days, 'hours'=>$hours, 'minutes'=>$minutes, 'seconds'=>$diff) );
			}
			else
			{
				trigger_error( "Ending date/time is earlier than the start date/time", E_USER_WARNING );
			}
		}
		else
		{
			trigger_error( "Invalid date/time data detected", E_USER_WARNING );
		}
		return( false );
	}



array_push($job_strings, 'send_Email_For_Auto_Track_Timings');

function send_Email_For_Auto_Track_Timings(){
		global $app_list_strings, $timedate, $current_user, $app_strings, $db, $sugar_config;
		
		$current_date_time = $timedate->get_gmt_db_datetime();
		
		// Prep time trackker Query
		$selectProjectTask = "SELECT * from sp_auto_timesheet_time_tracker WHERE processed = 0 and deleted = 0";
		
		$result = $db->query($selectProjectTask);

		while($row = $db->fetchByAssoc($result)){
			
			if(!empty($row['last_send_email_at'])){
				$last_send_email_at = $row['last_send_email_at'];
			}
			else{
				$last_send_email_at = $row['date_entered'];
			}
			
			$record_id = $row['id'];
			$current_user_id = $row['user_id_c'];
			$parent_type = $row['parent_type'];
			$parent_id = $row['parent_id'];
			
			$parent_module_bean = BeanFactory::getBean($parent_type, $parent_id);
			
			if(empty($parent_module_bean->id)){
				continue;
			}
			$record_name = $parent_module_bean->name;
			
			$mints = get_time_difference_in_emails($last_send_email_at, $current_date_time);
			
			if($mints >= 45){
				
				$user_obj = BeanFactory::getBean("Users", $current_user_id);
				$email_address = $user_obj->email1;
				if(empty($email_address)){
					continue;
				}
				
				$update_query = "UPDATE sp_auto_timesheet_time_tracker set last_send_email_at = '".$current_date_time."' where id = '".$record_id."'";
				
				$db->query($update_query);
				
				require_once('modules/EmailTemplates/EmailTemplate.php');
    	
				$objTemplate = new EmailTemplate();
				$objTemplate->retrieve_by_string_fields(array('name' => 'Timesheet - Auto Track Timer Notification'));
				
				$link = $sugar_config['site_url'].'/index.php?action=DetailView&module='.$parent_type.'&record=' . $parent_id;
				
				if (!empty($objTemplate->id)) {
					
					$subject = $objTemplate->subject;
					$subject = str_replace('{RELATED_RECORD}', $record_name, $subject);
					
					$body_html = $objTemplate->body_html;
					$body = $objTemplate->body;
				
					$body_html = str_replace('{LINK}', $link, $body_html);
					$body = str_replace('{LINK}', $link, $body);
					
					$body_html = str_replace('{CURRENT_USER}', $user_obj->name, $body_html);
					$body = str_replace('{CURRENT_USER}', $user_obj->name, $body);
					
					$body_html = str_replace('{RELATED_RECORD}', $record_name, $body_html);
					$body = str_replace('{RELATED_RECORD}', $record_name, $body);
				}
				else{
					$subject = "Timesheet Notification Reminder for Task ".$record_name;
					
					$body_html = "Hi ".$user_obj->name. ",<br/> <br/>
								
								You are auto tracking time for the Project task entitled '".$record_name."'.<br/>
								You can stop the timer by redirecting to this link: ".$link."  If your are still working on your task, feel free to continue working.<br/> <br/>
		
								Thank you.<br/> <br/>

								Timesheets Professional System.<br/> <br/>";
					$body = $body_html;
				}
				
				require_once("include/SugarPHPMailer.php");
				$note_msg=new SugarPHPMailer();
				$admin = new Administration();
				$admin->retrieveSettings();
				$note_msg->Subject = $subject;
				$note_msg->prepForOutbound();
				$note_msg->setMailerForSystem();
				
				$note_msg->IsHTML(true);
				$note_msg->From     = $admin->settings['notify_fromaddress'];
				$note_msg->FromName = $admin->settings['notify_fromname'];
				$note_msg->AddAddress($email_address);
				$note_msg->Body = from_html($body_html);
				$note_msg->AltBody = from_html($body);
				
				
				if (!$note_msg->Send()){
					 $GLOBALS['log']->info("Could not send email: " . $note_msg->ErrorInfo);
				}
			}
		}
		return true;
}

function get_time_difference_in_emails( $start, $end )
{
	$uts['start']      =    strtotime( $start );
	$uts['end']        =    strtotime( $end );
	
	if( $uts['start']!==-1 && $uts['end']!==-1 )
	{
		if( $uts['end'] >= $uts['start'] )
		{
			$diff    =    $uts['end'] - $uts['start'];
			$minutes=intval((floor($diff/60)));	
			return $minutes;
		}
		else
		{
			trigger_error( "Ending date/time is earlier than the start date/time", E_USER_WARNING );
		}
	}
	else
	{
		trigger_error( "Invalid date/time data detected", E_USER_WARNING );
	}
	return( false );
}




array_push($job_strings, 'hours_missing_on_projects');

function Hours_Missing_On_Projects(){
	global $db;

	$sql = "select id, reminder_time_limit, users_to_get_reminder from users where deleted = 0 and reminder_to_enter_time = '1'";
	$res = $db->query($sql);
	while($row = $db->fetchByAssoc($res)){
		$reminder_time_limit = $row['reminder_time_limit'];
		$weekStart = date('Y-m-d', strtotime('this week'));
		$weekEnd = date('Y-m-d', strtotime('this week +6 day'));
		$timesheetSql = "select id, hours, name from sp_timesheet where deleted = 0 and user_id_c = '{$row['id']}' and track_date_from = '{$weekStart}' and track_date_to = '{$weekEnd}'";
		$sendReminder = false;
		$timesheetRes = $db->query($timesheetSql);
		if($timesheetRes->num_rows == 0){
			$sendReminder = true;
		}
		
		while($timesheetRow = $db->fetchByAssoc($timesheetRes)){
			if((float)$timesheetRow['hours'] <= (float)$row['reminder_time_limit']){
				$sendReminder = true;
			}
		}
		if($sendReminder){
			$users = explode('^,^', $row['users_to_get_reminder']);
			$managerEmails = [];
			foreach($users as $user){
				$user_obj = BeanFactory::getBean("Users", ltrim(rtrim($user, '^'), '^'));
				$managerEmails[] = $user_obj->email1;		
			}
			notifyUser($row['id'], $managerEmails, "TIMESHEET: {$weekStart} - {$weekEnd}");
		}
	}
	return true;
}


function notifyUser($user_id, $managerEmails, $timesheetName){
	global $sugar_config;
	require_once('modules/EmailTemplates/EmailTemplate.php');
	
	$user_obj = BeanFactory::getBean("Users", $user_id);
	$email_address = $user_obj->email1;
	if(empty($email_address)){
		return;
	}
	
	$employee_name = $user_obj->first_name." ".$user_obj->last_name;
			
	$objTemplate = new EmailTemplate();
	$objTemplate->retrieve_by_string_fields(array('name' => 'Timesheet - Time logging missing reminder'));
	
	if (!empty($objTemplate->id)) {		
		$subject = $objTemplate->subject;
		$subject = str_replace('::employee_name::', $employee_name, $subject);
		$subject = str_replace('::timesheet_name::', $timesheetName, $subject);
		
		$body_html = $objTemplate->body_html;
		$body = $objTemplate->body;
	
		$body_html = str_replace('::employee_name::', $employee_name, $body_html);
		$body_html = str_replace('::timesheet_name::', $timesheetName, $body_html);
		
		$body = str_replace('::employee_name::', $employee_name, $body);
		$body = str_replace('::timesheet_name::', $timesheetName, $body);
	}
	else{
		$subject = "{$employee_name} time logs for $timesheetName are Blank";
		
		$body_html = "Hello {$employee_name},

					Your timesheet is blank for {$timesheetName}. This is a reminder to enter your time into the timesheet system by the end of day. The timesheet will lock and you will not be able to enter time for the past week once the timesheet is submitted by the end of day. 

					Thank you!.<br/>
					Service Push.<br/> <br/>";
		$body = $body_html;
	}
	
	require_once("include/SugarPHPMailer.php");
	$note_msg=new SugarPHPMailer();
	$admin = new Administration();
	$admin->retrieveSettings();
	$note_msg->Subject = $subject;
	$note_msg->prepForOutbound();
	$note_msg->setMailerForSystem();
	
	$note_msg->IsHTML(true);
	$note_msg->From     = $admin->settings['notify_fromaddress'];
	$note_msg->FromName = $admin->settings['notify_fromname'];
	$note_msg->AddAddress($email_address);
	if(!empty($managerEmails)) {
		foreach($managerEmails as $email) {
			$note_msg->AddCC($email);
		}
	}
	$note_msg->Body = from_html($body_html);
	$note_msg->AltBody = from_html($body);
	
	if (!$note_msg->Send()){
		 $GLOBALS['log']->info("Could not send email: " . $note_msg->ErrorInfo);
	}
}


 
array_push($job_strings, 'no_Activity_For_Project_Tasks');

function no_Activity_For_Project_Tasks(){
	global $db;
	$current_date = date("Y-m-d");
	$getProjectTaskQry = "SELECT pt.id as project_task_id, pt.name as project_task_name, pt.assigned_user_id FROM project_task pt WHERE pt.status IN('Not Started', 'Draft') AND pt.is_billable = 'yes' AND pt.deleted = 0";
	
	$resProjectTask = $db->query($getProjectTaskQry);
	while($rowProjectTaskId = $db->fetchByAssoc($resProjectTask))
	{
		$project_task_id = $rowProjectTaskId['project_task_id'];
		$assigned_user_id = $rowProjectTaskId['assigned_user_id'];
		$project_task_name = $rowProjectTaskId['project_task_name'];
		
		$timesheet_tracker_obj = new sp_timesheet_tracker();
		$timesheet_tracker_obj->retrieve_by_string_fields(array('track_date' => $current_date, 'projecttask_id_c' => $project_task_id));
		
		if(empty($timesheet_tracker_obj->id) || $timesheet_tracker_obj->hours<=0 ){
			sendEmailNotication($assigned_user_id, $project_task_id, $project_task_name);
		}
	}
	return true;
}

function sendEmailNotication($assigned_user_id, $record_id, $record_name){
	global $sugar_config;
	require_once('modules/EmailTemplates/EmailTemplate.php');
	
	$user_obj = BeanFactory::getBean("Users", $assigned_user_id);
	$email_address = $user_obj->email1;
	if(empty($email_address)){
		return;
	}
			
	$objTemplate = new EmailTemplate();
	$objTemplate->retrieve_by_string_fields(array('name' => 'Project Task - No Activity'));
	
	$link = $sugar_config['site_url'].'/index.php?action=DetailView&module=ProjectTask&record='. $record_id;
	
	if (!empty($objTemplate->id)) {
		
		$subject = $objTemplate->subject;
		$subject = str_replace('{RELATED_RECORD}', $record_name, $subject);
		
		$body_html = $objTemplate->body_html;
		$body = $objTemplate->body;
	
		$body_html = str_replace('{LINK}', $link, $body_html);
		$body = str_replace('{LINK}', $link, $body);
		
		$body_html = str_replace('{CURRENT_USER}', $user_obj->name, $body_html);
		$body = str_replace('{CURRENT_USER}', $user_obj->name, $body);
		
		$body_html = str_replace('{RELATED_RECORD}', $record_name, $body_html);
		$body = str_replace('{RELATED_RECORD}', $record_name, $body);
	}
	else{
		$subject = "No Activity on Project Task ".$record_name;
		
		$body_html = "Hi ".$user_obj->name. ",<br/> <br/>
					
					There is no time tracking activity for Project Task '".$record_name."'.<br/>
					You can start the timer by redirecting to this link: ".$link." <br/> <br/>

					Thank you.<br/> <br/>

					Timesheets Professional System.<br/> <br/>";
		$body = $body_html;
	}
	
	require_once("include/SugarPHPMailer.php");
	$note_msg=new SugarPHPMailer();
	$admin = new Administration();
	$admin->retrieveSettings();
	$note_msg->Subject = $subject;
	$note_msg->prepForOutbound();
	$note_msg->setMailerForSystem();
	
	$note_msg->IsHTML(true);
	$note_msg->From     = $admin->settings['notify_fromaddress'];
	$note_msg->FromName = $admin->settings['notify_fromname'];
	$note_msg->AddAddress($email_address);
	$note_msg->Body = from_html($body_html);
	$note_msg->AltBody = from_html($body);
	
	if (!$note_msg->Send()){
		 $GLOBALS['log']->info("Could not send email: " . $note_msg->ErrorInfo);
	}
}




/*** DOMAIN_CHECK ***/

if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');$job_strings = array_merge($job_strings, array('sendScheduledDashboards',));function sendScheduledDashboards()
{include_once "modules/AnalyticReporting/controllers/ARController.php";include_once "modules/AnalyticReporting/controllers/ARJobsController.php";$ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZq16 = new ARJobsController();$ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZq16->sendScheduledDashboards();return true;}


/*** DOMAIN_CHECK ***/
 if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');$job_strings = array_merge($job_strings, array('executeARJobs',));function executeARJobs() {include_once "modules/AnalyticReporting/controllers/ARController.php";include_once "modules/AnalyticReporting/controllers/ARJobsController.php";$ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZq16 = new ARJobsController();$ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZq16->executeJobs();return true;}


/*** DOMAIN_CHECK ***/
 if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');$job_strings = array_merge($job_strings, array('sendScheduledReports',));function sendScheduledReports()
{include_once "modules/AnalyticReporting/controllers/ARController.php";include_once "modules/AnalyticReporting/controllers/ARJobsController.php";$ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZq16 = new ARJobsController();$ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZq16->sendScheduledReports();return true;}
?>