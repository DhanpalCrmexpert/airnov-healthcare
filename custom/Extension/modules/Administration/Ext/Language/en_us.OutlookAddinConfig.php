<?php

$mod_strings['LBL_OUTLOOK_ADDIN_CONFIG_TITLE'] = 'Outlook Plugin Configuration';
$mod_strings['LBL_OUTLOOK_ADDIN'] = 'Configuration Settings for SuiteCRM Outlook Plugin';
$mod_strings['LBL_OUTLOOK_ADDIN_TITLE'] = 'SuiteCRM Outlook Plugin';
$mod_strings['LBL_OUTLOOK_ADDIN_TOOLS_DESC'] = 'Manage functionality and configuration for the Outlook Plugin.';
$mod_strings['LBL_OUTLOOK_ADDIN_DOWNLOAD_MANIFEST'] = 'Download Manifest';
$mod_strings['LBL_MICROSOFT_GRAPH_SETTINGS'] = 'Microsoft Graph Settings';
$mod_strings['LBL_MICROSOFT_GRAPH_TENANT_ID'] = 'Tenant ID';
$mod_strings['LBL_MICROSOFT_GRAPH_CLIENT_ID'] = 'Client ID';
$mod_strings['LBL_MICROSOFT_GRAPH_CLIENT_SECRET'] = 'Client Secret';
$mod_strings['LBL_OUTLOOK_ADDIN_LICENSE_HEADING'] = 'License Settings';
$mod_strings['LBL_OUTLOOK_ADDIN_LICENSE_KEY'] = 'Outlook Plugin License Key';
$mod_strings['LBL_OUTLOOK_ADDIN_BTN_VALIDATE_KEY'] = 'Validate';
$mod_strings['LBL_OUTLOOK_ADDIN_BTN_UPDATE_LICENSED'] = 'Update';
$mod_strings['LBL_OUTLOOK_ADDIN_SEARCH_SCHEDULERS'] = 'Schedulers';
$mod_strings['LBL_OUTLOOK_ADDIN_SEARCH_SCHEDULERS_HELP'] = 'The following Outlook Plugin schedulers were detected:';
$mod_strings['LBL_OUTLOOK_ADDIN_SEARCH_SCHEDULERS_LAST_RUN'] = 'last run:';
$mod_strings['LBL_OUTLOOK_ADDIN_SEARCH_SCHEDULERS_NEVER_RUN'] = 'This job has never run';
$mod_strings['LBL_OUTLOOK_ADDIN_SEARCH_SCHEDULERS_NOT_FOUND'] = 'No schedulers found. Consider creating one.';
$mod_strings['LBL_OUTLOOK_ADDIN_SEARCH_SCHEDULERS_DESC'] = 'The Outlook Plugin uses schedulers to synchronise data in the background.';
$mod_strings['LBL_OUTLOOK_ADDIN_SETTINGS'] = 'Outlook Add-in Settings';
$mod_strings['LBL_OUTLOOK_ADDIN_MODULES_SELECT'] = 'Select Modules for Manual Archive:';
