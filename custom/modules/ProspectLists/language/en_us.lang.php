<?php
// created: 2020-10-28 16:59:56
$mod_strings = array (
  'LBL_LEADS' => 'Prospects',
  'LBL_LEADS_SUBPANEL_TITLE' => 'Prospects',
  'LBL_ACCOUNTS' => 'Customers',
  'LBL_ACCOUNTS_SUBPANEL_TITLE' => 'Customers',
);