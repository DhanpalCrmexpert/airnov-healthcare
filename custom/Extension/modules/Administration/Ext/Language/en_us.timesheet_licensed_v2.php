<?php

$mod_strings['LBL_SAMPLELICENSEADDON'] = 'Timesheet Ninja Enterprise Monthly';
$mod_strings['LBL_SAMPLELICENSEADDON_LICENSE_TITLE'] = 'License Configuration';
$mod_strings['LBL_SAMPLELICENSEADDON_LICENSE'] = 'Manage and configure the license for this add-on';
