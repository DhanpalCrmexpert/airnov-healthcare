<?php 
 //WARNING: The contents of this file are auto-generated


/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.

 * SuiteCRM is an extension to SugarCRM Community Edition developed by Salesagility Ltd.
 * Copyright (C) 2011 - 2014 Salesagility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for  technical reasons, the Appropriate Legal Notices must
 * display the words  "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 ********************************************************************************/


$app_list_strings['moduleList']['sp_Timesheet'] = 'Timesheet Ninja';
$app_list_strings['moduleList']['sp_timesheet_tracker'] = 'Timesheet Ninja Reporting';
$app_list_strings['moduleList']['sp_Auto_TimeSheet_Time_Tracker'] = 'Timesheet Global Module Entry';
$app_list_strings['moduleList']['suh_timer_recording'] = 'Timer Recording';
$app_list_strings['moduleList']['sp_Timeoff_TimeCard'] = 'Timeoff TimeCard';
$app_strings['MSG_JS_ALERT_MTG_REMINDER_TIMESHEET_REMINDER'] = 'Timesheet';
$app_strings['LBL_QUICK_TIMESHEET'] = 'Create Timesheet';
$app_list_strings['timesheet_lock_dom'] = array(
	'none' => 'None',
	'week' => 'Week'
);

$app_list_strings['user_preferred_view_list'] = array(
	'Daily' => 'Daily',
	'Weekly' => 'Weekly'
);


$app_list_strings['yes_no_list'] = array(
	'Yes' => 'Yes',
	'No' => 'No',
);




$app_list_strings['moduleList']['AnalyticReporting'] = 'Analytic Reporting';


$app_list_strings['is_billable_list'] = array(
	'yes' => 'Yes',
	'no' => 'No'
);
$app_strings['LNK_NEW_RECORD'] = 'Create Daily Timesheet';
$app_strings['LNK_NEW_RECORD_WEEKLY'] = 'Create Weekly Timesheet';
$app_strings['LNK_LIST'] = 'View Timesheet';
$app_strings['LNK_TIMESHEET_SETTINGS'] = 'Timesheet Settings';

$app_strings['LBL_SA_OUTLOOK_LICENSE_NO_KEY'] = 'No Valid Key Found';
$app_strings['LBL_SA_OUTLOOK_LICENSE_TOO_MANY_SELECTED'] = 'Selected Users Exceeds Licensed Amount';
$app_strings['LBL_SA_OUTLOOK_LICENSE_USERS_SUCCESS'] = 'Licensed Users Updated Successfully';
$app_strings['LBL_SA_OUTLOOK_LICENSE_VALIDATION_FAIL'] = 'License Key Validation Failed';
$app_strings['LBL_SA_OUTLOOK_LICENSE_VALIDATION_SUCCESS'] = 'License Key Validation Successful';


/**
 * Language file for AnalyticReporting dashlet
 */
$app_strings['LBL_ANALYTIC_REPORTING_DASHLET'] = 'AnalyticReporting Dashlet';
$app_strings['LBL_ANALYTIC_REPORTING_DASHLET_DESCRIPTION'] = 'AnalyticReporting Dashlet';
$app_strings['LBL_ANALYTIC_REPORTING_DASHLET_REPORT_SELECTION'] = 'Select Report';
$app_strings['LBL_ANALYTIC_REPORTING_DASHLET_OPEN_REPORT'] = 'Open Report';

$app_list_strings['moduleList']['AnalyticReporting'] = 'Analytic Reporting';

/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2018 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for technical reasons, the Appropriate Legal Notices must
 * display the words "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */

$app_list_strings['moduleList']['VI_Price_Books'] = 'Price Books';


/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2018 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for technical reasons, the Appropriate Legal Notices must
 * display the words "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */

$app_list_strings['moduleList']['ahc_Key_Accounts'] = 'Key Accounts';
$app_list_strings['ahc_key_accounts_type_dom'][''] = '';
$app_list_strings['ahc_key_accounts_type_dom']['Analyst'] = 'Analyst';
$app_list_strings['ahc_key_accounts_type_dom']['Competitor'] = 'Competitor';
$app_list_strings['ahc_key_accounts_type_dom']['Customer'] = 'Customer';
$app_list_strings['ahc_key_accounts_type_dom']['Integrator'] = 'Integrator';
$app_list_strings['ahc_key_accounts_type_dom']['Investor'] = 'Investor';
$app_list_strings['ahc_key_accounts_type_dom']['Partner'] = 'Partner';
$app_list_strings['ahc_key_accounts_type_dom']['Press'] = 'Press';
$app_list_strings['ahc_key_accounts_type_dom']['Prospect'] = 'Prospect';
$app_list_strings['ahc_key_accounts_type_dom']['Reseller'] = 'Reseller';
$app_list_strings['ahc_key_accounts_type_dom']['Other'] = 'Other';


/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2018 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for technical reasons, the Appropriate Legal Notices must
 * display the words "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */

$app_list_strings['moduleList']['sg_SendGrid_Event'] = 'SendGrid Event';

?>