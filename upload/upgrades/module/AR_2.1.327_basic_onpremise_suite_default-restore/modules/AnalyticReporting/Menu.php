<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once('modules/AnalyticReporting/controllers/ARController.php');
require_once('modules/AnalyticReporting/controllers/ARDashboardController.php');

global $mod_strings, $app_strings, $sugar_config;

$items = array();

$canView = ACLController::checkAccess('AnalyticReporting', 'view', true);

if ($canView) {
    $arCtrl = new ARDashboardController();
    $items = $arCtrl->getDashboardList();
}


foreach($items as $item) {
    $uri = "index.php?module=AnalyticReporting&action=dashboard&&record={$item['id']}";

    $module_menu[] = array($uri, $item['title'], null);
}

