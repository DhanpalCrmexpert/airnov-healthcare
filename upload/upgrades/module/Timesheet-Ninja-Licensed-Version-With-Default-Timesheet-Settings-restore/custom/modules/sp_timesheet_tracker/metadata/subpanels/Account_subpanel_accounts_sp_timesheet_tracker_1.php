<?php
// created: 2016-10-24 16:56:08
$subpanel_layout['list_fields'] = array (
  'track_user' => 
  array (
    'type' => 'relate',
    'studio' => 'visible',
    'vname' => 'LBL_TRACK_USER',
    'id' => 'USER_ID_C',
    'link' => true,
    'width' => '10%',
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'Users',
    'target_record_key' => 'user_id_c',
  ),
  'track_date' => 
  array (
    'type' => 'date',
    'vname' => 'LBL_TRACK_DATE',
    'width' => '10%',
    'default' => true,
  ),
  'hours' => 
  array (
    'type' => 'float',
    'vname' => 'LBL_HOURS',
    'width' => '10%',
    'default' => true,
  ),
  'project_sp_timesheet_tracker_1_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_PROJECT_SP_TIMESHEET_TRACKER_1_FROM_PROJECT_TITLE',
    'id' => 'PROJECT_SP_TIMESHEET_TRACKER_1PROJECT_IDA',
    'width' => '10%',
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'Project',
    'target_record_key' => 'project_sp_timesheet_tracker_1project_ida',
  ),
  'date_modified' => 
  array (
    'vname' => 'LBL_DATE_MODIFIED',
    'width' => '45%',
    'default' => true,
  ),
);