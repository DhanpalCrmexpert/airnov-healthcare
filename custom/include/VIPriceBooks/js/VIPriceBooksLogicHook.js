/*********************************************************************************
 * This file is part of package Price Books.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Price Books is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
if(getPriceBookConfigValue == '1'){
    setTimeout(function(){
        $('body').find('div.actionMenuSidebar').css('display','block');
        $('body').find('div.recentlyViewedSidebar').css('display','block');
        $('body').find('li.current-module-action-links').css('display','block');
        $('body').find('li.recent-links-title').css('display','block'); 
        $('body').find('li.current-module-recent-links').css('display','block'); 
    },1000);
    //get record
    var decodeUrl = decodeURIComponent(window.location.href);
    function getParam(name){
        name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
        var regexS = "[\\?&]"+name+"=([^&#]*)";
        var regex = new RegExp( regexS );
        var results = regex.exec(decodeUrl);
        if( results == null )
            return "";
        else
            return results[1];
    }

    if(returnAction == 'DetailView'){
        var recordId = record;
    }else{
        var recordId = getParam('record');
    }
    
    if(action == "EditView"){
        $('#detailpanel_0 > .tab-content').find('div.edit-view-row').remove();
    }else if(action == "DetailView"){
        $('#top-panel-0 > .tab-content').find('div.detail-view-row').remove();
    }

    $.ajax({
        url : 'index.php?entryPoint=VIPriceBooksAddProducts',
        type: 'POST',
        data : {recordId : recordId,
                action : action,
                onLoad : 1
            },
        success : function(data) {
            if(action == "EditView"){
                $('body').find('#detailpanel_0').append(data);
            }else if(action == "DetailView"){
                $('body').find('#top-panel-0').append(data);
            }
        }
    });
    
    function addProduct(){
        var productRow = parseInt($('#productEntryCount').val());
        var num = productRow + 1;
        var record = "";

        $.ajax({
            url : 'index.php?entryPoint=VIPriceBooksAddProducts',
            type: 'POST',
            data : {recordId : record,
                    productRow : productRow,
                    count : num,
                    onLoad : 0
                },
            success : function(data) {
                $('#productTable').append(data);
                $('#productEntryCount').val(num);
                if($('#price_type'+num).val() == "1"){
                    $('td#percentageMark'+num).remove();
                    $('td#roundOff'+num).remove();
                    $('select#price_type'+num).parent().after('<td id="percentageMarkField" style="width:15%;"></td><td id="roundOffField"></td>');
                }  
            }
        });
    }

    function changePriceType(obj,ln){
        $('tr#productQuantityRow'+ln).remove();
        var priceType = $(obj).val();
        if(priceType == "2" || priceType == "3"){
            if($('tr#product_header > th:nth-child(5)').text() != SUGAR.language.get('VI_Price_Books','LBL_PERCENTAGE_MARK') && $('tr#product_header> th:nth-child(6)').text() != SUGAR.language.get('VI_Price_Books','LBL_ROUND_OFF')){
                $('tr#product_header > th:nth-child(5)').text(SUGAR.language.get('VI_Price_Books','LBL_PERCENTAGE_MARK'));
                $('tr#product_header > th:nth-child(6)').text(SUGAR.language.get('VI_Price_Books','LBL_ROUND_OFF'));
            }   

            if($('tr#productLine'+ln+' > td:nth-child(5)').attr('id') != 'percentageMark'+ln && $('tr#productLine'+ln+' > td:nth-child(6)').attr('id') != 'round_off'+ln){
                var addMarkUpRounOffHtml = '<td id="percentageMark'+ln+'">'+
                                            '<select name="percentage_mark'+ln+'" id="percentage_mark'+ln+'" class="column">'+
                                                '<option value="1">'+SUGAR.language.get('VI_Price_Books','LBL_MARK_UP')+'</option>'+
                                                '<option value="0">'+SUGAR.language.get('VI_Price_Books','LBL_MARK_DOWN')+'</option>'+
                                            '</select>'+
                                        '</td>'+
                                        '<td id="roundOff'+ln+'">'+
                                            '<select name="round_off'+ln+'" id="round_off'+ln+'" class="column">'+
                                                '<option value="1">'+SUGAR.language.get('VI_Price_Books','LBL_YES')+'</option>'+
                                                '<option value="0">'+SUGAR.language.get('VI_Price_Books','LBL_NO')+'</option>'+
                                            '</select>'+
                                        '</td>';
                $('tr#productLine'+ln+' > td:nth-child(4)').after(addMarkUpRounOffHtml);
                $('tr#productLine'+ln+' > td#percentageMarkField').remove();
                $('tr#productLine'+ln+' > td#roundOffField').remove();            
            }

            if(priceType == "2"){
                var diffTable = '<table id="percentagePriceTable'+ln+'" class="column"><input type="hidden" name="percenPriceEntryCount'+ln+'" id="percenPriceEntryCount'+ln+'" value="0"/>';
                var differentialSymbol = "(%)";
            }else if(priceType == "3"){
                var diffTable = '<table id="dollarPriceTable'+ln+'" class="column"><input type="hidden" name="dollarPriceEntryCount'+ln+'" id="dollarPriceEntryCount'+ln+'" value="0"/>';
                var differentialSymbol = "($)";
            }

            var QuantityRow = '<tr id="productQuantityRow'+ln+'">'+
                                '<td></td>'+
                                '<td colspan="4" id=productQuantityColumn>'+
                                    diffTable+
                                    '</table>'+
                                            '<button type="button" class="button add_price column" name="add_price" class="column" style="margin-top: 5px;" onclick="addQuantity('+priceType+','+ln+','+"'"+differentialSymbol+"'"+')">'+SUGAR.language.get('VI_Price_Books','LBL_ADD_QUANTITY')+'</button>'+
                                        '</td>'+
                                    '</tr>';
            $('#productLine'+ln).after(QuantityRow);
            
        }else if(priceType == "1"){
            $('td#percentageMark'+ln).remove();
            $('td#roundOff'+ln).remove();
            $('tr#productQuantityRow'+ln).remove();
            $('select#price_type'+ln).parent().after('<td id="percentageMarkField" style="width:15%;"></td><td id="roundOffField"></td>');
        }
    }

    function addQuantity(priceType,ln,differentialSymbol){
        var productName = $('#product_name'+ln).val();
        var listPrice = $('#list_price'+ln).val();
        var productPrice = $('#product_price'+ln).val();
        if(productName != "" && listPrice != ""){
            if(priceType == '2'){
                var priceBooksRow = parseInt($('#percenPriceEntryCount'+ln).val());
                var priceTableId = $('#percentagePriceTable'+ln);
                var id = $('#percenPriceEntryCount'+ln); 
            }else if(priceType == '3'){
                var priceBooksRow = parseInt($('#dollarPriceEntryCount'+ln).val());
                var priceTableId = $('#dollarPriceTable'+ln);
                var id = $('#dollarPriceEntryCount'+ln); 
            } 
            var num = priceBooksRow + 1;
            if(priceBooksRow == 0){
                var priceHeader = '<tr id="priceQuantityHeader">'+
                                        '<th style="padding-right: 64px !important;">'+SUGAR.language.get('VI_Price_Books','LBL_FROM_QUANTITY')+'</th>'+
                                        '<th style="padding-right: 89px !important;" class="quantityHeader">'+SUGAR.language.get('VI_Price_Books','LBL_TO_QUANTITY')+'</th>'+
                                        '<th style="padding-right: 64px !important;" class="quantityHeader">'+SUGAR.language.get('VI_Price_Books','LBL_DIFFERENTIAL')+differentialSymbol+'</th>'+
                                    '</tr>';
                priceTableId.append(priceHeader);
            }
            
            var priceRow = '<tr id="priceQuantityLine'+ln+'_'+num+'">'+
                                '<td>'+
                                    '<input type="text" name="from_quantity'+ln+'_'+num+'" id="from_quantity" value="" label="'+SUGAR.language.get('VI_Price_Books','LBL_FROM_QUANTITY')+'" onfocusout="checkQuantityValueType(this)"></td>'+
                                '<td>'+
                                    '<input type="text" class="quantityColumn" name="to_quantity'+ln+'_'+num+'" id="to_quantity"  label="'+SUGAR.language.get('VI_Price_Books','LBL_TO_QUANTITY')+'" value="" onfocusout="checkQuantityValueType(this)"></td>'+
                                '<td>'+
                                    '<input type="text" class="quantityColumn" name="differential'+ln+'_'+num+'" id="differential" label="'+SUGAR.language.get('VI_Price_Books','LBL_DIFFERENTIAL')+'"  value="" onfocusout="checkQuantityValueType(this)"></td>'+
                                '<td class="minusButton">'+
                                    '<button type="button" class="button btn_minus" onclick="removeQuantity(this,'+ln+','+priceType+')" name="remove">-</button></td>'+
                            '</tr>';
            priceTableId.append(priceRow);
            id.val(num);    
        } else {
            if(productName == "" && listPrice == ""){
                alert(SUGAR.language.get('VI_Price_Books','LBL_PRODUCT_LIST_PRICE_VALID'));
            }else if(productName == ""){
                alert(SUGAR.language.get('VI_Price_Books','LBL_PRODUCT_VALID'));
            }else if(productPrice == ""){
                alert(SUGAR.language.get('VI_Price_Books','LBL_PRODCUT_PRICE_VALID'));
            }else if(listPrice == ""){
                alert(SUGAR.language.get('VI_Price_Books','LBL_LIST_PRICE_VALID'));
            }
        }
    }//end of function

    //Remove Relate Field Values
    function removeRelateValue(ln){
        $('#product_price'+ln).val('');
        $('#list_price'+ln).val('');
        $('#list_price'+ln).removeAttr('readonly');
        if($('#price_type'+ln).val() == "2"){
            $('#percenPriceEntryCount'+ln).val('0');
            $('table#percentagePriceTable'+ln).find('tbody').remove();
        }
        if($('#price_type'+ln).val() == "3"){
            $('#dollarPriceEntryCount'+ln).val('0');
            $('table#dollarPriceTable'+ln).find('tbody').remove();
        }
    }//end of function

    //Remove added quantity
    function removeQuantity(obj,ln,price_type){
        var priceQuantityTable = $(obj).closest('table').attr('id');
        $(obj).closest('tr').remove();
        if(price_type == '2'){
            var priceQuantityRow = parseInt($('#percenPriceEntryCount'+ln).val());
            var num = priceQuantityRow - 1;
            $('#percenPriceEntryCount'+ln).val(num);
            var id = $('#percenPriceEntryCount'+ln);
            var count = $('#percenPriceEntryCount'+ln).val();
        }else if(price_type == '3'){
            var priceQuantityRow = parseInt($('#dollarPriceEntryCount'+ln).val());
            var num = priceQuantityRow - 1;
            $('#dollarPriceEntryCount'+ln).val(num);
            var id = $('#dollarPriceEntryCount'+ln);
            var count = $('#dollarPriceEntryCount'+ln).val();
        }
        
        var j = 1;
        $("#"+priceQuantityTable+" > tbody > tr[id^='priceQuantityLine']").each(function(){
            $(this).attr('id','priceQuantityLine'+ln+'_'+j);
            $(this).find('td:first input').attr('name','from_quantity'+ln+'_'+j);
            $(this).find('td:nth-child(2) input').attr('name','to_quantity'+ln+'_'+j);
            $(this).find('td:nth-child(3) input').attr('name','differential'+ln+'_'+j);
            j++;
        });

        if(num == 0){
            $('tr#priceQuantityHeader').remove();
        }
        id.val(num);
    }//end of function

    //Validation of Quantity Value
    function checkQuantityValueType(obj,ln){
        var quantityVal = $(obj).val();
        var label  = $(obj).attr('label');
        if(quantityVal != ""){
            if(!isInteger(quantityVal)){
                $(obj).next('div#validMsg').remove();
                if(label == "From Quantity"){
                    setBackgroundTimeout(obj);
                    $(obj).after('<div id="validMsg">'+SUGAR.language.get('VI_Price_Books','LBL_QUANTITY_VALID_MSG')+" "+label+'</div>');
                    $(obj).val('');
                }else if(label == "List Price"){
                    if(!isFloat(quantityVal)){
                        setTimeout(obj);
                        $(obj).after('<div id="validMsg" class="column">'+SUGAR.language.get('VI_Price_Books','LBL_QUANTITY_VALID_MSG')+" "+label+'</div>');
                        $(obj).val('');
                    }
                }else if(label == "To Quantity" || label == "Differential"){
                    setBackgroundTimeout(obj);
                    $(obj).after('<div id="validMsg" class="quantityColumn">'+SUGAR.language.get('VI_Price_Books','LBL_QUANTITY_VALID_MSG')+" "+label+'</div>');
                    $(obj).val('');
                }
            }else{
                $(obj).next('div#validMsg').remove();
                if(label == "From Quantity"){
                    if($(obj).parent().parent('tr').prev('tr[id^="priceQuantityLine"]').length == "1"){
                        var prevQuantityVal = $(obj).parent().parent('tr').prev('tr[id^="priceQuantityLine"]').find('td:nth-child(2) > input#to_quantity').val();
                        var quantityVal = $(obj).val();
                        if(parseInt(prevQuantityVal) >= parseInt(quantityVal)){
                            alert(SUGAR.language.get('VI_Price_Books','LBL_FROM_QUANTITY_VALID'));
                            $(obj).val("");
                        }
                    }
                }else if(label == "To Quantity"){
                    var prevQuantityVal = $(obj).parent().parent('tr[id^="priceQuantityLine"]').find('td:nth-child(1) > input#from_quantity').val();
                    var quantityVal = $(obj).val();
                    if(parseInt(prevQuantityVal) >= parseInt(quantityVal)){
                        alert(SUGAR.language.get('VI_Price_Books','LBL_TO_QUANTITY_VALID'));
                        $(obj).val("");
                    }
                }
            }
        }else{
            if(label == "List Price"){
                if($('#price_type'+ln).val() == "2"){
                    $('#percenPriceEntryCount'+ln).val('0');
                    $('table#percentagePriceTable'+ln).find('tbody').remove();
                }
                if($('#price_type'+ln).val() == "3"){
                    $('#dollarPriceEntryCount'+ln).val('0');
                    $('table#dollarPriceTable'+ln).find('tbody').remove();
                }
            }
        }
        
    }//end of function
    
    function setBackgroundTimeout(obj){
        $(obj).css("background", "rgb(255,0,0)");
        setTimeout(function(){
            $(obj).css("background","");
        },2000);
    }

    function removeProduct(obj,ln){
        var productTable = $(obj).closest('table').attr('id');
        $(obj).closest('tr').remove();
        if($('tr#productQuantityRow'+ln).length == "1"){
            $('tr#productQuantityRow'+ln).remove();
        }
        var productRow = parseInt($('#productEntryCount').val());
        var num = productRow - 1;
        $('#productEntryCount').val(num);
        var i = 1;
        $("#"+productTable+" > tbody > tr[id^='productLine']").each(function(){
            $(this).attr('id','productLine'+i);
            $(this).find('td:nth-child(1) input:first').attr('name','product_name'+i);
            $(this).find('td:nth-child(1) input:last').attr('name','product_product_id'+i);
            $(this).find('td:nth-child(2) input').attr('name','product_price'+i);
            $(this).find('td:nth-child(3) input').attr('name','list_price'+i);
            $(this).find('td:nth-child(4) select').attr('name','price_type'+i);
            $(this).find('td:nth-child(5)').attr('id','percentageMark'+i);
            $(this).find('td:nth-child(5) select').attr('name','percentage_mark'+i);
            $(this).find('td:nth-child(6)').attr('id','roundOff'+i);
            $(this).find('td:nth-child(6) select').attr('name','round_off'+i);
            $(this).find('td:nth-child(1) input:first').attr('id','product_name'+i);
            $(this).find('td:nth-child(1) input:last').attr('id','product_product_id'+i);
            $(this).find('td:nth-child(2) input').attr('id','product_price'+i);
            $(this).find('td:nth-child(3) input').attr('id','list_price'+i);
            $(this).find('td:nth-child(4) select').attr('id','price_type'+i);
            $(this).find('td:nth-child(5) select').attr('id','percentage_mark'+i);
            $(this).find('td:nth-child(6) select').attr('id','round_off'+i);

            if($(this).next("tr[id^='productQuantityRow']").length == "1"){
                $(this).next("tr").attr('id','productQuantityRow'+i);
                var productQuantityRow = $(this).next("tr[id='productQuantityRow"+i+"']");

                if(productQuantityRow.find("table[id^='percentagePriceTable']").length == "1"){
                    var priceTable =  'percentagePriceTable'+i;
                    var priceEntryCount = 'percenPriceEntryCount'+i;
                }else if(productQuantityRow.find("table[id^='dollarPriceTable']").length == "1"){
                    var priceTable =  'dollarPriceTable'+i;
                    var priceEntryCount = 'dollarPriceEntryCount'+i;
                }

                productQuantityRow.find("table").attr('id',priceTable);
                productQuantityRow.find("table > input").attr('id',priceEntryCount);
                productQuantityRow.find("table > input").attr('name',priceEntryCount);

                if(productQuantityRow.find("table[id^='percentagePriceTable']").length == "1"){
                    var priceCount = $('#percenPriceEntryCount'+i).val();
                    var priceType = "2";
                    var differentialSymbol = "(%)";
                }else if(productQuantityRow.find("table[id^='dollarPriceTable']").length == "1"){
                    var priceCount = $('#dollarPriceEntryCount'+i).val();
                    var priceType = "3";
                    var differentialSymbol = "($)";
                }
            
                var j = 1;
                productQuantityRow.find("table > tbody > tr").each(function(){
                    if($(this).attr('id') != "priceQuantityHeader"){
                        $(this).attr('id','priceQuantityLine'+i+'_'+j);
                        $(this).find('td:first input').attr('name','from_quantity'+i+'_'+j);
                        $(this).find('td:nth-child(2) input').attr('name','to_quantity'+i+'_'+j);
                        $(this).find('td:nth-child(3) input').attr('name','differential'+i+'_'+j);
                        j++;
                    }
                });

                if(productQuantityRow.find('button.add_price').length == "1"){
                   productQuantityRow.find('button.add_price').attr("onclick","addQuantity("+priceType+","+i+","+"'"+differentialSymbol+"'"+")" );
                }  
            }
            i++;
        });   
        
        if(num == 0){
            $('tr#product_header').remove();    
        }
    }
    
    var lineno;
    function openModulePopup(moduleName,ln){
        lineno = ln;
        var popupRequestData = {
                "call_back_function": "setProductReturn",
                "form_name": "EditView",
                "field_to_name_array": {
                    "id" : "product_product_id"+ln,
                    "name" : "product_name"+ln,
                }
            };//end
        open_popup(moduleName,600,400,'',true,false,popupRequestData); //open popup 
    }//end of function

    function setProductReturn(popupReplyData){
        set_return(popupReplyData);
        formatListPrice(lineno);
    }

    function formatListPrice(lineno){
        for(var i=1;i<=lineno;i++){
            var num = i - 1; 
            var productId = document.getElementById('product_product_id'+lineno).value;
            if(num > 0){
                var prevProductId = document.getElementById('product_product_id'+num).value;
                if(prevProductId == productId){
                    alert(SUGAR.language.get('VI_Price_Books','LBL_PRODUCT_VALIDATION'));
                    document.getElementById('product_product_id'+lineno).value = "";
                    document.getElementById('product_name'+lineno).value = "";
                    document.getElementById('product_price'+lineno).value = "";
                    return false;
                }
            }
        }

        $.ajax({
            url : 'index.php?entryPoint=VIPriceBooksGetProductPrice',
            type: 'POST',
            data : {productId : productId
                },
            success : function(data) {
                $('#product_price'+lineno).val(data);
                $('#list_price'+lineno).removeAttr('readonly');
            }
        }); 
    }
}else if(getPriceBookConfigValue == '0' || getPriceBookConfigValue == ''){
    setTimeout(function(){
        $('body').find('div.actionMenuSidebar').css('display','none');
        $('body').find('div.recentlyViewedSidebar').css('display','none');
        $('body').find('li.current-module-action-links').css('display','none');
        $('body').find('li.recent-links-title').css('display','none'); 
        $('body').find('li.current-module-recent-links').css('display','none');
    },1000);
    var priceBooksResultDiv = '<p class="priceBooksResult"></p>';
    var priceBooksResultDivHtml = SUGAR.language.get('VI_Price_Books','LBL_PRICE_BOOKS_ACCESS')+' '+'<a href="index.php?module=Administration&action=vi_pricebooksconfig">'+SUGAR.language.get('VI_Price_Books','LBL_CLICK_HERE')+'</a> '+SUGAR.language.get('VI_Price_Books','LBL_ENABLE_LABEL');
    if($('body').find('div.list-view-rounded-corners').length == "1"){
        $('body').find('div.list-view-rounded-corners > table.list').hide();
        $('body').find('div.list-view-rounded-corners').html(priceBooksResultDiv);
        $('body').find('div.list-view-rounded-corners > p.priceBooksResult').html(priceBooksResultDivHtml); 
    }else if($('body').find('div.listViewEmpty > p.msg').length == "1"){
        $('body').find('div.listViewEmpty').removeClass('view');
        $('body').find('div.listViewEmpty > p.msg').hide();
        $('body').find('div.listViewEmpty').html(priceBooksResultDiv);
        $('body').find('div.listViewEmpty > p.priceBooksResult').html(priceBooksResultDivHtml); 
    }
    window.location.href = "index.php?action=ajaxui#ajaxUILoc=index.php%3Fmodule%3DVI_Price_Books%26action%3Dindex%26parentTab%3DAll";
}