<?php
// created: 2019-03-30 13:22:29
$dictionary["sg_sendgrid_event_emails"] = array (
  'true_relationship_type' => 'one-to-many',
  'relationships' => 
  array (
    'sg_sendgrid_event_emails' => 
    array (
      'lhs_module' => 'Emails',
      'lhs_table' => 'emails',
      'lhs_key' => 'id',
      'rhs_module' => 'sg_SendGrid_Event',
      'rhs_table' => 'sg_sendgrid_event',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'sg_sendgrid_event_emails_c',
      'join_key_lhs' => 'sg_sendgrid_event_emailsemails_ida',
      'join_key_rhs' => 'sg_sendgrid_event_emailssg_sendgrid_event_idb',
    ),
  ),
  'table' => 'sg_sendgrid_event_emails_c',
  'fields' => 
  array (
    0 => 
    array (
      'name' => 'id',
      'type' => 'varchar',
      'len' => 36,
    ),
    1 => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    2 => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'len' => '1',
      'default' => '0',
      'required' => true,
    ),
    3 => 
    array (
      'name' => 'sg_sendgrid_event_emailsemails_ida',
      'type' => 'varchar',
      'len' => 36,
    ),
    4 => 
    array (
      'name' => 'sg_sendgrid_event_emailssg_sendgrid_event_idb',
      'type' => 'varchar',
      'len' => 36,
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'sg_sendgrid_event_emailsspk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'sg_sendgrid_event_emails_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'sg_sendgrid_event_emailsemails_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'sg_sendgrid_event_emails_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'sg_sendgrid_event_emailssg_sendgrid_event_idb',
      ),
    ),
  ),
);