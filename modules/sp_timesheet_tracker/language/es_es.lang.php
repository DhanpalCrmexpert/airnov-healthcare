<?php
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.

 * SuiteCRM is an extension to SugarCRM Community Edition developed by Salesagility Ltd.
 * Copyright (C) 2011 - 2014 Salesagility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for  technical reasons, the Appropriate Legal Notices must
 * display the words  "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 ********************************************************************************/

$mod_strings = array (
  'LBL_ASSIGNED_TO_ID' => 'Asignado a Usuario con Id',
  'LBL_ASSIGNED_TO_NAME' => 'Usuario',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Fecha de Creación',
  'LBL_DATE_MODIFIED' => 'Última Modificación',
  'LBL_MODIFIED' => 'Modificado Por',
  'LBL_MODIFIED_ID' => 'Modificado Por Id',
  'LBL_MODIFIED_NAME' => 'Modificado Por Nombre',
  'LBL_CREATED' => 'Creado Por',
  'LBL_CREATED_ID' => 'Creado Por Id',
  'LBL_DESCRIPTION' => 'Descripción',
  'LBL_DELETED' => 'Eliminado',
  'LBL_NAME' => 'Nombre',
  'LBL_CREATED_USER' => 'Creado Por Usuario',
  'LBL_MODIFIED_USER' => 'Modificado Por Usuario',
  'LBL_LIST_NAME' => 'Nombre',
  'LBL_EDIT_BUTTON' => 'Editar',
  'LBL_REMOVE' => 'Quitar',
  'LBL_LIST_FORM_TITLE' => 'Timesheet Tracker Lista',
  'LBL_MODULE_NAME' => 'Timesheet Tracker',
  'LBL_MODULE_TITLE' => 'Timesheet Tracker',
  'LBL_HOMEPAGE_TITLE' => 'Mi Timesheet Tracker',
  'LNK_NEW_RECORD' => 'Crear Timesheet Tracker',
  'LNK_LIST' => 'Vista Timesheet Tracker',
  'LNK_IMPORT_SP_TIMESHEET_TRACKER' => 'Import Timesheet Tracker',
  'LBL_SEARCH_FORM_TITLE' => 'Buscar Timesheet Tracker',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Ver Historial',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Actividades',
  'LBL_SP_TIMESHEET_TRACKER_SUBPANEL_TITLE' => 'Timesheet Tracker',
  'LBL_NEW_FORM_TITLE' => 'Nuevo Timesheet Tracker',
  'LBL_TRACK_DATE' => 'Date',
  'LBL_HOURS' => 'Hours',
  'LBL_PROJECT_PROJECT_ID' => 'Project (related Project ID)',
  'LBL_PROJECT' => 'Project',
  'LBL_PROJECT_TASKS_PROJECTTASK_ID' => 'Project Tasks (related Project Task ID)',
  'LBL_PROJECT_TASKS' => 'Project Tasks',
  'LBL_TRACK_USER_USER_ID' => 'User (related User ID)',
  'LBL_TRACK_USER' => 'User',
);