<?php
$module_name = 'sp_Auto_TimeSheet_Time_Tracker';
$viewdefs [$module_name] = 
array (
  'DetailView' => 
  array (
    'templateMeta' => 
    array (
      'form' => 
      array (
        'buttons' => 
        array (
          0 => 'EDIT',
          1 => 'DUPLICATE',
          2 => 'DELETE',
          3 => 'FIND_DUPLICATES',
        ),
      ),
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
      'syncDetailEditViews' => true,
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 'name',
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'parent_name',
            'studio' => 'visible',
            'label' => 'LBL_RELATED_MODULE',
          ),
          1 => 
          array (
            'name' => 'selected_module_id',
            'label' => 'LBL_SELECTED_MODULE_ID',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'timesheetid',
            'studio' => 'visible',
            'label' => 'LBL_TIMESHEETID',
          ),
          1 => 
          array (
            'name' => 'timesheet_tracker_id',
            'studio' => 'visible',
            'label' => 'LBL_TIMESHEET_TRACKER_ID',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'start_date',
            'label' => 'LBL_START_DATE',
          ),
          1 => 
          array (
            'name' => 'end_date',
            'label' => 'LBL_END_DATE',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'processed',
            'label' => 'LBL_PROCESSED',
          ),
          1 => 
          array (
            'name' => 'employee_id',
            'studio' => 'visible',
            'label' => 'LBL_EMPLOYEE_ID',
          ),
        ),
        5 => 
        array (
          'hours',
          'manual_entry',
        ),
      ),
    ),
  ),
);
?>
