<?php
$module_name = 'sp_Timesheet';
$searchdefs [$module_name] = 
array (
  'layout' => 
  array (
    'basic_search' => 
    array (
     'user_id_c' => 
      array (
        'name' => 'user_id_c',
        'label' => 'LBL_TRACK_USER',
        'type' => 'enum',
        'function' => 
        array (
          'name' => 'get_user_array',
          'params' => 
          array (
            0 => false,
          ),
        ),
        'default' => true,
        'width' => '10%',
      ),
	  'module' => 
      array (
        'name' => 'module',
        'label' => 'LBL_MODULE',
	   ),
	   'related_module' => 
      array (
        'name' => 'related_module',
        'label' => 'LBL_RELATED_MODULE',
	   ),
    ),
    'advanced_search' => 
    array (
     'user_id_c' => 
      array (
        'name' => 'user_id_c',
        'label' => 'LBL_TRACK_USER',
        'type' => 'enum',
        'function' => 
        array (
          'name' => 'get_user_array',
          'params' => 
          array (
            0 => false,
          ),
        ),
        'default' => true,
        'width' => '10%',
      ),
	  'module' => 
      array (
        'name' => 'module',
        'label' => 'LBL_MODULE',
	   ),
	   'related_module' => 
      array (
        'name' => 'related_module',
        'label' => 'LBL_RELATED_MODULE',
	   ),
    ),
  ),
  'templateMeta' => 
  array (
    'maxColumns' => '3',
    'maxColumnsBasic' => '4',
    'widths' => 
    array (
      'label' => '10',
      'field' => '30',
    ),
  ),
);
?>
