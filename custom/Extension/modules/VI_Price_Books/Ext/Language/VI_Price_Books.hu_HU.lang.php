<?php
/*********************************************************************************
 * This file is part of package Price Books.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Price Books is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
$mod_strings['LBL_ACTIVE'] = 'Aktív';
$mod_strings['LBL_PERCENTAGE_MARK'] = 'Százalékjel';
$mod_strings['LBL_TYPE'] = 'Ár típusa';
$mod_strings['LBL_ROUND_OFF'] = 'Kikapcsolva';
$mod_strings['LBL_ACCOUNT_NAME'] = 'Fiók neve';
$mod_strings['LBL_ACCOUNT_ID'] = 'Számla azonosítója';
$mod_strings['LBL_ACCOUNTS'] = 'Számlák';
$mod_strings['LBL_CONTACT_NAME'] = 'Kapcsolattartó neve';
$mod_strings['LBL_CONTACT_ID'] = 'Kapcsolattartó azonosítója';
$mod_strings['LBL_CONTACTS'] = 'Kapcsolatok';
$mod_strings['LBL_FLAT_PRICE'] = 'Lapos ár';
$mod_strings['LBL_DIFFERENTIAL_PERCENTAGE'] = 'Különbség (%)';
$mod_strings['LBL_DIFFERENTIAL_DOLLAR'] = 'Különbség szerint ($)';
$mod_strings['LBL_PRICE_BOOKS'] = 'Árkönyvek';
$mod_strings['LBL_NAME'] = 'Név';
$mod_strings['LBL_PRICE_TYPE'] = 'Ár típusa';
$mod_strings['LBL_ASSIGNED_TO'] = 'Hozzárendelve';
$mod_strings['LBL_NO_RECORDS'] = 'Nincs rekord található';
$mod_strings['LBL_MARK_UP'] = 'MarkUp (Add)';
$mod_strings['LBL_MARK_DOWN'] = 'MarkDown (Sub)';
$mod_strings['LBL_ADD_QUANTITY'] = 'Mennyiség hozzáadása';
$mod_strings['LBL_PRODUCT_NAME'] = 'Termék neve';
$mod_strings['LBL_PRODUCT_PRICE'] = 'Termék ára';
$mod_strings['LBL_LIST_PRICE'] = 'Listaár';
$mod_strings['LBL_ADD_PRODUCT'] = 'Termék hozzáadása';
$mod_strings['LBL_YES'] = 'Igen';
$mod_strings['LBL_NO'] = 'Nem';
$mod_strings['LBL_SELECT_PRODUCT_MSG'] = "Kérjük, válassza ki a terméket, mielőtt rákattint az árkönyvekre !!!";
$mod_strings['LBL_SELECT_PRICEBOOK_MSG'] = "Kérjük, válasszon csak egy árjegyzéket.";
$mod_strings['LBL_PRODUCT_VALIDATION'] = 'Kérjük, válasszon másik terméket. Ezt a terméket már kiválasztottuk. ';
$mod_strings['LBL_QUANTITY_VALID_MSG'] = "Érvénytelen érték:";
$mod_strings['LBL_FROM_QUANTITY'] = "Mennyiségből";
$mod_strings['LBL_TO_QUANTITY'] = "Mennyiségig";
$mod_strings['LBL_DIFFERENTIAL'] = "Differenciál";
$mod_strings['LBL_PRICE_BOOKS_ACCESS'] = "Kérjük, engedélyezze az Árkönyvek szolgáltatást az Árkönyvek modul eléréséhez.";
$mod_strings['LBL_CLICK_HERE'] = "Kattintson ide";
$mod_strings['LBL_ENABLE_LABEL'] = "engedélyezve.";
$mod_strings['LBL_FROM_QUANTITY_VALID'] = "Kérjük, írjon be nagyobb mennyiségi értéket, mint korábbi a mennyiségi értékre.";
$mod_strings['LBL_TO_QUANTITY_VALID'] = "Kérjük, írjon be nagyobb mennyiségi értéket, mint mennyiségi értéket.";
$mod_strings['LBL_PRODUCT_LIST_PRICE_VALID'] = "Kérjük, adjon hozzá termék- és listaárat a mennyiséghez.";
$mod_strings['LBL_PRODCUT_PRICE_VALID'] = "Kérjük, adja hozzá a termék árát.";
$mod_strings['LBL_LIST_PRICE_VALID'] = "Kérem, adja hozzá a termék árát.";
$mod_strings['LBL_QUANTITY_VALID_RANGE'] = "Kérjük, adjon hozzá termékmennyiségi tartományt.";
$mod_strings['LBL_PRODUCT_VALID'] = "Kérjük, adjon terméket a mennyiség hozzáadásához.";
$mod_strings['LBL_PRODUCT_FIELDS_VALIDATION'] = "Kérjük, adjon hozzá terméknevet és listaárat az árkönyvekhez.";
$mod_strings['LBL_QUANTITY_FIELD_VALIDATION'] = "Kérjük, töltse ki az üres táblák mennyiségtartományát.";
$mod_strings['LBL_EDITVIEW_PANEL1'] = "Árkönyvek részletei";
$mod_strings['LBL_DETAILVIEW_PANEL1'] = "Árkönyvek részletei";
?> 