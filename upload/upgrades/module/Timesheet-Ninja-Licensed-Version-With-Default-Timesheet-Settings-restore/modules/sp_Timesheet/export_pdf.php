<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.

 * SuiteCRM is an extension to SugarCRM Community Edition developed by Salesagility Ltd.
 * Copyright (C) 2011 - 2014 Salesagility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for  technical reasons, the Appropriate Legal Notices must
 * display the words  "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 ********************************************************************************/
require_once('include/export_utils.php');
require_once('include/tcpdf/tcpdf.php');
global $sugar_config;
global $locale;
global $current_user;
global $app_list_strings;

$content = get_custom_pdf_data(clean_string($_REQUEST['record']));

$filename = $_REQUEST['module'];
//use label if one is defined
if(!empty($app_list_strings['moduleList'][$_REQUEST['module']])){
    $filename = $app_list_strings['moduleList'][$_REQUEST['module']];
}

//strip away any blank spaces
$filename = str_replace(' ','',$filename);

if($_REQUEST['members'] == true)
	$filename .= '_'.'members';
///////////////////////////////////////////////////////////////////////////////
////	BUILD THE EXPORT PDF FILE

downloadPDF($filename, $content);

function downloadPDF($filename, $content)
{
	$filename = $filename.".pdf";
	$records = $content['records'];
	$records_inner = $content['records'];
	
	if(sugar_is_file('custom/themes/default/images/logo.jpg'))
	{
		$pdf_data_table = '<img src="custom/themes/default/images/logo.jpg" height="150" width="300" />'.'<br/>';
	}
	else if(sugar_is_file('custom/themes/default/images/company_logo.png')){
		$pdf_data_table = '<img src="custom/themes/default/images/company_logo.png" height="150" width="300" />'.'<br/>';
	}
	else{
		$pdf_data_table = '<img src="themes/default/images/company_logo.png" height="150" width="300" />'.'<br/>';
	}
	
	foreach($records as $key_out=>$record_out){
		$total_hours = 0;
		// $pdf_data_table .= '<br/><br/>';
		if(!isset($records[$key_out]['processed']) || $records[$key_out]['processed'] != '1'){
			$user_id = $record_out["user_id_c"];
			$records[$key_out]['processed'] = 1;
			$user_obj = BeanFactory::getBean("Users", $user_id);
			$pdf_data_table .= '<br/><br/><br/><br/>';
			$pdf_data_table .= '<table>';
			$pdf_data_table .= '<tr>'; 
			$pdf_data_table .= '<td align="left">Employee Name:</td> <td align="left">'.$user_obj->name.'</td>'; 
			$pdf_data_table .= '<td align="left">Title:</td> <td align="left">'.$user_obj->title.'</td>'; 
			$pdf_data_table .= '</tr>'; 
			$pdf_data_table .= '<tr>'; 
			$pdf_data_table .= '<td align="left">Employee Number:</td> <td align="left"></td>'; 
			$pdf_data_table .= '<td align="left">Status:</td> <td align="left">'.$user_obj->status.'</td>'; 
			$pdf_data_table .= '</tr>';
			$pdf_data_table .= '<tr>'; 		
			$pdf_data_table .= '<td align="left">Department:</td> <td align="left">'.$user_obj->department.'</td>'; 
			$pdf_data_table .= '<td align="left">Supervisor:</td> <td align="left">'.$user_obj->reports_to_name.'</td>'; 
			$pdf_data_table .= '</tr>';
			$pdf_data_table .= '</table>'; 
			
		
			$pdf_data_table .= '<br/><br/><br/><br/>';
			$pdf_data_table .= '<table border="1" cellspacing="3" cellpadding="4">';
			$pdf_data_table .= '<tr>'; 
			$pdf_data_table .= '<th align="center"><b>Date</b></th>'; 
			$pdf_data_table .= '<th align="center"><b>Activity</b></th>'; 
			$pdf_data_table .= '<th align="center"><b>Notes</b></th>'; 
			$pdf_data_table .= '<th align="center"><b>Hours</b></th>'; 
			$pdf_data_table .= '</tr>';
				
			foreach($records_inner as $key=>$record){
				if($user_id == $record["user_id_c"] && (!isset($$records_inner[$key_out]['processed'] )|| $records_inner[$key_out]['processed'] != '1')){
					$records[$key]['processed'] = 1;
					$$records_inner[$key]['processed'] = 1;
					$pdf_data_table .= '<tr>'; 
					$pdf_data_table .= '<td align="center">'.$record["track_date"].'</td>'; 
					$pdf_data_table .= '<td align="center">'.$record["related_record_url"].'</td>'; 
					$pdf_data_table .= '<td align="center">'.$record["description"].'</td>'; 
					$pdf_data_table .= '<td align="center">'.$record["hours"].'</td>'; 
					$pdf_data_table .= '</tr>';
					$total_hours = $total_hours  + $record["hours"];
				}
			}
			$pdf_data_table .= '<tr>';  
			$pdf_data_table .= '<td align="right" colspan="3"><b>Total Hours&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b> </td>'; 
			$pdf_data_table .= '<td align="center">'.$total_hours.'</td>'; 
			$pdf_data_table .= '</tr>';
			$pdf_data_table .= '</table>';
		}
	}

	$pdf = new TCPDF('L', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
	
	$pdf_data = $pdf_data_table;
   // set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('spcrm');
	$pdf->SetTitle('spcrm');
	$pdf->SetSubject('spcrm');
	$pdf->SetPrintHeader(false);
	$pdf->SetPrintFooter(false);
	$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
	$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
	$pdf->setFooterMargin();
	$pdf->SetAutoPageBreak(TRUE,20);
	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
	$pdf->SetFont('helvetica', '', 12);
	$pdf->AddPage();
	$pdf->writeHTML($pdf_data, true, 0, true, 0);
	ob_clean();//error removed cant send pdf file for output
	$pdf->Output($filename, 'D');
	exit;
}

function get_custom_pdf_data($record_id){
	global $db;
	
	$timesheet_obj = BeanFactory::getBean("sp_Timesheet", $record_id);
	$timesheet_obj_emp = $timesheet_obj->user_id_c;
	
	$i = 0;
	$time_sheet_tracker_list = array();
	$getTimeSheetQry = "Select * from sp_timesheet_tracker where deleted = 0 and timesheet_id='$record_id' and hours > 0 and user_id_c = '$timesheet_obj_emp' ORDER BY track_date ASC";
	$resTimeSheet = $db->query($getTimeSheetQry);
	while($rowTimeSheetId = $db->fetchByAssoc($resTimeSheet))
	{
		$time_sheet_tracker_list['records'][$rowTimeSheetId['id']]['track_date'] = $rowTimeSheetId['track_date'];
		$time_sheet_tracker_list['records'][$rowTimeSheetId['id']]['hours'] = $rowTimeSheetId['hours'];
		$time_sheet_tracker_list['records'][$rowTimeSheetId['id']]['description'] = $rowTimeSheetId['description'];
		
		$res = explode('&gt;', $rowTimeSheetId['related_record_url']);
		
		if(isset($res[1]) && !empty($res[1])){
			$url_name = chop($res[1],"&lt;/a");
		}
		else{
			$url_name = $res[0];
		}
		
		$time_sheet_tracker_list['records'][$rowTimeSheetId['id']]['related_record_url'] = $url_name;
		
		$time_sheet_tracker_list['records'][$rowTimeSheetId['id']]['user_id_c'] = $rowTimeSheetId['user_id_c'];
		
		$i++;
	}
	return $time_sheet_tracker_list;
}