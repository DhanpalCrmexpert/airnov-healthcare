<?php
/*********************************************************************************
 * This file is part of package Price Books.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Price Books is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
$mod_strings['LBL_PRICE_BOOKS'] = "Книга цен";
$mod_strings['LBL_PRICE_BOOKS_DESCRIPTION'] = "Управление книгами цен продуктов для определенных контактов / учетных записей.";
$mod_strings['LBL_ENABLE_PRICE_BOOKS'] = "Включить функцию ценовых книг";
$mod_strings['LBL_PRICE_BOOKS_ACTIVATED'] = "Функция ценовых книг успешно активирована.";
$mod_strings['LBL_PRICE_BOOKS_DEACTIVATED'] = "Функция ценовых книг успешно отключена.";
$mod_strings['LBL_CLICK_HERE'] = "Нажмите здесь";
$mod_strings['LBL_MANAGE_PRICEBOOK'] = "Управление книгами цен";
//license
$mod_strings['LBL_LICENCE_ACTIVE_LABEL'] = "VIPriceBooksLicenseAddon больше не активен по следующей причине:";
$mod_strings['LBL_LICENCE_ISSUES'] = "Пользователи не будут ограничены в доступе, пока проблема не будет решена";
$mod_strings['LBL_CLICK_HERE'] = "Нажмите здесь";
$mod_strings['LBL_LICENCE_ACTIVE'] = "VIPriceBooksLicenseAddon больше не активен";
$mod_strings['LBL_RENEW_LICENCE'] = "Пожалуйста, продлите подписку или проверьте конфигурацию лицензии.";
$mod_strings['LBL_UPDATE_LICENSE'] = "Обновить лицензию";
?>