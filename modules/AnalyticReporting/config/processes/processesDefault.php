<?php
return array(
    'calculateAuditedFieldStateByTimePeriod',
    'calculateAuditedFieldStateByMonth',
    'combineMeetingsTasksCalls',
    'combineMeetingsTasksCallsEmails',
    'combineMeetingsTasksCallsEmailsFull',
    'updateMeetingsTasksCalls',
    'updateMeetingsTasksCallsEmails',
    'updateMeetingsTasksCallsEmailsFull',
    'generateAuditData',
    'syncReportsWithGoogleDrive',
    'addIndexes',
);