<?php


if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

global $current_user, $sugar_config;
$current_user_id = $current_user->id;
$current_user_name = $current_user->name;

$related_record_id = $_POST['related_record_id'];
$module = $_POST['related_module'];

$record_module_bean = BeanFactory::getBean($module, $related_record_id);
$related_record_name = $record_module_bean->name;

if($module == "ProjectTask"){
	$selected_record_id = $record_module_bean->project_id;
	$selected_module = 'Project';
	$related_module = 'ProjectTask';		
	$relationship_name = 'projecttask';
}
else if($module == "Cases"){
	$selected_record_id = $record_module_bean->account_id;
	$selected_module = 'Accounts';
	$related_module = 'Cases';		
	$relationship_name = 'cases';
}
$week_start_date = date("Y-m-d");
$result = getDates($week_start_date);

$week_start_date = $result['start'];
$week_end_date = $result['end'];

// $selected_module = $sugar_config['timesheet_module_'.$current_user_id];
// $related_module = $sugar_config['related_module_'.$current_user_id];		
// $relationship_name = $sugar_config['related_module_relationship_'.$current_user_id];		

// $GLOBALS['log']->fatal('selected_module::'.print_r($selected_module, 1));
// $GLOBALS['log']->fatal('related_module::'.print_r($related_module, 1));
// $GLOBALS['log']->fatal('timesheet_id::'.print_r($timesheet_id, 1));

if(checkIfRelatedRecordAlreadyInQueue($related_record_id, $related_module)){
	echo "Auto Time tracker already activiated.";
	exit;	
}

if(empty($selected_record_id)){
	$selected_record_id = getParentID($relationship_name, $related_record_id, $selected_module, $related_module);
}

$timesheet_id = getTimeSheetID($current_user_id, $current_user_name, $selected_module, $related_module, $week_start_date, $week_end_date, $relationship_name);

$timesheet_tracker_id = getTimeSheetTrackerID($current_user_id, $selected_module, $related_module, $timesheet_id, $selected_record_id, $related_record_id, $related_record_name);


$auto_record_id = createAutoTimeTrackerRecord($timesheet_id, $timesheet_tracker_id, $current_user_id, $selected_module, $related_module, $selected_record_id, $related_record_id);
createTimeTrackingObject($related_module, $related_record_id, $current_user_id, $auto_record_id);
echo "Time calculation started automatically.";
exit;


function createAutoTimeTrackerRecord($timesheet_id, $timesheet_tracker_id, $current_user_id, $selected_module, $related_module, $selected_record_id, $related_record_id){
	global $timedate;
	$auto_timesheet_time_tracker_obj = BeanFactory::getBean('sp_Auto_TimeSheet_Time_Tracker');
	$auto_timesheet_time_tracker_obj->name = "Auto TimeSheet Record";
	$auto_timesheet_time_tracker_obj->parent_type = $related_module;
	$auto_timesheet_time_tracker_obj->parent_id = $related_record_id;
	$auto_timesheet_time_tracker_obj->sp_timesheet_id_c = $timesheet_id;
	$auto_timesheet_time_tracker_obj->sp_timesheet_tracker_id_c = $timesheet_tracker_id;
	$auto_timesheet_time_tracker_obj->start_date = $timedate->now();
	$auto_timesheet_time_tracker_obj->end_date = $timedate->now();
	$auto_timesheet_time_tracker_obj->processed = 0;
	$auto_timesheet_time_tracker_obj->selected_module_id = $selected_record_id;
	$auto_timesheet_time_tracker_obj->user_id_c = $current_user_id;
	$auto_timesheet_time_tracker_id = $auto_timesheet_time_tracker_obj->save();
	return $auto_timesheet_time_tracker_id;
}

function getTimeSheetTrackerID($current_user_id, $selected_module, $related_module, $timesheet_id, $selected_record_id, $related_record_id, $related_record_name){
	
	$current_date = date("Y-m-d");
	$filters = array(
		"track_date" => $current_date,
		"parent_type" => $selected_module,
		"parent_id" => $selected_record_id,
		"projecttask_id_c" => $related_record_id,
		"user_id_c" => $current_user_id,
		"timesheet_id" => $timesheet_id,
	);

	$timesheet_tracker_obj = BeanFactory::getBean('sp_timesheet_tracker');
	$timesheet_tracker_obj->retrieve_by_string_fields($filters);
	
	if(empty($timesheet_tracker_obj->id)){
		$timesheet_tracker_obj->name =  "TimeSheet Tracker";
		$timesheet_tracker_obj->track_date =  $current_date;
		$timesheet_tracker_obj->project_id_c =  $selected_record_id;
		$timesheet_tracker_obj->projecttask_id_c =  $related_record_id;
		$timesheet_tracker_obj->parent_type =  $selected_module;
		$timesheet_tracker_obj->parent_id =  $selected_record_id;
		$timesheet_tracker_obj->related_record_url =  '<a href="index.php?module='.$related_module.'&action=DetailView&record='.$related_record_id.'">'.$related_record_name.'</a>';
		$timesheet_tracker_obj->hours =  "0.00";
		$timesheet_tracker_obj->user_id_c =  $current_user_id;
		$timesheet_tracker_obj->timesheet_id =  $timesheet_id;
		$timesheet_tracker_obj->save();
	}
	$timesheet_tracker_obj->save();
	return $timesheet_tracker_obj->id;
	
}

function getTimeSheetID($current_user_id, $current_user_name, $selected_module, $related_module, $week_start_date, $week_end_date, $relationship_name){

	$filters = array(
		"module" => $selected_module,
		"related_module" => $related_module,
		"track_date_from" => $week_start_date,
		"track_date_to" => $week_end_date,
		"user_id_c" => $current_user_id,
	);
	
	$timesheet_obj = BeanFactory::getBean('sp_Timesheet');
	$timesheet_obj->retrieve_by_string_fields($filters);
	
	if(empty($timesheet_obj->id)){
		$timesheet_obj->name =  "TimeSheet: ".$week_start_date." - ".$week_end_date;
		$timesheet_obj->track_date_from =  $week_start_date;
		$timesheet_obj->track_date_to =  $week_end_date;
		$timesheet_obj->user_id_c =  $current_user_id;
		$timesheet_obj->hours =  0;
		$timesheet_obj->module =  $selected_module;
		$timesheet_obj->related_module =  $related_module;
		$timesheet_obj->related_module_relationship =  $relationship_name;
		$timesheet_obj->save();
		
	}
	return $timesheet_obj->id;
}

function getParentID($relationship_name, $related_record_id, $selected_module, $related_module){
	// echo $relationship_name;
	$related_module_bean = BeanFactory::getBean($related_module, $related_record_id);
	if($related_module_bean->load_relationship("projecttask")){
		$selectedBeans = $related_module_bean->projecttask->getBeans();
		
		 $parentBean = false;
		if (!empty($selectedBeans))
		{
			//order the results
			reset($selectedBeans);

			//first record in the list is the parent
			$parentBean = current($selectedBeans);
			return $parentBean->id;
		}
	}
	return false;
}

function getDates($date){
	$week = date("W",strtotime($date));
	$year = date("Y",strtotime($date));
	$dto = new DateTime();
	$result['start'] = $dto->setISODate($year, $week, 1)->format('Y-m-d');
	$result['end'] = $dto->setISODate($year, $week, 7)->format('Y-m-d');
	return $result;
}

function checkIfRelatedRecordAlreadyInQueue($related_record_id, $related_module){
	$auto_timesheet_time_tracker_obj = BeanFactory::getBean('sp_Auto_TimeSheet_Time_Tracker');
	
	$auto_timesheet_time_tracker_obj->retrieve_by_string_fields(array('parent_id' => $related_record_id, 'parent_type'=>$related_module, 'processed'=>0));
	
	if(!empty($auto_timesheet_time_tracker_obj->id) && $auto_timesheet_time_tracker_obj->processed == 0){
		return true;
	}
	return false;
}

function createTimeTrackingObject($selected_module, $selected_record_id, $current_user_id, $auto_record_id){
	global $db;
	$id = create_guid();
	$start_date = gmdate("Y-m-d H:i:s");
	$name = $selected_module.$start_date;

	$sql = "INSERT INTO suh_timer_recording ( id, name, date_entered, date_modified, start_date, created_by, assigned_user_id, parent_type, parent_id, time_spend , auto_time_tracker_id )
            VALUES ( '".$id."', '".$name."', '".$start_date."', '".$start_date."', '".$start_date."', '".$current_user_id."', '".$current_user_id."', '".$selected_module."', '".$selected_record_id."',0 , '".$auto_record_id."');";
	$db->query($sql);
}
?>