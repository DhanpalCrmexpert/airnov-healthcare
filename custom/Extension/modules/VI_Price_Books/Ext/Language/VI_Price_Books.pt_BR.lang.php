<?php
/*********************************************************************************
 * This file is part of package Price Books.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Price Books is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
$mod_strings['LBL_ACTIVE'] = 'Ativo';
$mod_strings['LBL_PERCENTAGE_MARK'] = 'Marca de porcentagem';
$mod_strings['LBL_TYPE'] = 'Tipo de preço';
$mod_strings['LBL_ROUND_OFF'] = 'Arredondar para';
$mod_strings['LBL_ACCOUNT_NAME'] = 'Nome da conta';
$mod_strings['LBL_ACCOUNT_ID'] = 'ID da conta';
$mod_strings['LBL_ACCOUNTS'] = 'Contas';
$mod_strings['LBL_CONTACT_NAME'] = 'Nome do contato';
$mod_strings['LBL_CONTACT_ID'] = 'ID do contato';
$mod_strings['LBL_CONTACTS'] = 'Contatos';
$mod_strings['LBL_FLAT_PRICE'] = 'Preço fixo';
$mod_strings['LBL_DIFFERENTIAL_PERCENTAGE'] = 'Diferencial em (%)';
$mod_strings['LBL_DIFFERENTIAL_DOLLAR'] = 'Diferencial por ($)';
$mod_strings['LBL_PRICE_BOOKS'] = 'Catálogo de preços';
$mod_strings['LBL_NAME'] = 'Nome';
$mod_strings['LBL_PRICE_TYPE'] = 'Tipo de preço';
$mod_strings['LBL_ASSIGNED_TO'] = 'Atribuído a';
$mod_strings['LBL_NO_RECORDS'] = 'Nenhum registro encontrado';
$mod_strings['LBL_MARK_UP'] = 'MarkUp (Adicionar)';
$mod_strings['LBL_MARK_DOWN'] = 'MarkDown (Sub)';
$mod_strings['LBL_ADD_QUANTITY'] = 'Adicionar quantidade';
$mod_strings['LBL_PRODUCT_NAME'] = 'Nome do produto';
$mod_strings['LBL_PRODUCT_PRICE'] = 'Preço do produto';
$mod_strings['LBL_LIST_PRICE'] = 'Preço de tabela';
$mod_strings['LBL_ADD_PRODUCT'] = 'Adicionar produto';
$mod_strings['LBL_YES'] = 'Sim';
$mod_strings['LBL_NO'] = 'Não';
$mod_strings['LBL_SELECT_PRODUCT_MSG'] = "Selecione o produto antes de clicar nos catálogos de preços !!!";
$mod_strings['LBL_SELECT_PRICEBOOK_MSG'] = "Por favor, selecione apenas uma tabela de preços.";
$mod_strings['LBL_PRODUCT_VALIDATION'] = 'Por favor, selecione outro produto. Este produto já está selecionado. ';
$mod_strings['LBL_QUANTITY_VALID_MSG'] = "Valor inválido:";
$mod_strings['LBL_FROM_QUANTITY'] = "Da quantidade";
$mod_strings['LBL_TO_QUANTITY'] = "Para quantidade";
$mod_strings['LBL_DIFFERENTIAL'] = "Diferencial";
$mod_strings['LBL_PRICE_BOOKS_ACCESS'] = "Ative o recurso de catálogos de preços para acessar o módulo de catálogos de preços.";
$mod_strings['LBL_CLICK_HERE'] = "Clique aqui";
$mod_strings['LBL_ENABLE_LABEL'] = "para habilitá-lo.";
$mod_strings['LBL_FROM_QUANTITY_VALID'] = "Por favor, digite um valor maior da quantidade que o anterior ao valor da quantidade.";
$mod_strings['LBL_TO_QUANTITY_VALID'] = "Por favor, digite um valor maior para a quantidade do que para o valor da quantidade.";
$mod_strings['LBL_PRODUCT_LIST_PRICE_VALID'] = "Adicione produto e preço de tabela para adicionar quantidade.";
$mod_strings['LBL_PRODCUT_PRICE_VALID'] = "Por favor, adicione o preço do produto.";
$mod_strings['LBL_LIST_PRICE_VALID'] = "Por favor, adicione o preço de tabela do produto.";
$mod_strings['LBL_QUANTITY_VALID_RANGE'] = "Por favor, adicione um intervalo de quantidades ao produto.";
$mod_strings['LBL_PRODUCT_VALID'] = "Adicione produto para adicionar quantidade.";
$mod_strings['LBL_PRODUCT_FIELDS_VALIDATION'] = "Adicione o nome do produto e o preço de tabela dos catálogos de preços.";
$mod_strings['LBL_QUANTITY_FIELD_VALIDATION'] = "Preencha a faixa de quantidade para placas vazias.";
$mod_strings['LBL_EDITVIEW_PANEL1'] = "Detalhes dos livros de preços";
$mod_strings['LBL_DETAILVIEW_PANEL1'] = "Detalhes dos livros de preços";
?> 