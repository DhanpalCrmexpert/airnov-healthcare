{literal}
<script id='schedulerTemplate' type='text/ractive'>
    <h2>Test Scheduler</h2>
    <div class="ar-container clearfix">
        <p>Select Report:</p>
        <select value="{{selectedReport}}" decorator="select2" style="min-width:375px;">
            {{#types.availableReports}}
                <option value="{{name}}">{{title}}</option>
            {{/}}
        </select>
        {{#hasScheduling}}
        <p>Use report configuration options: <input type='checkbox' checked='{{useDefault}}'></p>
        {{/}}
        {{^useDefault}}
            <p>Select User:</p>
            <select value="{{selectedUser}}" decorator="select2" style="min-width:250px;">
                {{#types.availableOwners}}
                    <option value="{{id}}">{{name}}</option>
                {{/}}
            </select>
            <p>Or custom email:</p>
            <input type="text" value="{{customEmail}}" placeholder="Type email address" style="padding:3px;">
            <p>Formats:</p>
            <label><input type="checkbox" checked="{{scheduledFormats.xlsx}}"> {/literal}{$MOD.label_export_send_xlsx}{literal}</label>
            {{#scheduledFormats.xlsx}}
            <label><input type="checkbox" checked="{{options.excelWithHeaders}}" />{/literal}{$MOD.excelWithHeaders}{literal}</label>
            {{/scheduledFormats.xlsx}}
            <label><input type="checkbox" checked="{{scheduledFormats.pdf}}"> {/literal}{$MOD.label_export_send_pdf}{literal}</label>
            <label>
            {{^isBasic}}
                <input type="checkbox" checked="{{scheduledFormats.pdfxls}}"> {/literal}{$MOD.label_export_send_pdfxls}{literal}
            {{/isBasic}}

            {{#isBasic}}
                <input type="checkbox" disabled> {/literal}{$MOD.label_export_send_pdfxls} <a target="_blank" href="https://www.bisapiens.com/public/file/solution/art-sugarcrm/Comparison_PRO_Basic_Analytic_Reporting_for_SugarCRM.pdf">{$MOD.label_pro}{literal}</a>
            {{/isBasic}}
            </label>
            <label><input type="checkbox" checked="{{scheduledFormats.url}}"> {/literal}{$MOD.label_export_send_url}{literal}</label>
        {{/}}

        <p><button class="ar-button green" on-click='sendReport'>{/literal}Send report{literal}</button></p>

        <h2 style="margin-top:10px;">Actions</h2>
        <p>Run "Send scheduled reports" script immediately</p>
        <button class="ar-button green" on-click='sendScheduledReport'>{/literal}Launch{literal}</button>
        <h2 style="margin-top:10px;">System information</h2>
        <p>Server Current time: {{debug.currentTime}} {{debug.currentTimeZone}}. Reports are sent based on this time.</p>
        <p>User Current time: {{debug.userCurrentTime}} {{debug.userCurrentTimeZone}}. The list bellow shows schedule times in server time which are obtained using report owner and server timezone offsets. </p>
        <h2 style="margin-top:10px;">Scheduled reports information</h2>
        <table class="ar-table">
            <thead>
                <tr>
                    <th>Report ID</th>
                    <th>Report Name</th>
                    <th>Interval</th>
                    <th>Interval options</th>
                    <th>Time</th>
                    <th>Next Time (Server Time + Report Owner timezone)</th>
                    <th>Formats</th>
                    <th>Emails</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                {{#debug.scheduledReports}}
                <tr>
                    <td>{{id}}</td>
                    <td><a href="{/literal}{$urls.report}{literal}{{id}}">{{title}}</a></td>
                    <td>{{interval}}</td>
                    <td>{{interval_options}}</td>
                    <td>{{time}}</td>
                    <td><input type="text" value={{nexttime}} /> <button class="ar-button" on-click='updateNextTime:{{id}},{{nexttime}}'>Update</button></td>
                    <td>{{formats}}</td>
                    <td>{{emails}}</td>
                    <td><button class="ar-button" on-click='showUsers:{{id}}'>Show users</button></td>
                </tr>
                {{/}}
            </tbody>
        </table>
        <h2 style="margin-top:10px;">Server response {{#loader}}<img src="modules/AnalyticReporting/assets/css/select2-spinner.gif" alt="{/literal}{$MOD.label_loading}{literal}..." />{{/}}</h2>
        <pre>{{response}}</pre>

    </div>

</script>
{/literal}


<div id='ar-scheduler'></div>


{literal}
<script>
    //passed to reportBuilder
    var schedulerOptions = {
        //lists of predefined elements
        types:{
            {/literal}
            availableReports: {$reportNames},
            availableOwners: {$availableOwners}
            {literal}
        },
        debug:{
            {/literal}
            currentTime:'{$currentTime}',
            currentTimeZone:'{$currentTimeZone}',
            userCurrentTime:'{$userCurrentTime}',
            userCurrentTimeZone:'{$userCurrentTimeZone}',
            scheduledReports:{$scheduledReports}
            {literal}
        },
        isBasic: {/literal}{$ISBASIC}{literal}
    };
</script>
<!-- env:dev --#>
    <script src="modules/AnalyticReporting/assets/js/components/settings/scheduler.js"></script>
<!-- env:dev:end -->
<!-- env:prod -->
    <script src="modules/AnalyticReporting/assets/js/scheduler.min.js?v=2.1.327"></script>
<!-- env:prod:end -->
{/literal}

