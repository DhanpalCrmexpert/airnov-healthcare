<?php

$mod_strings['LBL_NPR'] = 'NPR';
$mod_strings['LBL_NDA'] = 'NDA';
$mod_strings['LBL_POTENTIAL_LAUNCH'] = 'Potential launch';
$mod_strings['LBL_SUCCESS_RATE'] = 'Success rate';
$mod_strings['LBL_SALES'] = 'Sales/year USD in full year';
$mod_strings['LBL_PRODUCTION_SITE'] = 'Production Site';
$mod_strings['LBL_POTENTIAL_IMPACT_USD_BUDGET_NINETEEN'] = 'Potential impact USD budget 2019';
$mod_strings['LBL_POTENTIAL_IMPACT_USD_BUDGET_TWENTY'] = 'Potential impact USD budget 2020';
$mod_strings['LBL_POTENTIAL_IMPACT_USD_BUDGET_TWENTYONE'] = 'Potential impact USD budget 2021';
$mod_strings['LBL_DESCRIPTION'] = 'Comments / info';

?>