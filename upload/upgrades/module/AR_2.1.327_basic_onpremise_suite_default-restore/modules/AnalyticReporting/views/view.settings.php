<?php

/**
 * List view
 */
class AnalyticReportingViewSettings extends SugarView {

	public function display() {
        $this->ss->assign('DATETIMENOW', date('Y-m-d H:i:s'));

        $config = new ARConfigUtils(new ARConfigService());

        $this->ss->assign('ISBASIC', $config->isBasicPlan() ? 1 : 0);

		foreach($this->view_object_map as $key => $value) {
			$this->ss->assign($key, $value);
		}

		echo $this->ss->fetch('modules/AnalyticReporting/templates/settings.tpl');
	}
}