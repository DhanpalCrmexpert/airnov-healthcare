<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

class sp_Auto_TimeSheet_Time_TrackerViewDetail extends ViewDetail
{
	public function __construct()
 	{
        parent::__construct();
	}
	
    function display()
    {
	?>
		<script>
		$(document).ready(function(){
			var parent_type =  '<?php echo $this->bean->parent_type; ?>';
			if(parent_type!='ProjectTask'){
			  $("div[field='related_project']").parent().hide();
			  $("div[field='related_project']").parent().next().hide();
			}
		});
		</script>
		<?php
		parent::display();
		global $current_user, $sugar_config, $app_list_strings;
		$user_id = $current_user->id;
		$current_user_id = $current_user->id;
	 
		//////////////////
		
		$siteUrl = $sugar_config['site_url'];
		require_once('modules/TimesheetNinja/license/OutfittersLicense.php');
		$validate_license = OutfittersLicense::isValid('TimesheetNinja');
		if($validate_license !== true) {
		if(is_admin($current_user)) {
			SugarApplication::appendErrorMessage('Timesheet is no longer active due to the following reason: '.$validate_license.' Users will have limited to no access until the issue has been addressed.');
		}
		echo '<p class="error">Timesheet is no longer active Please renew your subscription or check your license configuration.Will be redirect in seconds.....</p>';
		header("refresh:2;url=$siteUrl");
		//functionality may be altered here in response to the key failing to validate
		}
		echo "</br>";
		
	}

}
