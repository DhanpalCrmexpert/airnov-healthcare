<?php
/**
 * @author SalesAgility <info@salesagility.com>.
 */

require_once 'modules/Users/views/view.edit.php';

class CustomUsersViewEdit extends UsersViewEdit
{
    public function display()
    {
        require_once 'custom/include/SA_Outlook/Outlook.php';
        require_once 'custom/include/SA_Outlook/admin/utils.php';

        $outlook = new SalesAgility\Outlook\Outlook();

        $outlookEnabled = false;
        $outlookFolders = '';

        if($outlook->isLicenced() && $this->bean->sa_outlook_is_licensed == 1){
            $outlookEnabled = true;
            $outlookFolders = getOutlookFolders($this->bean);
        }

        $this->ss->assign('SA_OUTLOOK_ENABLE', $outlookEnabled);
        $this->ss->assign('SA_OUTLOOK_SYNC_ENABLE', $this->bean->sa_outlook_enable);
        $this->ss->assign('SA_OUTLOOK_FOLDER_OPTIONS', $outlookFolders);
        parent::display();

    }
}
