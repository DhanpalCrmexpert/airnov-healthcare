$(document).ready(function(){
	function toggleDataSeries(e) {
	if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
		e.dataSeries.visible = false;
	}
	else {
		e.dataSeries.visible = true;
	}
	chart.render();
}

	var type = urlParam('type');
	if(type == 'time_by_account'){
		call_ajax('./index.php?module=sp_Timesheet&action=visualreports&sugar_body_only=true', 'GET', {type:'time_by_account', time:true}, function(response){
			if(response != undefined && response != null){
				drawChart('Time Spent By Account', JSON.parse(response),'time_by_account');
				$('div.data').show();
			}
		});
	}
	if(type == 'time_by_cases'){
		call_ajax('./index.php?module=sp_Timesheet&action=visualreports&sugar_body_only=true', 'GET', {type:'time_by_cases', time:true}, function(response){
			if(response != undefined && response != null){
				drawChart('Support Cases By Month', JSON.parse(response),'time_by_cases');
				$('div.data').show();
			}
		});
	}
	
	//Start: Initial params//
	if($('#report-time').val()=='this_month'){
		$('#report-heading').text('Time by Employee (This Month)');
		call_ajax('./index.php?module=sp_Timesheet&action=monthly_reports&sugar_body_only=true', 'GET', {'type':'time_by_employee','time':'this_month'}, function(response){
			if(response != undefined && response != null){
				drawChart($('#report-heading').text(), JSON.parse(response),'report-time');
			}
		});

		$('div.data').show();
	};
	if($('#report-time-project').val()=='this_month'){
		$('#report-heading').text('Time on Project (This Month)');
		call_ajax('./index.php?module=sp_Timesheet&action=monthly_reports&sugar_body_only=true', 'GET', {'type':'time_by_project','time':'this_month'}, function(response){
			if(response != undefined && response != null){
				drawChart($('#report-heading').text(), JSON.parse(response),'report-time-project');
			}
		});

		$('div.data').show();
	};
if($('#report-time-cases').val()=='this_month'){
		$('#report-heading').text('Time spent on Support Cases  (This Month)');
		call_ajax('./index.php?module=sp_Timesheet&action=monthly_reports&sugar_body_only=true', 'GET', {'type':'time_by_cases','time':'this_month'}, function(response){
			if(response != undefined && response != null){
				drawChart($('#report-heading').text(), JSON.parse(response),'report-time-cases');
			}
		});

		$('div.data').show();
	};
if($('#report-time-accounts').val()=='this_month'){
	    $('#report-heading').text('Time Spent on Projects By Account (This Month)');
		call_ajax('./index.php?module=sp_Timesheet&action=monthly_reports&sugar_body_only=true', 'GET', {'type':'time_by_account','time':'this_month'}, function(response){
			if(response != undefined && response != null){
				drawChart($('#report-heading').text(), JSON.parse(response),'report-time-accounts');
			}
		});

		$('div.data').show();
	};
if($('#report-time-payroll-cost').val()=='this_month'){
			$('#report-heading').text('Payroll Cost by Employee (This Month)');
		call_ajax('./index.php?module=sp_Timesheet&action=monthly_reports&sugar_body_only=true', 'GET', {'type':'payroll_cost_by_employee','time':'this_month'}, function(response){
			if(response != undefined && response != null){
				drawChartPayroll($('#report-heading').text(), JSON.parse(response),'report-time-payroll-cost');
			}
		});

		$('div.data').show();
	};
if($('#report-time-employee-project').val()=='this_month'){
		$('#report-heading').text('Time By Employee and Project (This Month)');
		
		call_ajax('./index.php?module=sp_Timesheet&action=monthly_reports&sugar_body_only=true', 'GET', {'type':'time_by_employee_and_project','time':'this_month'}, function(response){
			if(response != undefined && response != null){
				//drawChart($('#report-heading').text(), JSON.parse(response),'report-time-employee-project');
				var chart = new CanvasJS.Chart("chart-container", {
					animationEnabled: true,
				title:{
					text: "Time by Employee & Project",
					fontWeight: "normal",
				},	
				axisY: {
						gridThickness: 0.5
				},
                   				
				toolTip: {
					shared: true
				},
				legend: {
					cursor:"pointer",
					itemclick: toggleDataSeries
				},
				data:JSON.parse(response),
				
					});
				chart.render();
			}
		});

		$('div.data').show();
	};
if($('#report-budget-by-hours-by-project').val()=='this_month'){
			$('#report-heading').text('Budgeted vs Actual Hours by Project (This Month)');
		
		call_ajax('./index.php?module=sp_Timesheet&action=monthly_reports&sugar_body_only=true', 'GET', {'type':'budget_by_hours_by_project','time':'this_month'}, function(response){
				if(response != undefined && response != null){
				//drawChart($('#report-heading').text(), JSON.parse(response),'report-budget-by-hours-by-project');
				var chart = new CanvasJS.Chart("chart-container", {
					animationEnabled: true,
				title:{
					text: "Budgeted vs Actual Hours by Project",
					fontWeight: "normal",
				},	
				axisY: {
						gridThickness: 0.5
				},
                   				
				toolTip: {
					shared: true
				},
				legend: {
					cursor:"pointer",
					itemclick: toggleDataSeries
				},
				data:JSON.parse(response),
				
					});
				chart.render();
			}
		});

		$('div.data').show();
	};

	//Start: Initial params//
	
	$('#report-time').change(function(){
		var report_time = $(this).val();
		if($(this).val() == 'this_month'){
			$('#report-heading').text('Time by Employee (This Month)');
		}else if($(this).val() == 'last_month'){
			$('#report-heading').text('Time by Employee (Last Month)');
		}else if($(this).val() == 'overall_time'){
			$('#report-heading').text('Time by Employee (Overall Time)');
		}else if($(this).val() == 'is_between'){
			$('#report-heading').text('Time by Employee (Between Selected Dates)');
		}else{
			$('div.data').hide();
			return;
		}
		call_ajax('./index.php?module=sp_Timesheet&action=visualreports&sugar_body_only=true', 'GET', {'type':'time_by_employee','time':report_time}, function(response){
			if(response != undefined && response != null){
				drawChart($('#report-heading').text(), JSON.parse(response),'report-time');
			}
		});

		$('div.data').show();
	});
	$('#report-time-project').change(function(){
		var report_time = $(this).val();
		if($(this).val() == 'this_month'){
			$('#report-heading').text('Time on Project (This Month)');
		}else if($(this).val() == 'last_month'){
			$('#report-heading').text('Time on Project (Last Month)');
		}else if($(this).val() == 'overall_time'){
			$('#report-heading').text('Time by Project (Overall Time)');
		}else{
			$('div.data').hide();
			return;
		}
		call_ajax('./index.php?module=sp_Timesheet&action=visualreports&sugar_body_only=true', 'GET', {'type':'time_by_project','time':report_time}, function(response){
			if(response != undefined && response != null){
				drawChart($('#report-heading').text(), JSON.parse(response),'report-time-project');
			}
		});

		$('div.data').show();
	});
	$('#report-time-cases').change(function(){
		var report_time = $(this).val();
		if($(this).val() == 'this_month'){
			$('#report-heading').text('Time spent on Support Cases  (This Month)');
		}else if($(this).val() == 'last_month'){
			$('#report-heading').text('Time spent on Support Cases  (Last Month)');
		}else if($(this).val() == 'overall_time'){
			$('#report-heading').text('Time spent on Support Cases (Overall Time)');
		}else{
			$('div.data').hide();
			return;
		}
		call_ajax('./index.php?module=sp_Timesheet&action=visualreports&sugar_body_only=true', 'GET', {'type':'time_by_cases','time':report_time}, function(response){
			if(response != undefined && response != null){
				drawChart($('#report-heading').text(), JSON.parse(response),'report-time-cases');
			}
		});

		$('div.data').show();
	});
	$('#report-time-accounts').change(function(){
		var report_time = $(this).val();
		if($(this).val() == 'this_month'){
			$('#report-heading').text('Time Spent on Projects By Account (This Month)');
		}else if($(this).val() == 'last_month'){
			$('#report-heading').text('Time Spent on Projects By Account (Last Month)');
		}else if($(this).val() == 'overall_time'){
			$('#report-heading').text('Time spent on Projects By Account (Overall Time)');
		}else{
			$('div.data').hide();
			return;
		}
		call_ajax('./index.php?module=sp_Timesheet&action=visualreports&sugar_body_only=true', 'GET', {'type':'time_by_account','time':report_time}, function(response){
			if(response != undefined && response != null){
				drawChart($('#report-heading').text(), JSON.parse(response),'report-time-accounts');
			}
		});

		$('div.data').show();
	});
	$('#report-time-payroll-cost').change(function(){
		var report_time = $(this).val();
		if($(this).val() == 'this_month'){
			$('#report-heading').text('Payroll Cost by Employee (This Month)');
		}else if($(this).val() == 'last_month'){
			$('#report-heading').text('Payroll Cost by Employee (Last Month)');
		}else if($(this).val() == 'overall_time'){
			$('#report-heading').text('Payroll Cost by Employee (Overall Time)');
		}else{
			$('div.data').hide();
			return;
		}
		call_ajax('./index.php?module=sp_Timesheet&action=visualreports&sugar_body_only=true', 'GET', {'type':'payroll_cost_by_employee','time':report_time}, function(response){
			if(response != undefined && response != null){
				drawChartPayroll($('#report-heading').text(), JSON.parse(response),'report-time-payroll-cost');
			}
		});

		$('div.data').show();
	});
	$('#report-time-employee-project').change(function(){
		var report_time = $(this).val();
		if($(this).val() == 'this_month'){
			$('#report-heading').text('Time By Employee and Project (This Month)');
		}else if($(this).val() == 'last_month'){
			$('#report-heading').text('Time By Employee and Project (Last Month)');
		}else if($(this).val() == 'overall_time'){
			$('#report-heading').text('Time By Employee and Project (Overall Time)');
		}else{
			$('div.data').hide();
			return;
		}
		call_ajax('./index.php?module=sp_Timesheet&action=visualreports&sugar_body_only=true', 'GET', {'type':'time_by_employee_and_project','time':report_time}, function(response){
			if(response != undefined && response != null){
				//drawChart($('#report-heading').text(), JSON.parse(response),'report-time-employee-project');
				var chart = new CanvasJS.Chart("chart-container", {
					animationEnabled: true,
				title:{
					text: "Time by Employee & Project",
					fontWeight: "normal",
				},	
				axisY: {
					gridThickness: 0.5
				},
                   				
				toolTip: {
					shared: true
				},
				legend: {
					cursor:"pointer",
					itemclick: toggleDataSeries
				},
				data:JSON.parse(response),
				
					});
				chart.render();
			}
		});

		$('div.data').show();
	});
	
	$('#report-budget-by-hours-by-project').change(function(){
		var report_time = $(this).val();
		if($(this).val() == 'this_month'){
			$('#report-heading').text('Budgeted vs Actual Hours by Project (This Month)');
		}else if($(this).val() == 'last_month'){
			$('#report-heading').text('Budgeted vs Actual Hours by Project (Last Month)');
		}else if($(this).val() == 'overall_time'){
			$('#report-heading').text('Budgeted vs Actual Hours by Project (Overall Time)');
		}else{
			$('div.data').hide();
			return;
		}
		call_ajax('./index.php?module=sp_Timesheet&action=visualreports&sugar_body_only=true', 'GET', {'type':'budget_by_hours_by_project','time':report_time}, function(response){
				if(response != undefined && response != null){
				//drawChart($('#report-heading').text(), JSON.parse(response),'report-budget-by-hours-by-project');
				var chart = new CanvasJS.Chart("chart-container", {
					animationEnabled: true,
				title:{
					text: "Budgeted vs Actual Hours by Project",
					fontWeight: "normal",
				},	
				axisY: {
					gridThickness: 0.5
				},
                   				
				toolTip: {
					shared: true
				},
				legend: {
					cursor:"pointer",
					itemclick: toggleDataSeries
				},
				data:JSON.parse(response),
				
					});
				chart.render();
			}
		});

		$('div.data').show();
	});
});
call_ajax = function(url, method, data, success_callback){
	$.ajax({
		url:url,
		type:method,
		data:data,
		success:function(response){
			if(success_callback){
				success_callback(response);
			}
		}
	});
};

drawChart = function(chart_name, response,report_name){
	var y_label = 'Total Hour(s)';
	if(report_name=='report-time-payroll-cost' || report_name=='report-budget-by-hours-by-project'){
		y_label = 'Total Amount ($)';
	}
	Highcharts.chart('chart-container', {
	    chart: {
	        type: 'column',
	        width: 900
	    },
	    title: {
	        text: ''
	    },
	    xAxis: {
	        categories: response.categories,
	        crosshair: true
	    },
	    yAxis: {
	        min: 0,
	        title: {
	            text: y_label
	        }
	    },
	    tooltip: {
	        headerFormat:'<span style="font-size:10px">{point.key}</span><table>',
	        pointFormat: '<tr><td style="color:{series.color};padding:0">'+y_label+': </td>' +
	            		 '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
	        footerFormat:'</table>',
	        shared: true,
	        useHTML: true
	    },
	    plotOptions: {
	        column: {
	            pointPadding: 0.15,
	            groupPadding: 0.15,
	            borderWidth: 0
	        }
	    },
	    series: [{
	        name: chart_name,
	        colorByPoint: true,
	        data: response.data
	    }]
	});
};


drawChartPayroll = function(chart_name, response,report_name){
	var chart = new CanvasJS.Chart("chart-container", {
	animationEnabled: true,
	theme: "light2",
	title:{
		text: ''
	},
	axisY: {
		title: "Total Amount ($)"
	},
	data: [{
		type: "column",
		yValueFormatString: "#,##0.## $",
		toolTipContent: "{label}<br>Total Amount:{total_hours[0]} $<br>Total Hours: {total_hours[1]}",
		dataPoints: response
	}]
	});
	chart.render();
};


urlParam = function(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null){
       return null;
    }
    else{
       return decodeURI(results[1]) || 0;
    }
};