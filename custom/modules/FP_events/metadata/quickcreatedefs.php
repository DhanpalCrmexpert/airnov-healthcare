<?php
$module_name = 'FP_events';
$viewdefs [$module_name] = 
array (
  'QuickCreate' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => true,
      'tabDefs' => 
      array (
        'LBL_PANEL_OVERVIEW' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
        'LBL_EMAIL_INVITE' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
      ),
    ),
    'panels' => 
    array (
      'LBL_PANEL_OVERVIEW' => 
      array (
        0 => 
        array (
          0 => 'name',
          1 => 
          array (
            'name' => 'fp_event_locations_fp_events_1_name',
            'label' => 'LBL_FP_EVENT_LOCATIONS_FP_EVENTS_1_FROM_FP_EVENT_LOCATIONS_TITLE',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'date_start',
            'comment' => 'Date of start of meeting',
            'label' => 'LBL_DATE',
          ),
          1 => 
          array (
            'name' => 'date_end',
            'comment' => 'Date meeting ends',
            'label' => 'LBL_DATE_END',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'duration',
            'comment' => 'Duration handler dropdown',
            'label' => 'LBL_DURATION',
          ),
          1 => 
          array (
            'name' => 'budget',
            'label' => 'LBL_BUDGET',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'description',
            'comment' => 'Full text of the note',
            'label' => 'LBL_DESCRIPTION',
          ),
        ),
        4 => 
        array (
          0 => 'assigned_user_name',
        ),
      ),
      'LBL_EMAIL_INVITE' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'invite_templates',
            'studio' => 'visible',
            'label' => 'LBL_INVITE_TEMPLATES',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'accept_redirect',
            'label' => 'LBL_ACCEPT_REDIRECT',
          ),
          1 => 
          array (
            'name' => 'decline_redirect',
            'label' => 'LBL_DECLINE_REDIRECT',
          ),
        ),
      ),
    ),
  ),
);
;
?>
