<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * This file is part of package Price Books.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Price Books is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
class VIPriceBooksGetProductPrice{
    public function __construct(){
    	$this->getPrice();
    } 
    //Get Product Price
    public function getPrice(){
    	$productId = $_REQUEST['productId'];
    	$productBean = BeanFactory::getBean('AOS_Products',$productId);
    	$price = $productBean->price;
        $price =  round($price,2);
        echo number_format($price, 2);
    }//end of function
}//end of class
new VIPriceBooksGetProductPrice();
?>