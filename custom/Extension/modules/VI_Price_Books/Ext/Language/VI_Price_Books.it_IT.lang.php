<?php
/*********************************************************************************
 * This file is part of package Price Books.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Price Books is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
$mod_strings['LBL_ACTIVE'] = 'Attivo';
$mod_strings['LBL_PERCENTAGE_MARK'] = 'Contrassegno percentuale';
$mod_strings['LBL_TYPE'] = 'Tipo di prezzo';
$mod_strings['LBL_ROUND_OFF'] = 'Arrotonda a';
$mod_strings['LBL_ACCOUNT_NAME'] = 'Nome account';
$mod_strings['LBL_ACCOUNT_ID'] = 'ID account';
$mod_strings['LBL_ACCOUNTS'] = 'Account';
$mod_strings['LBL_CONTACT_NAME'] = 'Nome contatto';
$mod_strings['LBL_CONTACT_ID'] = 'ID contatto';
$mod_strings['LBL_CONTACTS'] = 'Contatti';
$mod_strings['LBL_FLAT_PRICE'] = 'Prezzo forfettario';
$mod_strings['LBL_DIFFERENTIAL_PERCENTAGE'] = 'Differenziale di (%)';
$mod_strings['LBL_DIFFERENTIAL_DOLLAR'] = 'Differenziale di ($)';
$mod_strings['LBL_PRICE_BOOKS'] = 'Libri dei prezzi';
$mod_strings['LBL_NAME'] = 'Nome';
$mod_strings['LBL_PRICE_TYPE'] = 'Tipo di prezzo';
$mod_strings['LBL_ASSIGNED_TO'] = 'Assegnato a';
$mod_strings['LBL_NO_RECORDS'] = 'Nessun record trovato';
$mod_strings['LBL_MARK_UP'] = 'MarkUp (Aggiungi)';
$mod_strings['LBL_MARK_DOWN'] = 'MarkDown (Sub)';
$mod_strings['LBL_ADD_QUANTITY'] = 'Aggiungi quantità';
$mod_strings['LBL_PRODUCT_NAME'] = 'Nome prodotto';
$mod_strings['LBL_PRODUCT_PRICE'] = 'Prezzo del prodotto';
$mod_strings['LBL_LIST_PRICE'] = 'Prezzo di listino';
$mod_strings['LBL_ADD_PRODUCT'] = 'Aggiungi prodotto';
$mod_strings['LBL_YES'] = 'Sì';
$mod_strings['LBL_NO'] = 'No';
$mod_strings['LBL_SELECT_PRODUCT_MSG'] = "Seleziona il prodotto prima di fare clic sui libri dei prezzi !!!";
$mod_strings['LBL_SELECT_PRICEBOOK_MSG'] = "Seleziona solo un listino prezzi.";
$mod_strings['LBL_PRODUCT_VALIDATION'] = 'Seleziona un altro prodotto. Questo prodotto è già selezionato. ';
$mod_strings['LBL_QUANTITY_VALID_MSG'] = "Valore non valido:";
$mod_strings['LBL_FROM_QUANTITY'] = "Dalla quantità";
$mod_strings['LBL_TO_QUANTITY'] = "Alla quantità";
$mod_strings['LBL_DIFFERENTIAL'] = "Differenziale";
$mod_strings['LBL_PRICE_BOOKS_ACCESS'] = "Abilita la funzione Book Book per accedere al modulo Book Book.";
$mod_strings['LBL_CLICK_HERE'] = "Fai clic qui";
$mod_strings['LBL_ENABLE_LABEL'] = "per Abilitarlo.";
$mod_strings['LBL_FROM_QUANTITY_VALID'] = "Inserisci un valore maggiore dal valore della quantità rispetto al precedente al valore della quantità.";
$mod_strings['LBL_TO_QUANTITY_VALID'] = "Inserisci un valore maggiore per quantità rispetto a un valore quantità.";
$mod_strings['LBL_PRODUCT_LIST_PRICE_VALID'] = "Aggiungi prodotto e prezzo di listino per aggiungere quantità.";
$mod_strings['LBL_PRODCUT_PRICE_VALID'] = "Aggiungi il prezzo del prodotto per il prodotto.";
$mod_strings['LBL_LIST_PRICE_VALID'] = "Aggiungi il prezzo di listino per il prodotto.";
$mod_strings['LBL_QUANTITY_VALID_RANGE'] = "Aggiungi intervallo di quantità per prodotto.";
$mod_strings['LBL_PRODUCT_VALID'] = "Aggiungi prodotto per aggiungere quantità.";
$mod_strings['LBL_PRODUCT_FIELDS_VALIDATION'] = "Aggiungi il nome del prodotto e il prezzo di listino per i libri dei prezzi.";
$mod_strings['LBL_QUANTITY_FIELD_VALIDATION'] = "Compilare l'intervallo di quantità per lastre vuote.";
$mod_strings['LBL_EDITVIEW_PANEL1'] = "Dettagli libri prezzi";
$mod_strings['LBL_DETAILVIEW_PANEL1'] = "Dettagli libri prezzi";
?> 