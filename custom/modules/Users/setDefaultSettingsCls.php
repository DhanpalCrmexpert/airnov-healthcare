<?php
class setDefaultSettingsCls{
	function setDefaultSettingsFunc(&$bean,$event,$arguments){
		global $db,$sugar_config,$current_user,$app_list_strings;
		$timesheet_config_index = 'timesheet_module_'.$current_user->id;
		if(!isset($sugar_config[$timesheet_config_index])){
			$conf = new Configurator();
			$conf->config['timesheet_module_'.$current_user->id] = 'Project';
            $conf->config['related_module_relationship_'.$current_user->id] = 'projecttask';
			$conf->config['related_module_'.$current_user->id] = 'ProjectTask';
		    $conf->saveConfig();
		}
	}
}
?>