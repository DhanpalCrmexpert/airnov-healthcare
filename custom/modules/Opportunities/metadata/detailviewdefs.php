<?php
$viewdefs ['Opportunities'] = 
array (
  'DetailView' => 
  array (
    'templateMeta' => 
    array (
      'form' => 
      array (
        'buttons' => 
        array (
          0 => 'EDIT',
          1 => 'DUPLICATE',
          2 => 'DELETE',
          3 => 'FIND_DUPLICATES',
        ),
      ),
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => true,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
        'LBL_DETAILVIEW_PANEL1' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_PANEL_ASSIGNMENT' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
      ),
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 'name',
          1 => 'account_name',
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'amount',
            'label' => '{$MOD.LBL_AMOUNT} ({$CURRENCY})',
          ),
          1 => 'date_closed',
        ),
        2 => 
        array (
          0 => 'sales_stage',
          1 => 'opportunity_type',
        ),
        3 => 
        array (
          0 => 'probability',
          1 => 'lead_source',
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'sales_stage_amount_c',
            'label' => 'LBL_SALES_STAGE_AMOUNT',
          ),
          1 => 
          array (
            'name' => 'competitor_c',
            'studio' => 'visible',
            'label' => 'LBL_COMPETITOR',
          ),
        ),
        5 => 
        array (
          0 => 'next_step',
          1 => 'campaign_name',
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'description',
            'nl2br' => true,
          ),
        ),
        7 => 
        array (
          0 => 
          array (
            'name' => 'assigned_user_name',
            'label' => 'LBL_ASSIGNED_TO',
          ),
        ),
      ),
      'lbl_detailview_panel1' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'nda_c',
            'label' => 'LBL_NDA',
          ),
          1 => 
          array (
            'name' => 'npr_c',
            'label' => 'LBL_NPR',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'production_site_c',
            'label' => 'LBL_PRODUCTION_SITE',
          ),
          1 => 
          array (
            'name' => 'potential_launch_c',
            'label' => 'LBL_POTENTIAL_LAUNCH',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'item_description_c',
            'label' => 'LBL_ITEM_DESCRIPTION',
          ),
          1 => 
          array (
            'name' => 'quantity_c',
            'label' => 'LBL_QUANTITY_C',
          ),
        ),
        3 => 
        array (
          0 => '',
          1 => 
          array (
            'name' => 'success_rate_c',
            'label' => 'LBL_SUCCESS_RATE',
          ),
        ),
      ),
      'LBL_PANEL_ASSIGNMENT' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'date_modified',
            'label' => 'LBL_DATE_MODIFIED',
            'customCode' => '{$fields.date_modified.value} {$APP.LBL_BY} {$fields.modified_by_name.value}',
          ),
          1 => 
          array (
            'name' => 'date_entered',
            'customCode' => '{$fields.date_entered.value} {$APP.LBL_BY} {$fields.created_by_name.value}',
          ),
        ),
      ),
    ),
  ),
);
;
?>
