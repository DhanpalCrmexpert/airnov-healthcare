<?php
/**
 * CSRF validation is mandatory starting from 7.8.0.0
 *
 * It can be configured in config_override.php with
 * $sugar_config['csrf']['soft_fail_form'] = true/false
 */

$csrfData = PlatformConnector::getCSRF();
$moduleName = PlatformConnector::$moduleName;
$rootUrl = PlatformConnector::getUrl();
$siteUrl = PlatformConnector::getSiteUrl();

// Javascript Link Template should be formatted like ES6 string templates
$jsLinkTemplate = 'index.php?module=${this.module}&action=DetailView&record=${this.entityId}';
if(PlatformConnector::isSidecar()) {
    $jsLinkTemplate = $siteUrl . '/#${this.module}/${this.entityId}';
}

/**
 * SuiteCRM have no CSRF, so we should skip adding empty ampersand,
 * because no $csrfData["string"]. It will mame more cleaner URL
 */
$csrfUrlString = "";
if(!empty($csrfData["string"])) {
    $csrfUrlString = "&{$csrfData["string"]}";
}

$baseUrl = "index.php?module={$moduleName}";
$baseWithActionUrl = "{$baseUrl}&action=";

return  array(
    'moduleName' => $moduleName,
    'rootUrl' => $rootUrl,
    'jsLinkTemplate' => $jsLinkTemplate,
    'url' => array(
        'base' => "{$baseUrl}",
        'baseCSRF' => "{$baseUrl}{$csrfUrlString}",
        'index' => "{$baseWithActionUrl}listview",
        'indexCSRF' => "{$baseWithActionUrl}listview{$csrfUrlString}",
        'reportBuilder' => "{$baseWithActionUrl}reportBuilder&parenttab=simple",
        'reportBuilderCSRF' => "{$baseWithActionUrl}reportBuilder&parenttab=simple{$csrfUrlString}",
        'settings' => "{$baseWithActionUrl}settings",
        'settingsCSRF' => "{$baseWithActionUrl}settings{$csrfUrlString}",
        'withoutAction' => "{$baseWithActionUrl}",
        'withoutActionCSRF' => "{$baseUrl}{$csrfUrlString}&action=",

        'downloadTemplate' => "index.php?entryPoint=downloadTemplate&templateId=", // Using Sugar Entry Point registry

        'delete' => "{$baseWithActionUrl}Delete{$csrfUrlString}&record=",
        'deleteDashboard' => "{$baseWithActionUrl}deleteDashboard{$csrfUrlString}&record=",
        'hideReport' => "{$baseWithActionUrl}Toggle&key=report&value=0{$csrfUrlString}&record=",
        'unhideReport' => "{$baseWithActionUrl}Toggle&key=report&value=1{$csrfUrlString}&record=",
        'hideCategory' => "{$baseWithActionUrl}Toggle&key=category&value=0{$csrfUrlString}&record=",
        'unhideCategory' => "{$baseWithActionUrl}Toggle&key=category&value=1{$csrfUrlString}&record=",
        'hideDashboard' => "{$baseWithActionUrl}Toggle&key=dashboard&value=0{$csrfUrlString}&record=",
        'unhideDashboard' => "{$baseWithActionUrl}Toggle&key=dashboard&value=1{$csrfUrlString}&record=",

        'report' => "{$baseWithActionUrl}report&record=",
        'reportCSRF' => "{$baseWithActionUrl}report{$csrfUrlString}&record=",
        'reportFull' => "{$rootUrl}?module={$moduleName}&action=report{$csrfUrlString}&record=",

        'saveReport' => "{$baseWithActionUrl}saveReport",

        'makeXLS' => "{$baseWithActionUrl}makeXLS&record=",
        'makePDF' => "{$baseWithActionUrl}makePDF&record=",
        'makePDFXLS' => "{$baseWithActionUrl}makePDFXLS&record=",
        'makeCSV' => "{$baseWithActionUrl}makeCSV&record=",
        'getXLSWorksheets' => "{$baseWithActionUrl}getXLSWorksheets&templateId=",

        'addToTargetList' => "{$baseWithActionUrl}addToTargetList",
        'loadTargetLists' => "{$baseWithActionUrl}loadTargetLists",

        'reportEditUrl' => "{$baseWithActionUrl}reportBuilder&parentaction=edit&record=",
        'uploadTemplate' => "{$baseWithActionUrl}settings&currentTab=4&reportId=",

        'loadReport' => "{$baseWithActionUrl}loadReport&record=",
        'loadPicklists' => "{$baseWithActionUrl}loadPicklists",
        'loadRoles' => "{$baseWithActionUrl}getAllowedAddToDashboardRoles",
        'loadUsers' => "{$baseWithActionUrl}getAllowedUserList",
        

        'saveToDashboard' => "{$baseWithActionUrl}addToDashboard",


        'list' => "{$baseWithActionUrl}index{$csrfUrlString}",
        'dashboard' => "{$baseWithActionUrl}dashboard{$csrfUrlString}&record=",
        'dashboardFull' => "{$rootUrl}?module={$moduleName}&action=dashboard{$csrfUrlString}&record=",
        'saveCategoryUrl' => "{$baseWithActionUrl}saveCategory&to_pdf=1{$csrfUrlString}",
        'massScheduleUrl' => "{$baseWithActionUrl}massSchedule&to_pdf=1{$csrfUrlString}",
        'exportReportsUrl' => "{$baseWithActionUrl}exportReports{$csrfUrlString}",
        'importReportsUrl' => "{$baseWithActionUrl}importReports{$csrfUrlString}",
        'toggleVisible' => "{$baseWithActionUrl}index{$csrfUrlString}#toggleVisible",
        'checkFontsAjax' => "{$baseUrl}&to_pdf=1{$csrfUrlString}",
        'downloadFontCheckUrl' => "{$baseUrl}&to_pdf=1&action=downloadFontCheck{$csrfUrlString}",
        'downloadFontUrl' => "{$baseUrl}&to_pdf=1&action=downloadFont{$csrfUrlString}",
        'runSchedulerDebug' => "{$baseWithActionUrl}run_scheduler{$csrfUrlString}",
        'forceSendReport' => "{$baseWithActionUrl}force_send_report{$csrfUrlString}",
        'runDashboardSchedulerDebug' => "{$baseWithActionUrl}run_dashboard_scheduler{$csrfUrlString}",
        'forceSendDashboard' => "{$baseWithActionUrl}force_send_dashboard{$csrfUrlString}",
        'getScheduledItemUserList' => "{$baseWithActionUrl}getScheduledItemUserList{$csrfUrlString}",
        'updateScheduledNextTime' => "{$baseWithActionUrl}updateScheduledNextTime{$csrfUrlString}",
        'getLicenseUserList' => "{$baseWithActionUrl}getLicenseUserList{$csrfUrlString}",

        'userDetailView' => "{$rootUrl}?module=Users&action=DetailView{$csrfUrlString}&record=",

        // Dashboard URL's
        'saveDashboardUrl' => "{$baseWithActionUrl}saveDashboard",
        'fetchDashboardWidgetsUrl' => "{$baseWithActionUrl}getDashboardWidgets&dashboardId=",
        'fetchWidgetDataUrl' => "{$baseWithActionUrl}getWidgetData&reportId=",
        'fetchMapDataUrl' => "{$baseWithActionUrl}loadMapData",
        'fetchReportListUrl' => "{$baseWithActionUrl}getReportList&placeholder=1",
        'fetchReportTree' => "{$baseWithActionUrl}getReportTree&placeholder=1",
        'savePermissionsUrl' => "{$baseWithActionUrl}savePermissions",
        'saveScheduleUrl' => "{$baseWithActionUrl}saveSchedule",
    ),
    'dateMySqlFormat' => array(
        "microsecond" => "MICROSECOND",
        "second" => "SECOND",
        "minute" => "MINUTE",
        "hour" => "HOUR",
        "day" => "DAY",
        "week" => "WEEK",
        "month" => "MONTH",
        "quarter" => "QUARTER",
        "year" => "YEAR"
    ),
    'customSqlBlacklist' => array(
        'select ', 'insert ', 'update ', 'delete ', ' file ', 'create ', 'alter ', 'index ', 'drop ', 'temporary ', 'show ', 'view ', 'routine ', 'execute ', 'trigger ', 'grant ', 'super ', 'process ', 'reload ', 'shutdown ', 'databases ', 'lock ', 'tables ', 'references ', 'replication ', '--', 'join '
    )

);
