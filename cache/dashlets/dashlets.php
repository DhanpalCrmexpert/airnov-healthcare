<?php
// created: 2020-11-19 07:32:28
$dashletsFiles = array (
  'SurveyQuestionsDashlet' => 
  array (
    'file' => 'modules/SurveyQuestions/Dashlets/SurveyQuestionsDashlet/SurveyQuestionsDashlet.php',
    'class' => 'SurveyQuestionsDashlet',
    'meta' => 'modules/SurveyQuestions/Dashlets/SurveyQuestionsDashlet/SurveyQuestionsDashlet.meta.php',
    'module' => 'SurveyQuestions',
  ),
  'MyCasesDashlet' => 
  array (
    'file' => 'modules/Cases/Dashlets/MyCasesDashlet/MyCasesDashlet.php',
    'class' => 'MyCasesDashlet',
    'meta' => 'modules/Cases/Dashlets/MyCasesDashlet/MyCasesDashlet.meta.php',
    'module' => 'Cases',
  ),
  'FP_eventsDashlet' => 
  array (
    'file' => 'modules/FP_events/Dashlets/FP_eventsDashlet/FP_eventsDashlet.php',
    'class' => 'FP_eventsDashlet',
    'meta' => 'modules/FP_events/Dashlets/FP_eventsDashlet/FP_eventsDashlet.meta.php',
    'module' => 'FP_events',
  ),
  'SpotsDashlet' => 
  array (
    'file' => 'modules/Spots/Dashlets/SpotsDashlet/SpotsDashlet.php',
    'class' => 'SpotsDashlet',
    'meta' => 'modules/Spots/Dashlets/SpotsDashlet/SpotsDashlet.meta.php',
  ),
  'jjwg_MarkersDashlet' => 
  array (
    'file' => 'modules/jjwg_Markers/Dashlets/jjwg_MarkersDashlet/jjwg_MarkersDashlet.php',
    'class' => 'jjwg_MarkersDashlet',
    'meta' => 'modules/jjwg_Markers/Dashlets/jjwg_MarkersDashlet/jjwg_MarkersDashlet.meta.php',
    'module' => 'jjwg_Markers',
  ),
  'jjwg_MapsDashlet' => 
  array (
    'file' => 'modules/jjwg_Maps/Dashlets/jjwg_MapsDashlet/jjwg_MapsDashlet.php',
    'class' => 'jjwg_MapsDashlet',
    'meta' => 'modules/jjwg_Maps/Dashlets/jjwg_MapsDashlet/jjwg_MapsDashlet.meta.php',
    'module' => 'jjwg_Maps',
  ),
  'FavoritesDashlet' => 
  array (
    'file' => 'modules/Favorites/Dashlets/Favorites/FavoritesDashlet.php',
    'class' => 'FavoritesDashlet',
  ),
  'AORReportsDashlet' => 
  array (
    'file' => 'modules/AOR_Reports/Dashlets/AORReportsDashlet/AORReportsDashlet.php',
    'class' => 'AORReportsDashlet',
    'meta' => 'modules/AOR_Reports/Dashlets/AORReportsDashlet/AORReportsDashlet.meta.php',
    'module' => 'AOR_Reports',
  ),
  'MyProjectTaskDashlet' => 
  array (
    'file' => 'modules/ProjectTask/Dashlets/MyProjectTaskDashlet/MyProjectTaskDashlet.php',
    'class' => 'MyProjectTaskDashlet',
    'meta' => 'modules/ProjectTask/Dashlets/MyProjectTaskDashlet/MyProjectTaskDashlet.meta.php',
    'module' => 'ProjectTask',
  ),
  'AOS_PDF_TemplatesDashlet' => 
  array (
    'file' => 'modules/AOS_PDF_Templates/Dashlets/AOS_PDF_TemplatesDashlet/AOS_PDF_TemplatesDashlet.php',
    'class' => 'AOS_PDF_TemplatesDashlet',
    'meta' => 'modules/AOS_PDF_Templates/Dashlets/AOS_PDF_TemplatesDashlet/AOS_PDF_TemplatesDashlet.meta.php',
    'module' => 'AOS_PDF_Templates',
  ),
  'jjwg_AreasDashlet' => 
  array (
    'file' => 'modules/jjwg_Areas/Dashlets/jjwg_AreasDashlet/jjwg_AreasDashlet.php',
    'class' => 'jjwg_AreasDashlet',
    'meta' => 'modules/jjwg_Areas/Dashlets/jjwg_AreasDashlet/jjwg_AreasDashlet.meta.php',
    'module' => 'jjwg_Areas',
  ),
  'TopCampaignsDashlet' => 
  array (
    'file' => 'modules/Campaigns/Dashlets/TopCampaignsDashlet/TopCampaignsDashlet.php',
    'class' => 'TopCampaignsDashlet',
    'meta' => 'modules/Campaigns/Dashlets/TopCampaignsDashlet/TopCampaignsDashlet.meta.php',
    'module' => 'Campaigns',
  ),
  'MyProjectDashlet' => 
  array (
    'file' => 'modules/Project/Dashlets/MyProjectDashlet/MyProjectDashlet.php',
    'class' => 'MyProjectDashlet',
    'meta' => 'modules/Project/Dashlets/MyProjectDashlet/MyProjectDashlet.meta.php',
    'module' => 'Project',
  ),
  'AM_TaskTemplatesDashlet' => 
  array (
    'file' => 'modules/AM_TaskTemplates/Dashlets/AM_TaskTemplatesDashlet/AM_TaskTemplatesDashlet.php',
    'class' => 'AM_TaskTemplatesDashlet',
    'meta' => 'modules/AM_TaskTemplates/Dashlets/AM_TaskTemplatesDashlet/AM_TaskTemplatesDashlet.meta.php',
    'module' => 'AM_TaskTemplates',
  ),
  'MyBugsDashlet' => 
  array (
    'file' => 'modules/Bugs/Dashlets/MyBugsDashlet/MyBugsDashlet.php',
    'class' => 'MyBugsDashlet',
    'meta' => 'modules/Bugs/Dashlets/MyBugsDashlet/MyBugsDashlet.meta.php',
    'module' => 'Bugs',
  ),
  'AOK_KnowledgeBaseDashlet' => 
  array (
    'file' => 'modules/AOK_KnowledgeBase/Dashlets/AOK_KnowledgeBaseDashlet/AOK_KnowledgeBaseDashlet.php',
    'class' => 'AOK_KnowledgeBaseDashlet',
    'meta' => 'modules/AOK_KnowledgeBase/Dashlets/AOK_KnowledgeBaseDashlet/AOK_KnowledgeBaseDashlet.meta.php',
    'module' => 'AOK_KnowledgeBase',
  ),
  'AM_ProjectTemplatesDashlet' => 
  array (
    'file' => 'modules/AM_ProjectTemplates/Dashlets/AM_ProjectTemplatesDashlet/AM_ProjectTemplatesDashlet.php',
    'class' => 'AM_ProjectTemplatesDashlet',
    'meta' => 'modules/AM_ProjectTemplates/Dashlets/AM_ProjectTemplatesDashlet/AM_ProjectTemplatesDashlet.meta.php',
    'module' => 'AM_ProjectTemplates',
  ),
  'WidgetDashlet' => 
  array (
    'file' => 'modules/AnalyticReporting/Dashlets/WidgetDashlet/WidgetDashlet.php',
    'class' => 'WidgetDashlet',
    'meta' => 'modules/AnalyticReporting/Dashlets/WidgetDashlet/WidgetDashlet.meta.php',
    'module' => 'AnalyticReporting',
  ),
  'MyLeadsDashlet' => 
  array (
    'file' => 'modules/Leads/Dashlets/MyLeadsDashlet/MyLeadsDashlet.php',
    'class' => 'MyLeadsDashlet',
    'meta' => 'modules/Leads/Dashlets/MyLeadsDashlet/MyLeadsDashlet.meta.php',
    'module' => 'Leads',
  ),
  'suh_timer_recordingDashlet' => 
  array (
    'file' => 'modules/suh_timer_recording/Dashlets/suh_timer_recordingDashlet/suh_timer_recordingDashlet.php',
    'class' => 'suh_timer_recordingDashlet',
    'meta' => 'modules/suh_timer_recording/Dashlets/suh_timer_recordingDashlet/suh_timer_recordingDashlet.meta.php',
    'module' => 'suh_timer_recording',
  ),
  'SurveyQuestionResponsesDashlet' => 
  array (
    'file' => 'modules/SurveyQuestionResponses/Dashlets/SurveyQuestionResponsesDashlet/SurveyQuestionResponsesDashlet.php',
    'class' => 'SurveyQuestionResponsesDashlet',
    'meta' => 'modules/SurveyQuestionResponses/Dashlets/SurveyQuestionResponsesDashlet/SurveyQuestionResponsesDashlet.meta.php',
    'module' => 'SurveyQuestionResponses',
  ),
  'MyMeetingsDashlet' => 
  array (
    'file' => 'modules/Meetings/Dashlets/MyMeetingsDashlet/MyMeetingsDashlet.php',
    'class' => 'MyMeetingsDashlet',
    'meta' => 'modules/Meetings/Dashlets/MyMeetingsDashlet/MyMeetingsDashlet.meta.php',
    'module' => 'Meetings',
  ),
  'ChartsDashlet' => 
  array (
    'file' => 'modules/Home/Dashlets/ChartsDashlet/ChartsDashlet.php',
    'class' => 'ChartsDashlet',
    'meta' => 'modules/Home/Dashlets/ChartsDashlet/ChartsDashlet.meta.php',
  ),
  'RSSDashlet' => 
  array (
    'file' => 'modules/Home/Dashlets/RSSDashlet/RSSDashlet.php',
    'class' => 'RSSDashlet',
    'meta' => 'modules/Home/Dashlets/RSSDashlet/RSSDashlet.meta.php',
    'icon' => 'modules/Home/Dashlets/RSSDashlet/RSSDashlet.icon.jpg',
  ),
  'SugarNewsDashlet' => 
  array (
    'file' => 'modules/Home/Dashlets/SugarNewsDashlet/SugarNewsDashlet.php',
    'class' => 'SugarNewsDashlet',
    'meta' => 'modules/Home/Dashlets/SugarNewsDashlet/SugarNewsDashlet.meta.php',
    'module' => 'Home',
  ),
  'JotPadDashlet' => 
  array (
    'file' => 'modules/Home/Dashlets/JotPadDashlet/JotPadDashlet.php',
    'class' => 'JotPadDashlet',
    'meta' => 'modules/Home/Dashlets/JotPadDashlet/JotPadDashlet.meta.php',
  ),
  'iFrameDashlet' => 
  array (
    'file' => 'modules/Home/Dashlets/iFrameDashlet/iFrameDashlet.php',
    'class' => 'iFrameDashlet',
    'meta' => 'modules/Home/Dashlets/iFrameDashlet/iFrameDashlet.meta.php',
    'module' => 'Home',
  ),
  'InvadersDashlet' => 
  array (
    'file' => 'modules/Home/Dashlets/InvadersDashlet/InvadersDashlet.php',
    'class' => 'InvadersDashlet',
    'meta' => 'modules/Home/Dashlets/InvadersDashlet/InvadersDashlet.meta.php',
    'icon' => 'modules/Home/Dashlets/InvadersDashlet/InvadersDashlet.icon.jpg',
  ),
  'MyOpportunitiesDashlet' => 
  array (
    'file' => 'modules/Opportunities/Dashlets/MyOpportunitiesDashlet/MyOpportunitiesDashlet.php',
    'class' => 'MyOpportunitiesDashlet',
    'meta' => 'modules/Opportunities/Dashlets/MyOpportunitiesDashlet/MyOpportunitiesDashlet.meta.php',
    'module' => 'Opportunities',
  ),
  'MyClosedOpportunitiesDashlet' => 
  array (
    'file' => 'modules/Opportunities/Dashlets/MyClosedOpportunitiesDashlet/MyClosedOpportunitiesDashlet.php',
    'class' => 'MyClosedOpportunitiesDashlet',
    'meta' => 'modules/Opportunities/Dashlets/MyClosedOpportunitiesDashlet/MyClosedOpportunitiesDashlet.meta.php',
    'module' => 'Opportunities',
  ),
  'FP_Event_LocationsDashlet' => 
  array (
    'file' => 'modules/FP_Event_Locations/Dashlets/FP_Event_LocationsDashlet/FP_Event_LocationsDashlet.php',
    'class' => 'FP_Event_LocationsDashlet',
    'meta' => 'modules/FP_Event_Locations/Dashlets/FP_Event_LocationsDashlet/FP_Event_LocationsDashlet.meta.php',
    'module' => 'FP_Event_Locations',
  ),
  'MyCallsDashlet' => 
  array (
    'file' => 'modules/Calls/Dashlets/MyCallsDashlet/MyCallsDashlet.php',
    'class' => 'MyCallsDashlet',
    'meta' => 'modules/Calls/Dashlets/MyCallsDashlet/MyCallsDashlet.meta.php',
    'module' => 'Calls',
  ),
  'SurveysDashlet' => 
  array (
    'file' => 'modules/Surveys/Dashlets/SurveysDashlet/SurveysDashlet.php',
    'class' => 'SurveysDashlet',
    'meta' => 'modules/Surveys/Dashlets/SurveysDashlet/SurveysDashlet.meta.php',
    'module' => 'Surveys',
  ),
  'MyEmailsDashlet' => 
  array (
    'file' => 'modules/Emails/Dashlets/MyEmailsDashlet/MyEmailsDashlet.php',
    'class' => 'MyEmailsDashlet',
    'meta' => 'modules/Emails/Dashlets/MyEmailsDashlet/MyEmailsDashlet.meta.php',
    'module' => 'Emails',
  ),
  'SugarFeedDashlet' => 
  array (
    'file' => 'modules/SugarFeed/Dashlets/SugarFeedDashlet/SugarFeedDashlet.php',
    'class' => 'SugarFeedDashlet',
    'meta' => 'modules/SugarFeed/Dashlets/SugarFeedDashlet/SugarFeedDashlet.meta.php',
    'module' => 'SugarFeed',
  ),
  'MyTasksDashlet' => 
  array (
    'file' => 'modules/Tasks/Dashlets/MyTasksDashlet/MyTasksDashlet.php',
    'class' => 'MyTasksDashlet',
    'meta' => 'modules/Tasks/Dashlets/MyTasksDashlet/MyTasksDashlet.meta.php',
    'module' => 'Tasks',
  ),
  'sp_timesheet_trackerDashlet' => 
  array (
    'file' => 'modules/sp_timesheet_tracker/Dashlets/sp_timesheet_trackerDashlet/sp_timesheet_trackerDashlet.php',
    'class' => 'sp_timesheet_trackerDashlet',
    'meta' => 'modules/sp_timesheet_tracker/Dashlets/sp_timesheet_trackerDashlet/sp_timesheet_trackerDashlet.meta.php',
    'module' => 'sp_timesheet_tracker',
  ),
  'AOW_ProcessedDashlet' => 
  array (
    'file' => 'modules/AOW_Processed/Dashlets/AOW_ProcessedDashlet/AOW_ProcessedDashlet.php',
    'class' => 'AOW_ProcessedDashlet',
    'meta' => 'modules/AOW_Processed/Dashlets/AOW_ProcessedDashlet/AOW_ProcessedDashlet.meta.php',
    'module' => 'AOW_Processed',
  ),
  'AOK_Knowledge_Base_CategoriesDashlet' => 
  array (
    'file' => 'modules/AOK_Knowledge_Base_Categories/Dashlets/AOK_Knowledge_Base_CategoriesDashlet/AOK_Knowledge_Base_CategoriesDashlet.php',
    'class' => 'AOK_Knowledge_Base_CategoriesDashlet',
    'meta' => 'modules/AOK_Knowledge_Base_Categories/Dashlets/AOK_Knowledge_Base_CategoriesDashlet/AOK_Knowledge_Base_CategoriesDashlet.meta.php',
    'module' => 'AOK_Knowledge_Base_Categories',
  ),
  'MyDocumentsDashlet' => 
  array (
    'file' => 'modules/Documents/Dashlets/MyDocumentsDashlet/MyDocumentsDashlet.php',
    'class' => 'MyDocumentsDashlet',
    'meta' => 'modules/Documents/Dashlets/MyDocumentsDashlet/MyDocumentsDashlet.meta.php',
    'module' => 'Documents',
  ),
  'OutboundEmailAccountsDashlet' => 
  array (
    'file' => 'modules/OutboundEmailAccounts/Dashlets/OutboundEmailAccountsDashlet/OutboundEmailAccountsDashlet.php',
    'class' => 'OutboundEmailAccountsDashlet',
    'meta' => 'modules/OutboundEmailAccounts/Dashlets/OutboundEmailAccountsDashlet/OutboundEmailAccountsDashlet.meta.php',
    'module' => 'OutboundEmailAccounts',
  ),
  'MyAccountsDashlet' => 
  array (
    'file' => 'modules/Accounts/Dashlets/MyAccountsDashlet/MyAccountsDashlet.php',
    'class' => 'MyAccountsDashlet',
    'meta' => 'modules/Accounts/Dashlets/MyAccountsDashlet/MyAccountsDashlet.meta.php',
    'module' => 'Accounts',
  ),
  'sp_Auto_TimeSheet_Time_TrackerDashlet' => 
  array (
    'file' => 'modules/sp_Auto_TimeSheet_Time_Tracker/Dashlets/sp_Auto_TimeSheet_Time_TrackerDashlet/sp_Auto_TimeSheet_Time_TrackerDashlet.php',
    'class' => 'sp_Auto_TimeSheet_Time_TrackerDashlet',
    'meta' => 'modules/sp_Auto_TimeSheet_Time_Tracker/Dashlets/sp_Auto_TimeSheet_Time_TrackerDashlet/sp_Auto_TimeSheet_Time_TrackerDashlet.meta.php',
    'module' => 'sp_Auto_TimeSheet_Time_Tracker',
  ),
  'MyPipelineBySalesStageDashlet' => 
  array (
    'file' => 'modules/Charts/Dashlets/MyPipelineBySalesStageDashlet/MyPipelineBySalesStageDashlet.php',
    'class' => 'MyPipelineBySalesStageDashlet',
    'meta' => 'modules/Charts/Dashlets/MyPipelineBySalesStageDashlet/MyPipelineBySalesStageDashlet.meta.php',
    'module' => 'Opportunities',
  ),
  'CampaignROIChartDashlet' => 
  array (
    'file' => 'modules/Charts/Dashlets/CampaignROIChartDashlet/CampaignROIChartDashlet.php',
    'class' => 'CampaignROIChartDashlet',
    'meta' => 'modules/Charts/Dashlets/CampaignROIChartDashlet/CampaignROIChartDashlet.meta.php',
    'module' => 'Campaigns',
  ),
  'OutcomeByMonthDashlet' => 
  array (
    'file' => 'modules/Charts/Dashlets/OutcomeByMonthDashlet/OutcomeByMonthDashlet.php',
    'class' => 'OutcomeByMonthDashlet',
    'meta' => 'modules/Charts/Dashlets/OutcomeByMonthDashlet/OutcomeByMonthDashlet.meta.php',
    'module' => 'Opportunities',
  ),
  'PipelineBySalesStageDashlet' => 
  array (
    'file' => 'modules/Charts/Dashlets/PipelineBySalesStageDashlet/PipelineBySalesStageDashlet.php',
    'class' => 'PipelineBySalesStageDashlet',
    'meta' => 'modules/Charts/Dashlets/PipelineBySalesStageDashlet/PipelineBySalesStageDashlet.meta.php',
    'module' => 'Opportunities',
  ),
  'OpportunitiesByLeadSourceByOutcomeDashlet' => 
  array (
    'file' => 'modules/Charts/Dashlets/OpportunitiesByLeadSourceByOutcomeDashlet/OpportunitiesByLeadSourceByOutcomeDashlet.php',
    'class' => 'OpportunitiesByLeadSourceByOutcomeDashlet',
    'meta' => 'modules/Charts/Dashlets/OpportunitiesByLeadSourceByOutcomeDashlet/OpportunitiesByLeadSourceByOutcomeDashlet.meta.php',
    'module' => 'Opportunities',
  ),
  'OpportunitiesByLeadSourceDashlet' => 
  array (
    'file' => 'modules/Charts/Dashlets/OpportunitiesByLeadSourceDashlet/OpportunitiesByLeadSourceDashlet.php',
    'class' => 'OpportunitiesByLeadSourceDashlet',
    'meta' => 'modules/Charts/Dashlets/OpportunitiesByLeadSourceDashlet/OpportunitiesByLeadSourceDashlet.meta.php',
    'module' => 'Opportunities',
  ),
  'sg_SendGrid_EventDashlet' => 
  array (
    'file' => 'modules/sg_SendGrid_Event/Dashlets/sg_SendGrid_EventDashlet/sg_SendGrid_EventDashlet.php',
    'class' => 'sg_SendGrid_EventDashlet',
    'meta' => 'modules/sg_SendGrid_Event/Dashlets/sg_SendGrid_EventDashlet/sg_SendGrid_EventDashlet.meta.php',
    'module' => 'sg_SendGrid_Event',
  ),
  'CalendarDashlet' => 
  array (
    'file' => 'modules/Calendar/Dashlets/CalendarDashlet/CalendarDashlet.php',
    'class' => 'CalendarDashlet',
    'meta' => 'modules/Calendar/Dashlets/CalendarDashlet/CalendarDashlet.meta.php',
    'module' => 'Calendar',
  ),
  'SurveyResponsesDashlet' => 
  array (
    'file' => 'modules/SurveyResponses/Dashlets/SurveyResponsesDashlet/SurveyResponsesDashlet.php',
    'class' => 'SurveyResponsesDashlet',
    'meta' => 'modules/SurveyResponses/Dashlets/SurveyResponsesDashlet/SurveyResponsesDashlet.meta.php',
    'module' => 'SurveyResponses',
  ),
  'AOS_QuotesDashlet' => 
  array (
    'file' => 'modules/AOS_Quotes/Dashlets/AOS_QuotesDashlet/AOS_QuotesDashlet.php',
    'class' => 'AOS_QuotesDashlet',
    'meta' => 'modules/AOS_Quotes/Dashlets/AOS_QuotesDashlet/AOS_QuotesDashlet.meta.php',
    'module' => 'AOS_Quotes',
  ),
  'MyNotesDashlet' => 
  array (
    'file' => 'modules/Notes/Dashlets/MyNotesDashlet/MyNotesDashlet.php',
    'class' => 'MyNotesDashlet',
    'meta' => 'modules/Notes/Dashlets/MyNotesDashlet/MyNotesDashlet.meta.php',
    'module' => 'Notes',
  ),
  'AOS_InvoicesDashlet' => 
  array (
    'file' => 'modules/AOS_Invoices/Dashlets/AOS_InvoicesDashlet/AOS_InvoicesDashlet.php',
    'class' => 'AOS_InvoicesDashlet',
    'meta' => 'modules/AOS_Invoices/Dashlets/AOS_InvoicesDashlet/AOS_InvoicesDashlet.meta.php',
    'module' => 'AOS_Invoices',
  ),
  'sp_Timeoff_TimeCardDashlet' => 
  array (
    'file' => 'modules/sp_Timeoff_TimeCard/Dashlets/sp_Timeoff_TimeCardDashlet/sp_Timeoff_TimeCardDashlet.php',
    'class' => 'sp_Timeoff_TimeCardDashlet',
    'meta' => 'modules/sp_Timeoff_TimeCard/Dashlets/sp_Timeoff_TimeCardDashlet/sp_Timeoff_TimeCardDashlet.meta.php',
    'module' => 'sp_Timeoff_TimeCard',
  ),
  'VI_Price_BooksDashlet' => 
  array (
    'file' => 'modules/VI_Price_Books/Dashlets/VI_Price_BooksDashlet/VI_Price_BooksDashlet.php',
    'class' => 'VI_Price_BooksDashlet',
    'meta' => 'modules/VI_Price_Books/Dashlets/VI_Price_BooksDashlet/VI_Price_BooksDashlet.meta.php',
    'module' => 'VI_Price_Books',
  ),
  'sp_TimesheetDashlet' => 
  array (
    'file' => 'modules/sp_Timesheet/Dashlets/sp_TimesheetDashlet/sp_TimesheetDashlet.php',
    'class' => 'sp_TimesheetDashlet',
    'meta' => 'modules/sp_Timesheet/Dashlets/sp_TimesheetDashlet/sp_TimesheetDashlet.meta.php',
    'module' => 'sp_Timesheet',
  ),
  'ahc_Key_AccountsDashlet' => 
  array (
    'file' => 'modules/ahc_Key_Accounts/Dashlets/ahc_Key_AccountsDashlet/ahc_Key_AccountsDashlet.php',
    'class' => 'ahc_Key_AccountsDashlet',
    'meta' => 'modules/ahc_Key_Accounts/Dashlets/ahc_Key_AccountsDashlet/ahc_Key_AccountsDashlet.meta.php',
    'module' => 'ahc_Key_Accounts',
  ),
  'SurveyQuestionOptionsDashlet' => 
  array (
    'file' => 'modules/SurveyQuestionOptions/Dashlets/SurveyQuestionOptionsDashlet/SurveyQuestionOptionsDashlet.php',
    'class' => 'SurveyQuestionOptionsDashlet',
    'meta' => 'modules/SurveyQuestionOptions/Dashlets/SurveyQuestionOptionsDashlet/SurveyQuestionOptionsDashlet.meta.php',
    'module' => 'SurveyQuestionOptions',
  ),
  'MyContactsDashlet' => 
  array (
    'file' => 'modules/Contacts/Dashlets/MyContactsDashlet/MyContactsDashlet.php',
    'class' => 'MyContactsDashlet',
    'meta' => 'modules/Contacts/Dashlets/MyContactsDashlet/MyContactsDashlet.meta.php',
    'module' => 'Contacts',
  ),
  'AOS_Product_CategoriesDashlet' => 
  array (
    'file' => 'modules/AOS_Product_Categories/Dashlets/AOS_Product_CategoriesDashlet/AOS_Product_CategoriesDashlet.php',
    'class' => 'AOS_Product_CategoriesDashlet',
    'meta' => 'modules/AOS_Product_Categories/Dashlets/AOS_Product_CategoriesDashlet/AOS_Product_CategoriesDashlet.meta.php',
    'module' => 'AOS_Product_Categories',
  ),
  'AOW_WorkFlowDashlet' => 
  array (
    'file' => 'modules/AOW_WorkFlow/Dashlets/AOW_WorkFlowDashlet/AOW_WorkFlowDashlet.php',
    'class' => 'AOW_WorkFlowDashlet',
    'meta' => 'modules/AOW_WorkFlow/Dashlets/AOW_WorkFlowDashlet/AOW_WorkFlowDashlet.meta.php',
    'module' => 'AOW_WorkFlow',
  ),
  'AOS_ProductsDashlet' => 
  array (
    'file' => 'modules/AOS_Products/Dashlets/AOS_ProductsDashlet/AOS_ProductsDashlet.php',
    'class' => 'AOS_ProductsDashlet',
    'meta' => 'modules/AOS_Products/Dashlets/AOS_ProductsDashlet/AOS_ProductsDashlet.meta.php',
    'module' => 'AOS_Products',
  ),
  'AOS_ContractsDashlet' => 
  array (
    'file' => 'modules/AOS_Contracts/Dashlets/AOS_ContractsDashlet/AOS_ContractsDashlet.php',
    'class' => 'AOS_ContractsDashlet',
    'meta' => 'modules/AOS_Contracts/Dashlets/AOS_ContractsDashlet/AOS_ContractsDashlet.meta.php',
    'module' => 'AOS_Contracts',
  ),
);