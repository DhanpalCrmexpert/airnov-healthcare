<?php

$dictionary['User']['fields']['sa_outlook_sync_date'] = array(
    'name' => 'sa_outlook_sync_date',
    'vname' => 'LBL_SA_OUTLOOK_SYNC_DATE',
    'type' => 'datetime',
    'size' => '20',
    'required' => false,
    'audited' => true,
    'reportable' => true,
    'importable' => true,
    'unified_search' => false,
    'duplicate_merge' => 'true',
    'inline_edit' => true
);
$dictionary['User']['fields']['sa_outlook_folders'] = array(
    'name' => 'sa_outlook_folders',
    'vname' => 'LBL_SA_OUTLOOK_USER_FOLDERS',
    'type' => 'multienum',
    'options' => 'sa_outlook_folders_options',
    'function' => array('name'=>'getOutlookFolders', 'returns'=>'html', 'include'=>'custom/include/SA_Outlook/admin/utils.php'),
);

$dictionary['User']['fields']['sa_outlook_enable'] = array(
    'name' => 'sa_outlook_enable',
    'vname' => 'LBL_SA_OUTLOOK_USER_ENABLE',
    'type' => 'bool',
);

$dictionary['User']['fields']['sa_outlook_is_licensed'] = array(
    'name' => 'sa_outlook_is_licensed',
    'vname' => 'LBL_SA_OUTLOOK_USER_IS_LICENSED',
    'type' => 'bool',
);
