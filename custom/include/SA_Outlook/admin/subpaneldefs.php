<?php
/**
 * Copyright (c) SalesAgility Ltd.  All Rights Reserved.
 * Licensed under the GPL3 License.  See License in the project root
 * for license information.
 *
 * @package Outlook Addin for SuiteCRM
 * @copyright 2020 SalesAgility Ltd http://www.salesagility.com
 * @author SalesAgility <info@salesagility.com
 *
 */

$layout_defs['OutlookAdmin'] = [
    'subpanel_setup' => [

        'users' => [
            'order' => 1,
            'module' => 'Users',
            'subpanel_name' => 'default',
            'get_subpanel_data' => 'function:getOutlookUsers',
            'function_parameters' => [
                'import_function_file' => 'custom/include/SA_Outlook/admin/utils.php',
            ],
            'title_key' => 'LBL_OUTLOOK_USERS_SUBPANEL_TITLE',
            'top_buttons' => [
                [
                    'widget_class' => 'SubPanelTopSelectButton',
                    'popup_module' => 'Users',
                    'mode' => 'MultiSelect'
                ],
            ],
        ],
    ],
];
