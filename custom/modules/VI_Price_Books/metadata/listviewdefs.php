<?php
$module_name = 'VI_Price_Books';
$listViewDefs[$module_name] = 
array (
  'NAME' => 
  array (
    'width' => '32%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
  ),
  'ACTIVE_C' => 
  array (
    'type' => 'bool',
    'label' => 'LBL_ACTIVE',
    'width' => '10%',
    'default' => true,
  ),
  'ACCOUNT_NAME' => 
  array (
    'width' => '10%',
    'label' => 'LBL_ACCOUNT_NAME',
    'module' => 'Accounts',
    'id' => 'ACCOUNT_ID',
    'type' => 'relate',
    'link' => true,
    'default' => true,
    'ACLTag' => 'ACCOUNT',
    'related_fields' => array('account_id')
  ),

  'CONTACT_NAME' => 
  array (
    'width' => '10%',
    'label' => 'LBL_CONTACT_NAME',
    'module' => 'Contacts',
    'id' => 'CONTACT_ID',
    'type' => 'relate',
    'link' => true,
    'default' => true,
    'ACLTag' => 'CONTACT',
    'related_fields' => array('contact_id')
  ),
  'ASSIGNED_USER_NAME' => 
  array (
    'width' => '9%',
    'label' => 'LBL_ASSIGNED_TO_NAME',
    'module' => 'Employees',
    'id' => 'ASSIGNED_USER_ID',
    'default' => true,
  ),
  'DATE_ENTERED' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_DATE_ENTERED',
    'width' => '10%',
    'default' => true,
  ),
  'DATE_MODIFIED' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_DATE_MODIFIED',
    'width' => '10%',
    'default' => true,
  )
);
?>
