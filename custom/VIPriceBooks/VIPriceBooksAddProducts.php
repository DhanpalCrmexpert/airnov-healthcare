<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * This file is part of package Price Books.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Price Books is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
require_once('custom/VIPriceBooks/VIPriceBooksFunction.php');
class VIPriceBooksAddProducts{
    public function __construct(){
    	$this->add_products();
    } 
    public function add_products(){
    	global $theme;
    	$recordId = $_REQUEST['recordId'];
    	$module = 'AOS_Products';
 
    	$html = '';
    	if($recordId == "" && $_REQUEST['onLoad'] == "1"){
    		$html .= addProductTable();
    	}else if($recordId == "" && $_REQUEST['onLoad'] == "0"){
    		$productRow = $_REQUEST['productRow'];
    		$count = $_REQUEST['count'];
			if($productRow == 0){
            	$html .= '<tr id="product_header">
                    <th style="padding-right:141px !important;">'.translate('LBL_PRODUCT_NAME','VI_Price_Books').'</th>
                    <th style="padding-right:82px !important;">'.translate('LBL_PRODUCT_PRICE','VI_Price_Books').'</th>
                    <th style="padding-right:86px !important;">'.translate('LBL_LIST_PRICE','VI_Price_Books').'</th>
                    <th style="padding-right:90px !important;">'.translate('LBL_TYPE','VI_Price_Books').'</th>
                    <th style="width:12%;padding-right: 52px !important;" id="percentage_mark"></th>
            		<th style="width:11%;padding-right: 4px !important;" id="round_off"></th>
            	</tr>';
        	}
        	$html .= '<tr id="productLine'.$count.'">
                            <td>
                                <input class="sqsEnabled product_name" autocomplete="off" type="text" name="product_name'.$count.'" id="product_name'.$count.'" maxlength="50"  title="" tabindex="116" value="">
                                <input type="hidden" name="product_product_id'.$count.'" id="product_product_id'.$count.'"  maxlength="50" value="">
                                <span class="id-ff multiple">
                                    <button type="button" name="btn_relate" id="productButton" tabindex="0" class="button firstChild" onclick="openModulePopup('."'".$module."'".','.$count.');">
                                        <img src="themes/default/images/id-ff-select.png?v=mTmf-1lP7RIiI-bMI3e72w">
                                    </button>';
                                    $product_name = 'product_name'.$count;
					                $product_product_id = 'product_product_id'.$count;

                                    $html .= '<button type="button" name="" id="productButton" tabindex="0" class="button lastChild " onclick="SUGAR.clearRelateField(this.form,'."'".$product_name."'".','."'".$product_product_id."'".'); removeRelateValue('."'".$count."'".');" value="">
                                        <img src="themes/default/images/id-ff-clear.png?v=mTmf-1lP7RIiI-bMI3e72w">
                                    </button>
                                </span>
                            </td>
                            <td>
                                <input class="column" type="text" name="product_price'.$count.'" id="product_price'.$count.'" value="" readonly>
                            </td>
                            <td>
                                <input class="column" type="text" name="list_price'.$count.'" id="list_price'.$count.'" label="'.translate('LBL_LIST_PRICE','VI_Price_Books').'" value="" onfocusout = "checkQuantityValueType(this,'."'".$count."'".')" readonly>
                            </td>
                            <td id="priceType">
                                <select name="price_type'.$count.'" id="price_type'.$count.'" class="column" onchange="changePriceType(this,'.$count.')">
                                    <option value="1">'.translate('LBL_FLAT_PRICE','VI_Price_Books').'</option>
                                    <option value="2">'.translate('LBL_DIFFERENTIAL_PERCENTAGE','VI_Price_Books').'</option>
                                    <option value="3">'.translate('LBL_DIFFERENTIAL_DOLLAR','VI_Price_Books').'</option>
                                </select>
                            </td>
                            <td class="minusButton"><button type="button" class="button btn_minus" onclick="removeProduct(this,'.$count.')" name="remove">-</button></td>
                        </tr>';

    	}else if($recordId != ""){
    		$action = $_REQUEST['action'];

    		$tablename = 'vi_pricebook_productentry';
    		$fieldNames = array("*");
    		$where = array('pricebooks_id' => $recordId);
			$getProductQuery = getPriceBooksRecord($tablename,$fieldNames,$where);  
    		$getProductResult = $GLOBALS['db']->query($getProductQuery);
			$getProductRows = $GLOBALS['db']->fetchByAssoc($getProductResult);
		    $productEntryData = json_decode(html_entity_decode($getProductRows['productentry']));
			
			$priceType = array();
			if(!empty($productEntryData)){
				foreach($productEntryData as $key=>$productValues){
					foreach($productValues as $key=>$values){
						$productData = (array)$values;
						$priceType[] = $productData['price_type'];
					}
				}
			}
			
			if(!empty($productEntryData)){
				if($action == "EditView"){
					$html .= '<table id="productTable">
								<input type="hidden" name="productEntryCount" id="productEntryCount" value="'.count($productEntryData).'"/>
								<tr id="product_header">
									<th style="padding-right:141px !important;">'.translate('LBL_PRODUCT_NAME','VI_Price_Books').'</th>
									<th style="padding-right:82px !important;">'.translate('LBL_PRODUCT_PRICE','VI_Price_Books').'</th>
									<th style="padding-right:86px !important;">'.translate('LBL_LIST_PRICE','VI_Price_Books').'</th>
									<th style="padding-right:90px !important;">'.translate('LBL_TYPE','VI_Price_Books').'</th>';
								if((in_array("1",$priceType) && in_array("2",$priceType) && in_array("3",$priceType)) || (in_array("1",$priceType) && in_array("2",$priceType)) || (in_array("1",$priceType) && in_array("3",$priceType)) || (in_array("2",$priceType) && in_array("3",$priceType)) || (in_array("2",$priceType)) || (in_array("3",$priceType))){
									$html .= '<th style="padding-right: 52px !important;" id="percentage_mark" style="width:12%;">'.translate('LBL_PERCENTAGE_MARK','VI_Price_Books').'</th>
						            <th style="padding-right: 4px;" id="round_off" style="width:11%;">'.translate('LBL_ROUND_OFF','VI_Price_Books').'</th>';
				        		}else{
				        			$html .= '<th style="padding-right: 52px !important;"  id="percentage_mark" style="width:12%;"></th>
						            <th style="padding-right: 4px;" id="round_off" style="width:11%;"></th>';
				        		}
						$html .= '</tr>';
				}elseif($action == "DetailView"){
					$html .= '<table id="productTable">
								<input type="hidden" name="productEntryCount" id="productEntryCount" value="'.count($productEntryData).'"/>
								<tr id="product_header">
									<th ';
									if($theme == "SuiteR"){
										$html .= 'class="productNameLabel"';
									}
									$html .= '>'.translate('LBL_PRODUCT_NAME','VI_Price_Books').'</th>
									<th ';
									if($theme == "SuiteR"){
										$html .= 'class="priceLabel"';
									}
									$html .= '>'.translate('LBL_PRODUCT_PRICE','VI_Price_Books').'</th>
									<th ';
									if($theme == "SuiteR"){
										$html .= 'style="padding-left: 38px;"';
									}
									$html .= '>'.translate('LBL_LIST_PRICE','VI_Price_Books').'</th>
									<th>'.translate('LBL_TYPE','VI_Price_Books').'</th>';
									if((in_array("1",$priceType) && in_array("2",$priceType) && in_array("3",$priceType)) || (in_array("1",$priceType) && in_array("2",$priceType)) || (in_array("1",$priceType) && in_array("3",$priceType)) || (in_array("2",$priceType) && in_array("3",$priceType)) || (in_array("2",$priceType)) || (in_array("3",$priceType))){
										$html .= '<th ';
										if($theme == "SuiteR"){
											$html .= 'class="markLabel"';
										}
										$html .= '>'.translate('LBL_PERCENTAGE_MARK','VI_Price_Books').'</th>';
										$html .= '<th ';
										if($theme == "SuiteR"){
											$html .= 'class="roundLabel"';
										}
										$html .= '>'.translate('LBL_ROUND_OFF','VI_Price_Books').'</th>';
	            					} 
					$html .= '</tr>';
				}
				$i = 1;
				foreach($productEntryData as $key=>$productValues){
					foreach($productValues as $key=>$values){
						$productData = (array)$values;
						if($action == "EditView"){
							$html .= '<tr id="productLine'.$i.'">
				                		<td>
				                    		<input class="sqsEnabled product_name" autocomplete="off" type="text" name="product_name'.$i.'" id="product_name'.$i.'" maxlength="50"  title="" tabindex="116" value="'.$productData['product_name'].'">
				                    		<input type="hidden" name="product_product_id'.$i.'" id="product_product_id'.$i.'"  maxlength="50" value="'.$productData['product_id'].'">
				                    		<span class="id-ff multiple">
					                        	<button type="button" name="btn_product_name" id="btn_product_id" tabindex="0" class="button firstChild" onclick="openModulePopup('."'".$module."'".','.$i.');">
					                           		<img src="themes/default/images/id-ff-select.png?v=mTmf-1lP7RIiI-bMI3e72w">
					                        	</button>';
					                        	$product_name = 'product_name'.$i;
					                        	$product_product_id = 'product_product_id'.$i;
					                        	$html .= '<button type="button" name="" id="btn_clr" tabindex="0" class="button lastChild" onclick="SUGAR.clearRelateField(this.form,'."'".$product_name."'".','."'".$product_product_id."'".'); removeRelateValue('."'".$i."'".');" value=""><img src="themes/default/images/id-ff-clear.png?v=mTmf-1lP7RIiI-bMI3e72w">
					                            </button>
				                   			 </span>
				               			</td>
				                		<td>
				                    		<input class="column" type="text" name="product_price'.$i.'" id="product_price'.$i.'" value="'.$productData['product_price'].'" readonly>
				                		</td>
				                		<td>
				                    		<input class="column" type="text" name="list_price'.$i.'" id="list_price'.$i.'" label="'.translate('LBL_LIST_PRICE','VI_Price_Books').'" value="'.$productData['list_price'].'" onfocusout = "checkQuantityValueType(this,'."'".$i."'".')">
				                		</td>
				                		<td>
				                    		<select name="price_type'.$i.'" id="price_type'.$i.'" class="column" onchange="changePriceType(this,'.$i.')">';
				                    		$html .= '<option value="1"';
				                    		if($productData['price_type'] == "1"){
				                        		$html.=' selected="selected"';
				                        	}
				                        	$html .= '>'.translate('LBL_FLAT_PRICE','VI_Price_Books').'</option>';
				                        	$html .= '<option value="2"';
				                        	if($productData['price_type'] == "2"){
				                        		$html.=' selected="selected"';
				                        	}
				                         	$html .= '>'.translate('LBL_DIFFERENTIAL_PERCENTAGE','VI_Price_Books').'</option>';
				                        	$html .= '<option value="3"';
				                        	if($productData['price_type'] == "3"){
				                        		$html .= ' selected="selected"';
				                        	}
				                        	$html .= '>'.translate('LBL_DIFFERENTIAL_DOLLAR','VI_Price_Books').'</option>'; 
				                    		$html .= '</select>
				                		</td>';
				                	if(isset($productData['percentage_mark'])){
				                    	$html .= '<td id="percentageMark'.$i.'">
				                                <select name="percentage_mark'.$i.'" class="column">';
				                                $html .= '<option value="1"';
				                                if($productData['percentage_mark'] == "1"){
				                                	$html.=' selected="selected"';
				                                }
				                                $html .= '>'.translate('LBL_MARK_UP','VI_Price_Books').'</option>';
				                                $html .= '<option value="0"';
				                                if($productData['percentage_mark'] == "0"){
				                                    $html .= ' selected="selected"';
				                                }
				                                $html .= '>'.translate('LBL_MARK_DOWN','VI_Price_Books').'</option>';
				                                $html .='</select>
				                            </td>';
				                	} 
				                	if(isset($productData['round_off'])){
				                   		$html .= '<td id="roundOff'.$i.'">
				                                <select name="round_off'.$i.'" class="column">';
				                                $html .= '<option value="1"';
				                                if($productData['round_off'] == "1"){
				                                    $html .= ' selected="selected"';
				                                }
				                                $html .= '>'.translate('LBL_YES','VI_Price_Books').'</option>';
				                                $html .= '<option value="0"';
				                                if($productData['round_off'] == "0"){
				                                	$html .= ' selected="selected"';
				                                }
				                                $html .= '>'.translate('LBL_NO','VI_Price_Books').'</option>';
				                        $html .= '</select>
				                            </td>';
				                	}
				                	if($productData['price_type'] == "1"){
				                    	$html .= '<td id="percentageMarkField" style="width:15%;"></td><td id="roundOffField"></td>';
				                	}
				                	$html .= '<td class="minusButton"><button type="button" class="button btn_minus" onclick="removeProduct(this,'.$i.')" name="remove">-</button></td>';
				                $html .= '</tr>';
				                if(isset($productData['price_entry'])){
				                    if($productData['price_type'] == "2"){
				                        $diffTable = '<table id="percentagePriceTable'.$i.'" class="column"><input type="hidden" name="percenPriceEntryCount'.$i.'" id="percenPriceEntryCount'.$i.'" value="'.count($productData['price_entry']).'"/>';
				                    }else if($productData['price_type'] == "3"){
				                        $diffTable = '<table id="dollarPriceTable'.$i.'" class="column"><input type="hidden" name="dollarPriceEntryCount'.$i.'" id="dollarPriceEntryCount'.$i.'" value="'.count($productData['price_entry']).'"/>';
				                    }
				                    if($productData['price_type'] == "2"){
				                        $differentialSymbol = "(%)";
				                    }elseif($productData['price_type'] == "3"){
				                        $differentialSymbol = "($)";
				                    }

				                    $html .= '<tr id="productQuantityRow'.$i.'">
				                                <td></td>
				                                <td colspan="4" id=productQuantityColumn>'
				                                    .$diffTable;
				                                if(!empty($productData['price_entry'])){
													$html .= '<tr id="priceQuantityHeader">
				                                        <th style="padding-right: 70px!important;">'.translate('LBL_FROM_QUANTITY','VI_Price_Books').'</th>
				                                        <th style="padding-right: 71px !important;" class="quantityHeader">'.translate('LBL_TO_QUANTITY','VI_Price_Books').'</th>';
				                                    $html .= '<th style="padding-right:50px !important;"class="quantityHeader">'.translate('LBL_DIFFERENTIAL','VI_Price_Books').$differentialSymbol.'</th>';
				                            	}
				                    $html .= '</tr>';
				                    $j = 1;
				                    foreach($productData['price_entry'] as $key => $values){
				                        $priceData = (array)$values;
				                        $html .= '<tr id="priceQuantityLine'.$i.'_'.$j.'">
				                                    <td>
				                                        <input type="text" name="from_quantity'.$i.'_'.$j.'" value="'.$priceData['from_quantity'].'" id="from_quantity" label="'.translate('LBL_FROM_QUANTITY','VI_Price_Books').'" onfocusout = "checkQuantityValueType(this,'."'".$i."'".')">
				                                    </td>
				                                    <td>
				                                        <input type="text" class="quantityColumn" name="to_quantity'.$i.'_'.$j.'" id="to_quantity" label="'.translate('LBL_TO_QUANTITY','VI_Price_Books').'" value="'.$priceData['to_quantity'].'" onfocusout = "checkQuantityValueType(this,'."'".$i."'".')">
				                                    </td>
				                                    <td>
				                                        <input type="text" class="quantityColumn" name="differential'.$i.'_'.$j.'" id="differential" label="'.translate('LBL_DIFFERENTIAL','VI_Price_Books').'"  value="'.$priceData['differential'].'"onfocusout = "checkQuantityValueType(this,'."'".$i."'".')">
				                                    </td>
				                                    <td class="minusButton">
				                                        <button type="button" class="button btn_minus" onclick="removeQuantity(this,'.$i.','.$productData['price_type'].')" name="remove">-</button>
				                                    </td>
				                                </tr>';
				                            $j++;
				                        }//End of Price Quantity Entry Loop
				                    $html .= '</table>';
				                    $html .= '<button type="button" class="button add_price column" name="add_price" onclick="addQuantity('.$productData['price_type'].','.$i.','."'".$differentialSymbol."'".')" style="margin-top: 5px;">'.translate('LBL_ADD_QUANTITY','VI_Price_Books').'</button>';
				                    $html .= '</td></tr>';  
				                }
							$i++;
						}else if($action == "DetailView"){
							$html .= '<tr>
										<td>
											<a href="index.php?module='.$module.'&action=DetailView&record='.$productData['product_id'].'"><span id="product_product_id" class="sugar_field" data-id-value="'.$productData['product_id'].'"';
												if($theme == "SuiteR"){
													$html .= 'style="margin-left: 11px;"';
												}else if($theme == "Suite7"){
													$html .= 'style="padding-left: 50px;"';
												}
												$html .= '>'.$productData['product_name'].'</span></a>
												</td>
											<td>
												<span class="sugar_field" id="product_price"';
								                	if($theme == "SuiteR"){
								                		$html .= 'style="margin-left: 83px;"';
								                	}else if($theme == "Suite7"){
								                		$html .= 'style="padding-left: 32px;"';
								                	}
								                	$html .= '>'.$productData['product_price'].'
								                </span>
					                		</td>
											<td>
												<span class="sugar_field" id="list_price"';
								                if($theme == "SuiteR"){
								                	$html .= 'style="margin-left: 45px;"';
								                }else if($theme == "Suite7"){
								                	$html .= 'style="margin-left: 40px;"';
								                }
								                $html .= '>'.$productData['list_price'].'</span>
											</td>
											<td>';
												if($productData['price_type'] == "1"){
									                $html .= '<span class="sugar_field" id="price_type"';
									                if($theme == "SuiteR"){
									                	$html .= 'style="margin-left: 77px;"';
									                }else if($theme == "Suite7"){
									                	$html .= 'style="padding-left: 149px;"';
									                }
									                $html .= '>'.translate('LBL_FLAT_PRICE','VI_Price_Books').'</span>';
									            }elseif($productData['price_type']=="2"){
									                $html .= '<span class="sugar_field" id="price_type"';
									                if($theme == "SuiteR"){
									                	$html .= 'style="margin-left: 77px;"';
									                }else if($theme == "Suite7"){
									                	$html .= 'style="padding-left: 149px;"';
									                }
									                $html .= '>'.translate('LBL_DIFFERENTIAL_PERCENTAGE','VI_Price_Books').'</span>';
									            }elseif($productData['price_type']=="3"){
									                $html .= '<span class="sugar_field" id="price_type"';
									                if($theme == "SuiteR"){
									                	$html .= 'style="margin-left: 77px;"';
									                }else if($theme == "Suite7"){
									                	$html .= 'style="padding-left: 149px;"';
									                } 
									                $html .= '>'.translate('LBL_DIFFERENTIAL_DOLLAR','VI_Price_Books').'</span>';
									            }
											$html .= '</td>
											<td>';
											if(isset($productData['percentage_mark'])){
					      						if($productData['percentage_mark'] == "1"){
					                        		$html .= '<span class="sugar_field"';
							                		if($theme == "SuiteR"){
							                			$html .= 'style="margin-left: 114px;"';
							                		}else if($theme == "Suite7"){
							                			$html .= 'style="margin-left: 79px;"';
							                		}
							               	 		$html .= '>'.translate('LBL_MARK_UP','VI_Price_Books').'</span>';
					                    		}elseif($productData['percentage_mark'] == "0"){
					                        		$html .= '<span class="sugar_field"';
							                		if($theme == "SuiteR"){
							                			$html .= 'style="margin-left: 114px;"';
							                		}else if($theme == "Suite7"){
							                			$html .= 'style=margin-left: 79px;"';
							                		}
							               			$html .= '>'.translate('LBL_MARK_DOWN','VI_Price_Books').'</span>';
					                    		}
					                    	}
											$html .= '</td>
											<td>';
											if(isset($productData['round_off'])){
												if($productData['round_off'] == "1"){
									                $html .= '<span class="sugar_field"';
										            if($theme == "SuiteR"){
										              	$html .= 'style="margin-left: 69px;"';
										            }else if($theme == "Suite7"){
										               	$html .= 'style="margin-left: 79px;"';
										            } 
										            $html .= '>'.translate('LBL_YES','VI_Price_Books').'</span>';
									            }elseif($productData['round_off']=="0"){
									                $html .= '<span class="sugar_field"';
										            if($theme == "SuiteR"){
										              	$html .= 'style="margin-left: 69px;"';
										            }else if($theme == "Suite7"){
										              	$html .= 'style="margin-left: 79px;"';
										            }
										            $html .= '>'.translate('LBL_NO','VI_Price_Books').'</span>';
									            }
									        }
											$html .= '</td>
										</tr>';
										if(isset($productData['price_entry'])){
											if($productData['price_type'] == "2" || $productData['price_type'] == "3"){
					            	 			if($productData['price_type'] == "2"){
				                        			$differentialSymbol = "(%)";
				                    			}elseif($productData['price_type'] == "3"){
				                        			$differentialSymbol = "($)";
				                    			}
										
												if(!empty($productData['price_entry'])){
													$html .= '<tr id="productQuantityRange">
															<th colspan=2></th>
															<th class="fromQuantity">'.translate('LBL_FROM_QUANTITY','VI_Price_Books').'</th>
															<th class="toQuantity">'.translate('LBL_TO_QUANTITY','VI_Price_Books').'</th>
															<th class="differential">'.translate('LBL_DIFFERENTIAL','VI_Price_Books').$differentialSymbol.'</th>
															<th></th>
														 </tr>';
													
													foreach($productData['price_entry'] as $key => $values){
														$priceData = (array)$values;
														$html .= '<tr>
																<td colspan=2></td>
																<td style="text-align:center;">
																	<span class="sugar_field" id="fromQuantity">'.$priceData['from_quantity'].'</span></td>
																<td style="text-align:center;">
																	<span class="sugar_field" id="toQuantity">'.$priceData['to_quantity'].'</span></td>
																<td style="text-align:center;">
																	<span class="sugar_field" id="differential">'.$priceData['differential'].'</span></td>
																<td></td>
																</tr>';
													}
												}
											}
										}
						}
					}//end of for loop
				}//end of for loop
				$html .= '</table>';
				if($action == "EditView"){
					$html .= '<button type="button" class="button add_product" name="add_product" onclick="addProduct()" style="margin-top: 10px;">'.translate('LBL_ADD_PRODUCT','VI_Price_Books').'</button>';		
				}
			}else{
				if($action == "EditView"){
					$html .= addProductTable();
				}
			}//end of if
    	}//end of elseif
    	echo $html;
    }//end of function
}//end of class
new VIPriceBooksAddProducts();
?>