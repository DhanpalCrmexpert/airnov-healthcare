<?php
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.

 * SuiteCRM is an extension to SugarCRM Community Edition developed by Salesagility Ltd.
 * Copyright (C) 2011 - 2014 Salesagility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for  technical reasons, the Appropriate Legal Notices must
 * display the words  "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 ********************************************************************************/

$mod_strings = array (
  'LBL_ASSIGNED_TO_ID' => 'Assigned User Id',
  'LBL_ASSIGNED_TO_NAME' => 'Assigned to',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Date Created',
  'LBL_DATE_MODIFIED' => 'Date Modified',
  'LBL_MODIFIED' => 'Modified By',
  'LBL_MODIFIED_ID' => 'Modified By Id',
  'LBL_MODIFIED_NAME' => 'Modified By Name',
  'LBL_CREATED' => 'Created By',
  'LBL_CREATED_ID' => 'Created By Id',
  'LBL_DESCRIPTION' => 'Description',
  'LBL_DELETED' => 'Deleted',
  'LBL_NAME' => 'Name',
  'LBL_CREATED_USER' => 'Created by User',
  'LBL_MODIFIED_USER' => 'Modified by User',
  'LBL_LIST_NAME' => 'Name',
  'LBL_EDIT_BUTTON' => 'Edit',
  'LBL_REMOVE' => 'Remove',
  'LBL_LIST_FORM_TITLE' => 'Timesheet Pro Reporting List',
  'LBL_MODULE_NAME' => 'Timesheet Pro Reporting',
  'LBL_MODULE_TITLE' => 'Timesheet Pro Reporting',
  'LBL_HOMEPAGE_TITLE' => 'My Timesheet Pro Reporting',
  'LNK_NEW_RECORD' => 'Create Timesheet Pro Reporting',
  'LNK_LIST' => 'View Timesheet Pro Reporting',
  'LNK_IMPORT_SP_TIMESHEET_TRACKER' => 'Import Timesheet Pro Reporting',
  'LBL_SEARCH_FORM_TITLE' => 'Search Timesheet Pro Reporting',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'View History',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activities',
  'LBL_SP_TIMESHEET_TRACKER_SUBPANEL_TITLE' => 'Timesheet Pro Reporting',
  'LBL_NEW_FORM_TITLE' => 'New Timesheet Pro Reporting',
  'LBL_TRACK_DATE' => 'Date',
  'LBL_HOURS' => 'Hours',
  'LBL_PROJECT_PROJECT_ID' => 'Project (related Project ID)',
  'LBL_PROJECT' => 'Project',
  'LBL_PROJECT_TASKS_PROJECTTASK_ID' => 'Project Tasks (related Project Task ID)',
  'LBL_PROJECT_TASKS' => 'Project Tasks',
  'LBL_TRACK_USER_USER_ID' => 'User (related User ID)',
  'LBL_TRACK_USER' => 'User',
  'LBL_PARENT_NAME' => 'Related Module',
  'LBL_PARENT_TYPE' => 'Parent',
  'LBL_RELATED_RECORD_URL' => 'Related Record',
  'LBL_EXPORT' => 'Export to CSV',
  'LBL_EXPORT_PDF' => 'Export to PDF',
  'LBL_CREATE_INVOICE' => 'Create Invoice',
  'LBL_RELATED_MODULE' => 'Related Module',
);