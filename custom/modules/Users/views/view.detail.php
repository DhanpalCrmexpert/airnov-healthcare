<?php
if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

require_once('modules/Users/views/view.detail.php');

class CustomUsersViewDetail extends UsersViewDetail
{
    function display() {
		global $current_user;
		if(file_exists){
			unlink('cache/themes/SuiteP/modules/Users/DetailView.tpl');
		}
		if(!is_admin($current_user)){
			unset($this->dv->defs['panels']['LBL_TIMESHEET_NINJA_USER_CONFIGURATION']);
		}
		parent::display();
    }
}
