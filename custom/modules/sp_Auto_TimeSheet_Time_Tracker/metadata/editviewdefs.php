<?php
$module_name = 'sp_Auto_TimeSheet_Time_Tracker';
$viewdefs [$module_name] = 
array (
  'EditView' => 
  array (
    'templateMeta' => 
    array (
      'form' => 
      array (
        'buttons' => 
        array (
          0 => 'SAVE',
          1 => 'CANCEL',
          2 => 'save_and_continue',
          3 => 
          array (
            'customCode' => '<input title="Save And Create New Entry" accesskey="l" class="button"  type="button" name="button" value="Save And Create New Entry" id="Save_And_Create_New_Entry" >',
          ),
        ),
        'enctype' => 'multipart/form-data',
      ),
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
      'syncDetailEditViews' => true,
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'parent_name',
            'studio' => 'visible',
            'label' => 'LBL_RELATED_MODULE',
				'displayParams' => 
            array (
              'field_to_name_array' => 
              array (
                'id' => 'parent_id',
                'project_name' => 'related_project',
                'project_id' => 'related_project_id',
              ),
            ),
          ),
          1 => 
          array (
            'name' => 'description',
            'label' => 'LBL_DESCRIPTION',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'related_project',
            'studio' => 'visible',
            'label' => 'Project',
          ),
          1 => '',
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'start_date',
            'label' => 'LBL_START_DATE',
          ),
          1 => '',
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'processed',
            'label' => 'LBL_PROCESSED',
          ),
          1 => 
          array (
            'name' => 'employee_id',
            'studio' => 'visible',
            'label' => 'LBL_EMPLOYEE_ID',
          ),
        ),
        4 => 
        array (
          0 => 'hours',
          1 => 'manual_entry',
        ),
      ),
    ),
  ),
);
;
?>
