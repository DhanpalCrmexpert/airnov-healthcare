<?php
// created: 2020-10-28 16:59:56
$mod_strings = array (
  'LBL_CAMPAIGN_STATUS' => 'Status: ',
  'LBL_LEADS' => 'Prospects',
  'LBL_CAMPAIGN_LEAD_SUBPANEL_TITLE' => 'Prospects',
  'LBL_LOG_ENTRIES_LEAD_TITLE' => 'Prospects Created',
  'LBL_ACCOUNTS' => 'Customers',
  'LBL_CAMPAIGN_ACCOUNTS_SUBPANEL_TITLE' => 'Customers',
);