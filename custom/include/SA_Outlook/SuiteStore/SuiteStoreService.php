<?php
/**
 * Copyright (c) SalesAgility Ltd.  All Rights Reserved.
 * Licensed under the GPL3 License.  See License in the project root
 * for license information.
 *
 * @package Outlook Addin for SuiteCRM
 * @copyright 2020 SalesAgility Ltd http://www.salesagility.com
 * @author SalesAgility <info@salesagility.com
 *
 */

namespace SalesAgility\Outlook\SuiteStore;

use Configurator;
use DBManagerFactory;

class SuiteStoreService
{
    private $storeApi;
    private $active;

    public function __construct(StoreAPI $store, $active = false)
    {
        $this->storeApi = $store;
        $this->active = $active;
    }

    public function doValidate($key)
    {
        return $this->storeApi->validate($key);
    }

    private function getCurrentUsers()
    {
        $selectedList = [];
        $db = DBManagerFactory::getInstance();
        $query = 'SELECT id FROM users WHERE sa_outlook_is_licensed != 0 AND deleted = 0';
        $result = $db->query($query);
        while ($user = $db->fetchRow($result)) {
            $selectedList[] = $user['id'];
        }

        return $selectedList;
    }

    public function getLicensedUserCount($key)
    {
        return $this->storeApi->getLicensedUserCount($key);
    }

    public function updateStatus($active = false)
    {
        $this->active = $active;
        $licence = $this->active ? 1 : 2;

        $db = DBManagerFactory::getInstance();
        $db->query('UPDATE users SET sa_outlook_is_licensed = ' . $licence . ' WHERE deleted=0 AND sa_outlook_is_licensed != 0');

    }

    private function updateLicense($userList, $status)
    {
        $licence = 0;
        if ($status) {
            $licence = $this->active ? 1 : 2;
        }

        $db = DBManagerFactory::getInstance();
        $query = 'UPDATE users SET sa_outlook_is_licensed = ' . $licence . ' WHERE deleted=0 AND id IN (';
        $quotedId = [];
        foreach ($userList as $id) {
            $quotedId[] = $db->quoted($id);
        }
        $query .= implode(',', $quotedId) . ')';
        $db->query($query);
    }

    public function updateValidUsers($Users, $config, $update = true)
    {
        global $app_strings;

        if (!is_array($Users) || count($Users) < 1) {
            return array(
                'success' => true,
                'result' => $app_strings['LBL_SA_OUTLOOK_LICENSE_USERS_SUCCESS']
            );
        }

        $newUsers = array_filter($Users);
        if ($update) {
            if (!$config['sa_outlook_license_key']) {
                return array(
                    'success' => false,
                    'result' => $app_strings['LBL_SA_OUTLOOK_LICENSE_NO_KEY']
                );
            }
            $key = $config['sa_outlook_license_key'];

            $selectedUsers = array_filter($Users);
            $currentUsers = $this->getCurrentUsers();
            $newUsers = array_diff($selectedUsers, $currentUsers);

            $licenseCount = $this->getLicensedUserCount($key);
            if (!$licenseCount) {
                return array(
                    'success' => false,
                    'result' => $app_strings['LBL_SA_OUTLOOK_LICENSE_VALIDATION_FAIL']
                );
            }
            if ((count($newUsers) + count($currentUsers)) > $licenseCount) {
                return array(
                    'success' => false,
                    'result' => $app_strings['LBL_SA_OUTLOOK_LICENSE_TOO_MANY_SELECTED']
                );
            }
        }
        $this->updateLicense($newUsers, $update);

        return array(
            'success' => true,
            'result' => $app_strings['LBL_SA_OUTLOOK_LICENSE_USERS_SUCCESS']
        );
    }
}
