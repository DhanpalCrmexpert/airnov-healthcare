
<a href="javascript:void(0);" id="printReport">{$MOD.label_print_report}</a> |
<a href="javascript:void(0);" id="printReportChart">{$MOD.label_print_reportChart}</a> |
<a href="javascript:void(0);" class="loadPDF">{$MOD.label_export_pdf}</a> |
<div style="display: inline">
    {$MOD.label_export_pdf_from_xls}
    {include file="modules/$moduleName/templates/elements/proLink.tpl"}
</div> |
<a href="javascript:void(0);" class="loadXLSX">{$MOD.label_export_xlsx}</a> |
<div style="display: inline">
    {$MOD.label_export_xlsx_headers}
    {include file="modules/$moduleName/templates/elements/proLink.tpl"}
</div> |
<div style="display: inline">
    {$MOD.label_export_csv}
    {include file="modules/$moduleName/templates/elements/proLink.tpl"}
</div> |
