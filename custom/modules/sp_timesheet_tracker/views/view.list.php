<?php
//ini_set('display_errors',1);
//error_reporting(E_ALL);
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
require_once('modules/sp_timesheet_tracker/sp_timesheet_trackerListViewSmarty.php');

class sp_timesheet_trackerViewList extends ViewList{

    function ViewList(){
        parent::ViewList();
    }
	
	/*function listViewProcess(){
		$this->processSearchForm();
		echo '<pre>';
		print_r($_REQUEST);
       //$this->lv->searchColumns = $this->searchForm->searchColumns;

        if(!$this->headers)
            return;
        if(empty($_REQUEST['search_form_only']) || $_REQUEST['search_form_only'] == false){
            $this->lv->ss->assign("SEARCH",true);
            $this->lv->setup($this->seed, 'modules/sp_timesheet_tracker/ListViewGeneric.tpl', $this->where, $this->params);
           // $savedSearchName = empty($_REQUEST['saved_search_select_name']) ? '' : (' - ' . $_REQUEST['saved_search_select_name']);
			
			$total_hours = 0;
			$data_arr = $this->lv->data['data'];
			foreach($data_arr as $key=>$val){
				$total_hours = $total_hours + $val['HOURS'];  
			}
			$this->lv->ss->assign('total_hours', $total_hours);
	        echo $this->lv->display();
        }
    }*/

    /*function preDisplay(){

		//$this->lv = new sp_timesheet_trackerListViewSmarty();
		
         //$this->lv = new ListViewSmarty();
    }*/
    function display(){
		
		parent::display();
		//////////////////
		global $current_user, $sugar_config, $app_list_strings;
		$siteUrl = $sugar_config['site_url'];
		require_once('modules/TimesheetNinja/license/OutfittersLicense.php');
		$validate_license = OutfittersLicense::isValid('TimesheetNinja');
		if($validate_license !== true) {
		if(is_admin($current_user)) {
			SugarApplication::appendErrorMessage('Timesheet is no longer active due to the following reason: '.$validate_license.' Users will have limited to no access until the issue has been addressed.');
		}
		echo '<p class="error">Timesheet is no longer active Please renew your subscription or check your license configuration.Will be redirect in seconds.....</p>';
		header("refresh:2;url=$siteUrl");
		//functionality may be altered here in response to the key failing to validate
		}
		echo "</br>";
		
		
		//////////////////
		
        /*if(!$this->bean || !$this->bean->ACLAccess('list')){
            ACLController::displayNoAccess();
        } else {
            $this->listViewPrepare();
            $this->listViewProcess();
        }*/
    }
    public function getModuleTitle(
        $show_help = true
    ) {

echo <<< EOS
	<style>
	</style>
	<script type='text/javascript'>
		$(document).ready(function(){
			$('body').on('click', '#get_hours_count', function(){

				$('body').append('<div id="alert_dialog" title="Basic dialog" style="display:none;"><p id="alert_dialog_body"></p></div>');
				var html;
				$.ajax({
					url:'./index.php?entryPoint=timesheet_custom_view&popup=true',
					type:'GET',
					success:function(req_response){
						html = req_response;

						$('body').append('<div  class="mask" id="backgroung_blur" style="z-index: 1; height: 100%; width: 100%; display: block;">&nbsp;</div>');
						$('#alert_dialog_body').html(html);
						$('#alert_dialog').dialog({
		                  title: "Hours Count",
		                  closeText: "",
		                  minWidth: 800,
		                  maxHeight: 400,
		                  close: function() {
		                      $('#backgroung_blur').remove();
		                  },
		                  buttons: [{
		                      text: "Cancel",
		                      click: function() {
		                      	$(this).dialog('close');
		                    	$('#backgroung_blur').remove();    
		                      }
		                  }],
		              	});
					}
				});
			});
		});
	</script>
EOS;
    	$theTitle = parent::getModuleTitle($show_help);
    	$buttonHtml = "<input type='button' id='get_hours_count' class='button' name='get_hours_count' value='Get Weekly Summary Report' />";
    	$theTitle = str_replace("Timesheet Ninja Reporting", "Timesheet Ninja Reporting <br/>{$buttonHtml}", $theTitle);
    	return $theTitle;
    }
}
?>
