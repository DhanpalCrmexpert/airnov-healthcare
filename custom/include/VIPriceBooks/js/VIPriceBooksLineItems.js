/*********************************************************************************
 * This file is part of package Price Books.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Price Books is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
//Add Price Books Icon in Invoice/Quotes Line Items
var count=0;
var groupButton = document.getElementById('addGroup');
var productButton = document.getElementById('product_group'+count+'addProductLine');
if($('body').find('table#lineItems > tr[id^="group_body"]').length >= "1"){
	updateLineItemsHeader(count);
	updateLineItemsBody();

	if(productButton.addEventListener) {
    	productButton.addEventListener("click", function() {
        	updateLineItemsBody();
    	}, false);
	}
	
	if(groupButton.addEventListener){ 
    	groupButton.addEventListener("click", function() { 
    		count++;
        	productButton.addEventListener("click", function() {
				updateLineItemsHeader(count);
				updateLineItemsBody();
			});
    	});
	}
}else{
	groupButton.addEventListener("click", function() {
		document.getElementById('product_group'+count+'addProductLine').addEventListener("click", function() {
			updateLineItemsHeader(count);
			updateLineItemsBody();

		});
		count++;
	});//end of function
}

function updateLineItemsHeader(count){
	$('table[id^="product_group"] > thead > tr#product_head > td').each(function(){
		if($(this).attr('colspan') == "2"){
			if($(this).next().next('td[id^="pricebooks"]').length != '1'){
				$(this).next().after('<td id="pricebooks'+count+'" style="color: rgb(68, 68, 68);"></td>');
			}
		}
	});
}//end of function 

function updateLineItemsBody(){
	var i = 0;
	$('table[id^="product_group"] > tbody > tr[id^="product_line"]').each(function(){
		if($(this).find('td:nth-child(6) > img').attr('class') != 'price_books'){
			$(this).find('td:nth-child(5)').after("<td><img src='custom/themes/Suite7/images/PriceBookIcon.png' alt='"+priceBooksLabel+"' class='price_books' data-id = '"+i+"' title='"+priceBooksLabel+"' style='cursor:pointer;' ></td>");
		}
		i++;
	});
}//end of function

//Get Price Books Icon Click Event
$('body').on('click','.price_books',function(){
	var count = $(this).attr('data-id');
	var accountId = $('#billing_account_id').val();
	var contactId = $('#billing_contact_id').val();
	var productId = $('#product_product_id'+count).val();
	
	if(productId != ""){
		$.ajax({
			url : 'index.php?entryPoint=VIPriceBooksGetData',
			type: 'POST',
			data : {accountId:accountId,
					contactId:contactId,
					productId:productId
				},
			success : function(data) {
				$('#priceBookPopup').css('display','block');
				$('#popup-loading').show(); //show loader
				$('.modalBody').remove();
				$('.modal-body').append(data);
				$('#popup-loading').hide(); //hide loader
				if($('.modalFooter').find('input#add_line_items').length == "1"){
					$('.modalFooter').find('input#add_line_items').attr("onclick","updateListPrice("+count+")");
					$('.modalFooter').find('input#add_line_items').attr("disabled","true");
				}					
			}
		});
	}else{
		alert(productValidation);
	}
});//end of function

//Close Price Books Popop
function closePriceBooksPopup(){
	$('#priceBookPopup').css('display','none');
}

function selectPriceBook(){
	var id = [];
	$(".listViewEntriesCheckBox:checked").each(function() {
		if(this.checked == true){
			id.push($(this).val());			
		}
	});
	
	if(id.length >= 1){
	 	$('#add_line_items').attr('disabled',false);
    }else{
    	$('#add_line_items').attr('disabled',true);
    }
}//end of function
	
function updateListPrice(count){
	var quantity = $('#product_product_qty'+count).val();
	var productId = $('#product_product_id'+count).val();
	var id = [];
    $(".listViewEntriesCheckBox:checked").each(function() {
     	id.push($(this).val());
    });//end of each
    
    if(id.length > 1){
    	alert(priceBooksMsg);
    }else if(id.length == 1){
		$.ajax({
			url : 'index.php?entryPoint=VIPriceBooksGetUpdatedListPrice',
			type: 'POST',
			data : {
					id:id,
					quantity:quantity,
					productId:productId},
			success : function(data) {
				$('#priceBookPopup').css('display','none');
				if(data != ""){
					$('#product_product_list_price'+count).val(data);
					$('#product_name'+count).trigger("blur");
					$('#product_product_list_price'+count).trigger("blur");
				}
			}
		});
    }
}//end of function