<?php 
 //WARNING: The contents of this file are auto-generated



$mod_strings['LBL_NPR'] = 'NPR';
$mod_strings['LBL_NDA'] = 'NDA';
$mod_strings['LBL_POTENTIAL_LAUNCH'] = 'Potential launch';
$mod_strings['LBL_SUCCESS_RATE'] = 'Success rate';
$mod_strings['LBL_SALES'] = 'Sales/year USD in full year';
$mod_strings['LBL_PRODUCTION_SITE'] = 'Production Site';
$mod_strings['LBL_POTENTIAL_IMPACT_USD_BUDGET_NINETEEN'] = 'Potential impact USD budget 2019';
$mod_strings['LBL_POTENTIAL_IMPACT_USD_BUDGET_TWENTY'] = 'Potential impact USD budget 2020';
$mod_strings['LBL_POTENTIAL_IMPACT_USD_BUDGET_TWENTYONE'] = 'Potential impact USD budget 2021';
$mod_strings['LBL_DESCRIPTION'] = 'Comments / info';



//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_ACCOUNTS_SP_TIMESHEET_TRACKER_1_FROM_SP_TIMESHEET_TRACKER_TITLE'] = 'Timesheet Entries';
$mod_strings['LBL_TOTAL_HOURS_AT'] = 'Total Hours';


/*********************************************************************************
 * This file is part of package Price Books.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Price Books is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
$mod_strings['LBL_PRICE_BOOKS_LABEL'] = 'Price Books';


//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_AHC_KEY_ACCOUNTS_ACCOUNTS_1_FROM_AHC_KEY_ACCOUNTS_TITLE'] = 'Key Accounts';

?>