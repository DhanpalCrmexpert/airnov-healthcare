<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

$license_strings = array (
    'LBL_STEPS_TO_LOCATE_KEY_TITLE' => 'Om Uw sleutel Locate',
    'LBL_STEPS_TO_LOCATE_KEY_1' => '1. Log in om',
    'LBL_STEPS_TO_LOCATE_KEY_2' => '2. Gaan naar Account->Purchases',
    'LBL_STEPS_TO_LOCATE_KEY_3' => '3. Zoek de sleutel voor de aankoop van deze add-on',
    'LBL_STEPS_TO_LOCATE_KEY_4' => '4. Plakken in de Licentiecode vak hieronder',
    'LBL_STEPS_TO_LOCATE_KEY_5' => '5. Klik op "Validate"',
    'LBL_LICENSE_KEY' => 'Licentiecode',
    'LBL_CURRENT_USERS' => 'Huidige gebruiker Graaf',
    'LBL_LICENSED_USERS' => 'Licensed Gebruikersaantal',
    'LBL_VALIDATE_LABEL' => 'Bevestigen',
    'LBL_VALIDATED_LABEL' => 'Gevalideerd',
);

