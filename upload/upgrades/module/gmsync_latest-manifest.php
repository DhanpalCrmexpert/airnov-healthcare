<?php

$manifest = array (
    'acceptable_sugar_versions' =>  array (
        'regex_matches' => array(
            '.*',
        ),
    ),
    'acceptable_sugar_flavors' => array(
        'CE',
        'PRO',
        'ENT',
        'CORP',
        'ULT',
    ),
    'readme'=>'',
    'key'=>'gmsync',
    'author' => 'GrinMark',
    'description' => 'GrinMark AESync and Office 365 Plugin helper',
    'is_uninstallable' => true,
    'name' => 'GMSyncAddon',
    'published_date' => '2015/07/15',
    'type' => 'module',
    'version' => '1.9.4',
    'remove_tables' => false,

);
$installdefs = array (
    'id' => 'GMSyncAddon',
    'copy' =>
        array (
            //copy license directory to your module
            array (
                'from' => '<basepath>/license',
                'to' => 'modules/GMSyncAddon',
            ),
            array (
                'from' => '<basepath>/custom/service',
                'to' => 'custom/service',
            ),        
        ),
    'language' =>
        array (
            array(
                'from'=> '<basepath>/license_admin/language/en_us.GMSyncAddon.php',
                'to_module'=> 'Administration',
                'language'=>'en_us'
            ),
        ),
    'administration' =>
        array(
            array(
                'from'=>'<basepath>/license_admin/menu/GMSyncAddon_admin.php',
                'to' => 'modules/Administration/GMSyncAddon_admin.php',
            ),
        ),
    'action_view_map' =>
        array (
            array(
                'from'=> '<basepath>/license_admin/actionviewmap/GMSyncAddon_actionviewmap.php', 
                'to_module'=> 'GMSyncAddon',
            ),
        ),
);

?>