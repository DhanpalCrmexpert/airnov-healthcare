<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

$license_strings = array (
    'LBL_STEPS_TO_LOCATE_KEY_TITLE' => 'Per individuare la chiave',
    'LBL_STEPS_TO_LOCATE_KEY_1' => '1. Accedi per',
    'LBL_STEPS_TO_LOCATE_KEY_2' => '2. Vai a Account->Purchases',
    'LBL_STEPS_TO_LOCATE_KEY_3' => '3. Individuare la chiave per l\'acquisto di questo add-on',
    'LBL_STEPS_TO_LOCATE_KEY_4' => '4. Incollare nella casella Chiave di Licenza di seguito',
    'LBL_STEPS_TO_LOCATE_KEY_5' => '5. Fare clic su "Validate"',
    'LBL_LICENSE_KEY' => 'Chiave di Licenza',
    'LBL_CURRENT_USERS' => 'Conte utente corrente',
    'LBL_LICENSED_USERS' => 'Numero di utenti con licenza',
    'LBL_VALIDATE_LABEL' => 'Convalidare',
    'LBL_VALIDATED_LABEL' => 'Convalidato',
);

