
{include file="modules/$moduleName/templates/settings/pro_notification.tpl"}

<div>
    <h2>{$MOD.label_date_format}</h2>
    <div class="ar-settings-table">
        <table>
            <tr>
                <td style="width:50%">{$MOD.label_date_format_settings}</td>
                <td>
                    {html_options name=dateformat options=$DATE_FORMAT_LABELS selected=$CURRENT_FORMAT_ID disabled=true}
                </td>
            </tr>
        </table>
    </div>

    <h2>{$MOD.label_update_records_per_page}</h2>
    <div class="ar-settings-table">
        <table>
            <tr>
                <td style="width:50%">{$MOD.label_show} {$MOD.label_records_per_page}</td>
                <td>
                    <input value="{$RECORDSPERPAGE}" size="3" maxlength="3" disabled />
                </td>
            </tr>
            <tr>
                <td>{$MOD.label_overwrite_all}</td>
                <td>
                    <input type="checkbox" disabled />
                </td>
            </tr>
        </table>
    </div>

    <h2>{$MOD.showNullValues}</h2>
    <div class="ar-settings-table">
        <table>
            <tr>
                <td style="width:50%">{$MOD.label_show}</td>
                <td>
                    <input id="show-null-values-checkbox" type="checkbox" checked="checked" disabled />
                </td>
            </tr>
        </table>
    </div>

    <h2>{$MOD.skipDeletedRecords}</h2>
    <div class="ar-settings-table">
        <table>
            <tr>
                <td style="width:50%">{$MOD.label_skip_deleted}</td>
                <td>
                    <input id="show-null-values-checkbox" type="checkbox" disabled />
                </td>
            </tr>
        </table>
    </div>

    <h2>{$MOD.label_file_format_settings}</h2>
    <div class="ar-settings-table">
        <table>
            <tr>
                <td style="width:50%">{$MOD.label_filename_title_length}</td>
                <td>
                    {html_options name=fileNameLength options=$FILE_NAME_LENGTH_OPTIONS selected=$FILE_NAME_LENGTH disabled=true}
                </td>
            </tr>
            <tr>
                <td>{$MOD.label_add_timestamp_to_the_end_of_filename}</td>
                <td>
                    <input id="addTimestampToFileName" type="checkbox" disabled />
                </td>
            </tr>
        </table>
    </div>

    <h2>{$MOD.label_scheduler_settings}</h2>
    <div class="ar-settings-table">
        <table>
            <tr>
                <td style="width:50%">{$MOD.label_add_origin_info_to_email}</td>
                <td>
                    <input id="addOriginInfoToEmail" type="checkbox" name="addOriginInfoToEmail" value="true" {if $addOriginInfoToEmail eq true} checked="checked" {/if} />
                </td>
            </tr>
        </table>
    </div>

    <h2>{$MOD.label_other_settings}</h2>
    <div class="ar-settings-table">
        <table>
            <tr>
                <td style="width:50%">{$MOD.label_api_endpoint}</td>
                <td>
                    <input id="apiEndpoint-text" type="text" value="{$apiEndpoint}" size="50" disabled />
                </td>
            </tr>
            <tr>
                <td style="width:50%">{$MOD.label_force_api_to_generate_files}</td>
                <td>
                    <input id="forceApi-checkbox" type="checkbox" disabled />
                </td>
            </tr>
            <tr>
                <td style="width:50%">{$MOD.label_force_api_diagnostics}</td>
                <td>
                    <input id="forceApiDiagnostics-checkbox" type="checkbox" disabled />
                </td>
            </tr>
            <tr>
                <td style="width:50%">{$MOD.label_clear_all_output_buffers}</td>
                <td>
                    <input id="clearBuffer-checkbox" type="checkbox" disabled />
                </td>
            </tr>
            <tr>
                <td style="width:50%" title="Value 0 is for infinite timeout">{$MOD.label_api_client_timeout}</td>
                <td>
                    <input id="apiClientTimeout-checkbox" type="number" min="0" disabled />
                </td>
            </tr>
        </table>
    </div>
</div>