<p class="text-muted">{$MOD.LBL_OUTLOOK_ADDIN_SETTINGS_HELP}</p>

<br/>

<form id="ConfigureSettings"
      name="ConfigureSettings"
      class="detail-view enctype='multipart/form-data'"
      method="POST"
      action="index.php?module=Administration&action=OutlookAddinConfig&do=save">

    <div class="row">
        <div class="panel panel-primary">
            <div class="panel-heading">{$MOD.LBL_OUTLOOK_ADDIN_LICENSE_HEADING}</div>
            <div class="panel-body tab-content form-row">
                <div class="col-md-6">
                    <div class="form-check">
                        <div class="col-md-8">
                            <label for="sa_outlook_license_key">{$MOD.LBL_OUTLOOK_ADDIN_LICENSE_KEY}</label>
                            <input type='text' class="form-control" name='sa_outlook_license_key'
                                   id='sa_outlook_license_key'
                                   value='{$config.sa_outlook_license_key}'>
                        </div>
                        <div class="col-md-4">
                            <label for="validate_key_btn">&nbsp;</label>
                            <button class="btn btn-primary form-control"
                                    id="validate_key_btn"
                                    name='validate_key_btn'
                                    onclick="callValidateEntry($('#sa_outlook_license_key').val())"
                                    type="button">{$MOD.LBL_OUTLOOK_ADDIN_BTN_VALIDATE_KEY}</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <label for="validate">&nbsp;</label>
                    <span id="valid_key" style="display: none"><p id="valid_response" class="text-success"></p></span>
                    <span id="invalid_key" style="display: none"><p id="invalid_response"
                                                                    class="text-danger"></p></span>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">{$MOD.LBL_MICROSOFT_GRAPH_SETTINGS}</div>
                <div class="panel-body tab-content">
                    <div class="form-group">
                        <label for="graph_tenant_id">{$MOD.LBL_MICROSOFT_GRAPH_TENANT_ID}</label>
                        <input type="text" class="form-control"
                               id="graph_tenant_id" name="graph_tenant_id" value="{$config.graph_tenant_id}">
                        <label for="graph_client_id">{$MOD.LBL_MICROSOFT_GRAPH_CLIENT_ID}</label>
                        <input type="text" class="form-control"
                               id="graph_client_id" name="graph_client_id" value="{$config.graph_client_id}">
                        <label for="graph_client_secret">{$MOD.LBL_MICROSOFT_GRAPH_CLIENT_SECRET}</label>
                        <input type="password" class="form-control"
                               id="graph_client_secret" name="graph_client_secret" value="{if !empty($config.graph_client_secret)}SECRET_UNCHANGED{/if}">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">{$MOD.LBL_OUTLOOK_ADDIN_SEARCH_SCHEDULERS}</div>
                <div class="panel-body tab-content">
                    <label>{$MOD.LBL_OUTLOOK_ADDIN_SEARCH_SCHEDULERS_HELP}</label>
                    <ul class="list-group">
                        {foreach from=$schedulers item=scheduler}
                            <li class="list-group-item {if $scheduler->status eq 'Inactive'}list-group-item-warning{/if}">
                                {sugar_link
                                module=$scheduler->module_name
                                record=$scheduler->id
                                label=$scheduler->name
                                action='DetailView'}
                                &mdash;
                                <b>{$scheduler->status}</b>
                                &mdash;
                                {if !empty($scheduler->last_run)}
                                    {$MOD.LBL_OUTLOOK_ADDIN_SEARCH_SCHEDULERS_LAST_RUN}
                                    <b>{$scheduler->last_run|default_date_value}</b>
                                {else}
                                    {$MOD.LBL_OUTLOOK_ADDIN_SEARCH_SCHEDULERS_NEVER_RUN}
                                {/if}
                            </li>
                            {foreachelse}
                            <p class="error">{$MOD.LBL_OUTLOOK_ADDIN_SEARCH_SCHEDULERS_NOT_FOUND}</p>
                        {/foreach}
                    </ul>
                    <small class="form-text text-muted">{$MOD.LBL_OUTLOOK_ADDIN_SEARCH_SCHEDULERS_DESC}</small>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">{$MOD.LBL_OUTLOOK_ADDIN_SETTINGS}</div>
                <div class="panel-body tab-content">
                    <div class="form-group">
                        <label for="module_select[]">{$MOD.LBL_OUTLOOK_ADDIN_MODULES_SELECT}</label>
                        <div>
                            {html_options name="module_select[]" id="module_select[]" selected=$OUTLOOK_MODULES options=$EMAIL_MODULES multiple="true" size="6"}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div>
        {$BUTTONS}
    </div>

    {$JAVASCRIPT}

</form>
<form action="index.php" method="post" name="DetailView" id="formDetailView">
    <input type="hidden" name="module" value="Users">
    <input type="hidden" name="record" value="{$USER_ID}">
    <input type="hidden" name="layout_def_key" value="outlookAdmin">
</form>

<div style="padding-top: 20px">
    <span id="license_success" style="display: none" ><p id="license_success_response" class="text-success"></p></span>
    <span id="license_fail" style="display: none" ><p id="license_fail_response" class="text-danger"></p></span>
</div>

{$SUBPANEL}


<script src="custom/include/SA_Outlook/admin/admin.js"></script>
