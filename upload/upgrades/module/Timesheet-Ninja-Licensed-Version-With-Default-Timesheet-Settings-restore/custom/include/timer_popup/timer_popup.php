<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
require_once 'modules/Configurator/Configurator.php';
class TimerPopup {

	var $not_allowed_modules = array(
		'Administration',
		'ModuleBuilder',
		// 'Home',
	);

	function load_popup($event, $arguments) {
		global $db, $current_user;

		$sql = "SELECT id FROM suh_timer_recording 
				WHERE deleted=0 AND timer_ended=0 AND assigned_user_id='".$current_user->id."'
				ORDER BY start_date DESC
				LIMIT 1";
		$rslt = $db->query($sql);
		if(!in_array($_REQUEST['module'],$this->not_allowed_modules) && $rslt->num_rows > 0){
			$row = $db->fetchByAssoc($rslt);
			$timer_bean = BeanFactory::getBean("suh_timer_recording",$row['id']);
			if(!empty($timer_bean->id)){

				echo '<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">';
				echo "<style>.ui-dialog { z-index: 9999999 !important ;}</style>";
				echo '<script src="custom/include/timer_popup/easytimer.min.js"></script>';
				echo '<script src="custom/include/timer_popup/timerPopup.js"></script>';
				echo '<script src="custom/include/html2canvas.js"></script>';

				echo "<script>
					addTimerPopJS('".$timer_bean->time_spend."','".$timer_bean->parent_name."','".$timer_bean->parent_type."','".$timer_bean->parent_id."','".$timer_bean->assigned_user_name."','".$timer_bean->assigned_user_id."', '".$timer_bean->sec_spend."', '".$timer_bean->id."', '".$timer_bean->timer_pause."' );
				</script>";
			}
		}
	}
}