"use strict";

(function(window) {

    var $t = window.translated_labels;
    var jQuery = window.jQuery;
    var $urls = window.$urls;
    var $categories = window.$categories;
    var $reportTree = window.$reportTree;

    var $ReportData = window.ReportData;
    var $searchCriteria = window.$searchCriteria;
    var $reportTreeAdminAccess = window.$reportTreeAdminAccess;
    var $isAdmin = window.$isAdmin;
    var $isBasic = window.$isBasic;
    var $toggleHidden = window.$toggleHidden;

    function pop_include_details() {
        var newwindow2 = window.open('','name','height=140,width=580');
        var tmp = newwindow2.document;
        tmp.write('<html><head><title>Include Details Help</title>');
        tmp.write('</head><body>');
        tmp.write('<p>'+$t.label_to_create_new+'</p>');
        tmp.write('</ body></html>'); // #4470 - Added whitespace in closing body tag, so csrf-magic will not add tokens and kill JS
        tmp.close();
    }

    function toggleVisible(show) {
        var location = window.location.href;
        location = location.replace("&showHidden=true","");
        location = location.replace("#toggleVisible","");
        if(show) {
            location = location + "&showHidden=true";
        }

        window.location.href = location;
    }

    //#5722 [START] Highlight search results
    function highlightResults(highlight){
        var highlights = highlight.split(" ");
        highlights = highlights.filter(function(value){return value.length>0;});
        highlights = highlights.join("|");
        var re = new RegExp(highlights, "gi");
        var titles = document.getElementsByClassName("reportTreeTitle");
        var descriptions = document.getElementsByClassName("reportTreeDescription");
        for(var i = 0, l = titles.length; i<l; i++){
            titles[i].innerHTML = titles[i].innerHTML.replace(re, function(x){return '<span class="highlight">'+x+'</span>'});
            if(descriptions[i]!=undefined){
                descriptions[i].innerHTML = descriptions[i].innerHTML.replace(re, function(x){return '<span class="highlight">'+x+'</span>'});
            }
        }
    }
    //#5722 [END]

    /**
     * RFC4122 version 4 compliant solution for generating UUID
     *
     * @returns {string}
     */
    function getUUID () {
        var pattern = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx';
        var uid = pattern.replace(/[xy]/g, function(c) {
            var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
            return v.toString(16);
        });

        return uid;
    }

// jsTree context menu
    function contextMenu(node) {
        // Only for categories
        if (node.type !== 'category') {
            return;
        }

        // The default set of all items
        var items = {
            createDashboard: {
                _disabled: $isBasic,
                label: $t.label_add_dashboard  + ($isBasic ? ' (PRO)' : ''),
                action: function () {
                    var categoryId = node.id.replace("c_","");
                    var dashboardId = getUUID();
                    var urlCreate = $urls.withoutActionCSRF + "createDashboard&dashboardId="+dashboardId+"&categoryId=" + categoryId;
                    var urlRedirect = $urls.withoutActionCSRF + "dashboard&record="+dashboardId;

                    var options = {
                        method: 'GET',
                        credentials: "same-origin",
                        headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json'
                        },
                    };

                    SUGAR.ajaxUI.showLoadingPanel();
                    window.fetch(urlCreate, options)
                        .then(function (response) {
                            SUGAR.ajaxUI.hideLoadingPanel();
                            window.location = urlRedirect;
                        })
                        .catch(function (error) {
                            SUGAR.ajaxUI.hideLoadingPanel();
                            console.log(error);
                        })
                }
            },
            repairReportPositions: {
                separator_before: true,
                label: "Repair report positions",
                action: function() {
                    var saveCategoryUrl = $urls.saveCategoryUrl;
                    var categoryId = node.id.replace("c_","");

                    var newData = {
                        _action: 'repairReports',
                        parentId: categoryId,
                    };

                    // Add CSRF post tokens
                    if(csrf && csrf.name && csrf.value) {
                        newData[csrf.name] = csrf.value
                    }

                    // Send to server
                    jQuery.post(saveCategoryUrl, newData, function(response){
                        window.location.reload();
                    });
                }
            },
            repairCategoriesPositions: {
                label: "Repair categories positions",
                action: function() {
                    var saveCategoryUrl = $urls.saveCategoryUrl;
                    var categoryId = node.id.replace("c_","");

                    var newData = {
                        _action: 'repairCategories',
                        parentId: categoryId,
                    };

                    // Add CSRF post tokens
                    if(csrf && csrf.name && csrf.value) {
                        newData[csrf.name] = csrf.value
                    }

                    // Send to server
                    jQuery.post(saveCategoryUrl, newData, function(response){
                        window.location.reload();
                    });
                }
            },
            repairRootPostions: {
                label: "Repair root positions",
                action: function() {
                    var saveCategoryUrl = $urls.saveCategoryUrl;

                    var newData = {
                        _action: 'repairCategories',
                        parentId: 0,
                    };

                    // Add CSRF post tokens
                    if(csrf && csrf.name && csrf.value) {
                        newData[csrf.name] = csrf.value
                    }

                    // Send to server
                    jQuery.post(saveCategoryUrl, newData, function(response){
                        window.location.reload();
                    });
                }
            }
        };

        return items;
    }




    jQuery(function() {
        // URL for saving category
        var saveCategoryUrl = $urls.saveCategoryUrl;
        var massScheduleUrl = $urls.massScheduleUrl;
        var exportReportsUrl = $urls.exportReportsUrl;

        // Report tree JSON data
        var treeData = $reportTree;
        var highlight = $searchCriteria;

        var reportTreeAdminAccess = $reportTreeAdminAccess;
        var isAdmin = $isAdmin;

        var tools_add = '';
        var jsTreePlugins = ["grid", "state", "types", "checkbox", "contextmenu"];
        if(isAdmin || !reportTreeAdminAccess) {
            jsTreePlugins.push("dnd"); // Add Drag-and-Drop

            var toggleText = $t.label_hide_hidden;
            if($toggleHidden) {
                toggleText = $t.label_show_hidden;
            }

            jQuery("#toggleVisible").live("click", function(e) {
                e.preventDefault();
                toggleVisible($toggleHidden);
            });

            var tools_add = '<a href="'+$urls.toggleVisible+'" id="toggleVisible" style="color:blue">'+toggleText+'</a>';
        }

        // Bind jsTree on selected node
        var treeNodeId = "report-tree";
        var jsTree = jQuery('#'+treeNodeId).jstree({
            plugins : jsTreePlugins,
            core : {
                data: treeData,
                // #3844 [start] - Every time node has been moving, run validator
                check_callback: function(operation, node, parent, position) {
                    if (node.id.substring(0, 2) != "c_"){//#4033 - fixed folder moving
                        if(operation == "move_node") {
                            //#3879 [start] - prevent moving reports that the current user is unable to edit
                            if (node.id.substring(0, 2) != "c_"){//#4033 - fixed folder moving
                                if(del_rep.indexOf(node.id)==-1){
                                    return false;
                                }
                            }
                            //#3879 [end]
                            // Reports can not be moved directly to the root
                            if((node.type == "report" || node.type == "dashboard") && parent.id == "#") {
                                return false;
                            }
                        }
                    }
                    return true;
                }
                // #3844 [end]
            },
            contextmenu: {
                'select_node': false,
                items: contextMenu
            },
            state: {
                "key":treeNodeId,
                // Forget selected checkboxes after reload
                filter : function (state) {
                    delete state.checkbox;
                    return state;
                }
            },
            grid: {
                width:'100%',
                columns: [
                    // This is mystification for cross-browser support. It should be something like 30%/50%/20%
                    {width: '50%', header: $t.label_report_folders},
                    {width: '50%', header: $t.label_description, value: "description", cellClass: "desctooltip"},//#4391 added new tooltip
                    {width: '10%', header: $t.label_tools + '&nbsp;' + tools_add, value: "tools"}
                ],
            },
            checkbox: {
                tie_selection: false,
                whole_node: false,
            },
            types: {
                category: {
                    icon: "jstree-folder",
                    valid_children : ["category", "report", "dashboard"],
                },
                report: {
                    icon: "jstree-file",
                    valid_children: [],
                },
                dashboard: {
                    icon: "modules/AnalyticReporting/assets/img/gears_16x16.png",
                    valid_children: [],
                },
            },

        });

        // #5696 [start] - allow checking of deletable reports only
        jsTree.on('check_node.jstree', function (e, data){
            e.preventDefault();
            var referenceJSTree = jsTree;

            var children = data.node.children;
            var folderHasSelectedElements = false;
            if(data.node.id.indexOf("c_") == 0){
                for (var i = 0; i < children.length; i++) {
                    if(data.selected.indexOf(children[i])!=-1){
                        folderHasSelectedElements = true;
                        break;
                    }
                }
            }

            if(!folderHasSelectedElements){
                checkIfDeletableRecursive(data.node.id, referenceJSTree);
            }else{
                //if folder has selected elements, that means it was already selected, deselect it instead
                referenceJSTree.jstree(true).uncheck_node(data.node.id);
            }

        });

        function checkIfDeletableRecursive(nodeId, referenceJSTree){
            var children = referenceJSTree.jstree(true).get_node(nodeId).children;
            //single node
            if(children.length == 0 && //no children
                nodeId.indexOf("c_") != 0 && //not a folder
                del_rep.indexOf(nodeId) == -1)referenceJSTree.jstree(true).uncheck_node(nodeId);
            //folder
            else{
                for (var i = 0; i < children.length; i++) {
                    if((children[i].indexOf("c_") == 0)){
                        var allDeletable = checkIfDeletableRecursive(children[i], referenceJSTree);
                        if(allDeletable == false)break;
                    }else if (del_rep.indexOf(children[i]) == -1) {
                        referenceJSTree.jstree(true).uncheck_node(children[i]);
                    }
                }
            }

        }
        // #5696 [end]

        // Filter report onclick
        jsTree.on('select_node.jstree', function (e, data) {
            // Don't trigger select_node event if triggered from saved state
            if(!data.event) {
                return false;
            }

            // Reports open as links
            if(data.node.type == "report" || data.node.type == "dashboard") {
                window.location.href = data.node.a_attr.href;
            } else {
                // Other types just toggle
                return data.instance.toggle_node(data.node);
            }
        });

        jsTree.on('ready.jstree', function (e, data) {
            if(highlight.length > 0){ //#5722 Highlight results is search criteria is passed
                highlightResults(highlight);
            }
        });

        // After node has been moved
        jsTree.on("move_node.jstree", function (e, data) {
            var newData = {};

            // Move category
            if(data.node.type == "category") {
                var action= "moveCategory";
            } else {
                // Move report
                var action= "moveItem";
            }

            // Remove c_ from category ids
            var parentId = data.parent.replace("c_", "");
            var id = data.node.id.replace("c_", "");

            // Replace category root elements to zero
            if(parentId == "#") {
                parentId = 0;
            }

            // Set POST data items
            newData._action= action;
            newData.id = id;
            newData.parentId = parentId;
            newData.position = data.position;

            // Add CSRF post tokens
            if(csrf && csrf.name && csrf.value) {
                newData[csrf.name] = csrf.value
            }

            // Send to server
            jQuery.post(saveCategoryUrl, newData, function(response){
                window.location.reload();
            });
        });


        // Ractive.js modal component for saving new reports or edit existing
        var ReportFolder = Ractive.extend({
            el: jQuery('#modalWindow'),
            append: false,
            template: '{{>modalContent}}',

            data: {
            },
            init: function() {
                // Get passed data
                var data = this.data;

                // Open dialog and save
                jQuery("#modalWindow").dialog({
                    modal: true,
                    draggable:false,
                    resizable: false,
                    titl$e: (data._action == "edit" ? $t.label_edit_report_folder : $t.label_new_report_folder),//#3931
                    open: function (type, _data) {
                        // If edit, then get existing title and description
                        if(data._action == "edit") {
                            jQuery(this).find("[name=categoryName]").val(data.title);
                            jQuery(this).find("[name=categoryDescription]").val(data.description)
                        }
                    },
                    buttons: [
                        {
                            text: $t.label_save,
                            click: function() {
                                var that = this;

                                data.title = jQuery(this).find("[name=categoryName]").val();
                                data.description = jQuery(this).find("[name=categoryDescription]").val();

                                // Add CSRF post tokens
                                if(csrf && csrf.name && csrf.value) {
                                    data[csrf.name] = csrf.value
                                }

                                // Send to server
                                jQuery.post(saveCategoryUrl, data, function(response){
                                    response = JSON.parse(response);
                                    jQuery(that).dialog("close");
                                    window.location.reload();
                                });
                            }
                        },
                        {
                            text: $t.label_cancel,
                            click: function() {
                                jQuery(this).dialog("close");
                            }
                        }
                    ]
                });
            }
        });

        // Unified Ractive dialog component (should be merged with ReportFolder component)
        var RactiveDialog = Ractive.extend({
            el: jQuery('#modalWindow'),
            append: false,
            template: '{{>modalContent}}', // Using partials
            data: {
                dialogOptions: {}
            },
            init: function() {
                // Get passed data
                var data = this.data;

                // Init callback
                if(data.init) {
                    data.init();
                }

                // Open dialog and save
                jQuery(this.el).dialog(data.dialogOptions);
            },
            // On shutdown, destroy dialog
            onteardown: function(){
                if(this.dialog){
                    this.dialog.dialog('destroy');
                }
            }
        });

        var modalContentTemplate = '<div style="padding-top:10px;"><label>'+$t.label_title+': <input style="width: 280px;" type="text" name="categoryName" /></label><label>'+$t.label_description+': <textarea style="width: 280px;" name="categoryDescription"></textarea></label></div>';

        jQuery("[data-action='addCategory']").live("click", function(e) {
            e.preventDefault();
            // Get data for modal
            var data = {}
            data.id = jQuery(this).data("id");
            data._action = "add";

            // Initialize Ractive.js modal component with data
            var modal = new ReportFolder({
                data: data,
                partials: {
                    modalContent: modalContentTemplate
                }
            });
        });

        jQuery("[data-action='editCategory']").live("click", function(e) {
            e.preventDefault();
            // Get data for modal
            var data = {}
            data.id  = jQuery(this).data("id");
            data.title  = jQuery(this).data("title");
            data.description = jQuery(this).data("description");
            data._action  = "edit";

            // Initialize Ractive.js modal component with data
            var modal = new ReportFolder({
                data: data,
                partials: {
                    modalContent: modalContentTemplate
                }
            });
        });

        jQuery("[data-action='deleteCategory']").live("click", function(e) {
            //#3839 [start] - delete reports from before deleting report folder
            var children = jsTree.jstree(true).get_node("#c_"+jQuery(this).data("id")).children;
            if(children.length>0){
                alert($t.label_delete_children);
                return false;
            }
            //#3839 [end]
            e.preventDefault();
            var data = {};
            data.id = jQuery(this).data("id");
            data._action = "delete";

            // Add CSRF post tokens
            if(csrf && csrf.name && csrf.value) {
                data[csrf.name] = csrf.value
            }


            jQuery.post(saveCategoryUrl, data, function(response){
                window.location.reload();
            });
        });

        jQuery("[data-action='massSchedule']").click(function(e) {
            e.preventDefault();
            var reportIds = [];

            // Get all checked node ids
            jsTree.jstree("get_checked").forEach(function(id){
                // Filter reports
                if(id.substring(0, 2) != "c_") {
                    reportIds.push(id);
                }
            });

            // Show dialog only if there is at least one report selected
            if(reportIds.length > 0) {

                var accessManagerOpt = {
                    users:$ReportData.reportAccessUsers,
                    filter: "",
                    accessRights:[],
                    globalSharing:'PRIV',
                    availableOwners:$ReportData.users,
                    owner:'1',
                    globalSharingRights:'VIEW',
                    updatePermissions:false,//#5286
                    t: window.translated_labels,
                    userPermissions: window.userPermissions,
                    moduleName: window.moduleName
                };
                var scheduleManagerOpt = {
                    users:$ReportData.reportScheduleUsers,
                    filter: "",
                    isScheduled:false,
                    selectedUsers:[],
                    timeH:'12',
                    timeM:'00',
                    interval:0,
                    intervalOptions:['01','01'],
                    scheduledFormats:{},
                    t: window.translated_labels,
                    userPermissions: window.userPermissions,
                    moduleName: window.moduleName
                };

                var accessManager = new reportAccessManager({data:accessManagerOpt});
                var scheduleManager = new reportScheduleManager({data: scheduleManagerOpt});

                // Set dialog modal options
                var dialogOptions = {
                    modal: true,
                    draggable:true,
                    resizable: false,
                    width:755,
                    titl$e: $t.label_mass_schedule_title,
                    buttons: [
                        {
                            text: $t.label_save,
                            click: function() {
                                var that = this;

                                var params = {};
                                params.globalSharing = accessManager.get("globalSharing");
                                params.globalSharingRights = accessManager.get("globalSharingRights");
                                params.userSharing = accessManager.getSelectedAccess();
                                params.owner = accessManager.get("owner");
                                params.isScheduled = (scheduleManager.get("isScheduled")==true)?(1):(0);
                                params.updatePermissions = (accessManager.get("updatePermissions")==true)?(1):(0);//#5286
                                params.interval = scheduleManager.get("interval");
                                params.intervalH = scheduleManager.get("timeH");
                                params.intervalM = scheduleManager.get("timeM");
                                params.intervalOptions = scheduleManager.get("intervalOptions");
                                params.scheduledRecipients = scheduleManager.getSelectedRecipients();
                                params.scheduledFormats = scheduleManager.get("scheduledFormats");
                                params.reportIds = reportIds;
                                params.options = $ReportData.options;

                                // Add CSRF post tokens
                                if(csrf && csrf.name && csrf.value) {
                                    params[csrf.name] = csrf.value
                                }

                                // Params keys which should not be converted to JSON (@TODO: Should make DRY)
                                var nonJSONSaveKeys = [
                                    "reportIds","globalSharing", "globalSharingRights", "owner","isScheduled",
                                    "updatePermissions", "interval", "intervalH", "intervalM", "intervalOptions",
                                    "scheduledRecipients", "csrf_token"
                                ];
                                // Send to server
                                jQuery.post(massScheduleUrl, reportUtils.convertToJSONPost(params, nonJSONSaveKeys), function(response){
                                    var response = JSON.parse(response);
                                    jQuery(that).dialog("close");
                                });
                            }
                        },
                        {
                            text: $t.label_cancel,
                            click: function() {
                                jQuery(this).dialog("close");
                            }
                        }
                    ]
                }

                // Initialize Ractive.js modal component with data
                var modal = new RactiveDialog({
                    data: {
                        dialogOptions:dialogOptions,
                        init:function(){
                            accessManager.render("#ar-rv-editor-access-sharing");
                            scheduleManager.render("#ar-rv-editor-access-scheduling");
                        }
                    },
                    partials: {
                        modalContent: '<div class="ar-container"><div id="ar-rv-editor-access"><strong>'+$t.label_sharing+'</strong><br><div id="ar-rv-editor-access-sharing"></div><br><strong>'+$t.label_scheduling+'</strong><br><div id="ar-rv-editor-access-scheduling"></div></div></div>'
                    }
                });
            }else{
                alert("Please select at least one report!");
            }
        });

        jQuery("[data-action='export']").click(function(e) {
            e.preventDefault();
            var reportIds = [];

            // Get all checked node ids
            jsTree.jstree("get_checked").forEach(function(id){
                // Filter reports
                if(id.substring(0, 2) != "c_") {
                    reportIds.push(id);
                }
            });

            // Show dialog only if there is at least one report selected
            if(reportIds.length > 0) {
                var params = '';
                reportIds.forEach(function(id){
                    params+= '&reportIds[]='+id;
                });
                window.location = exportReportsUrl + '&reportIds=' + params;
            }else{
                alert($t.label_err_select_reports);
            }
        });

        jQuery("[data-action='import']").click(function(e) {
            e.preventDefault();

            var dialogOptions = {
                modal: true,
                draggable:true,
                resizable: false,
                width:450,
                titl$e: $t.label_import_wizard,
                buttons: [
                    {
                        text: $t.label_import,
                        click: function() {
                            if(jQuery("[name=importType]:checked").val() == "inCategory" && jQuery("[name=category]").val() == -1){
                                alert($t.label_err_select_folder);
                            } else if(!jQuery("[name=importFile]").val() || jQuery("[name=importFile]").val().substr(-4) != ".xml") {
                                alert($t.label_err_xml_extension);
                            } else {
                                jQuery("#import-reports").submit();
                                jQuery(this).dialog("close");
                            }
                        }
                    },
                    {
                        text: $t.label_cancel,
                        click: function() {
                            jQuery(this).dialog("close");
                        }
                    }
                ]
            };

            var ImportWizard = Ractive.extend({
                template: '#importReportsTemplate',
                oninit:function(){
                    var that = this;
                    var data = this.data;
                },
                data:{
                    categories:[],
                    t: $t,
                    moduleName:window.moduleName,
                },
            });

            // Initialize Ractive.js modal component with data
            var modal = new RactiveDialog({
                data: {
                    dialogOptions:dialogOptions,
                    init:function(){
                        var importOptions = {categories: $categories};
                        var importWizard = new ImportWizard({data:importOptions});
                        importWizard.render("#ar-rv-import-wizard");
                    }
                    },
                        partials: {
                            modalContent: '<div id="ar-rv-import-wizard"></div>'
                        }
                    });
        });

        function checkImportErrors(){
            var checkErrorsUrl = $urls.importReportsUrl;

            var newData = {
                _action: 'errorCheck'
            };

            // Add CSRF post tokens
            if(csrf && csrf.name && csrf.value) {
                newData[csrf.name] = csrf.value
            }

            jQuery.post(checkErrorsUrl, newData, function(response){
                response = JSON.parse(response);
                if(response.status != "ok"){
                    var dialogOptions = {
                        modal: true,
                        draggable:true,
                        resizable: false,
                        width:250,
                        title: $t.label_import_error,
                        buttons: [
                            {
                                text: $t.label_close,
                                click: function() {
                                    jQuery(this).dialog("close");
                                }
                            }
                        ]
                    };

                    var modal = new RactiveDialog({
                        data: {
                            dialogOptions:dialogOptions,
                        },
                        partials: {
                            modalContent: '<div style="text-align:center;">'+t[response.errorMsg]+'</div>'
                        }
                    });
                }
            });
        }

        checkImportErrors();

        // #6076 - Added font checking and downloading frontend
        function checkFonts() {
            // Check for additional font existence
            function downloadFontsCheck(modalObj) {
                var downloadFontCheckUrl = $urls.downloadFontCheckUrl;
                jQuery("#status").show();
                jQuery.getJSON(downloadFontCheckUrl, function(response) {
                    jQuery("#status").hide();
                    if(response.status) {
                        // Close dialog
                        jQuery(modalObj).dialog("close");
                    } else {
                        // Set dialog modal options
                        var dialogOptions = {
                            modal: true,
                            draggable:true,
                            resizable: false,
                            width:400,
                            title: $t.additionalFontsInfo,
                            buttons: {
                                "downloadButton": {
                                    text: $t.download,
                                    class: 'downloadFontsButton',
                                    click: function() {
                                        jQuery(".downloadFontsButton > .ui-button-text").text("Downloading...");
                                        downloadFonts(jQuery(this));
                                    }
                                },
                                "cancelButton": {
                                    text: $t.label_cancel,
                                    click: function() {
                                        jQuery(this).dialog("close");
                                    }
                                }
                            }
                        }

                        var modal = new RactiveDialog({
                            data: {
                                dialogOptions:dialogOptions,
                                init:function(){
                                }
                            },
                            partials: {
                                modalContent: '<div>'+response.error+'</div>'
                            }
                        });
                    }
                });
            }

            // Download fonts in background
            function downloadFonts(modal) {
                var downloadFontUrl = $urls.downloadFontUrl;
                jQuery("#downloadFontsStatus").html(""+$t.downloading+"");
                jQuery("#status").show();
                jQuery.getJSON(downloadFontUrl, function(response) {
                    jQuery("#status").hide();
                    downloadFontsCheck(modal);
                });
            }

            downloadFontsCheck();
        }

        checkFonts();

        // After jsTree has been loaded, set valid widths in percents
        setTimeout(function(){
            jQuery(".jstree-grid-header th:nth-child(1)").css({width:"30%"});
            jQuery(".jstree-grid-header th:nth-child(2)").css({width:"55%"});
            jQuery(".jstree-grid-header th:nth-child(3)").css({width:"15%"});
        }, 600);
    });

})(this);