<?php

$mod_strings = array_merge($mod_strings,
	array(
		'LBL_SENDSCHEDULEDREPORTS' => "Analytic Reporting Send Scheduled Reports",
		'LBL_SENDSCHEDULEDDASHBOARDS' => "Analytic Reporting Send Scheduled Dashboard",
		'LBL_EXECUTEARJOBS' => "Analytic Reporting Processes",
	)
);