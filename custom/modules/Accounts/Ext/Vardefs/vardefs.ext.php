<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2020-09-01 11:04:27
$dictionary['Account']['fields']['npr_c'] = array(
    'name' => 'npr_c',
    'vname' => 'LBL_NPR',
    'type' => 'enum',
    'options' => 'yes_no_list',
    'required' => false,
    'audited' => true,
    'reportable' => true,
    'default' => '',
);

$dictionary['Account']['fields']['nda_c'] = array(
    'name' => 'nda_c',
    'vname' => 'LBL_NDA',
    'type' => 'enum',
    'options' => 'yes_no_list',
    'required' => false,
    'audited' => true,
    'reportable' => true,
    'default' => '',
);

$dictionary['Account']['fields']['potential_launch_c'] = array(
    'name' => 'potential_launch_c',
    'vname' => 'LBL_POTENTIAL_LAUNCH',
    'type' => 'date',
    'required' => false,
    'audited' => true,
    'reportable' => true,
);

$dictionary['Account']['fields']['success_rate_c'] = array(
    'name' => 'success_rate_c',
    'vname' => 'LBL_SUCCESS_RATE',
    'type' => 'float',
    'required' => false,
    'audited' => true,
    'reportable' => true,
);

$dictionary['Account']['fields']['projects_won_c'] = array(
    'name' => 'projects_won_c',
    'vname' => 'LBL_PROJECTS_WON',
    'type' => 'float',
    'required' => false,
    'audited' => true,
    'reportable' => true,
);

$dictionary['Account']['fields']['sales_c'] = array(
    'name' => 'sales_c',
    'vname' => 'LBL_SALES',
    'type' => 'float',
    'required' => false,
    'audited' => true,
    'reportable' => true,
);

$dictionary['Account']['fields']['production_site_c'] = array(
    'name' => 'production_site_c',
    'vname' => 'LBL_PRODUCTION_SITE',
    'type' => 'varchar',
    'required' => false,
    'audited' => true,
    'reportable' => true,
);

$dictionary['Account']['fields']['potential_impact_usd_budget_nineteen_c'] = array(
    'name' => 'potential_impact_usd_budget_nineteen_c',
    'vname' => 'LBL_POTENTIAL_IMPACT_USD_BUDGET_NINETEEN',
    'type' => 'currency',
    'precision' => 2,
    'required' => false,
    'audited' => true,
    'reportable' => true,
);

$dictionary['Account']['fields']['potential_impact_usd_budget_twenty_c'] = array(
    'name' => 'potential_impact_usd_budget_twenty_c',
    'vname' => 'LBL_POTENTIAL_IMPACT_USD_BUDGET_TWENTY',
    'type' => 'currency',
    'precision' => 2,
    'required' => false,
    'audited' => true,
    'reportable' => true,
);

$dictionary['Account']['fields']['potential_impact_usd_budget_twentyone_c'] = array(
    'name' => 'potential_impact_usd_budget_twentyone_c',
    'vname' => 'LBL_POTENTIAL_IMPACT_USD_BUDGET_TWENTYONE',
    'type' => 'currency',
    'required' => false,
    'audited' => true,
    'reportable' => true,
    'precision' => 2,
);



 // created: 2020-11-04 08:07:35
$dictionary['Account']['fields']['customer_group5_c']['inline_edit']='';
$dictionary['Account']['fields']['customer_group5_c']['labelValue']='Customer Group 5';

 

 // created: 2020-09-01 22:20:26
$dictionary['Account']['fields']['projects_won_c']['inline_edit']=true;
$dictionary['Account']['fields']['projects_won_c']['merge_filter']='disabled';
$dictionary['Account']['fields']['projects_won_c']['enable_range_search']=false;

 

 // created: 2020-11-04 08:04:40
$dictionary['Account']['fields']['customer_group1_market_segm_c']['inline_edit']='';
$dictionary['Account']['fields']['customer_group1_market_segm_c']['labelValue']='Customer Group 1/Market Segment';

 

 // created: 2019-02-21 03:39:45
$dictionary['Account']['fields']['jjwg_maps_lat_c']['inline_edit']=1;

 

 // created: 2020-11-04 08:09:17
$dictionary['Account']['fields']['terms_of_payment_c']['inline_edit']='';
$dictionary['Account']['fields']['terms_of_payment_c']['labelValue']='Terms of Payment';

 

 // created: 2020-11-08 09:43:45
$dictionary['Account']['fields']['customer_number_c']['inline_edit']='';
$dictionary['Account']['fields']['customer_number_c']['labelValue']='Customer Number';

 

 // created: 2019-02-21 03:39:45
$dictionary['Account']['fields']['jjwg_maps_address_c']['inline_edit']=1;

 

 // created: 2020-11-21 18:08:10
$dictionary['Account']['fields']['annual_revenue_year_c']['inline_edit']='1';
$dictionary['Account']['fields']['annual_revenue_year_c']['labelValue']='Annual Revenue Year';

 

 // created: 2016-08-15 22:08:49
$dictionary['Account']['fields']['total_hours_at'] = array(
	  'labelValue' => 'Total Hours',
      'required' => false,
      'name' => 'total_hours_at',
      'vname' => 'LBL_TOTAL_HOURS_AT',
      'type' => 'decimal',
      'massupdate' => '0',
      'default' => '',
      'no_default' => false,
      'comments' => '',
      'help' => '',
      'importable' => 'true',
      'duplicate_merge' => 'disabled',
      'duplicate_merge_dom_value' => '0',
      'audited' => false,
      'reportable' => true,
      'unified_search' => false,
      'merge_filter' => 'disabled',
      'len' => '18',
      'size' => '20',
      'enable_range_search' => false,
      'precision' => '2',
);

 

 // created: 2020-11-04 08:09:55
$dictionary['Account']['fields']['customer_group3_c']['inline_edit']='';
$dictionary['Account']['fields']['customer_group3_c']['labelValue']='customer group 3';

 

 // created: 2020-11-03 14:01:58
$dictionary['Account']['fields']['potential_impact_usd_budget_nineteen_c']['inline_edit']=true;
$dictionary['Account']['fields']['potential_impact_usd_budget_nineteen_c']['merge_filter']='disabled';
$dictionary['Account']['fields']['potential_impact_usd_budget_nineteen_c']['enable_range_search']=false;
$dictionary['Account']['fields']['potential_impact_usd_budget_nineteen_c']['vname']='Potential impact USD budget 2020';
 

 // created: 2020-09-05 12:02:40
$dictionary['Account']['fields']['account_type']['default']='';
$dictionary['Account']['fields']['account_type']['len']=100;
$dictionary['Account']['fields']['account_type']['inline_edit']=true;
$dictionary['Account']['fields']['account_type']['comments']='The Company is of this type';
$dictionary['Account']['fields']['account_type']['merge_filter']='disabled';

 

 // created: 2020-11-04 05:04:07
$dictionary['Account']['fields']['last_interaction_date_c']['inline_edit']='';
$dictionary['Account']['fields']['last_interaction_date_c']['labelValue']='Last Interaction Date';

 

 // created: 2020-11-04 08:06:58
$dictionary['Account']['fields']['customer_group4_custclass_c']['inline_edit']='';
$dictionary['Account']['fields']['customer_group4_custclass_c']['labelValue']='Customer Group 4 (Customer Class)';

 

 // created: 2020-11-03 09:06:59
$dictionary['Account']['fields']['potential_impact_usd_budget_twenty_c']['inline_edit']=true;
$dictionary['Account']['fields']['potential_impact_usd_budget_twenty_c']['merge_filter']='disabled';
$dictionary['Account']['fields']['potential_impact_usd_budget_twenty_c']['enable_range_search']=false;
$dictionary['Account']['fields']['potential_impact_usd_budget_nineteen_c']['vname']='Potential impact USD budget 2022';
 

 // created: 2020-11-11 06:40:49
$dictionary['Account']['fields']['po_box_c']['inline_edit']='1';
$dictionary['Account']['fields']['po_box_c']['labelValue']='PO Box';

 

 // created: 2019-02-21 03:39:45
$dictionary['Account']['fields']['jjwg_maps_geocode_status_c']['inline_edit']=1;

 

// created: 2020-11-03 16:17:29
$dictionary["Account"]["fields"]["ahc_key_accounts_accounts_1"] = array (
  'name' => 'ahc_key_accounts_accounts_1',
  'type' => 'link',
  'relationship' => 'ahc_key_accounts_accounts_1',
  'source' => 'non-db',
  'module' => 'ahc_Key_Accounts',
  'bean_name' => 'ahc_Key_Accounts',
  'vname' => 'LBL_AHC_KEY_ACCOUNTS_ACCOUNTS_1_FROM_AHC_KEY_ACCOUNTS_TITLE',
  'id_name' => 'ahc_key_accounts_accounts_1ahc_key_accounts_ida',
);
$dictionary["Account"]["fields"]["ahc_key_accounts_accounts_1_name"] = array (
  'name' => 'ahc_key_accounts_accounts_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_AHC_KEY_ACCOUNTS_ACCOUNTS_1_FROM_AHC_KEY_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'ahc_key_accounts_accounts_1ahc_key_accounts_ida',
  'link' => 'ahc_key_accounts_accounts_1',
  'table' => 'ahc_key_accounts',
  'module' => 'ahc_Key_Accounts',
  'rname' => 'name',
);
$dictionary["Account"]["fields"]["ahc_key_accounts_accounts_1ahc_key_accounts_ida"] = array (
  'name' => 'ahc_key_accounts_accounts_1ahc_key_accounts_ida',
  'type' => 'link',
  'relationship' => 'ahc_key_accounts_accounts_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_AHC_KEY_ACCOUNTS_ACCOUNTS_1_FROM_ACCOUNTS_TITLE',
);


/*********************************************************************************
 * This file is part of package Price Books.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Price Books is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/

/*
 * Relationship definition
 */
$dictionary['Account']['relationships']['account_vi_price_books'] = array(
    'lhs_module'        => 'Accounts',
    'lhs_table'         => 'accounts',
    'lhs_key'           => 'id',
    'rhs_module'        => 'VI_Price_Books',
    'rhs_table'         => 'vi_price_books',
    'rhs_key'           => 'account_id',
    'relationship_type' => 'one-to-many',
);

$dictionary['Account']['fields']['vi_price_books'] = array(
    'name'         => 'vi_price_books',
    'type'         => 'link',
    'relationship' => 'account_vi_price_books',
    'module'       => 'VI_Price_Books',
    'bean_name'    => 'VI_Price_Books',
    'source'       => 'non-db',
    'vname'        => 'LBL_PRICE_BOOKS_LABEL',
);



 // created: 2020-11-19 04:25:26
$dictionary['Account']['fields']['phone_office']['required']=true;
$dictionary['Account']['fields']['phone_office']['inline_edit']=true;
$dictionary['Account']['fields']['phone_office']['comments']='The office phone number';
$dictionary['Account']['fields']['phone_office']['merge_filter']='disabled';

 

 // created: 2020-11-03 09:04:44
$dictionary['Account']['fields']['potential_launch_c']['inline_edit']=true;
$dictionary['Account']['fields']['potential_launch_c']['merge_filter']='disabled';
$dictionary['Account']['fields']['potential_launch_c']['enable_range_search']=false;
$dictionary['Account']['fields']['potential_impact_usd_budget_nineteen_c']['vname']='Potential impact USD 2020';
 

 // created: 2020-11-04 05:05:26
$dictionary['Account']['fields']['currency_c']['inline_edit']='';
$dictionary['Account']['fields']['currency_c']['labelValue']='Currency';

 

 // created: 2020-11-04 04:49:12
$dictionary['Account']['fields']['customer_type_c']['inline_edit']='';
$dictionary['Account']['fields']['customer_type_c']['labelValue']='Customer Type';

 

 // created: 2020-11-04 04:44:53
$dictionary['Account']['fields']['sales_organization_c']['inline_edit']='';
$dictionary['Account']['fields']['sales_organization_c']['labelValue']='Sales Organization';

 

 // created: 2020-11-11 06:39:06
$dictionary['Account']['fields']['vat_registration_number_c']['inline_edit']='1';
$dictionary['Account']['fields']['vat_registration_number_c']['labelValue']='VAT Registration Number';

 

 // created: 2020-11-04 04:57:41
$dictionary['Account']['fields']['customer_group2_csd_c']['inline_edit']='';
$dictionary['Account']['fields']['customer_group2_csd_c']['labelValue']='Customer Group 2 (CSD)';

 

// created: 2016-09-26 21:38:56
$dictionary["Account"]["fields"]["accounts_sp_timesheet_tracker_1"] = array (
  'name' => 'accounts_sp_timesheet_tracker_1',
  'type' => 'link',
  'relationship' => 'accounts_sp_timesheet_tracker_1',
  'source' => 'non-db',
  'module' => 'sp_timesheet_tracker',
  'bean_name' => 'sp_timesheet_tracker',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_SP_TIMESHEET_TRACKER_1_FROM_SP_TIMESHEET_TRACKER_TITLE',
);


 // created: 2020-11-11 06:24:57
$dictionary['Account']['fields']['name2_c']['inline_edit']='1';
$dictionary['Account']['fields']['name2_c']['labelValue']='Name 2';

 

 // created: 2020-11-04 08:08:37
$dictionary['Account']['fields']['incoterms_part2_c']['inline_edit']='';
$dictionary['Account']['fields']['incoterms_part2_c']['labelValue']='Incoterms (Part 2)';

 

 // created: 2020-11-11 06:40:24
$dictionary['Account']['fields']['transportation_zone_c']['inline_edit']='1';
$dictionary['Account']['fields']['transportation_zone_c']['labelValue']='Transportation Zone';

 

 // created: 2020-11-11 06:38:34
$dictionary['Account']['fields']['customer_account_group_c']['inline_edit']='1';
$dictionary['Account']['fields']['customer_account_group_c']['labelValue']='Customer Account Group';

 

 // created: 2019-02-21 03:39:45
$dictionary['Account']['fields']['jjwg_maps_lng_c']['inline_edit']=1;

 

 // created: 2020-11-04 08:08:05
$dictionary['Account']['fields']['incoterms_part1_c']['inline_edit']='';
$dictionary['Account']['fields']['incoterms_part1_c']['labelValue']='Incoterms (Part 1)';

 
?>