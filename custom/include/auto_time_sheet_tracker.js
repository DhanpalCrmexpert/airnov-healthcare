	var curr_module = $('#formDetailView [name="module"]').val();
	
	if(curr_module == "ProjectTask" || curr_module == "Cases"){
	
		//Create start timer button   
		var element = document.createElement("input");
		//Assign different attributes to the element. 
		element.type = "button";
		element.value = "Start Timer";
		element.name = "start_timer";
		element.id = "start_timer";
		element.onclick = function() {
		   
			var related_record_id = $('#formDetailView [name="record"]').val();
			var module = $('#formDetailView [name="module"]').val();
			
			start_timesheet_timer(related_record_id, module);
		};
		
		// var parentGuest = document.getElementById("create_link");
		var parentGuest = document.getElementById("tab-actions");
		if(typeof(parentGuest) == "undefined" || parentGuest == "null" || parentGuest == null || parentGuest == ""){
			parentGuest = document.getElementById("delete_button");
			if(typeof(parentGuest) == "undefined" || parentGuest == "null" || parentGuest == null || parentGuest == ""){
				parentGuest = document.getElementById("formDetailView");
			}
		}
		
		if (parentGuest.nextSibling) {
		  parentGuest.parentNode.insertBefore(element, parentGuest.nextSibling);
		}
		else {
		  parentGuest.parentNode.appendChild(element);
		}
		
		
		//Create end timer button   
		var element_end_timer = document.createElement("input");
		//Assign different attributes to the element. 
		element_end_timer.type = "button";
		element_end_timer.value = "End Timer";
		element_end_timer.name = "end_timer";
		element_end_timer.id = "end_timer";
		element_end_timer.onclick = function() {
		   
			var related_record_id = $('#formDetailView [name="record"]').val();
			var module = $('#formDetailView [name="module"]').val();
			var timerRecordId = $("#timeRecordId").text();
			end_timesheet_timer(related_record_id, module,timerRecordId);
		};
		
		if (parentGuest.nextSibling) {
		  parentGuest.parentNode.insertBefore(element_end_timer, parentGuest.nextSibling);
		}
		else {
		  parentGuest.parentNode.appendChild(element_end_timer);
		}
	}


function start_timesheet_timer(related_record_id, module){	
	var callback = {
		success:function(o){
			alert(o.responseText);
			window.location.reload(true);
		}
	}
	postData = '&sugar_body_only=true&related_record_id=' + related_record_id + '&related_module='+module;

	YAHOO.util.Connect.asyncRequest('POST', 'index.php?module=sp_Timesheet&action=autoTrackTime', callback, postData);
}

function end_timesheet_timer(related_record_id, module, timerRecordId=null){	
	var callback = {
		success:function(o){
			alert(o.responseText);
			// window.location.reload(true);
		}
	}
	postData = '&sugar_body_only=true&related_record_id=' + related_record_id + '&related_module='+module+'&timer_record_id='+timerRecordId;

	YAHOO.util.Connect.asyncRequest('POST', 'index.php?module=sp_Timesheet&action=endAutoTrackTime', callback, postData);
}