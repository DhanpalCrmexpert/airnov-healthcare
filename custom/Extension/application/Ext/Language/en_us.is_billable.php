<?php

$app_list_strings['is_billable_list'] = array(
	'yes' => 'Yes',
	'no' => 'No'
);
$app_strings['LNK_NEW_RECORD'] = 'Create Daily Timesheet';
$app_strings['LNK_NEW_RECORD_WEEKLY'] = 'Create Weekly Timesheet';
$app_strings['LNK_LIST'] = 'View Timesheet';
$app_strings['LNK_TIMESHEET_SETTINGS'] = 'Timesheet Settings';