<?php
 // created: 2020-11-19 04:25:02
$dictionary['Contact']['fields']['phone_work']['required']=true;
$dictionary['Contact']['fields']['phone_work']['inline_edit']=true;
$dictionary['Contact']['fields']['phone_work']['comments']='Work phone number of the contact';
$dictionary['Contact']['fields']['phone_work']['merge_filter']='disabled';

 ?>