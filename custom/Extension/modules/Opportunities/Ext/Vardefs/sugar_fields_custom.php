<?php
 // created: 2020-09-01 11:04:27
$dictionary['Opportunity']['fields']['item_description_c'] = array(
    'name' => 'item_description_c',
    'vname' => 'LBL_ITEM_DESCRIPTION',
    'type' => 'text',
    'required' => false,
    'audited' => true,
    'reportable' => true,
);

$dictionary['Opportunity']['fields']['quantity_c'] = array(
    'name' => 'quantity_c',
    'vname' => 'LBL_QUANTITY_C',
    'type' => 'float',
    'required' => false,
    'audited' => true,
    'reportable' => true,
);

$dictionary['Opportunity']['fields']['npr_c'] = array(
    'name' => 'npr_c',
    'vname' => 'LBL_NPR',
    'type' => 'enum',
    'options' => 'yes_no_list',
    'required' => false,
    'audited' => true,
    'reportable' => true,
    'default' => '',
);

$dictionary['Opportunity']['fields']['nda_c'] = array(
    'name' => 'nda_c',
    'vname' => 'LBL_NDA',
    'type' => 'enum',
    'options' => 'yes_no_list',
    'required' => false,
    'audited' => true,
    'reportable' => true,
    'default' => '',
);

$dictionary['Opportunity']['fields']['potential_launch_c'] = array(
    'name' => 'potential_launch_c',
    'vname' => 'LBL_POTENTIAL_LAUNCH',
    'type' => 'date',
    'required' => false,
    'audited' => true,
    'reportable' => true,
);

$dictionary['Opportunity']['fields']['success_rate_c'] = array(
    'name' => 'success_rate_c',
    'vname' => 'LBL_SUCCESS_RATE',
    'type' => 'float',
    'required' => false,
    'audited' => true,
    'reportable' => true,
);

$dictionary['Opportunity']['fields']['production_site_c'] = array(
    'name' => 'production_site_c',
    'vname' => 'LBL_PRODUCTION_SITE',
    'type' => 'varchar',
    'required' => false,
    'audited' => true,
    'reportable' => true,
);

?>