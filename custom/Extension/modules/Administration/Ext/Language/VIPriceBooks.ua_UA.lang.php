<?php
/*********************************************************************************
 * This file is part of package Price Books.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Price Books is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
$mod_strings['LBL_PRICE_BOOKS'] = "Цінові книги";
$mod_strings['LBL_PRICE_BOOKS_DESCRIPTION'] = "Управління ціновими книгами продуктів для конкретних контактів / облікових записів.";
$mod_strings['LBL_ENABLE_PRICE_BOOKS'] = "Увімкнути функцію цінних книг";
$mod_strings['LBL_PRICE_BOOKS_ACTIVATED'] = "Функція цінових книг успішно активована.";
$mod_strings['LBL_PRICE_BOOKS_DEACTIVATED'] = "Цінова функція книг успішно деактивована.";
$mod_strings['LBL_CLICK_HERE'] = "Натисніть тут";
$mod_strings['LBL_MANAGE_PRICEBOOK'] = "Управління ціновими книгами";
//license
$mod_strings['LBL_LICENCE_ACTIVE_LABEL'] = "VIPriceBooksLicenseAddon більше не активний через наступну причину:";
$mod_strings['LBL_LICENCE_ISSUES'] = "Користувачі матимуть доступ до доступу, поки проблема не буде вирішена";
$mod_strings['LBL_CLICK_HERE'] = "Натисніть тут";
$mod_strings['LBL_LICENCE_ACTIVE'] = "VIPriceBooksLicenseAddon більше не активний";
$mod_strings['LBL_RENEW_LICENCE'] = "Поновіть підписку або перевірте конфігурацію ліцензії.";
$mod_strings['LBL_UPDATE_LICENSE'] = "Оновити ліцензію";
?>