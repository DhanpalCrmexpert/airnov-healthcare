<?php

function getUsersList(){
	static $userList = null;
	if(!$userList){
		global $db;
		
		$query = "select id, CONCAT_WS(' ',first_name,last_name) as 'name' from users where deleted = 0 AND `status` = 'active' order by name asc ";
		
		$result = $db->query($query, false);

		$userList = array();
		$userList[''] = '';

		while (($row = $db->fetchByAssoc($result)) != null) {
			$userList[$row['id']] = $row['name'];
		}

	}
	return $userList;
}

function getModulesList(){
	global $app_list_strings;
	$module_list = $app_list_strings['moduleList'];
	return $module_list;
}

function getRelatedModulesList(){
	global $app_list_strings;
	$related_module_list = [];
	foreach($app_list_strings['moduleList'] as $key => $value){
		$related_module_list = array_merge($related_module_list, getRelatedModuleList($key));
	}
	return $related_module_list;
}

function getRelatedModuleList($selected_module){
	global $app_list_strings;
	if(empty($selected_module)){
		return $value = array(''=>'Select Related Module');
	}
	else{
		$module_meta_data_fields = '';
		$bean = BeanFactory::getBean($selected_module);
		if(!empty($bean)){
			$module_meta_data_fields = $bean->getFieldDefinitions();
		}
		
		$modules_list = array();
		
		if(!empty($module_meta_data_fields)){	
			foreach($module_meta_data_fields as $module_field_key=>$module_field_val){
				$include = true;
				if(is_object($module_field_val) || is_array($module_field_val)){	
					foreach($module_field_val as $key=>$val){
						// Add the types you don't want to send
						if($key == "relationship"){
							$related_module_name = getRelatedModuleNameCustomFunc($bean, $module_field_key);
							if($related_module_name != "-1"  && !empty($related_module_name)){
								$related_module_val = $app_list_strings['moduleList'][$related_module_name];
								if(!empty($related_module_val)){
									$modules_list[$selected_module."_".$module_field_key] = $related_module_val;
								}
							}
						}
					}
				}
			}	
		 }
		return $modules_list;
	}
}

function getRelatedModuleNameCustomFunc($module_obj, $relationship_name){
	if($module_obj->load_relationship($relationship_name)){
		$related_module = $module_obj->$relationship_name->getRelatedModuleName();
		return $related_module;
	}
	return '-1';
}

?>