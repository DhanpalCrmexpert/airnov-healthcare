<?php
// created: 2020-11-19 07:31:39
$mod_strings = array (
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Date Created',
  'LBL_DATE_MODIFIED' => 'Date Modified',
  'LBL_MODIFIED' => 'Modified By',
  'LBL_MODIFIED_NAME' => 'Modified By Name',
  'LBL_CREATED' => 'Created By',
  'LBL_DESCRIPTION' => 'Description',
  'LBL_DELETED' => 'Deleted',
  'LBL_NAME' => 'Name',
  'LBL_CREATED_USER' => 'Created by User',
  'LBL_MODIFIED_USER' => 'Modified by User',
  'LBL_LIST_NAME' => 'Name',
  'LBL_EDIT_BUTTON' => 'Edit',
  'LBL_REMOVE' => 'Remove',
  'LBL_ASCENDING' => 'Ascending',
  'LBL_DESCENDING' => 'Descending',
  'LBL_OPT_IN' => 'Opt In',
  'LBL_OPT_IN_PENDING_EMAIL_NOT_SENT' => 'Pending Confirm opt in, Confirm opt in not sent',
  'LBL_OPT_IN_PENDING_EMAIL_SENT' => 'Pending Confirm opt in, Confirm opt in sent',
  'LBL_OPT_IN_CONFIRMED' => 'Opted in',
  'LBL_ASSIGNED_TO_ID' => 'Assigned User Id',
  'LBL_ASSIGNED_TO_NAME' => 'Assigned to',
  'LBL_MODIFIED_ID' => 'Modified By Id',
  'LBL_CREATED_ID' => 'Created By Id',
  'LBL_LIST_FORM_TITLE' => 'Timesheet List',
  'LBL_MODULE_NAME' => 'Timesheet',
  'LBL_MODULE_TITLE' => 'Timesheet',
  'LBL_HOMEPAGE_TITLE' => 'My Timesheet',
  'LNK_NEW_RECORD' => 'Create Daily Timesheet',
  'LNK_NEW_RECORD_WEEKLY' => 'Create Weekly Timesheet',
  'LNK_LIST' => 'View Timesheet',
  'LNK_IMPORT_SP_TIMESHEET' => 'Import Timesheet',
  'LBL_SEARCH_FORM_TITLE' => 'Search Timesheet',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'View History',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activities',
  'LBL_SP_TIMESHEET_SUBPANEL_TITLE' => 'Timesheet',
  'LBL_NEW_FORM_TITLE' => 'New Timesheet',
  'LBL_TRACK_DATE_FROM' => 'From',
  'LBL_TRACK_DATE_TO' => 'To',
  'LBL_TRACK_USER' => 'Employee',
  'LBL_HOURS' => 'Hours',
  'LBL_CONVERT_TO_INVOICE' => 'Convert to SuiteCRM Invoice',
  'LBL_RELATED_MODULE' => 'Related Module',
  'LBL_MODULE' => 'Module',
  'LNK_TIMESHEET_SETTINGS' => 'Timesheet Settings',
  'LBL_EXPORT_PDF' => 'Export to PDF',
  'LBL_EMAIL_PDF' => 'Email Timesheet',
);