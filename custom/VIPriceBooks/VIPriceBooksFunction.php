<?php
/*********************************************************************************
 * This file is part of package Price Books.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Price Books is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
//insert record
function insertPriceBooksRecord($tabelName,$fielddata){
    //data key
    $key = array_keys($fielddata);
    $fieldName = implode(",",$key);
    
    //data val
    $val = array_values($fielddata);
    $fieldVal = implode(",",$val);
    
    //insert
    $insertPriceBooksData = "INSERT INTO $tabelName ($fieldName)VALUES($fieldVal)";
    $insertPriceBooksDataResult = $GLOBALS['db']->query($insertPriceBooksData);

    return $insertPriceBooksDataResult;
}//end of function

//update record
function updatePriceBooksRecord($tabelName,$data,$where){

    //update
    $updatePriceBooksData = "UPDATE $tabelName SET";

    $fieldName = array_keys($data); //database field name
    $fieldValue = array_values($data); //field value

    $whereFieldName = array_keys($where); //where condition field name
    $whereFieldValue = array_values($where); // where condition field value

    $i=0;
    $count = count($data);
    foreach($data as $fieldData){
        if($count == $i+1){
            $updatePriceBooksData .= " $fieldName[$i]=$fieldValue[$i]";
        }else{
            $updatePriceBooksData .= " $fieldName[$i]=$fieldValue[$i],";
        }//end of else
        $i++;
    }//end of foreach
    if(!empty($where)){
       $j=0;
        $updatePriceBooksData .= " where";
        foreach($where as $whereConditionData){
            if($j == 0){
                $updatePriceBooksData .=" $whereFieldName[$j]='$whereFieldValue[$j]'";
            }else{
                $updatePriceBooksData .=" and $whereFieldName[$j]='$whereFieldValue[$j]'";
            }//end of else 
            $j++;
        }//end of foreach
    }
    $updatePriceBooksDataResult = $GLOBALS['db']->query($updatePriceBooksData);
    return $updatePriceBooksDataResult;
}//end of function

//delete record
function deletePriceBooksRecord($tabelName,$where){

    //delete
    $deletePriceBooksData = "DELETE FROM $tabelName";

    $whereFieldName = array_keys($where); //where condition field name
    $whereFieldValue = array_values($where); // where condition field value

    if(!empty($where)){
       $j=0;
        $deletePriceBooksData .= " where";
        foreach($where as $whereConditionData){
            if($j == 0){
                $deletePriceBooksData .=" $whereFieldName[$j]='$whereFieldValue[$j]'";
            }else{
                $deletePriceBooksData .=" and $whereFieldName[$j]='$whereFieldValue[$j]'";
            }//end of else 
            $j++;
        }//end of foreach
    }
    $deletePriceBooksDataResult = $GLOBALS['db']->query($deletePriceBooksData);
    return $deletePriceBooksDataResult;
}//end of function

function getPriceBooksRecord($tablename,$fieldNames,$where){
    $selectPriceBooksData = '';
    //select
    $selectPriceBooksData .= "SELECT ";
    foreach($fieldNames as $key => $value){
        if($key == 0){
            $selectPriceBooksData .= $value;
        }else{
            $selectPriceBooksData .= ",".$value;
        }
    }

    $selectPriceBooksData .= " from $tablename"; 
    $whereFieldName = array_keys($where); //where condition field name
    $whereFieldValue = array_values($where); // where condition field value

    $j=0;
    if(!empty($where)){
        $selectPriceBooksData .= " WHERE";
    }
    $count = count($where);
    foreach($where as $key => $w){
        $fieldName = $whereFieldName[$j];
        $fieldValue = $whereFieldValue[$j];
        if($count > 1 && $j >= 1){
            $selectPriceBooksData .=" AND $fieldName='$fieldValue'";
        }else{
            $selectPriceBooksData .=" $fieldName='$fieldValue'";
        }
        $j++;
    }//end of foreach
    return $selectPriceBooksData;
}//end of function

function getPriceBooksRecordsForQuotesInvoice($operator,$accountId,$contactId,$productId){
    $getProductEntryQuery = "select vi_pricebook_productentry.productentry,vi_pricebook_productentry.id from vi_pricebook_productentry  INNER JOIN vi_price_books ON vi_price_books.id = vi_pricebook_productentry.pricebooks_id where vi_price_books.active_c=1 and vi_price_books.deleted=0 and ";
    if($operator == "OR"){
        if($accountId != ""){
            $getProductEntryQuery .= "account_id='$accountId'";
        }else if($contactId != ""){
            $getProductEntryQuery .= "contact_id='$contactId'";
        }
    }else{
        $getProductEntryQuery .= "(account_id='$accountId' ".$operator." contact_id='$contactId')";
    }
   
    $getProductEntryQueryResult = $GLOBALS['db']->query($getProductEntryQuery);
    
    $priceBooksData = array();
    while($getProductEntryRows = $GLOBALS['db']->fetchByAssoc($getProductEntryQueryResult)){
        $productEntrydata = json_decode(html_entity_decode($getProductEntryRows['productentry']));
        $productIdsArray = array();
        foreach($productEntrydata as $key=>$productValues){
            foreach($productValues as $key=>$values){
                $productData = (array)$values;
                $productIdsArray[] = "'".$productData['product_id']."'"; 
            }
        }//end of foreach
        $id = $getProductEntryRows['id'];
        $getpriceBooksId = "SELECT pricebooks_id FROM vi_pricebook_productentry WHERE '$productId' IN (".implode(',',$productIdsArray).") and id='$id'";
        $getPriceBooksResult = $GLOBALS['db']->query($getpriceBooksId);

        while($getPriceBooksRows = $GLOBALS['db']->fetchByAssoc($getPriceBooksResult)){
            $priceBookId =  $getPriceBooksRows['pricebooks_id'];
            
            //Get Price Books Data
            $tableName = 'vi_price_books';
            $fields = array("*");
            $whereArray = array('id' => $priceBookId,'active_c'=>1,'deleted'=>0);
            $priceBooksQuery = getPriceBooksRecord($tableName,$fields,$whereArray);
            $getPriceBooksRow = $GLOBALS['db']->fetchOne($priceBooksQuery);
            //Store Price Books Data
            if(!empty($getPriceBooksRow)){
                $priceBooksData[] = $getPriceBooksRow;
            }
        }//end of while
    }//end of while
    return $priceBooksData;
}

function addProductTable(){
    $html = '<table id="productTable"><input type="hidden" name="productEntryCount" id="productEntryCount" value="0"/></table><button type="button" class="button add_product" name="add_product" onclick="addProduct()" style="margin-top: 10px;">'.translate('LBL_ADD_PRODUCT','VI_Price_Books').'</button>';
    echo $html;
}

function getPriceBooksHelpBoxHtml($url){
    global $suitecrm_version,$theme,$current_language;
    
    $helpBoxContent = '';
    $curl = curl_init();

    $postData = json_encode(array("suiteCRMVersion"=>$suitecrm_version,"themeName" => $theme,'currentLanguage' => $current_language));
   
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $postData);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER,false);
    $data = curl_exec($curl);

    $httpCode = curl_getinfo($curl,CURLINFO_HTTP_CODE);
    if($httpCode == 200){
        $helpBoxContent = $data;
    }//end of if
    curl_close($curl);

    return $helpBoxContent;
}//end of function  
?>