<?php
error_reporting('E_ALL ~ E_NOTICE ~ E_WARNING');
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/

class ContactsController extends SugarController
{
	function action_resendemailnotification()
	{
		global $current_user,$db;
		$logTime = gmdate('Y-m-d H:i:s');
		
		$sql="SELECT
		  emails.name
		  , emails_beans.bean_id
		  , emails_beans.bean_module
		  , emails_beans.campaign_data
		  , emails_text.description
		  , emails_text.description_html
		FROM emails
		  , emails_beans
		  , emails_text
		WHERE emails.id = emails_beans.email_id
			AND emails_text.email_id = emails.id
			AND emails.id = '".$_REQUEST['id']."'
			AND emails_beans.bean_id = '".$_REQUEST['tmp_parent_id']."'";
		$rs=$db->query($sql);
		$rows = $db->fetchByAssoc($rs);
		if($rows['bean_id']!='')
		{
			$key_account_user_detail =BeanFactory::getBean($rows['bean_module'],$rows['bean_id']);
			if($key_account_user_detail->email1!='')
			{
				require_once('include/SugarPHPMailer.php');
				require_once('modules/EmailTemplates/EmailTemplate.php');
				$emailTo=$key_account_user_detail->email1;
				//$template_id='16eed23a-c7c5-2181-5676-5f70ae2f7e49';
				//$template = new EmailTemplate();
				//$template->retrieve($template_id); 
				$emailObj = new Email();
				$defaults = $emailObj->getSystemDefaultEmail();
				$mail = new SugarPHPMailer();
				//$olead = new Contact();
				//$olead->retrieve('6245b560-f2b6-4bc9-87dc-53830bb576ef');
				$mail->IsHTML(true);
				//$emailSubject=$template->parse_template_bean($template->subject,'Contacts', $olead).' - '.$bean->name;
				//$emailBody=$template->parse_template_bean($template->body_html,'Contacts', $olead);
				
				$relatedBean = null;
				$mail->setMailerForSystem();
				$mail->From = $defaults['email'];
				$mail->FromName = $defaults['name']; 
				$mail->ClearAllRecipients();
				$mail->ClearReplyTos();
				$mail->Subject=$rows['name'];
				if($rows['bean_module']=='Accounts')
				{
					$mail->Body=str_replace('$contact_last_name',$key_account_user_detail->name, str_replace('$contact_first_name',$key_account_user_detail->name, str_replace('$contact_name',$key_account_user_detail->name,$rows['description_html'])));
				}
				else
				{
					$mail->Body=str_replace('$contact_last_name',$key_account_user_detail->last_name, str_replace('$contact_first_name',$key_account_user_detail->first_name, str_replace('$contact_name',$key_account_user_detail->name,$rows['description_html'])));
				}
				$mail->prepForOutbound();
				$mail->AddAddress($emailTo);
				//print_r($mail);
				if ($mail->Send()) {
					$emailObj->to_addrs= $emailTo;
					$emailObj->type= 'archived';
					$emailObj->deleted = '0';
					$emailObj->name = $mail->Subject; 
					$emailObj->description = $mail->Body; 
					$emailObj->description_html = $mail->Body;
					$emailObj->from_addr = $mail->From;
					$emailObj->parent_type = $rows['bean_module'];
					$emailObj->parent_id = $rows['bean_id'];
					$emailObj->date_sent = TimeDate::getInstance()->nowDb();
					$emailObj->modified_user_id = $current_user->id;
					$emailObj->assigned_user_id = $current_user->id;
					$emailObj->created_by = $current_user->id;
					$emailObj->status = 'sent';
					$emailObj->save();
					echo 'Email Sent Successfully!';
				}
				else
				{
					echo 'Email Not Sent!';
				}
			}
			else
			{
				echo 'Not Found Parent Record Email!';
			}
		}
		else
		{
			echo 'Somthing Went Wrong!';
		}
		//echo 1;
		exit;
	}
}
?>