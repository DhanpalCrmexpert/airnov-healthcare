<?php
/**
 * Copyright (c) SalesAgility Ltd.  All Rights Reserved.
 * Licensed under the GPL3 License.  See License in the project root
 * for license information.
 *
 * @package Outlook Addin for SuiteCRM
 * @copyright 2020 SalesAgility Ltd http://www.salesagility.com
 * @author SalesAgility <info@salesagility.com
 *
 */

if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

$cfg = new Configurator();
$manifestLocation = './custom/include/SA_Outlook/admin/entrypoint/manifest.xml';
$readFile = file_get_contents($manifestLocation);

if (!array_key_exists('site_url', $cfg->config) || $readFile === false) {
    die('Error Writing manifest.xml');
}

$siteUrl = $cfg->config['site_url'];

//Remove protocol from Site URL
$siteUrl = str_replace(array('http://', 'https://'), '', $siteUrl);

//If last character is '/' then remove it
$lastCharIndex = strlen($siteUrl) - 1;
if (substr($siteUrl, $lastCharIndex) === '/') {
    $siteUrl = substr($siteUrl, 0, $lastCharIndex);
}

$newManifest = str_replace('{{PLUGIN_URL}}', $siteUrl . '/public/plugins/SA_Outlook', $readFile);

header('Cache-Control: no-store, no-cache, must-revalidate, max-age=0');
header('Cache-Control: post-check=0, pre-check=0', false);
header('Pragma: no-cache');
header('Content-type: text/xml');
header('Content-Disposition: attachment; filename="manifest.xml";');
header('X-Content-Type-Options: nosniff');
header('Expires: ' . gmdate('D, d M Y H:i:s \G\M\T', time() + 2592000));
set_time_limit(0);
header('Content-Description: File Transfer');
echo($newManifest);

