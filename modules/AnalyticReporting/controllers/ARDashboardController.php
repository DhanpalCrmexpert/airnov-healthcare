<?php

class ARDashboardController extends ARController
{
    /**
     * Return array of front end application initial state
     *
     * @param $dashboardId
     * @return array
     */
    public function getAppInitialState($dashboardId)
    {
        return array();
    }

    public function displayDashboard($dashboardId)
    {
        // Not implemented yet
    }

    /**
     * Create new dashboard in category
     *
     * @param $dashboardId
     * @param $categoryIdId
     * @return bool
     */
    public function createDashboard($dashboardId, $categoryId)
    {
        return true;
    }

    /**
     * Save current dashboard (with filters, etc) and widgets
     *
     * @param DashboardDataModel $dataModel
     * @return bool
     */
    public function saveDashboard($dashboardId, $data)
    {
        return false;
    }

    /**
     * Delete dashboard from database
     *
     * @param $dashboardId
     */
    public function deleteDashboard($dashboardId)
    {
        return false;
    }

    /**
     * Save current dashboard widgets
     *
     * @param $dashboardId
     * @param array $widgets
     * @return bool
     */
//    public function saveDashboardWidgets($dashboardId, $widgets = array())
//    {
//        return false;
//    }

    /**
     * Retrieve current dashboard widget array
     *
     * @param $dashboardId
     * @return array
     */
    public function getDashboardWidgets($dashboardId)
    {
        return array();
    }

    /**
     * Retrieve list of available dashboards
     *
     * @return array
     */
    public function getDashboardList()
    {
        return array();
    }

    /**
     * Save current dashboard permissions
     *
     * @param DashboardDataModel $dataModel
     * @return bool
     */
    public function savePermissions($dashboardId, $data)
    {
        return false;
    }

    public function getDashboardTree()
    {
        return array();
    }

    /**
     * @param string $id
     * @param array $data
     *
     * @return array
     */
    public function saveSchedule($id, $data)
    {
        return [];
    }
}
