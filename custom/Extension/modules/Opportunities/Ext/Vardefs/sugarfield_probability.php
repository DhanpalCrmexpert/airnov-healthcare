<?php
 // created: 2020-10-27 15:49:16
$dictionary['Opportunity']['fields']['probability']['required']=true;
$dictionary['Opportunity']['fields']['probability']['inline_edit']=true;
$dictionary['Opportunity']['fields']['probability']['comments']='The probability of closure';
$dictionary['Opportunity']['fields']['probability']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['probability']['enable_range_search']=false;
$dictionary['Opportunity']['fields']['probability']['min']=0;
$dictionary['Opportunity']['fields']['probability']['max']=100;
$dictionary['Opportunity']['fields']['probability']['disable_num_format']='';

 ?>