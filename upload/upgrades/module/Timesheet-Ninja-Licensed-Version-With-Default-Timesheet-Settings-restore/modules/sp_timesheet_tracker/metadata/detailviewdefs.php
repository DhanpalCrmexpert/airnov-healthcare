<?php
$module_name = 'sp_timesheet_tracker';
$viewdefs [$module_name] = 
array (
  'DetailView' => 
  array (
    'templateMeta' => 
    array (
      'form' => 
      array (
        'buttons' => 
        array (
          0 => 'EDIT',
          1 => 'DUPLICATE',
          2 => 'DELETE',
          3 => 'FIND_DUPLICATES',
        ),
      ),
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
      'syncDetailEditViews' => true,
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 'name',
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'parent_name',
            'studio' => 'visible',
            'label' => 'LBL_PARENT_NAME',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'track_date',
            'label' => 'LBL_TRACK_DATE',
          ),
          1 => 
          array (
            'name' => 'hours',
            'label' => 'LBL_HOURS',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'track_user',
            'studio' => 'visible',
            'label' => 'LBL_TRACK_USER',
          ),
          1 => 'description',
        ),
        4 => 
        array (
          0 => 'assigned_user_name',
        ),
      ),
    ),
  ),
);
?>
