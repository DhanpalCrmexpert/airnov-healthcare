<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

class sp_TimesheetLogicHook {

    function processRecord(&$bean, $event, $arguments) {
		global $app_list_strings;
		
		$selected_module = $bean->module;
		$related_module = $bean->related_module;
		
		$selected_module_lable = $app_list_strings['moduleList'][$selected_module];
		$related_module_lable = $app_list_strings['moduleList'][$related_module];
		
		$bean->module = $selected_module_lable;
		$bean->related_module = $related_module_lable;
	}
	
	function afterDelete(&$bean, $event, $arguments) {
		global $db;
		
		$timesheet_id = $bean->id;
		if(!empty($timesheet_id)){
			$qry_sel = "select id,hours from sp_timesheet_tracker where timesheet_id='".$timesheet_id."'";
			$qry_sel = $db->query($qry_sel);
			if($qry_sel->num_rows>0){
				  while($res = $db->fetchByAssoc($qry_sel)){
					        $total_hours_at = $total_hours_pt = 0;
							$total_hours = $res['hours'];
							
							$query = "DELETE from sp_timesheet_tracker where timesheet_id = '$timesheet_id'";
							$db->query($query);
							
							//Account
							$qry_acc = "select accounts_sp_timesheet_tracker_1accounts_ida from accounts_sp_timesheet_tracker_1_c where accounts_sp_timesheet_tracker_1sp_timesheet_tracker_idb = '".$res['id']."' and deleted=0";
							$qry_acc1 = $db->query($qry_acc);
							if($qry_acc1->num_rows>0){
								$res_acc = $db->fetchByAssoc($qry_acc1);
								
								$accBean = new Account();
								$accBean->retrieve($res_acc['accounts_sp_timesheet_tracker_1accounts_ida']);
								$accHours = $accBean->total_hours_at;
								$total_hours_at = $accHours-$total_hours;
								$qry_upd = "update accounts set total_hours_at='".$total_hours_at."' where id='".$accBean->id."'";
	                            $db->query($qry_upd);
								
								$del_acc = "DELETE from accounts_sp_timesheet_tracker_1_c where accounts_sp_timesheet_tracker_1sp_timesheet_tracker_idb = '".$res['id']."'";
								$db->query($del_acc);		
							}
							
							//Project
							 $qry_project = "select project_sp_timesheet_tracker_1project_ida  from project_sp_timesheet_tracker_1_c where project_sp_timesheet_tracker_1sp_timesheet_tracker_idb = '".$res['id']."' and deleted=0";
							$qry_project1 = $db->query($qry_project);
							if($qry_project1->num_rows>0){
								$res_project = $db->fetchByAssoc($qry_project1);
								
								$projBean = new Project();
								$projBean->retrieve($res_project['project_sp_timesheet_tracker_1project_ida']);
								$projHours = $projBean->total_hours_pt;
								$total_hours_pt = $projHours-$total_hours;
							    $qry_upd1 = "update project set total_hours_pt='".$total_hours_pt."' where id='".$projBean->id."'";
								$db->query($qry_upd1);
								
								$del_project = "DELETE from project_sp_timesheet_tracker_1_c where project_sp_timesheet_tracker_1sp_timesheet_tracker_idb = '".$res['id']."'";
								$db->query($del_project);		
							}
					}
			}
		}
	}
}
