<?php
/**
 * @author SalesAgility <info@salesagility.com>.
 */

if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

global $current_user, $mod_strings, $app_list_strings;

if (!is_admin($current_user)) {
    sugar_die("Unauthorized access to administration.");
}

require_once 'custom/include/SA_Outlook/Outlook.php';

$outlook = new SalesAgility\Outlook\Outlook();
$settings = $outlook->getSettings();

//emails
$emails = BeanFactory::getBean('Emails');
$emailModules = [];
$emailLinks = [];
$exclude = ['EmailTemplates', 'Meetings', 'Notes', 'Tasks', 'Users', 'SecurityGroups'];
foreach ($emails->get_linked_fields() as $name => $arr) {
    if (isset($arr['module']) && $arr['module'] !== '') {
        $relModule = $arr['module'];
    } elseif ($emails->load_relationship($name)) {
        $relModule = $emails->$name->getRelatedModuleName();
    } else {
        continue;
    }

    $relBean = BeanFactory::getBean($relModule);
    if ($relBean instanceof File) {
        continue;
    }
    if (isset($app_list_strings['moduleList'][$relModule])
        && !isset($emailModules[$relModule])
        && !in_array($relModule, $exclude, true)
    ) {
        $emailModules[$relModule] = $app_list_strings['moduleList'][$relModule];
        $emailLinks[$relModule] = $name;
    }
}
asort($emailModules);


if (isset($_REQUEST['do']) && $_REQUEST['do'] === 'save') {

    $settings['graph_tenant_id'] = !empty($_REQUEST['graph_tenant_id']) ? $_REQUEST['graph_tenant_id'] : '';
    $settings['graph_client_id'] = !empty($_REQUEST['graph_client_id']) ? $_REQUEST['graph_client_id'] : '';
    $settings['graph_client_secret'] = $_REQUEST['graph_client_secret'] !== 'SECRET_UNCHANGED' ? $_REQUEST['graph_client_secret'] : $settings['graph_client_secret'];

    $modules = !empty($_REQUEST['module_select']) ? $_REQUEST['module_select'] : [];
    $moduleResults = [];
    foreach ($modules as $module) {
        if (isset($emailLinks[$module])) {
            $bean = BeanFactory::newBean($module);
            $result = [];
            $result['key'] = $module;
            $result['value'] = $app_list_strings['moduleList'][$module];
            $result['link'] = $emailLinks[$module];
            $result['object'] = $bean->object_name;
            if ($bean instanceof Company || $bean instanceof Person) {
                $result['fields'] = ['name', 'email'];
            } else {
                $result['fields'] = ['name'];
            }
            $moduleResults['modules'][] = $result;
        }
    }
    $settings['modules'] = $moduleResults;

    $outlook->saveSettings($settings);

    header('Location: index.php?module=Administration&action=index');
    exit();
}

echo getClassicModuleTitle('Administration', [$mod_strings['LBL_OUTLOOK_ADDIN_TITLE']]);

$buttons = <<<EOQ
<input title="{$app_strings['LBL_SAVE_BUTTON_TITLE']}"
                   accessKey="{$app_strings['LBL_SAVE_BUTTON_KEY']}"
                   class="button primary"
                   type="submit"
                   name="save"
                   onclick="return check_form('ConfigureSettings');"
                   value="{$app_strings['LBL_SAVE_BUTTON_LABEL']}" >
&nbsp;
<input title="{$mod_strings['LBL_CANCEL_BUTTON_TITLE']}"  
                    onclick="document.location.href='index.php?module=Administration&action=index'" 
                    class="button primary"  
                    type="button" 
                    name="cancel" 
                    value="{$app_strings['LBL_CANCEL_BUTTON_LABEL']}" >
<input title="{$mod_strings['LBL_OUTLOOK_ADDIN_DOWNLOAD_MANIFEST']}"  
                    onclick='document.location.href="index.php?entryPoint=downloadOutlookManifest"'
                    class="button"  
                    type="button" 
                    name="{$mod_strings['LBL_OUTLOOK_ADDIN_DOWNLOAD_MANIFEST']}" 
                    value="{$mod_strings['LBL_OUTLOOK_ADDIN_DOWNLOAD_MANIFEST']}" >
EOQ;


//Schedulers
$where = "schedulers.job='function::OutlookSyncScheduler'";
$schedulers = BeanFactory::getBean('Schedulers')->get_full_list(null, $where);

//Users
$GLOBALS['currentModule'] = 'Users';
$_POST['module'] = 'Users';
$_POST['action'] = 'SubPanelViewer';
$_POST['record'] = $current_user->id;

//Modules
$outlookModules = [];
if(isset($settings['modules'])){
    foreach ($settings['modules']['modules'] as $mod) {
        $outlookModules[] = $mod['key'];
    }
}


require_once 'include/SubPanel/SubPanelTiles.php';
$subPanel = new SubPanelTiles($current_user, 'outlookAdmin');

$sugar_smarty = new Sugar_Smarty();

$sugar_smarty->assign('MOD', $mod_strings);
$sugar_smarty->assign('LANGUAGES', get_languages());
$sugar_smarty->assign("JAVASCRIPT", get_set_focus_js());
$sugar_smarty->assign('error', []);
$sugar_smarty->assign('config', $settings);
$sugar_smarty->assign("schedulers", $schedulers);
$sugar_smarty->assign("BUTTONS", $buttons);
$sugar_smarty->assign("SUBPANEL", $subPanel->display());
$sugar_smarty->assign("USER_ID", $current_user->id);
$sugar_smarty->assign("EMAIL_MODULES", $emailModules);
$sugar_smarty->assign("OUTLOOK_MODULES", $outlookModules);

$sugar_smarty->display('custom/include/SA_Outlook/admin/admin.tpl');

$javascript = new javascript();
$javascript->setFormName('ConfigureSettings');
echo $javascript->getScript();
