<?php
/*********************************************************************************
 * This file is part of package Price Books.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Price Books is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
$mod_strings['LBL_PRICE_BOOKS'] = "Prijsboeken";
$mod_strings['LBL_PRICE_BOOKS_DESCRIPTION'] = "Beheer prijslijsten van producten voor bepaalde contacten / accounts.";
$mod_strings['LBL_ENABLE_PRICE_BOOKS'] = "Prijsboekenfunctie inschakelen";
$mod_strings['LBL_PRICE_BOOKS_ACTIVATED'] = "Prijsboeken zijn met succes geactiveerd.";
$mod_strings['LBL_PRICE_BOOKS_DEACTIVATED'] = "Prijsboeken zijn met succes gedeactiveerd.";
$mod_strings['LBL_CLICK_HERE'] = "Klik hier";
$mod_strings['LBL_MANAGE_PRICEBOOK'] = "Om prijsboeken te beheren";
//license
$mod_strings['LBL_LICENCE_ACTIVE_LABEL'] = "VIPriceBooksLicenseAddon is niet langer actief om de volgende reden:";
$mod_strings['LBL_LICENCE_ISSUES'] = "Gebruikers hebben geen toegang tot het probleem is verholpen";
$mod_strings['LBL_CLICK_HERE'] = "Klik hier";
$mod_strings['LBL_LICENCE_ACTIVE'] = "VIPriceBooksLicenseAddon is niet langer actief";
$mod_strings['LBL_RENEW_LICENCE'] = "Vernieuw uw abonnement of controleer uw licentieconfiguratie.";
$mod_strings['LBL_UPDATE_LICENSE'] = "Update licentie";
?>