<?php
return  array(
    'restrictedModules' => array('AnalyticReporting', 'AnalyticReportingDashboard'),
    'allowedBeans' => array(
        'AOS_Products_Quotes',
        'EmailAddresses',
        'ProjectTask',
        'Users',
        'CampaignLog',
        'EmailMarketing',
        'CampaignTrackers',
        'EEG1_Accounts_Receivables',
        'ForecastWorksheets',
        'ForecastManagerWorksheets',
        'Quotas',
        /**
         * Removed these modules, because they have only custom fields
         * therefore we can't use them in report builder, because of
         * no available fields (all fields are non-db)
         */
        // 'ForecastOpportunities',
        // 'ForecastDirectReports',
        'TimePeriods',
        'ProductCategories',
        'ProductTypes',
        'ProductTemplates',
        'bnv_SpecialRate',
        'bnv_StatMonthlyDetailed',
        'bnv_StatWeekly',
        'bnv_CustomerStatMonthly',
        'bnv_StatMonthly',
        'bnv_Stat',
        'bnv_StatImport',

        // AddOptify Customer Journey Plugin
        'DRI_Workflow_Templates',
        'DRI_Workflows',
        'DRI_SubWorkflow_Templates',
        'DRI_SubWorkflows',
        'DRI_Workflow_Task_Templates',

        // Cireson
        'sf_Dialogs',
        'sf_webActivity',
        'sf_WebActivityDetail',
        'sf_EventManagement',
        'sf_ExternalActions',

        // Humanforce
        'ActivityLogs',
    ),
);