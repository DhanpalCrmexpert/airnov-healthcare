<?php

global $sugar_version;

$admin_option_defs=array();

    
$admin_option_defs['Administration']['dt_sendgrid_config']= array('','LBL_DTSENDGRID_CONFIGURATION','LBL_DTSENDGRID_MESSAGE','./index.php?module=sg_SendGrid_Event&action=sendgrid_config');

$admin_option_defs['Administration']['dt_sendgrid_info']= array('','LBL_DTSENDGRID_TITLE','LBL_DTSENDGRID_LICENSE','./index.php?module=sg_SendGrid_Event&action=license');

$admin_group_header[]= array('LBL_DTSENDGRID','',false,$admin_option_defs, '');
