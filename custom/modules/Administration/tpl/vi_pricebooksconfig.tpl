{*
/*********************************************************************************
 * This file is part of package Price Books.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Price Books is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
*}
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="custom/modules/Administration/css/VIPriceBooksConfig.css">
	</head>
	<body>
		<form method ='post'>			
			<div class="moduleTitle">
				<h2 class="module-title-text">{$MOD.LBL_PRICE_BOOKS}</h2>
				<div class="update_license">
					<a href="index.php?module=VIPriceBooksLicenseAddon&action=license">
						<input type="button" class="button" name="button" value="{$MOD.LBL_UPDATE_LICENSE}"/>
					</a>
				</div>
				<div class="clear"></div>
			</div>
            {$HELPBOXCONTENT}
			<div id="enable_price_books">
				<table id="enable_price_books">
					<tr>
						<td><b>{$MOD.LBL_ENABLE_PRICE_BOOKS}</b></td> 
						<td>
							<label class="switch">
		    					<input type="checkbox" id="enabled_price_books" name="enabled_price_books" {if $ENABLE eq 1}checked="checked"{/if}>
		    					<span class="slider round" id='slider_round'></span>
	    					</label>
	    				</td>
					</tr>
				</table>
			</div>
			<br/>
			<div id="price_books_link" style="display:none;">
				<a href="{$LISTVIEWLINK}" target="_blank" class="relateModuleLink">
					<b>{$MOD.LBL_CLICK_HERE}</b>
				</a>{$MOD.LBL_MANAGE_PRICEBOOK}
			</div>
		</form>
	</body>
</html>

{literal}
    <script type="text/javascript">
        var enable = {/literal}{$ENABLE}{literal};
        var script = document.createElement("script");
        script.type = "text/javascript";
        script.src = "custom/modules/Administration/js/VIPriceBooksConfig.js?v="+Math.random();
        document.body.appendChild(script);
    </script>
{/literal}