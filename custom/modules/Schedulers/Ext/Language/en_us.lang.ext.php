<?php 
 //WARNING: The contents of this file are auto-generated



$mod_strings['LBL_OUTLOOKSYNCSCHEDULER'] = 'OutlookSyncScheduler';



$mod_strings = array_merge($mod_strings,
	array(
		'LBL_SENDSCHEDULEDREPORTS' => "Analytic Reporting Send Scheduled Reports",
		'LBL_SENDSCHEDULEDDASHBOARDS' => "Analytic Reporting Send Scheduled Dashboard",
		'LBL_EXECUTEARJOBS' => "Analytic Reporting Processes",
	)
);


$mod_strings['LBL_HOURS_MISSING_ON_PROJECTS'] = 'TimeSheet: Notify user for missing hours';



$mod_strings['LBL_CALCULATE_AUTO_TRACK_TIMINGS'] = 'TimeSheet: Auto Track Timings';
$mod_strings['LBL_SEND_EMAIL_FOR_AUTO_TRACK_TIMINGS'] = 'TimeSheet: Send Email Notifications for Auto Track Timings';
$mod_strings['LBL_NO_ACTIVITY_FOR_PROJECT_TASKS'] = 'TimeSheet: No Activity For Project Tasks';

?>