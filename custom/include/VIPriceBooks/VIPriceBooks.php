<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * This file is part of package Price Books.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Price Books is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
require_once('custom/VIPriceBooks/VIPriceBooksFunction.php');
class VIPriceBooks{
	static $already_ran = false;
	
	//Price Books Save Logic
	function VIPriceBooksSave(SugarBean $bean,$event,$arguments){
		if(self::$already_ran == true) return;self::$already_ran = true;

		if($_REQUEST['module'] != 'ModuleBuilder'){
			if($_REQUEST['module'] == "VI_Price_Books"){
				$productEntryCount = $_REQUEST['productEntryCount'];

				$productEntry = array();
				for($j=1;$j<=$productEntryCount;$j++){
					$productId = $_REQUEST['product_product_id'.$j];
					$productName = $_REQUEST['product_name'.$j];
					$productPrice = $_REQUEST['product_price'.$j];
					$listPrice = $_REQUEST['list_price'.$j];
					$priceType = $_REQUEST['price_type'.$j];
					if($priceType != "1"){
						$percentageMark = $_REQUEST['percentage_mark'.$j];
						$roundOff = $_REQUEST['round_off'.$j];
						if(isset($_REQUEST['percenPriceEntryCount'.$j])){
							$percenPriceEntryCount = $_REQUEST['percenPriceEntryCount'.$j];
						}
						if(isset($_REQUEST['dollarPriceEntryCount'.$j])){
							$dollarPriceEntryCount = $_REQUEST['dollarPriceEntryCount'.$j];
						}	
					}
					if($productId != "" && $productName != "" && $productPrice != "" && $listPrice != "" && $priceType != ""){
							
							if($priceType == "2"){
								$quantityCount = $percenPriceEntryCount;
							}
							if($priceType == "3"){
								$quantityCount = $dollarPriceEntryCount;
							}

							if($priceType == "2" || $priceType == "3"){
								$priceEntry = array();
								for($i=1;$i<=$quantityCount;$i++){
									$fromQuantity = $_REQUEST['from_quantity'.$j.'_'.$i];
									$toQuantity = $_REQUEST['to_quantity'.$j.'_'.$i];
									$differential = $_REQUEST['differential'.$j.'_'.$i];
									if($fromQuantity != '' && $toQuantity != "" && $differential != ""){
										$priceEntry[] = array('from_quantity'=>$fromQuantity, 'to_quantity'=>$toQuantity, 'differential'=>$differential);
									}					
								}
							}
							
							if($priceType == "1"){
								$productEntry[]=array($j=>array('product_id'=>$productId,'product_name'=>$productName, 'product_price'=>$productPrice,'list_price'=>$listPrice,'price_type'=>$priceType));
							}else{
								$productEntry[] = array($j=>array('product_id'=>$productId,'product_name'=>$productName,'product_price'=>$productPrice,'list_price'=>$listPrice,'price_type'=>$priceType,'percentage_mark'=>$percentageMark,'round_off'=>$roundOff,'price_entry'=>$priceEntry));
							}
					}
				}//end of for loop
				
				if(isset($bean->record_id)){
					$recordId = $bean->record_id;
				}else if(isset($bean->id)){
					$recordId = $bean->id;
				}
				
				$tableName = "vi_pricebook_productentry";
				
				$id = create_guid();
				if($recordId != ""){
					$fieldData = array('id'=>"'".$id."'",'pricebooks_id'=>"'".$recordId."'",'productentry'=>"'".json_encode($productEntry)."'");

					$fieldNames = array("*");
					$where = array();
					$getProductData = getPriceBooksRecord($tableName,$fieldNames,$where);
					$getProductDataResult = $GLOBALS['db']->query($getProductData);
					
					if(!empty($getProductDataResult)){
						$whereData = array('pricebooks_id'=>$recordId);
						$deleteProductDataResult = deletePriceBooksRecord($tableName,$whereData);
					}
					$productDataResult = insertPriceBooksRecord($tableName,$fieldData);
				}
			}
		}
	}//end of function
}//end of class