({
    extendsFrom: 'ModuleMenuView',

    // TODO: Implement
    dashboards: null, // Visible dashboard count
    defaultSettings: {
        dashboards: 20,
    },


    initialize: function (options) {
        this._super('initialize', [options]);
        this._initCollection();
    },

    _initCollection: function (data) {
        // Create an empty collection of any sugar module, you can fill this collection using fetch or passing an array of beans
        this.dashboards = app.data.createBeanCollection('AnalyticReporting', data);
        return this;
    },

    _renderHtml: function () {
        this._super('_renderHtml');
    },

    populateMenu: function () {
        var that = this;
        App.api.call('GET', App.api.buildURL('AnalyticReporting/GetDashboards'), null, {
            success: function (data) {
                that._initCollection(data);
                that.renderMenuListItems()
            }
        });
    },

    renderMenuListItems: function() {
        this.$('.active').removeClass('active');

        this._renderPartial('dashboards', {
            collection: this.dashboards,
            active: this.context.get('module') === 'AnalyticReporting' && this.context.get('model')
        });
    },
})