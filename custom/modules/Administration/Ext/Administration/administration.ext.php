<?php 
 //WARNING: The contents of this file are auto-generated



global $sugar_version;

$admin_option_defs=array();

if(preg_match( "/^6.*/", $sugar_version) ) {
    $admin_option_defs['Administration']['analyticreporting_info']= array('helpInline','LBL_ANALYTICREPORTING_LICENSE_TITLE','LBL_ANALYTICREPORTING_LICENSE','./index.php?module=AnalyticReporting&action=license');
} else {
    $admin_option_defs['Administration']['analyticreporting_info']= array('helpInline','LBL_ANALYTICREPORTING_LICENSE_TITLE','LBL_ANALYTICREPORTING_LICENSE','javascript:parent.SUGAR.App.router.navigate("#bwc/index.php?module=AnalyticReporting&action=license", {trigger: true});');
}

$admin_group_header[]= array('LBL_ANALYTICREPORTING','',false,$admin_option_defs, '');



global $sugar_version;

$admin_option_defs=array();

if(preg_match( "/^6.*/", $sugar_version) ) {
    $admin_option_defs['Administration']['gmsyncaddon_info']= array('helpInline','LBL_GMSYNCADDON_LICENSE_TITLE','LBL_GMSYNCADDON_LICENSE','./index.php?module=GMSyncAddon&action=license');
} else {
    $admin_option_defs['Administration']['gmsyncaddon_info']= array('helpInline','LBL_GMSYNCADDON_LICENSE_TITLE','LBL_GMSYNCADDON_LICENSE','javascript:parent.SUGAR.App.router.navigate("#bwc/index.php?module=GMSyncAddon&action=license", {trigger: true});');
}

$admin_group_header[]= array('LBL_GMSYNCADDON','',false,$admin_option_defs, '');



global $sugar_version;

$admin_option_defs=array();

if(preg_match( "/^6.*/", $sugar_version) ) {
    $admin_option_defs['Administration']['samplelicenseaddon_info']= array('helpInline','LBL_SAMPLELICENSEADDON_LICENSE_TITLE','LBL_SAMPLELICENSEADDON_LICENSE','./index.php?module=TimesheetNinja&action=license');
} else {
    $admin_option_defs['Administration']['samplelicenseaddon_info']= array('helpInline','LBL_SAMPLELICENSEADDON_LICENSE_TITLE','LBL_SAMPLELICENSEADDON_LICENSE','javascript:parent.SUGAR.App.router.navigate("#bwc/index.php?module=TimesheetNinja&action=license", {trigger: true});');
}

$admin_group_header[]= array('LBL_SAMPLELICENSEADDON','',false,$admin_option_defs, '');



$admin_option_defs = array();

$admin_option_defs['Administration']['<section key>'] = array(
    //Icon name. Available icons are located in ./themes/default/images
    'Administration',

    //Link name label
    'LBL_LINK_KANBAN_CONFIG_NAME',

    //Link description label
    'LBL_LINK_KANBAN_CONFIG_DESCRIPTION',

    //Link URL
    './index.php?module=Administration&action=KanbanConf',
);

$admin_group_header[] = array(
    //Section header label
    'LBL_KANBAN_CONFIG_SECTION_HEADER',

    //$other_text parameter for get_form_header()
    '',

    //$show_help parameter for get_form_header()
    false,

    //Section links
    $admin_option_defs,

    //Section description label
    'LBL_KANBAN_CONFIG_SECTION_DESCRIPTION'
);


global $sugar_version;

$admin_option_defs=array();

if(preg_match( "/^6.*/", $sugar_version) ) {
    $admin_option_defs['Administration']['kanbanlicenseaddon_info']= array('helpInline','LBL_KANBANLICENSEADDON_LICENSE_TITLE','LBL_KANBANLICENSEADDON_LICENSE','./index.php?module=kanban&action=license');
} else {
    $admin_option_defs['Administration']['kanbanlicenseaddon_info']= array('helpInline','LBL_KANBANLICENSEADDON_LICENSE_TITLE','LBL_KANBANLICENSEADDON_LICENSE','javascript:parent.SUGAR.App.router.navigate("#bwc/index.php?module=kanban&action=license", {trigger: true});');
}

$admin_group_header[]= array('LBL_KANBANLICENSEADDON','',false,$admin_option_defs, '');



$admin_option_defs=array();

$admin_option_defs['Outlook']['outlook_addin_config'] = array(
    'Outlook Plugin Configuration',
    'LBL_OUTLOOK_ADDIN_CONFIG_TITLE',
    'LBL_OUTLOOK_ADDIN',
    './index.php?module=Administration&action=OutlookAddinConfig',
    'outlook-addin-config'
);

$admin_group_header[]= array('LBL_OUTLOOK_ADDIN_TITLE','',false,$admin_option_defs, 'LBL_OUTLOOK_ADDIN_TOOLS_DESC');


/*********************************************************************************
 * This file is part of package Price Books.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Price Books is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
require_once('modules/VIPriceBooksLicenseAddon/license/VIPriceBooksOutfittersLicense.php');
require_once('include/MVC/Controller/SugarController.php');
global $sugar_config;
global $mod_strings;
$siteURL = $sugar_config['site_url'];
$url = $siteURL."/index.php?module=VIPriceBooksLicenseAddon&action=license";
$sqlLicenseCheck = "SELECT * from config where name = 'lic_price-books'";
$result = $GLOBALS['db']->query($sqlLicenseCheck);
$selectResultData = $GLOBALS['db']->fetchRow($GLOBALS['db']->query($sqlLicenseCheck));
if(empty($selectResultData)){
    $validate_license = VIPriceBooksOutfittersLicense::isValid('VIPriceBooksLicenseAddon');
    if($validate_license !== true) {
        if(is_admin($current_user)) {
         SugarApplication::appendErrorMessage($mod_strings['LBL_LICENCE_ACTIVE_LABEL'].$validate_license.$mod_strings['LBL_LICENCE_ISSUES']. '<a href='.$url.'>'.$mod_strings['LBL_CLICK_HERE'].'</a>');
        }
            echo '<h2><p class="error">'.$mod_strings['LBL_LICENCE_ACTIVE'].'</p></h2><p class="error">'.$mod_strings['LBL_RENEW_LICENCE'].'</p><a href='.$url.'>'.$mod_strings['LBL_CLICK_HERE'].'</a>';
    }else{
        foreach ($admin_group_header as $key => $value) {
            $values[] = $value[0];
        }   
        if (in_array("Other", $values)){
                $array['PriceBooks'] = array('PriceBooks',
                                        $mod_strings["LBL_PRICE_BOOKS"],
                                        $mod_strings["LBL_PRICE_BOOKS_DESCRIPTION"],
                                        './index.php?module=Administration&action=vi_pricebooksconfig',
                                        'pricebooks'
                                    );
                $admin_group_header['Other'][3]['Administration'] = array_merge($admin_group_header['Other'][3]['Administration'],$array);
        }else{
            $admin_option_defs = array();
            $admin_option_defs['Administration']['PriceBooks'] = array(
                //Icon name. Available icons are located in ./themes/default/images
                'PriceBooks',
                //Link name label 
                $mod_strings["LBL_PRICE_BOOKS"],

                //Link description label
                $mod_strings["LBL_PRICE_BOOKS_DESCRIPTION"],

                //Link URL
                './index.php?module=Administration&action=vi_pricebooksconfig',
                'pricebooks'
            );
            $admin_group_header['Other'] = array(
                //Section header label
                'Other',
                //$other_text parameter for get_form_header()
                '',
                //$show_help parameter for get_form_header()
                false,

                //Section links
                $admin_option_defs,

                //Section description label
                ''
            );
        }   
    }
}else{
    foreach ($admin_group_header as $key => $value) {
        $values[] = $value[0];
    }
    if (in_array("Other", $values)){
        $array['PriceBooks'] = array('PriceBooks',
                                $mod_strings["LBL_PRICE_BOOKS"],
                                $mod_strings["LBL_PRICE_BOOKS_DESCRIPTION"],
                                './index.php?module=Administration&action=vi_pricebooksconfig',
                                'pricebooks'
                            );
        $admin_group_header['Other'][3]['Administration'] = array_merge($admin_group_header['Other'][3]['Administration'],$array);
    }else{
        $admin_option_defs = array();
        $admin_option_defs['Administration']['pricebooks'] = array(
            //Icon name. Available icons are located in ./themes/default/images
            'PriceBooks',

            //Link name label 
            $mod_strings["LBL_PRICE_BOOKS"],

            //Link description label
            $mod_strings["LBL_PRICE_BOOKS_DESCRIPTION"],

            //Link URL
            './index.php?module=Administration&action=vi_pricebooksconfig',
            'pricebooks'
        );
        $admin_group_header['Other'] = array(
            //Section header label
            'Other',

            //$other_text parameter for get_form_header()
            '',

            //$show_help parameter for get_form_header()
            false,

            //Section links
            $admin_option_defs,

            //Section description label
            ''
        );
    }
}



global $sugar_version;

$admin_option_defs=array();

    
$admin_option_defs['Administration']['dt_sendgrid_config']= array('','LBL_DTSENDGRID_CONFIGURATION','LBL_DTSENDGRID_MESSAGE','./index.php?module=sg_SendGrid_Event&action=sendgrid_config');

$admin_option_defs['Administration']['dt_sendgrid_info']= array('','LBL_DTSENDGRID_TITLE','LBL_DTSENDGRID_LICENSE','./index.php?module=sg_SendGrid_Event&action=license');

$admin_group_header[]= array('LBL_DTSENDGRID','',false,$admin_option_defs, '');

?>