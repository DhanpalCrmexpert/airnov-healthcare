<?php
class logichook_Opportunities
{
	function updateSalesStageAmount($bean, $event, $arguments)
	{
		$db =  DBManagerFactory::getInstance();
		global $current_user;
		$cuid = $current_user->id;
		//$bean->fetched_row['sales_stage']
		$ssamount=0;
		$ssprobability=0;
		if($bean->amount!='' && $bean->probability!='')
		{
			$ssamount=round(($bean->amount*$bean->probability)/100,2);				
			$sql_updaterecord="UPDATE opportunities,opportunities_cstm SET sales_stage_amount_c=".$ssamount." WHERE id=id_c and id='".$bean->id."'";
			$db->query($sql_updaterecord);
		}
		if($bean->date_entered==$bean->date_modified)
		{
			if($bean->account_id!='')
			{
				$accountdetail =BeanFactory::getBean('Accounts',$bean->account_id);
				if($accountdetail->parent_id!='')
				{
					$key_accountdetail =BeanFactory::getBean('Accounts',$accountdetail->parent_id);
					if($key_accountdetail->assigned_user_id!='')
					{
						$key_account_user_detail =BeanFactory::getBean('Users',$key_accountdetail->assigned_user_id);
						if($key_account_user_detail->email1!='')
						{
							require_once('include/SugarPHPMailer.php');
							require_once('modules/EmailTemplates/EmailTemplate.php');
							$emailTo=$key_account_user_detail->email1;
							$template_id='16eed23a-c7c5-2181-5676-5f70ae2f7e49';
							$template = new EmailTemplate();
							$template->retrieve($template_id); 
							$emailObj = new Email();
							$defaults = $emailObj->getSystemDefaultEmail();
							$mail = new SugarPHPMailer();
							$olead = new Contact();
							$olead->retrieve('6245b560-f2b6-4bc9-87dc-53830bb576ef');
							$mail->IsHTML(true);
							$emailSubject=$template->parse_template_bean($template->subject,'Contacts', $olead).' - '.$bean->name;
							$emailBody=$template->parse_template_bean($template->body_html,'Contacts', $olead);
							$relatedBean = null;
							$mail->setMailerForSystem();
							$mail->From = $defaults['email'];
							$mail->FromName = $defaults['name']; 
							$mail->ClearAllRecipients();
							$mail->ClearReplyTos();
							$mail->Subject=$emailSubject;
							$mail->Body=str_replace('$name',$bean->name, str_replace('$accountname',$accountdetail->name, str_replace('$amount',$bean->amount,str_replace('$probability',$bean->probability, str_replace('$sales_stage_amount_c',$ssamount, str_replace('$user_name',$key_account_user_detail->name, $emailBody))))));
									
							$mail->prepForOutbound();
							$mail->AddAddress($emailTo);
							if ($mail->Send()) {
								$emailObj->to_addrs= $emailTo;
								$emailObj->type= 'archived';
								$emailObj->deleted = '0';
								$emailObj->name = $emailSubject; 
								$emailObj->description = $template->parse_template_bean($mail->Body,'Contacts', $olead); 
								$emailObj->description_html = $template->parse_template_bean($mail->Body,'Contacts', $olead);
								$emailObj->from_addr = $mail->From;
								$emailObj->parent_type = 'Accounts';
								$emailObj->parent_id = $accountdetail->parent_id;
								$emailObj->date_sent = TimeDate::getInstance()->nowDb();
								$emailObj->modified_user_id = $current_user->id;
								$emailObj->assigned_user_id = $current_user->id;
								$emailObj->created_by = $current_user->id;
								$emailObj->status = 'sent';
								$emailObj->save();
							}
						}
					}
				}				
			}
		}
	}	
}