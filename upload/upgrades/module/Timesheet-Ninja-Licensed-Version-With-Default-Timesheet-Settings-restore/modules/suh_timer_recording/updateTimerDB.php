<?php

if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

global $db;

if(isset($_POST['timerPause'])){
  $timerPause = ($_POST['timerPause'] == 'true') ? 1 : 0;
  $db->query("UPDATE suh_timer_recording SET sec_spend = ".$_POST['second'].", timer_pause={$timerPause} WHERE id='".$_POST['timeRecordId']."'", true);
  if($timerPause === 1){
    $sec_res = $db->query("SELECT sec_spend FROM suh_timer_recording WHERE id='".$_POST['timeRecordId']."'", true);
    $sec_row = $db->fetchByAssoc($sec_res);

    $sec = (isset($sec_row['sec_spend'])) ? $sec_row['sec_spend'] : '0';
    echo $sec;
    exit;
  }
}

if(!empty($_POST['timeSpend'])){
    $sql_res = $db->query("select id, timer_ended, TIME_TO_SEC(timediff('".gmdate('Y-m-d h:i:s')."', minutes_updated)) diff from suh_timer_recording WHERE id='".$_POST['timeRecordId']."'", true);
   	$row = $db->fetchByAssoc($sql_res);
    if(isset($row['timer_ended'])){
      if(!empty($row['diff']) && (int)$row['diff'] < 55){//check added to handle multiple hits from multiple pages for single record.
        exit;
      }
      if($row['timer_ended'] == 0){
        $sql = "UPDATE suh_timer_recording SET time_spend=".$_POST['timeSpend'].", sec_spend=0, minutes_updated='".gmdate('Y-m-d h:i:s')."' WHERE id='".$row['id']."'";
        if((int)$_POST['timeSpend']%30 === 0){
          $str = "30mints";//check added to show popup every 30 minutes
        }
   		}
    	$db->query($sql);
   		echo $str;
   	}
   	exit;
}

if(isset($_POST['second']) && $_POST['second']){
    $sql_res = $db->query("select id, timer_ended, timer_pause, TIME_TO_SEC(timediff('".gmdate('Y-m-d h:i:s')."', date_modified)) diff from suh_timer_recording WHERE id='".$_POST['timeRecordId']."'", true);
    $row = $db->fetchByAssoc($sql_res);
    if(isset($row['timer_ended'])){
      if((int)$row['timer_pause'] === 1){
        echo "Pause";
        exit;
      }
      if((int)$row['diff'] < 2 && (int)$row['diff'] >0){
        exit;
      }
      $str = ($row['timer_ended'] == 1) ? "Ended" : "Not-Ended";
      if($row['timer_ended'] == 0){
        $sql = "UPDATE suh_timer_recording SET sec_spend=".$_POST['second'].", date_modified='".gmdate('Y-m-d h:i:s')."' WHERE id='".$row['id']."'";
      }
      $db->query($sql);
      echo $str;
    }
    exit;
}




?>