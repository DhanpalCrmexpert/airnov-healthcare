<?php
$dashletData['suh_timer_recordingDashlet']['searchFields'] = array (
  'timer_ended' => 
  array (
    'default' => 0,
  ),
  /*'parent_name' => 
  array (
    'default' => '',
  ),*/
);
$dashletData['suh_timer_recordingDashlet']['columns'] = array (
  'start_date' => 
  array (
    'type' => 'datetimecombo',
    'label' => 'LBL_START_DATE',
    'width' => '10%',
    'default' => true,
  ),
  'parent_type' => 
  array (
    'type' => 'parent_type',
    'label' => 'Module',
    'width' => '10%',
    'default' => true,
  ),
  'parent_name' => 
  array (
    'type' => 'parent',
    'label' => 'Record',
    'id' => 'PARENT_ID',
    'related_fields' => 
    array (
      0 => 'parent_id',
      1 => 'parent_type',
    ),
    'width' => '10%',
    'default' => true,
  ),
  'assigned_user_name' => 
  array (
    'width' => '8%',
    'label' => 'LBL_LIST_ASSIGNED_USER',
    'name' => 'assigned_user_name',
    'default' => true,
  ),
  'name' => 
  array (
    'width' => '40%',
    'label' => 'LBL_LIST_NAME',
    'link' => true,
    'default' => false,
    'name' => 'name',
  ),
  'date_modified' => 
  array (
    'width' => '15%',
    'label' => 'LBL_DATE_MODIFIED',
    'name' => 'date_modified',
    'default' => false,
  ),
  'date_entered' => 
  array (
    'width' => '15%',
    'label' => 'LBL_DATE_ENTERED',
    'default' => false,
    'name' => 'date_entered',
  ),
  'created_by' => 
  array (
    'width' => '8%',
    'label' => 'LBL_CREATED',
    'name' => 'created_by',
    'default' => false,
  ),
);
