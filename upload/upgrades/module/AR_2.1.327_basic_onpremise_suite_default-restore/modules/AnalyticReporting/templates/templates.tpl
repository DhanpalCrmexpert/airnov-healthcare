<!-- TEMPLATES -->
{literal}

<script id="reportButtonsTemplate" type="text/ractive-template">
{{#userPermissions.canEdit}}
<reportBtn text="{{t.label_save}}" on-clicked="save" className="green"/>
<reportBtn text="{{t.label_save_as}}" on-clicked="saveAs" className="green" />
{{/userPermissions.canEdit}}
{{#canDeleteReport}}
<reportBtn text="{{t.label_delete}}" on-clicked="delete" className="red"/>
{{/canDeleteReport}}
<reportBtn text="{{t.label_back_to_reports}}" on-clicked="back"/>

{{#userPermissions.canEdit || userPermissions.canExport}}
{{#isBasic}}
<a href="javascript:void(0);" class="loadPDF">
    <img style="vertical-align: middle; margin-right: 3px;" alt="Export report to PDF" src="modules/{{moduleName}}/assets/img/pdf_icon.png" />{{t.label_export_pdf}}
</a>
<a href="javascript:void(0);" style="text-decoration: none; color: grey">
    <img style="vertical-align: middle; margin-right: 3px;" alt="Export report to PDF from XLS" src="modules/{{moduleName}}/assets/img/pdf_icon.png" />{{t.label_export_pdf_from_xls}} <linkToProVersion />
</a>
<a href="javascript:void(0);" class="loadXLSX">
    <img style="vertical-align: middle; margin-right: 3px;" alt="Export report to Excel" src="modules/{{moduleName}}/assets/img/excel_icon.png" />{{t.label_export_xlsx}}
</a>
<a href="javascript:void(0);" style="text-decoration: none; color: grey">
    <img style="vertical-align: middle; margin-right: 3px;" alt="Export report to Excel with Headers" src="modules/{{moduleName}}/assets/img/excel_icon.png" />{{t.label_export_xlsx_headers}} <linkToProVersion />
</a>
<a href="javascript:void(0);" style="text-decoration: none; color: grey">
    <img style="vertical-align: middle; margin-right: 3px;" alt="Export report to CSV" src="modules/{{moduleName}}/assets/img/csv_icon.png" />{{t.label_export_csv}} <linkToProVersion />
</a>
{{/isBasic}}

{{^isBasic}}
<a href="javascript:void(0);" class="loadPDF">
    <img style="vertical-align: middle; margin-right: 3px;" alt="Export report to PDF" src="modules/{{moduleName}}/assets/img/pdf_icon.png" />{{t.label_export_pdf}}
</a>
<a href="javascript:void(0);" class="loadPDFXLS">
    <img style="vertical-align: middle; margin-right: 3px;" alt="Export report to PDF from XLS" src="modules/{{moduleName}}/assets/img/pdf_icon.png" />{{t.label_export_pdf_from_xls}}
</a>
<a href="javascript:void(0);" class="loadXLSX">
    <img style="vertical-align: middle; margin-right: 3px;" alt="Export report to Excel" src="modules/{{moduleName}}/assets/img/excel_icon.png" />{{t.label_export_xlsx}}
</a>
<a href="javascript:void(0);" class="loadXLSXHeaders">
    <img style="vertical-align: middle; margin-right: 3px;" alt="Export report to Excel with Headers" src="modules/{{moduleName}}/assets/img/excel_icon.png" />{{t.label_export_xlsx_headers}}
</a>
<a href="javascript:void(0);" class="loadCSV">
    <img style="vertical-align: middle; margin-right: 3px;" alt="Export report to CSV" src="modules/{{moduleName}}/assets/img/csv_icon.png" />{{t.label_export_csv}}
</a>
{{/isBasic}}
{{/}}

{{#userPermissions.canAddToTargetList && (userPermissions.canEdit || userPermissions.canExport)}}
{{^isBasic}}
<a href="javascript:void(0);" class="addToTargetList">
    <img style="vertical-align: middle; margin-right: 3px;"
         alt="Add result to Target List"
         src="modules/{{moduleName}}/assets/img/slickgrid/tag_red.png" />{{t.label_add_to_campaign}}
</a>
{{/isBasic}}

{{#isBasic}}
<a href="javascript:void(0);" style="text-decoration: none; color: grey; margin-left: 4px;">
    <img style="vertical-align: middle; margin-right: 3px;"
         alt="Add result to Target List"
         src="modules/{{moduleName}}/assets/img/slickgrid/tag_red.png" />{{t.label_add_to_campaign}} <linkToProVersion />
</a>
{{/isBasic}}
{{/}}

{{#userPermissions.isAdmin}}
<a href="{{urls.settings}}&reportId={{reportId}}" style="position:absolute;right:0;bottom:0;"><img src="modules/{{moduleName}}/assets/img/gears_16x16.png" alt="{{t.label_settings}}"/></a>
{{/userPermissions.isAdmin}}
</script>

<script id="selectBoxImgTemplate" type="text/ractive-template">
<select value="{{selected}}" id="{{id}}">
    {{#options:opt}}
    <option
            data-imagesrc="modules/{{moduleName}}/assets/img/{{options[opt][valueName]}}.png"
            data-description=""
            value="{{options[opt][valueName]}}">{{options[opt][titleName]}}</option>
    {{/options}}
</select>
</script>

<script id="multiSelectBoxTemplate" type="text/ractive-template">
<div style="display:inline-block">
    <select value="{{selected}}" decorator="multiSelect" multiple="multiple">
        {{#optionGroups}}
        <optgroup label="{{.title}}">
            {{#items}}
            <option value="{{.value}}" disabled="{{.disabled}}">{{.title}}</option>
            {{/items}}
        </optgroup>
        {{/optionGroups}}
    </select>
</div>
</script>


<script id="groupedBoxTemplate" type="text/ractive-template">
<select value="{{selected}}" decorator="select2:filter">
{{#groups}}
	<optgroup label="{{title}}">
		{{#items}}
			<option value="{{value}}">{{title}}</option>
		{{/items}}
	</optgroup>
{{/groups}}
</select>
</script>

<script id="fieldFilterValueTemplate" type="text/ractive-template">
{{#condition!=="empty" &&condition!=="filled" &&condition!=="today" &&condition!=="tomorrow" &&condition!=="aftert" &&condition!=="yesterday" &&condition!=="tilly" &&condition!=="yesterday" &&condition!=="ndays" &&condition!=="pdays" &&condition!=="gtdays" &&condition!=="ltdays" &&condition!=="tweek" &&condition!=="lweek" &&condition!=="tmonth" &&condition!=="lmonth" &&condition!=="nmonth" &&condition!=="tyear" &&condition!=="lyear" &&condition!=="nyear" &&condition!=="nquarter" &&condition!=="lquarter" &&condition!=="tquarter" &&condition!=="nweek"  &&condition!=="nolweeks" &&condition!=="nolmonths" &&condition!=="nolquarters" &&condition!=="checked" &&condition!=="unchecked" &&condition!=="assignedto"}}
	{{#fieldType === "txt"}}
		<input value="{{value.0}}" disabled="{{readOnly}}" />
	{{/fieldType}}
	{{#fieldType === "select" || fieldType === "user" || fieldType === "team"|| fieldType === "teams"}}
		<select decorator="select2:filter" value="{{value}}" style="width:100%;" multiple="multiple" disabled="{{readOnly}}">
			{{#availableValues}}
			<option value="{{val}}">{{title}}</option>
			{{/availableValues}}
		</select>
	{{/fieldType}}
	{{#fieldType === "number"}}
		<input pattern= "[0-9\.,]+" type="number" value="{{value.0}}" disabled="{{readOnly}}" />
	{{/fieldType}}
	{{#fieldType === "integer"}}
		<input pattern= "[0-9\.,]+" type="number" value="{{value.0}}" disabled="{{readOnly}}" />
	{{/fieldType}}
	{{#fieldType === "time"}}
		<selectBox selected = "{{value.0}}" options="{{timeValues.hours}}" titleName="name" valueName="name" readOnly="{{readOnly}}"/>
		<selectBox selected = "{{value.1}}" options="{{timeValues.minutes}}" titleName="name" valueName="name" readOnly="{{readOnly}}"/>
		<selectBox selected = "{{value.2}}" options="{{timeValues.AmPm}}" titleName="name" valueName="name" readOnly="{{readOnly}}"/>
		{{#condition=="between" || condition=="nbetween" }}
			<selectBox selected = "{{value.4}}" options="{{timeValues.hours}}" titleName="name" valueName="name" readOnly="{{readOnly}}"/>
			<selectBox selected = "{{value.5}}" options="{{timeValues.minutes}}" titleName="name" valueName="name" readOnly="{{readOnly}}"/>
			<selectBox selected = "{{value.6}}" options="{{timeValues.AmPm}}" titleName="name" valueName="name" readOnly="{{readOnly}}"/>
		{{/condition}}
	{{/fieldType}}
	{{#fieldType === "date"}}
		{{#condition != "weekAndYear"}}
			{{#condition != "monthAndYear"}}
				{{#condition != "quarterAndYear"}}
                    {{#condition == "year" || condition == "quarter" || condition == "month" || condition == "week"}}
                        <input type="text" value="{{value.0}}" disabled="{{readOnly}}" />
                    {{/}}
                    {{#condition != "year" && condition != "quarter" && condition != "month" && condition != "week"}}
                        <input type="text" decorator="datepicker" value="{{value.0}}" disabled="{{readOnly}}" />
                    {{/}}

					{{#condition=="between" || condition=="nbetween" }}
						<input type="text" decorator="datepicker" value="{{value.1}}" disabled="{{readOnly}}" />
					{{/condition}}
				{{/condition}}
			{{/condition}}
		{{/condition}}

		{{#condition == "weekAndYear" || condition == "monthAndYear" || condition == "quarterAndYear"}}
			<input type="text" value="{{value.0}}" disabled="{{readOnly}}" placeholder="Year" />
			<input type="text" value="{{value.1}}" disabled="{{readOnly}}" />
		{{/condition}}
	{{/fieldType}}
    {{#fieldType === "datetime"}}
        {{#condition != "weekAndYear"}}
            {{#condition != "monthAndYear"}}
                {{#condition != "quarterAndYear"}}
                    {{#condition == "year" || condition == "quarter" || condition == "month" || condition == "week"}}
                        <input type="text" value="{{value.0}}" disabled="{{readOnly}}" />
                    {{/}}
                    {{#condition != "year" && condition != "quarter" && condition != "month" && condition != "week"}}
                        <input type="text" decorator="datepicker" value="{{value.0}}" disabled="{{readOnly}}" />
                    {{/}}

                    {{#condition != "day" && condition != "week" && condition != "month" && condition != "quarter" && condition != "year"}}
                        <selectBox selected = "{{value.1}}" options="{{timeValues.hours}}" titleName="name" valueName="name" readOnly="{{readOnly}}"/>
                        <selectBox selected = "{{value.2}}" options="{{timeValues.minutes}}" titleName="name" valueName="name" readOnly="{{readOnly}}"/>
                        <selectBox selected = "{{value.3}}" options="{{timeValues.AmPm}}" titleName="name" valueName="name" readOnly="{{readOnly}}"/>
                        {{#condition=="between" || condition=="nbetween" }}
                            <input type="text" decorator="datepicker" value="{{value.4}}" disabled="{{readOnly}}">
                            <selectBox selected = "{{value.5}}" options="{{timeValues.hours}}" titleName="name" valueName="name" readOnly="{{readOnly}}"/>
                            <selectBox selected = "{{value.6}}" options="{{timeValues.minutes}}" titleName="name" valueName="name" readOnly="{{readOnly}}"/>
                            <selectBox selected = "{{value.7}}" options="{{timeValues.AmPm}}" titleName="name" valueName="name" readOnly="{{readOnly}}"/>
                        {{/condition}}
                    {{/condition}}
                {{/condition}}
            {{/condition}}
        {{/condition}}

        {{#condition == "weekAndYear" || condition == "monthAndYear" || condition == "quarterAndYear"}}
            <input type="text" value="{{value.0}}" disabled="{{readOnly}}" placeholder="Year" />
            <input type="text" value="{{value.1}}" disabled="{{readOnly}}" />
        {{/condition}}
    {{/fieldType}}
{{/condition}}
{{#condition=="ndays" || condition=="pdays" || condition=="gtdays" || condition=="ltdays" || condition=="nolweeks" || condition=="nolmonths" || condition=="nolquarters" }}
	<input value="{{value.0}}" disabled="{{readOnly}}" />
{{/condition}}
</script>


<script id="totalTableTemplate" type="text/ractive-template">
<table class="ar-data-table ar-calculated-column-table-left">
    <thead>
    <tr>
        <th>{{t.label_group}}</th>
        {{#aggregates:i}}
        {{#aggregatesDefinitions:aggregateType}}
        {{#value[aggregateType]}}
        <th>{{t[aggregatesDefinitions[aggregateType].label]}}({{fieldsByName[aggregates[i].selectedField].bareTitle}})</th>
        {{/value[aggregateType]}}
        {{/aggregatesDefinitions:aggregateType}}
        {{/aggregates}}
    </tr>
    </thead>
    <tbody>
    {{#selectedFields:i}}
    <tr style="{{#i==errorAggregates}}border:2px solid red;{{/i==errorAggregates}}">
        <td>{{fieldsByName[selectedFields[i].name].bareTitle}} {{addonTitle(fieldsByName, selectedFields[i].name, selectedFields[i])}}</td>
        {{#aggregates}}

        {{#aggregatesDefinitions:aggregateType}}
        {{#value[aggregateType]}}

        <td>
            <input style="vertical-align: middle;" type="checkbox" checked="{{selectedFields[i].showAggregates[selectedField][aggregateType]}}" />

            {{#selectedFields[i].showAggregates[selectedField][aggregateType]}}
            <select value="{{selectedFields[i].showAggregates[selectedField].calculatePercent[aggregateType]}}" decorator="multiSelect" multiple="multiple">
                <optgroup label="{{t.calculations}}">
                    <option value="percentFromTotal" >{{t.percentFromTotal}}</option>
                    <option value="percentFromSubTotal" >{{t.percentFromSubTotal}}</option>
                </optgroup>
            </select>
            {{/}}
        </td>
        {{/value[aggregateType]}}
        {{/aggregatesDefinitions:aggregateType}}
        {{/aggregates}}
    </tr>
    {{/selectedFields}}
    <tr>
        <td>
            {{t.label_grand_total}}
            {{^isBasic}}
            {{#grandTotalBothShow}}<input type="checkbox" checked="{{grandTotalBoth}}" /> {{t.grandTotalBoth}}{{/grandTotalBothShow}}
            {{/isBasic}}

            {{#isBasic}}
            {{#grandTotalBothShow}} <linkToProVersion /><input type="checkbox" disabled /> {{t.grandTotalBoth}}{{/grandTotalBothShow}}
            {{/isBasic}}
        </td>
        {{#aggregates}}
        {{#value}}
        {{#aggregatesDefinitions:aggregateType}}
        {{#value[aggregateType]}}
        {{#aggregateType}}<td><input type="checkbox" checked="{{totalAggregates[selectedField][aggregateType]}}" /></td>{{/aggregateType}}
        {{/value[aggregateType]}}
        {{/aggregatesDefinitions:aggregateType}}
        {{/value}}
        {{/aggregates}}
    </tr>
    </tbody>
</table>
</script>

<script id="reportFieldManagerTemplate" type="text/ractive-template">
<div class="tools">
{{#userPermissions.canEdit && userPermissions.showDetails}}
    <includeDetails on-changed="includeDetails" />
{{/}}
</div>
<div>
    <input type="text" id="fields-available-search" {{^userPermissions.canEdit}}disabled="disabled"{{/userPermissions.canEdit}} style="width:276px;border:1px solid #ddd;padding:3px;margin-bottom:2px;" placeholder="{{t.label_type_to_search}}" value="{{filter}}" />
    <a href="#" on-click="cleanFilter" id="fields-available-clear" style="margin-left:2px;"><img src="modules/{{moduleName}}/assets/img/cross_grey.png" alt="x" style="height:8px;width:8px;" /></a>
</div>
<div style="display:inline-block;">
    <select style="min-width:300px;height:100px;" {{^userPermissions.canEdit}}disabled="disabled"{{/userPermissions.canEdit}} multiple="multiple" value="{{markedFields}}">
    {{#filteredBlocks}}
        <optgroup label="{{title}}">
        {{#fields}}
            <option value="{{name}}">{{title}}</option>
        {{/fields}}
        </optgroup>
    {{/filteredBlocks}}
    </select>
</div>
<div style="display:inline-block;vertical-align:top;padding-top:50px">
    {{#userPermissions.canEdit}}
    <a href="#" on-click="selectField">
        <img alt="->" src="modules/{{moduleName}}/assets/img/arrow_right.png" />
    </a>
    {{/userPermissions.canEdit}}
</div>
<div style="display:inline-block;">
    <div style="position:absolute;margin-top:-30px;font-weight:bold;">{{t.label_selected_columns}}:</div>
    <select style="min-width:300px;height:100px;" multiple="multiple" value="{{markedFieldsSelected}}" {{^userPermissions.canEdit}}disabled="disabled"{{/userPermissions.canEdit}}>
        {{#selectableFields}}
        <option value="{{name}}">{{title}}</option>
        {{/selectableFields}}
    </select>
</div>
{{#userPermissions.canEdit}}
<div style="display:inline-block;vertical-align:top;padding-top:30px">
    <a style="display:block;" href="#" on-click="moveUp">
        <img alt="{{t.label_up}}" src="modules/{{moduleName}}/assets/img/arrow_up.png" />
    </a>
    <a style="display:block;" href="#" on-click="removeField" class="tooltipX">
        <img alt="{{t.label_remove}}" src="modules/{{moduleName}}/assets/img/cross.png" />
        <span>{{t.label_remove_item}}</span>
    </a>
    <a style="display:block;" href="#" on-click="moveDown">
        <img alt="{{t.label_down}}" src="modules/{{moduleName}}/assets/img/arrow_down.png" />
    </a>
</div>
{{/userPermissions.canEdit}}
<br/>
{{#userPermissions.canEdit}}

<a class="ar-edit-module" href="{{urls.reportEditUrl}}{{reportId}}">{{t.addModuleToCurrentReport}}</a>
{{/userPermissions.canEdit}}
<br/>
<reportButtonPreview on-preview="preview" />
</script>

<script id="reportFilterManagerTemplate" type="text/ractive-template">
<div style="display:inline-block;">

	<!-- TABLE -->
	<table>
		{{#groups:i}}
			<filterRuleGroup sequence="{{i}}" blocks="{{blocks}}"
                             connector="{{connector}}" filters="{{filters}}" isBasic="{{isBasic}}"
                             on-deleteGroup="groupDeleted" canDelete="{{groups.length>1}}" fieldHTML="{{~/fieldHTML}}" readOnly="{{readOnly}}" />
		    {{#userPermissions.canEdit}}
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td style="vertical-align:middle;">
                    <a href="javascript:void(0);" on-click="addFilter:{{i}}" class="tooltipX"><img src="modules/{{moduleName}}/assets/img/cross_g.png"/><span>{{t.label_add_filter}}</span></a>
                </td>
            </tr>
            {{/userPermissions.canEdit}}
		{{/groups}}

		{{#aggregateGroups.filters.length}}
		    <tr>
		        <td colspan="7"><strong>{{t.label_summary_filter_title}}</strong></td>
		    </tr>
		{{/aggregateGroups}}
		{{#aggregateGroups}}
			<filterAggRuleGroup
			    aggregate="{{true}}"
			    sequence="{{0}}" blocks="{{aggregateBlocks}}" connector="AND"
			    filters="{{filters}}" on-deleteGroup="groupDeleted" canDelete="{{true}}"
			    fieldHTML="{{~/aggregateFieldHTML}}" readOnly="{{readOnly}}" />
            {{#userPermissions.canEdit}}
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td style="vertical-align:middle;">
		            {{#aggregateGroups.filters.length}}
                    <a href="javascript:void(0);" on-click="addAggregateFilter:0" class="tooltipX">
                        <img src="modules/{{moduleName}}/assets/img/cross_g.png"/><span>{{t.label_add_summary_filter}}</span>
                    </a>
		            {{/aggregateGroups}}
                </td>
            </tr>
            {{/userPermissions.canEdit}}
		{{/aggregateGroups}}
	</table>
	<!-- // TABLE -->

	<!-- ADD BUTTONS -->
	<div style="margin-top:10px;">
    {{#userPermissions.canEdit}}
		<div style="float:left;">
			<reportBtn on-clicked="addGroup" text="{{t.label_add_group}}"/>
			{{^aggregateGroups.filters.length}}
			{{#aggregateBlocks.length}}
            {{^isBasic}}
			<reportBtn on-clicked="addAggGroup" text="{{t.label_add_summary_group}}"/>
            {{/isBasic}}
            {{#isBasic}}
            <reportBtn className="disabled" text="{{t.label_add_summary_group}} {{t.label_pro}}"/>
            {{/isBasic}}
			{{/aggregateBlocks}}
			{{/aggregateGroups}}
		</div>
    {{/userPermissions.canEdit}}
	</div>
	<!-- // ADD BUTTONS -->

</div>
<br/><br/>
<reportButtonPreview on-preview="preview"/>
</script>


<script id="reportAggregateManagerTemplate" type="text/ractive-template">
<div class="tools">
    {{#userPermissions.canEdit && userPermissions.showDetails}}
    <includeDetails on-changed="includeDetails" />
    {{/}}
</div>

<table class="ar-data-table {{^userPermissions.canEdit}}disabled{{/userPermissions.canEdit}}">
    <thead>
    <tr>
        <th>{{t.label_field}}</th>
        {{#aggregatesDefinitions:index}}
        {{^aggregatesDefinitions[index].isBasic}}
        <th>{{t[aggregatesDefinitions[index].label]}}</th>
        {{/}}
        {{#aggregatesDefinitions[index].isBasic}}
        <th style="color: grey">{{t[aggregatesDefinitions[index].label]}} {{#aggregatesDefinitions[index].isBasic}} <linkToProVersion />{{/}}</th>
        {{/}}
        {{/aggregatesDefinitions:index}}
        <th>&nbsp;</th>
    </tr>
    </thead>
    <tbody>
    {{#aggregates:i}}
    <reportAggregate isBasic="{{isBasic}}"
                seq="{{i}}" availableFields="{{~/availableFields}}"
                fieldsByName="{{~/fieldsByName}}" on-aggregate="updateAggregates"
                on-deleteAggregate="deleteAggregate" selectedField="{{.selectedField}}"
                value="{{.value}}" title="{{title}}" bareTitle="{{bareTitle}}" />
    {{/aggregates}}
    </tbody>
</table>

{{#userPermissions.canEdit}}
<div style="margin-left:705px">
    <reportBtnGC on-clicked="addAggregate" text="{{t.label_add_item}}"/>
</div>
{{/userPermissions.canEdit}}

<br/><br/>

<div>
    <reportButtonPreview on-preview="preview"/>
</div>
</script>

<script id="reportGroupingManagerTemplate" type="text/ractive-template">
{{#userPermissions.canEdit}}
<div class="tools">
    {{#userPermissions.showDetails}}
    <includeDetails on-changed="includeDetails"  />
    {{/userPermissions.showDetails}}
    <isCrosstab on-changed="isCrosstab" val="{{isCrosstab}}"  aggregatesAsColumnVal="{{aggregatesAsColumn}}" />
</div>
{{/userPermissions.canEdit}}

<div class="{{^userPermissions.canEdit}}disabled{{/userPermissions.canEdit}}">
    {{#isCombined}}
    <sortingByCombined blocks="{{blocks}}" combinedFields="{{combinedFields}}" combinedModules="{{combinedModules}}" selectedFields="{{selectedFields}}" aggregates="{{aggregates}}" isCrosstab="{{isCrosstab}}" isCombined="{{isCombined}}" availableFields="{{availableFields}}"/>
    {{/isCombined}}

    {{#isCombined}}
    <sortingBy
            blocks="{{blocks}}"
            canSort="{{canSort}}"
            selectedFields="{{selectedFields}}"
            aggregates="{{aggregates}}"
            isCrosstab="{{isCrosstab}}"
            isCombined="{{isCombined}}"
            availableFields="{{availableFieldsPossible}}"
            fieldsByName="{{~/fieldsByName}}"
    />
    {{/isCombined}}

    {{^isCombined}}
    <sortingBy
            blocks="{{blocks}}"
            canSort="{{canSort}}"
            selectedFields="{{selectedFields}}"
            aggregates="{{aggregates}}"
            isCrosstab="{{isCrosstab}}"
            isCombined="{{isCombined}}"
            availableFields="{{availableFields}}"
            fieldsByName="{{~/fieldsByName}}"
    />
    {{/isCombined}}

    {{#userPermissions.canEdit}}
    <div style="margin-left:731px;">
        <reportBtnGC on-clicked="addItem" text="{{t.label_add_item}}"/>
    </div>
    {{/userPermissions.canEdit}}

    <br/><br/>
    <div class="ar-clearfix">
        <div style="float: left;">
            <b>{{t.label_select_summaries}}</b>
            <br/>
            <totalTable selectedFields="{{selectedFields}}"
                        availableFields="{{availableFields}}"
                        aggregates="{{aggregates}}"
                        totalAggregates="{{totalAggregates}}"
                        isCrosstab="{{isCrosstab}}"
                        errorAggregates="{{errorAggregates}}"
                        fieldsByName="{{fieldsByName}}"
                        grandTotalBoth="{{options.grandTotalBoth}}"
                        isBasic="{{isBasic}}"
            />
        </div>
        <div style="float: left; margin-left: 3px;">

            {{#includeDetails == false && isCrosstab == false}}
            <calcColumns calculations="{{calculatedColumns}}"
                         selectedFields="{{selectedFields}}"
                         aggregates="{{aggregates}}"
                         isBasic="{{isBasic}}"
            />
            {{/}}
        </div>
        <br/><br/>
    </div>
</div>
<reportButtonPreview on-preview="preview"/>
</script>

<!-- ------------------- Calculated columns start ------------------- -->
<script id="calculatedColumnsTemplate" type="text/ractive-template">
<b>{{t.calculatedColumns}} {{#isBasic}}<linkToProVersion />{{/isBasic}}:</b>
<br/>
{{^isBasic}}
<table class="ar-data-table ar-calculated-column-table-right" style="min-width: 100px;">
    <thead>
    <tr>
        {{#calculations:i}}
        <th>
            {{name}}
            {{#level}}
            <button on-click='editFormula:{{i}}' class="btn btn-success">Edit</button> <button on-click='deleteFormula:{{i}}' class="btn btn-danger">Delete</button>
            {{/}}
            {{^level}}
            <button on-click='addFormula:{{i}}' class="btn btn-success">Add</button>
            {{/}}
            <img on-click="deleteCalculation:{{i}}" style="cursor: pointer; float:right;" src="modules/{{moduleName}}/assets/img/cross.png" alt="Delete"/>
        </th>
        {{/}}
        <th>Add Calculation</th>
    </tr>
    </thead>
    <tbody>
    {{#selectedFields:i}}
    <tr>
        {{#calculations:k}}
        <td>
            <input style="vertical-align: middle;" type="checkbox" checked="{{selectedFields[i].showAggregates[calculations[k].internalName].CalcCol}}"/>
        </td>
        {{/}}

        {{#if i == 0}}
        <td rowspan="{{selectedFields.length + 1}}">
            <img on-click="addCalculation" style="cursor: pointer;" src="modules/{{moduleName}}/assets/img/cross_g.png" alt="Add"/>
        </td>
        {{/if}}
    </tr>
    {{/selectedFields}}
    <tr>

        {{#calculations:k}}
        <td><input type="checkbox" checked="{{totalAggregates[calculations[k].internalName].CalcCol}}" /></td>
        {{/}}
    </tr>
    </tbody>
</table>
{{/isBasic}}
</script>

<script id="calcColumnsModalTemplate" type="text/ractive-template">
<div class="ar-calculated-column-formula-editor">
  <h2>Edit formula</h2>
   Name: <input value="{{formula.name}}" />
   {{#formula.calculation}}
        <arithmeticGroup connector="{{connector}}" value="{{value}}" type="{{type}}" disableType="true">
   {{/}}
   <br />
  <button class="ar-button" on-click='cancel'>Save and close</button>
</div>
</script>

<script id="arithmeticGroupTemplate" type="text/ractive-template">

    {{#if connector != 'none'}}
        <select value="{{connector}}">
            {{#availableConnectors}}
                <option value="{{id}}">{{name}}</option>
            {{/availableConnectors}}
        </select>
    {{/if}}
    {{^disableType}}
    <select value="{{type}}">
        {{#nodeTypes}}
            <option value="{{id}}">{{name}}</option>
        {{/nodeTypes}}
    </select>
    {{/}}
<ul class="ar-arithmetic-group">
    {{#value : i }}
        <li>
            {{#if i > 0}}
                <img on-click="remove:{{i}}" style="cursor: pointer; vertical-align: middle;" src="modules/{{moduleName}}/assets/img/cross.png" alt="Delete"/>
            {{/if}}
           {{>calcColumnsVariablesTemplate}}
        </li>
    {{/}}
    <li><button class="ar-button" on-click='add'>Add</button></li>

</ul>
</script>

<script id="calcColumnsVariablesTemplate" type="text/ractive-template">
    {{#if type == 'constant'}}
        <constant connector="{{connector}}" value="{{value}}" type="{{type}}"/>
    {{/if}}
    {{#if type == 'field'}}
        <columnField connector="{{connector}}" value="{{value}}" type="{{type}}"/>
    {{/if}}
    {{#if type == 'arithmeticGroup'}}
        <arithmeticGroup connector="{{connector}}" value="{{value}}"  type="{{type}}" disableType="false"/>
    {{/if}}
    {{#if type == 'logicGroup'}}
        <logicGroup connector="{{connector}}" value="{{value}}"  type="{{type}}"/>
    {{/if}}
</script>

<script id="logicGroupTemplate" type="text/ractive-template">

    {{#if connector != 'none'}}
        <select value="{{connector}}">
            {{#availableConnectors}}
                <option value="{{id}}">{{name}}</option>
            {{/availableConnectors}}
        </select>
    {{/if}}

    <select value="{{type}}">
        {{#nodeTypes}}
            <option value="{{id}}">{{name}}</option>
        {{/nodeTypes}}
    </select>

<ul class="ar-logic-group">
    {{#value }}

            {{#ifLogic}}
                <li><b>If</b>
                {{#operand1}}
                    {{>calcColumnsVariablesTemplate}}
                {{/}}
                </li>
                <li>
                    <select value="{{operator}}">
                        {{#availableLogicActions}}
                            <option value="{{id}}">{{name}}</option>
                        {{/availableConnectors}}
                    </select>
                </li>
                {{#if operator != 'empty'}}
                    {{#if operator != 'notEmpty'}}
                        <li>
                            {{#operand2}}
                                {{>calcColumnsVariablesTemplate}}
                            {{/}}
                        </li>
                    {{/if}}
                {{/if}}
                <li><b>Then</b>
                {{#result}}
                    {{>calcColumnsVariablesTemplate}}
                {{/}}
                </li>
            {{/}}

            {{#elseIfLogic:i}}
                <li><img on-click="removeElseIf:{{i}}" style="cursor: pointer; vertical-align: middle;" src="modules/{{moduleName}}/assets/img/cross.png" alt="Delete"/><b>Else If</b>
                {{#operand1}}
                    {{>calcColumnsVariablesTemplate}}
                {{/}}
                </li>
                <li>
                    <select value="{{operator}}">
                        {{#availableLogicActions}}
                            <option value="{{id}}">{{name}}</option>
                        {{/availableConnectors}}
                    </select>
                </li>
                {{#if operator != 'empty'}}
                    {{#if operator != 'notEmpty'}}
                        <li>
                        {{#operand2}}
                            {{>calcColumnsVariablesTemplate}}
                        {{/}}
                        </li>
                    {{/if}}
                {{/if}}
                <li><b>Then</b>
                {{#result}}
                    {{>calcColumnsVariablesTemplate}}
                {{/}}
                </li>
            {{/}}
            <button class="ar-button" on-click='addElseIf'>Add Else If</button>

            {{#elseLogic}}
                <li><b>Else</b>
                {{#result}}
                    {{>calcColumnsVariablesTemplate}}
                {{/}}
                </li>
            {{/}}

    {{/}}

</ul>
</script>

<script id="constantTemplate" type="text/ractive-template">
{{#if connector != 'none'}}
    <select value="{{connector}}">
        {{#availableConnectors}}
            <option value="{{id}}">{{name}}</option>
        {{/availableConnectors}}
    </select>
{{/if}}
    <select value="{{type}}">
        {{#nodeTypes}}
            <option value="{{id}}">{{name}}</option>
        {{/nodeTypes}}
    </select>
<input value="{{value}}" />
</script>

<script id="columnFieldTemplate" type="text/ractive-template">
{{#if connector != 'none'}}
    <select value="{{connector}}">
        {{#availableConnectors}}
            <option value="{{id}}">{{name}}</option>
        {{/availableConnectors}}
    </select>
{{/if}}
    <select value="{{type}}">
        {{#nodeTypes}}
            <option value="{{id}}">{{name}}</option>
        {{/nodeTypes}}
    </select>
    <select value="{{value}}">
        {{#availableValues}}
            <option value="{{id}}">{{name}}</option>
        {{/availableValues}}
    </select>
</script>

<!-- ------------------- Calculated columns END ------------------- -->

<script id="sortingByFieldTemplate" type="text/ractive-template">
<tr>
	<td class="groupingLevel"><h3></h3></td>
	<td>
		<selectBox options="{{sortOptions}}" valueName="name" titleName="title" selected="{{sortAction}}"/>
	</td>
	<td>
		<selectBox options="{{fields}}" valueName="name" titleName="title" selected="{{name}}" />
		{{#isDate}}
			<selectBox options="{{dateOptions}}" valueName="name" titleName="title" selected="{{dateGrouping}}"/>
		{{/isDate}}
		{{#isMultiSelect}}
		<br><label><input type="checkbox" checked="{{.transform}}"> {{t.label_merge_multiple_selections}}</label>
		{{/isMultiSelect}}

		{{#isNumeric}}
            <div>
                <div style="margin: 3px 0 3px 0;{{isBasic ? 'color:grey' : ''}}">
                    {{t.groupByRange}} {{#isBasic}}<linkToProVersion />{{/isBasic}} <input type="checkbox"
                                                                                      checked="{{groupByRange}}"
                                                                                      disabled="{{isBasic}}"
                                                                                      class="input-mini" />
                    {{#groupByRange}}
                        {{^includeDetails}}
                            {{#last}}
                                <selectBox options="{{groupByAggregateType}}" valueName="name" titleName="title" selected="{{groupByAggregate}}"/>
                            {{/}}
                        {{/}}
                    {{/}}
                </div>
                {{#groupByRange}}
                   {{#ranges:i}}
                   <p>
                        <label>{{t.start}}:</label><input type="text" value="{{start}}" placeholder="0" size="10" class="input-mini" />
                        <label>{{t.end}}:</label><input type="text" value="{{end}}" placeholder="0" size="10" class="input-mini" />
                        <label>{{t.rangeName}}:</label><input type="text" value="{{name}}" placeholder="" size="15" class="input-mini" />
                        <img style="vertical-align:middle; cursor: pointer;" on-click="removeRange:{{i}}" src="modules/{{moduleName}}/assets/img/cross.png"/>
                   </p>
                   {{/}}
                   <br/>
                    <button on-click='addRange' class="btn btn-success">New range</button>
                {{/}}
            </div>
		{{/isNumeric}}
	</td>
	<td>
	{{#sortAction == "sort" || sortAction == "groupsort" }}
        {{#hasAggregates}}
        <selectBox options="{{getAggregates}}" valueName="name" titleName="title" selected="{{aggregate}}"/>
        {{/hasAggregates}}

		<selectBox options="{{sortDirections}}" valueName="name" titleName="title" selected="{{sortDirection}}"/>
	{{/sortAction}}

	{{#isCombined == false && isCrosstab == false}}
	<div style="{{isBasic ? 'color:grey' : ''}}">
	{{t.label_show_top}} {{#isBasic}}<linkToProVersion />{{/isBasic}} <input type="text" value="{{showTop}}" placeholder="All" size="5" class="input-mini" disabled="{{isBasic}}" />
	</div>
	{{/seq}}

    {{#isCombined == false && canSort == false && allowedShowAll}}
    <div style="{{isBasic ? 'color:grey' : ''}}">
    {{t.showAllValues}} {{#isBasic}}<linkToProVersion />{{/isBasic}} <input type="checkbox" checked="{{showAllValues}}" class="input-mini" disabled="{{isBasic}}" />
	</div>
    {{/}}

	</td>
	{{#isCrosstab}}
	<td>
		<selectBox options="{{positions}}" valueName="name" titleName="title" selected="{{position}}"/>
	</td>
	{{/isCrosstab}}
	<td>
		<a href="#" on-click="delete:{{seq}}" class="tooltipX">
			<img src="modules/{{moduleName}}/assets/img/cross.png"/>
			<span>{{t.label_remove_item}}</span>
		</a>
	</td>
</tr>
</script>


<script id="sortingByCombinedFieldTemplate" type="text/ractive-template">
		<selectBox options="{{fields}}" valueName="name" titleName="title" selected="{{selectedValue}}" />
</script>

<script id="sortingByTemplate" type="text/ractive-template">
<table class="ar-data-table">
	<thead>
		<tr>
			<th>{{t.label_level}}</th>
			<th>{{t.label_action}}</th>
			<th>{{t.label_group_by}}</th>
			<th>{{t.label_sort}}</th>
			{{#isCrosstab}}
			<th>{{t.label_position}}</th>
			{{/isCrosstab}}
			<th style="width:12px;">&nbsp;</th>
		</tr>
	</thead>
	<tbody>
	{{#selectedFields:i}}
		<sortingByField canSortParam="{{ ( (i<1) || selectedFields[i-1].sortAction!='sort') }}" fields="{{fields}}"
                        canSort="{{canSort}}" sortOptions="{{sortOptions}}" title="{{title}}" name="{{name}}"
                        sortAction="{{sortAction}}" aggregate="{{aggregate}}" dateGrouping="{{.dateGrouping}}"
                        sortDirection="{{sortDirection}}" aggregates="{{aggregates}}" position="{{position}}"
                        isFirst="{{isFirst}}" seq="{{i}}" showAggregates="{{showAggregates}}"
                        fieldsByName="{{~/fieldsByName}}" on-deleted="deleteSort" on-fieldChanged="updateField"
		                transform="{{transform}}" showTop="{{.showTop}}" ranges="{{ranges}}" groupByRange="{{groupByRange}}"
		                groupByAggregate="{{groupByAggregate}}" last="{{i < (selectedFields.length-1) ? false : true}}"
				/>
	{{/selectedFields}}
	</tbody>
</table>
</script>

<script id="sortingByCombinedTemplate" type="text/ractive-template">
<table class="ar-data-table">
	<thead>
		<tr>
			<th>{{t.label_level}}</th>
			<th>{{t.label_action}}</th>
			<th>{{t.label_group_by}}</th>
			<th>{{t.label_date_group}}</th>
			<th>{{t.label_sort}}</th>
			{{#isCrosstab}}
			<th>{{t.label_position}}</th>
			{{/isCrosstab}}
		</tr>
	</thead>
	<tbody>
    <tr>
        <td class="groupingLevel">
            <h3></h3>
        </td>
        <td>
            <selectBox options="{{sortOptions}}" valueName="name" titleName="title" selected="{{combinedSelectedFields.sortAction}}"/>
        </td>
        <td>
            {{#combinedFields:moduleId}}
            <sortingByCombinedField fields="{{.}}" selectedValue="{{combinedSelectedFields.fields[moduleId]}}" title="{{title}}" name="{{name}}" sortAction="{{combinedSelectedFields.sortAction}}" dateGrouping="{{combinedSelectedFields.dateGrouping}}" sortDirection="{{combinedSelectedFields.sortDirection}}" position="{{combinedSelectedFields.position}}" seq="{{moduleId}}" combinedModulesCount="{{combinedModulesCount}}" /><br />
            {{/combinedFields}}
        </td>
        <td>
            <selectBox options="{{dateOptions}}" valueName="name" titleName="title" selected="{{combinedSelectedFields.dateGrouping}}"/>
        </td>

        <td>
            <selectBox options="{{sortDirections}}" valueName="name" titleName="title" selected="{{combinedSelectedFields.sortDirection}}"/>
        </td>
        {{#isCrosstab}}
        <td>
            <selectBox options="{{positions}}" valueName="name" titleName="title" selected="{{combinedSelectedFields.position}}"/>
        </td>
        {{/isCrosstab}}
    </tr>
	</tbody>
</table>
</script>

<script id="aggregateItemTmplt" type="text/ractive-template">
    {{^isBasic}}
    <td><input type="checkbox" checked="{{value[aggregateType]}}" disabled="{{disabled[aggregateType]}}" /></td>
    {{/isBasic}}
    {{#isBasic}}
    <td><input type="checkbox" disabled /></td>
    {{/isBasic}}
</script>

<script id="reportAggregateTemplate" type="text/ractive-template">
<tr style="{{#seq===invalidAggregate}}border:2px solid red;{{/seq===invalidAggregate}}">
    <td><selectBox options="{{availableFields}}" valueName="name" titleName="title" on-selectedValue="fieldSelected" selected="{{selectedField}}" /></td>

    {{#aggregatesDefinitions:aggregateType}}
    {{> aggregateItemTmplt}}
    {{/aggregatesDefinitions}}

    <td>
        <a href="#" on-click="remove" class="tooltipX">
            <img src="modules/{{moduleName}}/assets/img/cross.png"/>
            <span>{{t.label_remove_item}}</span>
        </a>
    </td>
</tr>
</script>

<script id="filterRuleGroupTemplate" type="text/ractive-template">
<tr class="{{#sequence}}filter group{{/sequence}}">
    <td colspan="5">{{#sequence}}<fieldConnector connector="{{connector}}" readOnly="{{readOnly}}"/>{{/sequence}}</td>
    <td style="vertical-align:middle;">
        {{#canDelete && sequence && !readOnly}}
        <a href="#" on-click="delete:{{sequence}}">
            <img src="modules/{{moduleName}}/assets/img/cross.png"/>
        </a>
        {{/canDelete}}
    </td>
</tr>
{{#filters:i}}
<filterRule
        aggregate="{{aggregate}}"
        on-deleteRule="ruleDeleted"
        sequence="{{i}}"
        fieldName="{{fieldName}}"
        value="{{value}}"
        condition="{{condition}}"
        connector="{{connector}}"
        blocks="{{blocks}}"
        fieldHTML="{{~/fieldHTML}}"
        selectedAccessRight="{{selectedAccessRight}}"
        commonFilter="{{commonFilter}}"
        isBasic="{{isBasic}}"
/>
{{/filters}}
</script>

<script id="filterRuleTemplate" type="text/ractive-template">
<tr class="filter">
	<td style="vertical-align:middle;{{^sequence}}padding:0;{{/sequence}}">{{#sequence}}<fieldConnector connector="{{connector}}" readOnly="{{readOnly}}" />{{/sequence}}</td>
	<td style="vertical-align:middle;"><fieldSelector fieldHTML="{{fieldHTML}}" selectedField="{{fieldName}}" readOnly="{{readOnlyPublicField}}" /></td>
	<td style="vertical-align:middle;"><fieldCondition fieldType="{{fieldType}}" condition="{{condition}}" readOnly="{{readOnlyPublic}}" selectedAccessRight="{{selectedAccessRight}}" /></td>
	<td style="vertical-align:middle;" class="conditions"><fieldFilterValue fieldType="{{fieldType}}" condition="{{condition}}" value="{{value}}" availableValues="{{availableValues}}" readOnly="{{readOnlyPublic}}" selectedAccessRight="{{selectedAccessRight}}" /></td>
	<td style="vertical-align:middle;">
    {{^readOnly}}
        <selectBox options="{{accessRights}}"
                   valueName="name" titleName="title" readOnly="{{isBasic}}"
                   descriptionName="description" selected="{{selectedAccessRight}}" />
    {{/readOnly}}
    </td>
	{{^aggregate}}
    {{#isBasic}}
	<td style="vertical-align:middle; color: grey;">
        <input type="checkbox" disabled />{{t.label_add_to_dashboard_filters}} <linkToProVersion />
    </td>
    {{/isBasic}}
    {{^isBasic}}
    <td style="vertical-align:middle;"><input type="checkbox" checked="{{commonFilter}}" />{{t.label_add_to_dashboard_filters}}</td>
    {{/isBasic}}
	{{/aggregate}}
	<td style="vertical-align:middle;">
	    {{^readOnly}}
			<a href="#" on-click="delete:{{sequence}}" class="tooltipX">
				<img src="modules/{{moduleName}}/assets/img/cross.png"/>
				<span>{{t.label_remove_filter}}</span>
			</a>
        {{/readOnly}}
	</td>
</tr>
</script>


<script id="reportAccessTemplate" type="text/ractive-template">

	<label>
		<input type="checkbox" checked="{{updatePermissions}}" />
        {{t.updatePermissions}}
	</label>
    {{#userPermissions.canEdit}}
    <label style="{{#isBasic}}color: grey{{/isBasic}}">
        <input type="checkbox" checked="{{allowDrilldownReadOnly}}" disabled="{{isBasic}}" />
        {{t.allowDrilldownReadOnly}} {{#isBasic}}<linkToProVersion />{{/isBasic}}
    </label>
    <label style="{{#isBasic}}color: grey{{/isBasic}}">
        <input type="checkbox" checked="{{allowExportReadOnly}}" disabled="{{isBasic}}" />
        {{t.allowExportReadOnly}} {{#isBasic}}<linkToProVersion />{{/isBasic}}
    </label>
    {{/userPermissions.canEdit}}
    <br>
    <div class="{{^userPermissions.canEdit}}disabled{{/}}">
    {{t.label_owner}}: <selectBox titleName="name" valueName="id" selected="{{owner}}" options="{{availableOwners}}" addDecorator="select2"/>
    <select value="{{globalSharing}}">
        <option value="PUB">{{t.label_public}}</option>
        <option value="PRIV">{{t.label_private}}</option>
    </select>
    {{#globalSharing=="PUB"}}
    {{t.label_everyone}}<reportAccessRights level="{{globalSharingRights}}" />
    {{/globalSharing=="PUB"}}
    <br><br>
    {{#globalSharing=="PRIV"}}
    <div>
        <input type="text" id="sharing-users-available-search" style="width:276px;border:1px solid #ddd;padding:3px;margin-bottom:2px;" placeholder="{{t.label_type_to_search}}" value="{{filter}}" />
        <a href="#" on-click="cleanFilter" id="sharing-users-available-clear" style="margin-left:2px;"><img src="modules/{{moduleName}}/assets/img/cross_grey.png" alt="x" style="height:8px;width:8px;" /></a>
    </div>
    <div style="display:inline-block;vertical-align:top;">
        <select  style="width:300px;height:100px;" multiple="multiple" on-change="changed" value="{{markedUsers}}">
            {{#filteredUsers:id}}
            {{^selected}}
            <option value="{{id}}">{{name}}</option>
            {{/selected}}
            {{/filteredUsers:id}}
        </select>
    </div>
    <div style="display:inline-block;vertical-align:top;padding-top:50px">
        <a href="#" on-click="selectUsers:{{markedUsers}}">
            <img alt="->" src="modules/{{moduleName}}/assets/img/arrow_right.png" />
        </a>
    </div>
    <div style="margin-bottom:-7px;border:1px solid darkgrey;min-height:100px;display:inline-block;width:30em;vertical-align:top;">
        <table>
        {{#usersById:id}}
            {{#selected}}
            <tr>
                <td>{{name}}</td>
                <td><reportAccessRights level="{{accessRights}}"/></td>
                <td><a href="#" on-click="removeUser:{{id}}" style="margin-left:2px;"><img src="modules/{{moduleName}}/assets/img/cross_grey.png" alt="x" style="height:8px;width:8px;" /></a></td>
            </tr>
            {{/selected}}
        {{/usersById:id}}
        <table>
    </div>
    {{/globalSharing=="PRIV"}}
    </div>
</script>


<script id="schedulerPDFXLSElement" type="text/ractive-template">
    <label style="{{#isBasic}}color: grey{{/isBasic}}">
        {{^isBasic}}
        <input type="checkbox" checked="{{scheduledFormats.pdfxls}}"> {{t.label_export_send_pdfxls}}
        {{/isBasic}}
        {{#isBasic}}
        <input type="checkbox" disabled="isBasic"> {{t.label_export_send_pdfxls}} <linkToProVersion />
        {{/isBasic}}
    </label>
</script>

<script id="customEmailBasic" type="text/ractive-template">
    {{^isBasic}}
    {{t.label_custom_email}}: <input type="text" name="schedemail" value="{{email}}">
    <a href="#" style="vertical-align:middle;" on-click="addCustomEmail:{{email}}">
        <img alt="->" src="modules/{{moduleName}}/assets/img/arrow_right.png" />
    </a>
    {{/isBasic}}

    {{#isBasic}}
    <div style="color: grey">
    {{t.label_custom_email}} <linkToProVersion />: <input type="text" disabled>
    </div>
    {{/isBasic}}
</script>

<script id="reportSchedulingTemplate" type="text/ractive-template">
<div class="{{^userPermissions.canEdit}}disabled{{/}}">
    <label><input type="checkbox" checked="{{isScheduled}}" /> {{t.label_schedule_report}}</label><br>
    {{#isScheduled}}
    {{t.label_frequency}}:
    <selectBox titleName="title" valueName="value" selected="{{interval}}" options="{{intervalAvailableValues}}"/>
    {{#interval==1 || interval==2}}
    {{#interval==2}}
    {{t.label_week_of_month}}: <selectBox selected="{{intervalOptions.1}}" options="{{monthweeks}}" titleName="title" valueName="val"/>
    {{/interval==2}}
    {{t.label_day}}: <selectBox selected="{{intervalOptions.0}}" options="{{weekdays}}" titleName="title" valueName="val"/>

    {{/interval==1 || interval==2}}
    {{#interval==3 || interval==4}}
    {{#interval==4}}
    {{t.label_month}}:
    <selectBox selected="{{intervalOptions.0}}" options="{{months}}" titleName="title" valueName="val"/>
    {{/interval==4}}
    {{t.label_day}}:
    <selectBox selected="{{intervalOptions.1}}" options="{{days}}" titleName="title" valueName="val"/>
    {{/interval==3 || interval==4}}
    {{t.label_time}}: <selectBox selected="{{timeH}}" options="{{hours}}" titleName="h" valueName="h" /> : <selectBox selected="{{timeM}}" options="{{minutes}}" titleName="m" valueName="m" /> (hh:mm)
    <label><input style="vertical-align:middle;" type="checkbox" checked="{{options.doNotSendEmpty}}" /> {{t.label_doNotSendEmpty}} </label><a class="tooltipX">(?)<span>{{t.label_doNotSendEmptyTooltip}}</span></a>
    <br>
    <strong>{{t.label_export_format}}</strong><br>
    <label><input type="checkbox" checked="{{scheduledFormats.xlsx}}"> {{t.label_export_send_xlsx}}</label>
    {{#scheduledFormats.xlsx}}
    <label><input type="checkbox" checked="{{options.excelWithHeaders}}" />{{t.excelWithHeaders}}</label>
    {{/scheduledFormats.xlsx}}
    <label><input type="checkbox" checked="{{scheduledFormats.pdf}}"> {{t.label_export_send_pdf}}</label>
    {{> schedulerPDFXLSElement}}
    {{#allowSendHtml}}
    <label><input type="checkbox" checked="{{scheduledFormats.html}}"> {{t.label_export_send_html}}</label>
    {{/allowSendHtml}}
    <label><input type="checkbox" checked="{{scheduledFormats.url}}"> {{t.label_export_send_url}}</label>
    <br>
    <div>
        <input type="text" id="schedule-users-available-search" style="width:276px;border:1px solid #ddd;padding:3px;margin-bottom:2px;" placeholder="{{t.label_type_to_search}}" value="{{filter}}" />
        <a href="#" on-click="cleanFilter" id="schedule-users-available-clear" style="margin-left:2px;"><img src="modules/{{moduleName}}/assets/img/cross_grey.png" alt="x" style="height:8px;width:8px;" /></a>
    </div>
    <div style="display:inline-block;vertical-align:top;">
        <select  style="width:300px;height:100px;" multiple="multiple" on-change="changed" value="{{markedUsers}}">
            {{#filteredUsers:id}}
            {{^selected}}
            <option value="{{id}}">{{name}}</option>
            {{/selected}}
            {{/filteredUsers:id}}
        </select>
    </div>
    <div style="display:inline-block;vertical-align:top;padding-top:50px">
        <a href="#" on-click="selectUsers:{{markedUsers}}">
            <img alt="->" src="modules/{{moduleName}}/assets/img/arrow_right.png" />
        </a>
    </div>
    <div style="margin-bottom:-7px;border:1px solid darkgrey;min-height:100px;display:inline-block;width:30em;vertical-align:top;">
        <table>
            {{#usersById:id}}
            {{#selected}}
            <tr>
                <td>{{name}}</td>
                <td><a href="#" on-click="removeUser:{{id}}" style="margin-left:2px;"><img src="modules/{{moduleName}}/assets/img/cross_grey.png" alt="x" style="height:8px;width:8px;" /></a></td>
            </tr>
            {{/selected}}
            {{/usersById:id}}
        </table>
    </div>
    {{#emailpermission}}
    <br>
    <div style="display:inline-block; margin-top:5px;">
        {{> customEmailBasic}}
    </div>
    {{/emailpermission}}
    {{/isScheduled}}
    <br>
</div>
</script>


<script id="reportLabelBasic" type="text/ractive-template">
    <div style="color: grey">{{t.label_format_thousand}} <linkToProVersion /></div>
</script>
<script id="reportLabelsTemplate" type="text/ractive-template">
{{#labels.length}}
<table class="ar-data-table {{^userPermissions.canEdit}}disabled{{/}}">
	<thead>
		<tr>
			<th>{{t.label_original_report_label}}</th>
			<th>{{t.label_new_report_label}}</th>
			<th>{{t.formatSettings}} {{#isBasic}}<linkToProVersion />{{/isBasic}}</th>
		</tr>
	</thead>
	<tbody>
		{{#labels}}
		<tr>
			<td>{{origValue}}</td>
			<td><input type="text" style="border:1px solid #ccc;width:97%;" value="{{value}}" /></td>

			<td>
            {{#format.type == 'number'}}
                {{#isBasic}}
                {{> reportLabelBasic}}
                {{/isBasic}}

                {{^isBasic}}
                <selectBox
                        selected="{{format.group}}"
                        options="{{formattingGroups}}"
                        titleName="title"
                        valueName="type"
                />
                {{#format.group == 'time'}}
                <selectBox
                        selected="{{format.separator}}"
                        options="{{format.group == 'time' ? ~/timeFormatting : ~/separators}}"
                        titleName="title"
                        valueName="name"
                />
                {{/#format.group}}
                {{^format.group == 'time'}}
                <selectBox selected="{{format.separator}}" options="{{~/separators}}" titleName="title" valueName="name" />
                {{t.label_format_decimal}} <selectBox selected="{{format.decimal}}" options="{{~/separators}}" titleName="title" valueName="name" />
                <selectBox selected="{{format.decimalDigits}}" options="{{~/decimals}}" titleName="title" valueName="name" />
                {{/^format.group}}
                {{/isBasic}}
			{{/#format.type}}

			{{#format.type == 'integer'}}
                {{#isBasic}}
                {{> reportLabelBasic}}
                {{/isBasic}}

                {{^isBasic}}
                <selectBox
                        selected="{{format.group}}"
                        options="{{formattingGroups}}"
                        titleName="title"
                        valueName="type"
                />
                <selectBox
                        selected="{{format.separator}}"
                        options="{{format.group == 'time' ? ~/timeFormatting : ~/separators}}"
                        titleName="title"
                        valueName="name"
                />
                {{/isBasic}}
			{{/#format.type}}
			</td>
		</tr>
		{{/labels}}
	</tbody>
</table>
<br /><br />
<reportButtonPreview on-preview="preview" />
{{/labels.length}}
</script>

<script id="templatesManagerTemplate" type="text/ractive-template">
{{^isBasic}}
<label class="ar-label" for="templateNameSelect">{{t.selectExcelTemplate}}:</label><br/>
<select id="templateNameSelect" value="{{options.excelTemplateId}}" {{^userPermissions.canEdit}}disabled="disabled"{{/userPermissions.canEdit}} >
    {{#availableTemplates}}
        <option value="{{name}}">{{title}}</option>
    {{/}}
</select>

{{#if pending}}
<img src="modules/AnalyticReporting/assets/css/select2-spinner.gif" alt="{{t.label_loading}}..." />
{{/if}}

{{#userPermissions.canEdit}}
<a href="{{urls.uploadTemplate}}{{reportId}}" style="margin-left:10px;">Upload template</a>
{{/userPermissions.canEdit}}

{{#if isProcessing}}
<div style="margin:10px 0; padding:10px; color: #176de5">
    <img src="modules/AnalyticReporting/assets/css/select2-spinner.gif" alt="Processing..." style="vertical-align:middle;"/>
    <span style="margin-left:3px;">This template worksheet processing is delayed. Worksheet list will be available after process will complete or after page refresh.</span>
</div>
{{/if}}

{{#if availableWorksheets}}
<div style="margin:10px 0;">
<label class="ar-label">{{t.selectHiddenWorksheets}}:</label><br/>
    {{#each availableWorksheets}}
    <div>
        <input type="checkbox" value="{{name}}" name="{{options.hiddenWorksheets}}" /> {{name}}
    </div>
    {{/each}}
</div>
{{/if}}

{{#if showAddTable}}
<br />
<table>
    <thead>
        <tr>
            <th></th>
            <th>{{t.row}}</th>
            <th>{{t.column}}</th>
            <th>{{t.sheet}}</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        {{#options.virtualReportIdsWithOptions:i}}
            <tr>
                <td>
                    <selectBox
                        selected="{{id}}"
                        options="{{availableReports}}"
                        titleName="title"
                        valueName="name"
                        addDecorator="select2"
                        {{^userPermissions.canEdit}}readOnly="true"{{/userPermissions.canEdit}}
                    />
                </td>
                <td><input size="5" value='{{shiftRows}}' {{^userPermissions.canEdit}}disabled="disabled"{{/userPermissions.canEdit}} type="number" min="0" max="100" /></td>
                <td><input size="5" value='{{shiftColumns}}' {{^userPermissions.canEdit}}disabled="disabled"{{/userPermissions.canEdit}} type="number" min="0" max="100" /></td>
                <td><input size="5" value='{{sheet}}' {{^userPermissions.canEdit}}disabled="disabled"{{/userPermissions.canEdit}} type="number" min="1" max="100" step="1" /></td>
                <td>
                    {{#userPermissions.canEdit}}
                    <button class="ar-button red" on-click='removeReportFromTemplate:{{i}}'>{{t.label_remove}}</button>
                    {{/userPermissions.canEdit}}

                    <a href="{{urls.report}}{{id}}">{{t.openReport}}</a>
                </td>
            </tr>
        {{/}}
    </tbody>
</table>

{{#userPermissions.canEdit}}
<br />
<button class="ar-button green" on-click='addReportToTemplate'>{{t.addReport}}</button>
{{/userPermissions.canEdit}}

<br /><br />
{{/if}}

<div>
    <label>{{t.label_sync_this_report_with_google}}: <input type="checkbox" checked="{{syncWithGoogle}}" /></label>
</div>
{{/isBasic}}
</script>

<script id="reportCalcFieldManagerTemplate" type="text/ractive-template">
<table class="ar-data-table {{^userPermissions.canEdit}}disabled{{/userPermissions.canEdit}}">
    <thead>
    <tr>
        <th>{{t.label_field}}</th>
        <th>{{t.label_calc_formula}}</th>
        <th>{{t.label_formula}}</th>
        <th>Custom formula</th>
        <th>&nbsp;</th>
    </tr>
    </thead>
    <tbody>
    {{#calcFields:i}}
    <tr>
        <td>
            {{#calcFields[i].val.op == "if"}}
            Type:
            <selectBox selected="{{val.fieldtype}}" options="{{fieldTypes}}" titleName="title" valueName="name" />
            {{/}}
            <input type="text" value="{{calcFields[i].title}}">
        </td>
        <td><calcField availableFields="{{~/availableFields}}" fns="{{~/fns}}" val="{{calcFields[i].val}}"/></td>
        <td><calcFieldDisplay fieldsByName="{{~/fieldsByName}}" fns="{{~/fns}}" val="{{calcFields[i].val}}"/></td>
        <td>
            {{^isBasic}}
            <input type='checkbox' checked='{{calcFields[i].enableCustomSQL}}'>Override formula <br/>
            <a href="#" on-click="editSQL:{{i}}">Edit formula</a>
            {{/isBasic}}
            {{#isBasic}}
            <div style="color: grey">
                <input type='checkbox' disabled>Override formula <linkToProVersion /><br/>
                <div>Edit formula <linkToProVersion /></div>
            </div>
            {{/isBasic}}
        </td>
        <td>
            <a href="#" on-click="removeItem:{{i}}" class="tooltipX">
                <img src="modules/{{moduleName}}/assets/img/cross.png"/>
                <span>{{t.label_remove_item}}</span>
            </a>
        </td>
    </tr>
    {{/calcFields}}
    </tbody>
</table>

{{#userPermissions.canEdit}}
<div style="margin-left:705px">
    <reportBtnGC on-clicked="addField" text="{{t.label_add_item}}"/>
</div>
{{/userPermissions.canEdit}}

<reportButtonPreview on-preview="preview"/>

<br/><br/>
</script>

<script id="reportCalcFieldTemplate" type="text/ractive-template">
<div style="display: inline-block;border: 0px solid #000000; padding: 0.66em;border-radius:0.33em;background: {{#level % 2}}#d0e5f5{{else}}#ffffff{{/level % 2}}">
    <selectBox selected="{{val.op}}" options="{{fns}}" titleName="title" valueName="name" />

    {{# selectedFn.name == "if"}}
        {{#val.valType:i}}

            {{#i == 0}}
                {{>reportCalcFieldTypesTemplate}}
                <br/>
                {{conditionLabel}}:
                <selectBox selected="{{val.cond}}" options="{{condTypes}}" titleName="title" valueName="name" />


            {{/}}


            {{#i == 1}}
                {{#val.cond == "equal"}}
                    {{>reportCalcFieldTypesTemplate}}
                {{else}}
                    {{#val.cond == "nequal"}}
                        {{>reportCalcFieldTypesTemplate}}
                    {{/}}
                {{/}}
            {{/}}


            {{#i == 2}}
                <br/>
                {{thenLabel}}:
                {{>reportCalcFieldTypesTemplate}}
            {{/}}

            {{#i == 3}}
                <br/>
                {{otherLabel}}:
                {{>reportCalcFieldTypesTemplate}}
            {{/}}
        {{/valType:i}}


    {{else}}
        {{# selectedFn.name == "addDate"}}
           {{#val.valType:i}}
            {{#i == 0}}
                {{>reportCalcFieldFTypesTemplate}}
            {{/}}
            {{#i == 1}}
                {{>reportCalcFieldCTypesTemplate}}
            {{/}}
            {{/valType:i}}
           <selectBox options="{{dateTimeTypes}}" valueName="name" titleName="title" selected="{{val.val[2]}}" />
        {{else}}
            {{# selectedFn.name == "subtractDate"}}
               {{#val.valType:i}}
                {{#i == 0}}
                    {{>reportCalcFieldFTypesTemplate}}
                {{/}}
                {{#i == 1}}
                    {{>reportCalcFieldCAndFTypesTemplate}}
                {{/}}
                {{/valType:i}}
               <selectBox options="{{dateTimeTypes}}" valueName="name" titleName="title" selected="{{val.val[2]}}" />
               <label>{{t.label_businessHours}}:&nbsp<input style="vertical-align: middle;" type="checkbox" checked="{{val.val[3]}}" /></label>
            {{else}}
                {{#val.valType:i}}
                   {{>reportCalcFieldTypesTemplate}}
                {{/valType:i}}
            {{/}}
        {{/}}
    {{/}}
</div>
</script>
<script type="text/ractive-template" id="reportCalcFieldTypesTemplate">
    <div style="display: inline-block;border: 1px #000000 dotted; border-radius: 0.25em; margin: 0.25em; padding: 0.25em">
        <selectBox selected="{{.}}" options="{{opTypes}}" titleName="title" valueName="name" />
        {{# val.valType[i]=="c"}}
            {{#i == 1}}
                {{# val.valType[0] == "f"}}
                    {{#availableFieldValues(val.val[0], availableFields).length}}
                        <select decorator="select2" value="{{val.val[i]}}" multiple="multiple">
                            {{#availableFieldValues(val.val[0], availableFields)}}
                            <option value="{{val}}">{{title}}</option>
                            {{/}}
                        </select>
                    {{/.length}}
                    {{^availableFieldValues(val.val[0], availableFields).length}}
            <input type="text" value="{{val.val[i]}}" />
        {{/}}
                {{else}}
                    <input type="text" value="{{val.val[i]}}" />
                {{/}}
            {{else}}
                <input type="text" value="{{val.val[i]}}" />
            {{/}}
        {{/}}
        {{# val.valType[i]=="f"}}

            <selectBox options="{{availableFieldsFiltered}}" valueName="name" titleName="title" selected="{{val.val[i]}}" addDecorator="select2" />
        {{/}}
        {{# val.valType[i]=="op"}}
            <calcField fns="{{fns}}" val="{{val.val[i]}}" level="{{level+1}}" availableFields="{{~/availableFields}}" fieldsByName="{{~/fieldsByName}}" />
        {{/}}
    </div>
</script>

<script type="text/ractive-template" id="reportCalcFieldCAndFTypesTemplate">
    <div style="display: inline-block;border: 1px #000000 dotted; border-radius: 0.25em; margin: 0.25em; padding: 0.25em">
        <selectBox selected="{{.}}" options="{{opTypesCAndFAndToday}}" titleName="title" valueName="name" />
        {{# val.valType[i]=="c"}}
            <input type="text" value="{{val.val[i]}}" />
        {{/}}
        {{# val.valType[i]=="f"}}
            <selectBox options="{{availableFieldsFiltered}}" valueName="name" titleName="title" selected="{{val.val[i]}}" addDecorator="select2" />
        {{/}}
        {{# val.valType[i]=="Today"}}
        {{/}}
    </div>
</script>

<script type="text/ractive-template" id="reportCalcFieldFTypesTemplate">
    <div style="display: inline-block;border: 1px #000000 dotted; border-radius: 0.25em; margin: 0.25em; padding: 0.25em">
        <selectBox selected="{{.}}" options="{{opTypesFAndToday}}" titleName="title" valueName="name" />
        {{# val.valType[i]=="f"}}
            <selectBox options="{{availableFieldsFiltered}}" valueName="name" titleName="title" selected="{{val.val[i]}}" addDecorator="select2" />
        {{/}}
        {{# val.valType[i]=="Today"}}
        {{/}}
    </div>
</script>

<script type="text/ractive-template" id="reportCalcFieldCTypesTemplate">
    <div style="display: inline-block;border: 1px #000000 dotted; border-radius: 0.25em; margin: 0.25em; padding: 0.25em">
        <selectBox selected="{{.}}" options="{{opTypesC}}" titleName="title" valueName="name" />
        {{# val.valType[i]=="c"}}
            <input type="text" value="{{val.val[i]}}" />
        {{/}}
    </div>
</script>

<script type="text/ractive-template" id="reportCalcFieldDisplayTemplate">
{{fnName}}
{{#fnName != 'If'}}({{/}}
{{#fnArgs:i}}
    {{.}}
    {{# val.valType[i]=="c"}}{{val.val[i]}}{{/}}
    {{# val.valType[i]=="f"}}{{fieldsByName[val.val[i]].bareTitle}}{{/}}
    {{# val.valType[i]=="op"}}<calcFieldDisplay fns="{{fns}}" val="{{val.val[i]}}" level="{{level+1}}" />{{/}}
    {{# val.valType[i]=="today"}}{{t.label_today}}{{/}}
{{/}}
{{#fnName != 'If'}}){{/}}
</script>

<script id="calcFieldCustomSQLTemplate" type="text/ractive-template">
<div class="ar-calc-customsql">
  <h2>Field formula</h2>
  <span class="ar-calc-customsql-fieldname">{{fieldSQL}}</span>
  <h2>Custom formula</h2>
  <textarea rows="10" cols="118" value='{{customSQL}}'></textarea>
  <h2>Field Name Convertor</h2>
  <div class="ar-calc-customsql-fieldselector">
    <selectBox selected="{{selectedField}}" options="{{availableFields}}" titleName="title" valueName="name" addDecorator="select2"/>
    <span class="ar-calc-customsql-fieldname">#{{selectedField}}#</span>
  </div>

  <button class="ar-button red" on-click='saveCustomSQL'>Save</button>
  <button class="ar-button" on-click='cancelCustomSQL'>Cancel</button>
</div>
</script>

<script type="text/ractive-template" id="reportPaginationTemplate">
{{#isPagination}}
{{t.label_page_no}}
<ul>
{{#pagination}}
	{{#currentPage}}
		<li class="active">{{pageNumber}}</li>
	{{/currentPage}}
	{{^currentPage}}
		{{#pageSeparator}}
		{{symbol}}
		{{/pageSeparator}}
		{{^pageSeparator}}
		<li on-click="changePage(pageNumber)">
			{{pageNumber}}
		</li>
		{{/pageSeparator}}
	{{/currentPage}}
{{/pagination}}
</ul>
{{/isPagination}}

{{#showLimitInput}}
<span {{#isPagination}}style="margin-left:20px;"{{/isPagination}}>
{{t.label_show}} <input value="{{meta.limit}}" size="3" maxlength="3" class="input-mini" /> {{t.label_records_per_page}}
<span>
{{/showLimitInput}}

{{#positionTop}}
{{#isBasic}}
<span style="margin-left:20px;color: grey">
{{t.label_freeze_first}} <linkToProVersion /> <input type="text" size="3" value="0" disabled /> {{t.label_columns}}
</span>
{{/isBasic}}

{{^isBasic}}
<span style="margin-left:20px;">
{{t.label_freeze_first}} <input type="text" id="frozenColumn" name="frozenColumn" size="3" value="{{frozenColumn}}" /> {{t.label_columns}}
</span>
{{/isBasic}}
{{/positionTop}}

</script>

<script type="text/ractive-template" id="addToDashboardModal">

<div style="padding-top:10px;">
    <input type="radio" name="addType" value="self" checked />{{t.add_to_own_home}}<br>
    <input type="radio" name="addType" value="role" />{{t.add_to_role_home}}
    <select name="chosenRoleID">
        {{#availableRoles}}
            <option value="{{id}}">{{name}}</option>
        {{/availableRoles}}
    </select>
</div>

</script>

<script type="text/ractive-template" id="importReportsTemplate">

    <form method="post" id="import-reports" enctype="multipart/form-data">
        <input type="hidden" name="module" value="{{moduleName}}" />
        <input type="hidden" name="action" value="importReports" />
        <input type="hidden" name="_action" value="import" />
        <label><input type="radio" name="importType" value="withCategories" checked/>{{t.label_import_type_structure}}</label><br />
        <label><input type="radio" name="importType" value="inCategory"/>{{t.label_import_type_category}}</label>
        <select name="category">
            {{#categories}}
                <option value="{{name}}">{{title}}</option>
            {{/categories}}
        </select>
        <br />
        <input name="importFile" type="file" />
    </form>

</script>

<script type="text/ractive-template" id="debugManagerTemplate">
    <h2>Response</h2><textarea>{{response}}</textarea>
    <h2>Debug</h2><textarea>{{debug}}</textarea>
    <h2>Query result data</h2><textarea>{{queryResult}}</textarea>
    <h2>Query error</h2><textarea>{{queryError}}</textarea>
    <h2>Query exection time</h2><textarea>{{queryTime}}</textarea>
    <h2>Query result count</h2><textarea>{{queryCount}}</textarea>
    <h2>Processor result count</h2><textarea>{{processorCount}}</textarea>

    <br /><br />
    <reportButtonPreview on-preview="preview" />
</script>

{/literal}
<!-- // TEMPLATES -->
