<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.

 * SuiteCRM is an extension to SugarCRM Community Edition developed by Salesagility Ltd.
 * Copyright (C) 2011 - 2014 Salesagility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for  technical reasons, the Appropriate Legal Notices must
 * display the words  "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 ********************************************************************************/

//Bug 30094, If zlib is enabled, it can break the calls to header() due to output buffering. This will only work php5.2+
// ini_set('zlib.output_compression', 'Off');

// ob_start();

 /* if(!(ACLController::checkAccess('AOS_Invoices', 'edit', true))){
	ACLController::displayNoAccess();
	die;
 } */

require_once('modules/AOS_Quotes/AOS_Quotes.php');
require_once('modules/AOS_Invoices/AOS_Invoices.php');
require_once('modules/AOS_Products_Quotes/AOS_Products_Quotes.php');
	
require_once('include/export_utils.php');
require_once('include/tcpdf/tcpdf.php');
global $sugar_config;
global $locale;
global $current_user, $timedate;
global $app_list_strings;

$the_module = clean_string($_REQUEST['module']);

if($sugar_config['disable_export'] 	|| (!empty($sugar_config['admin_export_only']) && !(is_admin($current_user) || (ACLController::moduleSupportsACL($the_module)  && ACLAction::getUserAccessLevel($current_user->id,$the_module, 'access') == ACL_ALLOW_ENABLED &&
    (ACLAction::getUserAccessLevel($current_user->id, $the_module, 'admin') == ACL_ALLOW_ADMIN ||
     ACLAction::getUserAccessLevel($current_user->id, $the_module, 'admin') == ACL_ALLOW_ADMIN_DEV))))){
	die($GLOBALS['app_strings']['ERR_EXPORT_DISABLED']);
}

//check to see if this is a request for a sample or for a regular export
if(!empty($_REQUEST['sample'])){
    //call special method that will create dummy data for bean as well as insert standard help message.
    $content = exportSample(clean_string($_REQUEST['module']));

}else if(!empty($_REQUEST['uid'])){
	$content = export_custom_pdf(clean_string($_REQUEST['module']), $_REQUEST['uid'], isset($_REQUEST['members']) ? $_REQUEST['members'] : false);
}else{
	$content = export_custom_pdf(clean_string($_REQUEST['module']));
}

// echo "<pre>";
// print_r($content);
// exit;

//Setting Invoice Values
	$invoice = new AOS_Invoices();
	$invoice->name = "Invoice";
	$invoice->save();

	
	$group_invoice = new AOS_Line_Item_Groups();
	$group_invoice->parent_id = $invoice->id;
	$group_invoice->parent_type = 'AOS_Invoices';
	$group_invoice->name = 'Time Sheet Billing';
	$group_invoice->save();
	
	//Setting Line Items
	foreach($content['records'] as $time_tracker_id => $time_tracker_arr){	
		$user_id = $time_tracker_arr['user_id_c'];
		$user_obj = BeanFactory::getBean("Users", $user_id);
		$per_hour_rate = $user_obj->hourly_rate;
		
		$task_name = clean($time_tracker_arr['related_record_url']);
		
		if(empty($task_name) || $task_name == "No Related Record"){
			$seed_obj = BeanFactory::getBean($time_tracker_arr['parent_type'], $time_tracker_arr['parent_id']);
			$task_name = $seed_obj->name;
		}
		
		$row['id'] = '';
		$row['name'] = "Billing of ".$task_name." :: ".$time_tracker_arr['track_date']. "  (".$time_tracker_arr['hours']." Hours)";
		$row['product_unit_price'] = $per_hour_rate * $time_tracker_arr['hours'];
		$row['vat_amt'] = '0.0';
		$row['product_list_price'] = $per_hour_rate * $time_tracker_arr['hours'];
		$row['product_total_price'] = $per_hour_rate * $time_tracker_arr['hours'];
		$row['group_id'] = $group_invoice->id;
		$row['parent_id'] = $invoice->id;
		$row['parent_type'] = 'AOS_Invoices';
		$row['currency_id'] = '-99';
		$prod_invoice = new AOS_Products_Quotes();
		$prod_invoice->populateFromRow($row);
		$prod_invoice->save();
	}
	ob_clean();
	header('Location: index.php?module=AOS_Invoices&action=EditView&record='.$invoice->id);