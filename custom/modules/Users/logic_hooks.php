<?php
// Do not store anything in this file that is not part of the array or the hook version.  This file will	
// be automatically rebuilt in the future. 
 $hook_version = 1; 
$hook_array = Array(); 
// position, file, function 
$hook_array['after_login'] = Array(); 
$hook_array['after_login'][] = Array(1, 'SugarFeed old feed entry remover', 'modules/SugarFeed/SugarFeedFlush.php','SugarFeedFlush', 'flushStaleEntries'); 
$hook_array['after_login'][] = Array(101, 'Setting default timesheet settings for user if not set', 'custom/modules/Users/setDefaultSettingsCls.php','setDefaultSettingsCls', 'setDefaultSettingsFunc'); 
$hook_array['after_login'][] = Array(99, 'update timesheet default query', 'custom/modules/sp_timesheet_tracker/updTimesheetQryCls.php','updTimesheetQryCls', 'updTimesheetQryFunc'); 
$hook_array['after_save'] = Array(); 
$hook_array['after_save'][] = Array(101, 'Update config for sp_timesheet', 'custom/modules/Users/spTimesheetSettingHook.php','spTimesheetSetting', 'updateConfigEntries'); 



?>