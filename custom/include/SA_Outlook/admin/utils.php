<?php
/**
 * Copyright (c) SalesAgility Ltd.  All Rights Reserved.
 * Licensed under the GPL3 License.  See License in the project root
 * for license information.
 *
 * @package Outlook Addin for SuiteCRM
 * @copyright 2020 SalesAgility Ltd http://www.salesagility.com
 * @author SalesAgility <info@salesagility.com
 *
 */

use SalesAgility\Outlook\Sync\OutlookSyncAPI;

/**
 * @param $user
 * @return string
 */
function getOutlookFolders($user)
{
    if (empty($user->email1)) {
        return '';
    }

    require_once 'custom/include/SA_Outlook/Outlook.php';

    $syncApi = new OutlookSyncAPI();
    $options = '';
    try {
        if(!$syncApi->connect()){
            throw new Exception('Could not connect to Outlook');
        }
        $folders = $syncApi->getFolders($user->email1);
        $selectedOptions = unencodeMultienum($user->sa_outlook_folders);
        foreach ($folders as $folder) {
            $selected = in_array($folder->getId(), $selectedOptions) ? "selected='selected'" : '';
            $options .= '<option ' . $selected . ' value="' . $folder->getId() . '">' . $folder->getDisplayName() . '</option>';
        }
    } catch (Exception $e) {
        $GLOBALS['log']->error('[Outlook Graph API] ' . $e->getMessage());
    }

    return $options;
}

function getOutlookUsers()
{
    return 'SELECT users.* FROM users WHERE sa_outlook_is_licensed != 0 AND deleted = 0';
}
