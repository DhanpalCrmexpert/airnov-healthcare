<?php
 // created: 2016-09-26 21:04:57
$layout_defs["Project"]["subpanel_setup"]['project_sp_timesheet_tracker_1'] = array (
  'order' => 100,
  'module' => 'sp_timesheet_tracker',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_PROJECT_SP_TIMESHEET_TRACKER_1_FROM_SP_TIMESHEET_TRACKER_TITLE',
  'get_subpanel_data' => 'project_sp_timesheet_tracker_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
