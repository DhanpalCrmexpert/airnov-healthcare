<?php
$viewdefs ['Accounts'] = 
array (
  'QuickCreate' => 
  array (
    'templateMeta' => 
    array (
      'form' => 
      array (
        'buttons' => 
        array (
          0 => 'SAVE',
          1 => 'CANCEL',
        ),
      ),
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'includes' => 
      array (
        0 => 
        array (
          'file' => 'modules/Accounts/Account.js',
        ),
      ),
      'useTabs' => true,
      'tabDefs' => 
      array (
        'LBL_ACCOUNT_INFORMATION' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
        'LBL_PANEL_ADVANCED' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL1' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
      ),
    ),
    'panels' => 
    array (
      'lbl_account_information' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'name',
            'displayParams' => 
            array (
              'required' => true,
            ),
          ),
          1 => 
          array (
            'name' => 'phone_office',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'website',
          ),
          1 => 
          array (
            'name' => 'phone_fax',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'customer_number_c',
            'label' => 'LBL_CUSTOMER_NUMBER',
          ),
          1 => 
          array (
            'name' => 'customer_account_group_c',
            'studio' => 'visible',
            'label' => 'LBL_CUSTOMER_ACCOUNT_GROUP',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'email1',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'billing_address_street',
            'comment' => 'The street address used for billing address',
            'label' => 'LBL_BILLING_ADDRESS_STREET',
          ),
          1 => 
          array (
            'name' => 'shipping_address_street',
            'comment' => 'The street address used for for shipping purposes',
            'label' => 'LBL_SHIPPING_ADDRESS_STREET',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'description',
            'comment' => 'Full text of the note',
            'label' => 'LBL_DESCRIPTION',
          ),
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'assigned_user_name',
          ),
          1 => 
          array (
            'name' => 'ahc_key_accounts_accounts_1_name',
            'label' => 'LBL_AHC_KEY_ACCOUNTS_ACCOUNTS_1_FROM_AHC_KEY_ACCOUNTS_TITLE',
          ),
        ),
      ),
      'LBL_PANEL_ADVANCED' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'account_type',
          ),
          1 => 
          array (
            'name' => 'industry',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'annual_revenue_year_c',
            'studio' => 'visible',
            'label' => 'LBL_ANNUAL_REVENUE_YEAR',
          ),
          1 => 
          array (
            'name' => 'annual_revenue',
            'comment' => 'Annual revenue for this company',
            'label' => 'LBL_ANNUAL_REVENUE',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'employees',
            'comment' => 'Number of employees, varchar to accomodate for both number (100) or range (50-100)',
            'label' => 'LBL_EMPLOYEES',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'parent_name',
            'label' => 'LBL_MEMBER_OF',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'campaign_name',
            'comment' => 'The first campaign name for Account (Meta-data only)',
            'label' => 'LBL_CAMPAIGN',
          ),
        ),
      ),
      'lbl_editview_panel1' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'potential_impact_usd_budget_nineteen_c',
            'label' => 'Potential impact USD budget 2020',
          ),
          1 => 
          array (
            'name' => 'potential_impact_usd_budget_twenty_c',
            'label' => 'Potential impact USD budget 2022',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'potential_impact_usd_budget_twentyone_c',
            'label' => 'LBL_POTENTIAL_IMPACT_USD_BUDGET_TWENTYONE',
          ),
          1 => 
          array (
            'name' => 'projects_won_c',
            'label' => 'LBL_PROJECTS_WON',
          ),
        ),
      ),
    ),
  ),
);
;
?>
