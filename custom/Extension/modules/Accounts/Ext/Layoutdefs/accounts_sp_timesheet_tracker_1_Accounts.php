<?php
 // created: 2016-09-26 21:38:56
$layout_defs["Accounts"]["subpanel_setup"]['accounts_sp_timesheet_tracker_1'] = array (
  'order' => 100,
  'module' => 'sp_timesheet_tracker',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_ACCOUNTS_SP_TIMESHEET_TRACKER_1_FROM_SP_TIMESHEET_TRACKER_TITLE',
  'get_subpanel_data' => 'accounts_sp_timesheet_tracker_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
