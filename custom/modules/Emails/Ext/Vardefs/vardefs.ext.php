<?php 
 //WARNING: The contents of this file are auto-generated


// created: 2019-03-30 13:22:29
$dictionary["Email"]["fields"]["sg_sendgrid_event_emails"] = array (
  'name' => 'sg_sendgrid_event_emails',
  'type' => 'link',
  'relationship' => 'sg_sendgrid_event_emails',
  'source' => 'non-db',
  'module' => 'sg_SendGrid_Event',
  'bean_name' => false,
  'side' => 'right',
  'vname' => 'LBL_SG_SENDGRID_EVENT_EMAILS_FROM_SG_SENDGRID_EVENT_TITLE',
);

?>