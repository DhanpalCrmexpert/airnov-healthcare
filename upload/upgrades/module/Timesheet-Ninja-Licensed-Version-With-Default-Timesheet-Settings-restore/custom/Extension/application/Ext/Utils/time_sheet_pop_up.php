<?php
function time_sheet_pop_up($js_alert_obj){
		global $app_list_strings, $timedate, $current_user, $app_strings, $db;
		
		$current_date_time = $timedate->get_gmt_db_datetime();
		// Prep time trackker Query
		$selectProjectTask = "SELECT * from sp_auto_timesheet_time_tracker WHERE processed = 0 and deleted = 0 and user_id_c = '".$current_user->id."'";
		
		$result = $db->query($selectProjectTask);

		while($row = $db->fetchByAssoc($result)){
			
			if(!empty($row['last_updated_at'])){
				$last_updated_at = $row['last_updated_at'];
			}
			else{
				$last_updated_at = $row['date_entered'];
			}
			
			$record_id = $row['id'];
			$parent_type = $row['parent_type'];
			$parent_id = $row['parent_id'];
			
			$parent_module_bean = BeanFactory::getBean($parent_type, $parent_id);
			
			if(empty($parent_module_bean->id)){
				continue;
			}
			
			$record_name = $parent_module_bean->name;
			
			$mints = get_time_difference_in_mints($last_updated_at, $current_date_time);
			
			if($mints >= 20){
				
				$update_query = "UPDATE sp_auto_timesheet_time_tracker set last_updated_at = '".$current_date_time."' where id = '".$record_id."'";
				
				$db->query($update_query);
				
				$projectTaskDescription = "To stop timer, click here";

				$js_alert_obj->addAlert($app_strings['MSG_JS_ALERT_MTG_REMINDER_TIMESHEET_REMINDER'], $record_name, '', $app_strings['MSG_JS_ALERT_MTG_REMINDER_DESC'].$projectTaskDescription , 1, 'index.php?action=DetailView&module='.$parent_type.'&record=' . $parent_id);
			}
		}
}

function get_time_difference_in_mints( $start, $end )
{
	$uts['start']      =    strtotime( $start );
	$uts['end']        =    strtotime( $end );
	
	if( $uts['start']!==-1 && $uts['end']!==-1 )
	{
		if( $uts['end'] >= $uts['start'] )
		{
			$diff    =    $uts['end'] - $uts['start'];
			$minutes=intval((floor($diff/60)));	
			return $minutes;
		}
		else
		{
			trigger_error( "Ending date/time is earlier than the start date/time", E_USER_WARNING );
		}
	}
	else
	{
		trigger_error( "Invalid date/time data detected", E_USER_WARNING );
	}
	return( false );
}