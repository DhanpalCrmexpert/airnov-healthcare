<?php
/*********************************************************************************
 * This file is part of package Price Books.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Price Books is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
$mod_strings['LBL_PRICE_BOOKS'] = "Libros de precios";
$mod_strings['LBL_PRICE_BOOKS_DESCRIPTION'] = "Administrar libros de precios de productos para contactos / cuentas particulares";
$mod_strings['LBL_ENABLE_PRICE_BOOKS'] = "Habilitar la función de libros de precios";
$mod_strings['LBL_PRICE_BOOKS_ACTIVATED'] = "Característica de libros de precios activada con éxito";
$mod_strings['LBL_PRICE_BOOKS_DEACTIVATED'] = "Característica de libros de precios desactivada con éxito";
$mod_strings['LBL_CLICK_HERE'] = "Haga clic aquí";
$mod_strings['LBL_MANAGE_PRICEBOOK'] = "Para administrar libros de precios";
//license
$mod_strings['LBL_LICENCE_ACTIVE_LABEL'] = "VIPriceBooksLicenseAddon ya no está activo debido a la siguiente razón:";
$mod_strings['LBL_LICENCE_ISSUES'] = "Los usuarios no tendrán acceso limitado hasta que se haya resuelto el problema";
$mod_strings['LBL_CLICK_HERE'] = "Haga clic aquí";
$mod_strings['LBL_LICENCE_ACTIVE'] = "VIPriceBooksLicenseAddon ya no está activo";
$mod_strings['LBL_RENEW_LICENCE'] = "Renueve su suscripción o verifique la configuración de su licencia.";
$mod_strings['LBL_UPDATE_LICENSE'] = "Actualizar licencia";
?>