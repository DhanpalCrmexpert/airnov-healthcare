<?php

$admin_option_defs=array();

$admin_option_defs['Outlook']['outlook_addin_config'] = array(
    'Outlook Plugin Configuration',
    'LBL_OUTLOOK_ADDIN_CONFIG_TITLE',
    'LBL_OUTLOOK_ADDIN',
    './index.php?module=Administration&action=OutlookAddinConfig',
    'outlook-addin-config'
);

$admin_group_header[]= array('LBL_OUTLOOK_ADDIN_TITLE','',false,$admin_option_defs, 'LBL_OUTLOOK_ADDIN_TOOLS_DESC');
