<?php 
 //WARNING: The contents of this file are auto-generated



$dictionary['User']['fields']['SendGrid']['name'] = 'SendGrid';
$dictionary['User']['fields']['SendGrid']['vname'] = 'LBL_SENDGRID';
$dictionary['User']['fields']['SendGrid']['type'] = 'bool';



$dictionary['User']['fields']['sa_outlook_sync_date'] = array(
    'name' => 'sa_outlook_sync_date',
    'vname' => 'LBL_SA_OUTLOOK_SYNC_DATE',
    'type' => 'datetime',
    'size' => '20',
    'required' => false,
    'audited' => true,
    'reportable' => true,
    'importable' => true,
    'unified_search' => false,
    'duplicate_merge' => 'true',
    'inline_edit' => true
);
$dictionary['User']['fields']['sa_outlook_folders'] = array(
    'name' => 'sa_outlook_folders',
    'vname' => 'LBL_SA_OUTLOOK_USER_FOLDERS',
    'type' => 'multienum',
    'options' => 'sa_outlook_folders_options',
    'function' => array('name'=>'getOutlookFolders', 'returns'=>'html', 'include'=>'custom/include/SA_Outlook/admin/utils.php'),
);

$dictionary['User']['fields']['sa_outlook_enable'] = array(
    'name' => 'sa_outlook_enable',
    'vname' => 'LBL_SA_OUTLOOK_USER_ENABLE',
    'type' => 'bool',
);

$dictionary['User']['fields']['sa_outlook_is_licensed'] = array(
    'name' => 'sa_outlook_is_licensed',
    'vname' => 'LBL_SA_OUTLOOK_USER_IS_LICENSED',
    'type' => 'bool',
);


$dictionary['User']['fields']['hourly_rate'] =  array (
      'name' => 'hourly_rate',
      'vname' => 'LBL_HOURLY_RATE',
      'type' => 'currency',
      'len' => '26,6',
      'merge_filter' => 'disabled',
      'audited' => 1,
	  'default' => '0',
    );
 

$dictionary['User']['fields']['timesheet_lock'] =  array (
      'name' => 'timesheet_lock',
      'vname' => 'LBL_TIMESHEET_LOCK',
      'type' => 'enum',
      'audited' => 1,
	  'default' => 'none',
	  'len' => 20,
      'options' => 'timesheet_lock_dom',
    );
	
// $dictionary['User']['fields']['reminder_for_time_log'] =  array (
      // 'name' => 'reminder_for_time_log',
      // 'vname' => 'Reminder to Enter Time',
      // 'label' => 'Reminder to Enter Time',
      // 'type' => 'bool',
	  // 'default' => false,
    // );
	
$dictionary['User']['fields']['require_description'] =  array (
      'name' => 'require_description',
      'vname' => 'Description Required',
      'label' => 'Description Required',
      'type' => 'bool',
	  'default' => false,
    );
	
$dictionary['User']['fields']['reminder_to_enter_time'] =  array (
      'name' => 'reminder_to_enter_time',
      'vname' => 'Reminder to enter time',
      'label' => 'Reminder to enter time',
      'type' => 'bool',
	  'default' => false,
    );	
	
$dictionary['User']['fields']['reminder_time_limit'] =  array (
      'name' => 'reminder_time_limit',
      'vname' => 'Reminder time limit',
      'label' => 'Reminder time limit',
      'type' => 'decimal',
	  'default' => 0.0,
	  'len' => '18',
	  'size' => '10',
	  'precision' => '2',
    );	
	
$dictionary['User']['fields']['users_to_get_reminder'] = array (
	'name' => 'users_to_get_reminder',
	'vname' => 'Users to get reminder',
	'label' => 'Users to get reminder',
	'function' => 'getUsersList',
	'type' => 'multienum',
	'dbType' => 'text',
	'comment' => 'This is multi-select field for related users.',
);	
	
$dictionary['User']['fields']['sp_timesheet_module'] = array (
	'name' => 'sp_timesheet_module',
	'vname' => 'Module',
	'label' => 'Module',
	'function' => 'getModulesList',
	'type' => 'enum',
	'dbType' => 'text',
	'comment' => 'Display list of modules.',
);	
	
$dictionary['User']['fields']['sp_timesheet_related_module'] = array (
	'name' => 'sp_timesheet_related_module',
	'vname' => 'Related Module',
	'label' => 'Related Module',
	'function' => 'getRelatedModulesList',
	'type' => 'dynamicenum',
	'parentenum' => 'sp_timesheet_module',
	'dbType' => 'text',
	'comment' => 'Display list of realted modules.',
);


$dictionary['User']['fields']['user_preferred_timesheet_view'] = array (
	'name' => 'user_preferred_timesheet_view',
	'vname' => 'User preferred view',
	'label' => 'User preferred view',
	'type' => 'multienum',
	'options' => 'user_preferred_view_list',
	'isMultiSelect' => true,
	'default' => '^Daily^,^Weekly^',
	'dbType' => 'text',
	'comment' => 'This is preferred user view for timesheet set by admin.',
);

?>