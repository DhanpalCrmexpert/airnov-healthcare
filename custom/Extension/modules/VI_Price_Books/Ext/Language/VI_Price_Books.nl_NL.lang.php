<?php
/*********************************************************************************
 * This file is part of package Price Books.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Price Books is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
$mod_strings['LBL_ACTIVE'] = 'Actief';
$mod_strings['LBL_PERCENTAGE_MARK'] = 'Percentagemarkering';
$mod_strings['LBL_TYPE'] = 'Prijstype';
$mod_strings['LBL_ROUND_OFF'] = 'Afronden op';
$mod_strings['LBL_ACCOUNT_NAME'] = 'Accountnaam';
$mod_strings['LBL_ACCOUNT_ID'] = 'Account-ID';
$mod_strings['LBL_ACCOUNTS'] = 'Accounts';
$mod_strings['LBL_CONTACT_NAME'] = 'Contactnaam';
$mod_strings['LBL_CONTACT_ID'] = 'Contact-ID';
$mod_strings['LBL_CONTACTS'] = 'Contacten';
$mod_strings['LBL_FLAT_PRICE'] = 'Vaste prijs';
$mod_strings['LBL_DIFFERENTIAL_PERCENTAGE'] = 'Differential By (%)';
$mod_strings['LBL_DIFFERENTIAL_DOLLAR'] = 'Differential By ($)';
$mod_strings['LBL_PRICE_BOOKS'] = 'Prijsboeken';
$mod_strings['LBL_NAME'] = 'Naam';
$mod_strings['LBL_PRICE_TYPE'] = 'Prijstype';
$mod_strings['LBL_ASSIGNED_TO'] = 'Toegewezen aan';
$mod_strings['LBL_NO_RECORDS'] = 'Geen records gevonden';
$mod_strings['LBL_MARK_UP'] = 'MarkUp (toevoegen)';
$mod_strings['LBL_MARK_DOWN'] = 'MarkDown (Sub)';
$mod_strings['LBL_ADD_QUANTITY'] = 'Hoeveelheid toevoegen';
$mod_strings['LBL_PRODUCT_NAME'] = 'Productnaam';
$mod_strings['LBL_PRODUCT_PRICE'] = 'Productprijs';
$mod_strings['LBL_LIST_PRICE'] = 'Catalogusprijs';
$mod_strings['LBL_ADD_PRODUCT'] = 'Product toevoegen';
$mod_strings['LBL_YES'] = 'Ja';
$mod_strings['LBL_NO'] = 'Nee';
$mod_strings['LBL_SELECT_PRODUCT_MSG'] = "Selecteer een product voordat je op de prijsboeken klikt !!!";
$mod_strings['LBL_SELECT_PRICEBOOK_MSG'] = "Selecteer slechts één prijsboek.";
$mod_strings['LBL_PRODUCT_VALIDATION'] = 'Selecteer een ander product. Dit product is al geselecteerd. ';
$mod_strings['LBL_QUANTITY_VALID_MSG'] = "Ongeldige waarde:";
$mod_strings['LBL_FROM_QUANTITY'] = "From Quantity";
$mod_strings['LBL_TO_QUANTITY'] = "To Quantity";
$mod_strings['LBL_DIFFERENTIAL'] = "Differentiaal";
$mod_strings['LBL_PRICE_BOOKS_ACCESS'] = "Schakel de functie Prijsboeken in om toegang te krijgen tot de module Prijsboeken.";
$mod_strings['LBL_CLICK_HERE'] = "Klik hier";
$mod_strings['LBL_ENABLE_LABEL'] = "voor inschakelen.";
$mod_strings['LBL_FROM_QUANTITY_VALID'] = "Voer een grotere waarde in van hoeveelheid dan van vorige naar hoeveelheid.";
$mod_strings['LBL_TO_QUANTITY_VALID'] = "Voer een grotere hoeveelheid in dan een kwantiteitswaarde.";
$mod_strings['LBL_PRODUCT_LIST_PRICE_VALID'] = "Voeg product en catalogusprijs toe voor toe te voegen hoeveelheid.";
$mod_strings['LBL_PRODCUT_PRICE_VALID'] = "Voeg productprijs toe voor product.";
$mod_strings['LBL_LIST_PRICE_VALID'] = "Voeg catalogusprijs toe voor product.";
$mod_strings['LBL_QUANTITY_VALID_RANGE'] = "Voeg hoeveelheidsbereik toe voor product.";
$mod_strings['LBL_PRODUCT_VALID'] = "Voeg product toe voor het toevoegen van een hoeveelheid.";
$mod_strings['LBL_PRODUCT_FIELDS_VALIDATION'] = "Voeg productnaam en catalogusprijs toe voor prijsboeken.";
$mod_strings['LBL_QUANTITY_FIELD_VALIDATION'] = "Vul het hoeveelheidsbereik in voor lege platen.";
$mod_strings['LBL_EDITVIEW_PANEL1'] = "Prijsboekdetails";
$mod_strings['LBL_DETAILVIEW_PANEL1'] = "Prijsboekdetails";
?> 