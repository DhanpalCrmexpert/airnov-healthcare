<?php
// created: 2020-11-18 06:37:33
$mod_strings = array (
  'LBL_EMAILS_LEADS_REL' => 'Emails:Prospects',
  'LBL_LEADS_SUBPANEL_TITLE' => 'Prospects',
  'LBL_EMAILS_ACCOUNTS_REL' => 'Emails:Customers',
  'LBL_ACCOUNTS_SUBPANEL_TITLE' => 'Customers',
  'LBL_EMAILS_CASES_REL' => 'Emails:QNS',
  'LBL_CASES_SUBPANEL_TITLE' => 'QNS',
);