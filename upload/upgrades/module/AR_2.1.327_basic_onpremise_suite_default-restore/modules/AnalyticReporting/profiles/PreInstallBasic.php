<?php


abstract class PreInstallBasic
{

    protected $adb;
    protected $params = array();

    public function __construct($adb, $newInstall)
    {
        $this->adb = $adb;
        $this->params['new_install'] = $newInstall;
    }

    abstract protected function configKeys();

    public function run()
    {
        $tableName = 'advancedreports_config';

        foreach ($this->configKeys() as $key => $value) {
            $query = null;
            $found = $this->adb->getOne("SELECT COUNT(*) AS count FROM {$tableName} WHERE id = '{$key}'");

            if ($found < 1) {
                $query = "INSERT INTO {$tableName} (id, value) VALUES ('{$key}', '{$value}')";
            } else {
                $query = "UPDATE {$tableName} SET value = '{$value}' WHERE id = '{$key}'";
            }

            $this->adb->query($query);
        }

        echo "Add env. configuration.\n<br />";
    }
}