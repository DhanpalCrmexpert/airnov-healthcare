<?php
/**
 * Advanced OpenSales, Advanced, robust set of sales modules.
 * @package Advanced OpenSales for SugarCRM
 * @copyright SalesAgility Ltd http://www.salesagility.com
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU AFFERO GENERAL PUBLIC LICENSE
 * along with this program; if not, see http://www.gnu.org/licenses
 * or write to the Free Software Foundation,Inc., 51 Franklin Street,
 * Fifth Floor, Boston, MA 02110-1301  USA
 *
 * @author SalesAgility <info@salesagility.com>
 */

    if(!(ACLController::checkAccess('AOS_Invoices', 'edit', true))){
        ACLController::displayNoAccess();
        die;
    }

	require_once('modules/AOS_Quotes/AOS_Quotes.php');
	require_once('modules/AOS_Invoices/AOS_Invoices.php');
	require_once('modules/AOS_Products_Quotes/AOS_Products_Quotes.php');
	
	global $timedate, $current_user;

	$sp_Timesheet = new sp_Timesheet();
	$sp_Timesheet->retrieve($_REQUEST['record']);
	
	$selected_module = $sp_Timesheet->module;
	$related_module = $sp_Timesheet->related_module;
	
	$selected_module_bean = BeanFactory::getBean($selected_module);
	$selected_module_table_name = $selected_module_bean->table_name;
	
	$related_module_bean = BeanFactory::getBean($related_module);
	$related_module_table_name = $related_module_bean->table_name;
	
	//Setting Invoice Values
	$invoice = new AOS_Invoices();
	$invoice->name = "Invoice :: ".$sp_Timesheet->name;
	$invoice->save();
	
	//Setting invoice quote relationship
	/*require_once('modules/Relationships/Relationship.php');
	$key = Relationship::retrieve_by_modules('AOS_Quotes', 'AOS_Invoices', $GLOBALS['db']);
	if (!empty($key)) {
		$quote->load_relationship($key);
		$quote->$key->add($invoice->id);
	} */
	
	$group_invoice = new AOS_Line_Item_Groups();
	$group_invoice->parent_id = $invoice->id;
	$group_invoice->parent_type = 'AOS_Invoices';
	$group_invoice->name = 'Time Sheet Billing';
	$group_invoice->save();
	$per_hour_rate = $current_user->hourly_rate;
	//Setting Line Items
	$sql = "SELECT * FROM sp_timesheet_tracker WHERE timesheet_id = '".$sp_Timesheet->id."' AND deleted = 0 order by track_date DESC";
  	$result = $this->bean->db->query($sql);
	while ($row = $this->bean->db->fetchByAssoc($result)) {
		
		$project_task_id = $row['projecttask_id_c'];
		$project_id = $row['project_id_c'];
		$project_name = '';
		if(!empty($project_task_id )){
			$project_task = "SELECT * FROM $related_module_table_name WHERE id = '".$project_task_id."' AND deleted = 0";
			$project_task_result = $this->bean->db->query($project_task);
			while ($project_task_row = $this->bean->db->fetchByAssoc($project_task_result)) {
				if(isset($project_task_row['name'])){
					$project_name = $project_task_row['name'];
				}
				else{
					$project_name = $project_task_row['first_name']." ".$project_task_row['last_name'];
				}
			}
		}
		else if(!empty($project_id )){
			$project = "SELECT * FROM $selected_module_table_name WHERE id = '".$project_id."' AND deleted = 0";
			$project_result = $this->bean->db->query($project);
			while ($project_row = $this->bean->db->fetchByAssoc($project_result)) {
				if(isset($project_row['name'])){
					$project_name = $project_row['name'];
				}
				else{
					$project_name = $project_row['first_name']." ".$project_row['last_name'];
				}
			}
		}
		$row['id'] = '';
		$row['name'] = "Billing of ".$project_name." :: ".$row['track_date']. "  (".$row['hours']." Hours)";
		$row['product_unit_price'] = $per_hour_rate * $row['hours'];
		$row['vat_amt'] = '0.0';
		$row['product_list_price'] = $per_hour_rate * $row['hours'];
		$row['product_total_price'] = $per_hour_rate * $row['hours'];
		$row['group_id'] = $group_invoice->id;
		$row['parent_id'] = $invoice->id;
		$row['parent_type'] = 'AOS_Invoices';
		$row['currency_id'] = '-99';
		$prod_invoice = new AOS_Products_Quotes();
		$prod_invoice->populateFromRow($row);
		$prod_invoice->save();
	}
	ob_clean();
	header('Location: index.php?module=AOS_Invoices&action=EditView&record='.$invoice->id);
?>
