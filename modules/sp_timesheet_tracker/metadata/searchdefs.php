<?php
$module_name = 'sp_timesheet_tracker';
$searchdefs [$module_name] = 
array (
  'layout' => 
  array (
    'basic_search' => 
    array (
      'name' => 
      array (
        'name' => 'name',
        'default' => true,
        'width' => '10%',
      ),
      'current_user_only' => 
      array (
        'name' => 'current_user_only',
        'label' => 'LBL_CURRENT_USER_FILTER',
        'type' => 'bool',
        'default' => true,
        'width' => '10%',
      ),
    ),
    'advanced_search' => 
    array (
      'parent_name' => 
      array (
        'type' => 'parent',
        'default' => true,
        'studio' => 'visible',
        'label' => 'LBL_PARENT_NAME',
        'link' => true,
        'sortable' => false,
        'ACLTag' => 'PARENT',
        'dynamic_module' => 'PARENT_TYPE',
        'id' => 'PARENT_ID',
        'related_fields' => 
        array (
          0 => 'parent_id',
          1 => 'parent_type',
        ),
        'width' => '10%',
        'name' => 'parent_name',
      ),
      'track_date' => 
      array (
        'type' => 'date',
        'label' => 'LBL_TRACK_DATE',
        'width' => '10%',
        'default' => true,
        'name' => 'track_date',
      ),
      'hours' => 
      array (
        'type' => 'float',
        'label' => 'LBL_HOURS',
        'width' => '10%',
        'default' => true,
        'name' => 'hours',
      ),
      'track_user' => 
      array (
        'type' => 'relate',
        'studio' => 'visible',
        'label' => 'LBL_TRACK_USER',
        'id' => 'USER_ID_C',
        'link' => true,
        'width' => '10%',
        'default' => true,
        'name' => 'track_user',
      ),
      'assigned_user_id' => 
      array (
        'name' => 'assigned_user_id',
        'label' => 'LBL_ASSIGNED_TO',
        'type' => 'enum',
        'function' => 
        array (
          'name' => 'get_user_array',
          'params' => 
          array (
            0 => false,
          ),
        ),
        'default' => true,
        'width' => '10%',
      ),
    ),
  ),
  'templateMeta' => 
  array (
    'maxColumns' => '3',
    'maxColumnsBasic' => '4',
    'widths' => 
    array (
      'label' => '10',
      'field' => '30',
    ),
  ),
);
?>
