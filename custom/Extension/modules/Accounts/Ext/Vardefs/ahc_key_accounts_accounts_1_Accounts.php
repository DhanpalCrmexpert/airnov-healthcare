<?php
// created: 2020-11-03 16:17:29
$dictionary["Account"]["fields"]["ahc_key_accounts_accounts_1"] = array (
  'name' => 'ahc_key_accounts_accounts_1',
  'type' => 'link',
  'relationship' => 'ahc_key_accounts_accounts_1',
  'source' => 'non-db',
  'module' => 'ahc_Key_Accounts',
  'bean_name' => 'ahc_Key_Accounts',
  'vname' => 'LBL_AHC_KEY_ACCOUNTS_ACCOUNTS_1_FROM_AHC_KEY_ACCOUNTS_TITLE',
  'id_name' => 'ahc_key_accounts_accounts_1ahc_key_accounts_ida',
);
$dictionary["Account"]["fields"]["ahc_key_accounts_accounts_1_name"] = array (
  'name' => 'ahc_key_accounts_accounts_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_AHC_KEY_ACCOUNTS_ACCOUNTS_1_FROM_AHC_KEY_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'ahc_key_accounts_accounts_1ahc_key_accounts_ida',
  'link' => 'ahc_key_accounts_accounts_1',
  'table' => 'ahc_key_accounts',
  'module' => 'ahc_Key_Accounts',
  'rname' => 'name',
);
$dictionary["Account"]["fields"]["ahc_key_accounts_accounts_1ahc_key_accounts_ida"] = array (
  'name' => 'ahc_key_accounts_accounts_1ahc_key_accounts_ida',
  'type' => 'link',
  'relationship' => 'ahc_key_accounts_accounts_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_AHC_KEY_ACCOUNTS_ACCOUNTS_1_FROM_ACCOUNTS_TITLE',
);
