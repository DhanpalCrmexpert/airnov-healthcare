<?php
class UpdateTimesheet{
	function update_timesheet_method(&$bean, $event, $arguments){
		global $db, $current_user;
		if($bean->manual_entry){
			$current_date = (isset($bean->start_date)) ? date('Y-m-d', strtotime($bean->start_date)):date("Y-m-d");
			$assigned_user_id = $current_user->id;
			if(!empty($bean->user_id_c)){
				$assigned_user_id = $bean->user_id_c;
			}
			if(!isset($bean->sp_timesheet_id_c) || empty($bean->sp_timesheet_id_c)){
				if(isset($bean->start_date) && !empty($bean->start_date)){
					$bean->sp_timesheet_id_c = $this->getTimeSheetID($current_date,$assigned_user_id);
				}
			}
			$time_sheet_bean = BeanFactory::getBean('sp_Timesheet', $bean->sp_timesheet_id_c);
			if(isset($time_sheet_bean->id)){
				$parent_bean = BeanFactory::getBean($bean->parent_type, $bean->parent_id);
				$related_record_name = (isset($parent_bean->id)) ? $parent_bean->name : "Related Record";
				$bean->name = "Manual TimeSheet Tracker:".$related_record_name;
				$related_record_url = "<a href='index.php?module={$bean->parent_type}&action=DetailView&record={$bean->parent_id}'>{$related_record_name}</a>";
				if(isset($bean->sp_timesheet_tracker_id_c) && !empty($bean->sp_timesheet_tracker_id_c)){
					$time_tracker_id = $bean->sp_timesheet_tracker_id_c;
					$update_sql = "UPDATE sp_timesheet_tracker set hours = {$bean->hours}, related_module='{$bean->parent_type}', projecttask_id_c='{$bean->parent_id}', sp_auto_timesheet_tracker_id='{$bean->id}', description='{$bean->description}' where id = '{$time_tracker_id}'";
					$res = $db->query($update_sql, true);
					// $GLOBALS['log']->fatal("Update SQL::".$update_sql);
				}else{
					/*$check_exist_sql = "SELECT id from sp_timesheet_tracker 
										where track_date='{$current_date}' 
										AND parent_type='{$time_sheet_bean->module}'
										AND parent_id='{$bean->selected_module_id}'
										AND projecttask_id_c='{$bean->parent_id}'
										AND user_id_c='{$assigned_user_id}'
										AND timesheet_id='{$time_sheet_bean->id}'
										AND deleted=0";*/
						$check_exist_sql = "SELECT id from sp_timesheet_tracker 
										where track_date='{$current_date}' 
										AND parent_type='{$bean->parent_type}'
										AND parent_id='{$bean->parent_id}'
										AND projecttask_id_c='{$bean->parent_id}'
										AND user_id_c='{$assigned_user_id}'
										AND timesheet_id='{$time_sheet_bean->id}'
										AND deleted=0";				
					$check_exist_res = $db->query($check_exist_sql,true);
					$check_exist_row = $db->fetchByAssoc($check_exist_res);
					if(isset($check_exist_row['id']) && !empty($check_exist_row['id'])){
						$update_sql = "UPDATE sp_timesheet_tracker set hours = {$bean->hours}, description='{$bean->description}', sp_auto_timesheet_tracker_id='{$bean->id}' where id = '".$check_exist_row['id']."'";
						$res = $db->query($update_sql, true);
						$time_tracker_id = $check_exist_row['id'];
						// $GLOBALS['log']->fatal("Update SQL::".$update_sql);
					}else{
						$time_tracker_id = create_guid();
						//$insert_sql = "INSERT into sp_timesheet_tracker(id, name, date_entered, date_modified, modified_user_id, user_id_c,assigned_user_id, deleted, track_date, hours, project_id_c, timesheet_id, parent_type, parent_id, related_record_url, related_module, projecttask_id_c,sp_auto_timesheet_tracker_id, description) VALUES(\"{$time_tracker_id}\", \"TimeSheet Tracker\", \"".gmdate('Y-m-d h:i:s')."\", \"".gmdate('Y-m-d h:i:s')."\", \"{$current_user->id}\",\"{$assigned_user_id}\",\"{$assigned_user_id}\", 0, \"{$current_date}\", {$bean->hours}, \"{$bean->selected_module_id}\", \"{$time_sheet_bean->id}\", \"{$time_sheet_bean->module}\", \"{$bean->selected_module_id}\", \"{$related_record_url}\", \"{$bean->parent_type}\", \"{$bean->parent_id}\", \"{$bean->id}\", \"{$bean->description}\")";
						
						$insert_sql = "INSERT into sp_timesheet_tracker(id, name, date_entered, date_modified, modified_user_id, user_id_c,assigned_user_id, deleted, track_date, hours, project_id_c, timesheet_id, parent_type, parent_id, related_record_url, related_module, projecttask_id_c,sp_auto_timesheet_tracker_id, description) VALUES(\"{$time_tracker_id}\", \"TimeSheet Tracker\", \"".gmdate('Y-m-d h:i:s')."\", \"".gmdate('Y-m-d h:i:s')."\", \"{$current_user->id}\",\"{$assigned_user_id}\",\"{$current_user->id}\", 0, \"{$current_date}\", {$bean->hours}, \"{$bean->selected_module_id}\", \"{$time_sheet_bean->id}\", \"{$bean->parent_type}\", \"{$bean->parent_id}\", \"{$related_record_url}\", \"{$bean->parent_type}\", \"{$bean->parent_id}\", \"{$bean->id}\", \"{$bean->description}\")";
						$res = $db->query($insert_sql, true);
						// $GLOBALS['log']->fatal("Insert SQL::".$insert_sql);
					}
					$bean->sp_timesheet_tracker_id_c = $time_tracker_id;
				}
				$sql_timesheet = "UPDATE sp_timesheet set hours=(SELECT sum(hours) hours FROM sp_timesheet_tracker WHERE deleted = 0 AND user_id_c = '{$current_user->id}' AND timesheet_id = '{$time_sheet_bean->id}') where id = '{$time_sheet_bean->id}' and deleted=0";
				$res = $db->query($sql_timesheet, true);
				// $GLOBALS['log']->fatal("SQL::".$sql_timesheet);
				//$bean->processed = '1';
			}
		}
	}

	 function getTimeSheetID($current_date,$assigned_user_id){
		global $sugar_config, $current_user;
		$week_start_date = date("Y-m-d", strtotime("monday this week", strtotime($current_date)));
		$week_end_date = date("Y-m-d", strtotime("sunday this week", strtotime($current_date)));

		$selected_module = (isset($sugar_config['timesheet_module_'.$current_user->id])) ? $sugar_config['timesheet_module_'.$current_user->id] : "Accounts";
		$related_module = (isset($sugar_config['related_module_'.$current_user->id])) ? $sugar_config['related_module_'.$current_user->id] : "Cases";		
		$relationship_name = (isset($sugar_config['related_module_relationship_'.$current_user->id])) ? $sugar_config['related_module_relationship_'.$current_user->id] : "cases";

		if(!isset($sugar_config['timesheet_module_'.$current_user->id])){
			$conf = new Configurator();
			$conf->config['timesheet_module_'.$current_user->id] = $selected_module;
			$conf->config['related_module_'.$current_user->id] = $related_module;
			$conf->config['related_module_relationship_'.$current_user->id] = $related_module_relationship;
			$conf->handleOverride();
			$conf->clearCache();			
		}
		
		$filters = array(
			"module" => $selected_module,
			"related_module" => $related_module,
			"track_date_from" => $week_start_date,
			"track_date_to" => $week_end_date,
			"user_id_c" => $assigned_user_id,
		);
		

		$timesheet_obj = BeanFactory::getBean('sp_Timesheet');
		$timesheet_obj->retrieve_by_string_fields($filters);

		if(empty($timesheet_obj->id)){
			$timesheet_obj->name =  "TimeSheet: ".$week_start_date." - ".$week_end_date;
			$timesheet_obj->track_date_from =  $week_start_date;
			$timesheet_obj->track_date_to =  $week_end_date;
			$timesheet_obj->user_id_c =  $assigned_user_id;
			$timesheet_obj->hours =  0;
			$timesheet_obj->module =  $selected_module;
			$timesheet_obj->related_module =  $related_module;
			$timesheet_obj->related_module_relationship =  $relationship_name;
			$timesheet_obj->assigned_user_id =  $current_user->id;
			$timesheet_obj->save();
		}
		return $timesheet_obj->id;
	}
}

?>