<?php 
 //WARNING: The contents of this file are auto-generated


/*********************************************************************************
 * This file is part of package Price Books.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Price Books is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
/*
 * Name relate field
 * * link point at the link field defined below
 * * make sure the field name matches the pattern <bean_name>_name
 */
$dictionary['VI_Price_Books']['fields']['contact_name'] = array(
    'source'    => 'non-db',
    'name'      => 'contact_name',
    'vname'     => 'LBL_CONTACT_NAME',
    'type'      => 'relate',
    'rname'     => 'name',
    'id_name'   => 'contact_id',
    'join_name' => 'contacts',
    'link'      => 'contacts',
    'table'     => 'contacts',
    'isnull'    => 'true',
    'module'    => 'Contacts',
);

/*
 * Linking id field
 * * make sure the field name matches the pattern <bean_name>_id
 */
$dictionary['VI_Price_Books']['fields']['contact_id'] = array(
    'name'              => 'contact_id',
    'rname'             => 'id',
    'vname'             => 'LBL_CONTACT_ID',
    'type'              => 'id',
    'table'             => 'contacts',
    'isnull'            => 'true',
    'module'            => 'Contacts',
    'dbType'            => 'id',
    'reportable'        => false,
    'massupdate'        => false,
    'duplicate_merge'   => 'disabled',
);

/*
 * Relationship link
 * * see the link defined on the name field above
 * * relationship name matches the relationship defined below
 */
$dictionary['VI_Price_Books']['fields']['contacts'] = array(
    'name'          => 'contacts',
    'type'          => 'link',
    'relationship'  => 'contact_vi_price_books',
    'module'        => 'Contacts',
    'bean_name'     => 'Contact',
    'source'        => 'non-db',
    'vname'         => 'LBL_CONTACTS',
);


//Create Index
$dictionary['VI_Price_Books']['indices'][] = array(
  'name' =>'idx_contact_id',
  'type' =>'index',
  'fields'=>array('contact_id')
);



/*********************************************************************************
 * This file is part of package Price Books.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Price Books is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
      $GLOBALS['dictionary']['VI_Price_Books']['fields']['active_c'] = array (
            'name' => 'active_c',
            'vname' => 'LBL_ACTIVE',
            'type' => 'bool',
            'module' => 'VI_Price_Books',
            'default_value' => true, // true or false
            'help' => 'Bool Field Help Text',
            'comment' => 'Bool Field Comment',
            'audited' => false, // true or false
            'mass_update' => false, // true or false
            'duplicate_merge' => false, // true or false
            'reportable' => true, // true or false
            'importable' => 'true', // 'true', 'false' or 'required'
      );


/*********************************************************************************
 * This file is part of package Price Books.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Price Books is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
/*
 * Name relate field
 * * link point at the link field defined below
 * * make sure the field name matches the pattern <bean_name>_name
 */
$dictionary['VI_Price_Books']['fields']['account_name'] = array(
    'source'    => 'non-db',
    'name'      => 'account_name',
    'vname'     => 'LBL_ACCOUNT_NAME',
    'type'      => 'relate',
    'rname'     => 'name',
    'id_name'   => 'account_id',
    'join_name' => 'accounts',
    'link'      => 'accounts',
    'table'     => 'accounts',
    'isnull'    => 'true',
    'module'    => 'Accounts',
);

/*
 * Linking id field
 * * make sure the field name matches the pattern <bean_name>_id
 */
$dictionary['VI_Price_Books']['fields']['account_id'] = array(
    'name'              => 'account_id',
    'rname'             => 'id',
    'vname'             => 'LBL_ACCOUNT_ID',
    'type'              => 'id',
    'table'             => 'accounts',
    'isnull'            => 'true',
    'module'            => 'Accounts',
    'dbType'            => 'id',
    'reportable'        => false,
    'massupdate'        => false,
    'duplicate_merge'   => 'disabled',
);

/*
 * Relationship link
 * * see the link defined on the name field above
 * * relationship name matches the relationship defined below
 */
$dictionary['VI_Price_Books']['fields']['accounts'] = array(
    'name'          => 'accounts',
    'type'          => 'link',
    'relationship'  => 'account_vi_price_books',
    'module'        => 'Accounts',
    'bean_name'     => 'Account',
    'source'        => 'non-db',
    'vname'         => 'LBL_ACCOUNTS',
);


//Create Index
$dictionary['VI_Price_Books']['indices'][] = array(
  'name' =>'idx_account_id',
  'type' =>'index',
  'fields'=>array('account_id')
);


?>