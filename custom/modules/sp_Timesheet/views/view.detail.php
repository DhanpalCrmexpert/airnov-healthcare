<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once('modules/sp_Timesheet/views/view.detail.php');

class Customsp_TimesheetViewDetail extends sp_TimesheetViewDetail
{	
    function display()
    {
		global $current_user;
		$index = array_search("EDIT", $this->dv->defs['templateMeta']['form']['buttons']);
		if(file_exists){
			unlink('cache/themes/SuiteP/modules/sp_Timesheet/DetailView.tpl');
		}
		if(!is_admin($current_user) && strtolower($current_user->timesheet_lock) != 'none' && !$this->isTimesheetOfCurrentWeek()){		
			unset($this->dv->defs['templateMeta']['form']['buttons'][$index]);
		}
		parent::display();
	}
	
	function getDates($date){
	  $week = date("W",strtotime($date));
	  $year = date("Y",strtotime($date));
	  $dto = new DateTime();
	  $result['start'] = $dto->setISODate($year, $week, 1)->format('Y-m-d');
	  $result['end'] = $dto->setISODate($year, $week, 7)->format('Y-m-d');
	  return $result;
	}
	
	private function isTimesheetOfCurrentWeek($bean){
		$results = $this->getDates(date('Y-m-d'));
		$weekStart = strtotime($results['start']);
		$weekEnd = strtotime($results['end']);
		$trackDate = strtotime($this->bean->track_date_from);
		return ($weekStart <= $trackDate && $trackDate <= $weekEnd);
	}
}
