<?php
//ini_set('display_errors',1);
//error_reporting(E_ALL);
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

class sp_Timeoff_TimeCardViewEdit extends ViewEdit {

 	function __construct(){
 		parent::__construct();
 		}
			
	function getProjectTimeTracked($project_id, $start_date, $end_date, $user_id){
		global $db;
		$i = 0;
		$project_time_sheet_list = array();
		$getProjectTimeSheetQry = "Select * from sp_timesheet_tracker where deleted = 0 and project_id_c='$project_id' and track_date >= '$start_date' and track_date <= '$end_date' and user_id_c = '$user_id' ORDER BY track_date ASC";
		$resProjectTimeSheet = $db->query($getProjectTimeSheetQry);
		while($rowProjectTimeSheetId = $db->fetchByAssoc($resProjectTimeSheet))
		{
			$project_task_id = $rowProjectTimeSheetId['projecttask_id_c'];
			$track_date = $rowProjectTimeSheetId['track_date'];
			$hours = $rowProjectTimeSheetId['hours'];
			
			if(empty($project_task_id)){
				$index = "project_". $project_id ."_". $track_date;
			}
			else{
				$index = "projecttask_". $project_id. "_". $project_task_id ."_". $track_date;
			}
			$project_time_sheet_list[$index] = $hours;
		}
		return $project_time_sheet_list;
	}
	
function getProjectsList($start_date, $end_date, $user_id, $selected_module, $related_module){
		global $db, $current_user;
		
		$current_user_id = $user_id;
		$selected_module_bean = BeanFactory::getBean($selected_module);
		$module_table_name = $selected_module_bean->table_name;
		
		$status_where = "";
		if($selected_module == "Project" || $selected_module == "ProjectTask"){
			$status_where = "and status not in('Completed', 'Closed', 'On_Hold')";
		}
		
		$i = 0;
		$project_list = array();
		
		if($selected_module == "Project"){
			$getProjectQry = "Select p.id, p.name from project p where p.deleted = 0 and status not in('Completed', 'Closed', 'On_Hold') ORDER BY p.date_entered DESC";
		}
		else{
			$getProjectQry = "Select * from $module_table_name p where p.deleted = 0 and assigned_user_id = '$current_user_id' $status_where ORDER BY p.date_entered DESC";
		}
		
		$resProject= $db->query($getProjectQry);
		while($rowProjectId = $db->fetchByAssoc($resProject))
		{
			$project_list[$i]['project_id'] = $rowProjectId['id'];
			if(isset($rowProjectId['name'])){
				$project_list[$i]['project_name'] = $rowProjectId['name'];
			}
			else{
				$project_list[$i]['project_name'] = $rowProjectId['first_name']." ".$rowProjectId['last_name'];
			}
			$timeSheetResult = $this->getProjectTimeTracked($rowProjectId['id'], $start_date, $end_date, $user_id);
			$project_list[$i]['project_tasks_list'] = $this->getProjectTasksList($rowProjectId['id'], $selected_module, $related_module, $current_user_id);
			$project_list[$i]['project_time_sheet_tracker'] = $timeSheetResult;
			$i++;
		}
		return $project_list;
	}
		function display()
    {
		global $current_user, $sugar_config, $app_list_strings;
		
		$current_user_id = $current_user->id;
		$user_id = $current_user->id;
		$user_name = $current_user->name;
		
		$total_hours = 0;
		$week_start_date = date("Y-m-d");
		$result = $this->getDates($week_start_date);
		$dates = $this->getWeekDates($result['start'], $result['end']);
		
		
		//////////////////
		
		$siteUrl = $sugar_config['site_url'];
		require_once('modules/TimesheetNinja/license/OutfittersLicense.php');
		$validate_license = OutfittersLicense::isValid('TimesheetNinja');
		if($validate_license !== true) {
		if(is_admin($current_user)) {
			SugarApplication::appendErrorMessage('Timesheet is no longer active due to the following reason: '.$validate_license.' Users will have limited to no access until the issue has been addressed.');
		}
		echo '<p class="error">Timesheet is no longer active Please renew your subscription or check your license configuration.Will be redirect in seconds.....</p>';
		header("refresh:2;url=$siteUrl");
		//functionality may be altered here in response to the key failing to validate
		}
		echo "</br>";
		
		
		//////////////////
		
		
		
		if(!empty($this->bean->id)){
			$timeSheetDates = $this->getTimesheetDates($this->bean->id);
			$week_start_date = $timeSheetDates['track_date_from'];
			$result['start'] = date("Y-m-d",strtotime($timeSheetDates['track_date_from']));
			$result['end'] = date("Y-m-d",strtotime($timeSheetDates['track_date_to']));
			$dates = $this->getWeekDates($result['start'], $result['end']);
		}
		
		if(!empty($_REQUEST['week_start_date'])){
			$week_start_date = $_REQUEST['week_start_date'];
			$result = $this->getDates($week_start_date);
			$dates = $this->getWeekDates($result['start'], $result['end']);
		}
		
		$next_week_start_date = $this->getNextPreviousDate($week_start_date, "+7 day");
		
		$previous_week_start_date = $this->getNextPreviousDate($week_start_date, "-7 day");
		
		$json_dates = json_encode($dates);
		
	
		$timecard_list = $this->getTimeCardList($result['start'], $result['end'], $user_id);
		
		$projects = $projects_list['project_list'];
		$custom_projects = $projects_list['custom_array'];
		$temp_array = array();
		
		$projects_new = $this->getProjectsList($result['start'], $result['end'], $user_id, $selected_module, $related_module);

		for($i=0; $i<count($projects); $i++){
				global $db;
				$select_weekly_query = "Select * from sp_timesheet_tracker where deleted = 0 and project_id_c='".$projects_new[$i]['project_id']."' and track_date >= '".$result['start']."' and track_date <= '".$result['end']."' and user_id_c = '". $user_id ."' ORDER BY track_date ASC";
			$resProjectTimeSheet = $db->query($select_weekly_query);
			while($rowProjectTimeSheetId = $db->fetchByAssoc($resProjectTimeSheet))
			{
				
				$weekly_set=$rowProjectTimeSheetId['name'];
				
				break;
			}
		}
		if($weekly_set=="Weekly") {$weekly ="selected";
		$week_total=0;
		}
		else{
			$day ="selected";
		}
		
		if(!empty($this->bean->id)){
			$selected_module = $this->bean->module;
			$related_module = $this->bean->related_module;
			$user_id = $this->bean->user_id_c;
			$current_user_id = $this->bean->user_id_c;
		}
		else{
			$selected_module = $sugar_config['timesheet_module_'.$current_user_id];
			$related_module = $sugar_config['related_module_'.$current_user_id];
		}
		
		$selected_module_lable = $app_list_strings['moduleList'][$selected_module];
		$related_module_lable = $app_list_strings['moduleList'][$related_module];
		
		echo "<input type='hidden' id='json_dates' name='json_dates' value='".$json_dates."' />";
		
		$title_header = '<strong style="color:red;">'.$user_name.': '.date('d F',strtotime($result['start'])).' -- '.date('d F',strtotime($result['end'])).'</strong>';
	//	<script src="modules/sp_Timesheet/js/timesheet.js"></script>
		$html = '
		
<script src="modules/sp_Timeoff_TimeCard/js/timesheet_card.js"></script>
		
		<form action="index.php" method="POST" name="EditView" id="EditView">
		<table id="topLevelButtonsRow" width="100%" cellpadding="0" cellspacing="0" border="0" class="dcQuickEdit" >
		<tbody><tr>
		<td class="buttons">
		<input type="hidden" id="week_start" name="week_start" value="'.$result['start'].'" />
		<input type="hidden" id="week_end" name="week_end" value="'.$result['end'].'" />
		<input type="hidden" id="current_user_id" name="current_user_id" value="'.$current_user_id.'" />
		<input type="hidden" id="next_week_date" name="next_week_date" value="'.$next_week_start_date.'" />
		<input type="hidden" id="previous_week_date" name="previous_week_date" value="'.$previous_week_start_date.'" />
		<input type="hidden" name="module" value="sp_Timeoff_TimeCard">
		<input type="hidden" name="record" value="'.$this->bean->id.'">
		<input type="hidden" name="selected_module" value="'.$this->bean->module.'">
		
		<input type="hidden" name="isDuplicate" value="false">
		<input type="hidden" name="action">
		<input type="hidden" name="return_module" value="">
		<input type="hidden" name="return_action" value="">
		<input type="hidden" name="return_id" value="">
		<input type="hidden" name="module_tab"> 
		<input type="hidden" name="contact_role">
		<input type="hidden" name="offset" value="1">
		<!-- to be used for id for buttons with custom code in def files-->
		
		<div class="action_buttons">
		<input title="Save" accesskey="a" class="button primary" onclick="var _form = document.getElementById(\'EditView\'); _form.action.value=\'Save\'; if(check_form(\'EditView\'))SUGAR.ajaxUI.submitForm(_form);return false;" type="submit" name="button" value="Save" id="SAVE_HEADER">  
		
		<input title="Cancel [Alt+l]" accesskey="l" class="button" onclick="SUGAR.ajaxUI.loadContent(\'index.php?action=index&amp;module=sp_Timeoff_TimeCard\'); return false;" type="button" name="button" value="Cancel" id="CANCEL_HEADER">  
		<input title="Last Week" accesskey="a" class="button" type="button" name="previous_week" value="<< last week" id="previous_week"/>
		
		
		'.$title_header.'
		
		<input title="Next Week" accesskey="a" class="button" type="button" name="next_button" value="next week >>" id="next_week"/>
		
		<input disabled type="text" name="date_field" id="date_field" value="" />

		<img border="0" src="custom/themes/default/images/jscalendar.gif" alt="Enter Date" id="date_triggere" align="absmiddle" />
		
		<input title="Search" accesskey="a" class="button" type="button" name="custom_week" value="Jump" id="custom_week"/>';
		
		/*if($selected_module_lable=="Projects")
		{
			 'herrre';
		$html.='<select name="calculate_in" id="calculate_in" onchange="calculation_in(2);">
		<option  value="Day_Wise" '.$day .'>Day Wise</option>
		<option  value="Weekly" '. $weekly .' selected>Weekly</option>
		</select>';
		
		}
		else
		{
			
			$html.='<input type="hidden" name="calculate_in" value="Day_Wise">';
		}*/
		$html.='
		<div class="clear"></div></div>
		</td>
		
		
		<td align="right">
		</td>
		</tr>
		</tbody></table>
		<script language="javascript">
			var _form_id = "EditView";
			SUGAR.util.doWhen(function(){
				_form_id = (_form_id == "") ? "EditView" : _form_id;
				return document.getElementById(_form_id) != null;
			}, SUGAR.themes.actionMenu);
			
			$(document).ready(function(){
				calculation_in(1);
			});
			
			function calculation_in(n)
			{
				var a;
				var cal=$("#calculate_in").val();
				if(n==2){
					
					if(cal=="Weekly")
						a= confirm("If you continue, the daily data will erase !");
					else
						a= confirm("If you continue, the weekly data will erase !");
					if(a==true)
						$("div.clearprojecttotal").html("");
					
				}
				else{
					a=true;
				}	
				if(a==true){
					
					if(cal=="Weekly")
					{
					  $("input[type=text]").each(function() {
					  if( $(this).hasClass("day_wies"))
					  {
						 $(this).val(""); 
						 $(this).prop( "disabled", true );
						 
					  }
					  if( $(this).hasClass("gettotal"))
					  {
						$(this).show();
						if(n==2){	
						$(this).val(""); 
						}
					
					  } 
					   
					});
					$("div.cleartotal").html("");
					
					if(n==2){	
						$("#total_hours").html("");
						$(".ttotal").css({display:"none"});
					}
					}
					else{
						$("input[type=text]").each(function() {
					  if( $(this).hasClass("day_wies"))
					  {
						 $(this).prop( "disabled", false );
					  }
					  if( $(this).hasClass("gettotal"))
					  {
						$(this).hide();
						$(this).val(""); 
						
					  } 
					   
					});
					
					$(".ttotal").css({display:"block"});
					calculateProjectHours();
					}
				}
				else{
				if(cal=="Weekly")
					
					$("#calculate_in option[value=\"Day_Wise\"]").prop("selected", true);
				else
				
					$("#calculate_in option[value=\"Weekly\"]").prop("selected", true);
					
				}
				
			}
			function calculateProjectHoursWeekly()
			{
				var total=0;
				var proj_total;
				var hr=0;
				$("input[type=text]").each(function() {
					if( $(this).hasClass("gettotal"))
					  {
						
						textid=$(this).attr("id");
						if(textid=="ptotal")
						{
							textname=$(this).attr("name");
							textname = textname.replace("sumproject", "projecthours");
							
							proj_total=0;
						}
					
						hr=parseInt($(this).val());
						if(isNaN(hr))
							hr=0;
							proj_total+=parseInt(hr);
							total+=parseInt(hr); 
							
							$("#"+textname).html(proj_total);
							
						
					  } 
				});
				$("#total_hours").html(total);
				
				
			}
		</script>';
 // background-color: #ccc;
		$weekHtml = $html;
		$weekHtml.='<style>
		table#mytab th  {
   border: 1px solid black;
  }
  table#mytab tr td {
   border: 1px solid black;
   padding:3px;
  }
  table#mytab th{
	 background-color: #3C8DBC; 
	 height:30px;
  }
   table#mytab tr{
	 background-color: #F7F7F7; 
  }
  </style>'
  
  ;
?> 
<script language='javascript'>
  $(document).ready(function(){
	  $('.rowdeleteclass').click(function(){
		   var id = $(this).attr('id');
		   // alert('hello'+id);
		if (confirm('Are you sure ?')) {
       
	SUGAR.ajaxUI.showLoadingPanel();
	$.ajax({
		  url: "index.php?module=sp_Timeoff_TimeCard&action=delete_timecard_row",
		  type: "get", //send it through get method
		  data: { 
			 UserID: id, 
		   
		  },
		  success: function(response) {
			  SUGAR.ajaxUI.hideLoadingPanel();
			//Do Something
			alert(response);
			 location.reload();
		  },
		 
		});
		 
		}				
                   
	  });
  });
  </script>
  <?php
		$weekHtml .= "<table id='mytab' style='margin-top:15px;'><thead ><tr><th>Timecard Type</th>";
		
		$html .= '<table id="myTableDates" border=1 style="margin-left: 125px;">';
		$html .= '<tr>';
		$html .= '<td style="width: 30px;"></td>';
		for($k=0; $k<7; $k++){
			$quoted_date = $dates[$k];
			$quoted_date = "'$quoted_date'";
			// $html .= '<td style="width: 100px;">'.date("D m/d",strtotime($dates[$k])).'</td>';
			$html .= '<td style="width: 15%;"><a id="cstm_date_'.$k.'"  href="#" onclick="showHideTable('.$quoted_date.', '.$k.');">'.date("D m/d",strtotime($dates[$k])).'</a></td>';
			// $html .= '<td style="width: 30px;"></td>';
			$weekHtml .= '<th style="width: 75px;">'.date("D m/d",strtotime($dates[$k])).'</th>';
		}
		$weekHtml .= "<th>Delete</th></tr>";
		$html .= "</tr></table>";

		for($kk=0; $kk<7; $kk++){
			$html .= '<table id="mytable_'.$dates[$kk].'" style="border-collapse: separate;  border-spacing: 0 1em;">';
			
			$html .= '<tr>
			<td style="width: 275px;"></td>
			<td><b>Hours</b></td><td style="width: 30px;"></td>
			<td><b>Description</b></td><td style="width: 30px;"></td>
			</tr>
			';
			
			$found = true;
			$per_project_sum = 0;
			$total_items = 0;
			
			
			$html .= "</table>";
	    }
		$html .= "</form>";

$pos=0;
		for($i=0; $i<7; $i++){


			$weekHtml .='<tr id="saqib_'.$i.'">';
			
			
			$Bereavement=$Jury_Duty=$Vacation=$Sick=$Personal=$Unpaid_Time_Off=$Other="";
			
			if($timecard_list['tracker'][$i]['name']=='Bereavement')
				$Bereavement="Selected";
			else if($timecard_list['tracker'][$i]['name']=='Jury_Duty')
				$Jury_Duty="Selected";
			else if($timecard_list['tracker'][$i]['name']=='Vacation')
				$Vacation="Selected";
			else if($timecard_list['tracker'][$i]['name']=='Sick')
				$Sick="Selected";
			else if($timecard_list['tracker'][$i]['name']=='Personal')
				$Personal="Selected";
			else if($timecard_list['tracker'][$i]['name']=='Unpaid_Time_Off')
				$Unpaid_Time_Off="Selected";
			else if($timecard_list['tracker'][$i]['name']=='Other')
				$Other="Selected";
			
			$weekHtml.= "<td style='width: 275px;padding-bottom: 0em;text-align:center;'>
			<input type='hidden' name='timecard_tracker_id[]' id='' value='".$timecard_list['tracker'][$i]['tracker_id']."' />
			<select name='timecard_type[$i]' id='timecard_type".$i."'>
			<option ></option>
			<option value='Bereavement' ".$Bereavement.">Bereavement(Enter in Hours, Max 5 days)</option>
			<option value='Jury_Duty' ".$Jury_Duty." >Jury Duty (Enter in Hours,  )</option>
			<option value='Vacation' ".$Vacation.">Vacation (Enter in Hours )</option>
			<option value='Sick' ".$Sick.">Sick</option>
			<option value='Personal' ".$Personal.">Personal</option>
			<option value='Unpaid_Time_Off' ".$Unpaid_Time_Off.">Unpaid Time Off (Only Acquired and Newly Hired Employees)</option>
			<option value='Other' ".$Other.">Other</option></td>";
			$project_total = 0.0;
			// $track_count=0;
			for($kk=0; $kk<7; $kk++){
				
				$field_name = 'time_'.$i.'_'.$dates[$kk].'_'.$kk;
				
					
$kt_count=1	;		
				for($kt=0; $kt<$kt_count; $kt++){
					//$class_name = 'class_'.$project_id;
					
				
					 $class_name = 'class_'.$kt;
					 
					 $val='';
				     $track_date='track_date'.$kk;
					 $hours='hours'.$kk;
					 $val= $timecard_list['tracker'][$i][$hours];
					
			   //here change
					$weekHtml .= '<td style="text-align:center;"><input class="'.$class_name.' projects_hours" type="text" name="'.$field_name.'[]" id="'.$field_name.'" value="'.$val.'" style="width: 50px; height: 30px" /> </td>';
					
					$weekHtml .= '<input type="hidden" name="'.$field_name.'[]" id="date_'.$field_name.'" value="'.$dates[$kk].'" />';
					$weekHtml .= '<input type="hidden" name="'.$field_name.'[]" id="date_'.$field_name.'" value="'.$dates[$kk].'" />';
					//$project_total += $projects[$i]['project_time_sheet_tracker'][$field_name][$kt]["hours"];
				}
			}
			if(!empty($timecard_list['tracker'][$i]['tracker_id']))
			$project_total="<a href='#' id='".$timecard_list['tracker'][$i]['tracker_id']."'  class='rowdeleteclass'>Delete</a>";
			else
				$project_total='';
			
			$weekHtml .= '<td style="width: 70px; text-align:center;"><span class="'.$class_name.'_total_hours total_hours" style="width: 50px; height: 30px" id="'.$field_name.'_total_hours" >'.$project_total.'</span> </td>';
			$weekHtml .='</tr>';
			
		}
		foreach ($temp_array as $project) {
			$total_items++;
			$weekHtml .= "<tr>";
			
			
			$project_total = 0.0;
			$class_name = 'pt_class_';
			foreach ($dates as $date) {
				$field_name = 'customproject_'.$date;
				
					$weekHtml .= '<td><input class="'.$class_name.' projects_hours" type="text" name="'.$field_name.'[]" id="'.$field_name.'" value="" style="width: 50px; height: 30px" /> </td>';
			
				$weekHtml.='<input type="hidden" name="'.$field_name.'[]" id="date_'.$field_name.'" value="'.$date.'"/>';
				$weekHtml.='<input type="hidden" name="'.$field_name.'[]" id="date_'.$field_name.'" value="'.$date.'"/>';
			}
			$weekHtml .= '<td><span class="'.$class_name.'_total_hours total_hours" style="width: 50px; height: 30px" id="'.$field_name.'_total_hours" >'.$project_total.'</span> </td>';
			
			$weekHtml .= "</tr>";
		}
		$weekHtml .= '</table></form>';

echo $weekHtml;
	
		
	}
	function getTimeCardList($start_date, $end_date, $user_id)
	{
		global $db, $current_user;
		$current_user_id = $user_id;
		$sql_get="SELECT * FROM   sp_timeoff_timecard  where track_date_from ='".$start_date."'  AND  track_date_to ='".$end_date."' AND assigned_user_id='".$user_id."' and deleted = '0'";
		$rstimecard= $db->query($sql_get);
		$timecard_list=array();
		$j=0;
		while($rowTimecard = $db->fetchByAssoc($rstimecard))
		{
	
		$sql_timecard_tracker="SELECT  * FROM  sp_timecard_tracker WHERE timecard_id='".$rowTimecard['id']."' AND  track_date0=  '".$rowTimecard['track_date_from']."'  AND  track_date6=  '".$rowTimecard['track_date_to']."'";
		$rs_timecard_tracker= $db->query($sql_timecard_tracker);
		$i=0;
		$timecard_list['timecard_id']=$rowTimecard['id'];
		
		$timecard_tracker=array();
		
		
		 while($row_timecard_tracker = $db->fetchByAssoc($rs_timecard_tracker))
		 {
				$timecard_tracker[$i]['name']=$row_timecard_tracker['name'];
				$timecard_tracker[$i]['tracker_id']=$row_timecard_tracker['id'];
				$timecard_tracker[$i]['position']=$row_timecard_tracker['position'];
				
				for($n=0;$n<7;$n++)
				{
				 	$track_date='track_date'.$n;
					$hours='hours'.$n;
					$timecard_tracker[$i][$track_date]=$row_timecard_tracker[$track_date];
					$timecard_tracker[$i][$hours]=$row_timecard_tracker[$hours];
				}	
				
				$i++;
		 }
		 $timecard_list['tracker']=$timecard_tracker;
		 $j++;
	}
		
		 return $timecard_list;
	
		
	}
	
	
	
	
	public function validateStatus($related_module_record, $related_module){
		if($related_module == "Project" || $related_module == "ProjectTask")
		{
			if($related_module_record->status == "Completed" || $related_module_record->status == "Closed" || $related_module_record->status == "On_Hold")
			{
				return false;
			}
		}
		return true;
	}
	
	function getDates($date){
	  $week = date("W",strtotime($date));
	  $year = date("Y",strtotime($date));
	  $dto = new DateTime();
	  $result['start'] = $dto->setISODate($year, $week, 1)->format('Y-m-d');
	  $result['end'] = $dto->setISODate($year, $week, 7)->format('Y-m-d');
	  return $result;
	}
	
	public function getWeekDates($start_date, $end_date){
		$start_timestamp = strtotime($start_date);
		$end_timestamp   = strtotime($end_date);
		$dates = array();
		while ($start_timestamp <= $end_timestamp) {
			$dates[] = date('Y-m-d', $start_timestamp);
			$start_timestamp = strtotime('+1 day', $start_timestamp);
		}
		return $dates;
	}
	
	public function getNextPreviousDate($date, $how_much_days){
		$week_start_date = strtotime($date);
		$week_start_date = strtotime($how_much_days, $week_start_date);
		return $week_start_date = date('Y-m-d', $week_start_date);
	}
	
	public function checkIfTimeAutoTracked($timesheet_id, $project_id, $project_task_id, $date, $current_user_id){
		global $db, $timedate;
		$getAutoTimeSheetQry = "Select * from sp_auto_timesheet_time_tracker where deleted = 0 and processed = 0 and sp_timesheet_id_c = '$timesheet_id' and selected_module_id = '$project_id' and parent_id = '$project_task_id' and user_id_c = '$current_user_id'";
		$resAutoTimeSheet = $db->query($getAutoTimeSheetQry);
		while($rowAutoTimeSheetId = $db->fetchByAssoc($resAutoTimeSheet))
		{
			$start_date = $timedate->to_display_date($rowAutoTimeSheetId['start_date']);
			$start_date = date('Y-m-d', strtotime($start_date));
			
			if($start_date == $date){
				return true;
			}
		}
		return false;
	}
	
	public function clean($string) {
		$string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

		return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
	}
	
	public function getTimesheetDates($timesheet_id){
		global $db;
		
		$resultTimesheet = array();
		$query = "Select * from sp_timeoff_timecard t where t.deleted = 0 and t.id = '$timesheet_id'";
		
		$result= $db->query($query);
		while($row = $db->fetchByAssoc($result))
		{
			$resultTimesheet["track_date_from"] = $row["track_date_from"];	
			$resultTimesheet["track_date_to"] = $row["track_date_to"];	
		}
		return $resultTimesheet;
	}
	
	public function getReleatedModule($row_pt){
		global $db;
		
		$related_record_url = $row_pt["related_record_url"];
		$projecttask_id_c = $row_pt["projecttask_id_c"];
		
		if($related_record_url == "No Related Record" || empty($projecttask_id_c)){
			return 'true';
		}
		
		$related_record_arr = explode("?", $related_record_url);
		$related_record_arr2 = explode("&", $related_record_arr[1]);
		$related_record_arr3 = explode("=", $related_record_arr2[0]);
		
		return $related_module = $related_record_arr3[1];
	}
}
?>