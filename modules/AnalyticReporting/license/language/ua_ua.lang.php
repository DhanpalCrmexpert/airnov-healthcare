﻿<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
$license_strings = array (
'LBL_STEPS_TO_LOCATE_KEY_TITLE' => 'Щоб знайти ключ',
'LBL_STEPS_TO_LOCATE_KEY_1' => '1. Введіть логін і пароль, щоб',
'LBL_STEPS_TO_LOCATE_KEY_2' => '2. До Account->Purchases',
'LBL_STEPS_TO_LOCATE_KEY_3' => '3. Знайдіть ключ на покупку цього доповнення',
'LBL_STEPS_TO_LOCATE_KEY_4' => '4. Вставити у вікні License Key нижче',
'LBL_STEPS_TO_LOCATE_KEY_5' => '5. натисніть на "Validate"',
'LBL_LICENSE_KEY' => 'ліцензійний ключ',
'LBL_CURRENT_USERS' => 'Поточний Граф Користувач',
'LBL_LICENSED_USERS' => 'Ліцензія Граф Користувач',
'LBL_VALIDATE_LABEL' => 'стверджувати',
'LBL_VALIDATED_LABEL' => 'затверджені',
);
