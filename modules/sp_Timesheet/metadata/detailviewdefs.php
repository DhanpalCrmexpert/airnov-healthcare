<?php
$module_name = 'sp_Timesheet';
$viewdefs [$module_name] = 
array (
  'DetailView' => 
  array (
    'templateMeta' => 
    array (
      'form' => 
      array (
        'buttons' => 
        array (
          0 => 'EDIT',
          1 => 'DUPLICATE',
          2 => 'DELETE',
          3 => 'FIND_DUPLICATES',
		  4 => 
          array (
            'customCode' => '<input type="submit" class="button" onClick="this.form.action.value=\'converToInvoice\';" value="{$MOD.LBL_CONVERT_TO_INVOICE}">',
            'sugar_html' => 
            array (
              'type' => 'submit',
              'value' => '{$MOD.LBL_CONVERT_TO_INVOICE}',
              'htmlOptions' => 
              array (
                'class' => 'button',
                'id' => 'convert_to_invoice_button',
                'title' => '{$MOD.LBL_CONVERT_TO_INVOICE}',
                'onclick' => 'this.form.action.value=\'converToInvoice\';',
                'name' => 'Convert to Invoice',
              ),
            ),
          ),
		  5 => 
          array (
            'customCode' => '<input type="submit" class="button" onClick="this.form.action.value=\'export_pdf\';" value="{$MOD.LBL_EXPORT_PDF}">',
            'sugar_html' => 
            array (
              'type' => 'submit',
              'value' => '{$MOD.LBL_EXPORT_PDF}',
              'htmlOptions' => 
              array (
                'class' => 'button',
                'id' => 'export_pdf_button',
                'title' => '{$MOD.LBL_EXPORT_PDF}',
                'onclick' => 'this.form.action.value=\'export_pdf\';',
                'name' => 'Convert to Timesheet',
              ),
            ),
          ),
		  6 => 
          array (
            'customCode' => '<input type="submit" class="button" onClick="this.form.action.value=\'email_pdf\';" value="{$MOD.LBL_EMAIL_PDF}">',
            'sugar_html' => 
            array (
              'type' => 'submit',
              'value' => '{$MOD.LBL_EMAIL_PDF}',
              'htmlOptions' => 
              array (
                'class' => 'button',
                'id' => 'email_pdf_button',
                'title' => '{$MOD.LBL_EMAIL_PDF}',
                'onclick' => 'this.form.action.value=\'email_pdf\';',
                'name' => 'Email Timesheet',
              ),
            ),
          ),
        ),
      ),
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 'name',
        ),
		 1 => 
        array (
          0 => 
          array (
            'name' => 'module',
            'label' => 'LBL_MODULE',
          ),
          1 => 
          array (
            'name' => 'related_module',
            'label' => 'LBL_RELATED_MODULE',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'track_date_from',
            'label' => 'LBL_TRACK_DATE_FROM',
          ),
          1 => 
          array (
            'name' => 'track_date_to',
            'label' => 'LBL_TRACK_DATE_TO',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'track_user',
            'studio' => 'visible',
            'label' => 'LBL_TRACK_USER',
          ),
          1 => 
		  array (
            'name' => 'hours',
            'studio' => 'visible',
            'label' => 'LBL_HOURS',
          ),
        ),
        4 => 
        array (
          0 => 'date_entered',
          1 => 'date_modified',
        ),
      ),
    ),
  ),
);
?>
