<?php
 // created: 2016-08-15 22:08:49
$dictionary['Project']['fields']['total_hours_pt'] = array(
	  'labelValue' => 'Total Hours',
      'required' => false,
      'name' => 'total_hours_pt',
      'vname' => 'LBL_TOTAL_HOURS_PT',
      'type' => 'decimal',
      'massupdate' => '0',
      'default' => '',
      'no_default' => false,
      'comments' => '',
      'help' => '',
      'importable' => 'true',
      'duplicate_merge' => 'disabled',
      'duplicate_merge_dom_value' => '0',
      'audited' => false,
      'reportable' => true,
      'unified_search' => false,
      'merge_filter' => 'disabled',
      'len' => '18',
      'size' => '20',
      'enable_range_search' => false,
      'precision' => '2',
);

 ?>