[
  {
    "FIPS": "AA",
    "ISO 3": "ABW",
    "ISO 2": "AW"
  },
  {
    "FIPS": "AC",
    "ISO 3": "ATG",
    "ISO 2": "AG"
  },
  {
    "FIPS": "AE",
    "ISO 3": "ARE",
    "ISO 2": "AE"
  },
  {
    "FIPS": "AF",
    "ISO 3": "AFG",
    "ISO 2": "AF"
  },
  {
    "FIPS": "AG",
    "ISO 3": "DZA",
    "ISO 2": "DZ"
  },
  {
    "FIPS": "AJ",
    "ISO 3": "AZE",
    "ISO 2": "AZ"
  },
  {
    "FIPS": "AL",
    "ISO 3": "ALB",
    "ISO 2": "AL"
  },
  {
    "FIPS": "AM",
    "ISO 3": "ARM",
    "ISO 2": "AM"
  },
  {
    "FIPS": "AN",
    "ISO 3": "AND",
    "ISO 2": "AD"
  },
  {
    "FIPS": "AO",
    "ISO 3": "AGO",
    "ISO 2": "AO"
  },
  {
    "FIPS": "AQ",
    "ISO 3": "ASM",
    "ISO 2": "AS"
  },
  {
    "FIPS": "AR",
    "ISO 3": "ARG",
    "ISO 2": "AR"
  },
  {
    "FIPS": "AS",
    "ISO 3": "AUS",
    "ISO 2": "AU"
  },
  {
    "FIPS": "AU",
    "ISO 3": "AUT",
    "ISO 2": "AT"
  },
  {
    "FIPS": "AV",
    "ISO 3": "AIA",
    "ISO 2": "AI"
  },
  {
    "FIPS": "AY",
    "ISO 3": "ATA",
    "ISO 2": "AQ"
  },
  {
    "FIPS": "BA",
    "ISO 3": "BHR",
    "ISO 2": "BH"
  },
  {
    "FIPS": "BB",
    "ISO 3": "BRB",
    "ISO 2": "BB"
  },
  {
    "FIPS": "BC",
    "ISO 3": "BWA",
    "ISO 2": "BW"
  },
  {
    "FIPS": "BD",
    "ISO 3": "BMU",
    "ISO 2": "BM"
  },
  {
    "FIPS": "BE",
    "ISO 3": "BEL",
    "ISO 2": "BE"
  },
  {
    "FIPS": "BF",
    "ISO 3": "BHS",
    "ISO 2": "BS"
  },
  {
    "FIPS": "BG",
    "ISO 3": "BGD",
    "ISO 2": "BD"
  },
  {
    "FIPS": "BH",
    "ISO 3": "BLZ",
    "ISO 2": "BZ"
  },
  {
    "FIPS": "BK",
    "ISO 3": "BIH",
    "ISO 2": "BA"
  },
  {
    "FIPS": "BL",
    "ISO 3": "BOL",
    "ISO 2": "BO"
  },
  {
    "FIPS": "BM",
    "ISO 3": "MMR",
    "ISO 2": "MM"
  },
  {
    "FIPS": "BN",
    "ISO 3": "BEN",
    "ISO 2": "BJ"
  },
  {
    "FIPS": "BO",
    "ISO 3": "BLR",
    "ISO 2": "BY"
  },
  {
    "FIPS": "BP",
    "ISO 3": "SLB",
    "ISO 2": "SB"
  },
  {
    "FIPS": "BR",
    "ISO 3": "BRA",
    "ISO 2": "BR"
  },
  {
    "FIPS": "BT",
    "ISO 3": "BTN",
    "ISO 2": "BT"
  },
  {
    "FIPS": "BU",
    "ISO 3": "BGR",
    "ISO 2": "BG"
  },
  {
    "FIPS": "BV",
    "ISO 3": "BVT",
    "ISO 2": "BV"
  },
  {
    "FIPS": "BX",
    "ISO 3": "BRN",
    "ISO 2": "BN"
  },
  {
    "FIPS": "BY",
    "ISO 3": "BDI",
    "ISO 2": "BI"
  },
  {
    "FIPS": "CA",
    "ISO 3": "CAN",
    "ISO 2": "CA"
  },
  {
    "FIPS": "CB",
    "ISO 3": "KHM",
    "ISO 2": "KH"
  },
  {
    "FIPS": "CD",
    "ISO 3": "TCD",
    "ISO 2": "TD"
  },
  {
    "FIPS": "CE",
    "ISO 3": "LKA",
    "ISO 2": "LK"
  },
  {
    "FIPS": "CF",
    "ISO 3": "COG",
    "ISO 2": "CG"
  },
  {
    "FIPS": "CG",
    "ISO 3": "COD",
    "ISO 2": "CD"
  },
  {
    "FIPS": "CH",
    "ISO 3": "CHN",
    "ISO 2": "CN"
  },
  {
    "FIPS": "CI",
    "ISO 3": "CHL",
    "ISO 2": "CL"
  },
  {
    "FIPS": "CJ",
    "ISO 3": "CYM",
    "ISO 2": "KY"
  },
  {
    "FIPS": "CK",
    "ISO 3": "CCK",
    "ISO 2": "CC"
  },
  {
    "FIPS": "CM",
    "ISO 3": "CMR",
    "ISO 2": "CM"
  },
  {
    "FIPS": "CN",
    "ISO 3": "COM",
    "ISO 2": "KM"
  },
  {
    "FIPS": "CO",
    "ISO 3": "COL",
    "ISO 2": "CO"
  },
  {
    "FIPS": "CQ",
    "ISO 3": "MNP",
    "ISO 2": "MP"
  },
  {
    "FIPS": "CS",
    "ISO 3": "CRI",
    "ISO 2": "CR"
  },
  {
    "FIPS": "CT",
    "ISO 3": "CAF",
    "ISO 2": "CF"
  },
  {
    "FIPS": "CU",
    "ISO 3": "CUB",
    "ISO 2": "CU"
  },
  {
    "FIPS": "CV",
    "ISO 3": "CPV",
    "ISO 2": "CV"
  },
  {
    "FIPS": "CW",
    "ISO 3": "COK",
    "ISO 2": "CK"
  },
  {
    "FIPS": "CY",
    "ISO 3": "CYP",
    "ISO 2": "CY"
  },
  {
    "FIPS": "DA",
    "ISO 3": "DNK",
    "ISO 2": "DK"
  },
  {
    "FIPS": "DJ",
    "ISO 3": "DJI",
    "ISO 2": "DJ"
  },
  {
    "FIPS": "DO",
    "ISO 3": "DMA",
    "ISO 2": "DM"
  },
  {
    "FIPS": "DR",
    "ISO 3": "DOM",
    "ISO 2": "DO"
  },
  {
    "FIPS": "EC",
    "ISO 3": "ECU",
    "ISO 2": "EC"
  },
  {
    "FIPS": "EG",
    "ISO 3": "EGY",
    "ISO 2": "EG"
  },
  {
    "FIPS": "EI",
    "ISO 3": "IRL",
    "ISO 2": "IE"
  },
  {
    "FIPS": "EK",
    "ISO 3": "GNQ",
    "ISO 2": "GQ"
  },
  {
    "FIPS": "EN",
    "ISO 3": "EST",
    "ISO 2": "EE"
  },
  {
    "FIPS": "ER",
    "ISO 3": "ERI",
    "ISO 2": "ER"
  },
  {
    "FIPS": "ES",
    "ISO 3": "SLV",
    "ISO 2": "SV"
  },
  {
    "FIPS": "ET",
    "ISO 3": "ETH",
    "ISO 2": "ET"
  },
  {
    "FIPS": "EZ",
    "ISO 3": "CZE",
    "ISO 2": "CZ"
  },
  {
    "FIPS": "FG",
    "ISO 3": "GUF",
    "ISO 2": "GF"
  },
  {
    "FIPS": "FI",
    "ISO 3": "FIN",
    "ISO 2": "FI"
  },
  {
    "FIPS": "FJ",
    "ISO 3": "FJI",
    "ISO 2": "FJ"
  },
  {
    "FIPS": "FK",
    "ISO 3": "FLK",
    "ISO 2": "FK"
  },
  {
    "FIPS": "FM",
    "ISO 3": "FSM",
    "ISO 2": "FM"
  },
  {
    "FIPS": "FO",
    "ISO 3": "FRO",
    "ISO 2": "FO"
  },
  {
    "FIPS": "FP",
    "ISO 3": "PYF",
    "ISO 2": "PF"
  },
  {
    "FIPS": "FR",
    "ISO 3": "FRA",
    "ISO 2": "FR"
  },
  {
    "FIPS": "FS",
    "ISO 3": "ATF",
    "ISO 2": "TF"
  },
  {
    "FIPS": "GA",
    "ISO 3": "GMB",
    "ISO 2": "GM"
  },
  {
    "FIPS": "GB",
    "ISO 3": "GAB",
    "ISO 2": "GA"
  },
  {
    "FIPS": "GG",
    "ISO 3": "GEO",
    "ISO 2": "GE"
  },
  {
    "FIPS": "GH",
    "ISO 3": "GHA",
    "ISO 2": "GH"
  },
  {
    "FIPS": "GI",
    "ISO 3": "GIB",
    "ISO 2": "GI"
  },
  {
    "FIPS": "GJ",
    "ISO 3": "GRD",
    "ISO 2": "GD"
  },
  {
    "FIPS": "GK",
    "ISO 3": "GGY",
    "ISO 2": "GG"
  },
  {
    "FIPS": "GL",
    "ISO 3": "GRL",
    "ISO 2": "GL"
  },
  {
    "FIPS": "GM",
    "ISO 3": "DEU",
    "ISO 2": "DE"
  },
  {
    "FIPS": "GP",
    "ISO 3": "GLP",
    "ISO 2": "GP"
  },
  {
    "FIPS": "GQ",
    "ISO 3": "GUM",
    "ISO 2": "GU"
  },
  {
    "FIPS": "GR",
    "ISO 3": "GRC",
    "ISO 2": "GR"
  },
  {
    "FIPS": "GT",
    "ISO 3": "GTM",
    "ISO 2": "GT"
  },
  {
    "FIPS": "GV",
    "ISO 3": "GIN",
    "ISO 2": "GN"
  },
  {
    "FIPS": "GY",
    "ISO 3": "GUY",
    "ISO 2": "GY"
  },
  {
    "FIPS": "HA",
    "ISO 3": "HTI",
    "ISO 2": "HT"
  },
  {
    "FIPS": "HK",
    "ISO 3": "HKG",
    "ISO 2": "HK"
  },
  {
    "FIPS": "HM",
    "ISO 3": "HMD",
    "ISO 2": "HM"
  },
  {
    "FIPS": "HO",
    "ISO 3": "HND",
    "ISO 2": "HN"
  },
  {
    "FIPS": "HR",
    "ISO 3": "HRV",
    "ISO 2": "HR"
  },
  {
    "FIPS": "HU",
    "ISO 3": "HUN",
    "ISO 2": "HU"
  },
  {
    "FIPS": "IC",
    "ISO 3": "ISL",
    "ISO 2": "IS"
  },
  {
    "FIPS": "ID",
    "ISO 3": "IDN",
    "ISO 2": "ID"
  },
  {
    "FIPS": "IM",
    "ISO 3": "IMN",
    "ISO 2": "IM"
  },
  {
    "FIPS": "IN",
    "ISO 3": "IND",
    "ISO 2": "IN"
  },
  {
    "FIPS": "IO",
    "ISO 3": "IOT",
    "ISO 2": "IO"
  },
  {
    "FIPS": "IR",
    "ISO 3": "IRN",
    "ISO 2": "IR"
  },
  {
    "FIPS": "IS",
    "ISO 3": "ISR",
    "ISO 2": "IL"
  },
  {
    "FIPS": "IT",
    "ISO 3": "ITA",
    "ISO 2": "IT"
  },
  {
    "FIPS": "IV",
    "ISO 3": "CIV",
    "ISO 2": "CI"
  },
  {
    "FIPS": "IZ",
    "ISO 3": "IRQ",
    "ISO 2": "IQ"
  },
  {
    "FIPS": "JA",
    "ISO 3": "JPN",
    "ISO 2": "JP"
  },
  {
    "FIPS": "JE",
    "ISO 3": "JEY",
    "ISO 2": "JE"
  },
  {
    "FIPS": "JM",
    "ISO 3": "JAM",
    "ISO 2": "JM"
  },
  {
    "FIPS": "JO",
    "ISO 3": "JOR",
    "ISO 2": "JO"
  },
  {
    "FIPS": "KE",
    "ISO 3": "KEN",
    "ISO 2": "KE"
  },
  {
    "FIPS": "KG",
    "ISO 3": "KGZ",
    "ISO 2": "KG"
  },
  {
    "FIPS": "KN",
    "ISO 3": "PRK",
    "ISO 2": "KP"
  },
  {
    "FIPS": "KR",
    "ISO 3": "KIR",
    "ISO 2": "KI"
  },
  {
    "FIPS": "KS",
    "ISO 3": "KOR",
    "ISO 2": "KR"
  },
  {
    "FIPS": "KT",
    "ISO 3": "CXR",
    "ISO 2": "CX"
  },
  {
    "FIPS": "KU",
    "ISO 3": "KWT",
    "ISO 2": "KW"
  },
  {
    "FIPS": "KZ",
    "ISO 3": "KAZ",
    "ISO 2": "KZ"
  },
  {
    "FIPS": "LA",
    "ISO 3": "LAO",
    "ISO 2": "LA"
  },
  {
    "FIPS": "LE",
    "ISO 3": "LBN",
    "ISO 2": "LB"
  },
  {
    "FIPS": "LG",
    "ISO 3": "LVA",
    "ISO 2": "LV"
  },
  {
    "FIPS": "LH",
    "ISO 3": "LTU",
    "ISO 2": "LT"
  },
  {
    "FIPS": "LI",
    "ISO 3": "LBR",
    "ISO 2": "LR"
  },
  {
    "FIPS": "LO",
    "ISO 3": "SVK",
    "ISO 2": "SK"
  },
  {
    "FIPS": "LS",
    "ISO 3": "LIE",
    "ISO 2": "LI"
  },
  {
    "FIPS": "LT",
    "ISO 3": "LSO",
    "ISO 2": "LS"
  },
  {
    "FIPS": "LU",
    "ISO 3": "LUX",
    "ISO 2": "LU"
  },
  {
    "FIPS": "LY",
    "ISO 3": "LBY",
    "ISO 2": "LY"
  },
  {
    "FIPS": "MA",
    "ISO 3": "MDG",
    "ISO 2": "MG"
  },
  {
    "FIPS": "MB",
    "ISO 3": "MTQ",
    "ISO 2": "MQ"
  },
  {
    "FIPS": "MC",
    "ISO 3": "MAC",
    "ISO 2": "MO"
  },
  {
    "FIPS": "MD",
    "ISO 3": "MDA",
    "ISO 2": "MD"
  },
  {
    "FIPS": "MF",
    "ISO 3": "MYT",
    "ISO 2": "YT"
  },
  {
    "FIPS": "MG",
    "ISO 3": "MNG",
    "ISO 2": "MN"
  },
  {
    "FIPS": "MH",
    "ISO 3": "MSR",
    "ISO 2": "MS"
  },
  {
    "FIPS": "MI",
    "ISO 3": "MWI",
    "ISO 2": "MW"
  },
  {
    "FIPS": "MJ",
    "ISO 3": "MNE",
    "ISO 2": "ME"
  },
  {
    "FIPS": "MK",
    "ISO 3": "MKD",
    "ISO 2": "MK"
  },
  {
    "FIPS": "ML",
    "ISO 3": "MLI",
    "ISO 2": "ML"
  },
  {
    "FIPS": "MN",
    "ISO 3": "MCO",
    "ISO 2": "MC"
  },
  {
    "FIPS": "MO",
    "ISO 3": "MAR",
    "ISO 2": "MA"
  },
  {
    "FIPS": "MP",
    "ISO 3": "MUS",
    "ISO 2": "MU"
  },
  {
    "FIPS": "MR",
    "ISO 3": "MRT",
    "ISO 2": "MR"
  },
  {
    "FIPS": "MT",
    "ISO 3": "MLT",
    "ISO 2": "MT"
  },
  {
    "FIPS": "MU",
    "ISO 3": "OMN",
    "ISO 2": "OM"
  },
  {
    "FIPS": "MV",
    "ISO 3": "MDV",
    "ISO 2": "MV"
  },
  {
    "FIPS": "MX",
    "ISO 3": "MEX",
    "ISO 2": "MX"
  },
  {
    "FIPS": "MY",
    "ISO 3": "MYS",
    "ISO 2": "MY"
  },
  {
    "FIPS": "MZ",
    "ISO 3": "MOZ",
    "ISO 2": "MZ"
  },
  {
    "FIPS": "NC",
    "ISO 3": "NCL",
    "ISO 2": "NC"
  },
  {
    "FIPS": "NE",
    "ISO 3": "NIU",
    "ISO 2": "NU"
  },
  {
    "FIPS": "NF",
    "ISO 3": "NFK",
    "ISO 2": "NF"
  },
  {
    "FIPS": "NG",
    "ISO 3": "NER",
    "ISO 2": "NE"
  },
  {
    "FIPS": "NH",
    "ISO 3": "VUT",
    "ISO 2": "VU"
  },
  {
    "FIPS": "NI",
    "ISO 3": "NGA",
    "ISO 2": "NG"
  },
  {
    "FIPS": "NL",
    "ISO 3": "NLD",
    "ISO 2": "NL"
  },
  {
    "FIPS": "NO",
    "ISO 3": "NOR",
    "ISO 2": "NO"
  },
  {
    "FIPS": "NP",
    "ISO 3": "NPL",
    "ISO 2": "NP"
  },
  {
    "FIPS": "NR",
    "ISO 3": "NRU",
    "ISO 2": "NR"
  },
  {
    "FIPS": "NS",
    "ISO 3": "SUR",
    "ISO 2": "SR"
  },
  {
    "FIPS": "NT",
    "ISO 3": "ANT",
    "ISO 2": "AN"
  },
  {
    "FIPS": "NU",
    "ISO 3": "NIC",
    "ISO 2": "NI"
  },
  {
    "FIPS": "NZ",
    "ISO 3": "NZL",
    "ISO 2": "NZ"
  },
  {
    "FIPS": "PA",
    "ISO 3": "PRY",
    "ISO 2": "PY"
  },
  {
    "FIPS": "PC",
    "ISO 3": "PCN",
    "ISO 2": "PN"
  },
  {
    "FIPS": "PE",
    "ISO 3": "PER",
    "ISO 2": "PE"
  },
  {
    "FIPS": "PK",
    "ISO 3": "PAK",
    "ISO 2": "PK"
  },
  {
    "FIPS": "PL",
    "ISO 3": "POL",
    "ISO 2": "PL"
  },
  {
    "FIPS": "PM",
    "ISO 3": "PAN",
    "ISO 2": "PA"
  },
  {
    "FIPS": "PO",
    "ISO 3": "PRT",
    "ISO 2": "PT"
  },
  {
    "FIPS": "PP",
    "ISO 3": "PNG",
    "ISO 2": "PG"
  },
  {
    "FIPS": "PS",
    "ISO 3": "PLW",
    "ISO 2": "PW"
  },
  {
    "FIPS": "PU",
    "ISO 3": "GNB",
    "ISO 2": "GW"
  },
  {
    "FIPS": "QA",
    "ISO 3": "QAT",
    "ISO 2": "QA"
  },
  {
    "FIPS": "RB",
    "ISO 3": "SRB",
    "ISO 2": "RS"
  },
  {
    "FIPS": "RE",
    "ISO 3": "REU",
    "ISO 2": "RE"
  },
  {
    "FIPS": "RM",
    "ISO 3": "MHL",
    "ISO 2": "MH"
  },
  {
    "FIPS": "RN",
    "ISO 3": "MAF",
    "ISO 2": "MF"
  },
  {
    "FIPS": "RO",
    "ISO 3": "ROU",
    "ISO 2": "RO"
  },
  {
    "FIPS": "RP",
    "ISO 3": "PHL",
    "ISO 2": "PH"
  },
  {
    "FIPS": "RQ",
    "ISO 3": "PRI",
    "ISO 2": "PR"
  },
  {
    "FIPS": "RS",
    "ISO 3": "RUS",
    "ISO 2": "RU"
  },
  {
    "FIPS": "RW",
    "ISO 3": "RWA",
    "ISO 2": "RW"
  },
  {
    "FIPS": "SA",
    "ISO 3": "SAU",
    "ISO 2": "SA"
  },
  {
    "FIPS": "SB",
    "ISO 3": "SPM",
    "ISO 2": "PM"
  },
  {
    "FIPS": "SC",
    "ISO 3": "KNA",
    "ISO 2": "KN"
  },
  {
    "FIPS": "SE",
    "ISO 3": "SYC",
    "ISO 2": "SC"
  },
  {
    "FIPS": "SF",
    "ISO 3": "ZAF",
    "ISO 2": "ZA"
  },
  {
    "FIPS": "SG",
    "ISO 3": "SEN",
    "ISO 2": "SN"
  },
  {
    "FIPS": "SH",
    "ISO 3": "SHN",
    "ISO 2": "SH"
  },
  {
    "FIPS": "SI",
    "ISO 3": "SVN",
    "ISO 2": "SI"
  },
  {
    "FIPS": "SL",
    "ISO 3": "SLE",
    "ISO 2": "SL"
  },
  {
    "FIPS": "SM",
    "ISO 3": "SMR",
    "ISO 2": "SM"
  },
  {
    "FIPS": "SN",
    "ISO 3": "SGP",
    "ISO 2": "SG"
  },
  {
    "FIPS": "SO",
    "ISO 3": "SOM",
    "ISO 2": "SO"
  },
  {
    "FIPS": "SP",
    "ISO 3": "ESP",
    "ISO 2": "ES"
  },
  {
    "FIPS": "ST",
    "ISO 3": "LCA",
    "ISO 2": "LC"
  },
  {
    "FIPS": "SU",
    "ISO 3": "SDN",
    "ISO 2": "SD"
  },
  {
    "FIPS": "SV",
    "ISO 3": "SJM",
    "ISO 2": "SJ"
  },
  {
    "FIPS": "SW",
    "ISO 3": "SWE",
    "ISO 2": "SE"
  },
  {
    "FIPS": "SX",
    "ISO 3": "SGS",
    "ISO 2": "GS"
  },
  {
    "FIPS": "SY",
    "ISO 3": "SYR",
    "ISO 2": "SY"
  },
  {
    "FIPS": "SZ",
    "ISO 3": "CHE",
    "ISO 2": "CH"
  },
  {
    "FIPS": "TB",
    "ISO 3": "BLM",
    "ISO 2": "BL"
  },
  {
    "FIPS": "TD",
    "ISO 3": "TTO",
    "ISO 2": "TT"
  },
  {
    "FIPS": "TH",
    "ISO 3": "THA",
    "ISO 2": "TH"
  },
  {
    "FIPS": "TI",
    "ISO 3": "TJK",
    "ISO 2": "TJ"
  },
  {
    "FIPS": "TK",
    "ISO 3": "TCA",
    "ISO 2": "TC"
  },
  {
    "FIPS": "TL",
    "ISO 3": "TKL",
    "ISO 2": "TK"
  },
  {
    "FIPS": "TN",
    "ISO 3": "TON",
    "ISO 2": "TO"
  },
  {
    "FIPS": "TO",
    "ISO 3": "TGO",
    "ISO 2": "TG"
  },
  {
    "FIPS": "TP",
    "ISO 3": "STP",
    "ISO 2": "ST"
  },
  {
    "FIPS": "TS",
    "ISO 3": "TUN",
    "ISO 2": "TN"
  },
  {
    "FIPS": "TT",
    "ISO 3": "TLS",
    "ISO 2": "TL"
  },
  {
    "FIPS": "TU",
    "ISO 3": "TUR",
    "ISO 2": "TR"
  },
  {
    "FIPS": "TV",
    "ISO 3": "TUV",
    "ISO 2": "TV"
  },
  {
    "FIPS": "TW",
    "ISO 3": "TWN",
    "ISO 2": "TW"
  },
  {
    "FIPS": "TX",
    "ISO 3": "TKM",
    "ISO 2": "TM"
  },
  {
    "FIPS": "TZ",
    "ISO 3": "TZA",
    "ISO 2": "TZ"
  },
  {
    "FIPS": "UG",
    "ISO 3": "UGA",
    "ISO 2": "UG"
  },
  {
    "FIPS": "UK",
    "ISO 3": "GBR",
    "ISO 2": "GB"
  },
  {
    "FIPS": "UP",
    "ISO 3": "UKR",
    "ISO 2": "UA"
  },
  {
    "FIPS": "US",
    "ISO 3": "USA",
    "ISO 2": "US"
  },
  {
    "FIPS": "UV",
    "ISO 3": "BFA",
    "ISO 2": "BF"
  },
  {
    "FIPS": "UY",
    "ISO 3": "URY",
    "ISO 2": "UY"
  },
  {
    "FIPS": "UZ",
    "ISO 3": "UZB",
    "ISO 2": "UZ"
  },
  {
    "FIPS": "VC",
    "ISO 3": "VCT",
    "ISO 2": "VC"
  },
  {
    "FIPS": "VE",
    "ISO 3": "VEN",
    "ISO 2": "VE"
  },
  {
    "FIPS": "VI",
    "ISO 3": "VGB",
    "ISO 2": "VG"
  },
  {
    "FIPS": "VM",
    "ISO 3": "VNM",
    "ISO 2": "VN"
  },
  {
    "FIPS": "VQ",
    "ISO 3": "VIR",
    "ISO 2": "VI"
  },
  {
    "FIPS": "VT",
    "ISO 3": "VAT",
    "ISO 2": "VA"
  },
  {
    "FIPS": "WA",
    "ISO 3": "NAM",
    "ISO 2": "NA"
  },
  {
    "FIPS": "WF",
    "ISO 3": "WLF",
    "ISO 2": "WF"
  },
  {
    "FIPS": "WI",
    "ISO 3": "ESH",
    "ISO 2": "EH"
  },
  {
    "FIPS": "WS",
    "ISO 3": "WSM",
    "ISO 2": "WS"
  },
  {
    "FIPS": "WZ",
    "ISO 3": "SWZ",
    "ISO 2": "SZ"
  },
  {
    "FIPS": "YM",
    "ISO 3": "YEM",
    "ISO 2": "YE"
  },
  {
    "FIPS": "ZA",
    "ISO 3": "ZMB",
    "ISO 2": "ZM"
  },
  {
    "FIPS": "ZI",
    "ISO 3": "ZWE",
    "ISO 2": "ZW"
  }
]