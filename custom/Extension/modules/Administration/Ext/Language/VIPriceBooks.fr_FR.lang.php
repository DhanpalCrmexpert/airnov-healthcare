<?php
/*********************************************************************************
 * This file is part of package Price Books.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Price Books is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
$mod_strings['LBL_PRICE_BOOKS'] = "Tarifs";
$mod_strings['LBL_PRICE_BOOKS_DESCRIPTION'] = "Gérer les grilles de prix des produits pour des contacts / comptes particuliers.";
$mod_strings['LBL_ENABLE_PRICE_BOOKS'] = "Activer la fonctionnalité des listes de prix";
$mod_strings['LBL_PRICE_BOOKS_ACTIVATED'] = "Fonctionnalité des catalogues de prix activée avec succès.";
$mod_strings['LBL_PRICE_BOOKS_DEACTIVATED'] = "La fonctionnalité de tarification a été désactivée avec succès.";
$mod_strings['LBL_CLICK_HERE'] = "Cliquez ici";
$mod_strings['LBL_MANAGE_PRICEBOOK'] = "Pour gérer les tarifs";
//license
$mod_strings['LBL_LICENCE_ACTIVE_LABEL'] = "VIPriceBooksLicenseAddon n'est plus actif pour la raison suivante:";
$mod_strings['LBL_LICENCE_ISSUES'] = "Les utilisateurs n'auront aucun accès jusqu'à ce que le problème soit résolu";
$mod_strings['LBL_CLICK_HERE'] = "Cliquez ici";
$mod_strings['LBL_LICENCE_ACTIVE'] = "VIPriceBooksLicenseAddon n'est plus actif";
$mod_strings['LBL_RENEW_LICENCE'] = "Veuillez renouveler votre abonnement ou vérifier la configuration de votre licence.";
$mod_strings['LBL_UPDATE_LICENSE'] = "Mettre à jour la licence";
?>