
function sp_rem_conf(){
  return true;
}

function sub_p_rem(key, link, user_id){
  callUpdateUsersEntry(key, [user_id], false);
}

function set_return_and_save_background(popup_reply_data) {
  var subpanel = popup_reply_data.passthru_data.child_field;
  var name_to_value_array = popup_reply_data.name_to_value_array;
  var selection_list = popup_reply_data.selection_list;
  var users = [];
  if (name_to_value_array !== 'undefined') {
    for (var nv_key in name_to_value_array) {
      if (name_to_value_array.hasOwnProperty(nv_key) && nv_key !== 'toJSON') {
        users.push(name_to_value_array[nv_key]);
      }
    }
  }
  if (selection_list !== 'undefined') {
    for (var sl_key in selection_list) {
      if (selection_list.hasOwnProperty(sl_key)) {
        users.push(selection_list[sl_key]);
      }
    }
  }
  callUpdateUsersEntry(subpanel, users, true);
}


function callUpdateUsersEntry(sp, users, update) {
  $('#license_fail').hide();
  document.getElementById('license_fail_response').innerText = '';
  $('#license_success').hide();
  document.getElementById('license_success_response').innerText = '';
  $.ajax({
    url: 'index.php?entryPoint=updateLicensedOutlookUsers',
    contentType: 'JSON',
    data: {
      users: users,
      update: update
    },
    dataType: 'JSON',
    success: function(res) {
      updateUsersCallback(sp, res);
    }
  });
}

function callValidateEntry(key) {
  $('#invalid_key').hide();
  document.getElementById('invalid_response').innerText = '';
  $('#valid_key').hide();
  document.getElementById('valid_response').innerText = '';
  $.ajax({
    url: 'index.php?entryPoint=validateOutlookKey',
    contentType: 'JSON',
    data: {
      sa_outlook_license_key: key
    },
    dataType: 'JSON',
    success: validationCallback
  });
}

function updateUsersCallback(sp, usersResult) {
  if (usersResult.success) {
    document.getElementById('license_success_response').innerText = usersResult.result;
    $('#license_success').show();
  } else {
    document.getElementById('license_fail_response').innerText = usersResult.result;
    $('#license_fail').show();
  }
  showSubPanel(sp, null, true);
}

function validationCallback(validationResult) {
  if (validationResult.success) {
    document.getElementById('valid_response').innerText = validationResult.result;
    $('#valid_key').show();
  } else {
    document.getElementById('invalid_response').innerText = validationResult.result;
    $('#invalid_key').show();
  }
}
