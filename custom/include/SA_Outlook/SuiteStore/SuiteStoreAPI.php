<?php
/**
 * Copyright (c) SalesAgility Ltd.  All Rights Reserved.
 * Licensed under the GPL3 License.  See License in the project root
 * for license information.
 *
 * @package Outlook Addin for SuiteCRM
 * @copyright 2020 SalesAgility Ltd http://www.salesagility.com
 * @author SalesAgility <info@salesagility.com
 *
 */

namespace SalesAgility\Outlook\SuiteStore;

class SuiteStoreAPI implements StoreAPI
{
    private $baseUrl;
    private $publicKey;

    /**
     * SuiteStoreAPI constructor.
     * @param array $config
     */
    public function __construct($config)
    {
        $this->baseUrl =  $config['base_api_url'];
        $this->publicKey = $config['pub_key'];
    }

    private function call($path, $data = array(), $method = 'post')
    {
        if (empty($path)) {
            return array(
                'success' => false,
                'result' => 'API Call Path is not defined.'
            );
        }
        if (!is_array($data)) {
            return array(
                'success' => false,
                'result' => 'Data for API Call must be an array.'
            );
        }

        //load license validation config and generate url from config
        $url = $this->baseUrl . '/' . $path;

        if (empty($data['key'])) {
            return array(
                'success' => false,
                'result' => 'Key could not be found locally. Please go to the license configuration tool and enter your key.'
            );
        }

        $ch = curl_init();

        // setup data based on type of request
        if ($method === 'post') {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        } else {
            $url .= '?' . http_build_query($data);
        }

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FAILONERROR, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

        $response = curl_exec($ch);
        $info = curl_getinfo($ch);
        curl_close($ch);

        $json = getJSONobj();
        $result = $json::decode($response);

        if ($info['http_code'] != 200) {
            $GLOBALS['log']->fatal('SuiteStoreAPI call(): HTTP Request failed: ' . print_r($info, true));

            return array(
                'success' => false,
                'result' => $result
            );
        }

        return array(
            'success' => true,
            'result' => $result
        );
    }

    public function getLicensedUserCount($key)
    {
        if (!$key || !$this->baseUrl || !$this->publicKey) {
            return 0;
        }

        $path = 'key/validate';
        $data = array(
            'public_key' => $this->publicKey,
            'key' => $key
        );
        $result = $this->call($path, $data, 'get');

        if ($result['success'] && $result['result']['licensed_user_count']) {
            return $result['result']['licensed_user_count'];
        }

        return 0;
    }

    public function validate($key)
    {
        global $app_strings;

        if (!$key || !$this->baseUrl || !$this->publicKey) {
            return array(
                'success' => false,
                'result' => $app_strings['LBL_SA_OUTLOOK_LICENSE_NO_KEY']
            );
        }

        $path = 'key/validate';
        $data = array(
            'public_key' => $this->publicKey,
            'key' => $key
        );
        $result = $this->call($path, $data, 'get');

        if ($result['success'] && $result['result']['validated']) {
            $result['result'] = $app_strings['LBL_SA_OUTLOOK_LICENSE_VALIDATION_SUCCESS'];

            return $result;
        }

        return $result;
    }
}
