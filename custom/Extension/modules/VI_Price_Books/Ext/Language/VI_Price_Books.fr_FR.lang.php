<?php
/*********************************************************************************
 * This file is part of package Price Books.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Price Books is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
$mod_strings['LBL_ACTIVE'] = 'Actif';
$mod_strings['LBL_PERCENTAGE_MARK'] = 'Marque de pourcentage';
$mod_strings['LBL_TYPE'] = 'Type de prix';
$mod_strings['LBL_ROUND_OFF'] = 'Arrondir à';
$mod_strings['LBL_ACCOUNT_NAME'] = 'Nom du compte';
$mod_strings['LBL_ACCOUNT_ID'] = 'ID de compte';
$mod_strings['LBL_ACCOUNTS'] = 'Comptes';
$mod_strings['LBL_CONTACT_NAME'] = 'Nom du contact';
$mod_strings['LBL_CONTACT_ID'] = 'Contact ID';
$mod_strings['LBL_CONTACTS'] = 'Contacts';
$mod_strings['LBL_FLAT_PRICE'] = 'Prix forfaitaire';
$mod_strings['LBL_DIFFERENTIAL_PERCENTAGE'] = 'Différentiel de (%)';
$mod_strings['LBL_DIFFERENTIAL_DOLLAR'] = 'Différentiel par ($)';
$mod_strings['LBL_PRICE_BOOKS'] = 'Liste de prix';
$mod_strings['LBL_NAME'] = 'Nom';
$mod_strings['LBL_PRICE_TYPE'] = 'Type de prix';
$mod_strings['LBL_ASSIGNED_TO'] = 'Assigné à';
$mod_strings['LBL_NO_RECORDS'] = 'Aucun enregistrement trouvé';
$mod_strings['LBL_MARK_UP'] = 'MarkUp (Add)';
$mod_strings['LBL_MARK_DOWN'] = 'MarkDown (Sub)';
$mod_strings['LBL_ADD_QUANTITY'] = 'Ajouter une quantité';
$mod_strings['LBL_PRODUCT_NAME'] = 'Nom du produit';
$mod_strings['LBL_PRODUCT_PRICE'] = 'Prix du produit';
$mod_strings['LBL_LIST_PRICE'] = 'Prix catalogue';
$mod_strings['LBL_ADD_PRODUCT'] = 'Ajouter un produit';
$mod_strings['LBL_YES'] = 'Oui';
$mod_strings['LBL_NO'] = 'Non';
$mod_strings['LBL_SELECT_PRODUCT_MSG'] = "Veuillez sélectionner le produit avant de cliquer sur les tarifs !!!";
$mod_strings['LBL_SELECT_PRICEBOOK_MSG'] = "Veuillez sélectionner un seul tarif.";
$mod_strings['LBL_PRODUCT_VALIDATION'] = 'Veuillez sélectionner un autre produit. Ce produit est déjà sélectionné. ';
$mod_strings['LBL_QUANTITY_VALID_MSG'] = "Valeur invalide:";
$mod_strings['LBL_FROM_QUANTITY'] = "De la quantité";
$mod_strings['LBL_TO_QUANTITY'] = "En quantité";
$mod_strings['LBL_DIFFERENTIAL'] = "Différentiel";
$mod_strings['LBL_PRICE_BOOKS_ACCESS'] = "Veuillez activer la fonctionnalité de tarifs pour accéder au module de tarifs.";
$mod_strings['LBL_CLICK_HERE'] = "Cliquez ici";
$mod_strings['LBL_ENABLE_LABEL'] = "pour l'activer.";
$mod_strings['LBL_FROM_QUANTITY_VALID'] = "Veuillez saisir une valeur supérieure à la quantité précédente à la précédente.";
$mod_strings['LBL_TO_QUANTITY_VALID'] = "Veuillez saisir une valeur supérieure à la quantité par rapport à la valeur par la quantité.";
$mod_strings['LBL_PRODUCT_LIST_PRICE_VALID'] = "Veuillez ajouter le produit et le prix catalogue pour ajouter la quantité.";
$mod_strings['LBL_PRODCUT_PRICE_VALID'] = "Veuillez ajouter le prix du produit pour le produit.";
$mod_strings['LBL_LIST_PRICE_VALID'] = "Veuillez ajouter le prix courant du produit.";
$mod_strings['LBL_QUANTITY_VALID_RANGE'] = "Veuillez ajouter une plage de quantité pour le produit.";
$mod_strings['LBL_PRODUCT_VALID'] = "Veuillez ajouter un produit pour ajouter une quantité.";
$mod_strings['LBL_PRODUCT_FIELDS_VALIDATION'] = "Veuillez ajouter le nom du produit et le prix catalogue pour les grilles de prix.";
$mod_strings['LBL_QUANTITY_FIELD_VALIDATION'] = "Veuillez remplir la plage de quantité pour les dalles vides.";
$mod_strings['LBL_EDITVIEW_PANEL1'] = "Détails des listes de prix";
$mod_strings['LBL_DETAILVIEW_PANEL1'] = "Détails des listes de prix";
?>