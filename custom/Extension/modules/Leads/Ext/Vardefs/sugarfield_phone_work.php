<?php
 // created: 2020-11-19 04:21:24
$dictionary['Lead']['fields']['phone_work']['required']=true;
$dictionary['Lead']['fields']['phone_work']['inline_edit']=true;
$dictionary['Lead']['fields']['phone_work']['comments']='Work phone number of the contact';
$dictionary['Lead']['fields']['phone_work']['merge_filter']='disabled';

 ?>