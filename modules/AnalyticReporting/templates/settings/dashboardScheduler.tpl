
{include file="modules/$moduleName/templates/settings/pro_notification.tpl"}

<div>
    <h2>Test Dashboard Scheduler</h2>

    <div class="ar-container clearfix">
        <p>Select Dashboard:</p>
        <select disabled decorator="select2" style="min-width:375px;">
            <option>Select dashboard to send</option>
        </select>

        <p>Select User:</p>
        <select disabled decorator="select2" style="min-width:250px;">
            <option>Select User</option>
        </select>

        <p>Or custom email:</p>
        <input type="text" placeholder="Type email address" style="padding:3px;" disabled>

        <p><button class="ar-button green" disabled>Send dashboard report</button></p>

        <h2 style="margin-top:10px;">Actions</h2>
        <p>Run "Send scheduled dashboard" script immediately</p>
        <button class="ar-button green" disabled>Launch</button>

        <h2 style="margin-top:10px;">System information</h2>
        <p>Server Current time: {$currentTime} {$currentTimeZone}. Dashboard reports are sent based on this time.</p>
        <p>User Current time: {$userCurrentTime} {$userCurrentTimeZone}. The list bellow shows schedule times in server time which are obtained using dashboard owner and server timezone offsets. </p>

        <h2 style="margin-top:10px;">Scheduled dashboard information</h2>
        <table class="ar-table">
            <thead>
                <tr>
                    <th>Dashboard ID</th>
                    <th>Dashboard Name</th>
                    <th>Interval</th>
                    <th>Interval options</th>
                    <th>Time</th>
                    <th>Next Time (Server Time + Report Owner timezone)</th>
                    <th>Emails</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>
