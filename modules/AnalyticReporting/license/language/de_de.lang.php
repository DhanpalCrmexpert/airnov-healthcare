<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

$license_strings = array (
    'LBL_STEPS_TO_LOCATE_KEY_TITLE' => 'Zum Suchen Sie Ihr Schl�ssel',
    'LBL_STEPS_TO_LOCATE_KEY_1' => '1. Melden Sie sich bei',
    'LBL_STEPS_TO_LOCATE_KEY_2' => '2. gehen Sie zu Account->Purchases',
    'LBL_STEPS_TO_LOCATE_KEY_3' => '3. Suchen Sie den Schl�ssel f�r den Kauf dieses Add-on',
    'LBL_STEPS_TO_LOCATE_KEY_4' => '4. F�gen Sie in das Feld Lizenzschl�ssel unter',
    'LBL_STEPS_TO_LOCATE_KEY_5' => '5. Klicken Sie auf "Validate"',
    'LBL_LICENSE_KEY' => 'Lizenzschl�ssel',
    'LBL_CURRENT_USERS' => 'Aktuelle Benutzeranzahl',
    'LBL_LICENSED_USERS' => 'Lizenzierte Benutzer Anzahl',
    'LBL_VALIDATE_LABEL' => 'best�tigen',
    'LBL_VALIDATED_LABEL' => 'Validierte',
);

