<?php 

    if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

    class projectTaskLogicHookImplementationTimeSheets
    {
        function after_ui_frame_method($event, $arguments)
        {
            if (strtolower($_REQUEST['action']) == 'detailview') {
				echo '<script src="custom/include/auto_time_sheet_tracker.js"></script>';
			}
        }
    }
	
?>