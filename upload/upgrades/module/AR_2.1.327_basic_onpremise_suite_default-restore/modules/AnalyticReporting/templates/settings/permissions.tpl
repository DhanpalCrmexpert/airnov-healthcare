{include file="modules/$moduleName/templates/settings/pro_notification.tpl"}

<div>
    <h2 style="color:#333;font-size:1.5em;text-align:left;">{$MOD.permissions}</h2>
    <p>
        <input id="report-builder-public-access" type="checkbox" disabled />
        <label style="font-size:1em;" for="report-builder-public-access">{$MOD.reportBuilderPublicAccess}</label>
        <a class="ar-button" href="{$SERVERURL}?module={$moduleName}&action=reportBuilder">{$MOD.open} {$MOD.reportBuilder}</a>
    </p>

    <p>
        <input id="report-tree-admin-access" type="checkbox" disabled />
        <label style="font-size:1em;" for="report-tree-admin-access">{$MOD.reportTreeAdminAccess}</label>
    </p>
</div>