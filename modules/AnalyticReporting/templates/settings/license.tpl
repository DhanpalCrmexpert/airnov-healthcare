<form action="index.php" method="post" enctype="multipart/form-data">
    <input type="hidden" name="module" value="{$moduleName}" />
    <input type="hidden" name="action" value="settings" />
    <input type="hidden" name="call" value="updateLicense" />

    <h2 style="color:#333;font-size:1.5em;text-align:left;">{$MOD.licenseManagment}</h2>

    <div style="padding:5px;">
        <table>
            <tr>
                <td style="vertical-align: top;">
                    <div>
                        <textarea id="license" name="key" style="font-family: mono; width: 600px; height: 130px;">{$licenseKey}</textarea>
                    </div>
                    <div style="text-align:right">
                        <input class="ar-button green" type="submit" name="submit" value="{$MOD.label_save}" />
                    </div>
                    <div>
                        {$response}
                    </div>
                </td>
                <td>
                    <div style="margin-left:7px;">
                        {foreach from=$licenseData item=test}
                            <p>{$test.title}: <b>{$test.value}</b></p>
                        {/foreach}

                        {* Added unique key for server instance identification *}
                        <p>{$MOD.label_unique_key}: <b>{$uniqueKey}</b></p>
                    <div>
                </td>
            </tr>
        </table>
    </div>

    <div id="ar-license-users-table" class="ar-settings-table"></div>
</form>
{literal}
<script id='licenseUsersTable' type='text/ractive'>
{{#isAnyUsers}}
<table>
    <tr>
        <th>First Name</th>
        <th>Last Name</th>
        <th>User Type</th>
        <th>Access Type</th>
    </tr>
    {{#users}}
    <tr>
        <td><a href="{{linkTo}}">{{first_name}}</a></td>
        <td><a href="{{linkTo}}">{{last_name}}</a></td>
        <td>Regular</td>
        <td>{{accessName}}</td>
    </tr>
    {{/users}}
</table>
{{/isAnyUsers}}

<div style="vertical-align:middle">
{{^isAnyUsers}}
{/literal}{$MOD.label_show_license_users}{literal}: {{^loader}}<input class="ar-button green" type="button" value="{/literal}{$MOD.label_load}{literal}" on-click="loadUsers" />{{/loader}}
{{/isAnyUsers}}

{{#loader}}<img src="modules/AnalyticReporting/assets/css/select2-spinner.gif" alt="{/literal}{$MOD.label_loading}{literal}..." />{{/loader}}
</div>

{{#error}}
<div style="color:red">{{error}}</div>
{{/error}}

{{#isUserCountLimit}}
<div style="margin:3px 0;">
    <b style="color:red">*</b>
    <i style="color:#999">{{getUserCountLimitText}}</i>
</div>
{{/isUserCountLimit}}
</script>
<!-- env:dev --#>
<script src="modules/AnalyticReporting/assets/js/components/settings/licenseUsersTable.js"></script>
<!-- env:dev:end -->
<!-- env:prod -->
<script src="modules/AnalyticReporting/assets/js/licenseUsersTable.min.js?v=2.1.327"></script>
<!-- env:prod:end -->
{/literal}
