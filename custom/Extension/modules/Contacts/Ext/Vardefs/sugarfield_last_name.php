<?php
 // created: 2020-11-03 18:22:43
$dictionary['Contact']['fields']['last_name']['audited']=true;
$dictionary['Contact']['fields']['last_name']['inline_edit']=true;
$dictionary['Contact']['fields']['last_name']['comments']='Last name of the contact';
$dictionary['Contact']['fields']['last_name']['merge_filter']='disabled';

 ?>