<?php
/*********************************************************************************
 * This file is part of package Price Books.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Price Books is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/

/*
 * Relationship definition
 */
$dictionary['Account']['relationships']['account_vi_price_books'] = array(
    'lhs_module'        => 'Accounts',
    'lhs_table'         => 'accounts',
    'lhs_key'           => 'id',
    'rhs_module'        => 'VI_Price_Books',
    'rhs_table'         => 'vi_price_books',
    'rhs_key'           => 'account_id',
    'relationship_type' => 'one-to-many',
);

$dictionary['Account']['fields']['vi_price_books'] = array(
    'name'         => 'vi_price_books',
    'type'         => 'link',
    'relationship' => 'account_vi_price_books',
    'module'       => 'VI_Price_Books',
    'bean_name'    => 'VI_Price_Books',
    'source'       => 'non-db',
    'vname'        => 'LBL_PRICE_BOOKS_LABEL',
);

?>