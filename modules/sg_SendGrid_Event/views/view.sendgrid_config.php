<?php

if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

require_once("modules/Administration/Administration.php");

class Viewsendgrid_config extends SugarView {

    public function preDisplay() {
        global $current_user;
        global $sugar_config;
        if (!is_admin($current_user)) {
            sugar_die("Unauthorized access to administration.");
        }

       require_once('modules/sg_SendGrid_Event/license/DT_SendGrid_OutfittersLicense.php');
       $validate = DT_SendGrid_OutfittersLicense::isValid('sg_SendGrid_Event');
       $validate = 1;
       if ($validate != 1) {
           header('Location: index.php?module=sg_SendGrid_Event&action=license');
       }

    }

    public function display() {

        require_once('include/Sugar_Smarty.php');
        global $sugar_config, $db;

        if (!is_admin($GLOBALS['current_user'])) {
            sugar_die('You do not have permission.');
        }

        $sendgrid_admin = new Administration();
        $sendgrid_ss = new Sugar_Smarty();
        

        if ((isset($_POST['save_sendgrid'])) && (!empty($_POST['save_sendgrid']))) {            
            $sendgrid_admin->saveSetting('sendgrid_config', 'sendgrid_api_key', $_REQUEST['sendgrid_api_key']);
            $sendgrid_admin->saveSetting('sendgrid_config', 'use_sendgrid_as_default', $_REQUEST['use_sendgrid_as_default']); 
            SugarApplication::redirect('index.php?module=sg_SendGrid_Event&action=sendgrid_config');
            
        }else
        {
            $settings_sendgrid = $sendgrid_admin->retrieveSettings('sendgrid_config');            
            $sendgrid_ss->assign('sendgrid_api_key', $settings_sendgrid->settings['sendgrid_config_sendgrid_api_key']);
            if($settings_sendgrid->settings['sendgrid_config_use_sendgrid_as_default'] == "on")
            {             
                $sendgrid_ss->assign('use_sendgrid_as_default', "checked");
            }
            else
            {                
                $sendgrid_ss->assign('use_sendgrid_as_default', "");   
            }            
        }

        $sendgrid_ss->assign('MOD', $GLOBALS['mod_strings']);
        $sendgrid_ss->assign('APP', $GLOBALS['app_strings']);
        echo $sendgrid_ss->fetch('modules/sg_SendGrid_Event/tpls/sendgrid_config.tpl');
    }

}
