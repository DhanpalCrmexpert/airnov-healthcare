<?php
$module_name = 'sp_timesheet_tracker';
$listViewDefs [$module_name] = 
array (
  'PARENT_NAME' => 
  array (
    'type' => 'parent',
    'default' => true,
    'studio' => 'visible',
    'label' => 'LBL_PARENT_NAME',
    'link' => true,
    'sortable' => false,
    'ACLTag' => 'PARENT',
    'dynamic_module' => 'PARENT_TYPE',
    'id' => 'PARENT_ID',
    'related_fields' => 
    array (
      0 => 'parent_id',
      1 => 'parent_type',
    ),
    'width' => '10%',
  ),
  'RELATED_RECORD_URL' => 
  array (
    'label' => 'LBL_RELATED_RECORD_URL',
    'width' => '10%',
    'default' => true,
  ),
  'TRACK_DATE' => 
  array (
    'type' => 'date',
    'label' => 'LBL_TRACK_DATE',
    'width' => '10%',
    'default' => true,
  ),
  'HOURS' => 
  array (
    'type' => 'float',
    'label' => 'LBL_HOURS',
    'width' => '10%',
    'default' => true,
  ),
  'TRACK_USER' => 
  array (
    'type' => 'relate',
    'studio' => 'visible',
    'label' => 'LBL_TRACK_USER',
    'id' => 'USER_ID_C',
    'link' => true,
    'width' => '10%',
    'default' => true,
  ),
  'description' => 
  array (
    'type' => 'text',
    'label' => 'LBL_DESCRIPTION',
    'width' => '30%',
    'default' => true,
  ),
);
?>
