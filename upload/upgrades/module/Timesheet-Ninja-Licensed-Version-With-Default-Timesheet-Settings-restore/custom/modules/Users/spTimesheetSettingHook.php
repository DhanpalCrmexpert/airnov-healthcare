<?php

class spTimesheetSetting{
	public function updateConfigEntries(&$bean, $args, $params){
		global $current_user, $sugar_config;
		
		$conf = new Configurator();
		// Save module settings
		$selected_module = trim($bean->sp_timesheet_module);
		$related_module_relationship = trim(str_replace($selected_module."_", '', $bean->sp_timesheet_related_module));
		$conf->config['timesheet_module_'.$current_user->id] = $selected_module;
		$conf->config['related_module_relationship_'.$current_user->id] = $related_module_relationship;
		
		if(!empty($selected_module) && !empty($related_module_relationship)){
			$module_obj = BeanFactory::getBean($selected_module);
			if($module_obj->load_relationship($related_module_relationship)){
				$related_module = $module_obj->$related_module_relationship->getRelatedModuleName();
				$conf->config['related_module_'.$current_user->id] = $related_module;
			}
			$conf->handleOverride();
			$conf->clearCache();
		}
	}
}

?>