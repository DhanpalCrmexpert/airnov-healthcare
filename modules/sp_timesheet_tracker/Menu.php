<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point'); 
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.

 * SuiteCRM is an extension to SugarCRM Community Edition developed by Salesagility Ltd.
 * Copyright (C) 2011 - 2014 Salesagility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for  technical reasons, the Appropriate Legal Notices must
 * display the words  "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 ********************************************************************************/


global $mod_strings, $app_strings, $sugar_config;
 
if(ACLController::checkAccess('sp_Timesheet', 'edit', true))$module_menu[]=Array("index.php?module=sp_Timesheet&action=EditView&return_module=sp_Timesheet&return_action=DetailView", $app_strings['LNK_NEW_RECORD'],"Createsp_Timesheet", 'sp_Timesheet');
if(ACLController::checkAccess('sp_Timesheet', 'edit', true))$module_menu[]=Array("index.php?module=sp_Timesheet&action=EditView&return_module=sp_Timesheet&return_action=DetailView&week_view=true", $app_strings['LNK_NEW_RECORD_WEEKLY'],"Createsp_Timesheet", 'sp_Timesheet');
if(ACLController::checkAccess('sp_Timesheet', 'list', true))$module_menu[]=Array("index.php?module=sp_Timesheet&action=index&return_module=sp_Timesheet&return_action=DetailView", $app_strings['LNK_LIST'],"sp_Timesheet", 'sp_Timesheet');
if(ACLController::checkAccess('sp_timesheet_tracker', 'list', true))$module_menu[]=Array("index.php?module=sp_timesheet_tracker&action=index&return_module=sp_timesheet_tracker&return_action=DetailView", "View Timesheet Ninja Reporting","sp_timesheet_tracker", 'sp_timesheet_tracker');
if(ACLController::checkAccess('sp_Timesheet', 'list', true))$module_menu[]=Array("index.php?module=sp_Timesheet&action=visualreports", "Timesheet Visual Reports","sp_Timesheet", 'sp_Timesheet');
if(ACLController::checkAccess('sp_timesheet_tracker', 'list', true))$module_menu[]=Array("index.php?module=sp_Auto_TimeSheet_Time_Tracker&action=EditView&return_module=sp_Auto_TimeSheet_Time_Tracker&return_action=DetailView", "Create Timesheet Global Modules Entry","sp_Auto_TimeSheet_Time_Tracker", 'sp_Auto_TimeSheet_Time_Tracker');

if(ACLController::checkAccess('sp_Timesheet', 'edit', true))$module_menu[]=Array("index.php?module=sp_Timesheet&action=TimesheetSettings&return_module=sp_Timesheet&return_action=EditView", $app_strings['LNK_TIMESHEET_SETTINGS'],"Settingssp_Timesheet", 'sp_Timesheet');

 if(ACLController::checkAccess('sp_timesheet_tracker', 'import', true))$module_menu[]=Array("index.php?module=Import&action=Step1&import_module=sp_timesheet_tracker&return_module=sp_timesheet_tracker&return_action=index", $app_strings['LBL_IMPORT'],"Import", 'sp_timesheet_tracker');