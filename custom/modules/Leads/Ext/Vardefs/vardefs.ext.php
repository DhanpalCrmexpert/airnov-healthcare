<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2019-02-21 03:39:45
$dictionary['Lead']['fields']['jjwg_maps_lat_c']['inline_edit']=1;

 

 // created: 2019-02-21 03:39:45
$dictionary['Lead']['fields']['jjwg_maps_address_c']['inline_edit']=1;

 

 // created: 2020-11-18 16:10:41
$dictionary['Lead']['fields']['department_ahp_c']['inline_edit']='1';
$dictionary['Lead']['fields']['department_ahp_c']['labelValue']='Department';

 

 // created: 2020-11-19 04:21:24
$dictionary['Lead']['fields']['phone_work']['required']=true;
$dictionary['Lead']['fields']['phone_work']['inline_edit']=true;
$dictionary['Lead']['fields']['phone_work']['comments']='Work phone number of the contact';
$dictionary['Lead']['fields']['phone_work']['merge_filter']='disabled';

 

 // created: 2019-02-21 03:39:45
$dictionary['Lead']['fields']['jjwg_maps_geocode_status_c']['inline_edit']=1;

 

 // created: 2020-11-18 17:10:57
$dictionary['Lead']['fields']['salutation']['len']=100;
$dictionary['Lead']['fields']['salutation']['required']=true;
$dictionary['Lead']['fields']['salutation']['inline_edit']=true;
$dictionary['Lead']['fields']['salutation']['comments']='Contact salutation (e.g., Mr, Ms)';
$dictionary['Lead']['fields']['salutation']['merge_filter']='disabled';

 

 // created: 2020-11-19 04:21:00
$dictionary['Lead']['fields']['first_name']['required']=true;
$dictionary['Lead']['fields']['first_name']['inline_edit']=true;
$dictionary['Lead']['fields']['first_name']['comments']='First name of the contact';
$dictionary['Lead']['fields']['first_name']['merge_filter']='disabled';

 

 // created: 2020-11-18 16:09:10
$dictionary['Lead']['fields']['department']['inline_edit']=true;
$dictionary['Lead']['fields']['department']['comments']='Department the lead belongs to';
$dictionary['Lead']['fields']['department']['merge_filter']='disabled';

 

 // created: 2020-11-18 17:11:42
$dictionary['Lead']['fields']['email1']['required']=true;
$dictionary['Lead']['fields']['email1']['inline_edit']=true;
$dictionary['Lead']['fields']['email1']['merge_filter']='disabled';

 

 // created: 2019-02-21 03:39:45
$dictionary['Lead']['fields']['jjwg_maps_lng_c']['inline_edit']=1;

 
?>