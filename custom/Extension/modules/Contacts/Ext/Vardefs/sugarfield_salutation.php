<?php
 // created: 2020-11-05 14:16:47
$dictionary['Contact']['fields']['salutation']['len']=100;
$dictionary['Contact']['fields']['salutation']['required']=true;
$dictionary['Contact']['fields']['salutation']['inline_edit']=true;
$dictionary['Contact']['fields']['salutation']['comments']='Contact salutation (e.g., Mr, Ms)';
$dictionary['Contact']['fields']['salutation']['merge_filter']='disabled';

 ?>