<?php
// created: 2020-11-18 06:37:33
$mod_strings = array (
  'LBL_LEADS' => 'Prospects',
  'LBL_ACCOUNTS' => 'Customers',
  'LBL_ACCOUNT_ID' => 'Customer ID:',
  'LBL_CASES' => 'QNS',
  'LBL_CASE_ID' => 'QNS ID:',
);