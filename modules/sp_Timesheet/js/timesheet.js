$(document).ready(function() {
	
	$(document).on("input",".hours_field", function(evt) {
	//$(".hours_field").on("input", function(evt) {
   var self = $(this);
   self.val(self.val().replace(/[^0-9\.]/g, ''));
   if ((evt.which != 46 || self.val().indexOf('.') != -1) && (evt.which < 48 || evt.which > 57)) 
   {
     evt.preventDefault();
   }
 });
 
	Calendar.setup ({
	inputField : "date_field",
	daFormat : "%d-%m-%Y",
	button : "date_triggere",
	singleClick : true,
	dateStr : "",
	step : 1,
	weekNumbers:false
	});

	$("#next_week").click(function(){
		var next_week_date = $("#next_week_date").val();
		if(getUrlParameter('week_view') !== undefined){
			window.location = "index.php?module=sp_Timesheet&action=EditView&return_module=sp_Timesheet&return_action=DetailView&week_start_date="+next_week_date+"&week_view=true";
		}else{
			window.location = "index.php?module=sp_Timesheet&action=EditView&return_module=sp_Timesheet&return_action=DetailView&week_start_date="+next_week_date;
		}
	});
	
	$("#previous_week").click(function(){
		var previous_week_date = $("#previous_week_date").val();
		if(getUrlParameter('week_view') !== undefined){
			window.location = "index.php?module=sp_Timesheet&action=EditView&return_module=sp_Timesheet&return_action=DetailView&week_start_date="+previous_week_date+"&week_view=true";
		}else{
			window.location = "index.php?module=sp_Timesheet&action=EditView&return_module=sp_Timesheet&return_action=DetailView&week_start_date="+previous_week_date;
		}
	});
	
	$("#custom_week").click(function(){
		var custom_week_date = $("#date_field").val();
		if(getUrlParameter('week_view') !== undefined){
			window.location = "index.php?module=sp_Timesheet&action=EditView&return_module=sp_Timesheet&return_action=DetailView&week_start_date="+custom_week_date+"&week_view=true";
		}else{
			window.location = "index.php?module=sp_Timesheet&action=EditView&return_module=sp_Timesheet&return_action=DetailView&week_start_date="+custom_week_date;
		}
	});

	$("input.projects_hours").on('input', function(){
		var class_name = $(this).attr('class');
		class_name = trim(class_name.replace('projects_hours', ''));
		var total = 0;
		$("input."+class_name).each(function(){
			var hours = parseInt($(this).val());
			if(!isNaN(hours)){
				total += hours;
			}
		});

		$("span."+class_name+"_total_hours").text(total);

		var id = $(this).attr('id');
		id = id.split('_');

		var p_total = 0;
		$("span.project_id_"+id[1]).each(function(){
			var hours = parseInt($(this).text());
			if(!isNaN(hours)){
				p_total += hours;
			}
		});

		$("#project_"+id[1]+"_project_hours").text(p_total);

		total=0;
		$("span.total_hours").each(function(){
			var hours = parseInt($(this).text());
			if(!isNaN(hours)){
				total += hours;
			}
		});

		$("span.project_hours").text(total);
	});
	
	var week_start = $("#week_start").val();
	showHideTable(week_start, 0);
});

function showHideTable(date, k_index){
	if(getUrlParameter('week_view') !== undefined){
		return;
	}
	
	var action = getUrlParameter('action');
	if(action == "DetailView"){
		return;
	}
	
	var str = GetFormattedDate(date)
	$("#selected_date").html("<font color='#E56455'><b>"+str+"</b></font>");
	
	for(var hh=0; hh<7; hh++){
		document.getElementById("cstm_date_"+hh).style.backgroundColor = "transparent";
	}
	
	document.getElementById("cstm_date_"+k_index).style.backgroundColor = "yellow";
	
	var table_id = "mytable_"+date;
	
	var json_dates = $("#json_dates").val();
	var json_dates_arr = jQuery.parseJSON(json_dates);
	for(jj=0; jj<json_dates_arr.length; jj++){
		$("#mytable_"+json_dates_arr[jj]).hide();
	}
	
	$("#topLevelButtonsRow").show();
	$("#myTableDates").show();
	$("#"+table_id).show();
}

function GetFormattedDate(date) {
	 var todayTime = new Date(date);
    var month = (todayTime .getMonth() + 1);
    var day = (todayTime .getUTCDate());
    var year = (todayTime .getFullYear());
	var days = ["Sun","Mon","Tue","Wed","Thu","Fri","Sat"];
	var months = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
    var day_name = days[todayTime.getUTCDay()];
    var month_name = months[todayTime.getMonth()];
	return day_name + ", " + day +" "+month_name+", "+year;
}

function getUrlParameter(sParam) {
	// debugger;
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
}

function calculateProjectHours(elem, project_id, date){
	var json_dates = $("#json_dates").val();
	var dates_arr = JSON.parse(json_dates);
	var field_name = '';
	var field_value = 0;
	var sum = 0.0;
	for(var i=0; i<dates_arr.length; i++){
		field_name = "project_"+project_id+"_"+dates_arr[i];
		field_value = $("#"+field_name).val();
		if(field_value == ""){
			field_value = 0;
		}
		sum = sum + parseFloat(field_value);
	}
	$("#sumproject_"+project_id).html(sum);
	
	
	//summation per date
	var per_day_sum = 0;
	$(".class_"+date).each(function(index, element) {
		field_value = element.value;
		if(field_value == ""){
			field_value = 0;
		}
		per_day_sum = per_day_sum + parseFloat(field_value);
	});
	$("#sumallprojects_"+date).html(per_day_sum);
	
	
	//total_hours updation
	var per_day_hours = 0;
	$(".per_day_hours").each(function(index, element) {
		field_value = element.innerHTML; 
		
		if(field_value == ""){
			field_value = 0;
		}
		per_day_hours = per_day_hours + parseFloat(field_value);
	});
	$("#total_hours").html(per_day_hours);
	
	
	
	//project hours updation
	var project_hours = 0;
	$(".project_"+project_id).each(function(index, element) {
		field_value = element.innerHTML; 
		
		if(field_value == ""){
			field_value = 0;
		}
		project_hours = project_hours + parseFloat(field_value);
	});
	$("#projecthours_"+project_id).html(project_hours);
}


function calculateProjectTaskHours(elem, project_id, project_task_id, date){
	var json_dates = $("#json_dates").val();
	var dates_arr = JSON.parse(json_dates);
	var field_name = '';
	var field_value = 0;
	var sum = 0.0;
	for(var i=0; i<dates_arr.length; i++){
		field_name = "projecttask_"+project_id+"_"+project_task_id+"_"+dates_arr[i];
		field_value = $("#"+field_name).val();
		if(field_value == ""){
			field_value = 0;
		}
		sum = sum + parseFloat(field_value);
	}
	$("#sumprojecttask_"+project_id+"_"+project_task_id).html(sum);
	
	//summation per date
	var per_day_sum = 0;
	$(".class_"+date).each(function(index, element) {
		field_value = element.value;
		if(field_value == ""){
			field_value = 0;
		}
		per_day_sum = per_day_sum + parseFloat(field_value);
	});
	$("#sumallprojects_"+date).html(per_day_sum);
	
	//total_hours updation
	var per_day_hours = 0;
	$(".per_day_hours").each(function(index, element) {
		field_value = element.innerHTML; 
		
		if(field_value == ""){
			field_value = 0;
		}
		per_day_hours = per_day_hours + parseFloat(field_value);
	});
	$("#total_hours").html(per_day_hours);
	
	//project hours updation
	var project_hours = 0;
	$(".project_"+project_id).each(function(index, element) {
		console.log(element);
		field_value = element.innerHTML; 
		
		if(field_value == ""){
			field_value = 0;
		}
		project_hours = project_hours + parseFloat(field_value);
	});
	$("#projecthours_"+project_id).html(project_hours);
}