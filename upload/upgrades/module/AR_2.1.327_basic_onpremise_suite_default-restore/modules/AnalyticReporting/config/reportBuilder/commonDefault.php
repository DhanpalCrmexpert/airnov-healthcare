<?php

$moduleName = PlatformConnector::$moduleName;
$rootUrl = PlatformConnector::getUrl();

return  array(
    'moduleName' => $moduleName,
    'rootUrl' => $rootUrl,
    'internal' => array(
        'postUrl' => "index.php?module={$moduleName}&action=reportBuilder&file=reportBuilder",
        'reportUrl' => "{$rootUrl}?module={$moduleName}&action=report&record=",
        'reportEditUrl' => "index.php?module={$moduleName}&action=reportBuilder&parentaction=edit&record="
    ),
    'reportTypes' => array(
        0 => array(
            'name' => 'standard',
            'title' => PlatformConnector::translate('standard')
        ),
        1 => array(
            'name' => 'combined',
            'title' => PlatformConnector::translate('Combined')
        )
    ),
    'relationTypes' => array(
        0 => array(
            'name' => 'oneToMany',
            'title' => PlatformConnector::translate('oneToMany')
        ),
        1 => array(
            'name' => 'manyToMany',
            'title' => PlatformConnector::translate('manyToMany')
        )
    ),
);