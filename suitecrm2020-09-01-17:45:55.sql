-- MySQL dump 10.13  Distrib 5.7.25, for Linux (x86_64)
--
-- Host: localhost    Database: suitecrm
-- ------------------------------------------------------
-- Server version	5.7.25-0ubuntu0.18.10.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `accounts`
--

DROP TABLE IF EXISTS `accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accounts` (
  `id` char(36) NOT NULL,
  `name` varchar(150) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `account_type` varchar(50) DEFAULT NULL,
  `industry` varchar(50) DEFAULT NULL,
  `annual_revenue` varchar(100) DEFAULT NULL,
  `phone_fax` varchar(100) DEFAULT NULL,
  `billing_address_street` varchar(150) DEFAULT NULL,
  `billing_address_city` varchar(100) DEFAULT NULL,
  `billing_address_state` varchar(100) DEFAULT NULL,
  `billing_address_postalcode` varchar(20) DEFAULT NULL,
  `billing_address_country` varchar(255) DEFAULT NULL,
  `rating` varchar(100) DEFAULT NULL,
  `phone_office` varchar(100) DEFAULT NULL,
  `phone_alternate` varchar(100) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `ownership` varchar(100) DEFAULT NULL,
  `employees` varchar(10) DEFAULT NULL,
  `ticker_symbol` varchar(10) DEFAULT NULL,
  `shipping_address_street` varchar(150) DEFAULT NULL,
  `shipping_address_city` varchar(100) DEFAULT NULL,
  `shipping_address_state` varchar(100) DEFAULT NULL,
  `shipping_address_postalcode` varchar(20) DEFAULT NULL,
  `shipping_address_country` varchar(255) DEFAULT NULL,
  `parent_id` char(36) DEFAULT NULL,
  `sic_code` varchar(10) DEFAULT NULL,
  `campaign_id` char(36) DEFAULT NULL,
  `total_hours_at` decimal(18,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_accnt_id_del` (`id`,`deleted`),
  KEY `idx_accnt_name_del` (`name`,`deleted`),
  KEY `idx_accnt_assigned_del` (`deleted`,`assigned_user_id`),
  KEY `idx_accnt_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accounts`
--

LOCK TABLES `accounts` WRITE;
/*!40000 ALTER TABLE `accounts` DISABLE KEYS */;
INSERT INTO `accounts` VALUES ('2978e526-e27b-1ce0-3822-5f46974d0157','Pfizer Global','2020-08-26 17:10:44','2020-08-26 17:18:16','1','1',NULL,0,'b70243be-ca4c-b256-aff8-5f46980acf9f',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'http://',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,'',0.00),('4712d4f2-e7f2-593a-8243-5f469751802e','Pfizer-China','2020-08-26 17:11:02','2020-08-26 17:18:16','1','1',NULL,0,'1674d634-fb3a-8d8b-34c1-5f4698fed125',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'http://',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2978e526-e27b-1ce0-3822-5f46974d0157',NULL,'',0.00),('72cb1f4a-5f55-b910-e731-5e90b810ad4d','CRM Experts Online','2020-04-10 18:19:13','2020-08-28 09:59:53','b70243be-ca4c-b256-aff8-5f46980acf9f','dc26b995-5550-b848-6f73-5e90aa679348','',1,'dc26b995-5550-b848-6f73-5e90aa679348','','','','','','','','','',NULL,'',NULL,'http://',NULL,'',NULL,'','','','','','',NULL,'',0.00),('9ae1967b-3fc4-6421-9d77-5f4697488f9e','Pfizer-India','2020-08-26 17:11:18','2020-08-26 17:18:16','1','1',NULL,0,'8d544213-bcac-b962-fc13-5f4698c75700',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'http://',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2978e526-e27b-1ce0-3822-5f46974d0157',NULL,'',0.00),('c4910e0c-08a6-3d7d-d073-5f3fad5bff46','Pharma Company','2020-08-21 11:18:40','2020-08-28 09:59:53','b70243be-ca4c-b256-aff8-5f46980acf9f','1',NULL,1,'1','','',NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,'http://',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00);
/*!40000 ALTER TABLE `accounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `accounts_audit`
--

DROP TABLE IF EXISTS `accounts_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accounts_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_accounts_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accounts_audit`
--

LOCK TABLES `accounts_audit` WRITE;
/*!40000 ALTER TABLE `accounts_audit` DISABLE KEYS */;
INSERT INTO `accounts_audit` VALUES ('8b969bbf-3e4b-5724-2aeb-5f4698548053','4712d4f2-e7f2-593a-8243-5f469751802e','2020-08-26 17:15:25','1','assigned_user_id','relate','1','1674d634-fb3a-8d8b-34c1-5f4698fed125',NULL,NULL),('97f7c62b-cfa9-6892-cf67-5f46988295ed','9ae1967b-3fc4-6421-9d77-5f4697488f9e','2020-08-26 17:15:06','1','assigned_user_id','relate','1','8d544213-bcac-b962-fc13-5f4698c75700',NULL,NULL),('9df7196c-ad07-89dd-7071-5f46983ec85e','2978e526-e27b-1ce0-3822-5f46974d0157','2020-08-26 17:15:47','1','assigned_user_id','relate','1','b70243be-ca4c-b256-aff8-5f46980acf9f',NULL,NULL),('acd72b5c-5b50-3825-acb6-5f4699e41870','9ae1967b-3fc4-6421-9d77-5f4697488f9e','2020-08-26 17:18:16','1','parent_id','id','','2978e526-e27b-1ce0-3822-5f46974d0157',NULL,NULL),('b8b68f09-0558-46fc-ff9a-5f4699839ac8','4712d4f2-e7f2-593a-8243-5f469751802e','2020-08-26 17:18:16','1','parent_id','id','','2978e526-e27b-1ce0-3822-5f46974d0157',NULL,NULL);
/*!40000 ALTER TABLE `accounts_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `accounts_bugs`
--

DROP TABLE IF EXISTS `accounts_bugs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accounts_bugs` (
  `id` varchar(36) NOT NULL,
  `account_id` varchar(36) DEFAULT NULL,
  `bug_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_acc_bug_acc` (`account_id`),
  KEY `idx_acc_bug_bug` (`bug_id`),
  KEY `idx_account_bug` (`account_id`,`bug_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accounts_bugs`
--

LOCK TABLES `accounts_bugs` WRITE;
/*!40000 ALTER TABLE `accounts_bugs` DISABLE KEYS */;
/*!40000 ALTER TABLE `accounts_bugs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `accounts_cases`
--

DROP TABLE IF EXISTS `accounts_cases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accounts_cases` (
  `id` varchar(36) NOT NULL,
  `account_id` varchar(36) DEFAULT NULL,
  `case_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_acc_case_acc` (`account_id`),
  KEY `idx_acc_acc_case` (`case_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accounts_cases`
--

LOCK TABLES `accounts_cases` WRITE;
/*!40000 ALTER TABLE `accounts_cases` DISABLE KEYS */;
/*!40000 ALTER TABLE `accounts_cases` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `accounts_contacts`
--

DROP TABLE IF EXISTS `accounts_contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accounts_contacts` (
  `id` varchar(36) NOT NULL,
  `contact_id` varchar(36) DEFAULT NULL,
  `account_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_account_contact` (`account_id`,`contact_id`),
  KEY `idx_contid_del_accid` (`contact_id`,`deleted`,`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accounts_contacts`
--

LOCK TABLES `accounts_contacts` WRITE;
/*!40000 ALTER TABLE `accounts_contacts` DISABLE KEYS */;
INSERT INTO `accounts_contacts` VALUES ('3edcdf5d-fc95-7a6e-75f5-5f48dcae15df','3898ea01-ac05-7e9f-4dce-5f48dcaf2e89','4712d4f2-e7f2-593a-8243-5f469751802e','2020-08-28 10:30:41',0),('54534fe3-9e81-da67-5f2e-5f44f8a165ed','4da4191b-796e-24f8-313a-5f44f8418e64','c4910e0c-08a6-3d7d-d073-5f3fad5bff46','2020-08-28 09:59:53',1),('56f138ac-4a88-b872-d02d-5f48da4ad055','5200556a-9070-b927-1928-5f48da876d80','9ae1967b-3fc4-6421-9d77-5f4697488f9e','2020-08-28 10:23:31',0);
/*!40000 ALTER TABLE `accounts_contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `accounts_cstm`
--

DROP TABLE IF EXISTS `accounts_cstm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accounts_cstm` (
  `id_c` char(36) NOT NULL,
  `jjwg_maps_lng_c` float(11,8) DEFAULT '0.00000000',
  `jjwg_maps_lat_c` float(10,8) DEFAULT '0.00000000',
  `jjwg_maps_geocode_status_c` varchar(255) DEFAULT NULL,
  `jjwg_maps_address_c` varchar(255) DEFAULT NULL,
  `npr_c` varchar(100) DEFAULT NULL,
  `nda_c` varchar(100) DEFAULT NULL,
  `sales_full_year_c` float(18,2) DEFAULT NULL,
  `success_rate_c` float(18,2) DEFAULT NULL,
  `potential_launch_c` date DEFAULT NULL,
  `production_site_c` varchar(255) DEFAULT NULL,
  `potential_impact_budget_ly_c` float(18,2) DEFAULT NULL,
  `potential_impact_budget_20_c` float(18,2) DEFAULT NULL,
  `potential_impact_budget_21_c` float(18,2) DEFAULT NULL,
  `projects_won_full_yr_c` float(18,2) DEFAULT NULL,
  PRIMARY KEY (`id_c`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accounts_cstm`
--

LOCK TABLES `accounts_cstm` WRITE;
/*!40000 ALTER TABLE `accounts_cstm` DISABLE KEYS */;
INSERT INTO `accounts_cstm` VALUES ('2978e526-e27b-1ce0-3822-5f46974d0157',0.00000000,0.00000000,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('4712d4f2-e7f2-593a-8243-5f469751802e',0.00000000,0.00000000,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('72cb1f4a-5f55-b910-e731-5e90b810ad4d',0.00000000,0.00000000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('9ae1967b-3fc4-6421-9d77-5f4697488f9e',0.00000000,0.00000000,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('c4910e0c-08a6-3d7d-d073-5f3fad5bff46',0.00000000,0.00000000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `accounts_cstm` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `accounts_opportunities`
--

DROP TABLE IF EXISTS `accounts_opportunities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accounts_opportunities` (
  `id` varchar(36) NOT NULL,
  `opportunity_id` varchar(36) DEFAULT NULL,
  `account_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_account_opportunity` (`account_id`,`opportunity_id`),
  KEY `idx_oppid_del_accid` (`opportunity_id`,`deleted`,`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accounts_opportunities`
--

LOCK TABLES `accounts_opportunities` WRITE;
/*!40000 ALTER TABLE `accounts_opportunities` DISABLE KEYS */;
INSERT INTO `accounts_opportunities` VALUES ('134c7112-f24f-b7e7-64f8-5f48db6a3314','12c3340b-0f62-8d28-b800-5f48db0951f9','9ae1967b-3fc4-6421-9d77-5f4697488f9e','2020-08-28 10:25:25',0),('1786e499-a42b-f6bf-adfd-5f3fad57632c','170ab7e4-7129-fd99-34c8-5f3fad24bc03','c4910e0c-08a6-3d7d-d073-5f3fad5bff46','2020-08-28 09:59:53',1),('1f59f3fb-8017-6816-4304-5f48dde5810e','1ee05743-c112-d3a1-163a-5f48dd532edf','4712d4f2-e7f2-593a-8243-5f469751802e','2020-08-28 10:31:57',0),('4c84ef0e-d86e-8871-6727-5f43bea11ef9','4c12f4da-751b-5429-80ea-5f43bed3071f','c4910e0c-08a6-3d7d-d073-5f3fad5bff46','2020-08-28 09:59:53',1),('693dd32b-1d52-347c-c0f5-5f48daf6dc7d','68b8f5a5-cd40-decc-cebb-5f48da51a3f2','9ae1967b-3fc4-6421-9d77-5f4697488f9e','2020-08-28 10:20:11',0),('96112b8a-bcdf-1786-9f10-5f43bcc16998','9583611d-7521-f3a8-6bcd-5f43bc7dfab0','c4910e0c-08a6-3d7d-d073-5f3fad5bff46','2020-08-28 09:59:53',1),('a60c9005-2bb8-a17b-227f-5f48ddf2ec3d','a57b6fdf-beda-0f2f-164f-5f48dd72c10f','4712d4f2-e7f2-593a-8243-5f469751802e','2020-08-28 10:32:19',0),('ac4ae19f-3229-7eab-4a6c-5f3fad92e831','ab99004a-eee8-20c6-f636-5f3fad54c0d0','c4910e0c-08a6-3d7d-d073-5f3fad5bff46','2020-08-28 09:59:53',1),('d8fd0dcb-dd2b-2327-798a-5f44fa5d2296','cf6e07ce-3763-a98c-69d1-5f44fa63cfd9','72cb1f4a-5f55-b910-e731-5e90b810ad4d','2020-08-28 09:59:53',1),('f18e8ff0-e1c9-7c7a-3e31-5f43bca4dcd4','f10d6a43-2a2f-d743-d2ce-5f43bca15ede','c4910e0c-08a6-3d7d-d073-5f3fad5bff46','2020-08-28 09:59:53',1);
/*!40000 ALTER TABLE `accounts_opportunities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `accounts_sp_timesheet_tracker_1_c`
--

DROP TABLE IF EXISTS `accounts_sp_timesheet_tracker_1_c`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accounts_sp_timesheet_tracker_1_c` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `accounts_sp_timesheet_tracker_1accounts_ida` varchar(36) DEFAULT NULL,
  `accounts_sp_timesheet_tracker_1sp_timesheet_tracker_idb` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `accounts_sp_timesheet_tracker_1_ida1` (`accounts_sp_timesheet_tracker_1accounts_ida`),
  KEY `accounts_sp_timesheet_tracker_1_alt` (`accounts_sp_timesheet_tracker_1sp_timesheet_tracker_idb`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accounts_sp_timesheet_tracker_1_c`
--

LOCK TABLES `accounts_sp_timesheet_tracker_1_c` WRITE;
/*!40000 ALTER TABLE `accounts_sp_timesheet_tracker_1_c` DISABLE KEYS */;
INSERT INTO `accounts_sp_timesheet_tracker_1_c` VALUES ('51441165-814d-2ee6-ff19-5e90b8facbe4','2020-04-10 18:17:40',0,'','5082566c-5d34-5b12-bca7-5e90b8f556d4'),('51e565d4-d81e-80fb-9222-5e90b83aee39','2020-04-10 18:17:40',0,'','51978fe1-1bba-14ff-37f4-5e90b8db54c4');
/*!40000 ALTER TABLE `accounts_sp_timesheet_tracker_1_c` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `acl_actions`
--

DROP TABLE IF EXISTS `acl_actions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acl_actions` (
  `id` char(36) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `name` varchar(150) DEFAULT NULL,
  `category` varchar(100) DEFAULT NULL,
  `acltype` varchar(100) DEFAULT NULL,
  `aclaccess` int(3) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_aclaction_id_del` (`id`,`deleted`),
  KEY `idx_category_name` (`category`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acl_actions`
--

LOCK TABLES `acl_actions` WRITE;
/*!40000 ALTER TABLE `acl_actions` DISABLE KEYS */;
INSERT INTO `acl_actions` VALUES ('101ad241-f96c-8797-b838-5c6e1df463e2','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','view','AOS_Contracts','module',90,0),('10849107-d2b4-4ba1-4f7e-5c6e1dd0ec65','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','export','AM_ProjectTemplates','module',90,0),('10f6b83e-e87a-61fb-d12a-5c6e1dffd363','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','export','Opportunities','module',90,0),('114e35ef-a176-57a6-cbe4-5c6e1dde9572','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','import','jjwg_Address_Cache','module',90,0),('11e2c447-3cdb-c380-663b-5c6e1dbbbdef','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','list','AOS_Contracts','module',90,0),('121aa5d9-ed75-7bb0-a8c8-5c6e1de8a86a','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','massupdate','AM_ProjectTemplates','module',90,0),('12a22378-ce15-c2f0-402b-5c6e1d224f96','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','massupdate','Opportunities','module',90,0),('1312db1d-35b4-46f1-10e5-5c6e1d15c98e','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','export','jjwg_Address_Cache','module',90,0),('13c35bd9-edd9-8d1d-9148-5c6e1dd73cbe','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','edit','AOS_Contracts','module',90,0),('14edf037-2af6-6859-18f0-5c6e1d9ae20a','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','massupdate','jjwg_Address_Cache','module',90,0),('15c2ddf5-d59a-f00b-e3fd-5c6e1dd8862b','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','delete','AOS_Contracts','module',90,0),('17825d0b-9ae5-f818-8af6-5c6e1d44db2b','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','import','AOS_Contracts','module',90,0),('181a0df8-d32e-3a78-63b5-5c6e1db02bf9','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','massupdate','jjwg_Areas','module',90,0),('184cafd9-5da5-233b-61b2-5c6e1dcc1ae2','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','access','AM_TaskTemplates','module',89,0),('195d5fd1-dcbe-f8c7-791b-5c6e1d95e515','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','export','AOS_Contracts','module',90,0),('19e5dfc5-63d8-d00f-4a5c-5c6e1de57f20','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','view','AM_TaskTemplates','module',90,0),('1b120fe0-ce1d-c39b-038c-5c6e1d886a54','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','massupdate','AOS_Contracts','module',90,0),('1b163909-2912-40f9-8708-5c6e1dad4ee9','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','access','EmailTemplates','module',89,0),('1b7cf5f1-f8ac-d35a-0532-5c6e1d6bd505','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','list','AM_TaskTemplates','module',90,0),('1b958cf9-ae40-438b-f8c5-5c6e1da93153','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','access','Calls_Reschedule','module',89,0),('1cd1d38b-fce9-a981-1846-5c6e1d132836','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','view','EmailTemplates','module',90,0),('1d2877da-72f4-f0e6-a6bc-5c6e1da8da72','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','edit','AM_TaskTemplates','module',90,0),('1d94faaf-6dae-f882-8d0e-5c6e1dee4687','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','view','Calls_Reschedule','module',90,0),('1e6a83a2-afae-db99-ec4f-5c6e1d752ed9','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','list','EmailTemplates','module',90,0),('1eca7007-450c-966f-c0f1-5c6e1d074aee','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','delete','AM_TaskTemplates','module',90,0),('1f915e87-b617-1952-6ea3-5c6e1d890550','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','list','Calls_Reschedule','module',90,0),('200a1c61-6924-8cd8-5af7-5c6e1d640527','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','edit','EmailTemplates','module',90,0),('206ab75e-6ea7-cf2a-57ea-5c6e1d9ebd40','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','import','AM_TaskTemplates','module',90,0),('211e0ddd-c042-9ae3-e81f-5c6e1d5431d7','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','edit','AOR_Scheduled_Reports','module',90,0),('2169ee86-765e-bd5e-d6ca-5c6e1d9ae49c','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','edit','Calls_Reschedule','module',90,0),('21a4b930-240a-495d-674b-5c6e1da71eb7','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','delete','EmailTemplates','module',90,0),('22996860-ab57-6633-73f6-5c6e1d5dcb18','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','export','AM_TaskTemplates','module',90,0),('232e2ca6-d75b-da80-9dfe-5c6e1de86dd1','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','delete','Calls_Reschedule','module',90,0),('233a9995-db1a-2078-e61f-5c6e1df9a25d','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','import','EmailTemplates','module',90,0),('23909eed-24e9-1383-65a6-5c6e1d4dc04a','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','access','AOS_Invoices','module',89,0),('245a684c-7f66-e44a-a4c6-5c6e1dfe0a22','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','massupdate','AM_TaskTemplates','module',90,0),('24d68f78-a84f-a13d-7f4a-5c6e1d934bac','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','export','EmailTemplates','module',90,0),('2507c934-350d-93e7-6009-5c6e1d1a4ebf','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','import','Calls_Reschedule','module',90,0),('255f689c-30fa-9743-6b97-5c6e1d16820d','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','view','AOS_Invoices','module',90,0),('26783917-465d-4805-eb95-5c6e1d251635','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','massupdate','EmailTemplates','module',90,0),('26f60559-7804-b3e2-2c5a-5c6e1d08216c','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','export','Calls_Reschedule','module',90,0),('273511c0-4119-a9e5-de4c-5c6e1d59d6dc','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','list','AOS_Invoices','module',90,0),('28140bad-21e3-58b2-5adf-5c6e1d2a37ec','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','edit','Cases','module',90,0),('284d5d27-c0b6-9f4a-7f21-5e909d653e5d','2020-04-10 16:25:28','2020-04-10 16:25:28','1','1','access','sp_Timesheet','module',89,0),('28ff06fc-14b0-98ec-737f-5c6e1d816cc7','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','massupdate','Calls_Reschedule','module',90,0),('2add29bf-94df-524f-816b-5c6e1df26606','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','access','Bugs','module',89,0),('2ae367b2-742d-5ab8-1ea2-5c6e1db4adef','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','edit','AOS_Invoices','module',90,0),('2bb8a788-d9b2-317e-ee27-5c6e1d99e1e0','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','access','Notes','module',89,0),('2c76062e-78dc-b6f6-36df-5c6e1d53d9e3','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','view','Bugs','module',90,0),('2cf32cca-b9b9-409b-bd3e-5c6e1dfd8c3b','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','delete','AOS_Invoices','module',90,0),('2d4c98ca-5d2e-6f0a-17fc-5c6e1d9fd7b6','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','access','AOK_Knowledge_Base_Categories','module',89,0),('2e4a26b7-6b7b-55c2-11db-5c6e1d2fe155','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','list','Bugs','module',90,0),('2ebfecbf-817e-c0f6-494c-5c6e1d375b9d','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','import','AOS_Invoices','module',90,0),('2efef55f-d14c-224a-1cc6-5c6e1d1d53cf','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','view','AOK_Knowledge_Base_Categories','module',90,0),('2f3392ef-1a9a-f99d-c414-5c6e1d868dc9','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','view','Notes','module',90,0),('304550da-9a68-1c5d-70b9-5c6e1d0308c6','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','edit','Bugs','module',90,0),('30b24203-0540-da78-baa4-5c6e1dd6ddce','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','export','AOS_Invoices','module',90,0),('30d76631-82e8-b7fe-ddfa-5c6e1dd9abc2','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','access','SecurityGroups','module',89,0),('31872bf6-c690-3e22-7268-5e909d387954','2020-04-10 16:25:28','2020-04-10 16:25:28','1','1','view','sp_Timesheet','module',90,0),('31c44a2d-47fa-fb4f-0b01-5c6e1d201232','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','list','Notes','module',90,0),('31cac616-99da-efcf-aa0f-5c6e1d3afd35','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','list','AOK_Knowledge_Base_Categories','module',90,0),('31f99891-140f-0f15-8210-5c6e1da1c586','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','delete','Bugs','module',90,0),('3273e589-c6a6-c63a-53fc-5c6e1d017b90','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','massupdate','AOS_Invoices','module',90,0),('32e6c274-6c87-2818-4db5-5c6e1dfe853d','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','view','SecurityGroups','module',90,0),('337b8214-09d6-962e-b12b-5c6e1deefec8','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','edit','AOK_Knowledge_Base_Categories','module',90,0),('338337d8-861f-20ab-eb8a-5c6e1d0794c7','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','edit','Notes','module',90,0),('33977c10-585e-e3e4-76b3-5c6e1d29503c','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','import','Bugs','module',90,0),('347e56bb-e0e3-1619-1016-5e909d1a0b25','2020-04-10 16:25:28','2020-04-10 16:25:28','1','1','list','sp_Timesheet','module',90,0),('34d441bb-0fe8-5906-d567-5c6e1dff5aef','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','list','SecurityGroups','module',90,0),('3516982c-ed8e-543f-3813-5c6e1d0bdfbd','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','delete','AOK_Knowledge_Base_Categories','module',90,0),('352832c1-13a4-6df3-47b0-5c6e1d95364b','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','export','Bugs','module',90,0),('35351234-40a5-72c4-4de3-5c6e1dacbb2e','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','delete','Notes','module',90,0),('36b63ee1-ab68-5783-63da-5c6e1de6b4e5','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','import','AOK_Knowledge_Base_Categories','module',90,0),('36b9e4fa-f86d-727e-fca7-5c6e1de8e18d','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','massupdate','Bugs','module',90,0),('36bc1d33-71d2-bb7a-1bb4-5c6e1dcc38c1','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','edit','SecurityGroups','module',90,0),('36e993eb-3592-761c-b5b0-5c6e1d8e0760','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','import','Notes','module',90,0),('3760e1ef-f22f-8223-a180-5e909d815c59','2020-04-10 16:25:28','2020-04-10 16:25:28','1','1','edit','sp_Timesheet','module',90,0),('3852582d-e33f-175c-76b2-5c6e1d893051','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','export','AOK_Knowledge_Base_Categories','module',90,0),('3889a69a-c00b-8f99-585b-5c6e1d8b38cb','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','export','Notes','module',90,0),('388d4a70-5bce-7181-208f-5c6e1d88cade','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','delete','SecurityGroups','module',90,0),('392b4520-bc4a-0b1f-059f-5c6e1d653b57','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','access','AOS_PDF_Templates','module',89,0),('39f45967-6ace-927e-9b01-5c6e1daa3eda','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','massupdate','AOK_Knowledge_Base_Categories','module',90,0),('3a22e344-99f3-e402-1503-5c6e1daeca94','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','massupdate','Notes','module',90,0),('3a77715d-b123-819f-19ae-5c6e1db13f47','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','import','SecurityGroups','module',90,0),('3a88b4c8-dd0b-3a95-43b1-5e909d542ac7','2020-04-10 16:25:28','2020-04-10 16:25:28','1','1','delete','sp_Timesheet','module',90,0),('3acf69d8-9b59-caed-5eb9-5c6e1d95d81a','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','view','AOS_PDF_Templates','module',90,0),('3c5a66b1-961d-620d-103f-5c6e1d689c1c','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','export','SecurityGroups','module',90,0),('3c6faf0b-9351-4b5a-34f1-5c6e1d899517','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','list','AOS_PDF_Templates','module',90,0),('3d3b556a-48b1-9d71-2a7b-5e909d062e9e','2020-04-10 16:25:28','2020-04-10 16:25:28','1','1','import','sp_Timesheet','module',90,0),('3e0a8d82-3855-407e-0ac4-5c6e1d0cdf9f','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','edit','AOS_PDF_Templates','module',90,0),('3e4fc88a-0fb7-4a6e-935e-5c6e1da1cf38','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','massupdate','SecurityGroups','module',90,0),('3ebe26a0-aa22-85c5-8f28-5c6e1d664cfd','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','access','Users','module',89,0),('3ef5062d-c353-a08c-c667-5c6e1d0d53b7','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','delete','AOR_Scheduled_Reports','module',90,0),('3fa48fbe-c3d1-67ba-7543-5c6e1d64f949','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','delete','AOS_PDF_Templates','module',90,0),('3fec8550-d616-39f3-2c24-5e909da3cebb','2020-04-10 16:25:28','2020-04-10 16:25:28','1','1','export','sp_Timesheet','module',90,0),('4052b0e5-3814-2fd9-051c-5c6e1db62368','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','view','Users','module',90,0),('4096a9d2-bd4a-eb01-973f-5c6e1d5a45e2','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','access','AOK_KnowledgeBase','module',89,0),('40a52983-5401-23cb-9769-5c6e1dc74de0','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','access','Calls','module',89,0),('413eb557-390e-1809-3fee-5c6e1db396b6','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','import','AOS_PDF_Templates','module',90,0),('419b0ff8-8bd3-c967-1f1c-5c6e1d4e8fb9','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','delete','Cases','module',90,0),('41e32f04-5c86-5130-3777-5c6e1d91ce09','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','list','Users','module',90,0),('42416112-83ca-03b7-352a-5c6e1db0f5e5','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','view','AOK_KnowledgeBase','module',90,0),('42465f76-4843-8654-22a2-5c6e1db7e678','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','view','Calls','module',90,0),('42972982-baf6-fdcd-9660-5e909d9bae21','2020-04-10 16:25:28','2020-04-10 16:25:28','1','1','massupdate','sp_Timesheet','module',90,0),('42dbf3b7-2439-13c1-da55-5c6e1dd94e7d','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','export','AOS_PDF_Templates','module',90,0),('437149d8-c479-f569-bd43-5c6e1d1d6f54','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','edit','Users','module',90,0),('43dcc714-a359-b934-1906-5c6e1d8e6d82','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','list','Calls','module',90,0),('43e09d57-a48f-6375-1331-5c6e1d87f59a','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','list','AOK_KnowledgeBase','module',90,0),('447e4027-4eb0-7267-7547-5c6e1dda96aa','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','massupdate','AOS_PDF_Templates','module',90,0),('450414ac-abd5-18db-0e32-5c6e1df10736','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','delete','Users','module',90,0),('453b3704-5824-5632-462a-5c6e1dce0981','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','access','OutboundEmailAccounts','module',89,0),('4574d08f-6962-323a-85a8-5c6e1d5971c5','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','edit','Calls','module',90,0),('458a5983-6996-5853-fe72-5c6e1d222fc5','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','edit','AOK_KnowledgeBase','module',90,0),('4697bda0-e534-262f-c9f7-5c6e1d21fb85','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','import','Users','module',90,0),('471273a9-50cb-30b6-1772-5c6e1d4750af','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','delete','Calls','module',90,0),('47288bba-4c05-a22a-8c63-5c6e1d3dc05c','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','delete','AOK_KnowledgeBase','module',90,0),('4829f284-53a8-eb57-fc9d-5c6e1dbadee1','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','export','Users','module',90,0),('48bad485-ae5a-7893-e9d9-5c6e1df2102e','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','import','Calls','module',90,0),('48c6d617-d7a0-6fd8-c0a4-5c6e1d8a861c','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','import','AOK_KnowledgeBase','module',90,0),('49bec4c8-184e-d717-7f17-5c6e1d4fa325','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','massupdate','Users','module',90,0),('4a57e684-d9e3-d7f2-83fd-5c6e1d1dc552','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','export','Calls','module',90,0),('4a657e01-e916-f669-a099-5c6e1d1e5567','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','export','AOK_KnowledgeBase','module',90,0),('4abd9300-75b3-c2d2-685e-5c6e1df785c7','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','access','AOS_Product_Categories','module',89,0),('4b17887f-a8cf-69f1-d0d3-5e909dc73ad9','2020-04-10 16:25:28','2020-04-10 16:25:28','1','1','access','sp_timesheet_tracker','module',89,0),('4bf15a14-a85b-47a9-8349-5c6e1d3aa8fe','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','massupdate','Calls','module',90,0),('4c0a451b-4439-f8d8-2592-5c6e1d1fe047','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','massupdate','AOK_KnowledgeBase','module',90,0),('4c5e89e7-59ec-82d0-2835-5c6e1d1e856d','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','view','AOS_Product_Categories','module',90,0),('4da6d5e4-5f2a-058f-67a4-5e909dfafc91','2020-04-10 16:25:28','2020-04-10 16:25:28','1','1','view','sp_timesheet_tracker','module',90,0),('4dff9ed3-fe53-4b2e-38fc-5c6e1d318481','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','list','AOS_Product_Categories','module',90,0),('4e23c1ff-9369-80c8-9296-5c6e1d9eda74','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','view','OutboundEmailAccounts','module',90,0),('4fa28488-be59-b43c-3a3f-5c6e1de2faca','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','edit','AOS_Product_Categories','module',90,0),('502a38bf-fe4f-4c7c-33b2-5c6e1d7254d0','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','list','OutboundEmailAccounts','module',90,0),('505e3bdd-18eb-ba6c-5ce8-5e909d8c96bf','2020-04-10 16:25:28','2020-04-10 16:25:28','1','1','list','sp_timesheet_tracker','module',90,0),('51468e5a-b69b-bd8d-77b6-5c6e1d52286e','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','delete','AOS_Product_Categories','module',90,0),('51900e3d-f193-7048-426c-5c6e1dfe209e','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','list','AOR_Scheduled_Reports','module',90,0),('5204b668-b7ff-ccdd-8e4d-5c6e1d632d90','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','edit','OutboundEmailAccounts','module',90,0),('52c2ed19-3cfd-a2a8-3cce-5c6e1d04c3d8','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','access','Emails','module',89,0),('52e9af88-fc3a-2da4-8afd-5c6e1d7802ed','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','import','AOS_Product_Categories','module',90,0),('5312fe14-8b77-db19-15f8-5e909df9f318','2020-04-10 16:25:28','2020-04-10 16:25:28','1','1','edit','sp_timesheet_tracker','module',90,0),('53dd2dcb-b198-1a9c-6c63-5c6e1d958589','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','delete','OutboundEmailAccounts','module',90,0),('54651337-2a36-a3e0-cc3f-5c6e1dd7ff25','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','view','Emails','module',90,0),('5470bd94-dff3-8a95-720a-5c6e1dbd7da5','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','access','Project','module',89,0),('54928ee5-4276-ffbe-fdb1-5c6e1dc20efa','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','export','AOS_Product_Categories','module',90,0),('55b878d0-2944-bdc5-2d7a-5c6e1d195b81','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','import','OutboundEmailAccounts','module',90,0),('55d445e5-2ef1-8430-5d2f-5e909deca916','2020-04-10 16:25:28','2020-04-10 16:25:28','1','1','delete','sp_timesheet_tracker','module',90,0),('5603f923-25a8-fdd2-328c-5c6e1de852fd','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','list','Emails','module',90,0),('5622f630-b2c6-dffe-a454-5c6e1db76a36','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','view','Project','module',90,0),('5638e0fd-5245-0cd9-ce3a-5c6e1de3594b','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','massupdate','AOS_Product_Categories','module',90,0),('5789c7f2-cad5-5b8c-4353-5c6e1d5345eb','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','export','OutboundEmailAccounts','module',90,0),('57a40c89-305e-8dc2-31a9-5c6e1d9922d3','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','edit','Emails','module',90,0),('57ad2510-4b44-9372-fe6f-5c6e1d08759e','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','list','Project','module',90,0),('589d9e81-c2f0-7bd2-c4a6-5e909d891f4d','2020-04-10 16:25:28','2020-04-10 16:25:28','1','1','import','sp_timesheet_tracker','module',90,0),('593a5e01-2184-6ac9-0aef-5c6e1d7818e2','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','edit','Project','module',90,0),('593e1378-f97b-72e9-4a72-5c6e1d9089b4','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','delete','Emails','module',90,0),('5962e588-9521-bc29-9540-5c6e1d63b93c','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','massupdate','OutboundEmailAccounts','module',90,0),('5a116d0a-93ce-3f38-6b3c-5c6e1ddfb588','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','access','FP_events','module',89,0),('5acb056c-34b1-a7a7-b1f6-5c6e1dee52c4','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','delete','Project','module',90,0),('5ad89177-2751-9788-e516-5c6e1d29c5ae','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','import','Emails','module',90,0),('5b420296-9b9b-f422-444c-5c6e1d94f411','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','import','Cases','module',90,0),('5b4ca0aa-8bd6-4cea-7241-5e909d8e5d85','2020-04-10 16:25:28','2020-04-10 16:25:28','1','1','export','sp_timesheet_tracker','module',90,0),('5bbba79c-a0f1-e71e-ea73-5c6e1da0a9e2','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','view','FP_events','module',90,0),('5c565cf0-d6e3-4173-5d2d-5c6e1d926139','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','import','Project','module',90,0),('5c7c1f01-024e-a89d-9299-5c6e1d9b9c91','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','export','Emails','module',90,0),('5d297527-1f3f-14f3-d10f-5c6e1d048547','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','access','AOS_Products','module',89,0),('5d500441-9b55-3adc-8e5a-5c6e1de5ab45','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','import','AOR_Scheduled_Reports','module',90,0),('5d5e2201-0772-eea0-a538-5c6e1d9fbe40','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','list','FP_events','module',90,0),('5de87979-2cf3-acb0-b065-5c6e1dfdc2c3','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','export','Project','module',90,0),('5e1b71ea-7c87-c530-779a-5c6e1d9e0d17','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','massupdate','Emails','module',90,0),('5e3c3e09-2a8c-bc08-0096-5e909dd501e1','2020-04-10 16:25:28','2020-04-10 16:25:28','1','1','massupdate','sp_timesheet_tracker','module',90,0),('5ed3bcca-6841-1c42-dec8-5c6e1dcd1654','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','view','AOS_Products','module',90,0),('5efb35f8-4d02-ff48-a0a1-5c6e1d8bd05a','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','edit','FP_events','module',90,0),('5f7ff9db-59ef-9e32-9104-5c6e1d3ee0bf','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','massupdate','Project','module',90,0),('601fdecf-a626-35b3-9142-5c6e1d3b2ee3','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','access','TemplateSectionLine','module',89,0),('6080d88e-9d8f-594a-949c-5c6e1de8ce23','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','list','AOS_Products','module',90,0),('60a6e40c-cd7c-6300-f332-5c6e1df4ba1d','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','delete','FP_events','module',90,0),('61ea6896-ce86-401e-d0bf-5c6e1d0f725e','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','view','TemplateSectionLine','module',90,0),('623518f3-d355-4c06-6dbd-5c6e1dc6f618','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','edit','AOS_Products','module',90,0),('6280ad76-9e85-2b59-5565-5c6e1daf4e8d','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','import','FP_events','module',90,0),('63a5cabe-966f-2e06-25f3-5c6e1d6ba561','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','list','TemplateSectionLine','module',90,0),('640483b3-6ace-7d25-28bc-5c6e1d09e2f4','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','delete','AOS_Products','module',90,0),('6431abe6-fb9d-097a-5dc9-5c6e1d9b476d','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','export','FP_events','module',90,0),('652163fe-3269-b1f4-9e3f-5c6e1d37dbcf','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','access','ProjectTask','module',89,0),('65632a8e-c384-0ba2-6405-5c6e1d997e7f','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','edit','TemplateSectionLine','module',90,0),('65807232-4fc2-aad5-52f8-5c6e1d60ad29','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','access','Meetings','module',89,0),('65c01214-075a-23ad-1a49-5c6e1d0c7711','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','import','AOS_Products','module',90,0),('660bd7c0-0c8f-0395-4bc7-5c6e1d57fdbb','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','massupdate','FP_events','module',90,0),('663299bc-dcf9-c549-4fa0-5e909d628646','2020-04-10 16:25:28','2020-04-10 16:25:28','1','1','access','sp_Auto_TimeSheet_Time_Tracker','module',89,0),('66c644d2-69ae-d214-fe80-5c6e1d9aafb1','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','view','ProjectTask','module',90,0),('6722c508-1381-f2ac-df11-5c6e1d531d47','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','delete','TemplateSectionLine','module',90,0),('672c3b21-fc37-cdc7-6c9b-5c6e1d56963c','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','view','Meetings','module',90,0),('67729b81-c610-e9b5-6626-5c6e1d6a9701','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','export','AOS_Products','module',90,0),('68556931-3a69-e3fa-3a95-5c6e1d28b747','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','list','ProjectTask','module',90,0),('68b60ce8-53e9-437d-7bbf-5c6e1d416598','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','access','Opportunities','module',89,0),('68c1bad8-f8ac-08db-960c-5e909d713286','2020-04-10 16:25:28','2020-04-10 16:25:28','1','1','view','sp_Auto_TimeSheet_Time_Tracker','module',90,0),('68c51dc8-b17b-d512-dec0-5c6e1dfbb5b2','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','list','Meetings','module',90,0),('68ee4e88-d2a3-9e0f-ea4a-5c6e1dc8db73','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','import','TemplateSectionLine','module',90,0),('6929e76d-97ed-b6c4-d138-5c6e1d2fa293','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','massupdate','AOS_Products','module',90,0),('69ea34b6-878a-0c7f-7878-5c6e1d97c3e4','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','edit','ProjectTask','module',90,0),('6a6a210a-9b29-a92b-5117-5c6e1d3defe1','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','edit','Meetings','module',90,0),('6adabb34-e2ff-75ee-8f62-5c6e1d00e805','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','export','TemplateSectionLine','module',90,0),('6b4e4fdc-6375-5146-7e8c-5e909d304fe3','2020-04-10 16:25:28','2020-04-10 16:25:28','1','1','list','sp_Auto_TimeSheet_Time_Tracker','module',90,0),('6b7c675b-97bd-5d96-6453-5c6e1d4f434a','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','delete','ProjectTask','module',90,0),('6c07fded-2f91-dcfd-0a5c-5c6e1d1e158f','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','delete','Meetings','module',90,0),('6ca5fca3-9719-e9bb-0913-5c6e1dc4e355','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','access','FP_Event_Locations','module',89,0),('6ce608cd-b55d-2703-a9c3-5c6e1d11ce48','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','access','AM_ProjectTemplates','module',89,0),('6d2d4783-4ec1-d9fc-0c70-5c6e1d1755c6','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','import','ProjectTask','module',90,0),('6da34898-2906-0ca9-4d9f-5c6e1d4a8e11','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','import','Meetings','module',90,0),('6dc4083a-2d47-470b-8335-5c6e1d46b191','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','massupdate','TemplateSectionLine','module',90,0),('6e16736e-c057-92a9-2ae0-5e909d12be7e','2020-04-10 16:25:28','2020-04-10 16:25:28','1','1','edit','sp_Auto_TimeSheet_Time_Tracker','module',90,0),('6e74df3c-97d7-ff5a-e257-5c6e1d89cdcf','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','view','FP_Event_Locations','module',90,0),('6ecb3a28-bd02-04f4-879d-5c6e1dccaeb9','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','export','ProjectTask','module',90,0),('6f3ef736-d1b8-d29a-31bd-5c6e1d45d1ce','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','export','Meetings','module',90,0),('7046a698-1203-22a6-6d4f-5c6e1d2fe533','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','list','FP_Event_Locations','module',90,0),('705b169d-be44-c4b1-1ded-5c6e1debbb34','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','massupdate','ProjectTask','module',90,0),('70c3a57c-fbcd-1c89-5375-5e909d38029e','2020-04-10 16:25:28','2020-04-10 16:25:28','1','1','delete','sp_Auto_TimeSheet_Time_Tracker','module',90,0),('70d8f01c-fd25-458c-37da-5c6e1de3ee6d','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','massupdate','Meetings','module',90,0),('721fe94f-3d15-b317-6350-5c6e1d44518e','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','edit','FP_Event_Locations','module',90,0),('7369c6c7-8a9a-947c-561e-5e909df4b40a','2020-04-10 16:25:28','2020-04-10 16:25:28','1','1','import','sp_Auto_TimeSheet_Time_Tracker','module',90,0),('746106cb-0c40-293e-cdbe-5c6e1deb825e','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','export','Cases','module',90,0),('74ebf998-0a54-6f0f-4a0c-5c6e1d95510e','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','delete','FP_Event_Locations','module',90,0),('76082a27-4de7-0e8c-58af-5e909d3f3bb7','2020-04-10 16:25:28','2020-04-10 16:25:28','1','1','export','sp_Auto_TimeSheet_Time_Tracker','module',90,0),('76c626c4-b247-8bde-6d8b-5c6e1d204ff2','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','access','Tasks','module',89,0),('77a70590-eb9e-e4e2-e99a-5c6e1df5b66c','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','import','FP_Event_Locations','module',90,0),('78628393-5890-71b9-4644-5c6e1dbb5160','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','view','Tasks','module',90,0),('78a44655-6d0b-4769-8398-5e909de2f14a','2020-04-10 16:25:28','2020-04-10 16:25:28','1','1','massupdate','sp_Auto_TimeSheet_Time_Tracker','module',90,0),('78bc3e90-a6f2-9b33-2c95-5c6e1d8c1327','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','access','Campaigns','module',89,0),('7972c1cc-203b-4783-9cee-5c6e1d68a18e','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','export','FP_Event_Locations','module',90,0),('79f90248-2bd9-8082-77c7-5c6e1d3272ae','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','export','AOR_Scheduled_Reports','module',90,0),('7a0c717c-743b-9dff-0e33-5c6e1d5c14d4','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','access','SurveyResponses','module',89,0),('7a370c0c-ffdd-d739-8f39-5c6e1dccc3c0','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','list','Tasks','module',90,0),('7a597959-caf4-8477-c2f8-5c6e1d6c7ccd','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','view','Campaigns','module',90,0),('7aac0b6d-7b37-a210-a863-5c6e1dfd72eb','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','access','AOS_Quotes','module',89,0),('7b49e17b-7855-cfbf-fe80-5c6e1d2f9182','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','massupdate','FP_Event_Locations','module',90,0),('7bcaac3e-0c7f-302d-e870-5c6e1dfb18f0','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','view','SurveyResponses','module',90,0),('7bd8bdff-cee3-fb8f-84c6-5c6e1dcd0834','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','edit','Tasks','module',90,0),('7bf1dc51-d297-b1c0-0c9a-5c6e1d3778ab','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','list','Campaigns','module',90,0),('7c4b47cf-121d-f430-d03f-5c6e1d0d9029','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','view','AOS_Quotes','module',90,0),('7d74c28a-6fd6-3816-ea14-5c6e1d65a241','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','delete','Tasks','module',90,0),('7d797f14-23ec-eae0-8e4d-5c6e1dd6311f','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','list','SurveyResponses','module',90,0),('7d8d7347-86e8-73a0-1348-5c6e1d86190b','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','edit','Campaigns','module',90,0),('7defbd7a-6478-d657-82e2-5c6e1ddffba3','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','list','AOS_Quotes','module',90,0),('7f16d69c-e29b-910f-4d1e-5c6e1d698cd8','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','import','Tasks','module',90,0),('7f2175cd-db29-b4f8-3753-5c6e1d46bab5','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','delete','Campaigns','module',90,0),('7f33c032-982b-f7e2-c3de-5c6e1d0aa89d','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','edit','SurveyResponses','module',90,0),('7f9d27b7-f779-c812-a360-5c6e1d4959c4','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','edit','AOS_Quotes','module',90,0),('80b641fa-5eda-2510-e682-5c6e1dd24ed4','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','import','Campaigns','module',90,0),('80c063d9-4753-653b-4561-5c6e1d3c54db','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','export','Tasks','module',90,0),('80ec0712-cbc7-f25a-d705-5c6e1d655f72','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','access','jjwg_Address_Cache','module',89,0),('80f68222-9e49-e9fa-1164-5e909d2a6681','2020-04-10 16:25:28','2020-04-10 16:25:28','1','1','access','suh_timer_recording','module',89,0),('80f9ae9e-3727-299f-afd5-5c6e1d0fa123','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','delete','SurveyResponses','module',90,0),('8160c332-1250-2295-9dc4-5c6e1da2bf97','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','delete','AOS_Quotes','module',90,0),('824f5a15-53b1-4755-d578-5c6e1d0a0440','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','export','Campaigns','module',90,0),('825eecdb-807d-fcea-97f9-5c6e1db6ed59','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','massupdate','Tasks','module',90,0),('82a9c474-0dc1-c281-491e-5c6e1d7b27b4','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','import','SurveyResponses','module',90,0),('82fe24f4-3ce6-a0a9-227d-5c6e1df8c135','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','access','AOD_IndexEvent','module',89,0),('83497410-27b0-b41e-f49a-5c6e1df67a5f','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','import','AOS_Quotes','module',90,0),('83aac233-eb13-15c1-fb5b-5e909d4c2678','2020-04-10 16:25:28','2020-04-10 16:25:28','1','1','view','suh_timer_recording','module',90,0),('83e9503e-4d1d-2241-0894-5c6e1d2d5ad3','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','massupdate','Campaigns','module',90,0),('845ceb37-c664-d7bb-14d7-5c6e1df0b770','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','export','SurveyResponses','module',90,0),('84d457d8-2ba5-48a6-5a1b-5c6e1d453ed0','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','view','AOD_IndexEvent','module',90,0),('850c5ce2-4f77-8308-e59f-5c6e1da9a8f0','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','export','AOS_Quotes','module',90,0),('860c4f68-86d7-ef63-880b-5c6e1df3f6e6','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','massupdate','SurveyResponses','module',90,0),('867e07d4-c36e-2ff7-cb57-5e909da7e961','2020-04-10 16:25:28','2020-04-10 16:25:28','1','1','list','suh_timer_recording','module',90,0),('86b6537f-e557-3610-62c3-5c6e1dd8169c','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','list','AOD_IndexEvent','module',90,0),('86bc0feb-f287-8ee8-345a-5c6e1dbd09d3','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','view','Opportunities','module',90,0),('86d7fbc5-843a-f9a3-a2f3-5c6e1d8c8ac7','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','massupdate','AOS_Quotes','module',90,0),('87360a24-2a17-a114-4da2-5c6e1dc660d3','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','view','AM_ProjectTemplates','module',90,0),('88712337-2eb8-7e5f-af4c-5c6e1d94bd75','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','edit','AOD_IndexEvent','module',90,0),('8996b5a7-524c-6482-4fd5-5e909dd4afa9','2020-04-10 16:25:28','2020-04-10 16:25:28','1','1','edit','suh_timer_recording','module',90,0),('8a32b4ee-2ae3-f3c0-9e82-5c6e1d41a51c','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','delete','AOD_IndexEvent','module',90,0),('8be9f5f8-465c-c9ea-d1bb-5c6e1dbecaff','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','import','AOD_IndexEvent','module',90,0),('8c28ae8a-3b44-22b0-8c48-5c6e1db709ed','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','access','ProspectLists','module',89,0),('8c37a3f8-e121-94b9-a6df-5e909d1dff74','2020-04-10 16:25:28','2020-04-10 16:25:28','1','1','delete','suh_timer_recording','module',90,0),('8d5f0375-04e8-9374-548c-5c6e1d4185cb','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','access','Surveys','module',89,0),('8d830de8-f2d0-21b8-09cb-5c6e1d131f85','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','massupdate','Cases','module',90,0),('8da7244b-1fd0-52bc-6c70-5c6e1d8b6bee','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','export','AOD_IndexEvent','module',90,0),('8dc2b087-f5ec-cf51-7690-5c6e1d369edb','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','view','ProspectLists','module',90,0),('8ed5f075-fc75-10d1-ad3b-5e909dd49592','2020-04-10 16:25:28','2020-04-10 16:25:28','1','1','import','suh_timer_recording','module',90,0),('8f3ad6ec-99d2-f98e-7785-5c6e1db67802','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','view','Surveys','module',90,0),('8f5dfb14-8ef8-cee3-ccc8-5c6e1da591cc','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','list','ProspectLists','module',90,0),('8f7040ab-20b9-1e82-89d6-5c6e1d5f3de2','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','massupdate','AOD_IndexEvent','module',90,0),('90b472dc-63cc-dc05-77bd-5c6e1d207f5b','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','access','AOW_WorkFlow','module',89,0),('90fd3e60-d40b-fcd4-fb55-5c6e1dae8fe0','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','edit','ProspectLists','module',90,0),('9114c47d-c20b-a4c8-c2a6-5c6e1da9acb1','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','list','Surveys','module',90,0),('916ef2d8-8108-7977-e586-5e909d2cf1ca','2020-04-10 16:25:28','2020-04-10 16:25:28','1','1','export','suh_timer_recording','module',90,0),('92665564-d5ba-d4a7-55d7-5c6e1de7d212','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','view','AOW_WorkFlow','module',90,0),('929da18d-66a1-fac9-8cbf-5c6e1d22072a','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','delete','ProspectLists','module',90,0),('92f3f737-caa2-dd8a-8a13-5c6e1d4916b9','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','edit','Surveys','module',90,0),('940d3876-ece1-f13c-45af-5c6e1d9f9019','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','list','AOW_WorkFlow','module',90,0),('94383006-21ad-710b-5dc1-5c6e1dfd3bb5','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','import','ProspectLists','module',90,0),('943de675-7b7d-dac8-7dfb-5e909d7e0319','2020-04-10 16:25:28','2020-04-10 16:25:28','1','1','massupdate','suh_timer_recording','module',90,0),('94cd0212-7227-a254-b111-5c6e1d48d71c','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','delete','Surveys','module',90,0),('950504b8-1278-d2c0-1d12-5c6e1ddfb235','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','massupdate','AOR_Scheduled_Reports','module',90,0),('95ac91c3-34ff-4358-5479-5c6e1df5f99e','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','edit','AOW_WorkFlow','module',90,0),('95ce0dd9-8872-02a1-0db3-5c6e1d9bcf73','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','export','ProspectLists','module',90,0),('96a17d4d-5ab9-2ed8-c2a4-5c6e1d987a1c','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','import','Surveys','module',90,0),('96a6e41c-fd1d-2d14-efec-5c6e1ddcd204','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','access','AOD_Index','module',89,0),('97680cfd-ea91-f40b-322a-5c6e1d972dd4','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','massupdate','ProspectLists','module',90,0),('97aa67e1-2ce4-f1b7-30a3-5c6e1d102e97','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','delete','AOW_WorkFlow','module',90,0),('9842d020-e157-2c63-4eca-5c6e1d7d10aa','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','access','Alerts','module',89,0),('9867e242-ed91-e741-d623-5c6e1d746db4','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','export','Surveys','module',90,0),('98b4e4cc-d8f5-e99a-60d3-5c6e1dc7787c','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','view','AOD_Index','module',90,0),('99ab8d2d-89a3-9c76-44ba-5c6e1dbbb117','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','import','AOW_WorkFlow','module',90,0),('99ed1f96-3c96-5191-aacb-5c6e1db0791e','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','view','Alerts','module',90,0),('9a32ca04-50c7-66b7-3185-5c6e1d384a7b','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','massupdate','Surveys','module',90,0),('9a9dea65-c49d-a612-b77a-5c6e1d860eb8','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','list','AOD_Index','module',90,0),('9b6618c8-31c6-d4f8-0ee8-5c6e1d6493d2','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','export','AOW_WorkFlow','module',90,0),('9b8488a5-8b90-64fc-cc6a-5c6e1deafdb8','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','list','Alerts','module',90,0),('9c434503-c420-8ea4-fed4-5e909d093d3f','2020-04-10 16:25:28','2020-04-10 16:25:28','1','1','access','sp_Timeoff_TimeCard','module',89,0),('9c9068c8-2889-4237-c4da-5c6e1d5fd6e0','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','edit','AOD_Index','module',90,0),('9d197071-f115-80c8-ddbe-5c6e1d19c577','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','edit','Alerts','module',90,0),('9d2871bf-a86f-48fc-4161-5c6e1d149e42','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','massupdate','AOW_WorkFlow','module',90,0),('9d5add96-09e7-13e4-5533-5c6e1d5eabe4','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','access','Prospects','module',89,0),('9e240243-5b8b-3fd9-ebdd-5c6e1d4e4fd7','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','view','jjwg_Address_Cache','module',90,0),('9e36c5da-d99c-4575-d041-5c6e1d1932a5','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','delete','AOD_Index','module',90,0),('9eb0255f-c82c-8880-a4e3-5c6e1d003339','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','delete','Alerts','module',90,0),('9ef99bea-c305-cfbf-a4ba-5c6e1dc6b98d','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','view','Prospects','module',90,0),('9f5c2b59-2fef-66ef-9bcb-5e909dafc23b','2020-04-10 16:25:28','2020-04-10 16:25:28','1','1','view','sp_Timeoff_TimeCard','module',90,0),('9febc0e8-571c-b6d9-033d-5c6e1dadd332','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','import','AOD_Index','module',90,0),('a053e50a-04cd-914e-fa5b-5c6e1d5600c1','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','import','Alerts','module',90,0),('a08e9c0b-d79a-1e4c-baed-5c6e1dd818bb','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','list','Prospects','module',90,0),('a1680948-76a8-e14b-1921-5c6e1ddc801a','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','list','AM_ProjectTemplates','module',90,0),('a18e13c0-7b00-9e5b-72c3-5c6e1d886c8c','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','access','SurveyQuestionResponses','module',89,0),('a19598bd-ecb8-b87c-42dc-5c6e1d69db8f','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','export','AOD_Index','module',90,0),('a208420e-202f-0a8e-21cc-5e909d8ce949','2020-04-10 16:25:28','2020-04-10 16:25:28','1','1','list','sp_Timeoff_TimeCard','module',90,0),('a221846d-edb3-d08b-955d-5c6e1de207c6','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','edit','Prospects','module',90,0),('a2490847-3b31-01e5-f954-5c6e1daf08eb','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','list','Opportunities','module',90,0),('a263e131-bb1e-2f23-e69c-5c6e1db832c5','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','export','Alerts','module',90,0),('a34e33e7-f029-be12-4f60-5c6e1d1ee705','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','view','SurveyQuestionResponses','module',90,0),('a3b6eade-aa75-fdfb-bb22-5c6e1d132305','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','delete','Prospects','module',90,0),('a4160e91-0182-6aca-6626-5c6e1dd95415','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','massupdate','Alerts','module',90,0),('a4e20e36-1f69-ef86-e53f-5e909d53c1fd','2020-04-10 16:25:28','2020-04-10 16:25:28','1','1','edit','sp_Timeoff_TimeCard','module',90,0),('a51ad6e5-d047-65d1-bc96-5c6e1d9b1f5c','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','list','SurveyQuestionResponses','module',90,0),('a53a56c9-6206-9bb2-70a8-5c6e1dd996a7','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','massupdate','AOD_Index','module',90,0),('a5507fba-e80d-d18b-7347-5c6e1d7ca48d','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','import','Prospects','module',90,0),('a6eaafe1-3a6e-8a53-b119-5c6e1db76720','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','export','Prospects','module',90,0),('a711af50-e0eb-81dd-aec0-5c6e1d1b2ddd','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','edit','SurveyQuestionResponses','module',90,0),('a80b7337-2bdb-fb18-8a1c-5e909dbccfa9','2020-04-10 16:25:28','2020-04-10 16:25:28','1','1','delete','sp_Timeoff_TimeCard','module',90,0),('a897f543-7d27-85ff-87fa-5c6e1dffec0f','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','massupdate','Prospects','module',90,0),('a8f890a5-c581-7a99-4005-5c6e1d7b2ffd','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','delete','SurveyQuestionResponses','module',90,0),('a9dee868-a6e3-408e-7c68-5c6e1da2e377','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','access','AOW_Processed','module',89,0),('aacba1b3-9082-850d-7134-5e909daf8e6d','2020-04-10 16:25:28','2020-04-10 16:25:28','1','1','import','sp_Timeoff_TimeCard','module',90,0),('ab2242ca-d4fd-9549-e791-5c6e1d385890','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','import','SurveyQuestionResponses','module',90,0),('abd4ec8c-c93c-c1e6-def1-5c6e1ddf4c9a','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','access','Documents','module',89,0),('ac00ed38-12f8-1e3e-18da-5c6e1d059af2','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','view','AOW_Processed','module',90,0),('ac28a8b7-049a-fc4a-e4de-5c6e1d1423a4','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','access','AOP_Case_Events','module',89,0),('ad0f4fee-d33e-2f3c-9f82-5c6e1d1e9a69','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','export','SurveyQuestionResponses','module',90,0),('ad806956-f797-6110-5c34-5e909d976f18','2020-04-10 16:25:28','2020-04-10 16:25:28','1','1','export','sp_Timeoff_TimeCard','module',90,0),('ad856238-c136-2afa-933c-5c6e1d1cdec2','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','view','Documents','module',90,0),('ade5ab40-975e-86e3-e4f9-5c6e1dc0c8a1','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','list','AOW_Processed','module',90,0),('ade60ebc-4744-3eb6-a3e1-5c6e1d94a89b','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','view','AOP_Case_Events','module',90,0),('af161148-77d1-f897-76a8-5c6e1d79ad82','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','access','EmailMarketing','module',89,0),('af31a49b-acde-7cfa-34f5-5c6e1dc79084','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','list','Documents','module',90,0),('af3813f0-504b-4051-224b-5c6e1dfd4d58','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','massupdate','SurveyQuestionResponses','module',90,0),('af9542f4-0edd-1926-48b7-5c6e1d8f9893','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','list','AOP_Case_Events','module',90,0),('afa20a90-5510-6d72-b11a-5c6e1d235b3e','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','edit','AOW_Processed','module',90,0),('b0990c1d-77d5-2980-28a5-5e909dd6a00f','2020-04-10 16:25:28','2020-04-10 16:25:28','1','1','massupdate','sp_Timeoff_TimeCard','module',90,0),('b0c29e2e-087d-18f2-5b3c-5c6e1d67fc2d','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','view','EmailMarketing','module',90,0),('b0d731ac-5b19-327b-4e2a-5c6e1d3ad23b','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','edit','Documents','module',90,0),('b1418e90-c9f7-20cb-7771-5c6e1d6469aa','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','edit','AOP_Case_Events','module',90,0),('b15af48d-52d9-ef94-0fee-5c6e1d9f03f8','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','delete','AOW_Processed','module',90,0),('b262f627-0c6c-c476-450e-5c6e1d7af1a7','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','list','EmailMarketing','module',90,0),('b28d3942-a8ed-93bf-21cd-5c6e1d0f6ae2','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','delete','Documents','module',90,0),('b2e44f1d-2865-1aec-18e0-5c6e1d495fff','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','delete','AOP_Case_Events','module',90,0),('b31c0fe7-ebd4-0419-2940-5c6e1d73fa54','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','import','AOW_Processed','module',90,0),('b40800b6-5ceb-b18f-2021-5c6e1ddf1c71','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','edit','EmailMarketing','module',90,0),('b42943d9-bb9e-b64f-e274-5c6e1d1e54b2','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','import','Documents','module',90,0),('b4845172-d53e-fede-4687-5c6e1da11cd4','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','import','AOP_Case_Events','module',90,0),('b4dc8091-0f5f-c9bf-f80c-5c6e1d875df1','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','export','AOW_Processed','module',90,0),('b5b23ad8-066c-bd1f-1756-5c6e1dda06a1','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','delete','EmailMarketing','module',90,0),('b5ca1507-2676-e260-08b9-5c6e1da38a6f','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','export','Documents','module',90,0),('b6261178-cb2f-3c4d-318a-5c6e1d71d570','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','export','AOP_Case_Events','module',90,0),('b62a2bfc-2615-5abd-0a60-5c6e1d86946a','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','access','SurveyQuestions','module',89,0),('b69e3b35-c9fe-e7f3-8a3c-5c6e1d69236a','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','massupdate','AOW_Processed','module',90,0),('b755c057-7846-ef4e-48a4-5c6e1d5b0de2','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','import','EmailMarketing','module',90,0),('b7652289-d6e5-41c6-96df-5c6e1dfb2f8a','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','massupdate','Documents','module',90,0),('b7c94ce9-b840-0559-bbb4-5c6e1d293842','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','massupdate','AOP_Case_Events','module',90,0),('b7d811b6-4b23-7df0-5897-5c6e1d72ffd6','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','view','SurveyQuestions','module',90,0),('b8f6333c-264a-d56c-f3f7-5c6e1d6717b5','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','export','EmailMarketing','module',90,0),('b99c202c-a3f2-0e4c-117c-5c6e1d334507','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','list','SurveyQuestions','module',90,0),('baa3bd6a-799c-c7cb-8167-5c6e1de0778f','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','massupdate','EmailMarketing','module',90,0),('bb1e0579-51fc-26c1-c49f-5c6e1d30b0d2','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','edit','AM_ProjectTemplates','module',90,0),('bb63e112-5c92-af82-2ed4-5c6e1d722c85','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','edit','SurveyQuestions','module',90,0),('bd05094c-c21b-c6bb-9649-5c6e1dbbf283','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','list','jjwg_Address_Cache','module',90,0),('bd16a03b-186c-145a-c165-5c6e1d2bb8aa','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','delete','SurveyQuestions','module',90,0),('bdb006b2-d132-b086-d28e-5c6e1d4cdff2','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','edit','Opportunities','module',90,0),('be604b96-6736-7e81-6d15-5c6e1d5dab3e','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','access','AOP_Case_Updates','module',89,0),('bebe1a01-5b96-efc4-c366-5c6e1dfc050d','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','import','SurveyQuestions','module',90,0),('bef20eff-0b7e-c1bb-7a8e-5c6e1df0685c','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','access','jjwg_Maps','module',89,0),('c0085f74-6446-7bec-6b0f-5c6e1d2d824b','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','view','AOP_Case_Updates','module',90,0),('c06bc7c3-f4ae-9709-ad32-5c6e1df9919b','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','export','SurveyQuestions','module',90,0),('c0a8ec91-575f-d850-fae9-5c6e1dbb2f31','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','view','jjwg_Maps','module',90,0),('c1cc01ac-a509-0cfd-5218-5c6e1d18f2cd','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','list','AOP_Case_Updates','module',90,0),('c216ae81-667d-57c6-1b66-5c6e1d66fedc','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','massupdate','SurveyQuestions','module',90,0),('c24efd4f-bdc0-94a8-5feb-5c6e1de80616','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','list','jjwg_Maps','module',90,0),('c3a67730-0e9c-4380-7ead-5c6e1df50457','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','edit','AOP_Case_Updates','module',90,0),('c407d548-1499-6d04-15ca-5c6e1d93705a','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','edit','jjwg_Maps','module',90,0),('c58c2f62-5135-6531-5629-5c6e1d7895af','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','delete','AOP_Case_Updates','module',90,0),('c5b282ff-1ee9-44ca-32c7-5c6e1d99ecbb','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','delete','jjwg_Maps','module',90,0),('c76b891a-5d75-28e1-7037-5c6e1d34fc61','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','import','AOP_Case_Updates','module',90,0),('c77bfcc8-254e-1ceb-08d4-5c6e1d2fbe7b','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','import','jjwg_Maps','module',90,0),('c8db86d1-7f26-b8aa-5323-5c6e1d4f870e','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','access','SurveyQuestionOptions','module',89,0),('c95817c6-d1e2-2fea-2350-5c6e1d6aa3fe','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','export','AOP_Case_Updates','module',90,0),('c95a4d72-da3c-06c1-c24d-5c6e1de6ff39','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','export','jjwg_Maps','module',90,0),('cb14dd05-873d-715e-92be-5c6e1d21e982','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','view','SurveyQuestionOptions','module',90,0),('cb2bdcf2-b789-3402-2c32-5c6e1d1d122e','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','massupdate','jjwg_Maps','module',90,0),('cb44e564-ea4c-4625-16e8-5c6e1d0a6ed8','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','massupdate','AOP_Case_Updates','module',90,0),('cd02e82c-7eb8-8630-015d-5c6e1d54dd16','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','list','SurveyQuestionOptions','module',90,0),('ce4558d9-c83f-511d-b67b-5c6e1dff0232','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','access','Contacts','module',89,0),('ced98a67-37f4-6707-c5ac-5c6e1da221c6','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','edit','SurveyQuestionOptions','module',90,0),('cf862ecd-de63-de1c-8ae3-5c6e1da03a50','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','access','Spots','module',89,0),('d0031d26-18da-f0d3-e7d3-5c6e1d89ca77','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','view','Contacts','module',90,0),('d096a892-9823-074d-edd4-5c6e1dfe248d','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','delete','SurveyQuestionOptions','module',90,0),('d122f516-ba83-0d8e-741a-5c6e1d51b454','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','view','Spots','module',90,0),('d1b5a193-7424-54fb-d5d1-5c6e1d7ab7e5','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','list','Contacts','module',90,0),('d2429bba-7f54-24aa-1491-5c6e1db06ab6','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','import','SurveyQuestionOptions','module',90,0),('d2d19ea3-bf2c-67b9-87a6-5c6e1d2d2561','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','list','Spots','module',90,0),('d363a602-e3a2-4f7b-15d5-5c6e1d33de0e','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','edit','Contacts','module',90,0),('d3f3c098-4d7c-d2c6-54b3-5c6e1dc0a477','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','export','SurveyQuestionOptions','module',90,0),('d41751ce-361d-6116-ba6a-5c6e1d93e0c2','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','access','AOR_Reports','module',89,0),('d46dedb9-a982-67a2-d70a-5c6e1da04d41','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','edit','Spots','module',90,0),('d4d800a3-cd2a-ee02-8c27-5c6e1d23e7fc','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','delete','AM_ProjectTemplates','module',90,0),('d4d93551-68c5-3088-cd6d-5c6e1dfc5050','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','access','jjwg_Markers','module',89,0),('d50a007b-3691-7ac6-b4b7-5c6e1dce4545','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','delete','Contacts','module',90,0),('d5b4803d-53f2-521a-2ee9-5c6e1d8e681b','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','massupdate','SurveyQuestionOptions','module',90,0),('d602ff9a-88a3-20a1-36f0-5c6e1ddae899','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','delete','Spots','module',90,0),('d63d2d97-856c-40aa-a915-5c6e1d5908f3','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','view','AOR_Reports','module',90,0),('d6b13e01-c92c-477f-5199-5c6e1d956e32','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','import','Contacts','module',90,0),('d79b79ce-5652-e699-c84a-5c6e1ddc8bc8','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','import','Spots','module',90,0),('d82a4ba7-7cba-70f5-d7a4-5c6e1ddf0b3b','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','list','AOR_Reports','module',90,0),('d84dbda5-0435-3268-4441-5c6e1dec53c2','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','view','jjwg_Markers','module',90,0),('d86004d0-2d45-49de-b8ab-5c6e1d5b4881','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','export','Contacts','module',90,0),('d90400ee-40f8-2dd5-19a3-5c6e1dd03e8b','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','delete','Opportunities','module',90,0),('d92f8b18-3f0e-d480-0183-5c6e1dea0580','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','export','Spots','module',90,0),('d9e62f26-2a44-8731-467e-5c6e1de31273','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','edit','AOR_Reports','module',90,0),('da0a2316-6c11-6f86-04fb-5c6e1d12145f','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','massupdate','Contacts','module',90,0),('da15b921-e3da-b9da-7692-5c6e1d338231','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','list','jjwg_Markers','module',90,0),('dacaaa84-1cf7-efaa-60c3-5c6e1d807a84','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','massupdate','Spots','module',90,0),('dada0a15-a601-6bd0-d323-5c6e1d2fd32f','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','edit','jjwg_Address_Cache','module',90,0),('db873033-8e64-6de0-193d-5c6e1d2f494c','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','access','Leads','module',89,0),('dba571ad-f302-219d-5f27-5c6e1dfa7f00','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','delete','AOR_Reports','module',90,0),('dbd75848-1e97-c63d-0ba7-5c6e1d60ef8c','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','edit','jjwg_Markers','module',90,0),('dd53b539-625a-a770-e54b-5c6e1da3a732','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','import','AOR_Reports','module',90,0),('dda1ad47-01b9-046e-cd89-5c6e1d09c51d','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','delete','jjwg_Markers','module',90,0),('de0488d2-31a1-08f9-d295-5c6e1dc33038','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','view','Leads','module',90,0),('df22d385-fffb-07f6-4cad-5c6e1d05385c','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','export','AOR_Reports','module',90,0),('df56ae76-a203-3777-2e68-5c6e1da9aa23','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','import','jjwg_Markers','module',90,0),('dff9e8c1-7bca-a0ba-d00a-5c6e1d2200da','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','list','Leads','module',90,0),('e10e0f4f-3089-f6dc-da51-5c6e1d2fda64','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','export','jjwg_Markers','module',90,0),('e137a998-bde5-ba95-c4c5-5c6e1d6dd872','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','massupdate','AOR_Reports','module',90,0),('e18fff67-dccb-b854-fb52-5c6e1d1c257e','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','edit','Leads','module',90,0),('e2be1f2e-bf7a-99e2-93a1-5c6e1d7b6bba','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','massupdate','jjwg_Markers','module',90,0),('e32106b1-5bea-8fc2-af47-5c6e1d9e72f7','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','delete','Leads','module',90,0),('e4b7d2d6-a856-a13b-9e18-5c6e1d70e0c0','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','import','Leads','module',90,0),('e4e294a9-b18d-ee54-645d-5c6e1dd1e8c6','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','access','Accounts','module',89,0),('e4fa58f2-e15d-1cf0-7dc4-5c6e1da17057','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','access','EAPM','module',89,0),('e5c20eac-6559-6531-9fc1-5c6e1d07319c','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','access','AOS_Contracts','module',89,0),('e64249f4-d1f2-0677-2be6-5c6e1d48a985','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','export','Leads','module',90,0),('e67f4d71-4b5b-0f8b-e816-5c6e1d6cbfdc','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','view','Accounts','module',90,0),('e69dd3d3-0eab-3022-3735-5c6e1d14a076','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','view','EAPM','module',90,0),('e7d1dbec-7b69-37d5-6b62-5c6e1da40e84','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','massupdate','Leads','module',90,0),('e81b1376-eea5-89f3-2af6-5c6e1d01ec62','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','list','Accounts','module',90,0),('e83709f6-9fef-96ff-7942-5c6e1d98ed5c','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','list','EAPM','module',90,0),('e941f915-63c6-f813-7912-5c6e1d687657','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','access','jjwg_Areas','module',89,0),('e9c4c00e-5b31-7e5e-ed1c-5c6e1d771e86','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','edit','Accounts','module',90,0),('e9d42efb-bea8-e95b-c776-5c6e1d3696e6','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','edit','EAPM','module',90,0),('eb1dc44d-6ed8-fa1b-6fbe-5c6e1d3d094a','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','view','jjwg_Areas','module',90,0),('eb68a252-8b3f-8d0e-b715-5c6e1ddb6365','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','delete','EAPM','module',90,0),('eb6ad108-3dbe-6c3f-6e09-5c6e1d42e30d','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','delete','Accounts','module',90,0),('ecd4a57b-0e61-6ae6-26be-5c6e1dfaf4d9','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','list','jjwg_Areas','module',90,0),('ecfefd1b-1800-f44f-1632-5c6e1db114d0','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','import','EAPM','module',90,0),('ed085713-2fef-f3a6-d794-5c6e1d5116c5','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','import','Accounts','module',90,0),('ee843bb5-3944-a90b-1821-5c6e1dd16399','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','edit','jjwg_Areas','module',90,0),('ee981ca9-fd8d-da61-03ed-5c6e1d57bf84','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','export','EAPM','module',90,0),('eea30100-cfbc-4f33-53e8-5c6e1d718073','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','import','AM_ProjectTemplates','module',90,0),('eea809c6-5494-ebd8-ea2a-5c6e1d2a1bc3','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','export','Accounts','module',90,0),('ef70059e-eb96-9eb7-e071-5c6e1d41766b','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','list','Cases','module',90,0),('f037714b-c744-9311-3833-5c6e1d91bac5','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','massupdate','EAPM','module',90,0),('f038848b-d6ea-eea1-82c9-5c6e1d8050b7','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','delete','jjwg_Areas','module',90,0),('f04405bc-ba89-bfc6-c5a7-5c6e1db2a756','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','massupdate','Accounts','module',90,0),('f0e63510-538e-a5f1-735f-5c6e1dee3384','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','access','AOR_Scheduled_Reports','module',89,0),('f1e9306e-dc78-fda0-504a-5c6e1d689e6e','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','access','Cases','module',89,0),('f1ee01c7-5136-1887-b4a6-5c6e1d1b29ab','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','import','jjwg_Areas','module',90,0),('f29b36cd-339b-ecb7-97b9-5c6e1d60039c','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','view','AOR_Scheduled_Reports','module',90,0),('f384474f-3e33-9f86-bdf5-5c6e1d2ca1a1','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','view','Cases','module',90,0),('f3ed8a4b-4202-a38e-95ab-5c6e1de1eb7b','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','export','jjwg_Areas','module',90,0),('f4460c0a-f75e-b471-0c7f-5c6e1d1459a1','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','import','Opportunities','module',90,0),('f7d00d0d-3e0b-a249-08f5-5c6e1d5a174f','2019-02-21 03:39:38','2019-02-21 03:39:38','1','','delete','jjwg_Address_Cache','module',90,0);
/*!40000 ALTER TABLE `acl_actions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `acl_roles`
--

DROP TABLE IF EXISTS `acl_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acl_roles` (
  `id` char(36) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `name` varchar(150) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_aclrole_id_del` (`id`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acl_roles`
--

LOCK TABLES `acl_roles` WRITE;
/*!40000 ALTER TABLE `acl_roles` DISABLE KEYS */;
INSERT INTO `acl_roles` VALUES ('3304ef15-3d54-d514-0f52-5f469cfc6b17','2020-08-26 17:30:11','2020-08-27 17:44:12','1','1','Local',NULL,0),('69d63f4e-88ef-58c9-0a28-5f469b0dc456','2020-08-26 17:29:34','2020-08-27 17:47:13','1','1','Manager',NULL,0),('824bc684-1b3b-2184-338e-5f47f0ca9e93','2020-08-27 17:44:23','2020-08-27 17:44:57','1','1','Test',NULL,1);
/*!40000 ALTER TABLE `acl_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `acl_roles_actions`
--

DROP TABLE IF EXISTS `acl_roles_actions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acl_roles_actions` (
  `id` varchar(36) NOT NULL,
  `role_id` varchar(36) DEFAULT NULL,
  `action_id` varchar(36) DEFAULT NULL,
  `access_override` int(3) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_acl_role_id` (`role_id`),
  KEY `idx_acl_action_id` (`action_id`),
  KEY `idx_aclrole_action` (`role_id`,`action_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acl_roles_actions`
--

LOCK TABLES `acl_roles_actions` WRITE;
/*!40000 ALTER TABLE `acl_roles_actions` DISABLE KEYS */;
INSERT INTO `acl_roles_actions` VALUES ('10100714-7c44-d838-b0ae-5f469c7b379f','3304ef15-3d54-d514-0f52-5f469cfc6b17','68b60ce8-53e9-437d-7bbf-5c6e1d416598',89,'2020-08-27 17:49:53',0),('103cf97b-cf98-262f-9c27-5f469c4140b0','3304ef15-3d54-d514-0f52-5f469cfc6b17','d90400ee-40f8-2dd5-19a3-5c6e1dd03e8b',-99,'2020-08-27 17:49:53',0),('10696966-6f83-e03e-55ed-5f469c4b6186','3304ef15-3d54-d514-0f52-5f469cfc6b17','bdb006b2-d132-b086-d28e-5c6e1d4cdff2',75,'2020-08-27 17:49:53',0),('10f898e0-a1c2-c6f8-260c-5f469c54b8a4','3304ef15-3d54-d514-0f52-5f469cfc6b17','10f6b83e-e87a-61fb-d12a-5c6e1dffd363',75,'2020-08-27 17:49:53',0),('11267ca3-bb9d-99fc-7e82-5f469c28c3f4','3304ef15-3d54-d514-0f52-5f469cfc6b17','f4460c0a-f75e-b471-0c7f-5c6e1d1459a1',90,'2020-08-27 17:49:53',0),('1157fe5d-3f24-eaca-e0d5-5f469c4b0b84','3304ef15-3d54-d514-0f52-5f469cfc6b17','a2490847-3b31-01e5-f954-5c6e1daf08eb',75,'2020-08-27 17:49:53',0),('11900440-9b8f-3231-abed-5f469c3072c6','3304ef15-3d54-d514-0f52-5f469cfc6b17','12a22378-ce15-c2f0-402b-5c6e1d224f96',90,'2020-08-27 17:49:53',0),('11d40ea0-3a4c-d480-4102-5f469cba3272','3304ef15-3d54-d514-0f52-5f469cfc6b17','e4b7d2d6-a856-a13b-9e18-5c6e1d70e0c0',90,'2020-08-27 17:49:53',0),('11d687ea-0bc7-388b-8346-5f469c2b8d8a','3304ef15-3d54-d514-0f52-5f469cfc6b17','86bc0feb-f287-8ee8-345a-5c6e1dbd09d3',75,'2020-08-27 17:49:53',0),('120798d6-e3ea-8afd-d64f-5f469cc73274','3304ef15-3d54-d514-0f52-5f469cfc6b17','453b3704-5824-5632-462a-5c6e1dce0981',89,'2020-08-27 17:49:53',0),('1234aca6-2b4b-a7f2-8281-5f469c068cb7','3304ef15-3d54-d514-0f52-5f469cfc6b17','53dd2dcb-b198-1a9c-6c63-5c6e1d958589',-99,'2020-08-27 17:49:53',0),('1260a437-1951-a507-8f2e-5f469c14ae6d','3304ef15-3d54-d514-0f52-5f469cfc6b17','5204b668-b7ff-ccdd-8e4d-5c6e1d632d90',75,'2020-08-27 17:49:53',0),('12a83b77-a085-a1f3-e7c1-5f469c712695','3304ef15-3d54-d514-0f52-5f469cfc6b17','5789c7f2-cad5-5b8c-4353-5c6e1d5345eb',75,'2020-08-27 17:49:53',0),('12d42827-bb3f-0b40-5046-5f469c08b8dc','3304ef15-3d54-d514-0f52-5f469cfc6b17','55b878d0-2944-bdc5-2d7a-5c6e1d195b81',90,'2020-08-27 17:49:53',0),('12ff43c8-c12b-80b8-d913-5f469c4a3535','3304ef15-3d54-d514-0f52-5f469cfc6b17','502a38bf-fe4f-4c7c-33b2-5c6e1d7254d0',75,'2020-08-27 17:49:53',0),('132d098a-fc64-6011-115b-5f469c4ae79e','3304ef15-3d54-d514-0f52-5f469cfc6b17','5962e588-9521-bc29-9540-5c6e1d63b93c',90,'2020-08-27 17:49:53',0),('135ae53c-a4b2-3468-fb8a-5f469c1ba1f1','3304ef15-3d54-d514-0f52-5f469cfc6b17','4e23c1ff-9369-80c8-9296-5c6e1d9eda74',75,'2020-08-27 17:49:53',0),('13a2805e-3be4-3438-95da-5f469c52106e','3304ef15-3d54-d514-0f52-5f469cfc6b17','392b4520-bc4a-0b1f-059f-5c6e1d653b57',89,'2020-08-27 17:49:53',0),('13d2ae78-691a-2f29-126a-5f469cbe9e1f','3304ef15-3d54-d514-0f52-5f469cfc6b17','3fa48fbe-c3d1-67ba-7543-5c6e1d64f949',-99,'2020-08-27 17:49:53',0),('14014ec5-4133-0375-2d86-5f469c159f26','3304ef15-3d54-d514-0f52-5f469cfc6b17','3e0a8d82-3855-407e-0ac4-5c6e1d0cdf9f',75,'2020-08-27 17:49:53',0),('142efb7e-3f7a-8830-417c-5f469cfead44','3304ef15-3d54-d514-0f52-5f469cfc6b17','42dbf3b7-2439-13c1-da55-5c6e1dd94e7d',75,'2020-08-27 17:49:53',0),('145cb41a-bf57-2e2f-46f5-5f469c2f9f41','3304ef15-3d54-d514-0f52-5f469cfc6b17','413eb557-390e-1809-3fee-5c6e1db396b6',90,'2020-08-27 17:49:53',0),('14a85948-a28f-8f10-3f30-5f469c9eeb73','3304ef15-3d54-d514-0f52-5f469cfc6b17','3c6faf0b-9351-4b5a-34f1-5c6e1d899517',75,'2020-08-27 17:49:53',0),('14e1bb16-0a0b-6f2b-d87a-5f469c098d2c','3304ef15-3d54-d514-0f52-5f469cfc6b17','447e4027-4eb0-7267-7547-5c6e1dda96aa',90,'2020-08-27 17:49:53',0),('150f072d-0d14-79fd-b6d4-5f469cef837e','3304ef15-3d54-d514-0f52-5f469cfc6b17','3acf69d8-9b59-caed-5eb9-5c6e1d95d81a',75,'2020-08-27 17:49:53',0),('153d9b67-5f87-7e3a-9edf-5f469c1783bb','3304ef15-3d54-d514-0f52-5f469cfc6b17','a9dee868-a6e3-408e-7c68-5c6e1da2e377',89,'2020-08-27 17:49:53',0),('156d4ace-5be4-bc0c-dbbd-5f469c851fc0','3304ef15-3d54-d514-0f52-5f469cfc6b17','b15af48d-52d9-ef94-0fee-5c6e1d9f03f8',-99,'2020-08-27 17:49:53',0),('15b2dc3f-9f5a-4bc3-f9b9-5f469ccfd0cb','3304ef15-3d54-d514-0f52-5f469cfc6b17','afa20a90-5510-6d72-b11a-5c6e1d235b3e',75,'2020-08-27 17:49:53',0),('15de0355-0d54-c579-d076-5f469c83cf2d','3304ef15-3d54-d514-0f52-5f469cfc6b17','b4dc8091-0f5f-c9bf-f80c-5c6e1d875df1',75,'2020-08-27 17:49:53',0),('16098915-b199-1473-7dcd-5f469ce009c7','3304ef15-3d54-d514-0f52-5f469cfc6b17','b31c0fe7-ebd4-0419-2940-5c6e1d73fa54',90,'2020-08-27 17:49:53',0),('1638472e-39be-5cb3-3d8d-5f469c0326e8','3304ef15-3d54-d514-0f52-5f469cfc6b17','ade5ab40-975e-86e3-e4f9-5c6e1dc0c8a1',75,'2020-08-27 17:49:53',0),('1668fa5a-2915-a055-7ace-5f469c8ff842','3304ef15-3d54-d514-0f52-5f469cfc6b17','b69e3b35-c9fe-e7f3-8a3c-5c6e1d69236a',90,'2020-08-27 17:49:53',0),('16b8fa35-a0e4-dce0-e4ce-5f469cc9b53e','3304ef15-3d54-d514-0f52-5f469cfc6b17','ac00ed38-12f8-1e3e-18da-5c6e1d059af2',75,'2020-08-27 17:49:53',0),('16ed2816-f98b-9b66-e226-5f469c4bf5e6','3304ef15-3d54-d514-0f52-5f469cfc6b17','5d297527-1f3f-14f3-d10f-5c6e1d048547',89,'2020-08-27 17:49:53',0),('1722c68c-c500-2b06-9181-5f469c89297b','3304ef15-3d54-d514-0f52-5f469cfc6b17','640483b3-6ace-7d25-28bc-5c6e1d09e2f4',-99,'2020-08-27 17:49:53',0),('175f907e-db84-c7cd-b7be-5f469c6e2662','3304ef15-3d54-d514-0f52-5f469cfc6b17','623518f3-d355-4c06-6dbd-5c6e1dc6f618',75,'2020-08-27 17:49:53',0),('17680345-154c-d1fc-8854-5f469c0fbb05','3304ef15-3d54-d514-0f52-5f469cfc6b17','dff9e8c1-7bca-a0ba-d00a-5c6e1d2200da',75,'2020-08-27 17:49:53',0),('17bdef92-16d1-a4a8-aab2-5f469c37e0d7','3304ef15-3d54-d514-0f52-5f469cfc6b17','67729b81-c610-e9b5-6626-5c6e1d6a9701',75,'2020-08-27 17:49:53',0),('17f006e7-ac7b-33b0-4e2a-5f469c25de1f','3304ef15-3d54-d514-0f52-5f469cfc6b17','65c01214-075a-23ad-1a49-5c6e1d0c7711',90,'2020-08-27 17:49:53',0),('182071dc-c3a3-aaf5-b8d7-5f469cbf4377','3304ef15-3d54-d514-0f52-5f469cfc6b17','6080d88e-9d8f-594a-949c-5c6e1de8ce23',75,'2020-08-27 17:49:53',0),('185be0b2-e11c-2b86-8c5b-5f469cbac578','3304ef15-3d54-d514-0f52-5f469cfc6b17','6929e76d-97ed-b6c4-d138-5c6e1d2fa293',90,'2020-08-27 17:49:53',0),('18fe1135-06ce-ed0c-7b6d-5f469c018ced','3304ef15-3d54-d514-0f52-5f469cfc6b17','5ed3bcca-6841-1c42-dec8-5c6e1dcd1654',75,'2020-08-27 17:49:53',0),('1932944d-c649-4cae-7360-5f469c3a42e3','3304ef15-3d54-d514-0f52-5f469cfc6b17','4abd9300-75b3-c2d2-685e-5c6e1df785c7',89,'2020-08-27 17:49:53',0),('198ba5b2-23d6-26a2-2e6f-5f469c74d36d','3304ef15-3d54-d514-0f52-5f469cfc6b17','51468e5a-b69b-bd8d-77b6-5c6e1d52286e',-99,'2020-08-27 17:49:53',0),('19b6ebdf-68b1-3b3f-da57-5f469cd0de52','3304ef15-3d54-d514-0f52-5f469cfc6b17','4fa28488-be59-b43c-3a3f-5c6e1de2faca',75,'2020-08-27 17:49:53',0),('19e993e5-19ed-67c0-a68d-5f469c17f722','3304ef15-3d54-d514-0f52-5f469cfc6b17','54928ee5-4276-ffbe-fdb1-5c6e1dc20efa',75,'2020-08-27 17:49:53',0),('1a161caf-955e-3882-e934-5f469ca95a08','3304ef15-3d54-d514-0f52-5f469cfc6b17','52e9af88-fc3a-2da4-8afd-5c6e1d7802ed',90,'2020-08-27 17:49:53',0),('1a4b8408-f5df-2eb4-8294-5f469cdd0ba3','3304ef15-3d54-d514-0f52-5f469cfc6b17','4dff9ed3-fe53-4b2e-38fc-5c6e1d318481',75,'2020-08-27 17:49:53',0),('1a9a0e6a-f4c9-2a15-0f1d-5f469c15d3d2','3304ef15-3d54-d514-0f52-5f469cfc6b17','e7d1dbec-7b69-37d5-6b62-5c6e1da40e84',90,'2020-08-27 17:49:53',0),('1aa16d0f-4ae5-ea56-87c8-5f469cf27a34','3304ef15-3d54-d514-0f52-5f469cfc6b17','5638e0fd-5245-0cd9-ce3a-5c6e1de3594b',90,'2020-08-27 17:49:53',0),('1ad44981-8266-c366-5d9a-5f469ca2a1ea','3304ef15-3d54-d514-0f52-5f469cfc6b17','4c5e89e7-59ec-82d0-2835-5c6e1d1e856d',75,'2020-08-27 17:49:53',0),('1b104133-d920-f3a6-86cb-5f469c242a11','3304ef15-3d54-d514-0f52-5f469cfc6b17','184cafd9-5da5-233b-61b2-5c6e1dcc1ae2',89,'2020-08-27 17:49:53',0),('1b457b12-06e8-5708-bc73-5f469c231c7f','3304ef15-3d54-d514-0f52-5f469cfc6b17','1eca7007-450c-966f-c0f1-5c6e1d074aee',-99,'2020-08-27 17:49:53',0),('1b96bf4f-4674-0bbe-4aef-5f469c1506a1','3304ef15-3d54-d514-0f52-5f469cfc6b17','1d2877da-72f4-f0e6-a6bc-5c6e1da8da72',75,'2020-08-27 17:49:53',0),('1bc685c9-eeb3-c606-081a-5f469c1a448b','3304ef15-3d54-d514-0f52-5f469cfc6b17','22996860-ab57-6633-73f6-5c6e1d5dcb18',75,'2020-08-27 17:49:53',0),('1bf351fc-57ed-1061-2a67-5f469c77a1b9','3304ef15-3d54-d514-0f52-5f469cfc6b17','206ab75e-6ea7-cf2a-57ea-5c6e1d9ebd40',90,'2020-08-27 17:49:53',0),('1c2100e2-2b55-d6d4-fe50-5f469cf73653','3304ef15-3d54-d514-0f52-5f469cfc6b17','1b7cf5f1-f8ac-d35a-0532-5c6e1d6bd505',75,'2020-08-27 17:49:53',0),('1c4cf10b-6227-b545-074a-5f469c7572be','3304ef15-3d54-d514-0f52-5f469cfc6b17','245a684c-7f66-e44a-a4c6-5c6e1dfe0a22',90,'2020-08-27 17:49:53',0),('1c98e35a-62a5-2ee3-786b-5f469cd7605b','3304ef15-3d54-d514-0f52-5f469cfc6b17','19e5dfc5-63d8-d00f-4a5c-5c6e1de57f20',75,'2020-08-27 17:49:53',0),('1cc90167-21fc-ee89-fa3c-5f469c105eb9','3304ef15-3d54-d514-0f52-5f469cfc6b17','652163fe-3269-b1f4-9e3f-5c6e1d37dbcf',89,'2020-08-27 17:49:53',0),('1d1318c9-9f0f-6d2c-5654-5f469ce539b0','3304ef15-3d54-d514-0f52-5f469cfc6b17','6b7c675b-97bd-5d96-6453-5c6e1d4f434a',-99,'2020-08-27 17:49:53',0),('1d4ea926-1495-bb98-3629-5f469c438a38','3304ef15-3d54-d514-0f52-5f469cfc6b17','69ea34b6-878a-0c7f-7878-5c6e1d97c3e4',75,'2020-08-27 17:49:53',0),('1d520ba2-9cb4-23db-12c1-5f469c7cc0eb','3304ef15-3d54-d514-0f52-5f469cfc6b17','de0488d2-31a1-08f9-d295-5c6e1dc33038',75,'2020-08-27 17:49:53',0),('1de8ab22-fec3-c582-ca0a-5f469c71f902','3304ef15-3d54-d514-0f52-5f469cfc6b17','6ecb3a28-bd02-04f4-879d-5c6e1dccaeb9',75,'2020-08-27 17:49:53',0),('1e15aed2-7c32-444a-7879-5f469c0d2fe5','3304ef15-3d54-d514-0f52-5f469cfc6b17','6d2d4783-4ec1-d9fc-0c70-5c6e1d1755c6',90,'2020-08-27 17:49:53',0),('1e5cff90-0bad-6cf4-0266-5f469c50fbd2','3304ef15-3d54-d514-0f52-5f469cfc6b17','68556931-3a69-e3fa-3a95-5c6e1d28b747',75,'2020-08-27 17:49:53',0),('1e8a7ea6-e704-6df1-71a2-5f469c1a7bc6','3304ef15-3d54-d514-0f52-5f469cfc6b17','705b169d-be44-c4b1-1ded-5c6e1debbb34',90,'2020-08-27 17:49:53',0),('1ecd30d6-fe46-90a2-f6ac-5f469c840379','3304ef15-3d54-d514-0f52-5f469cfc6b17','66c644d2-69ae-d214-fe80-5c6e1d9aafb1',75,'2020-08-27 17:49:53',0),('1f070758-5364-d003-d474-5f469c1dbb7d','3304ef15-3d54-d514-0f52-5f469cfc6b17','5470bd94-dff3-8a95-720a-5c6e1dbd7da5',89,'2020-08-27 17:49:53',0),('1fa111e7-3d26-d540-edf9-5f469cbdaa89','3304ef15-3d54-d514-0f52-5f469cfc6b17','5acb056c-34b1-a7a7-b1f6-5c6e1dee52c4',-99,'2020-08-27 17:49:53',0),('1fd464de-667d-055d-6592-5f469cae9915','3304ef15-3d54-d514-0f52-5f469cfc6b17','593a5e01-2184-6ac9-0aef-5c6e1d7818e2',75,'2020-08-27 17:49:53',0),('200f3664-1873-2768-7cae-5f469c15000a','3304ef15-3d54-d514-0f52-5f469cfc6b17','5de87979-2cf3-acb0-b065-5c6e1dfdc2c3',75,'2020-08-27 17:49:53',0),('20565205-4db4-8a1e-a34f-5f469c8eb1db','3304ef15-3d54-d514-0f52-5f469cfc6b17','5c565cf0-d6e3-4173-5d2d-5c6e1d926139',90,'2020-08-27 17:49:53',0),('2088638f-a504-d342-f4b2-5f469c84361e','3304ef15-3d54-d514-0f52-5f469cfc6b17','57ad2510-4b44-9372-fe6f-5c6e1d08759e',75,'2020-08-27 17:49:53',0),('20ba0f7d-3505-f45a-6e65-5f469c6c9144','3304ef15-3d54-d514-0f52-5f469cfc6b17','5f7ff9db-59ef-9e32-9104-5c6e1d3ee0bf',90,'2020-08-27 17:49:53',0),('20ea06f8-73f2-c690-5956-5f469c0e8bd4','3304ef15-3d54-d514-0f52-5f469cfc6b17','5622f630-b2c6-dffe-a454-5c6e1db76a36',75,'2020-08-27 17:49:53',0),('2124b187-a9bf-f09e-115d-5f469c8abf49','3304ef15-3d54-d514-0f52-5f469cfc6b17','6ce608cd-b55d-2703-a9c3-5c6e1d11ce48',89,'2020-08-27 17:49:53',0),('217557c8-3654-a63c-e4f5-5f469c3dfae4','3304ef15-3d54-d514-0f52-5f469cfc6b17','d4d800a3-cd2a-ee02-8c27-5c6e1d23e7fc',-99,'2020-08-27 17:49:53',0),('21a25694-a0b3-aa55-64e8-5f469c8a7af6','3304ef15-3d54-d514-0f52-5f469cfc6b17','bb1e0579-51fc-26c1-c49f-5c6e1d30b0d2',75,'2020-08-27 17:49:53',0),('21d56e6d-e1b2-e3aa-3961-5f469c6b6ad6','3304ef15-3d54-d514-0f52-5f469cfc6b17','10849107-d2b4-4ba1-4f7e-5c6e1dd0ec65',75,'2020-08-27 17:49:53',0),('21d909a7-868a-f716-db76-5f469c6bef78','3304ef15-3d54-d514-0f52-5f469cfc6b17','6ca5fca3-9719-e9bb-0913-5c6e1dc4e355',89,'2020-08-27 17:49:53',0),('2208d70c-e4d8-80f1-6062-5f469c88ebe1','3304ef15-3d54-d514-0f52-5f469cfc6b17','eea30100-cfbc-4f33-53e8-5c6e1d718073',90,'2020-08-27 17:49:53',0),('225175ea-4ae2-fe48-3a47-5f469c7827ea','3304ef15-3d54-d514-0f52-5f469cfc6b17','a1680948-76a8-e14b-1921-5c6e1ddc801a',75,'2020-08-27 17:49:53',0),('228086a3-3882-4765-c918-5f469c3d0be7','3304ef15-3d54-d514-0f52-5f469cfc6b17','121aa5d9-ed75-7bb0-a8c8-5c6e1de8a86a',90,'2020-08-27 17:49:53',0),('22aaff2d-4dc5-9804-4dfe-5f469c041d1a','3304ef15-3d54-d514-0f52-5f469cfc6b17','87360a24-2a17-a114-4da2-5c6e1dc660d3',75,'2020-08-27 17:49:53',0),('22d98852-759f-0192-a289-5f469cd2dfeb','3304ef15-3d54-d514-0f52-5f469cfc6b17','7aac0b6d-7b37-a210-a863-5c6e1dfd72eb',89,'2020-08-27 17:49:53',0),('230a3667-60ce-a8b5-c071-5f469c1033bc','3304ef15-3d54-d514-0f52-5f469cfc6b17','8160c332-1250-2295-9dc4-5c6e1da2bf97',-99,'2020-08-27 17:49:53',0),('235e57ea-fce6-3836-4db0-5f469c7db879','3304ef15-3d54-d514-0f52-5f469cfc6b17','7f9d27b7-f779-c812-a360-5c6e1d4959c4',75,'2020-08-27 17:49:53',0),('23a44aa1-d055-7607-d6f8-5f469c2db4e6','3304ef15-3d54-d514-0f52-5f469cfc6b17','850c5ce2-4f77-8308-e59f-5c6e1da9a8f0',75,'2020-08-27 17:49:53',0),('23cfb7bb-2cf7-c0cb-44ee-5f469c8987b8','3304ef15-3d54-d514-0f52-5f469cfc6b17','83497410-27b0-b41e-f49a-5c6e1df67a5f',90,'2020-08-27 17:49:53',0),('23fce9b1-9274-959f-87e3-5f469c29e80f','3304ef15-3d54-d514-0f52-5f469cfc6b17','7defbd7a-6478-d657-82e2-5c6e1ddffba3',75,'2020-08-27 17:49:53',0),('2447f896-7349-f5eb-2c77-5f469cc277bd','3304ef15-3d54-d514-0f52-5f469cfc6b17','86d7fbc5-843a-f9a3-a2f3-5c6e1d8c8ac7',90,'2020-08-27 17:49:53',0),('24769684-2651-e607-85a7-5f469c69dfac','3304ef15-3d54-d514-0f52-5f469cfc6b17','7c4b47cf-121d-f430-d03f-5c6e1d0d9029',75,'2020-08-27 17:49:53',0),('24a281fd-e8f4-cdcb-797c-5f469cdd2202','3304ef15-3d54-d514-0f52-5f469cfc6b17','d41751ce-361d-6116-ba6a-5c6e1d93e0c2',89,'2020-08-27 17:49:53',0),('24d4eb06-d5ea-8d53-d296-5f469c258774','3304ef15-3d54-d514-0f52-5f469cfc6b17','dba571ad-f302-219d-5f27-5c6e1dfa7f00',-99,'2020-08-27 17:49:53',0),('24e40439-9d76-47b2-f70f-5f469cafea7e','3304ef15-3d54-d514-0f52-5f469cfc6b17','74ebf998-0a54-6f0f-4a0c-5c6e1d95510e',-99,'2020-08-27 17:49:53',0),('25028046-a6dc-bb57-51e8-5f469cdf7a7a','3304ef15-3d54-d514-0f52-5f469cfc6b17','d9e62f26-2a44-8731-467e-5c6e1de31273',75,'2020-08-27 17:49:53',0),('255548d7-37aa-3322-43fe-5f469c9ff8cb','3304ef15-3d54-d514-0f52-5f469cfc6b17','df22d385-fffb-07f6-4cad-5c6e1d05385c',75,'2020-08-27 17:49:53',0),('2586f5c9-dc7f-33cd-e69f-5f469cb8fcc2','3304ef15-3d54-d514-0f52-5f469cfc6b17','dd53b539-625a-a770-e54b-5c6e1da3a732',90,'2020-08-27 17:49:53',0),('25b8436a-05cf-deb0-1f9e-5f469cda307e','3304ef15-3d54-d514-0f52-5f469cfc6b17','d82a4ba7-7cba-70f5-d7a4-5c6e1ddf0b3b',75,'2020-08-27 17:49:53',0),('25e3752d-a5f0-c20a-6ecd-5f469cf91dc1','3304ef15-3d54-d514-0f52-5f469cfc6b17','e137a998-bde5-ba95-c4c5-5c6e1d6dd872',90,'2020-08-27 17:49:53',0),('2612ed9d-8619-1090-35ad-5f469ce0762c','3304ef15-3d54-d514-0f52-5f469cfc6b17','d63d2d97-856c-40aa-a915-5c6e1d5908f3',75,'2020-08-27 17:49:53',0),('26651839-43be-d344-58dc-5f469c7dc0e2','3304ef15-3d54-d514-0f52-5f469cfc6b17','f0e63510-538e-a5f1-735f-5c6e1dee3384',89,'2020-08-27 17:49:53',0),('2691c41f-63ea-e4b2-2efc-5f469c5e9330','3304ef15-3d54-d514-0f52-5f469cfc6b17','3ef5062d-c353-a08c-c667-5c6e1d0d53b7',-99,'2020-08-27 17:49:53',0),('271409c7-9bb0-74e8-aa1e-5f469c5017d3','3304ef15-3d54-d514-0f52-5f469cfc6b17','211e0ddd-c042-9ae3-e81f-5c6e1d5431d7',75,'2020-08-27 17:49:53',0),('2771836a-b812-9e69-4610-5f469c4c2728','3304ef15-3d54-d514-0f52-5f469cfc6b17','79f90248-2bd9-8082-77c7-5c6e1d3272ae',75,'2020-08-27 17:49:53',0),('27a060b5-bc2b-de7a-c8c6-5f469cb37785','3304ef15-3d54-d514-0f52-5f469cfc6b17','5d500441-9b55-3adc-8e5a-5c6e1de5ab45',90,'2020-08-27 17:49:53',0),('27cd7966-d622-5013-7e5a-5f469cc684d6','3304ef15-3d54-d514-0f52-5f469cfc6b17','51900e3d-f193-7048-426c-5c6e1dfe209e',75,'2020-08-27 17:49:53',0),('27feef6c-27e7-93c0-d8e4-5f469c78bdb2','3304ef15-3d54-d514-0f52-5f469cfc6b17','950504b8-1278-d2c0-1d12-5c6e1ddfb235',90,'2020-08-27 17:49:53',0),('281304e8-9232-af73-ceea-5f469c23b56f','3304ef15-3d54-d514-0f52-5f469cfc6b17','721fe94f-3d15-b317-6350-5c6e1d44518e',75,'2020-08-27 17:49:53',0),('285ade2b-2c4b-a5d1-3107-5f469c07489e','3304ef15-3d54-d514-0f52-5f469cfc6b17','f29b36cd-339b-ecb7-97b9-5c6e1d60039c',75,'2020-08-27 17:49:53',0),('2888bdeb-2637-140d-30ed-5f469c79f643','3304ef15-3d54-d514-0f52-5f469cfc6b17','30d76631-82e8-b7fe-ddfa-5c6e1dd9abc2',89,'2020-08-27 17:49:53',0),('28b66423-9625-005b-05d2-5f469c8a9867','3304ef15-3d54-d514-0f52-5f469cfc6b17','388d4a70-5bce-7181-208f-5c6e1d88cade',-99,'2020-08-27 17:49:53',0),('29456244-484c-4321-6c3e-5f469c2c006b','3304ef15-3d54-d514-0f52-5f469cfc6b17','36bc1d33-71d2-bb7a-1bb4-5c6e1dcc38c1',75,'2020-08-27 17:49:53',0),('2973fe4a-78b8-3c87-6c10-5f469c06e336','3304ef15-3d54-d514-0f52-5f469cfc6b17','3c5a66b1-961d-620d-103f-5c6e1d689c1c',75,'2020-08-27 17:49:53',0),('29a3283a-594d-83c8-c644-5f469cdb5901','3304ef15-3d54-d514-0f52-5f469cfc6b17','3a77715d-b123-819f-19ae-5c6e1db13f47',90,'2020-08-27 17:49:53',0),('29d3e2dd-748a-9f60-a571-5f469cf68653','3304ef15-3d54-d514-0f52-5f469cfc6b17','34d441bb-0fe8-5906-d567-5c6e1dff5aef',75,'2020-08-27 17:49:53',0),('2a18b221-548a-1bc8-644c-5f469c4dc9f5','3304ef15-3d54-d514-0f52-5f469cfc6b17','3e4fc88a-0fb7-4a6e-935e-5c6e1da1cf38',90,'2020-08-27 17:49:53',0),('2a4797a1-4c44-06d7-dc58-5f469c7a9ac5','3304ef15-3d54-d514-0f52-5f469cfc6b17','32e6c274-6c87-2818-4db5-5c6e1dfe853d',75,'2020-08-27 17:49:53',0),('2a723aa4-0605-7272-e12e-5f469cc4cdf3','3304ef15-3d54-d514-0f52-5f469cfc6b17','cf862ecd-de63-de1c-8ae3-5c6e1da03a50',89,'2020-08-27 17:49:53',0),('2a9ec446-4c20-20ed-104e-5f469cb09157','3304ef15-3d54-d514-0f52-5f469cfc6b17','d602ff9a-88a3-20a1-36f0-5c6e1ddae899',-99,'2020-08-27 17:49:53',0),('2ad147a9-69d3-520c-f7b1-5f469c00e450','3304ef15-3d54-d514-0f52-5f469cfc6b17','d46dedb9-a982-67a2-d70a-5c6e1da04d41',75,'2020-08-27 17:49:53',0),('2ad60ea3-a9d2-fa62-0e8f-5f469c7a0da8','3304ef15-3d54-d514-0f52-5f469cfc6b17','7972c1cc-203b-4783-9cee-5c6e1d68a18e',75,'2020-08-27 17:49:53',0),('2b1d6c7a-e52d-b108-8048-5f469c780fa2','3304ef15-3d54-d514-0f52-5f469cfc6b17','d92f8b18-3f0e-d480-0183-5c6e1dea0580',75,'2020-08-27 17:49:53',0),('2b4f5ffd-1be0-7c06-4c40-5f469c49965b','3304ef15-3d54-d514-0f52-5f469cfc6b17','d79b79ce-5652-e699-c84a-5c6e1ddc8bc8',90,'2020-08-27 17:49:53',0),('2b7b295f-d8f3-1b4f-5fa4-5f469cc17cb8','3304ef15-3d54-d514-0f52-5f469cfc6b17','d2d19ea3-bf2c-67b9-87a6-5c6e1d2d2561',75,'2020-08-27 17:49:53',0),('2ba491ba-244b-4d92-fbc8-5f469cbfc6a3','3304ef15-3d54-d514-0f52-5f469cfc6b17','dacaaa84-1cf7-efaa-60c3-5c6e1d807a84',90,'2020-08-27 17:49:53',0),('2bd04320-2a48-6f0d-d245-5f469c0f10b8','3304ef15-3d54-d514-0f52-5f469cfc6b17','d122f516-ba83-0d8e-741a-5c6e1d51b454',75,'2020-08-27 17:49:53',0),('2c2024bd-4a72-6e4f-1fb6-5f469cb42264','3304ef15-3d54-d514-0f52-5f469cfc6b17','c8db86d1-7f26-b8aa-5323-5c6e1d4f870e',89,'2020-08-27 17:49:53',0),('2c4c45ca-745f-43e9-2735-5f469caec570','3304ef15-3d54-d514-0f52-5f469cfc6b17','d096a892-9823-074d-edd4-5c6e1dfe248d',-99,'2020-08-27 17:49:53',0),('2c759179-2a01-312d-5417-5f469c6081ba','3304ef15-3d54-d514-0f52-5f469cfc6b17','ced98a67-37f4-6707-c5ac-5c6e1da221c6',75,'2020-08-27 17:49:53',0),('2c9f73cb-0e41-b501-d80c-5f469c6c8871','3304ef15-3d54-d514-0f52-5f469cfc6b17','d3f3c098-4d7c-d2c6-54b3-5c6e1dc0a477',75,'2020-08-27 17:49:53',0),('2d2ec4e4-eaab-a5e4-89b1-5f469c7eb4d4','3304ef15-3d54-d514-0f52-5f469cfc6b17','d2429bba-7f54-24aa-1491-5c6e1db06ab6',90,'2020-08-27 17:49:53',0),('2dab0143-4ab7-a405-719a-5f469c50ec14','3304ef15-3d54-d514-0f52-5f469cfc6b17','77a70590-eb9e-e4e2-e99a-5c6e1df5b66c',90,'2020-08-27 17:49:53',0),('2dae8643-26a0-b8c7-aab4-5f469c131029','3304ef15-3d54-d514-0f52-5f469cfc6b17','cd02e82c-7eb8-8630-015d-5c6e1d54dd16',75,'2020-08-27 17:49:53',0),('2de48081-f137-1de9-90d2-5f469c720ca7','3304ef15-3d54-d514-0f52-5f469cfc6b17','d5b4803d-53f2-521a-2ee9-5c6e1d8e681b',90,'2020-08-27 17:49:53',0),('2e3bf6f3-8018-04b3-e2a0-5f469cbfa66d','3304ef15-3d54-d514-0f52-5f469cfc6b17','cb14dd05-873d-715e-92be-5c6e1d21e982',75,'2020-08-27 17:49:53',0),('2e75988c-f029-5d1c-5c8c-5f469cae69ef','3304ef15-3d54-d514-0f52-5f469cfc6b17','a18e13c0-7b00-9e5b-72c3-5c6e1d886c8c',89,'2020-08-27 17:49:53',0),('2ea9c9e8-8fc6-4ce9-89c0-5f469c258ca9','3304ef15-3d54-d514-0f52-5f469cfc6b17','a8f890a5-c581-7a99-4005-5c6e1d7b2ffd',-99,'2020-08-27 17:49:53',0),('2eef3a98-ddcb-d760-95f8-5f469c3e548c','3304ef15-3d54-d514-0f52-5f469cfc6b17','a711af50-e0eb-81dd-aec0-5c6e1d1b2ddd',75,'2020-08-27 17:49:53',0),('2f0911c3-dc73-4295-4056-5f47f1bfaac7','69d63f4e-88ef-58c9-0a28-5f469b0dc456','access',89,'2020-08-28 09:56:53',0),('2f3eb671-2df6-9efb-c9e0-5f469c984445','3304ef15-3d54-d514-0f52-5f469cfc6b17','ad0f4fee-d33e-2f3c-9f82-5c6e1d1e9a69',75,'2020-08-27 17:49:53',0),('2f731aba-d4b2-ccab-6b04-5f469c5fec02','3304ef15-3d54-d514-0f52-5f469cfc6b17','ab2242ca-d4fd-9549-e791-5c6e1d385890',90,'2020-08-27 17:49:53',0),('2fa17c4d-be63-b0f7-0c82-5f469cdf53a9','3304ef15-3d54-d514-0f52-5f469cfc6b17','a51ad6e5-d047-65d1-bc96-5c6e1d9b1f5c',75,'2020-08-27 17:49:53',0),('2face22a-5c9a-4a02-b989-5f47f17e2d26','69d63f4e-88ef-58c9-0a28-5f469b0dc456','delete',90,'2020-08-28 09:56:53',0),('2fd6d64e-b24e-46be-481b-5f469c48040f','3304ef15-3d54-d514-0f52-5f469cfc6b17','af3813f0-504b-4051-224b-5c6e1dfd4d58',90,'2020-08-27 17:49:53',0),('2ff7dc9d-c7bc-da9a-3ca1-5f47f1a3f49b','69d63f4e-88ef-58c9-0a28-5f469b0dc456','edit',90,'2020-08-28 09:56:53',0),('302ab17c-28f6-f33d-e4fc-5f469c8fc002','3304ef15-3d54-d514-0f52-5f469cfc6b17','a34e33e7-f029-be12-4f60-5c6e1d1ee705',75,'2020-08-27 17:49:53',0),('3038ebdc-ca4a-a66d-5b2b-5f47f1535767','69d63f4e-88ef-58c9-0a28-5f469b0dc456','export',90,'2020-08-28 09:56:53',0),('30582399-5c87-e8f6-4da9-5f469c050591','3304ef15-3d54-d514-0f52-5f469cfc6b17','b62a2bfc-2615-5abd-0a60-5c6e1d86946a',89,'2020-08-27 17:49:53',0),('3065a1a6-f0fc-6879-b12d-5f47f114f9bb','69d63f4e-88ef-58c9-0a28-5f469b0dc456','import',90,'2020-08-28 09:56:53',0),('3087a08d-2fa3-c985-472a-5f469ceee408','3304ef15-3d54-d514-0f52-5f469cfc6b17','bd16a03b-186c-145a-c165-5c6e1d2bb8aa',-99,'2020-08-27 17:49:53',0),('308f55b4-c420-7242-07bc-5f47f1e6fe17','69d63f4e-88ef-58c9-0a28-5f469b0dc456','list',90,'2020-08-28 09:56:53',0),('30bf9e6b-317f-0f32-0b9c-5f469c7f0255','3304ef15-3d54-d514-0f52-5f469cfc6b17','bb63e112-5c92-af82-2ed4-5c6e1d722c85',75,'2020-08-27 17:49:53',0),('30c28100-3a8b-4fa2-e43a-5f47f1b4b450','69d63f4e-88ef-58c9-0a28-5f469b0dc456','massupdate',90,'2020-08-28 09:56:53',0),('310f9e5b-a293-2c23-f8e0-5f469c88ea07','3304ef15-3d54-d514-0f52-5f469cfc6b17','c06bc7c3-f4ae-9709-ad32-5c6e1df9919b',75,'2020-08-27 17:49:53',0),('313af04f-4ae7-9600-f175-5f47f121d6ec','69d63f4e-88ef-58c9-0a28-5f469b0dc456','view',90,'2020-08-28 09:56:53',0),('313edcd7-86db-a9f2-d129-5f469c0233fd','3304ef15-3d54-d514-0f52-5f469cfc6b17','bebe1a01-5b96-efc4-c366-5c6e1dfc050d',90,'2020-08-27 17:49:53',0),('316dc589-1d37-ba4a-12e4-5f469c7d0f02','3304ef15-3d54-d514-0f52-5f469cfc6b17','b99c202c-a3f2-0e4c-117c-5c6e1d334507',75,'2020-08-27 17:49:53',0),('3173b00e-928a-a9af-ea94-5f47f10e90a9','69d63f4e-88ef-58c9-0a28-5f469b0dc456','e4e294a9-b18d-ee54-645d-5c6e1dd1e8c6',89,'2020-08-28 09:56:53',0),('31a04605-8243-2c12-b7e9-5f469cc021fe','3304ef15-3d54-d514-0f52-5f469cfc6b17','c216ae81-667d-57c6-1b66-5c6e1d66fedc',90,'2020-08-27 17:49:53',0),('31a1a0c4-4bbb-3cc0-a75d-5f47f11781a0','69d63f4e-88ef-58c9-0a28-5f469b0dc456','eb6ad108-3dbe-6c3f-6e09-5c6e1d42e30d',90,'2020-08-28 09:56:53',0),('31e482db-ea42-bc55-dcd1-5f47f1f69312','69d63f4e-88ef-58c9-0a28-5f469b0dc456','e9c4c00e-5b31-7e5e-ed1c-5c6e1d771e86',90,'2020-08-28 09:56:53',0),('31f33093-0760-834f-e642-5f469c399b3e','3304ef15-3d54-d514-0f52-5f469cfc6b17','b7d811b6-4b23-7df0-5897-5c6e1d72ffd6',75,'2020-08-27 17:49:53',0),('320cb047-aaad-a5fa-4045-5f47f183893b','69d63f4e-88ef-58c9-0a28-5f469b0dc456','eea809c6-5494-ebd8-ea2a-5c6e1d2a1bc3',90,'2020-08-28 09:56:53',0),('32390bab-4c40-1448-e019-5f47f1f95cbd','69d63f4e-88ef-58c9-0a28-5f469b0dc456','ed085713-2fef-f3a6-d794-5c6e1d5116c5',90,'2020-08-28 09:56:53',0),('32480b37-0cbe-d483-27b4-5f469c906587','3304ef15-3d54-d514-0f52-5f469cfc6b17','7046a698-1203-22a6-6d4f-5c6e1d2fe533',75,'2020-08-27 17:49:53',0),('3263d127-0c68-279d-1d75-5f47f12ac884','69d63f4e-88ef-58c9-0a28-5f469b0dc456','e81b1376-eea5-89f3-2af6-5c6e1d01ec62',90,'2020-08-28 09:56:53',0),('32784361-07fb-cb22-14fd-5f469cb617d9','3304ef15-3d54-d514-0f52-5f469cfc6b17','7a0c717c-743b-9dff-0e33-5c6e1d5c14d4',89,'2020-08-27 17:49:53',0),('328bfd69-c26f-f40a-630a-5f47f1e36d69','69d63f4e-88ef-58c9-0a28-5f469b0dc456','f04405bc-ba89-bfc6-c5a7-5c6e1db2a756',90,'2020-08-28 09:56:53',0),('32af670f-18ed-2f1a-dae0-5f469c2ab800','3304ef15-3d54-d514-0f52-5f469cfc6b17','80f9ae9e-3727-299f-afd5-5c6e1d0fa123',-99,'2020-08-27 17:49:53',0),('32be3c9f-9a5f-08b5-6a1e-5f47f1d81383','69d63f4e-88ef-58c9-0a28-5f469b0dc456','e67f4d71-4b5b-0f8b-e816-5c6e1d6cbfdc',90,'2020-08-28 09:56:53',0),('32f86f41-9958-9f03-3e3b-5f469cc1a197','3304ef15-3d54-d514-0f52-5f469cfc6b17','7f33c032-982b-f7e2-c3de-5c6e1d0aa89d',75,'2020-08-27 17:49:53',0),('32ffbb01-0814-be46-8062-5f47f173e82c','69d63f4e-88ef-58c9-0a28-5f469b0dc456','9842d020-e157-2c63-4eca-5c6e1d7d10aa',89,'2020-08-28 09:56:53',0),('33291cfc-25b1-c82b-2bca-5f469c857273','3304ef15-3d54-d514-0f52-5f469cfc6b17','845ceb37-c664-d7bb-14d7-5c6e1df0b770',75,'2020-08-27 17:49:53',0),('332c6550-70f9-e6d4-6bd5-5f47f17c58e0','69d63f4e-88ef-58c9-0a28-5f469b0dc456','9eb0255f-c82c-8880-a4e3-5c6e1d003339',90,'2020-08-28 09:56:53',0),('335694e7-3131-6526-7ab5-5f47f178586d','69d63f4e-88ef-58c9-0a28-5f469b0dc456','9d197071-f115-80c8-ddbe-5c6e1d19c577',90,'2020-08-28 09:56:53',0),('3356b223-5ec2-8c66-6c0c-5f469c856ef0','3304ef15-3d54-d514-0f52-5f469cfc6b17','82a9c474-0dc1-c281-491e-5c6e1d7b27b4',90,'2020-08-27 17:49:53',0),('3382f074-8bc7-343c-2851-5f47f1f4bd4d','69d63f4e-88ef-58c9-0a28-5f469b0dc456','a263e131-bb1e-2f23-e69c-5c6e1db832c5',90,'2020-08-28 09:56:53',0),('3388b8e0-eeb0-2132-13a1-5f469c550588','3304ef15-3d54-d514-0f52-5f469cfc6b17','7d797f14-23ec-eae0-8e4d-5c6e1dd6311f',75,'2020-08-27 17:49:53',0),('33b50fc1-d623-d3f8-0ac2-5f47f18e7102','69d63f4e-88ef-58c9-0a28-5f469b0dc456','a053e50a-04cd-914e-fa5b-5c6e1d5600c1',90,'2020-08-28 09:56:53',0),('33bc0add-f363-55f3-2e94-5f469c5bb857','3304ef15-3d54-d514-0f52-5f469cfc6b17','860c4f68-86d7-ef63-880b-5c6e1df3f6e6',90,'2020-08-27 17:49:53',0),('33fa11de-889e-5a85-def6-5f47f1500f12','69d63f4e-88ef-58c9-0a28-5f469b0dc456','9b8488a5-8b90-64fc-cc6a-5c6e1deafdb8',90,'2020-08-28 09:56:53',0),('3407eaae-ba29-d56c-4dc5-5f469c0fe9ab','3304ef15-3d54-d514-0f52-5f469cfc6b17','7bcaac3e-0c7f-302d-e870-5c6e1dfb18f0',75,'2020-08-27 17:49:53',0),('34289024-d826-a049-cece-5f47f1b4820d','69d63f4e-88ef-58c9-0a28-5f469b0dc456','a4160e91-0182-6aca-6626-5c6e1dd95415',90,'2020-08-28 09:56:53',0),('3447873f-7baa-1371-c780-5f469c8c236b','3304ef15-3d54-d514-0f52-5f469cfc6b17','8d5f0375-04e8-9374-548c-5c6e1d4185cb',89,'2020-08-27 17:49:53',0),('34675538-6712-9d9b-ca5a-5f47f1168994','69d63f4e-88ef-58c9-0a28-5f469b0dc456','99ed1f96-3c96-5191-aacb-5c6e1db0791e',90,'2020-08-28 09:56:53',0),('34766ba6-3955-9a3a-133d-5f469cc2537e','3304ef15-3d54-d514-0f52-5f469cfc6b17','94cd0212-7227-a254-b111-5c6e1d48d71c',-99,'2020-08-27 17:49:53',0),('3491b149-a6e5-52a0-5003-5f47f13de259','69d63f4e-88ef-58c9-0a28-5f469b0dc456','2add29bf-94df-524f-816b-5c6e1df26606',89,'2020-08-28 09:56:53',0),('34a8452f-2dab-1554-247b-5f469c3c95a0','3304ef15-3d54-d514-0f52-5f469cfc6b17','92f3f737-caa2-dd8a-8a13-5c6e1d4916b9',75,'2020-08-27 17:49:53',0),('34d58373-5d94-dce3-9829-5f47f1ee711a','69d63f4e-88ef-58c9-0a28-5f469b0dc456','31f99891-140f-0f15-8210-5c6e1da1c586',90,'2020-08-28 09:56:53',0),('34fe06be-25c7-4f73-e6df-5f469c974bc9','3304ef15-3d54-d514-0f52-5f469cfc6b17','9867e242-ed91-e741-d623-5c6e1d746db4',75,'2020-08-27 17:49:53',0),('34ff0d67-ce4a-53c7-cd34-5f47f1a38cc3','69d63f4e-88ef-58c9-0a28-5f469b0dc456','304550da-9a68-1c5d-70b9-5c6e1d0308c6',90,'2020-08-28 09:56:53',0),('3528eb78-b021-b7ec-a808-5f47f125256b','69d63f4e-88ef-58c9-0a28-5f469b0dc456','352832c1-13a4-6df3-47b0-5c6e1d95364b',90,'2020-08-28 09:56:53',0),('3555d086-6bf2-2147-3d2f-5f47f12a4de4','69d63f4e-88ef-58c9-0a28-5f469b0dc456','33977c10-585e-e3e4-76b3-5c6e1d29503c',90,'2020-08-28 09:56:53',0),('3583c8a9-e02b-64cc-59f6-5f47f1aee470','69d63f4e-88ef-58c9-0a28-5f469b0dc456','2e4a26b7-6b7b-55c2-11db-5c6e1d2fe155',90,'2020-08-28 09:56:53',0),('35af81e0-3e24-27fc-e1d2-5f47f143a9ae','69d63f4e-88ef-58c9-0a28-5f469b0dc456','36b9e4fa-f86d-727e-fca7-5c6e1de8e18d',90,'2020-08-28 09:56:53',0),('35b629d0-7152-c94c-2f38-5f469cc8cd85','3304ef15-3d54-d514-0f52-5f469cfc6b17','96a17d4d-5ab9-2ed8-c2a4-5c6e1d987a1c',90,'2020-08-27 17:49:53',0),('35df0bef-263c-2bea-88ae-5f469c1c0a0b','3304ef15-3d54-d514-0f52-5f469cfc6b17','7b49e17b-7855-cfbf-fe80-5c6e1d2f9182',90,'2020-08-27 17:49:53',0),('35eb916c-e3d5-a300-7f3f-5f47f1463287','69d63f4e-88ef-58c9-0a28-5f469b0dc456','2c76062e-78dc-b6f6-36df-5c6e1d53d9e3',90,'2020-08-28 09:56:53',0),('3605f304-d2c3-6abd-661a-5f469c4be64b','3304ef15-3d54-d514-0f52-5f469cfc6b17','9114c47d-c20b-a4c8-c2a6-5c6e1da9acb1',75,'2020-08-27 17:49:53',0),('361b7135-0cf2-44e3-71d3-5f47f11e78b5','69d63f4e-88ef-58c9-0a28-5f469b0dc456','40a52983-5401-23cb-9769-5c6e1dc74de0',89,'2020-08-28 09:56:53',0),('3633372e-4d71-2ed3-00ca-5f469c70a785','3304ef15-3d54-d514-0f52-5f469cfc6b17','9a32ca04-50c7-66b7-3185-5c6e1d384a7b',90,'2020-08-27 17:49:53',0),('364bfcbd-2787-e06f-7092-5f47f1493960','69d63f4e-88ef-58c9-0a28-5f469b0dc456','471273a9-50cb-30b6-1772-5c6e1d4750af',90,'2020-08-28 09:56:53',0),('365da08b-86bd-2aa5-601f-5f469c0b7f69','3304ef15-3d54-d514-0f52-5f469cfc6b17','8f3ad6ec-99d2-f98e-7785-5c6e1db67802',75,'2020-08-27 17:49:53',0),('3677cccb-f974-8e21-aff3-5f47f1d42a27','69d63f4e-88ef-58c9-0a28-5f469b0dc456','4574d08f-6962-323a-85a8-5c6e1d5971c5',90,'2020-08-28 09:56:53',0),('368fb1b4-f9ab-bdfe-2dac-5f469c2f6e77','3304ef15-3d54-d514-0f52-5f469cfc6b17','9d5add96-09e7-13e4-5533-5c6e1d5eabe4',89,'2020-08-27 17:49:53',0),('36a4cc87-ba68-3dd0-0dde-5f47f1f1fbd2','69d63f4e-88ef-58c9-0a28-5f469b0dc456','4a57e684-d9e3-d7f2-83fd-5c6e1d1dc552',90,'2020-08-28 09:56:53',0),('36d823aa-8603-06cf-170b-5f469cf02a02','3304ef15-3d54-d514-0f52-5f469cfc6b17','a3b6eade-aa75-fdfb-bb22-5c6e1d132305',-99,'2020-08-27 17:49:53',0),('36e5426e-7646-419f-d3e2-5f47f1524234','69d63f4e-88ef-58c9-0a28-5f469b0dc456','48bad485-ae5a-7893-e9d9-5c6e1df2102e',90,'2020-08-28 09:56:53',0),('370a5748-ceea-1eef-f2f8-5f469c638c7e','3304ef15-3d54-d514-0f52-5f469cfc6b17','a221846d-edb3-d08b-955d-5c6e1de207c6',75,'2020-08-27 17:49:53',0),('370f21dd-fc72-0c1e-969b-5f47f1fc85e8','69d63f4e-88ef-58c9-0a28-5f469b0dc456','43dcc714-a359-b934-1906-5c6e1d8e6d82',90,'2020-08-28 09:56:53',0),('374704fb-464e-1c16-7484-5f469c419c52','3304ef15-3d54-d514-0f52-5f469cfc6b17','a6eaafe1-3a6e-8a53-b119-5c6e1db76720',75,'2020-08-27 17:49:53',0),('37494654-6920-c0da-5c5b-5f47f1844421','69d63f4e-88ef-58c9-0a28-5f469b0dc456','4bf15a14-a85b-47a9-8349-5c6e1d3aa8fe',90,'2020-08-28 09:56:53',0),('3773c603-d97b-e75f-ee3f-5f47f19f6e3f','69d63f4e-88ef-58c9-0a28-5f469b0dc456','42465f76-4843-8654-22a2-5c6e1db7e678',90,'2020-08-28 09:56:53',0),('37747bd1-bed9-1170-5b3f-5f469cee8c22','3304ef15-3d54-d514-0f52-5f469cfc6b17','a5507fba-e80d-d18b-7347-5c6e1d7ca48d',90,'2020-08-27 17:49:53',0),('379d0ab6-a124-49c4-9e1b-5f47f1ed7baf','69d63f4e-88ef-58c9-0a28-5f469b0dc456','1b958cf9-ae40-438b-f8c5-5c6e1da93153',89,'2020-08-28 09:56:53',0),('37a7a259-267d-17b9-c171-5f469cc79197','3304ef15-3d54-d514-0f52-5f469cfc6b17','a08e9c0b-d79a-1e4c-baed-5c6e1dd818bb',75,'2020-08-27 17:49:53',0),('37e0dd06-0501-bd04-6ea4-5f47f170d6fc','69d63f4e-88ef-58c9-0a28-5f469b0dc456','232e2ca6-d75b-da80-9dfe-5c6e1de86dd1',90,'2020-08-28 09:56:53',0),('380019e0-271d-cea1-7639-5f469c89f7be','3304ef15-3d54-d514-0f52-5f469cfc6b17','a897f543-7d27-85ff-87fa-5c6e1dffec0f',90,'2020-08-27 17:49:53',0),('380f3ed9-e038-9be9-85d5-5f47f170d3d8','69d63f4e-88ef-58c9-0a28-5f469b0dc456','2169ee86-765e-bd5e-d6ca-5c6e1d9ae49c',90,'2020-08-28 09:56:53',0),('3837903f-cbed-382d-604a-5f469c2f4128','3304ef15-3d54-d514-0f52-5f469cfc6b17','9ef99bea-c305-cfbf-a4ba-5c6e1dc6b98d',75,'2020-08-27 17:49:53',0),('383a3a3c-fa6c-3e42-65a4-5f47f1fc0e03','69d63f4e-88ef-58c9-0a28-5f469b0dc456','26f60559-7804-b3e2-2c5a-5c6e1d08216c',90,'2020-08-28 09:56:53',0),('38661a19-961c-f56d-daf7-5f47f1e2b06c','69d63f4e-88ef-58c9-0a28-5f469b0dc456','2507c934-350d-93e7-6009-5c6e1d1a4ebf',90,'2020-08-28 09:56:53',0),('3868bc9c-3590-05d1-6676-5f469c6eb1c6','3304ef15-3d54-d514-0f52-5f469cfc6b17','8c28ae8a-3b44-22b0-8c48-5c6e1db709ed',89,'2020-08-27 17:49:53',0),('3890a3a8-b3fa-33a4-7b9d-5f47f1698c64','69d63f4e-88ef-58c9-0a28-5f469b0dc456','1f915e87-b617-1952-6ea3-5c6e1d890550',90,'2020-08-28 09:56:53',0),('389d8795-561c-5e60-52ce-5f469c86afba','3304ef15-3d54-d514-0f52-5f469cfc6b17','929da18d-66a1-fac9-8cbf-5c6e1d22072a',-99,'2020-08-27 17:49:53',0),('38d02eb8-89dd-f18d-420d-5f47f1a8913b','69d63f4e-88ef-58c9-0a28-5f469b0dc456','28ff06fc-14b0-98ec-737f-5c6e1d816cc7',90,'2020-08-28 09:56:53',0),('38e27dcb-52d3-2023-8de7-5f469cece75d','3304ef15-3d54-d514-0f52-5f469cfc6b17','90fd3e60-d40b-fcd4-fb55-5c6e1dae8fe0',75,'2020-08-27 17:49:53',0),('38fa1654-7710-c349-aaa2-5f47f1b4ee81','69d63f4e-88ef-58c9-0a28-5f469b0dc456','1d94faaf-6dae-f882-8d0e-5c6e1dee4687',90,'2020-08-28 09:56:53',0),('39130d34-1caf-1bfb-de51-5f469c1e7d93','3304ef15-3d54-d514-0f52-5f469cfc6b17','95ce0dd9-8872-02a1-0db3-5c6e1d9bcf73',75,'2020-08-27 17:49:53',0),('3924bba7-e60f-a7f0-35bc-5f47f1e8b6c0','69d63f4e-88ef-58c9-0a28-5f469b0dc456','78bc3e90-a6f2-9b33-2c95-5c6e1d8c1327',89,'2020-08-28 09:56:53',0),('394cedf4-c95a-b4b1-dd1e-5f47f11bd0cc','69d63f4e-88ef-58c9-0a28-5f469b0dc456','7f2175cd-db29-b4f8-3753-5c6e1d46bab5',90,'2020-08-28 09:56:53',0),('394e613f-2ba0-5945-50d3-5f469cfc5f69','3304ef15-3d54-d514-0f52-5f469cfc6b17','94383006-21ad-710b-5dc1-5c6e1dfd3bb5',90,'2020-08-27 17:49:53',0),('395a01ff-3d58-168e-8aef-5f469cfc959d','3304ef15-3d54-d514-0f52-5f469cfc6b17','6e74df3c-97d7-ff5a-e257-5c6e1d89cdcf',75,'2020-08-27 17:49:53',0),('39767ef4-fa84-9015-f89f-5f47f117544d','69d63f4e-88ef-58c9-0a28-5f469b0dc456','7d8d7347-86e8-73a0-1348-5c6e1d86190b',90,'2020-08-28 09:56:53',0),('39cda6d2-b0d1-1a6e-b60e-5f469cc89c0f','3304ef15-3d54-d514-0f52-5f469cfc6b17','8f5dfb14-8ef8-cee3-ccc8-5c6e1da591cc',75,'2020-08-27 17:49:53',0),('39ff6627-142c-4883-83ea-5f469cbd827c','3304ef15-3d54-d514-0f52-5f469cfc6b17','97680cfd-ea91-f40b-322a-5c6e1d972dd4',90,'2020-08-27 17:49:53',0),('3a03e351-a361-32b6-5dc7-5f47f1f04ff6','69d63f4e-88ef-58c9-0a28-5f469b0dc456','824f5a15-53b1-4755-d578-5c6e1d0a0440',90,'2020-08-28 09:56:53',0),('3a2f41bc-9d20-0c68-fee1-5f469c3c7971','3304ef15-3d54-d514-0f52-5f469cfc6b17','8dc2b087-f5ec-cf51-7690-5c6e1d369edb',75,'2020-08-27 17:49:53',0),('3a405eb2-c5b8-936d-5b35-5f47f1aa1da4','69d63f4e-88ef-58c9-0a28-5f469b0dc456','80b641fa-5eda-2510-e682-5c6e1dd24ed4',90,'2020-08-28 09:56:53',0),('3a5e309e-149c-3b22-b8e6-5f469c152db3','3304ef15-3d54-d514-0f52-5f469cfc6b17','76c626c4-b247-8bde-6d8b-5c6e1d204ff2',89,'2020-08-27 17:49:53',0),('3a6adc6c-af70-1086-8436-5f47f14ddcbf','69d63f4e-88ef-58c9-0a28-5f469b0dc456','7bf1dc51-d297-b1c0-0c9a-5c6e1d3778ab',90,'2020-08-28 09:56:53',0),('3a8b0209-c90b-efe0-23c1-5f469c41df56','3304ef15-3d54-d514-0f52-5f469cfc6b17','7d74c28a-6fd6-3816-ea14-5c6e1d65a241',-99,'2020-08-27 17:49:53',0),('3abf5c1b-b26d-e349-e68d-5f47f1818233','69d63f4e-88ef-58c9-0a28-5f469b0dc456','83e9503e-4d1d-2241-0894-5c6e1d2d5ad3',90,'2020-08-28 09:56:53',0),('3ad67a93-1873-2eae-21ba-5f469cbbbefd','3304ef15-3d54-d514-0f52-5f469cfc6b17','7bd8bdff-cee3-fb8f-84c6-5c6e1dcd0834',75,'2020-08-27 17:49:53',0),('3af38117-a496-3a9e-27fa-5f47f1a6dd74','69d63f4e-88ef-58c9-0a28-5f469b0dc456','7a597959-caf4-8477-c2f8-5c6e1d6c7ccd',90,'2020-08-28 09:56:53',0),('3b10347b-ec6a-e42c-36e0-5f469cbafe30','3304ef15-3d54-d514-0f52-5f469cfc6b17','80c063d9-4753-653b-4561-5c6e1d3c54db',75,'2020-08-27 17:49:53',0),('3b2a1574-860c-445c-0438-5f47f129c276','69d63f4e-88ef-58c9-0a28-5f469b0dc456','ac28a8b7-049a-fc4a-e4de-5c6e1d1423a4',89,'2020-08-28 09:56:53',0),('3b43cb34-b447-e2e2-4915-5f469c65ef9e','3304ef15-3d54-d514-0f52-5f469cfc6b17','7f16d69c-e29b-910f-4d1e-5c6e1d698cd8',90,'2020-08-27 17:49:53',0),('3b5d5096-4be9-ee40-3377-5f47f1820645','69d63f4e-88ef-58c9-0a28-5f469b0dc456','b2e44f1d-2865-1aec-18e0-5c6e1d495fff',90,'2020-08-28 09:56:53',0),('3b7f8fd6-582f-6ac2-1ee2-5f469c500f78','3304ef15-3d54-d514-0f52-5f469cfc6b17','7a370c0c-ffdd-d739-8f39-5c6e1dccc3c0',75,'2020-08-27 17:49:53',0),('3b97e6da-5008-17bf-d5c7-5f47f179e416','69d63f4e-88ef-58c9-0a28-5f469b0dc456','b1418e90-c9f7-20cb-7771-5c6e1d6469aa',90,'2020-08-28 09:56:53',0),('3bcf7bb5-a89e-9fd4-82d0-5f469ca493e7','3304ef15-3d54-d514-0f52-5f469cfc6b17','825eecdb-807d-fcea-97f9-5c6e1db6ed59',90,'2020-08-27 17:49:53',0),('3bf06dea-6385-1ad5-7f48-5f47f180bf33','69d63f4e-88ef-58c9-0a28-5f469b0dc456','b6261178-cb2f-3c4d-318a-5c6e1d71d570',90,'2020-08-28 09:56:53',0),('3bfb4144-e14d-8597-2bd1-5f469c5205be','3304ef15-3d54-d514-0f52-5f469cfc6b17','78628393-5890-71b9-4644-5c6e1dbb5160',75,'2020-08-27 17:49:53',0),('3c3047fa-6a86-e257-3642-5f47f1b9a17f','69d63f4e-88ef-58c9-0a28-5f469b0dc456','b4845172-d53e-fede-4687-5c6e1da11cd4',90,'2020-08-28 09:56:53',0),('3c322af3-0d0e-94b9-e593-5f469c64e49a','3304ef15-3d54-d514-0f52-5f469cfc6b17','601fdecf-a626-35b3-9142-5c6e1d3b2ee3',89,'2020-08-27 17:49:53',0),('3c65b971-e4d5-919f-bc01-5f469c32e073','3304ef15-3d54-d514-0f52-5f469cfc6b17','6722c508-1381-f2ac-df11-5c6e1d531d47',-99,'2020-08-27 17:49:53',0),('3c725ee4-77c0-a1ff-2b10-5f47f1c75f61','69d63f4e-88ef-58c9-0a28-5f469b0dc456','af9542f4-0edd-1926-48b7-5c6e1d8f9893',90,'2020-08-28 09:56:53',0),('3cacd7a0-da36-aa42-09c7-5f469c22b092','3304ef15-3d54-d514-0f52-5f469cfc6b17','65632a8e-c384-0ba2-6405-5c6e1d997e7f',75,'2020-08-27 17:49:53',0),('3cc3ab3a-9e74-bad9-bb6d-5f47f11fedfe','69d63f4e-88ef-58c9-0a28-5f469b0dc456','b7c94ce9-b840-0559-bbb4-5c6e1d293842',90,'2020-08-28 09:56:53',0),('3cf304b8-97f8-173a-54c0-5f469c4f7ccc','3304ef15-3d54-d514-0f52-5f469cfc6b17','bef20eff-0b7e-c1bb-7a8e-5c6e1df0685c',89,'2020-08-27 17:49:53',0),('3cf6e371-54df-6bb4-1611-5f47f190d7a7','69d63f4e-88ef-58c9-0a28-5f469b0dc456','ade60ebc-4744-3eb6-a3e1-5c6e1d94a89b',90,'2020-08-28 09:56:53',0),('3d1a1990-33f4-21f7-51e3-5f469cb81a00','3304ef15-3d54-d514-0f52-5f469cfc6b17','6adabb34-e2ff-75ee-8f62-5c6e1d00e805',75,'2020-08-27 17:49:53',0),('3d297362-49a3-8bd4-8a17-5f47f17c81d2','69d63f4e-88ef-58c9-0a28-5f469b0dc456','be604b96-6736-7e81-6d15-5c6e1d5dab3e',89,'2020-08-28 09:56:53',0),('3d49773f-4a50-32d2-131d-5f469c4b29ed','3304ef15-3d54-d514-0f52-5f469cfc6b17','68ee4e88-d2a3-9e0f-ea4a-5c6e1dc8db73',90,'2020-08-27 17:49:53',0),('3d6b4bb1-ddde-6469-83d1-5f47f1f80023','69d63f4e-88ef-58c9-0a28-5f469b0dc456','c58c2f62-5135-6531-5629-5c6e1d7895af',90,'2020-08-28 09:56:53',0),('3d79a57b-f690-d6db-55a6-5f469c109204','3304ef15-3d54-d514-0f52-5f469cfc6b17','63a5cabe-966f-2e06-25f3-5c6e1d6ba561',75,'2020-08-27 17:49:53',0),('3db7f513-c8d5-cfe3-3a7b-5f469caeb2c9','3304ef15-3d54-d514-0f52-5f469cfc6b17','6dc4083a-2d47-470b-8335-5c6e1d46b191',90,'2020-08-27 17:49:53',0),('3dc9f2e3-06de-cf4b-5742-5f47f19b3434','69d63f4e-88ef-58c9-0a28-5f469b0dc456','c3a67730-0e9c-4380-7ead-5c6e1df50457',90,'2020-08-28 09:56:53',0),('3de86642-c2f0-ce3c-8576-5f469ca096db','3304ef15-3d54-d514-0f52-5f469cfc6b17','61ea6896-ce86-401e-d0bf-5c6e1d0f725e',75,'2020-08-27 17:49:53',0),('3e105ff2-aa5d-a3b6-600f-5f47f1133198','69d63f4e-88ef-58c9-0a28-5f469b0dc456','c95817c6-d1e2-2fea-2350-5c6e1d6aa3fe',90,'2020-08-28 09:56:53',0),('3e14688a-3165-aea5-c030-5f469cc40e85','3304ef15-3d54-d514-0f52-5f469cfc6b17','9c434503-c420-8ea4-fed4-5e909d093d3f',89,'2020-08-27 17:49:53',0),('3e3c93ba-329e-76c2-ff80-5f469cf14d05','3304ef15-3d54-d514-0f52-5f469cfc6b17','a80b7337-2bdb-fb18-8a1c-5e909dbccfa9',-99,'2020-08-27 17:49:53',0),('3e418c61-fd4f-63c4-33a8-5f47f11373a9','69d63f4e-88ef-58c9-0a28-5f469b0dc456','c76b891a-5d75-28e1-7037-5c6e1d34fc61',90,'2020-08-28 09:56:53',0),('3e661b89-8093-5c23-12d3-5f469c4d3b04','3304ef15-3d54-d514-0f52-5f469cfc6b17','a4e20e36-1f69-ef86-e53f-5e909d53c1fd',75,'2020-08-27 17:49:53',0),('3e722ff8-7d97-4cf9-cc64-5f47f17725cb','69d63f4e-88ef-58c9-0a28-5f469b0dc456','c1cc01ac-a509-0cfd-5218-5c6e1d18f2cd',90,'2020-08-28 09:56:53',0),('3ea798d1-f1ca-5b74-ecd4-5f469cd63f6c','3304ef15-3d54-d514-0f52-5f469cfc6b17','ad806956-f797-6110-5c34-5e909d976f18',75,'2020-08-27 17:49:53',0),('3ec01355-b842-39e3-b72d-5f47f16171b5','69d63f4e-88ef-58c9-0a28-5f469b0dc456','cb44e564-ea4c-4625-16e8-5c6e1d0a6ed8',90,'2020-08-28 09:56:53',0),('3ed126aa-eb4f-6254-60ac-5f469c58768a','3304ef15-3d54-d514-0f52-5f469cfc6b17','aacba1b3-9082-850d-7134-5e909daf8e6d',90,'2020-08-27 17:49:53',0),('3eef0483-5628-7ab2-36b6-5f47f1f556a1','69d63f4e-88ef-58c9-0a28-5f469b0dc456','c0085f74-6446-7bec-6b0f-5c6e1d2d824b',90,'2020-08-28 09:56:53',0),('3efc55cf-75dd-4eba-b617-5f469ca148ee','3304ef15-3d54-d514-0f52-5f469cfc6b17','a208420e-202f-0a8e-21cc-5e909d8ce949',75,'2020-08-27 17:49:53',0),('3f23fe45-6a7e-aa2d-6df2-5f47f1002878','69d63f4e-88ef-58c9-0a28-5f469b0dc456','f1e9306e-dc78-fda0-504a-5c6e1d689e6e',89,'2020-08-28 09:56:53',0),('3f2b3c5f-83c7-00c0-79b0-5f469c31a657','3304ef15-3d54-d514-0f52-5f469cfc6b17','b0990c1d-77d5-2980-28a5-5e909dd6a00f',90,'2020-08-27 17:49:53',0),('3f30044a-f0e9-9adb-ffe3-5f469c3248b3','3304ef15-3d54-d514-0f52-5f469cfc6b17','e32106b1-5bea-8fc2-af47-5c6e1d9e72f7',-99,'2020-08-27 17:49:53',0),('3f5984eb-6825-82cc-1508-5f469c4e3062','3304ef15-3d54-d514-0f52-5f469cfc6b17','9f5c2b59-2fef-66ef-9bcb-5e909dafc23b',75,'2020-08-27 17:49:53',0),('3f5d7f85-3081-fc03-f379-5f47f1c097a6','69d63f4e-88ef-58c9-0a28-5f469b0dc456','419b0ff8-8bd3-c967-1f1c-5c6e1d4e8fb9',90,'2020-08-28 09:56:53',0),('3fa32cfa-81db-94f0-7798-5f469c5ef524','3304ef15-3d54-d514-0f52-5f469cfc6b17','80f68222-9e49-e9fa-1164-5e909d2a6681',89,'2020-08-27 17:49:53',0),('3fa9173b-098e-e836-254f-5f47f1edb41a','69d63f4e-88ef-58c9-0a28-5f469b0dc456','28140bad-21e3-58b2-5adf-5c6e1d2a37ec',90,'2020-08-28 09:56:53',0),('3fce7c65-6923-5f2f-ea0b-5f469cbda253','3304ef15-3d54-d514-0f52-5f469cfc6b17','8c37a3f8-e121-94b9-a6df-5e909d1dff74',-99,'2020-08-27 17:49:53',0),('3fd6e474-99b8-e44a-8377-5f47f146b79e','69d63f4e-88ef-58c9-0a28-5f469b0dc456','746106cb-0c40-293e-cdbe-5c6e1deb825e',90,'2020-08-28 09:56:53',0),('4002cee4-ce0c-761b-0036-5f47f11075dc','69d63f4e-88ef-58c9-0a28-5f469b0dc456','5b420296-9b9b-f422-444c-5c6e1d94f411',90,'2020-08-28 09:56:53',0),('400fa074-0203-4363-efe8-5f469c6264eb','3304ef15-3d54-d514-0f52-5f469cfc6b17','8996b5a7-524c-6482-4fd5-5e909dd4afa9',75,'2020-08-27 17:49:53',0),('4030b3e9-d0f9-2ddb-8ce4-5f47f141848d','69d63f4e-88ef-58c9-0a28-5f469b0dc456','ef70059e-eb96-9eb7-e071-5c6e1d41766b',90,'2020-08-28 09:56:53',0),('403c0e62-81e3-885c-334e-5f469ce14821','3304ef15-3d54-d514-0f52-5f469cfc6b17','916ef2d8-8108-7977-e586-5e909d2cf1ca',75,'2020-08-27 17:49:53',0),('405f34a8-7e08-61e9-c8b9-5f47f163e34a','69d63f4e-88ef-58c9-0a28-5f469b0dc456','8d830de8-f2d0-21b8-09cb-5c6e1d131f85',90,'2020-08-28 09:56:53',0),('40665f03-26c3-f94d-d869-5f469c20906c','3304ef15-3d54-d514-0f52-5f469cfc6b17','8ed5f075-fc75-10d1-ad3b-5e909dd49592',90,'2020-08-27 17:49:53',0),('40a9b606-b4ce-739a-caa5-5f47f1181e67','69d63f4e-88ef-58c9-0a28-5f469b0dc456','f384474f-3e33-9f86-bdf5-5c6e1d2ca1a1',90,'2020-08-28 09:56:53',0),('40abb064-36e5-ac9e-d0bf-5f469c1d4a0e','3304ef15-3d54-d514-0f52-5f469cfc6b17','867e07d4-c36e-2ff7-cb57-5e909da7e961',75,'2020-08-27 17:49:53',0),('40d509e3-800a-b2fe-e97a-5f47f1d609e5','69d63f4e-88ef-58c9-0a28-5f469b0dc456','ce4558d9-c83f-511d-b67b-5c6e1dff0232',89,'2020-08-28 09:56:53',0),('40d956a2-2151-9c01-20bd-5f469c5ee2ae','3304ef15-3d54-d514-0f52-5f469cfc6b17','943de675-7b7d-dac8-7dfb-5e909d7e0319',90,'2020-08-27 17:49:53',0),('41000e42-49d6-73ac-91b1-5f47f1797fa8','69d63f4e-88ef-58c9-0a28-5f469b0dc456','d50a007b-3691-7ac6-b4b7-5c6e1dce4545',90,'2020-08-28 09:56:53',0),('410fe464-46b4-f38f-f586-5f469c30b817','3304ef15-3d54-d514-0f52-5f469cfc6b17','83aac233-eb13-15c1-fb5b-5e909d4c2678',75,'2020-08-27 17:49:53',0),('41357dc4-05ef-5a92-de2e-5f47f1d80e16','69d63f4e-88ef-58c9-0a28-5f469b0dc456','d363a602-e3a2-4f7b-15d5-5c6e1d33de0e',90,'2020-08-28 09:56:53',0),('413aa72f-098c-f075-444d-5f469ccc7bbb','3304ef15-3d54-d514-0f52-5f469cfc6b17','663299bc-dcf9-c549-4fa0-5e909d628646',89,'2020-08-27 17:49:53',0),('4165a343-e51f-4d20-638c-5f469c3f01af','3304ef15-3d54-d514-0f52-5f469cfc6b17','70c3a57c-fbcd-1c89-5375-5e909d38029e',-99,'2020-08-27 17:49:53',0),('417352e2-da82-26f4-aeac-5f47f150053f','69d63f4e-88ef-58c9-0a28-5f469b0dc456','d86004d0-2d45-49de-b8ab-5c6e1d5b4881',90,'2020-08-28 09:56:53',0),('41a831be-536a-42c7-fa3c-5f469c372702','3304ef15-3d54-d514-0f52-5f469cfc6b17','6e16736e-c057-92a9-2ae0-5e909d12be7e',75,'2020-08-27 17:49:53',0),('41bb8784-9189-cb2d-7a1c-5f47f130ae89','69d63f4e-88ef-58c9-0a28-5f469b0dc456','d6b13e01-c92c-477f-5199-5c6e1d956e32',90,'2020-08-28 09:56:53',0),('41d3f7ff-5d7f-0aa6-924a-5f469c34799a','3304ef15-3d54-d514-0f52-5f469cfc6b17','76082a27-4de7-0e8c-58af-5e909d3f3bb7',75,'2020-08-27 17:49:53',0),('41ea2fa1-0928-c891-3d9e-5f47f162e3b7','69d63f4e-88ef-58c9-0a28-5f469b0dc456','d1b5a193-7424-54fb-d5d1-5c6e1d7ab7e5',90,'2020-08-28 09:56:53',0),('4202fe31-ef6b-38f9-2951-5f469c58dc00','3304ef15-3d54-d514-0f52-5f469cfc6b17','7369c6c7-8a9a-947c-561e-5e909df4b40a',90,'2020-08-27 17:49:53',0),('420e0a86-a3bf-4d8f-0e51-5f469cb6fc5e','3304ef15-3d54-d514-0f52-5f469cfc6b17','c5b282ff-1ee9-44ca-32c7-5c6e1d99ecbb',-99,'2020-08-27 17:49:53',0),('421a1b57-279e-1b4f-5b34-5f47f1d43ea3','69d63f4e-88ef-58c9-0a28-5f469b0dc456','da0a2316-6c11-6f86-04fb-5c6e1d12145f',90,'2020-08-28 09:56:53',0),('423403a8-5f34-0c30-ec6c-5f469cf05c3e','3304ef15-3d54-d514-0f52-5f469cfc6b17','6b4e4fdc-6375-5146-7e8c-5e909d304fe3',75,'2020-08-27 17:49:53',0),('4245bf13-58ca-2fd7-088f-5f47f13786e5','69d63f4e-88ef-58c9-0a28-5f469b0dc456','d0031d26-18da-f0d3-e7d3-5c6e1d89ca77',90,'2020-08-28 09:56:53',0),('425ef9d6-8c36-1ddc-0b42-5f469cbc59b9','3304ef15-3d54-d514-0f52-5f469cfc6b17','78a44655-6d0b-4769-8398-5e909de2f14a',90,'2020-08-27 17:49:53',0),('428ce903-5a36-0a23-26f1-5f47f110e211','69d63f4e-88ef-58c9-0a28-5f469b0dc456','e5c20eac-6559-6531-9fc1-5c6e1d07319c',89,'2020-08-28 09:56:53',0),('42b707d0-6ec6-4b3c-041c-5f47f10927dd','69d63f4e-88ef-58c9-0a28-5f469b0dc456','15c2ddf5-d59a-f00b-e3fd-5c6e1dd8862b',90,'2020-08-28 09:56:53',0),('42e153d8-e724-17d3-cdce-5f47f1721e2d','69d63f4e-88ef-58c9-0a28-5f469b0dc456','13c35bd9-edd9-8d1d-9148-5c6e1dd73cbe',90,'2020-08-28 09:56:53',0),('42f12a87-221c-a4de-4d73-5f469c6d0c35','3304ef15-3d54-d514-0f52-5f469cfc6b17','68c1bad8-f8ac-08db-960c-5e909d713286',75,'2020-08-27 17:49:53',0),('430c4db8-522f-c5b8-1f88-5f47f10453c8','69d63f4e-88ef-58c9-0a28-5f469b0dc456','195d5fd1-dcbe-f8c7-791b-5c6e1d95e515',90,'2020-08-28 09:56:53',0),('431fe236-03a2-45da-e7dd-5f469c14ce61','3304ef15-3d54-d514-0f52-5f469cfc6b17','284d5d27-c0b6-9f4a-7f21-5e909d653e5d',89,'2020-08-27 17:49:53',0),('4337228b-e1b1-5edd-083f-5f47f16c0600','69d63f4e-88ef-58c9-0a28-5f469b0dc456','17825d0b-9ae5-f818-8af6-5c6e1d44db2b',90,'2020-08-28 09:56:53',0),('43506361-c75a-ee51-6d52-5f469c96a9c0','3304ef15-3d54-d514-0f52-5f469cfc6b17','3a88b4c8-dd0b-3a95-43b1-5e909d542ac7',-99,'2020-08-27 17:49:53',0),('4382f444-c16a-3e09-2ef9-5f47f1b3d8c0','69d63f4e-88ef-58c9-0a28-5f469b0dc456','11e2c447-3cdb-c380-663b-5c6e1dbbbdef',90,'2020-08-28 09:56:53',0),('4399d86b-b238-0fce-b000-5f469c81a0d9','3304ef15-3d54-d514-0f52-5f469cfc6b17','3760e1ef-f22f-8223-a180-5e909d815c59',75,'2020-08-27 17:49:53',0),('43b23197-6b3a-7fa4-5c53-5f47f1b3a30b','69d63f4e-88ef-58c9-0a28-5f469b0dc456','1b120fe0-ce1d-c39b-038c-5c6e1d886a54',90,'2020-08-28 09:56:53',0),('43cd76ac-7923-5bd0-ed8b-5f469c5d9984','3304ef15-3d54-d514-0f52-5f469cfc6b17','3fec8550-d616-39f3-2c24-5e909da3cebb',75,'2020-08-27 17:49:53',0),('43e09327-080f-c3f9-cc3d-5f47f1e7589e','69d63f4e-88ef-58c9-0a28-5f469b0dc456','101ad241-f96c-8797-b838-5c6e1df463e2',90,'2020-08-28 09:56:53',0),('4402c598-e43d-4ac9-ca74-5f469c716298','3304ef15-3d54-d514-0f52-5f469cfc6b17','3d3b556a-48b1-9d71-2a7b-5e909d062e9e',90,'2020-08-27 17:49:53',0),('4410b8d8-6981-b1c6-8c12-5f47f10441cd','69d63f4e-88ef-58c9-0a28-5f469b0dc456','abd4ec8c-c93c-c1e6-def1-5c6e1ddf4c9a',89,'2020-08-28 09:56:53',0),('443cfdce-1176-7138-ab4b-5f469c686f33','3304ef15-3d54-d514-0f52-5f469cfc6b17','347e56bb-e0e3-1619-1016-5e909d1a0b25',75,'2020-08-27 17:49:53',0),('4444745e-3544-9364-3029-5f47f13262e9','69d63f4e-88ef-58c9-0a28-5f469b0dc456','b28d3942-a8ed-93bf-21cd-5c6e1d0f6ae2',90,'2020-08-28 09:56:53',0),('4489abe1-ffa2-7609-849e-5f469c2aed43','3304ef15-3d54-d514-0f52-5f469cfc6b17','42972982-baf6-fdcd-9660-5e909d9bae21',90,'2020-08-27 17:49:53',0),('449551f8-945e-cd2e-7d92-5f47f16f5b9f','69d63f4e-88ef-58c9-0a28-5f469b0dc456','b0d731ac-5b19-327b-4e2a-5c6e1d3ad23b',90,'2020-08-28 09:56:53',0),('44bbaece-2841-75e2-b327-5f469cb23177','3304ef15-3d54-d514-0f52-5f469cfc6b17','31872bf6-c690-3e22-7268-5e909d387954',75,'2020-08-27 17:49:53',0),('44c787fb-fb79-facd-3978-5f47f102a874','69d63f4e-88ef-58c9-0a28-5f469b0dc456','b5ca1507-2676-e260-08b9-5c6e1da38a6f',90,'2020-08-28 09:56:53',0),('44f1df03-a1b5-53ab-7ffd-5f47f1ab94f9','69d63f4e-88ef-58c9-0a28-5f469b0dc456','b42943d9-bb9e-b64f-e274-5c6e1d1e54b2',90,'2020-08-28 09:56:53',0),('44fa4dbe-db29-5f01-fb80-5f469cd96ea8','3304ef15-3d54-d514-0f52-5f469cfc6b17','4b17887f-a8cf-69f1-d0d3-5e909dc73ad9',89,'2020-08-27 17:49:53',0),('4544a36e-89bd-8cf6-e0bd-5f469c4575c4','3304ef15-3d54-d514-0f52-5f469cfc6b17','55d445e5-2ef1-8430-5d2f-5e909deca916',-99,'2020-08-27 17:49:53',0),('454e23ff-0e73-dcdd-713e-5f47f1ffd5a9','69d63f4e-88ef-58c9-0a28-5f469b0dc456','af31a49b-acde-7cfa-34f5-5c6e1dc79084',90,'2020-08-28 09:56:53',0),('45926e1d-c822-db2d-8025-5f469c71b2d3','3304ef15-3d54-d514-0f52-5f469cfc6b17','5312fe14-8b77-db19-15f8-5e909df9f318',75,'2020-08-27 17:49:53',0),('45930325-af45-8796-d099-5f47f1272e6c','69d63f4e-88ef-58c9-0a28-5f469b0dc456','b7652289-d6e5-41c6-96df-5c6e1dfb2f8a',90,'2020-08-28 09:56:53',0),('45c1b3c3-0072-16c2-2108-5f469c8c9fcf','3304ef15-3d54-d514-0f52-5f469cfc6b17','5b4ca0aa-8bd6-4cea-7241-5e909d8e5d85',75,'2020-08-27 17:49:53',0),('45c2b4c4-d174-c9a3-b4df-5f47f19450f9','69d63f4e-88ef-58c9-0a28-5f469b0dc456','ad856238-c136-2afa-933c-5c6e1d1cdec2',90,'2020-08-28 09:56:53',0),('45f1708a-5677-ac68-7088-5f47f15c8c29','69d63f4e-88ef-58c9-0a28-5f469b0dc456','e4fa58f2-e15d-1cf0-7dc4-5c6e1da17057',89,'2020-08-28 09:56:53',0),('45f6f0a8-1ebc-eb09-d18a-5f469c43a433','3304ef15-3d54-d514-0f52-5f469cfc6b17','589d9e81-c2f0-7bd2-c4a6-5e909d891f4d',90,'2020-08-27 17:49:53',0),('4628a7fe-d8a5-220c-a478-5f469c15effe','3304ef15-3d54-d514-0f52-5f469cfc6b17','505e3bdd-18eb-ba6c-5ce8-5e909d8c96bf',75,'2020-08-27 17:49:53',0),('466651e9-254f-d722-203f-5f469c212f51','3304ef15-3d54-d514-0f52-5f469cfc6b17','5e3c3e09-2a8c-bc08-0096-5e909dd501e1',90,'2020-08-27 17:49:53',0),('4677c965-82b4-bd97-5fc9-5f47f177d379','69d63f4e-88ef-58c9-0a28-5f469b0dc456','eb68a252-8b3f-8d0e-b715-5c6e1ddb6365',90,'2020-08-28 09:56:53',0),('4696ae0c-eb8d-459c-31aa-5f47f1e24062','824bc684-1b3b-2184-338e-5f47f0ca9e93','access',0,'2020-08-27 17:44:57',1),('46a3c39d-e52f-5759-84fa-5f47f17b92ef','69d63f4e-88ef-58c9-0a28-5f469b0dc456','e9d42efb-bea8-e95b-c776-5c6e1d3696e6',90,'2020-08-28 09:56:53',0),('46d5a7af-1dd5-a04e-1138-5f47f13d9daf','69d63f4e-88ef-58c9-0a28-5f469b0dc456','ee981ca9-fd8d-da61-03ed-5c6e1d57bf84',90,'2020-08-28 09:56:53',0),('46eed037-fc16-f52e-d87c-5f469cc965bf','3304ef15-3d54-d514-0f52-5f469cfc6b17','4da6d5e4-5f2a-058f-67a4-5e909dfafc91',75,'2020-08-27 17:49:53',0),('46fe8fd6-ae81-61d1-e491-5f47f17a6f12','824bc684-1b3b-2184-338e-5f47f0ca9e93','delete',0,'2020-08-27 17:44:57',1),('4701f696-a2da-ceda-4491-5f47f1fc0415','69d63f4e-88ef-58c9-0a28-5f469b0dc456','ecfefd1b-1800-f44f-1632-5c6e1db114d0',90,'2020-08-28 09:56:53',0),('472d7651-4a56-b5a6-d0cf-5f469c57b419','3304ef15-3d54-d514-0f52-5f469cfc6b17','90b472dc-63cc-dc05-77bd-5c6e1d207f5b',89,'2020-08-27 17:49:53',0),('47320340-81ee-3e51-5c8c-5f47f132a2f6','69d63f4e-88ef-58c9-0a28-5f469b0dc456','e83709f6-9fef-96ff-7942-5c6e1d98ed5c',90,'2020-08-28 09:56:53',0),('4733e13e-4626-1ff6-b4cc-5f47f1ac9afc','824bc684-1b3b-2184-338e-5f47f0ca9e93','edit',0,'2020-08-27 17:44:57',1),('477914a8-7973-f114-f388-5f47f13c3267','824bc684-1b3b-2184-338e-5f47f0ca9e93','export',0,'2020-08-27 17:44:57',1),('47942f05-050f-d2eb-c85d-5f469cb7331c','3304ef15-3d54-d514-0f52-5f469cfc6b17','97aa67e1-2ce4-f1b7-30a3-5c6e1d102e97',-99,'2020-08-27 17:49:53',0),('47a5e190-e233-cbc5-4dec-5f47f17eb4ee','824bc684-1b3b-2184-338e-5f47f0ca9e93','import',0,'2020-08-27 17:44:57',1),('47c80776-4df8-0a4e-2c6c-5f469cef088e','3304ef15-3d54-d514-0f52-5f469cfc6b17','95ac91c3-34ff-4358-5479-5c6e1df5f99e',75,'2020-08-27 17:49:53',0),('47d83b9b-7a26-c980-cf7a-5f47f1d03dc7','824bc684-1b3b-2184-338e-5f47f0ca9e93','list',0,'2020-08-27 17:44:57',1),('47f7680b-8088-7aab-8953-5f469c7a2634','3304ef15-3d54-d514-0f52-5f469cfc6b17','9b6618c8-31c6-d4f8-0ee8-5c6e1d6493d2',75,'2020-08-27 17:49:53',0),('4804d393-44c9-3249-e957-5f47f14b1cba','824bc684-1b3b-2184-338e-5f47f0ca9e93','massupdate',0,'2020-08-27 17:44:57',1),('4829019c-20f5-b334-742a-5f469c55149e','3304ef15-3d54-d514-0f52-5f469cfc6b17','99ab8d2d-89a3-9c76-44ba-5c6e1dbbb117',90,'2020-08-27 17:49:53',0),('4836e97a-3eaf-7431-d80a-5f47f1106bba','824bc684-1b3b-2184-338e-5f47f0ca9e93','view',0,'2020-08-27 17:44:57',1),('4874f7e9-7e21-9b85-ea47-5f469c469a8e','3304ef15-3d54-d514-0f52-5f469cfc6b17','940d3876-ece1-f13c-45af-5c6e1d9f9019',75,'2020-08-27 17:49:53',0),('487d85a6-9cc8-b8da-13f4-5f47f17c4226','824bc684-1b3b-2184-338e-5f47f0ca9e93','e4e294a9-b18d-ee54-645d-5c6e1dd1e8c6',0,'2020-08-27 17:44:57',1),('48b2cc67-ddfd-652b-85e7-5f47f1cc934e','824bc684-1b3b-2184-338e-5f47f0ca9e93','eb6ad108-3dbe-6c3f-6e09-5c6e1d42e30d',0,'2020-08-27 17:44:57',1),('48b4428e-703d-399d-2bba-5f47f15b5eb3','69d63f4e-88ef-58c9-0a28-5f469b0dc456','f037714b-c744-9311-3833-5c6e1d91bac5',90,'2020-08-28 09:56:53',0),('48b975c8-61f2-799e-c309-5f469cf485f5','3304ef15-3d54-d514-0f52-5f469cfc6b17','9d2871bf-a86f-48fc-4161-5c6e1d149e42',90,'2020-08-27 17:49:53',0),('48e213ee-68f4-43af-c46e-5f47f1aae41d','824bc684-1b3b-2184-338e-5f47f0ca9e93','e9c4c00e-5b31-7e5e-ed1c-5c6e1d771e86',0,'2020-08-27 17:44:57',1),('48e7b547-ca37-7e1a-f241-5f469c9fdb1a','3304ef15-3d54-d514-0f52-5f469cfc6b17','92665564-d5ba-d4a7-55d7-5c6e1de7d212',75,'2020-08-27 17:49:53',0),('48ff8382-f33b-019b-f471-5f47f1e22fda','69d63f4e-88ef-58c9-0a28-5f469b0dc456','e69dd3d3-0eab-3022-3735-5c6e1d14a076',90,'2020-08-28 09:56:53',0),('490dd891-7a52-c593-c77e-5f47f11136f0','824bc684-1b3b-2184-338e-5f47f0ca9e93','eea809c6-5494-ebd8-ea2a-5c6e1d2a1bc3',0,'2020-08-27 17:44:57',1),('4930ed0d-aa7a-ba4c-d471-5f47f1f37760','69d63f4e-88ef-58c9-0a28-5f469b0dc456','1b163909-2912-40f9-8708-5c6e1dad4ee9',89,'2020-08-28 09:56:53',0),('493dece9-29c0-3f91-539e-5f47f1c7000d','824bc684-1b3b-2184-338e-5f47f0ca9e93','ed085713-2fef-f3a6-d794-5c6e1d5116c5',0,'2020-08-27 17:44:57',1),('497cadbd-affb-5eb5-c237-5f47f1fd8b9b','69d63f4e-88ef-58c9-0a28-5f469b0dc456','21a4b930-240a-495d-674b-5c6e1da71eb7',90,'2020-08-28 09:56:53',0),('4986791c-0e7b-01e7-42f0-5f47f11242ec','824bc684-1b3b-2184-338e-5f47f0ca9e93','e81b1376-eea5-89f3-2af6-5c6e1d01ec62',0,'2020-08-27 17:44:57',1),('49aad95c-6f87-3c49-d804-5f47f1421fac','69d63f4e-88ef-58c9-0a28-5f469b0dc456','200a1c61-6924-8cd8-5af7-5c6e1d640527',90,'2020-08-28 09:56:53',0),('49b608f3-1955-650e-dbe9-5f47f19b8751','824bc684-1b3b-2184-338e-5f47f0ca9e93','f04405bc-ba89-bfc6-c5a7-5c6e1db2a756',0,'2020-08-27 17:44:57',1),('49dc87ac-c4f8-5ce8-720b-5f47f1accd97','69d63f4e-88ef-58c9-0a28-5f469b0dc456','24d68f78-a84f-a13d-7f4a-5c6e1d934bac',90,'2020-08-28 09:56:53',0),('49f6eb81-6b35-dd8b-2421-5f47f13f61b9','824bc684-1b3b-2184-338e-5f47f0ca9e93','e67f4d71-4b5b-0f8b-e816-5c6e1d6cbfdc',0,'2020-08-27 17:44:57',1),('4a0cdee0-c3fd-63e5-adbb-5f47f15168c4','69d63f4e-88ef-58c9-0a28-5f469b0dc456','233a9995-db1a-2078-e61f-5c6e1df9a25d',90,'2020-08-28 09:56:53',0),('4a2d3026-64c3-185e-1210-5f47f154d4cb','824bc684-1b3b-2184-338e-5f47f0ca9e93','9842d020-e157-2c63-4eca-5c6e1d7d10aa',0,'2020-08-27 17:44:57',1),('4a523658-c1b4-6ff3-f746-5f47f11c605e','69d63f4e-88ef-58c9-0a28-5f469b0dc456','1e6a83a2-afae-db99-ec4f-5c6e1d752ed9',90,'2020-08-28 09:56:53',0),('4a76358b-b32b-befc-547a-5f47f11272ac','824bc684-1b3b-2184-338e-5f47f0ca9e93','9eb0255f-c82c-8880-a4e3-5c6e1d003339',0,'2020-08-27 17:44:57',1),('4a7e20b0-f4ad-e3e3-f3cc-5f47f18e8891','69d63f4e-88ef-58c9-0a28-5f469b0dc456','26783917-465d-4805-eb95-5c6e1d251635',90,'2020-08-28 09:56:53',0),('4aa1ca48-8949-a441-954f-5f47f1353f60','824bc684-1b3b-2184-338e-5f47f0ca9e93','9d197071-f115-80c8-ddbe-5c6e1d19c577',0,'2020-08-27 17:44:57',1),('4aa9d54e-6800-9674-fba7-5f47f10b9a26','69d63f4e-88ef-58c9-0a28-5f469b0dc456','1cd1d38b-fce9-a981-1846-5c6e1d132836',90,'2020-08-28 09:56:53',0),('4ace35cd-78b8-053d-ce72-5f47f1a618c7','824bc684-1b3b-2184-338e-5f47f0ca9e93','a263e131-bb1e-2f23-e69c-5c6e1db832c5',0,'2020-08-27 17:44:57',1),('4ad485e7-4edc-b58f-17b2-5f47f14dc72b','69d63f4e-88ef-58c9-0a28-5f469b0dc456','af161148-77d1-f897-76a8-5c6e1d79ad82',89,'2020-08-28 09:56:53',0),('4afc5e5d-d92b-d9d8-14db-5f47f1af95ca','824bc684-1b3b-2184-338e-5f47f0ca9e93','a053e50a-04cd-914e-fa5b-5c6e1d5600c1',0,'2020-08-27 17:44:57',1),('4afd7cac-5c51-3096-bebe-5f47f1fe4318','69d63f4e-88ef-58c9-0a28-5f469b0dc456','b5b23ad8-066c-bd1f-1756-5c6e1dda06a1',90,'2020-08-28 09:56:53',0),('4b040e35-3576-bd46-3978-5f469cb2da37','3304ef15-3d54-d514-0f52-5f469cfc6b17','c407d548-1499-6d04-15ca-5c6e1d93705a',75,'2020-08-27 17:49:53',0),('4b273dbe-09bb-5e2f-7127-5f47f1a8fd8e','69d63f4e-88ef-58c9-0a28-5f469b0dc456','b40800b6-5ceb-b18f-2021-5c6e1ddf1c71',90,'2020-08-28 09:56:53',0),('4b2b4176-cc34-c086-75a0-5f47f127ae2a','824bc684-1b3b-2184-338e-5f47f0ca9e93','9b8488a5-8b90-64fc-cc6a-5c6e1deafdb8',0,'2020-08-27 17:44:57',1),('4b6f6a94-8c2c-bfe7-f242-5f47f1b99bbc','69d63f4e-88ef-58c9-0a28-5f469b0dc456','b8f6333c-264a-d56c-f3f7-5c6e1d6717b5',90,'2020-08-28 09:56:53',0),('4b7527cd-e6d4-52ce-171a-5f47f19008fc','824bc684-1b3b-2184-338e-5f47f0ca9e93','a4160e91-0182-6aca-6626-5c6e1dd95415',0,'2020-08-27 17:44:57',1),('4b9ef494-5e18-94b0-b006-5f47f1f3d9a7','69d63f4e-88ef-58c9-0a28-5f469b0dc456','b755c057-7846-ef4e-48a4-5c6e1d5b0de2',90,'2020-08-28 09:56:53',0),('4ba5bc33-11b8-1d21-c854-5f47f10aedc5','824bc684-1b3b-2184-338e-5f47f0ca9e93','99ed1f96-3c96-5191-aacb-5c6e1db0791e',0,'2020-08-27 17:44:57',1),('4bcbf415-48cb-7f72-b43b-5f47f12043af','69d63f4e-88ef-58c9-0a28-5f469b0dc456','b262f627-0c6c-c476-450e-5c6e1d7af1a7',90,'2020-08-28 09:56:53',0),('4bcf75a7-fef6-9567-fe7f-5f47f154a32a','824bc684-1b3b-2184-338e-5f47f0ca9e93','2add29bf-94df-524f-816b-5c6e1df26606',0,'2020-08-27 17:44:57',1),('4bf93e31-ad7d-e213-92dc-5f47f1fab175','69d63f4e-88ef-58c9-0a28-5f469b0dc456','baa3bd6a-799c-c7cb-8167-5c6e1de0778f',90,'2020-08-28 09:56:53',0),('4bfb25f8-88b3-096d-850b-5f47f14a6443','824bc684-1b3b-2184-338e-5f47f0ca9e93','31f99891-140f-0f15-8210-5c6e1da1c586',0,'2020-08-27 17:44:57',1),('4c250950-384f-ac40-6cb7-5f47f1aed081','824bc684-1b3b-2184-338e-5f47f0ca9e93','304550da-9a68-1c5d-70b9-5c6e1d0308c6',0,'2020-08-27 17:44:57',1),('4c35e292-674d-44b9-94b6-5f47f14b5e79','69d63f4e-88ef-58c9-0a28-5f469b0dc456','b0c29e2e-087d-18f2-5b3c-5c6e1d67fc2d',90,'2020-08-28 09:56:53',0),('4c684935-0d0f-8485-edef-5f47f11aebee','824bc684-1b3b-2184-338e-5f47f0ca9e93','352832c1-13a4-6df3-47b0-5c6e1d95364b',0,'2020-08-27 17:44:57',1),('4c7999ae-40a6-a8ff-023c-5f47f141fd3f','69d63f4e-88ef-58c9-0a28-5f469b0dc456','52c2ed19-3cfd-a2a8-3cce-5c6e1d04c3d8',89,'2020-08-28 09:56:53',0),('4c94aee5-6e84-34b3-ab3c-5f47f18118bf','824bc684-1b3b-2184-338e-5f47f0ca9e93','33977c10-585e-e3e4-76b3-5c6e1d29503c',0,'2020-08-27 17:44:57',1),('4ca32f90-6984-fd55-f363-5f47f1995c95','69d63f4e-88ef-58c9-0a28-5f469b0dc456','593e1378-f97b-72e9-4a72-5c6e1d9089b4',90,'2020-08-28 09:56:53',0),('4cbea924-4e1a-6446-339b-5f47f11319d8','824bc684-1b3b-2184-338e-5f47f0ca9e93','2e4a26b7-6b7b-55c2-11db-5c6e1d2fe155',0,'2020-08-27 17:44:57',1),('4cce1602-f889-76fe-b1d2-5f47f1cf2da3','69d63f4e-88ef-58c9-0a28-5f469b0dc456','57a40c89-305e-8dc2-31a9-5c6e1d9922d3',90,'2020-08-28 09:56:53',0),('4cee66b3-3e46-6f9e-bc31-5f47f1fbea36','824bc684-1b3b-2184-338e-5f47f0ca9e93','36b9e4fa-f86d-727e-fca7-5c6e1de8e18d',0,'2020-08-27 17:44:57',1),('4cfb037a-0816-f9bb-b703-5f47f115fca4','69d63f4e-88ef-58c9-0a28-5f469b0dc456','5c7c1f01-024e-a89d-9299-5c6e1d9b9c91',90,'2020-08-28 09:56:53',0),('4d2bae03-0359-0674-96a8-5f47f1ea962f','824bc684-1b3b-2184-338e-5f47f0ca9e93','2c76062e-78dc-b6f6-36df-5c6e1d53d9e3',0,'2020-08-27 17:44:57',1),('4d438d9f-1554-9afd-3e31-5f47f160ea9c','69d63f4e-88ef-58c9-0a28-5f469b0dc456','5ad89177-2751-9788-e516-5c6e1d29c5ae',90,'2020-08-28 09:56:53',0),('4d6e2701-b3d1-4a0b-a8e3-5f47f14da768','824bc684-1b3b-2184-338e-5f47f0ca9e93','40a52983-5401-23cb-9769-5c6e1dc74de0',0,'2020-08-27 17:44:57',1),('4d70bb41-b345-191b-ffd0-5f47f18d9d1b','69d63f4e-88ef-58c9-0a28-5f469b0dc456','5603f923-25a8-fdd2-328c-5c6e1de852fd',90,'2020-08-28 09:56:53',0),('4d9e261e-dcf5-bcb5-0b56-5f47f156c700','824bc684-1b3b-2184-338e-5f47f0ca9e93','471273a9-50cb-30b6-1772-5c6e1d4750af',0,'2020-08-27 17:44:57',1),('4d9f351d-382c-0f6c-2c9a-5f47f13810e9','69d63f4e-88ef-58c9-0a28-5f469b0dc456','5e1b71ea-7c87-c530-779a-5c6e1d9e0d17',90,'2020-08-28 09:56:53',0),('4dccd239-3e04-7fd8-f4f4-5f47f1260b93','69d63f4e-88ef-58c9-0a28-5f469b0dc456','54651337-2a36-a3e0-cc3f-5c6e1dd7ff25',90,'2020-08-28 09:56:53',0),('4dcd6726-f9c4-9c3a-1358-5f47f1f553bf','824bc684-1b3b-2184-338e-5f47f0ca9e93','4574d08f-6962-323a-85a8-5c6e1d5971c5',0,'2020-08-27 17:44:57',1),('4df676f5-53a8-0d5a-3ade-5f47f1ab8409','69d63f4e-88ef-58c9-0a28-5f469b0dc456','5a116d0a-93ce-3f38-6b3c-5c6e1ddfb588',89,'2020-08-28 09:56:53',0),('4dfe14d1-4f2e-c3b7-4941-5f47f15a0468','824bc684-1b3b-2184-338e-5f47f0ca9e93','4a57e684-d9e3-d7f2-83fd-5c6e1d1dc552',0,'2020-08-27 17:44:57',1),('4e030dd2-d6e4-937d-331a-5f469cd9ead2','3304ef15-3d54-d514-0f52-5f469cfc6b17','c95a4d72-da3c-06c1-c24d-5c6e1de6ff39',75,'2020-08-27 17:49:53',0),('4e3dc82f-9a9c-db1d-f818-5f47f1bdaeb6','69d63f4e-88ef-58c9-0a28-5f469b0dc456','60a6e40c-cd7c-6300-f332-5c6e1df4ba1d',90,'2020-08-28 09:56:53',0),('4e3fa785-2f61-4fb5-aa11-5f47f1e68053','824bc684-1b3b-2184-338e-5f47f0ca9e93','48bad485-ae5a-7893-e9d9-5c6e1df2102e',0,'2020-08-27 17:44:57',1),('4e6962bc-642c-b7d3-ce3c-5f47f17f59d1','69d63f4e-88ef-58c9-0a28-5f469b0dc456','5efb35f8-4d02-ff48-a0a1-5c6e1d8bd05a',90,'2020-08-28 09:56:53',0),('4e6c8be8-769a-01d9-0eb4-5f47f1c11b0e','824bc684-1b3b-2184-338e-5f47f0ca9e93','43dcc714-a359-b934-1906-5c6e1d8e6d82',0,'2020-08-27 17:44:57',1),('4e951a83-009c-d200-7299-5f47f10eb85f','69d63f4e-88ef-58c9-0a28-5f469b0dc456','6431abe6-fb9d-097a-5dc9-5c6e1d9b476d',90,'2020-08-28 09:56:53',0),('4e9cbf4f-a62b-1da2-ca8c-5f47f1a614ed','824bc684-1b3b-2184-338e-5f47f0ca9e93','4bf15a14-a85b-47a9-8349-5c6e1d3aa8fe',0,'2020-08-27 17:44:57',1),('4ec36073-a31e-4663-f1ac-5f47f1637f2e','69d63f4e-88ef-58c9-0a28-5f469b0dc456','6280ad76-9e85-2b59-5565-5c6e1daf4e8d',90,'2020-08-28 09:56:53',0),('4ecd260d-f7bf-6c88-259f-5f47f19b1004','824bc684-1b3b-2184-338e-5f47f0ca9e93','42465f76-4843-8654-22a2-5c6e1db7e678',0,'2020-08-27 17:44:57',1),('4eef0dc1-f4dd-d489-4b20-5f47f1ccabc9','69d63f4e-88ef-58c9-0a28-5f469b0dc456','5d5e2201-0772-eea0-a538-5c6e1d9fbe40',90,'2020-08-28 09:56:53',0),('4ef8fc4c-85d1-12dc-5e32-5f47f18c3c03','824bc684-1b3b-2184-338e-5f47f0ca9e93','1b958cf9-ae40-438b-f8c5-5c6e1da93153',0,'2020-08-27 17:44:57',1),('4f3c19ed-3b4b-5fc2-4b0d-5f47f14b3404','824bc684-1b3b-2184-338e-5f47f0ca9e93','232e2ca6-d75b-da80-9dfe-5c6e1de86dd1',0,'2020-08-27 17:44:57',1),('4f4742a2-1c84-e265-8789-5f47f11a925d','69d63f4e-88ef-58c9-0a28-5f469b0dc456','660bd7c0-0c8f-0395-4bc7-5c6e1d57fdbb',90,'2020-08-28 09:56:53',0),('4f68cd10-2676-c213-c59a-5f47f13f73c6','824bc684-1b3b-2184-338e-5f47f0ca9e93','2169ee86-765e-bd5e-d6ca-5c6e1d9ae49c',0,'2020-08-27 17:44:57',1),('4f753cba-d057-6dc0-5393-5f47f1e2d2a1','69d63f4e-88ef-58c9-0a28-5f469b0dc456','5bbba79c-a0f1-e71e-ea73-5c6e1da0a9e2',90,'2020-08-28 09:56:53',0),('4f969043-44f6-3088-10e2-5f47f19906c6','824bc684-1b3b-2184-338e-5f47f0ca9e93','26f60559-7804-b3e2-2c5a-5c6e1d08216c',0,'2020-08-27 17:44:57',1),('4fa1df44-1b68-3c60-090a-5f47f14a1941','69d63f4e-88ef-58c9-0a28-5f469b0dc456','96a6e41c-fd1d-2d14-efec-5c6e1ddcd204',89,'2020-08-28 09:56:53',0),('4fc1f96d-0030-8730-1f23-5f47f1c3eb47','824bc684-1b3b-2184-338e-5f47f0ca9e93','2507c934-350d-93e7-6009-5c6e1d1a4ebf',0,'2020-08-27 17:44:57',1),('4fcc8ab4-d897-0f5c-8835-5f47f1a532eb','69d63f4e-88ef-58c9-0a28-5f469b0dc456','9e36c5da-d99c-4575-d041-5c6e1d1932a5',90,'2020-08-28 09:56:53',0),('4fedfc25-279f-3c74-16cb-5f47f1dc78a7','824bc684-1b3b-2184-338e-5f47f0ca9e93','1f915e87-b617-1952-6ea3-5c6e1d890550',0,'2020-08-27 17:44:57',1),('4ff80cd8-3373-5389-15ea-5f47f19a701f','69d63f4e-88ef-58c9-0a28-5f469b0dc456','9c9068c8-2889-4237-c4da-5c6e1d5fd6e0',90,'2020-08-28 09:56:53',0),('50387a3f-12d7-14d6-18b0-5f47f1e2dae9','69d63f4e-88ef-58c9-0a28-5f469b0dc456','a19598bd-ecb8-b87c-42dc-5c6e1d69db8f',90,'2020-08-28 09:56:53',0),('50402723-f010-a02e-6875-5f47f12a4de4','824bc684-1b3b-2184-338e-5f47f0ca9e93','28ff06fc-14b0-98ec-737f-5c6e1d816cc7',0,'2020-08-27 17:44:57',1),('5065da87-cffd-e9bb-f3a6-5f47f19acda1','69d63f4e-88ef-58c9-0a28-5f469b0dc456','9febc0e8-571c-b6d9-033d-5c6e1dadd332',90,'2020-08-28 09:56:53',0),('506c08d8-e380-6722-679a-5f47f1dedb0e','824bc684-1b3b-2184-338e-5f47f0ca9e93','1d94faaf-6dae-f882-8d0e-5c6e1dee4687',0,'2020-08-27 17:44:57',1),('509305bb-34e8-bb2c-b441-5f47f153f351','69d63f4e-88ef-58c9-0a28-5f469b0dc456','9a9dea65-c49d-a612-b77a-5c6e1d860eb8',90,'2020-08-28 09:56:53',0),('509a57b3-2b5d-0d1b-af14-5f47f1c1dac1','824bc684-1b3b-2184-338e-5f47f0ca9e93','78bc3e90-a6f2-9b33-2c95-5c6e1d8c1327',0,'2020-08-27 17:44:57',1),('50c0daaf-8e0e-3f1d-e1e9-5f47f1509117','69d63f4e-88ef-58c9-0a28-5f469b0dc456','a53a56c9-6206-9bb2-70a8-5c6e1dd996a7',90,'2020-08-28 09:56:53',0),('50c996d6-2cff-ab12-b1ca-5f47f116a8ed','824bc684-1b3b-2184-338e-5f47f0ca9e93','7f2175cd-db29-b4f8-3753-5c6e1d46bab5',0,'2020-08-27 17:44:57',1),('50ef6e5b-32ef-848c-8e43-5f47f11346cf','69d63f4e-88ef-58c9-0a28-5f469b0dc456','98b4e4cc-d8f5-e99a-60d3-5c6e1dc7787c',90,'2020-08-28 09:56:53',0),('50f6a2ad-1feb-20d8-3ba8-5f47f1e336d2','824bc684-1b3b-2184-338e-5f47f0ca9e93','7d8d7347-86e8-73a0-1348-5c6e1d86190b',0,'2020-08-27 17:44:57',1),('5138122e-2223-a2f4-e975-5f47f195c645','69d63f4e-88ef-58c9-0a28-5f469b0dc456','82fe24f4-3ce6-a0a9-227d-5c6e1df8c135',89,'2020-08-28 09:56:53',0),('513bfe81-10e6-b648-bdde-5f47f196719f','824bc684-1b3b-2184-338e-5f47f0ca9e93','824f5a15-53b1-4755-d578-5c6e1d0a0440',0,'2020-08-27 17:44:57',1),('51664416-de93-ebc8-9026-5f47f1687c89','69d63f4e-88ef-58c9-0a28-5f469b0dc456','8a32b4ee-2ae3-f3c0-9e82-5c6e1d41a51c',90,'2020-08-28 09:56:53',0),('5166a189-0db9-3b14-4f0d-5f47f15f13dc','824bc684-1b3b-2184-338e-5f47f0ca9e93','80b641fa-5eda-2510-e682-5c6e1dd24ed4',0,'2020-08-27 17:44:57',1),('5191b7d6-8c66-2810-af2b-5f47f15d2a07','824bc684-1b3b-2184-338e-5f47f0ca9e93','7bf1dc51-d297-b1c0-0c9a-5c6e1d3778ab',0,'2020-08-27 17:44:57',1),('51942c86-bbaf-311d-a292-5f47f16284c9','69d63f4e-88ef-58c9-0a28-5f469b0dc456','88712337-2eb8-7e5f-af4c-5c6e1d94bd75',90,'2020-08-28 09:56:53',0),('51bd3997-3528-47b9-6d85-5f47f1b0db52','69d63f4e-88ef-58c9-0a28-5f469b0dc456','8da7244b-1fd0-52bc-6c70-5c6e1d8b6bee',90,'2020-08-28 09:56:53',0),('51bd742a-b3c8-d62c-c06a-5f47f1458180','824bc684-1b3b-2184-338e-5f47f0ca9e93','83e9503e-4d1d-2241-0894-5c6e1d2d5ad3',0,'2020-08-27 17:44:57',1),('51ea7fd5-fd8a-5b4e-4e7c-5f47f102bbac','69d63f4e-88ef-58c9-0a28-5f469b0dc456','8be9f5f8-465c-c9ea-d1bb-5c6e1dbecaff',90,'2020-08-28 09:56:53',0),('52305798-db38-a276-686a-5f47f111a6ae','69d63f4e-88ef-58c9-0a28-5f469b0dc456','86b6537f-e557-3610-62c3-5c6e1dd8169c',90,'2020-08-28 09:56:53',0),('524cb2e4-2f8b-afe7-e148-5f47f11288f1','824bc684-1b3b-2184-338e-5f47f0ca9e93','7a597959-caf4-8477-c2f8-5c6e1d6c7ccd',0,'2020-08-27 17:44:57',1),('526e2621-e4b7-616f-b513-5f47f105de88','69d63f4e-88ef-58c9-0a28-5f469b0dc456','8f7040ab-20b9-1e82-89d6-5c6e1d5f3de2',90,'2020-08-28 09:56:53',0),('52993208-bc3c-12a1-e796-5f47f198c975','69d63f4e-88ef-58c9-0a28-5f469b0dc456','84d457d8-2ba5-48a6-5a1b-5c6e1d453ed0',90,'2020-08-28 09:56:53',0),('52c4b97c-107e-a61d-6e32-5f47f1440bce','69d63f4e-88ef-58c9-0a28-5f469b0dc456','23909eed-24e9-1383-65a6-5c6e1d4dc04a',89,'2020-08-28 09:56:53',0),('52f3ebaf-e5b3-95c1-3ebe-5f47f16d5c59','69d63f4e-88ef-58c9-0a28-5f469b0dc456','2cf32cca-b9b9-409b-bd3e-5c6e1dfd8c3b',90,'2020-08-28 09:56:53',0),('532b0a8f-2758-197e-6808-5f469ccaafeb','3304ef15-3d54-d514-0f52-5f469cfc6b17','c77bfcc8-254e-1ceb-08d4-5c6e1d2fbe7b',90,'2020-08-27 17:49:53',0),('533a9ca0-f276-a8d2-8c89-5f47f1723f8b','69d63f4e-88ef-58c9-0a28-5f469b0dc456','2ae367b2-742d-5ab8-1ea2-5c6e1db4adef',90,'2020-08-28 09:56:53',0),('53662ae3-b07c-2b9e-b827-5f47f104dd20','69d63f4e-88ef-58c9-0a28-5f469b0dc456','30b24203-0540-da78-baa4-5c6e1dd6ddce',90,'2020-08-28 09:56:53',0),('538962be-071a-e325-ae6c-5f47f14c58cd','824bc684-1b3b-2184-338e-5f47f0ca9e93','ac28a8b7-049a-fc4a-e4de-5c6e1d1423a4',0,'2020-08-27 17:44:57',1),('5391db97-132d-a674-f289-5f47f11f0347','69d63f4e-88ef-58c9-0a28-5f469b0dc456','2ebfecbf-817e-c0f6-494c-5c6e1d375b9d',90,'2020-08-28 09:56:53',0),('53b86d30-1cbf-0035-d30d-5f47f1918ce7','824bc684-1b3b-2184-338e-5f47f0ca9e93','b2e44f1d-2865-1aec-18e0-5c6e1d495fff',0,'2020-08-27 17:44:57',1),('53bee206-15f4-6674-1473-5f47f14f9205','69d63f4e-88ef-58c9-0a28-5f469b0dc456','273511c0-4119-a9e5-de4c-5c6e1d59d6dc',90,'2020-08-28 09:56:53',0),('53e81701-a65b-2f51-739f-5f47f135b15f','824bc684-1b3b-2184-338e-5f47f0ca9e93','b1418e90-c9f7-20cb-7771-5c6e1d6469aa',0,'2020-08-27 17:44:57',1),('53ecf30f-98f0-672a-e3e6-5f47f13e7980','69d63f4e-88ef-58c9-0a28-5f469b0dc456','3273e589-c6a6-c63a-53fc-5c6e1d017b90',90,'2020-08-28 09:56:53',0),('542c6c1f-aa34-b5cb-4958-5f47f1de41ab','69d63f4e-88ef-58c9-0a28-5f469b0dc456','255f689c-30fa-9743-6b97-5c6e1d16820d',90,'2020-08-28 09:56:53',0),('542c917e-41a8-6d1e-4147-5f47f10bf1cb','824bc684-1b3b-2184-338e-5f47f0ca9e93','b6261178-cb2f-3c4d-318a-5c6e1d71d570',0,'2020-08-27 17:44:57',1),('5457acb5-f312-5176-47e3-5f47f19ce17b','824bc684-1b3b-2184-338e-5f47f0ca9e93','b4845172-d53e-fede-4687-5c6e1da11cd4',0,'2020-08-27 17:44:57',1),('545a34e9-29de-6598-6524-5f47f1369f42','69d63f4e-88ef-58c9-0a28-5f469b0dc456','2d4c98ca-5d2e-6f0a-17fc-5c6e1d9fd7b6',89,'2020-08-28 09:56:53',0),('54854b25-e02e-9c8c-e87f-5f47f1cc8961','824bc684-1b3b-2184-338e-5f47f0ca9e93','af9542f4-0edd-1926-48b7-5c6e1d8f9893',0,'2020-08-27 17:44:57',1),('548b42f7-acc7-1ac8-176c-5f47f1821f8f','69d63f4e-88ef-58c9-0a28-5f469b0dc456','3516982c-ed8e-543f-3813-5c6e1d0bdfbd',90,'2020-08-28 09:56:53',0),('54b10f55-fc90-55b3-d3ed-5f47f145a978','824bc684-1b3b-2184-338e-5f47f0ca9e93','b7c94ce9-b840-0559-bbb4-5c6e1d293842',0,'2020-08-27 17:44:57',1),('54ba2c1f-9acc-bbc7-2e6a-5f47f1ffe07d','69d63f4e-88ef-58c9-0a28-5f469b0dc456','337b8214-09d6-962e-b12b-5c6e1deefec8',90,'2020-08-28 09:56:53',0),('54de2953-4460-d0e1-8eb1-5f47f1eeec97','824bc684-1b3b-2184-338e-5f47f0ca9e93','ade60ebc-4744-3eb6-a3e1-5c6e1d94a89b',0,'2020-08-27 17:44:57',1),('54eb4707-bdac-3130-1d8d-5f47f1272d41','69d63f4e-88ef-58c9-0a28-5f469b0dc456','3852582d-e33f-175c-76b2-5c6e1d893051',90,'2020-08-28 09:56:53',0),('5524ef28-7935-a1cc-7781-5f47f178bd0c','824bc684-1b3b-2184-338e-5f47f0ca9e93','be604b96-6736-7e81-6d15-5c6e1d5dab3e',0,'2020-08-27 17:44:57',1),('55549ad0-ef6a-9194-3d53-5f47f1053335','824bc684-1b3b-2184-338e-5f47f0ca9e93','c58c2f62-5135-6531-5629-5c6e1d7895af',0,'2020-08-27 17:44:57',1),('55776c65-54ad-0f72-9eb1-5f47f16cdb82','69d63f4e-88ef-58c9-0a28-5f469b0dc456','36b63ee1-ab68-5783-63da-5c6e1de6b4e5',90,'2020-08-28 09:56:53',0),('558152fc-b8d0-a8dd-1e93-5f47f1182499','824bc684-1b3b-2184-338e-5f47f0ca9e93','c3a67730-0e9c-4380-7ead-5c6e1df50457',0,'2020-08-27 17:44:57',1),('55a6a9c6-34dd-05f4-7827-5f47f1fded49','69d63f4e-88ef-58c9-0a28-5f469b0dc456','31cac616-99da-efcf-aa0f-5c6e1d3afd35',90,'2020-08-28 09:56:53',0),('55ac2ec0-16c1-56f3-7d8b-5f47f121fdfd','824bc684-1b3b-2184-338e-5f47f0ca9e93','c95817c6-d1e2-2fea-2350-5c6e1d6aa3fe',0,'2020-08-27 17:44:57',1),('55d6a03d-b1c8-fadb-1052-5f47f1c4597a','69d63f4e-88ef-58c9-0a28-5f469b0dc456','39f45967-6ace-927e-9b01-5c6e1daa3eda',90,'2020-08-28 09:56:53',0),('55e1f158-2c89-f768-0ea0-5f47f15786c5','824bc684-1b3b-2184-338e-5f47f0ca9e93','c76b891a-5d75-28e1-7037-5c6e1d34fc61',0,'2020-08-27 17:44:57',1),('56195d2d-a481-5d48-b6a6-5f47f15822b1','69d63f4e-88ef-58c9-0a28-5f469b0dc456','2efef55f-d14c-224a-1cc6-5c6e1d1d53cf',90,'2020-08-28 09:56:53',0),('56290cb9-3381-0523-883c-5f47f1456ecf','824bc684-1b3b-2184-338e-5f47f0ca9e93','c1cc01ac-a509-0cfd-5218-5c6e1d18f2cd',0,'2020-08-27 17:44:57',1),('564b44cf-b011-29d0-17ce-5f47f13c422a','69d63f4e-88ef-58c9-0a28-5f469b0dc456','4096a9d2-bd4a-eb01-973f-5c6e1d5a45e2',89,'2020-08-28 09:56:53',0),('56547b5e-1dc8-5b04-d679-5f47f1debb23','824bc684-1b3b-2184-338e-5f47f0ca9e93','cb44e564-ea4c-4625-16e8-5c6e1d0a6ed8',0,'2020-08-27 17:44:57',1),('567b15cb-7ce6-781e-9a0d-5f47f1c5e717','69d63f4e-88ef-58c9-0a28-5f469b0dc456','47288bba-4c05-a22a-8c63-5c6e1d3dc05c',90,'2020-08-28 09:56:53',0),('568f9a04-3b3f-919b-a55c-5f47f163c35f','824bc684-1b3b-2184-338e-5f47f0ca9e93','c0085f74-6446-7bec-6b0f-5c6e1d2d824b',0,'2020-08-27 17:44:57',1),('56a9bcd9-7a67-70d5-3856-5f47f1934557','69d63f4e-88ef-58c9-0a28-5f469b0dc456','458a5983-6996-5853-fe72-5c6e1d222fc5',90,'2020-08-28 09:56:53',0),('56bed3cf-0c51-c5c1-3156-5f47f18f2a00','824bc684-1b3b-2184-338e-5f47f0ca9e93','f1e9306e-dc78-fda0-504a-5c6e1d689e6e',0,'2020-08-27 17:44:57',1),('56e34941-bd5d-b448-9546-5f47f11bee42','69d63f4e-88ef-58c9-0a28-5f469b0dc456','4a657e01-e916-f669-a099-5c6e1d1e5567',90,'2020-08-28 09:56:53',0),('5729def6-9a23-57b6-c725-5f47f176a073','69d63f4e-88ef-58c9-0a28-5f469b0dc456','48c6d617-d7a0-6fd8-c0a4-5c6e1d8a861c',90,'2020-08-28 09:56:53',0),('57331df3-7b3d-e937-916a-5f47f1dcf17b','824bc684-1b3b-2184-338e-5f47f0ca9e93','419b0ff8-8bd3-c967-1f1c-5c6e1d4e8fb9',0,'2020-08-27 17:44:57',1),('57609cfb-6485-4549-7369-5f47f16bc5ed','824bc684-1b3b-2184-338e-5f47f0ca9e93','28140bad-21e3-58b2-5adf-5c6e1d2a37ec',0,'2020-08-27 17:44:57',1),('578a7f2b-fdac-6bb7-b149-5f47f18e1263','69d63f4e-88ef-58c9-0a28-5f469b0dc456','43e09d57-a48f-6375-1331-5c6e1d87f59a',90,'2020-08-28 09:56:53',0),('578d797d-438c-e3ad-b9c1-5f47f1aaa220','824bc684-1b3b-2184-338e-5f47f0ca9e93','746106cb-0c40-293e-cdbe-5c6e1deb825e',0,'2020-08-27 17:44:57',1),('57b70ec1-df2c-ca05-52f3-5f47f16b5368','69d63f4e-88ef-58c9-0a28-5f469b0dc456','4c0a451b-4439-f8d8-2592-5c6e1d1fe047',90,'2020-08-28 09:56:53',0),('57c62d1c-02d3-129d-7a4a-5f47f11642cd','824bc684-1b3b-2184-338e-5f47f0ca9e93','5b420296-9b9b-f422-444c-5c6e1d94f411',0,'2020-08-27 17:44:57',1),('57fd491e-058b-cec4-cb48-5f47f1d64d5c','69d63f4e-88ef-58c9-0a28-5f469b0dc456','42416112-83ca-03b7-352a-5c6e1db0f5e5',90,'2020-08-28 09:56:53',0),('580c7381-90f2-e4be-05e7-5f47f157e001','824bc684-1b3b-2184-338e-5f47f0ca9e93','ef70059e-eb96-9eb7-e071-5c6e1d41766b',0,'2020-08-27 17:44:57',1),('58295bf0-dc02-ef29-d1b7-5f47f135a5ef','69d63f4e-88ef-58c9-0a28-5f469b0dc456','db873033-8e64-6de0-193d-5c6e1d2f494c',89,'2020-08-28 09:56:53',0),('583a2612-733e-4a24-e534-5f47f187d294','824bc684-1b3b-2184-338e-5f47f0ca9e93','8d830de8-f2d0-21b8-09cb-5c6e1d131f85',0,'2020-08-27 17:44:57',1),('5855af25-df9d-2f81-0af5-5f47f151f091','69d63f4e-88ef-58c9-0a28-5f469b0dc456','e32106b1-5bea-8fc2-af47-5c6e1d9e72f7',90,'2020-08-28 09:56:53',0),('58660191-ef23-ad82-1141-5f47f1263c75','824bc684-1b3b-2184-338e-5f47f0ca9e93','f384474f-3e33-9f86-bdf5-5c6e1d2ca1a1',0,'2020-08-27 17:44:57',1),('5891569d-0bf7-77d2-a73f-5f47f1c9cdf1','824bc684-1b3b-2184-338e-5f47f0ca9e93','ce4558d9-c83f-511d-b67b-5c6e1dff0232',0,'2020-08-27 17:44:57',1),('58b9a073-da13-da46-f833-5f47f1810255','69d63f4e-88ef-58c9-0a28-5f469b0dc456','e18fff67-dccb-b854-fb52-5c6e1d1c257e',90,'2020-08-28 09:56:53',0),('58c3d5e8-c812-043d-2f24-5f47f12473fb','824bc684-1b3b-2184-338e-5f47f0ca9e93','d50a007b-3691-7ac6-b4b7-5c6e1dce4545',0,'2020-08-27 17:44:57',1),('5902debb-ce0d-b859-3370-5f47f13c1c4e','69d63f4e-88ef-58c9-0a28-5f469b0dc456','e64249f4-d1f2-0677-2be6-5c6e1d48a985',90,'2020-08-28 09:56:53',0),('59038848-fdf9-2a50-f41a-5f47f118d362','824bc684-1b3b-2184-338e-5f47f0ca9e93','d363a602-e3a2-4f7b-15d5-5c6e1d33de0e',0,'2020-08-27 17:44:57',1),('592fdfe1-91f8-b84c-f12a-5f47f1d135b6','69d63f4e-88ef-58c9-0a28-5f469b0dc456','e4b7d2d6-a856-a13b-9e18-5c6e1d70e0c0',90,'2020-08-28 09:56:53',0),('5931f7dc-ee1e-fa0b-81ac-5f47f1230ce5','824bc684-1b3b-2184-338e-5f47f0ca9e93','d86004d0-2d45-49de-b8ab-5c6e1d5b4881',0,'2020-08-27 17:44:57',1),('595f6d55-56ad-9b6d-bf98-5f47f19727a9','824bc684-1b3b-2184-338e-5f47f0ca9e93','d6b13e01-c92c-477f-5199-5c6e1d956e32',0,'2020-08-27 17:44:57',1),('5960ef5a-3c9d-d49d-2f08-5f47f19537d6','69d63f4e-88ef-58c9-0a28-5f469b0dc456','dff9e8c1-7bca-a0ba-d00a-5c6e1d2200da',90,'2020-08-28 09:56:53',0),('598d65ee-eb13-fc8e-f680-5f47f1f68dfb','824bc684-1b3b-2184-338e-5f47f0ca9e93','d1b5a193-7424-54fb-d5d1-5c6e1d7ab7e5',0,'2020-08-27 17:44:57',1),('599250c2-1b4b-0bde-da96-5f47f108fbf8','69d63f4e-88ef-58c9-0a28-5f469b0dc456','e7d1dbec-7b69-37d5-6b62-5c6e1da40e84',90,'2020-08-28 09:56:53',0),('59ba7233-f4d8-8a5f-aa0a-5f47f10df943','824bc684-1b3b-2184-338e-5f47f0ca9e93','da0a2316-6c11-6f86-04fb-5c6e1d12145f',0,'2020-08-27 17:44:57',1),('59c8c1e1-33ae-c2e7-d970-5f47f12bbd10','69d63f4e-88ef-58c9-0a28-5f469b0dc456','de0488d2-31a1-08f9-d295-5c6e1dc33038',90,'2020-08-28 09:56:53',0),('59fb1fda-26e7-6708-b81b-5f47f18ad972','824bc684-1b3b-2184-338e-5f47f0ca9e93','d0031d26-18da-f0d3-e7d3-5c6e1d89ca77',0,'2020-08-27 17:44:57',1),('5a069057-427f-cf32-dbbe-5f47f18c7142','69d63f4e-88ef-58c9-0a28-5f469b0dc456','6ca5fca3-9719-e9bb-0913-5c6e1dc4e355',89,'2020-08-28 09:56:53',0),('5a257de1-bbcf-2cef-6e6e-5f47f1d8db2a','824bc684-1b3b-2184-338e-5f47f0ca9e93','e5c20eac-6559-6531-9fc1-5c6e1d07319c',0,'2020-08-27 17:44:57',1),('5a35e6d6-7fc4-5033-8cab-5f47f1e80ece','69d63f4e-88ef-58c9-0a28-5f469b0dc456','74ebf998-0a54-6f0f-4a0c-5c6e1d95510e',90,'2020-08-28 09:56:53',0),('5a500739-2e98-ea44-faa1-5f47f1890d2a','824bc684-1b3b-2184-338e-5f47f0ca9e93','15c2ddf5-d59a-f00b-e3fd-5c6e1dd8862b',0,'2020-08-27 17:44:57',1),('5a5f822f-95e5-9ae9-cdcf-5f47f126b554','69d63f4e-88ef-58c9-0a28-5f469b0dc456','721fe94f-3d15-b317-6350-5c6e1d44518e',90,'2020-08-28 09:56:53',0),('5a799cce-0fae-1487-3f81-5f47f1d8b18e','824bc684-1b3b-2184-338e-5f47f0ca9e93','13c35bd9-edd9-8d1d-9148-5c6e1dd73cbe',0,'2020-08-27 17:44:57',1),('5a882145-7f94-e52f-c212-5f47f18c9d91','69d63f4e-88ef-58c9-0a28-5f469b0dc456','7972c1cc-203b-4783-9cee-5c6e1d68a18e',90,'2020-08-28 09:56:53',0),('5ab8077a-f7f7-ea5a-9a7a-5f47f1321ab8','69d63f4e-88ef-58c9-0a28-5f469b0dc456','77a70590-eb9e-e4e2-e99a-5c6e1df5b66c',90,'2020-08-28 09:56:53',0),('5ab9f19c-6ca4-9371-f41e-5f47f1715fcb','824bc684-1b3b-2184-338e-5f47f0ca9e93','195d5fd1-dcbe-f8c7-791b-5c6e1d95e515',0,'2020-08-27 17:44:57',1),('5afdcc6c-674a-50f7-7e08-5f47f1caf6a0','69d63f4e-88ef-58c9-0a28-5f469b0dc456','7046a698-1203-22a6-6d4f-5c6e1d2fe533',90,'2020-08-28 09:56:53',0),('5b250666-4a3f-9c8d-3960-5f47f1df6db4','824bc684-1b3b-2184-338e-5f47f0ca9e93','17825d0b-9ae5-f818-8af6-5c6e1d44db2b',0,'2020-08-27 17:44:57',1),('5b2d025d-5ab6-f41a-a1ff-5f47f12c4d4b','69d63f4e-88ef-58c9-0a28-5f469b0dc456','7b49e17b-7855-cfbf-fe80-5c6e1d2f9182',90,'2020-08-28 09:56:53',0),('5b562ec6-8e75-5a19-b154-5f47f1829f9a','824bc684-1b3b-2184-338e-5f47f0ca9e93','11e2c447-3cdb-c380-663b-5c6e1dbbbdef',0,'2020-08-27 17:44:57',1),('5b5bfcc7-fbbf-8105-6c28-5f47f1971034','69d63f4e-88ef-58c9-0a28-5f469b0dc456','6e74df3c-97d7-ff5a-e257-5c6e1d89cdcf',90,'2020-08-28 09:56:53',0),('5b80bfc5-c458-b7a1-e694-5f47f117808b','824bc684-1b3b-2184-338e-5f47f0ca9e93','1b120fe0-ce1d-c39b-038c-5c6e1d886a54',0,'2020-08-27 17:44:57',1),('5b8b81b9-9ab4-e1af-209b-5f47f17a056e','69d63f4e-88ef-58c9-0a28-5f469b0dc456','bef20eff-0b7e-c1bb-7a8e-5c6e1df0685c',89,'2020-08-28 09:56:53',0),('5baffec9-c43b-f5dc-1c67-5f47f1a845fb','824bc684-1b3b-2184-338e-5f47f0ca9e93','101ad241-f96c-8797-b838-5c6e1df463e2',0,'2020-08-27 17:44:57',1),('5bc0d991-dc77-44f3-3996-5f47f1774ad3','69d63f4e-88ef-58c9-0a28-5f469b0dc456','c5b282ff-1ee9-44ca-32c7-5c6e1d99ecbb',90,'2020-08-28 09:56:53',0),('5bf36d31-6458-b6e8-42fa-5f47f171a320','824bc684-1b3b-2184-338e-5f47f0ca9e93','abd4ec8c-c93c-c1e6-def1-5c6e1ddf4c9a',0,'2020-08-27 17:44:57',1),('5c1baa06-d1e8-80df-9ce4-5f47f1ed3f18','69d63f4e-88ef-58c9-0a28-5f469b0dc456','c407d548-1499-6d04-15ca-5c6e1d93705a',90,'2020-08-28 09:56:53',0),('5c1caa4e-5d54-a838-a4e9-5f47f1df8c66','824bc684-1b3b-2184-338e-5f47f0ca9e93','b28d3942-a8ed-93bf-21cd-5c6e1d0f6ae2',0,'2020-08-27 17:44:57',1),('5c46c3a1-f37e-ae16-e451-5f47f111e92c','824bc684-1b3b-2184-338e-5f47f0ca9e93','b0d731ac-5b19-327b-4e2a-5c6e1d3ad23b',0,'2020-08-27 17:44:57',1),('5c47ec9d-e1bc-da2b-307f-5f47f13c508d','69d63f4e-88ef-58c9-0a28-5f469b0dc456','c95a4d72-da3c-06c1-c24d-5c6e1de6ff39',90,'2020-08-28 09:56:53',0),('5c71db79-6b81-50fc-a07d-5f47f122f22e','824bc684-1b3b-2184-338e-5f47f0ca9e93','b5ca1507-2676-e260-08b9-5c6e1da38a6f',0,'2020-08-27 17:44:57',1),('5c762681-451f-c789-dacf-5f47f1e9ff62','69d63f4e-88ef-58c9-0a28-5f469b0dc456','c77bfcc8-254e-1ceb-08d4-5c6e1d2fbe7b',90,'2020-08-28 09:56:53',0),('5c9b4a4e-94fa-44d9-5f00-5f47f131a8e2','824bc684-1b3b-2184-338e-5f47f0ca9e93','b42943d9-bb9e-b64f-e274-5c6e1d1e54b2',0,'2020-08-27 17:44:57',1),('5ca79b36-7db9-07eb-6bba-5f47f18d7326','69d63f4e-88ef-58c9-0a28-5f469b0dc456','c24efd4f-bdc0-94a8-5feb-5c6e1de80616',90,'2020-08-28 09:56:53',0),('5cdea505-a19a-59f9-a2af-5f47f1b36a38','824bc684-1b3b-2184-338e-5f47f0ca9e93','af31a49b-acde-7cfa-34f5-5c6e1dc79084',0,'2020-08-27 17:44:57',1),('5cec4023-251c-4de6-1961-5f47f19d28e4','69d63f4e-88ef-58c9-0a28-5f469b0dc456','cb2bdcf2-b789-3402-2c32-5c6e1d1d122e',90,'2020-08-28 09:56:53',0),('5d0c3bd7-395e-51a4-7554-5f47f1201f3c','824bc684-1b3b-2184-338e-5f47f0ca9e93','b7652289-d6e5-41c6-96df-5c6e1dfb2f8a',0,'2020-08-27 17:44:57',1),('5d1c0696-32b0-2b15-76a5-5f469cf78c63','3304ef15-3d54-d514-0f52-5f469cfc6b17','c24efd4f-bdc0-94a8-5feb-5c6e1de80616',75,'2020-08-27 17:49:53',0),('5d214015-301d-8ad0-ee5b-5f47f1e4328f','69d63f4e-88ef-58c9-0a28-5f469b0dc456','c0a8ec91-575f-d850-fae9-5c6e1dbb2f31',90,'2020-08-28 09:56:53',0),('5d3f7c22-a2cb-34f9-41a0-5f47f1fc4578','824bc684-1b3b-2184-338e-5f47f0ca9e93','ad856238-c136-2afa-933c-5c6e1d1cdec2',0,'2020-08-27 17:44:57',1),('5d69d4cf-35e1-cd3f-b106-5f47f13c23bf','824bc684-1b3b-2184-338e-5f47f0ca9e93','e4fa58f2-e15d-1cf0-7dc4-5c6e1da17057',0,'2020-08-27 17:44:57',1),('5d916034-bc97-c0d5-987d-5f47f1355d7e','69d63f4e-88ef-58c9-0a28-5f469b0dc456','80ec0712-cbc7-f25a-d705-5c6e1d655f72',89,'2020-08-28 09:56:53',0),('5d9a5aa7-62f6-a3a9-5aeb-5f47f190b008','824bc684-1b3b-2184-338e-5f47f0ca9e93','eb68a252-8b3f-8d0e-b715-5c6e1ddb6365',0,'2020-08-27 17:44:57',1),('5dd6aa73-f64e-2957-57fd-5f47f18b79b4','69d63f4e-88ef-58c9-0a28-5f469b0dc456','f7d00d0d-3e0b-a249-08f5-5c6e1d5a174f',90,'2020-08-28 09:56:53',0),('5def376d-e8ee-627d-2d47-5f47f1830aad','824bc684-1b3b-2184-338e-5f47f0ca9e93','e9d42efb-bea8-e95b-c776-5c6e1d3696e6',0,'2020-08-27 17:44:57',1),('5e01eae4-f5d0-4302-0efa-5f47f1758c43','69d63f4e-88ef-58c9-0a28-5f469b0dc456','dada0a15-a601-6bd0-d323-5c6e1d2fd32f',90,'2020-08-28 09:56:53',0),('5e19f08f-6b20-7854-531c-5f47f15b1f61','824bc684-1b3b-2184-338e-5f47f0ca9e93','ee981ca9-fd8d-da61-03ed-5c6e1d57bf84',0,'2020-08-27 17:44:57',1),('5e36cc97-48ca-fc6a-ec0b-5f47f171685e','69d63f4e-88ef-58c9-0a28-5f469b0dc456','1312db1d-35b4-46f1-10e5-5c6e1d15c98e',90,'2020-08-28 09:56:53',0),('5e42df4e-1b84-c508-f3eb-5f47f170350a','824bc684-1b3b-2184-338e-5f47f0ca9e93','ecfefd1b-1800-f44f-1632-5c6e1db114d0',0,'2020-08-27 17:44:57',1),('5e68f461-6027-1455-a612-5f47f10bc1bd','69d63f4e-88ef-58c9-0a28-5f469b0dc456','114e35ef-a176-57a6-cbe4-5c6e1dde9572',90,'2020-08-28 09:56:53',0),('5e6c0cd8-1daa-70e9-2a51-5f47f18e6929','824bc684-1b3b-2184-338e-5f47f0ca9e93','e83709f6-9fef-96ff-7942-5c6e1d98ed5c',0,'2020-08-27 17:44:57',1),('5e94967d-3dc9-1938-8a57-5f47f15c14bb','69d63f4e-88ef-58c9-0a28-5f469b0dc456','bd05094c-c21b-c6bb-9649-5c6e1dbbf283',90,'2020-08-28 09:56:53',0),('5e954de8-3d82-c950-a44a-5f47f1fab590','824bc684-1b3b-2184-338e-5f47f0ca9e93','f037714b-c744-9311-3833-5c6e1d91bac5',0,'2020-08-27 17:44:57',1),('5edc170c-74ae-9cfd-2a2e-5f47f18ef61c','824bc684-1b3b-2184-338e-5f47f0ca9e93','e69dd3d3-0eab-3022-3735-5c6e1d14a076',0,'2020-08-27 17:44:57',1),('5ede040b-d14f-1c1f-46c7-5f47f162bde4','69d63f4e-88ef-58c9-0a28-5f469b0dc456','14edf037-2af6-6859-18f0-5c6e1d9ae20a',90,'2020-08-28 09:56:53',0),('5f0b69e0-c885-693c-c647-5f47f1d750cd','69d63f4e-88ef-58c9-0a28-5f469b0dc456','9e240243-5b8b-3fd9-ebdd-5c6e1d4e4fd7',90,'2020-08-28 09:56:53',0),('5f0c0d2e-d16f-7883-5ebf-5f47f15cc5a1','824bc684-1b3b-2184-338e-5f47f0ca9e93','1b163909-2912-40f9-8708-5c6e1dad4ee9',0,'2020-08-27 17:44:57',1),('5f372eb1-160f-b12f-6650-5f47f1772d4e','69d63f4e-88ef-58c9-0a28-5f469b0dc456','e941f915-63c6-f813-7912-5c6e1d687657',89,'2020-08-28 09:56:53',0),('5f3a8927-1456-67dd-359a-5f47f1e78853','824bc684-1b3b-2184-338e-5f47f0ca9e93','21a4b930-240a-495d-674b-5c6e1da71eb7',0,'2020-08-27 17:44:57',1),('5f661824-0b06-c12a-d575-5f47f1088083','69d63f4e-88ef-58c9-0a28-5f469b0dc456','f038848b-d6ea-eea1-82c9-5c6e1d8050b7',90,'2020-08-28 09:56:53',0),('5f677039-d8b5-fc19-1dad-5f47f184807b','824bc684-1b3b-2184-338e-5f47f0ca9e93','200a1c61-6924-8cd8-5af7-5c6e1d640527',0,'2020-08-27 17:44:57',1),('5f978ae6-544d-170f-6d35-5f47f1f35ffd','824bc684-1b3b-2184-338e-5f47f0ca9e93','24d68f78-a84f-a13d-7f4a-5c6e1d934bac',0,'2020-08-27 17:44:57',1),('5f9b051e-6da7-0351-cac8-5f47f1dc78ee','69d63f4e-88ef-58c9-0a28-5f469b0dc456','ee843bb5-3944-a90b-1821-5c6e1dd16399',90,'2020-08-28 09:56:53',0),('5fd9d5c9-a6a3-a320-4939-5f47f17a1bf5','824bc684-1b3b-2184-338e-5f47f0ca9e93','233a9995-db1a-2078-e61f-5c6e1df9a25d',0,'2020-08-27 17:44:57',1),('5fe15111-9fb1-482f-6fbc-5f47f1db72ff','69d63f4e-88ef-58c9-0a28-5f469b0dc456','f3ed8a4b-4202-a38e-95ab-5c6e1de1eb7b',90,'2020-08-28 09:56:53',0),('6009b5ce-01d2-a566-8d8c-5f47f1afbe9a','824bc684-1b3b-2184-338e-5f47f0ca9e93','1e6a83a2-afae-db99-ec4f-5c6e1d752ed9',0,'2020-08-27 17:44:57',1),('600f650b-f945-26eb-f1ab-5f47f1fdb579','69d63f4e-88ef-58c9-0a28-5f469b0dc456','f1ee01c7-5136-1887-b4a6-5c6e1d1b29ab',90,'2020-08-28 09:56:53',0),('603d80b0-6c81-0a31-9682-5f47f1ef60bb','824bc684-1b3b-2184-338e-5f47f0ca9e93','26783917-465d-4805-eb95-5c6e1d251635',0,'2020-08-27 17:44:57',1),('60420c0d-fa30-4895-53f0-5f47f139b51f','69d63f4e-88ef-58c9-0a28-5f469b0dc456','ecd4a57b-0e61-6ae6-26be-5c6e1dfaf4d9',90,'2020-08-28 09:56:53',0),('606b2472-0100-d450-e23b-5f47f1c533fb','824bc684-1b3b-2184-338e-5f47f0ca9e93','1cd1d38b-fce9-a981-1846-5c6e1d132836',0,'2020-08-27 17:44:57',1),('607fe528-eacf-130d-35f1-5f47f12b3e37','69d63f4e-88ef-58c9-0a28-5f469b0dc456','181a0df8-d32e-3a78-63b5-5c6e1db02bf9',90,'2020-08-28 09:56:53',0),('609bd2bf-ab6f-8326-fece-5f47f111ff00','824bc684-1b3b-2184-338e-5f47f0ca9e93','af161148-77d1-f897-76a8-5c6e1d79ad82',0,'2020-08-27 17:44:57',1),('60cc2249-b0a3-13ce-cccf-5f47f1785568','69d63f4e-88ef-58c9-0a28-5f469b0dc456','eb1dc44d-6ed8-fa1b-6fbe-5c6e1d3d094a',90,'2020-08-28 09:56:53',0),('60eb9aa9-8c3e-3f65-35ef-5f47f1cbf308','824bc684-1b3b-2184-338e-5f47f0ca9e93','b5b23ad8-066c-bd1f-1756-5c6e1dda06a1',0,'2020-08-27 17:44:57',1),('60f8380e-f7b0-d056-4926-5f47f10f709c','69d63f4e-88ef-58c9-0a28-5f469b0dc456','d4d93551-68c5-3088-cd6d-5c6e1dfc5050',89,'2020-08-28 09:56:53',0),('611675d8-bfd1-7bee-6daf-5f47f1d3b9f3','824bc684-1b3b-2184-338e-5f47f0ca9e93','b40800b6-5ceb-b18f-2021-5c6e1ddf1c71',0,'2020-08-27 17:44:57',1),('6121cef4-0ea2-629f-0435-5f47f1c94aa3','69d63f4e-88ef-58c9-0a28-5f469b0dc456','dda1ad47-01b9-046e-cd89-5c6e1d09c51d',90,'2020-08-28 09:56:53',0),('613fbe83-cf18-5959-3b4f-5f47f18060b5','824bc684-1b3b-2184-338e-5f47f0ca9e93','b8f6333c-264a-d56c-f3f7-5c6e1d6717b5',0,'2020-08-27 17:44:57',1),('61520ad4-1a67-4864-7c03-5f47f127b113','69d63f4e-88ef-58c9-0a28-5f469b0dc456','dbd75848-1e97-c63d-0ba7-5c6e1d60ef8c',90,'2020-08-28 09:56:53',0),('617f48b1-ba0a-8a1e-2edf-5f47f185ac4d','69d63f4e-88ef-58c9-0a28-5f469b0dc456','e10e0f4f-3089-f6dc-da51-5c6e1d2fda64',90,'2020-08-28 09:56:53',0),('61a92446-a890-be97-fcba-5f47f1bc4048','824bc684-1b3b-2184-338e-5f47f0ca9e93','b755c057-7846-ef4e-48a4-5c6e1d5b0de2',0,'2020-08-27 17:44:57',1),('61c01fe4-8624-a59f-6b37-5f47f1682c0f','69d63f4e-88ef-58c9-0a28-5f469b0dc456','df56ae76-a203-3777-2e68-5c6e1da9aa23',90,'2020-08-28 09:56:53',0),('61ef0344-bd14-ae51-16bc-5f469c53d898','3304ef15-3d54-d514-0f52-5f469cfc6b17','cb2bdcf2-b789-3402-2c32-5c6e1d1d122e',90,'2020-08-27 17:49:53',0),('61ef0ce3-278f-8bc2-f136-5f47f16ea42e','69d63f4e-88ef-58c9-0a28-5f469b0dc456','da15b921-e3da-b9da-7692-5c6e1d338231',90,'2020-08-28 09:56:53',0),('61f498fd-4740-3f7c-3b77-5f47f188fdba','824bc684-1b3b-2184-338e-5f47f0ca9e93','b262f627-0c6c-c476-450e-5c6e1d7af1a7',0,'2020-08-27 17:44:57',1),('6224b5da-a4c3-8371-73e2-5f47f1039023','69d63f4e-88ef-58c9-0a28-5f469b0dc456','e2be1f2e-bf7a-99e2-93a1-5c6e1d7b6bba',90,'2020-08-28 09:56:53',0),('6226721e-f769-f211-64b6-5f47f15f35c5','824bc684-1b3b-2184-338e-5f47f0ca9e93','baa3bd6a-799c-c7cb-8167-5c6e1de0778f',0,'2020-08-27 17:44:57',1),('62534e33-af4d-8e37-a59e-5f47f1e5d505','69d63f4e-88ef-58c9-0a28-5f469b0dc456','d84dbda5-0435-3268-4441-5c6e1dec53c2',90,'2020-08-28 09:56:53',0),('62549583-234e-3e4a-e4ce-5f47f16f0170','824bc684-1b3b-2184-338e-5f47f0ca9e93','b0c29e2e-087d-18f2-5b3c-5c6e1d67fc2d',0,'2020-08-27 17:44:57',1),('62806ef8-d0e0-5d5c-6041-5f47f1f94b29','824bc684-1b3b-2184-338e-5f47f0ca9e93','52c2ed19-3cfd-a2a8-3cce-5c6e1d04c3d8',0,'2020-08-27 17:44:57',1),('6280dbb1-db59-e3af-4ab1-5f47f1107b55','69d63f4e-88ef-58c9-0a28-5f469b0dc456','65807232-4fc2-aad5-52f8-5c6e1d60ad29',89,'2020-08-28 09:56:53',0),('62c53305-8654-fa68-4219-5f47f1dc63ee','69d63f4e-88ef-58c9-0a28-5f469b0dc456','6c07fded-2f91-dcfd-0a5c-5c6e1d1e158f',90,'2020-08-28 09:56:53',0),('62c6d42b-16d8-4f82-f335-5f47f118f0b6','824bc684-1b3b-2184-338e-5f47f0ca9e93','593e1378-f97b-72e9-4a72-5c6e1d9089b4',0,'2020-08-27 17:44:57',1),('62efe0fa-2ac9-f79f-fba1-5f47f1605f4f','69d63f4e-88ef-58c9-0a28-5f469b0dc456','6a6a210a-9b29-a92b-5117-5c6e1d3defe1',90,'2020-08-28 09:56:53',0),('62f94ec2-933c-6d69-b2c8-5f47f1c940c8','824bc684-1b3b-2184-338e-5f47f0ca9e93','57a40c89-305e-8dc2-31a9-5c6e1d9922d3',0,'2020-08-27 17:44:57',1),('631af93d-84d7-5be8-bb53-5f47f13f85f6','69d63f4e-88ef-58c9-0a28-5f469b0dc456','6f3ef736-d1b8-d29a-31bd-5c6e1d45d1ce',90,'2020-08-28 09:56:53',0),('63281316-8d17-e033-fb64-5f47f1a60ffc','824bc684-1b3b-2184-338e-5f47f0ca9e93','5c7c1f01-024e-a89d-9299-5c6e1d9b9c91',0,'2020-08-27 17:44:57',1),('634ed889-9c1b-51ed-e8ae-5f47f13c6712','69d63f4e-88ef-58c9-0a28-5f469b0dc456','6da34898-2906-0ca9-4d9f-5c6e1d4a8e11',90,'2020-08-28 09:56:53',0),('63539d3d-22fc-e3c2-7c35-5f47f166702f','824bc684-1b3b-2184-338e-5f47f0ca9e93','5ad89177-2751-9788-e516-5c6e1d29c5ae',0,'2020-08-27 17:44:57',1),('637e35cb-6b0b-e0ba-6ee6-5f47f14e1036','69d63f4e-88ef-58c9-0a28-5f469b0dc456','68c51dc8-b17b-d512-dec0-5c6e1dfbb5b2',90,'2020-08-28 09:56:53',0),('63803eea-258d-8dcc-75d7-5f47f1539d4f','824bc684-1b3b-2184-338e-5f47f0ca9e93','5603f923-25a8-fdd2-328c-5c6e1de852fd',0,'2020-08-27 17:44:57',1),('63c3f562-1891-22a7-0639-5f47f14cbc09','824bc684-1b3b-2184-338e-5f47f0ca9e93','5e1b71ea-7c87-c530-779a-5c6e1d9e0d17',0,'2020-08-27 17:44:57',1),('63d14fe2-df67-97d2-df0c-5f47f14adac1','69d63f4e-88ef-58c9-0a28-5f469b0dc456','70d8f01c-fd25-458c-37da-5c6e1de3ee6d',90,'2020-08-28 09:56:53',0),('63f156e7-8107-fd43-d020-5f47f13c1116','824bc684-1b3b-2184-338e-5f47f0ca9e93','54651337-2a36-a3e0-cc3f-5c6e1dd7ff25',0,'2020-08-27 17:44:57',1),('63fe2b04-f7e6-61aa-de77-5f47f1847d69','69d63f4e-88ef-58c9-0a28-5f469b0dc456','672c3b21-fc37-cdc7-6c9b-5c6e1d56963c',90,'2020-08-28 09:56:53',0),('641de032-859a-8297-63f1-5f47f1c79a06','824bc684-1b3b-2184-338e-5f47f0ca9e93','5a116d0a-93ce-3f38-6b3c-5c6e1ddfb588',0,'2020-08-27 17:44:57',1),('642acdaa-d8e7-bc19-49f4-5f47f17e2303','69d63f4e-88ef-58c9-0a28-5f469b0dc456','2bb8a788-d9b2-317e-ee27-5c6e1d99e1e0',89,'2020-08-28 09:56:53',0),('6448ca67-9990-16d6-398a-5f47f1a39c19','824bc684-1b3b-2184-338e-5f47f0ca9e93','60a6e40c-cd7c-6300-f332-5c6e1df4ba1d',0,'2020-08-27 17:44:57',1),('6456c62e-7012-b31d-7d9e-5f47f16768b1','69d63f4e-88ef-58c9-0a28-5f469b0dc456','35351234-40a5-72c4-4de3-5c6e1dacbb2e',90,'2020-08-28 09:56:53',0),('64752578-5089-1e6e-0e20-5f47f13e26c6','824bc684-1b3b-2184-338e-5f47f0ca9e93','5efb35f8-4d02-ff48-a0a1-5c6e1d8bd05a',0,'2020-08-27 17:44:57',1),('648a9e3b-1548-9c9a-4884-5f47f11f4336','69d63f4e-88ef-58c9-0a28-5f469b0dc456','338337d8-861f-20ab-eb8a-5c6e1d0794c7',90,'2020-08-28 09:56:53',0),('64b5292f-66e9-021f-a289-5f47f17ee86c','824bc684-1b3b-2184-338e-5f47f0ca9e93','6431abe6-fb9d-097a-5dc9-5c6e1d9b476d',0,'2020-08-27 17:44:57',1),('64cca396-309c-4056-0897-5f47f1109b0d','69d63f4e-88ef-58c9-0a28-5f469b0dc456','3889a69a-c00b-8f99-585b-5c6e1d8b38cb',90,'2020-08-28 09:56:53',0),('64e09919-1fc8-653f-8fcf-5f47f172ac83','824bc684-1b3b-2184-338e-5f47f0ca9e93','6280ad76-9e85-2b59-5565-5c6e1daf4e8d',0,'2020-08-27 17:44:57',1),('64fa93da-804e-1efe-e62d-5f47f132b1ce','69d63f4e-88ef-58c9-0a28-5f469b0dc456','36e993eb-3592-761c-b5b0-5c6e1d8e0760',90,'2020-08-28 09:56:53',0),('650bc697-be5e-60ed-623f-5f47f1f2b4bc','824bc684-1b3b-2184-338e-5f47f0ca9e93','5d5e2201-0772-eea0-a538-5c6e1d9fbe40',0,'2020-08-27 17:44:57',1),('65255bb0-cf3c-237f-3272-5f47f191dd08','69d63f4e-88ef-58c9-0a28-5f469b0dc456','31c44a2d-47fa-fb4f-0b01-5c6e1d201232',90,'2020-08-28 09:56:53',0),('65366027-7cec-a97e-cdd3-5f47f146a23c','824bc684-1b3b-2184-338e-5f47f0ca9e93','660bd7c0-0c8f-0395-4bc7-5c6e1d57fdbb',0,'2020-08-27 17:44:57',1),('6553334d-ca67-bf4c-2cef-5f47f110e7e3','69d63f4e-88ef-58c9-0a28-5f469b0dc456','3a22e344-99f3-e402-1503-5c6e1daeca94',90,'2020-08-28 09:56:53',0),('656209d2-75c1-fcc5-6f76-5f47f1e42b72','824bc684-1b3b-2184-338e-5f47f0ca9e93','5bbba79c-a0f1-e71e-ea73-5c6e1da0a9e2',0,'2020-08-27 17:44:57',1),('65829ac3-2b3c-dc50-9f6e-5f47f16bfa92','69d63f4e-88ef-58c9-0a28-5f469b0dc456','2f3392ef-1a9a-f99d-c414-5c6e1d868dc9',90,'2020-08-28 09:56:53',0),('65a8f384-7aa6-3ef9-71c1-5f47f185351e','824bc684-1b3b-2184-338e-5f47f0ca9e93','96a6e41c-fd1d-2d14-efec-5c6e1ddcd204',0,'2020-08-27 17:44:57',1),('65c700d6-f717-5d52-2b27-5f469cb580a7','3304ef15-3d54-d514-0f52-5f469cfc6b17','c0a8ec91-575f-d850-fae9-5c6e1dbb2f31',75,'2020-08-27 17:49:53',0),('65d693c2-8c06-4c5f-5c7a-5f47f1a2d84c','69d63f4e-88ef-58c9-0a28-5f469b0dc456','68b60ce8-53e9-437d-7bbf-5c6e1d416598',89,'2020-08-28 09:56:53',0),('65e2e662-9b36-e3f9-043c-5f47f12c2267','824bc684-1b3b-2184-338e-5f47f0ca9e93','9e36c5da-d99c-4575-d041-5c6e1d1932a5',0,'2020-08-27 17:44:57',1),('660f1cb7-e14d-6112-4368-5f47f17fee0b','824bc684-1b3b-2184-338e-5f47f0ca9e93','9c9068c8-2889-4237-c4da-5c6e1d5fd6e0',0,'2020-08-27 17:44:57',1),('66116840-21a7-c659-da4e-5f47f1d0dedd','69d63f4e-88ef-58c9-0a28-5f469b0dc456','d90400ee-40f8-2dd5-19a3-5c6e1dd03e8b',90,'2020-08-28 09:56:53',0),('663c3292-f678-5a80-5129-5f47f1366248','824bc684-1b3b-2184-338e-5f47f0ca9e93','a19598bd-ecb8-b87c-42dc-5c6e1d69db8f',0,'2020-08-27 17:44:57',1),('664bbe85-693b-f4ae-6ef6-5f47f11c00bb','69d63f4e-88ef-58c9-0a28-5f469b0dc456','bdb006b2-d132-b086-d28e-5c6e1d4cdff2',90,'2020-08-28 09:56:53',0),('666a8eaa-5d45-4ba2-7f3b-5f47f1719525','824bc684-1b3b-2184-338e-5f47f0ca9e93','9febc0e8-571c-b6d9-033d-5c6e1dadd332',0,'2020-08-27 17:44:57',1),('668e8836-817a-e1c6-2243-5f47f17c6015','69d63f4e-88ef-58c9-0a28-5f469b0dc456','10f6b83e-e87a-61fb-d12a-5c6e1dffd363',90,'2020-08-28 09:56:53',0),('66aa3bbe-5b57-d18a-36ce-5f47f1f57108','824bc684-1b3b-2184-338e-5f47f0ca9e93','9a9dea65-c49d-a612-b77a-5c6e1d860eb8',0,'2020-08-27 17:44:57',1),('66d6cba7-f457-4f8a-a6db-5f47f1623b83','824bc684-1b3b-2184-338e-5f47f0ca9e93','a53a56c9-6206-9bb2-70a8-5c6e1dd996a7',0,'2020-08-27 17:44:57',1),('66e0136a-9be3-781c-d604-5f47f1cd6718','69d63f4e-88ef-58c9-0a28-5f469b0dc456','f4460c0a-f75e-b471-0c7f-5c6e1d1459a1',90,'2020-08-28 09:56:53',0),('671aa870-36bb-b9c7-5ecb-5f47f18d8913','69d63f4e-88ef-58c9-0a28-5f469b0dc456','a2490847-3b31-01e5-f954-5c6e1daf08eb',90,'2020-08-28 09:56:53',0),('673d3d9e-e9d1-41a6-81f8-5f47f1b8f165','824bc684-1b3b-2184-338e-5f47f0ca9e93','98b4e4cc-d8f5-e99a-60d3-5c6e1dc7787c',0,'2020-08-27 17:44:57',1),('675e0807-6780-392b-a0bd-5f47f1945896','69d63f4e-88ef-58c9-0a28-5f469b0dc456','12a22378-ce15-c2f0-402b-5c6e1d224f96',90,'2020-08-28 09:56:53',0),('676d02db-41bb-e716-6545-5f47f1b8c0b5','824bc684-1b3b-2184-338e-5f47f0ca9e93','82fe24f4-3ce6-a0a9-227d-5c6e1df8c135',0,'2020-08-27 17:44:57',1),('67ac8755-fa20-942e-9b21-5f47f1a1d203','69d63f4e-88ef-58c9-0a28-5f469b0dc456','86bc0feb-f287-8ee8-345a-5c6e1dbd09d3',90,'2020-08-28 09:56:53',0),('67b3cb1c-e434-1197-e79c-5f47f159402b','824bc684-1b3b-2184-338e-5f47f0ca9e93','8a32b4ee-2ae3-f3c0-9e82-5c6e1d41a51c',0,'2020-08-27 17:44:57',1),('67dda65a-9625-8013-844a-5f47f1c2fbcf','824bc684-1b3b-2184-338e-5f47f0ca9e93','88712337-2eb8-7e5f-af4c-5c6e1d94bd75',0,'2020-08-27 17:44:57',1),('67de931a-cce0-c0ad-890c-5f47f1c9498c','69d63f4e-88ef-58c9-0a28-5f469b0dc456','453b3704-5824-5632-462a-5c6e1dce0981',89,'2020-08-28 09:56:53',0),('680b1e37-20b4-416b-6c41-5f47f1a79e29','824bc684-1b3b-2184-338e-5f47f0ca9e93','8da7244b-1fd0-52bc-6c70-5c6e1d8b6bee',0,'2020-08-27 17:44:57',1),('680e014a-17cf-1e18-fe39-5f47f12325ff','69d63f4e-88ef-58c9-0a28-5f469b0dc456','53dd2dcb-b198-1a9c-6c63-5c6e1d958589',90,'2020-08-28 09:56:53',0),('68367598-770e-aa8d-b029-5f47f1412ab1','824bc684-1b3b-2184-338e-5f47f0ca9e93','8be9f5f8-465c-c9ea-d1bb-5c6e1dbecaff',0,'2020-08-27 17:44:57',1),('683a1fa4-b0b3-4b7d-5d6e-5f47f15d1e8a','69d63f4e-88ef-58c9-0a28-5f469b0dc456','5204b668-b7ff-ccdd-8e4d-5c6e1d632d90',90,'2020-08-28 09:56:53',0),('686319e4-f587-0781-e463-5f47f14f1a27','824bc684-1b3b-2184-338e-5f47f0ca9e93','86b6537f-e557-3610-62c3-5c6e1dd8169c',0,'2020-08-27 17:44:57',1),('68697957-b86a-b2e0-2a59-5f47f16bae56','69d63f4e-88ef-58c9-0a28-5f469b0dc456','5789c7f2-cad5-5b8c-4353-5c6e1d5345eb',90,'2020-08-28 09:56:53',0),('68a6ee75-9517-588d-960d-5f47f18d5baa','824bc684-1b3b-2184-338e-5f47f0ca9e93','8f7040ab-20b9-1e82-89d6-5c6e1d5f3de2',0,'2020-08-27 17:44:57',1),('68bc5187-a005-3a40-fd03-5f47f1a3d15d','69d63f4e-88ef-58c9-0a28-5f469b0dc456','55b878d0-2944-bdc5-2d7a-5c6e1d195b81',90,'2020-08-28 09:56:53',0),('68d490d2-e4a8-8c2f-33be-5f47f1c90045','824bc684-1b3b-2184-338e-5f47f0ca9e93','84d457d8-2ba5-48a6-5a1b-5c6e1d453ed0',0,'2020-08-27 17:44:57',1),('6900d5f1-f04b-c7a1-a631-5f47f1d7f44e','69d63f4e-88ef-58c9-0a28-5f469b0dc456','502a38bf-fe4f-4c7c-33b2-5c6e1d7254d0',90,'2020-08-28 09:56:53',0),('6903b1ea-aa91-a7d2-140a-5f47f1d5adbe','824bc684-1b3b-2184-338e-5f47f0ca9e93','23909eed-24e9-1383-65a6-5c6e1d4dc04a',0,'2020-08-27 17:44:57',1),('693561d4-709f-e997-49f1-5f47f17b2ad2','824bc684-1b3b-2184-338e-5f47f0ca9e93','2cf32cca-b9b9-409b-bd3e-5c6e1dfd8c3b',0,'2020-08-27 17:44:57',1),('693ab95c-e125-1f75-d648-5f47f1af7070','69d63f4e-88ef-58c9-0a28-5f469b0dc456','5962e588-9521-bc29-9540-5c6e1d63b93c',90,'2020-08-28 09:56:53',0),('695f0f10-d33f-a863-e4d6-5f47f1d878eb','824bc684-1b3b-2184-338e-5f47f0ca9e93','2ae367b2-742d-5ab8-1ea2-5c6e1db4adef',0,'2020-08-27 17:44:57',1),('69620665-9c73-9661-59df-5f469ccd9b56','3304ef15-3d54-d514-0f52-5f469cfc6b17','80ec0712-cbc7-f25a-d705-5c6e1d655f72',89,'2020-08-27 17:49:53',0),('6970a664-324a-e56a-c5d6-5f47f1b75dbe','69d63f4e-88ef-58c9-0a28-5f469b0dc456','4e23c1ff-9369-80c8-9296-5c6e1d9eda74',90,'2020-08-28 09:56:53',0),('69a0ef43-9846-0e7e-7b9a-5f47f1dc14e0','824bc684-1b3b-2184-338e-5f47f0ca9e93','30b24203-0540-da78-baa4-5c6e1dd6ddce',0,'2020-08-27 17:44:57',1),('69c07b5c-9400-3344-03a7-5f47f1374c03','69d63f4e-88ef-58c9-0a28-5f469b0dc456','392b4520-bc4a-0b1f-059f-5c6e1d653b57',89,'2020-08-28 09:56:53',0),('69cbaff1-babb-253f-d205-5f47f12d4472','824bc684-1b3b-2184-338e-5f47f0ca9e93','2ebfecbf-817e-c0f6-494c-5c6e1d375b9d',0,'2020-08-27 17:44:57',1),('6a02c9aa-ecbe-ca15-6bac-5f47f136e6ea','824bc684-1b3b-2184-338e-5f47f0ca9e93','273511c0-4119-a9e5-de4c-5c6e1d59d6dc',0,'2020-08-27 17:44:57',1),('6a2e13fd-0f08-53f4-538d-5f47f13867b5','824bc684-1b3b-2184-338e-5f47f0ca9e93','3273e589-c6a6-c63a-53fc-5c6e1d017b90',0,'2020-08-27 17:44:57',1),('6a5b892a-c5d1-9b45-7e25-5f47f15459b4','824bc684-1b3b-2184-338e-5f47f0ca9e93','255f689c-30fa-9743-6b97-5c6e1d16820d',0,'2020-08-27 17:44:57',1),('6adc29a7-9248-4e95-a3b3-5f47f11a481d','69d63f4e-88ef-58c9-0a28-5f469b0dc456','3fa48fbe-c3d1-67ba-7543-5c6e1d64f949',90,'2020-08-28 09:56:53',0),('6ae126b7-4c1b-188f-3a2a-5f47f1b84591','824bc684-1b3b-2184-338e-5f47f0ca9e93','2d4c98ca-5d2e-6f0a-17fc-5c6e1d9fd7b6',0,'2020-08-27 17:44:57',1),('6b0ca475-b44e-db2e-c3e6-5f47f1546652','824bc684-1b3b-2184-338e-5f47f0ca9e93','3516982c-ed8e-543f-3813-5c6e1d0bdfbd',0,'2020-08-27 17:44:57',1),('6b3c4758-c09f-32ab-4924-5f47f11e905a','824bc684-1b3b-2184-338e-5f47f0ca9e93','337b8214-09d6-962e-b12b-5c6e1deefec8',0,'2020-08-27 17:44:57',1),('6b44ee82-7d90-ad1f-bae1-5f47f12dd8cb','69d63f4e-88ef-58c9-0a28-5f469b0dc456','3e0a8d82-3855-407e-0ac4-5c6e1d0cdf9f',90,'2020-08-28 09:56:53',0),('6b86c82d-266e-f526-a1f2-5f47f1e6db01','824bc684-1b3b-2184-338e-5f47f0ca9e93','3852582d-e33f-175c-76b2-5c6e1d893051',0,'2020-08-27 17:44:57',1),('6b9f7e2b-5393-3c21-e963-5f47f1f79b3c','69d63f4e-88ef-58c9-0a28-5f469b0dc456','42dbf3b7-2439-13c1-da55-5c6e1dd94e7d',90,'2020-08-28 09:56:53',0),('6bb3d20a-5444-6164-1fad-5f47f102448a','824bc684-1b3b-2184-338e-5f47f0ca9e93','36b63ee1-ab68-5783-63da-5c6e1de6b4e5',0,'2020-08-27 17:44:57',1),('6bd4ffc6-0d09-6840-b589-5f47f1f7c2d2','69d63f4e-88ef-58c9-0a28-5f469b0dc456','413eb557-390e-1809-3fee-5c6e1db396b6',90,'2020-08-28 09:56:53',0),('6be400c0-f7b2-a197-9509-5f47f1280cdb','824bc684-1b3b-2184-338e-5f47f0ca9e93','31cac616-99da-efcf-aa0f-5c6e1d3afd35',0,'2020-08-27 17:44:57',1),('6c0f69eb-aac1-846f-cc0c-5f47f16cf37b','69d63f4e-88ef-58c9-0a28-5f469b0dc456','3c6faf0b-9351-4b5a-34f1-5c6e1d899517',90,'2020-08-28 09:56:53',0),('6c154ef1-246e-8635-d5cd-5f47f1c57bfc','824bc684-1b3b-2184-338e-5f47f0ca9e93','39f45967-6ace-927e-9b01-5c6e1daa3eda',0,'2020-08-27 17:44:57',1),('6c465e78-4124-372e-6db8-5f47f102e508','69d63f4e-88ef-58c9-0a28-5f469b0dc456','447e4027-4eb0-7267-7547-5c6e1dda96aa',90,'2020-08-28 09:56:53',0),('6c5510ef-5175-10bd-bfac-5f47f1c52b3d','824bc684-1b3b-2184-338e-5f47f0ca9e93','2efef55f-d14c-224a-1cc6-5c6e1d1d53cf',0,'2020-08-27 17:44:57',1),('6c9f66e8-d78d-f09a-959b-5f47f1ea56a3','824bc684-1b3b-2184-338e-5f47f0ca9e93','4096a9d2-bd4a-eb01-973f-5c6e1d5a45e2',0,'2020-08-27 17:44:57',1),('6ca9cff3-368d-79af-7514-5f47f1935736','69d63f4e-88ef-58c9-0a28-5f469b0dc456','3acf69d8-9b59-caed-5eb9-5c6e1d95d81a',90,'2020-08-28 09:56:53',0),('6ccb24f6-94cf-0ff4-669e-5f47f157612d','824bc684-1b3b-2184-338e-5f47f0ca9e93','47288bba-4c05-a22a-8c63-5c6e1d3dc05c',0,'2020-08-27 17:44:57',1),('6cd00107-ef3c-6f89-b0a3-5f469c0e32c8','3304ef15-3d54-d514-0f52-5f469cfc6b17','f7d00d0d-3e0b-a249-08f5-5c6e1d5a174f',-99,'2020-08-27 17:49:53',0),('6cdb10fb-df28-5c1c-503a-5f47f19d6c3c','69d63f4e-88ef-58c9-0a28-5f469b0dc456','a9dee868-a6e3-408e-7c68-5c6e1da2e377',89,'2020-08-28 09:56:53',0),('6cf84046-3419-9ed5-2196-5f47f1d2d897','824bc684-1b3b-2184-338e-5f47f0ca9e93','458a5983-6996-5853-fe72-5c6e1d222fc5',0,'2020-08-27 17:44:57',1),('6d048f1c-ecec-516a-2846-5f47f19cd538','69d63f4e-88ef-58c9-0a28-5f469b0dc456','b15af48d-52d9-ef94-0fee-5c6e1d9f03f8',90,'2020-08-28 09:56:53',0),('6d22cd22-a318-6b92-225d-5f47f143db1a','824bc684-1b3b-2184-338e-5f47f0ca9e93','4a657e01-e916-f669-a099-5c6e1d1e5567',0,'2020-08-27 17:44:57',1),('6d3269b3-fefd-a0b9-077d-5f47f176b45e','69d63f4e-88ef-58c9-0a28-5f469b0dc456','afa20a90-5510-6d72-b11a-5c6e1d235b3e',90,'2020-08-28 09:56:53',0),('6d51df65-b227-b1be-18ed-5f47f1384f07','824bc684-1b3b-2184-338e-5f47f0ca9e93','48c6d617-d7a0-6fd8-c0a4-5c6e1d8a861c',0,'2020-08-27 17:44:57',1),('6d8b950d-8adc-fb50-6d5d-5f47f1c2307d','69d63f4e-88ef-58c9-0a28-5f469b0dc456','b4dc8091-0f5f-c9bf-f80c-5c6e1d875df1',90,'2020-08-28 09:56:53',0),('6d9550af-ecff-dbb3-3c15-5f47f1efc13f','824bc684-1b3b-2184-338e-5f47f0ca9e93','43e09d57-a48f-6375-1331-5c6e1d87f59a',0,'2020-08-27 17:44:57',1),('6db83203-f20d-1976-ac95-5f47f16fb151','69d63f4e-88ef-58c9-0a28-5f469b0dc456','b31c0fe7-ebd4-0419-2940-5c6e1d73fa54',90,'2020-08-28 09:56:53',0),('6dc1415b-0ade-6271-8122-5f47f142e247','824bc684-1b3b-2184-338e-5f47f0ca9e93','4c0a451b-4439-f8d8-2592-5c6e1d1fe047',0,'2020-08-27 17:44:57',1),('6deb1cf4-f10f-c580-520f-5f47f1357817','69d63f4e-88ef-58c9-0a28-5f469b0dc456','ade5ab40-975e-86e3-e4f9-5c6e1dc0c8a1',90,'2020-08-28 09:56:53',0),('6df26394-6e41-b8fb-79cc-5f47f112fc6c','824bc684-1b3b-2184-338e-5f47f0ca9e93','42416112-83ca-03b7-352a-5c6e1db0f5e5',0,'2020-08-27 17:44:57',1),('6e1c4680-4d56-45fd-b434-5f47f1e61833','69d63f4e-88ef-58c9-0a28-5f469b0dc456','b69e3b35-c9fe-e7f3-8a3c-5c6e1d69236a',90,'2020-08-28 09:56:53',0),('6e2332ec-3bd9-10c9-b45d-5f47f15d43e3','824bc684-1b3b-2184-338e-5f47f0ca9e93','db873033-8e64-6de0-193d-5c6e1d2f494c',0,'2020-08-27 17:44:57',1),('6e5639fb-a7e0-aa14-fb9d-5f47f10ac0aa','69d63f4e-88ef-58c9-0a28-5f469b0dc456','ac00ed38-12f8-1e3e-18da-5c6e1d059af2',90,'2020-08-28 09:56:53',0),('6e5841ff-52a8-b687-5e9d-5f47f1c8f29e','824bc684-1b3b-2184-338e-5f47f0ca9e93','e32106b1-5bea-8fc2-af47-5c6e1d9e72f7',0,'2020-08-27 17:44:57',1),('6e9e8afc-d5c0-0fed-9ad1-5f47f1306b25','824bc684-1b3b-2184-338e-5f47f0ca9e93','e18fff67-dccb-b854-fb52-5c6e1d1c257e',0,'2020-08-27 17:44:57',1),('6ea6e2e7-3863-e463-3e34-5f47f16e718f','69d63f4e-88ef-58c9-0a28-5f469b0dc456','5d297527-1f3f-14f3-d10f-5c6e1d048547',89,'2020-08-28 09:56:53',0),('6ecbd4de-fff3-9a9c-8475-5f47f169291c','824bc684-1b3b-2184-338e-5f47f0ca9e93','e64249f4-d1f2-0677-2be6-5c6e1d48a985',0,'2020-08-27 17:44:57',1),('6ed5eaaf-89a1-e852-1fca-5f47f183cb07','69d63f4e-88ef-58c9-0a28-5f469b0dc456','640483b3-6ace-7d25-28bc-5c6e1d09e2f4',90,'2020-08-28 09:56:53',0),('6efc58a1-64b9-63f3-bdb2-5f47f1162fa7','824bc684-1b3b-2184-338e-5f47f0ca9e93','e4b7d2d6-a856-a13b-9e18-5c6e1d70e0c0',0,'2020-08-27 17:44:57',1),('6f0a4106-e288-da23-28e0-5f47f110541a','69d63f4e-88ef-58c9-0a28-5f469b0dc456','623518f3-d355-4c06-6dbd-5c6e1dc6f618',90,'2020-08-28 09:56:53',0),('6f2c69f7-8636-cf3f-bd21-5f47f1826216','824bc684-1b3b-2184-338e-5f47f0ca9e93','dff9e8c1-7bca-a0ba-d00a-5c6e1d2200da',0,'2020-08-27 17:44:57',1),('6f374141-a133-bf13-9772-5f47f12d401d','69d63f4e-88ef-58c9-0a28-5f469b0dc456','67729b81-c610-e9b5-6626-5c6e1d6a9701',90,'2020-08-28 09:56:53',0),('6f7d83f0-18ef-cae3-1b39-5f47f1c183c8','69d63f4e-88ef-58c9-0a28-5f469b0dc456','65c01214-075a-23ad-1a49-5c6e1d0c7711',90,'2020-08-28 09:56:53',0),('6f835ac7-36bf-65b3-c952-5f47f1d4998b','824bc684-1b3b-2184-338e-5f47f0ca9e93','e7d1dbec-7b69-37d5-6b62-5c6e1da40e84',0,'2020-08-27 17:44:57',1),('6fb19c2f-7d0c-88a8-87fc-5f47f12b2cd6','824bc684-1b3b-2184-338e-5f47f0ca9e93','de0488d2-31a1-08f9-d295-5c6e1dc33038',0,'2020-08-27 17:44:57',1),('6fb42fbb-35f4-df2a-6f8d-5f47f106058e','69d63f4e-88ef-58c9-0a28-5f469b0dc456','6080d88e-9d8f-594a-949c-5c6e1de8ce23',90,'2020-08-28 09:56:53',0),('6fdfd99f-0d39-6bc0-1030-5f47f19124cc','824bc684-1b3b-2184-338e-5f47f0ca9e93','6ca5fca3-9719-e9bb-0913-5c6e1dc4e355',0,'2020-08-27 17:44:57',1),('6fe6009e-6779-e8d2-5f68-5f47f11becf0','69d63f4e-88ef-58c9-0a28-5f469b0dc456','6929e76d-97ed-b6c4-d138-5c6e1d2fa293',90,'2020-08-28 09:56:53',0),('700c1532-a194-94fc-4e2b-5f47f1ec3606','824bc684-1b3b-2184-338e-5f47f0ca9e93','74ebf998-0a54-6f0f-4a0c-5c6e1d95510e',0,'2020-08-27 17:44:57',1),('701cbe56-ea51-e7e2-3e4b-5f47f1824475','69d63f4e-88ef-58c9-0a28-5f469b0dc456','5ed3bcca-6841-1c42-dec8-5c6e1dcd1654',90,'2020-08-28 09:56:53',0),('7039d345-b764-c11f-4cfd-5f47f18286b6','824bc684-1b3b-2184-338e-5f47f0ca9e93','721fe94f-3d15-b317-6350-5c6e1d44518e',0,'2020-08-27 17:44:57',1),('70565c5f-f6b5-7d9d-f56b-5f47f150693d','69d63f4e-88ef-58c9-0a28-5f469b0dc456','4abd9300-75b3-c2d2-685e-5c6e1df785c7',89,'2020-08-28 09:56:53',0),('7082b3e5-4b82-626e-7549-5f47f1e9f323','824bc684-1b3b-2184-338e-5f47f0ca9e93','7972c1cc-203b-4783-9cee-5c6e1d68a18e',0,'2020-08-27 17:44:57',1),('70a1bebc-0ce4-30c6-5683-5f47f14d7149','69d63f4e-88ef-58c9-0a28-5f469b0dc456','51468e5a-b69b-bd8d-77b6-5c6e1d52286e',90,'2020-08-28 09:56:53',0),('70b015e7-e268-3fec-154d-5f47f1371465','824bc684-1b3b-2184-338e-5f47f0ca9e93','77a70590-eb9e-e4e2-e99a-5c6e1df5b66c',0,'2020-08-27 17:44:57',1),('70cf26ca-f1e8-de14-16b0-5f47f1990b2e','69d63f4e-88ef-58c9-0a28-5f469b0dc456','4fa28488-be59-b43c-3a3f-5c6e1de2faca',90,'2020-08-28 09:56:53',0),('70deffba-e65d-b176-0f6f-5f47f1f23a80','824bc684-1b3b-2184-338e-5f47f0ca9e93','7046a698-1203-22a6-6d4f-5c6e1d2fe533',0,'2020-08-27 17:44:57',1),('710b865d-76fb-f9b9-0c9b-5f47f167f73f','824bc684-1b3b-2184-338e-5f47f0ca9e93','7b49e17b-7855-cfbf-fe80-5c6e1d2f9182',0,'2020-08-27 17:44:57',1),('7138e422-a661-b3c6-252e-5f47f117ea1b','69d63f4e-88ef-58c9-0a28-5f469b0dc456','54928ee5-4276-ffbe-fdb1-5c6e1dc20efa',90,'2020-08-28 09:56:53',0),('713ba7df-1b93-ed38-df3a-5f47f1b8c44f','824bc684-1b3b-2184-338e-5f47f0ca9e93','6e74df3c-97d7-ff5a-e257-5c6e1d89cdcf',0,'2020-08-27 17:44:57',1),('71812581-d583-1e2f-4408-5f47f16f5934','69d63f4e-88ef-58c9-0a28-5f469b0dc456','52e9af88-fc3a-2da4-8afd-5c6e1d7802ed',90,'2020-08-28 09:56:53',0),('718782ec-1b33-5b9d-3ae1-5f47f1e6d3e4','824bc684-1b3b-2184-338e-5f47f0ca9e93','bef20eff-0b7e-c1bb-7a8e-5c6e1df0685c',0,'2020-08-27 17:44:57',1),('71bec3a3-df9d-218c-035b-5f47f192035e','824bc684-1b3b-2184-338e-5f47f0ca9e93','c5b282ff-1ee9-44ca-32c7-5c6e1d99ecbb',0,'2020-08-27 17:44:57',1),('71c10b9f-fdb3-bcc7-cd2c-5f469c0b5d79','3304ef15-3d54-d514-0f52-5f469cfc6b17','dada0a15-a601-6bd0-d323-5c6e1d2fd32f',75,'2020-08-27 17:49:53',0),('71c4579b-5662-f831-d840-5f47f131c951','69d63f4e-88ef-58c9-0a28-5f469b0dc456','4dff9ed3-fe53-4b2e-38fc-5c6e1d318481',90,'2020-08-28 09:56:53',0),('71efdaab-b80a-c07c-a20a-5f47f14c6ba8','824bc684-1b3b-2184-338e-5f47f0ca9e93','c407d548-1499-6d04-15ca-5c6e1d93705a',0,'2020-08-27 17:44:57',1),('71f1b8d9-2dfa-bd27-405b-5f47f17ba1ad','69d63f4e-88ef-58c9-0a28-5f469b0dc456','5638e0fd-5245-0cd9-ce3a-5c6e1de3594b',90,'2020-08-28 09:56:53',0),('721fba66-229d-7c15-1691-5f47f1ce9cb2','69d63f4e-88ef-58c9-0a28-5f469b0dc456','4c5e89e7-59ec-82d0-2835-5c6e1d1e856d',90,'2020-08-28 09:56:53',0),('72233ae1-a53e-4c0b-ab1f-5f47f1af8269','824bc684-1b3b-2184-338e-5f47f0ca9e93','c95a4d72-da3c-06c1-c24d-5c6e1de6ff39',0,'2020-08-27 17:44:57',1),('726ebe15-2b58-1c2c-7409-5f47f1ad8dbf','69d63f4e-88ef-58c9-0a28-5f469b0dc456','184cafd9-5da5-233b-61b2-5c6e1dcc1ae2',89,'2020-08-28 09:56:53',0),('72701aae-e12f-ebdb-41b9-5f47f11bc61e','824bc684-1b3b-2184-338e-5f47f0ca9e93','c77bfcc8-254e-1ceb-08d4-5c6e1d2fbe7b',0,'2020-08-27 17:44:57',1),('72a1a175-1192-6d0b-a4dc-5f47f1063dbe','69d63f4e-88ef-58c9-0a28-5f469b0dc456','1eca7007-450c-966f-c0f1-5c6e1d074aee',90,'2020-08-28 09:56:53',0),('72a5c71d-c18e-86ae-5dad-5f47f1d46622','824bc684-1b3b-2184-338e-5f47f0ca9e93','c24efd4f-bdc0-94a8-5feb-5c6e1de80616',0,'2020-08-27 17:44:57',1),('72cd8b96-2e0d-c139-a3a3-5f47f155060e','69d63f4e-88ef-58c9-0a28-5f469b0dc456','1d2877da-72f4-f0e6-a6bc-5c6e1da8da72',90,'2020-08-28 09:56:53',0),('72d4c81f-27b8-b492-8894-5f47f15c8d11','824bc684-1b3b-2184-338e-5f47f0ca9e93','cb2bdcf2-b789-3402-2c32-5c6e1d1d122e',0,'2020-08-27 17:44:57',1),('73044c52-c411-bad1-2dd0-5f47f14aa67f','69d63f4e-88ef-58c9-0a28-5f469b0dc456','22996860-ab57-6633-73f6-5c6e1d5dcb18',90,'2020-08-28 09:56:53',0),('73145892-ee9e-4412-b5cb-5f47f1f912a6','824bc684-1b3b-2184-338e-5f47f0ca9e93','c0a8ec91-575f-d850-fae9-5c6e1dbb2f31',0,'2020-08-27 17:44:57',1),('7336214f-43c5-204c-ce88-5f47f106d744','69d63f4e-88ef-58c9-0a28-5f469b0dc456','206ab75e-6ea7-cf2a-57ea-5c6e1d9ebd40',90,'2020-08-28 09:56:53',0),('73599440-3f10-0404-1d5d-5f47f1e42b43','824bc684-1b3b-2184-338e-5f47f0ca9e93','80ec0712-cbc7-f25a-d705-5c6e1d655f72',0,'2020-08-27 17:44:57',1),('7386ce63-7099-7882-a5de-5f47f18062cf','69d63f4e-88ef-58c9-0a28-5f469b0dc456','1b7cf5f1-f8ac-d35a-0532-5c6e1d6bd505',90,'2020-08-28 09:56:53',0),('7387dc77-95ab-c5e9-f416-5f47f14bba72','824bc684-1b3b-2184-338e-5f47f0ca9e93','f7d00d0d-3e0b-a249-08f5-5c6e1d5a174f',0,'2020-08-27 17:44:57',1),('73b29e9a-7975-0b23-d542-5f47f19cd30b','824bc684-1b3b-2184-338e-5f47f0ca9e93','dada0a15-a601-6bd0-d323-5c6e1d2fd32f',0,'2020-08-27 17:44:57',1),('73c474a7-1290-3546-39cb-5f47f1ac4f1c','69d63f4e-88ef-58c9-0a28-5f469b0dc456','245a684c-7f66-e44a-a4c6-5c6e1dfe0a22',90,'2020-08-28 09:56:53',0),('73dd994c-80a0-928d-d6c1-5f47f131493e','824bc684-1b3b-2184-338e-5f47f0ca9e93','1312db1d-35b4-46f1-10e5-5c6e1d15c98e',0,'2020-08-27 17:44:57',1),('740348b9-15fd-1583-d7ff-5f47f12ebac8','69d63f4e-88ef-58c9-0a28-5f469b0dc456','19e5dfc5-63d8-d00f-4a5c-5c6e1de57f20',90,'2020-08-28 09:56:53',0),('740c3efd-93b9-b34e-6df5-5f47f1e5e108','824bc684-1b3b-2184-338e-5f47f0ca9e93','114e35ef-a176-57a6-cbe4-5c6e1dde9572',0,'2020-08-27 17:44:57',1),('744b717f-c059-a180-1008-5f47f16c8da5','824bc684-1b3b-2184-338e-5f47f0ca9e93','bd05094c-c21b-c6bb-9649-5c6e1dbbf283',0,'2020-08-27 17:44:57',1),('7478902a-94a3-5935-9aba-5f47f1456181','824bc684-1b3b-2184-338e-5f47f0ca9e93','14edf037-2af6-6859-18f0-5c6e1d9ae20a',0,'2020-08-27 17:44:57',1),('7495b9e4-2202-63d2-4598-5f47f1a44b85','69d63f4e-88ef-58c9-0a28-5f469b0dc456','652163fe-3269-b1f4-9e3f-5c6e1d37dbcf',89,'2020-08-28 09:56:53',0),('74a4ebaa-dcbb-9570-3f19-5f47f1e90b36','824bc684-1b3b-2184-338e-5f47f0ca9e93','9e240243-5b8b-3fd9-ebdd-5c6e1d4e4fd7',0,'2020-08-27 17:44:57',1),('74cad154-4a10-73b2-2925-5f47f1075368','69d63f4e-88ef-58c9-0a28-5f469b0dc456','6b7c675b-97bd-5d96-6453-5c6e1d4f434a',90,'2020-08-28 09:56:53',0),('74cedd56-98e2-c499-9eae-5f47f1c9a25f','824bc684-1b3b-2184-338e-5f47f0ca9e93','e941f915-63c6-f813-7912-5c6e1d687657',0,'2020-08-27 17:44:57',1),('74fa7510-f88a-80d6-ccab-5f47f1b967b9','69d63f4e-88ef-58c9-0a28-5f469b0dc456','69ea34b6-878a-0c7f-7878-5c6e1d97c3e4',90,'2020-08-28 09:56:53',0),('74fea77f-4e68-96d9-0e5c-5f47f1647c82','824bc684-1b3b-2184-338e-5f47f0ca9e93','f038848b-d6ea-eea1-82c9-5c6e1d8050b7',0,'2020-08-27 17:44:57',1),('7527032c-be81-8e2d-d54e-5f469cd3bde1','3304ef15-3d54-d514-0f52-5f469cfc6b17','1312db1d-35b4-46f1-10e5-5c6e1d15c98e',75,'2020-08-27 17:49:53',0),('752decac-77af-1bc2-b40f-5f47f1b43f96','824bc684-1b3b-2184-338e-5f47f0ca9e93','ee843bb5-3944-a90b-1821-5c6e1dd16399',0,'2020-08-27 17:44:57',1),('7532e292-069f-52f7-8b35-5f47f15cd447','69d63f4e-88ef-58c9-0a28-5f469b0dc456','6ecb3a28-bd02-04f4-879d-5c6e1dccaeb9',90,'2020-08-28 09:56:53',0),('7578e7f7-5e0c-d075-bde1-5f47f160acc9','824bc684-1b3b-2184-338e-5f47f0ca9e93','f3ed8a4b-4202-a38e-95ab-5c6e1de1eb7b',0,'2020-08-27 17:44:57',1),('75981559-a8fb-48a8-41d4-5f47f1b7235c','69d63f4e-88ef-58c9-0a28-5f469b0dc456','6d2d4783-4ec1-d9fc-0c70-5c6e1d1755c6',90,'2020-08-28 09:56:53',0),('75a980f9-8c3c-5a0d-6418-5f47f13d01a5','824bc684-1b3b-2184-338e-5f47f0ca9e93','f1ee01c7-5136-1887-b4a6-5c6e1d1b29ab',0,'2020-08-27 17:44:57',1),('75d9e14d-08e2-c415-057b-5f47f11c9eba','824bc684-1b3b-2184-338e-5f47f0ca9e93','ecd4a57b-0e61-6ae6-26be-5c6e1dfaf4d9',0,'2020-08-27 17:44:57',1),('75da2a3b-a745-2cc9-1023-5f47f1f64b61','69d63f4e-88ef-58c9-0a28-5f469b0dc456','68556931-3a69-e3fa-3a95-5c6e1d28b747',90,'2020-08-28 09:56:53',0),('760fb49b-f439-2b6b-b9a4-5f47f1f7d5e2','824bc684-1b3b-2184-338e-5f47f0ca9e93','181a0df8-d32e-3a78-63b5-5c6e1db02bf9',0,'2020-08-27 17:44:57',1),('762b8878-ed51-fcbb-1071-5f47f1981ce3','69d63f4e-88ef-58c9-0a28-5f469b0dc456','705b169d-be44-c4b1-1ded-5c6e1debbb34',90,'2020-08-28 09:56:53',0),('7661af5f-fe53-6cff-97b3-5f47f122601e','824bc684-1b3b-2184-338e-5f47f0ca9e93','eb1dc44d-6ed8-fa1b-6fbe-5c6e1d3d094a',0,'2020-08-27 17:44:57',1),('76826f23-89dc-ae30-59b2-5f47f1b48e9e','69d63f4e-88ef-58c9-0a28-5f469b0dc456','66c644d2-69ae-d214-fe80-5c6e1d9aafb1',90,'2020-08-28 09:56:53',0),('768d7850-5c86-789f-572a-5f47f1fe5090','824bc684-1b3b-2184-338e-5f47f0ca9e93','d4d93551-68c5-3088-cd6d-5c6e1dfc5050',0,'2020-08-27 17:44:57',1),('76b59b83-3694-ff5f-2648-5f47f1cb28d0','824bc684-1b3b-2184-338e-5f47f0ca9e93','dda1ad47-01b9-046e-cd89-5c6e1d09c51d',0,'2020-08-27 17:44:57',1),('76be58bd-0287-9f3e-e7c0-5f47f1a31f92','69d63f4e-88ef-58c9-0a28-5f469b0dc456','5470bd94-dff3-8a95-720a-5c6e1dbd7da5',89,'2020-08-28 09:56:53',0),('76e1562e-0e88-a234-2471-5f47f1c41957','824bc684-1b3b-2184-338e-5f47f0ca9e93','dbd75848-1e97-c63d-0ba7-5c6e1d60ef8c',0,'2020-08-27 17:44:57',1),('76f4430e-1123-ff8b-57e6-5f47f1baebad','69d63f4e-88ef-58c9-0a28-5f469b0dc456','5acb056c-34b1-a7a7-b1f6-5c6e1dee52c4',90,'2020-08-28 09:56:53',0),('770e730a-bfed-adcd-15d7-5f47f15d65a8','824bc684-1b3b-2184-338e-5f47f0ca9e93','e10e0f4f-3089-f6dc-da51-5c6e1d2fda64',0,'2020-08-27 17:44:57',1),('774c4620-512f-a5bf-a356-5f47f148a14e','69d63f4e-88ef-58c9-0a28-5f469b0dc456','593a5e01-2184-6ac9-0aef-5c6e1d7818e2',90,'2020-08-28 09:56:53',0),('777cf0b8-1eb1-0c7c-4c37-5f47f1d357c7','69d63f4e-88ef-58c9-0a28-5f469b0dc456','5de87979-2cf3-acb0-b065-5c6e1dfdc2c3',90,'2020-08-28 09:56:53',0),('7791e71a-cacf-3635-d488-5f47f1c652ff','824bc684-1b3b-2184-338e-5f47f0ca9e93','df56ae76-a203-3777-2e68-5c6e1da9aa23',0,'2020-08-27 17:44:57',1),('77aa7560-b2b6-c3dc-4deb-5f47f1264c93','69d63f4e-88ef-58c9-0a28-5f469b0dc456','5c565cf0-d6e3-4173-5d2d-5c6e1d926139',90,'2020-08-28 09:56:53',0),('77be1425-085f-fba7-c752-5f47f1ab996d','824bc684-1b3b-2184-338e-5f47f0ca9e93','da15b921-e3da-b9da-7692-5c6e1d338231',0,'2020-08-27 17:44:57',1),('77e6fe28-417e-b0f7-cada-5f47f1b8bde2','69d63f4e-88ef-58c9-0a28-5f469b0dc456','57ad2510-4b44-9372-fe6f-5c6e1d08759e',90,'2020-08-28 09:56:53',0),('77e9eb50-8cd5-fab5-5bb8-5f47f1cf42d9','824bc684-1b3b-2184-338e-5f47f0ca9e93','e2be1f2e-bf7a-99e2-93a1-5c6e1d7b6bba',0,'2020-08-27 17:44:57',1),('7818b6e7-b080-bc4f-09ea-5f47f16ee784','824bc684-1b3b-2184-338e-5f47f0ca9e93','d84dbda5-0435-3268-4441-5c6e1dec53c2',0,'2020-08-27 17:44:57',1),('781c566e-0777-d7e2-0acb-5f47f1894fe4','69d63f4e-88ef-58c9-0a28-5f469b0dc456','5f7ff9db-59ef-9e32-9104-5c6e1d3ee0bf',90,'2020-08-28 09:56:53',0),('785d0283-977d-8ffb-994c-5f47f1cc56b6','824bc684-1b3b-2184-338e-5f47f0ca9e93','65807232-4fc2-aad5-52f8-5c6e1d60ad29',0,'2020-08-27 17:44:57',1),('786d0a79-2551-9bb4-2fb9-5f469ce115d4','3304ef15-3d54-d514-0f52-5f469cfc6b17','114e35ef-a176-57a6-cbe4-5c6e1dde9572',90,'2020-08-27 17:49:53',0),('789a7975-e57b-98a3-ecf1-5f47f1e507d0','824bc684-1b3b-2184-338e-5f47f0ca9e93','6c07fded-2f91-dcfd-0a5c-5c6e1d1e158f',0,'2020-08-27 17:44:57',1),('789b87c4-fff4-3107-8a10-5f47f1f7d662','69d63f4e-88ef-58c9-0a28-5f469b0dc456','5622f630-b2c6-dffe-a454-5c6e1db76a36',90,'2020-08-28 09:56:53',0),('78c4e94b-7d27-867a-cea1-5f47f1029afd','824bc684-1b3b-2184-338e-5f47f0ca9e93','6a6a210a-9b29-a92b-5117-5c6e1d3defe1',0,'2020-08-27 17:44:57',1),('78e0bebb-9402-b3d1-3574-5f47f12be006','69d63f4e-88ef-58c9-0a28-5f469b0dc456','6ce608cd-b55d-2703-a9c3-5c6e1d11ce48',89,'2020-08-28 09:56:53',0),('78f16ff7-84c8-31ed-f85b-5f47f15f9ee9','824bc684-1b3b-2184-338e-5f47f0ca9e93','6f3ef736-d1b8-d29a-31bd-5c6e1d45d1ce',0,'2020-08-27 17:44:57',1),('79216f78-f08b-63f0-9ea7-5f47f16e9b91','69d63f4e-88ef-58c9-0a28-5f469b0dc456','d4d800a3-cd2a-ee02-8c27-5c6e1d23e7fc',90,'2020-08-28 09:56:53',0),('793afb67-0c8c-bf43-e5d0-5f47f146645e','824bc684-1b3b-2184-338e-5f47f0ca9e93','6da34898-2906-0ca9-4d9f-5c6e1d4a8e11',0,'2020-08-27 17:44:57',1),('79667c1b-a375-150b-3703-5f47f1ac144f','824bc684-1b3b-2184-338e-5f47f0ca9e93','68c51dc8-b17b-d512-dec0-5c6e1dfbb5b2',0,'2020-08-27 17:44:57',1),('797b4663-a083-beb6-cca6-5f47f113f568','69d63f4e-88ef-58c9-0a28-5f469b0dc456','bb1e0579-51fc-26c1-c49f-5c6e1d30b0d2',90,'2020-08-28 09:56:53',0),('79912962-dc95-6bc7-6c18-5f47f1aa77b2','824bc684-1b3b-2184-338e-5f47f0ca9e93','70d8f01c-fd25-458c-37da-5c6e1de3ee6d',0,'2020-08-27 17:44:57',1),('79bbcbe5-27f6-53e8-1987-5f47f16dce95','69d63f4e-88ef-58c9-0a28-5f469b0dc456','10849107-d2b4-4ba1-4f7e-5c6e1dd0ec65',90,'2020-08-28 09:56:53',0),('79bc1f84-07f9-8681-e6f3-5f47f1babbdb','824bc684-1b3b-2184-338e-5f47f0ca9e93','672c3b21-fc37-cdc7-6c9b-5c6e1d56963c',0,'2020-08-27 17:44:57',1),('79e6a300-daae-9811-17fc-5f47f177d09b','824bc684-1b3b-2184-338e-5f47f0ca9e93','2bb8a788-d9b2-317e-ee27-5c6e1d99e1e0',0,'2020-08-27 17:44:57',1),('79fdaef8-5371-4c35-fa8c-5f47f19ff921','69d63f4e-88ef-58c9-0a28-5f469b0dc456','eea30100-cfbc-4f33-53e8-5c6e1d718073',90,'2020-08-28 09:56:53',0),('7a2a4ccb-9648-0b0d-2b56-5f47f11ac3b4','824bc684-1b3b-2184-338e-5f47f0ca9e93','35351234-40a5-72c4-4de3-5c6e1dacbb2e',0,'2020-08-27 17:44:57',1),('7a543d7c-ab10-a6a1-2f17-5f47f1674076','69d63f4e-88ef-58c9-0a28-5f469b0dc456','a1680948-76a8-e14b-1921-5c6e1ddc801a',90,'2020-08-28 09:56:53',0),('7a584248-94a6-456a-90fd-5f47f116c57e','824bc684-1b3b-2184-338e-5f47f0ca9e93','338337d8-861f-20ab-eb8a-5c6e1d0794c7',0,'2020-08-27 17:44:57',1),('7a81f178-4d38-3bc3-b9ac-5f47f19343e1','824bc684-1b3b-2184-338e-5f47f0ca9e93','3889a69a-c00b-8f99-585b-5c6e1d8b38cb',0,'2020-08-27 17:44:57',1),('7a91cd6f-6a1b-4c0a-6fe3-5f47f1821854','69d63f4e-88ef-58c9-0a28-5f469b0dc456','121aa5d9-ed75-7bb0-a8c8-5c6e1de8a86a',90,'2020-08-28 09:56:53',0),('7aacfd3b-0964-5a8c-7274-5f47f1798a57','824bc684-1b3b-2184-338e-5f47f0ca9e93','36e993eb-3592-761c-b5b0-5c6e1d8e0760',0,'2020-08-27 17:44:57',1),('7adb97be-95aa-3297-9b15-5f47f1824778','824bc684-1b3b-2184-338e-5f47f0ca9e93','31c44a2d-47fa-fb4f-0b01-5c6e1d201232',0,'2020-08-27 17:44:57',1),('7ae65574-e30d-2fa0-c223-5f47f1d32396','69d63f4e-88ef-58c9-0a28-5f469b0dc456','87360a24-2a17-a114-4da2-5c6e1dc660d3',90,'2020-08-28 09:56:53',0),('7b059647-7a01-ce82-98b6-5f47f10db5ce','824bc684-1b3b-2184-338e-5f47f0ca9e93','3a22e344-99f3-e402-1503-5c6e1daeca94',0,'2020-08-27 17:44:57',1),('7b3e7aa3-157f-e900-b0e3-5f47f101fcc0','69d63f4e-88ef-58c9-0a28-5f469b0dc456','7aac0b6d-7b37-a210-a863-5c6e1dfd72eb',89,'2020-08-28 09:56:53',0),('7b448c7c-2fc0-a572-17a5-5f47f1c90590','824bc684-1b3b-2184-338e-5f47f0ca9e93','2f3392ef-1a9a-f99d-c414-5c6e1d868dc9',0,'2020-08-27 17:44:57',1),('7b77b5b5-2705-529c-a266-5f47f16e3dd8','69d63f4e-88ef-58c9-0a28-5f469b0dc456','8160c332-1250-2295-9dc4-5c6e1da2bf97',90,'2020-08-28 09:56:53',0),('7b7a62e9-9ec7-8f9a-bc29-5f47f144459a','824bc684-1b3b-2184-338e-5f47f0ca9e93','68b60ce8-53e9-437d-7bbf-5c6e1d416598',0,'2020-08-27 17:44:57',1),('7b7e0008-d4b8-4e96-ac44-5f469c3c0e92','3304ef15-3d54-d514-0f52-5f469cfc6b17','bd05094c-c21b-c6bb-9649-5c6e1dbbf283',75,'2020-08-27 17:49:53',0),('7ba32c01-5dac-d5de-5a0c-5f47f14c8cfa','824bc684-1b3b-2184-338e-5f47f0ca9e93','d90400ee-40f8-2dd5-19a3-5c6e1dd03e8b',0,'2020-08-27 17:44:57',1),('7ba4ddee-447d-4095-ef36-5f47f175acae','69d63f4e-88ef-58c9-0a28-5f469b0dc456','7f9d27b7-f779-c812-a360-5c6e1d4959c4',90,'2020-08-28 09:56:53',0),('7bd0c869-808b-9d1a-5740-5f47f12c439b','824bc684-1b3b-2184-338e-5f47f0ca9e93','bdb006b2-d132-b086-d28e-5c6e1d4cdff2',0,'2020-08-27 17:44:57',1),('7be638c7-ffce-1d79-bccc-5f47f17289f3','69d63f4e-88ef-58c9-0a28-5f469b0dc456','850c5ce2-4f77-8308-e59f-5c6e1da9a8f0',90,'2020-08-28 09:56:53',0),('7bfa23ef-00e7-74cb-d1c5-5f47f19a0ede','824bc684-1b3b-2184-338e-5f47f0ca9e93','10f6b83e-e87a-61fb-d12a-5c6e1dffd363',0,'2020-08-27 17:44:57',1),('7c3b3e98-1922-5553-e72f-5f47f1da34ef','824bc684-1b3b-2184-338e-5f47f0ca9e93','f4460c0a-f75e-b471-0c7f-5c6e1d1459a1',0,'2020-08-27 17:44:57',1),('7c49a629-ccfb-18de-e9dd-5f47f14cf7c2','69d63f4e-88ef-58c9-0a28-5f469b0dc456','83497410-27b0-b41e-f49a-5c6e1df67a5f',90,'2020-08-28 09:56:53',0),('7c66b3a7-d4ba-2d08-8a88-5f47f138ec34','824bc684-1b3b-2184-338e-5f47f0ca9e93','a2490847-3b31-01e5-f954-5c6e1daf08eb',0,'2020-08-27 17:44:57',1),('7c78be7f-d611-d395-e29c-5f47f18c566c','69d63f4e-88ef-58c9-0a28-5f469b0dc456','7defbd7a-6478-d657-82e2-5c6e1ddffba3',90,'2020-08-28 09:56:53',0),('7c90be58-2895-0dde-3d40-5f47f124175f','824bc684-1b3b-2184-338e-5f47f0ca9e93','12a22378-ce15-c2f0-402b-5c6e1d224f96',0,'2020-08-27 17:44:57',1),('7ca8466f-91be-054a-3d4d-5f47f128e876','69d63f4e-88ef-58c9-0a28-5f469b0dc456','86d7fbc5-843a-f9a3-a2f3-5c6e1d8c8ac7',90,'2020-08-28 09:56:53',0),('7cbc2a4d-b1ee-19fd-9f96-5f47f19fa9b2','824bc684-1b3b-2184-338e-5f47f0ca9e93','86bc0feb-f287-8ee8-345a-5c6e1dbd09d3',0,'2020-08-27 17:44:57',1),('7ce17959-4889-9576-6473-5f47f1619862','69d63f4e-88ef-58c9-0a28-5f469b0dc456','7c4b47cf-121d-f430-d03f-5c6e1d0d9029',90,'2020-08-28 09:56:53',0),('7cece3ca-d833-12fc-6c3c-5f47f1db67b9','824bc684-1b3b-2184-338e-5f47f0ca9e93','453b3704-5824-5632-462a-5c6e1dce0981',0,'2020-08-27 17:44:57',1),('7d2a5a42-0afe-5095-d02c-5f47f1100a3c','824bc684-1b3b-2184-338e-5f47f0ca9e93','53dd2dcb-b198-1a9c-6c63-5c6e1d958589',0,'2020-08-27 17:44:57',1),('7d3a3adb-bff7-44a5-6f09-5f47f1b11e5c','69d63f4e-88ef-58c9-0a28-5f469b0dc456','d41751ce-361d-6116-ba6a-5c6e1d93e0c2',89,'2020-08-28 09:56:53',0),('7d59262f-744c-5a1e-5523-5f47f19eef32','824bc684-1b3b-2184-338e-5f47f0ca9e93','5204b668-b7ff-ccdd-8e4d-5c6e1d632d90',0,'2020-08-27 17:44:57',1),('7d774ce4-b21f-0b32-027d-5f47f16f5431','69d63f4e-88ef-58c9-0a28-5f469b0dc456','dba571ad-f302-219d-5f27-5c6e1dfa7f00',90,'2020-08-28 09:56:53',0),('7d86018b-72fc-1956-63f9-5f47f118df27','824bc684-1b3b-2184-338e-5f47f0ca9e93','5789c7f2-cad5-5b8c-4353-5c6e1d5345eb',0,'2020-08-27 17:44:57',1),('7db1b33a-6cf6-705c-b3a9-5f47f1493eba','69d63f4e-88ef-58c9-0a28-5f469b0dc456','d9e62f26-2a44-8731-467e-5c6e1de31273',90,'2020-08-28 09:56:53',0),('7db34d19-9356-9de7-c9ce-5f47f1aab0c1','824bc684-1b3b-2184-338e-5f47f0ca9e93','55b878d0-2944-bdc5-2d7a-5c6e1d195b81',0,'2020-08-27 17:44:57',1),('7de2e330-9915-eaba-84c7-5f47f11e89df','824bc684-1b3b-2184-338e-5f47f0ca9e93','502a38bf-fe4f-4c7c-33b2-5c6e1d7254d0',0,'2020-08-27 17:44:57',1),('7de930d7-4da6-66a9-3ce6-5f47f112485e','69d63f4e-88ef-58c9-0a28-5f469b0dc456','df22d385-fffb-07f6-4cad-5c6e1d05385c',90,'2020-08-28 09:56:53',0),('7e2ad326-f2c8-8501-da60-5f47f19bacfe','824bc684-1b3b-2184-338e-5f47f0ca9e93','5962e588-9521-bc29-9540-5c6e1d63b93c',0,'2020-08-27 17:44:57',1),('7e3b56db-9664-65d8-d2ea-5f47f1d584a7','69d63f4e-88ef-58c9-0a28-5f469b0dc456','dd53b539-625a-a770-e54b-5c6e1da3a732',90,'2020-08-28 09:56:53',0),('7e73d54a-be47-6a70-9901-5f47f1bcf608','69d63f4e-88ef-58c9-0a28-5f469b0dc456','d82a4ba7-7cba-70f5-d7a4-5c6e1ddf0b3b',90,'2020-08-28 09:56:53',0),('7e8e4a84-dcac-ad49-b52c-5f47f1876033','824bc684-1b3b-2184-338e-5f47f0ca9e93','4e23c1ff-9369-80c8-9296-5c6e1d9eda74',0,'2020-08-27 17:44:57',1),('7ea1e5f7-3bbc-60a2-94fd-5f47f160c317','69d63f4e-88ef-58c9-0a28-5f469b0dc456','e137a998-bde5-ba95-c4c5-5c6e1d6dd872',90,'2020-08-28 09:56:53',0),('7ebbb49a-0ef3-2aee-c84d-5f47f1fd81bd','824bc684-1b3b-2184-338e-5f47f0ca9e93','392b4520-bc4a-0b1f-059f-5c6e1d653b57',0,'2020-08-27 17:44:57',1),('7ed88f39-1715-1c2a-6773-5f47f192e634','69d63f4e-88ef-58c9-0a28-5f469b0dc456','d63d2d97-856c-40aa-a915-5c6e1d5908f3',90,'2020-08-28 09:56:53',0),('7ee9d28c-a5f8-0156-d2fe-5f47f194f447','824bc684-1b3b-2184-338e-5f47f0ca9e93','3fa48fbe-c3d1-67ba-7543-5c6e1d64f949',0,'2020-08-27 17:44:57',1),('7f3fd5f7-00a5-5da7-d55f-5f47f1022c52','69d63f4e-88ef-58c9-0a28-5f469b0dc456','f0e63510-538e-a5f1-735f-5c6e1dee3384',89,'2020-08-28 09:56:53',0),('7f6bec50-9b1a-f7a6-065c-5f47f1cd7d44','824bc684-1b3b-2184-338e-5f47f0ca9e93','3e0a8d82-3855-407e-0ac4-5c6e1d0cdf9f',0,'2020-08-27 17:44:57',1),('7f7107bc-cb7e-561e-4d6e-5f47f106fbd6','69d63f4e-88ef-58c9-0a28-5f469b0dc456','3ef5062d-c353-a08c-c667-5c6e1d0d53b7',90,'2020-08-28 09:56:53',0),('7f95c832-0d3a-bb93-e58e-5f47f1c96bc8','824bc684-1b3b-2184-338e-5f47f0ca9e93','42dbf3b7-2439-13c1-da55-5c6e1dd94e7d',0,'2020-08-27 17:44:57',1),('7fac3eeb-a582-dbd7-a217-5f47f1d06739','69d63f4e-88ef-58c9-0a28-5f469b0dc456','211e0ddd-c042-9ae3-e81f-5c6e1d5431d7',90,'2020-08-28 09:56:53',0),('7fc090ac-0a6e-eb90-68fb-5f47f10be556','824bc684-1b3b-2184-338e-5f47f0ca9e93','413eb557-390e-1809-3fee-5c6e1db396b6',0,'2020-08-27 17:44:57',1),('7fe84879-b7dc-3d32-52ec-5f47f1203987','69d63f4e-88ef-58c9-0a28-5f469b0dc456','79f90248-2bd9-8082-77c7-5c6e1d3272ae',90,'2020-08-28 09:56:53',0),('7ffedfa7-ece3-c0fc-d7ff-5f47f19605ff','824bc684-1b3b-2184-338e-5f47f0ca9e93','3c6faf0b-9351-4b5a-34f1-5c6e1d899517',0,'2020-08-27 17:44:57',1),('802db177-d004-2886-6ddf-5f47f1aa4e6c','824bc684-1b3b-2184-338e-5f47f0ca9e93','447e4027-4eb0-7267-7547-5c6e1dda96aa',0,'2020-08-27 17:44:57',1),('80407e37-9451-daf4-6695-5f47f157f039','69d63f4e-88ef-58c9-0a28-5f469b0dc456','5d500441-9b55-3adc-8e5a-5c6e1de5ab45',90,'2020-08-28 09:56:53',0),('804c0743-aaba-de5e-b3f7-5f469c431ceb','3304ef15-3d54-d514-0f52-5f469cfc6b17','14edf037-2af6-6859-18f0-5c6e1d9ae20a',90,'2020-08-27 17:49:53',0),('806198c6-d79d-013c-3fb6-5f47f1e8504d','824bc684-1b3b-2184-338e-5f47f0ca9e93','3acf69d8-9b59-caed-5eb9-5c6e1d95d81a',0,'2020-08-27 17:44:57',1),('806f9b28-9a64-feb1-e985-5f47f1a7031f','69d63f4e-88ef-58c9-0a28-5f469b0dc456','51900e3d-f193-7048-426c-5c6e1dfe209e',90,'2020-08-28 09:56:53',0),('808ed4fa-dcaa-bce7-6979-5f47f1175188','824bc684-1b3b-2184-338e-5f47f0ca9e93','a9dee868-a6e3-408e-7c68-5c6e1da2e377',0,'2020-08-27 17:44:57',1),('80a40520-aea6-be30-4918-5f47f1fa688b','69d63f4e-88ef-58c9-0a28-5f469b0dc456','950504b8-1278-d2c0-1d12-5c6e1ddfb235',90,'2020-08-28 09:56:53',0),('80ba108d-51fd-f1e0-eadb-5f47f15d6983','824bc684-1b3b-2184-338e-5f47f0ca9e93','b15af48d-52d9-ef94-0fee-5c6e1d9f03f8',0,'2020-08-27 17:44:57',1),('80eab3b5-cc49-4df2-92be-5f47f11fc467','69d63f4e-88ef-58c9-0a28-5f469b0dc456','f29b36cd-339b-ecb7-97b9-5c6e1d60039c',90,'2020-08-28 09:56:53',0),('8103676c-2d16-90e3-06ab-5f47f1d0504e','824bc684-1b3b-2184-338e-5f47f0ca9e93','afa20a90-5510-6d72-b11a-5c6e1d235b3e',0,'2020-08-27 17:44:57',1),('8145c527-cd68-5d01-64aa-5f47f1fe3279','69d63f4e-88ef-58c9-0a28-5f469b0dc456','30d76631-82e8-b7fe-ddfa-5c6e1dd9abc2',89,'2020-08-28 09:56:53',0),('81474ae9-6f34-72be-5374-5f47f12a8b2e','824bc684-1b3b-2184-338e-5f47f0ca9e93','b4dc8091-0f5f-c9bf-f80c-5c6e1d875df1',0,'2020-08-27 17:44:57',1),('8181b5c2-929f-c595-e7c9-5f47f1612f38','824bc684-1b3b-2184-338e-5f47f0ca9e93','b31c0fe7-ebd4-0419-2940-5c6e1d73fa54',0,'2020-08-27 17:44:57',1),('8189f401-0dfa-59e0-5df7-5f47f10794c7','69d63f4e-88ef-58c9-0a28-5f469b0dc456','388d4a70-5bce-7181-208f-5c6e1d88cade',90,'2020-08-28 09:56:53',0),('81b80b61-df92-53cc-b2dc-5f47f1bfd63c','69d63f4e-88ef-58c9-0a28-5f469b0dc456','36bc1d33-71d2-bb7a-1bb4-5c6e1dcc38c1',90,'2020-08-28 09:56:53',0),('81bb86eb-6f0e-99d8-4835-5f47f116bb07','824bc684-1b3b-2184-338e-5f47f0ca9e93','ade5ab40-975e-86e3-e4f9-5c6e1dc0c8a1',0,'2020-08-27 17:44:57',1),('8210f0c5-e442-a33f-04ab-5f47f1361ea1','69d63f4e-88ef-58c9-0a28-5f469b0dc456','3c5a66b1-961d-620d-103f-5c6e1d689c1c',90,'2020-08-28 09:56:53',0),('8240be34-820f-ae18-c4c3-5f47f1949874','69d63f4e-88ef-58c9-0a28-5f469b0dc456','3a77715d-b123-819f-19ae-5c6e1db13f47',90,'2020-08-28 09:56:53',0),('8267cf28-7ee8-16f0-bd6c-5f47f156ed64','824bc684-1b3b-2184-338e-5f47f0ca9e93','b69e3b35-c9fe-e7f3-8a3c-5c6e1d69236a',0,'2020-08-27 17:44:57',1),('8270ad87-7f09-c892-2f04-5f47f1e3f12b','69d63f4e-88ef-58c9-0a28-5f469b0dc456','34d441bb-0fe8-5906-d567-5c6e1dff5aef',90,'2020-08-28 09:56:53',0),('82a4b7fd-167b-672d-dfe3-5f47f1703d93','824bc684-1b3b-2184-338e-5f47f0ca9e93','ac00ed38-12f8-1e3e-18da-5c6e1d059af2',0,'2020-08-27 17:44:57',1),('82ab0e74-669a-758a-75b5-5f47f14a1fd2','69d63f4e-88ef-58c9-0a28-5f469b0dc456','3e4fc88a-0fb7-4a6e-935e-5c6e1da1cf38',90,'2020-08-28 09:56:53',0),('82e34c12-cd87-4fc3-03cc-5f47f155b3ae','69d63f4e-88ef-58c9-0a28-5f469b0dc456','32e6c274-6c87-2818-4db5-5c6e1dfe853d',90,'2020-08-28 09:56:53',0),('82e3abfa-87b9-dee7-e2c8-5f47f1fba389','824bc684-1b3b-2184-338e-5f47f0ca9e93','5d297527-1f3f-14f3-d10f-5c6e1d048547',0,'2020-08-27 17:44:57',1),('833c358e-7707-6c19-7b8f-5f47f15d8559','824bc684-1b3b-2184-338e-5f47f0ca9e93','640483b3-6ace-7d25-28bc-5c6e1d09e2f4',0,'2020-08-27 17:44:57',1),('8344a21a-a722-4148-4b5f-5f47f1518f8d','69d63f4e-88ef-58c9-0a28-5f469b0dc456','cf862ecd-de63-de1c-8ae3-5c6e1da03a50',89,'2020-08-28 09:56:53',0),('838c8fe8-c3c7-67fe-0d9c-5f47f190f588','69d63f4e-88ef-58c9-0a28-5f469b0dc456','d602ff9a-88a3-20a1-36f0-5c6e1ddae899',90,'2020-08-28 09:56:53',0),('83bd49f5-e1de-89aa-0378-5f47f161e2d7','69d63f4e-88ef-58c9-0a28-5f469b0dc456','d46dedb9-a982-67a2-d70a-5c6e1da04d41',90,'2020-08-28 09:56:53',0),('8402f3ce-e080-18ed-ac61-5f47f1c64ba4','69d63f4e-88ef-58c9-0a28-5f469b0dc456','d92f8b18-3f0e-d480-0183-5c6e1dea0580',90,'2020-08-28 09:56:53',0),('84352d49-30f0-781f-3cd0-5f47f1af5681','69d63f4e-88ef-58c9-0a28-5f469b0dc456','d79b79ce-5652-e699-c84a-5c6e1ddc8bc8',90,'2020-08-28 09:56:53',0),('8440017c-492d-c37c-d968-5f469c91e5a1','3304ef15-3d54-d514-0f52-5f469cfc6b17','e18fff67-dccb-b854-fb52-5c6e1d1c257e',75,'2020-08-27 17:49:53',0),('8462f070-50e9-dde1-efd2-5f47f1d169c2','69d63f4e-88ef-58c9-0a28-5f469b0dc456','d2d19ea3-bf2c-67b9-87a6-5c6e1d2d2561',90,'2020-08-28 09:56:53',0),('84907483-7263-c21b-48f0-5f47f130f7af','69d63f4e-88ef-58c9-0a28-5f469b0dc456','dacaaa84-1cf7-efaa-60c3-5c6e1d807a84',90,'2020-08-28 09:56:53',0),('84c0a82d-22fb-88cb-f7f3-5f47f1c8b235','69d63f4e-88ef-58c9-0a28-5f469b0dc456','d122f516-ba83-0d8e-741a-5c6e1d51b454',90,'2020-08-28 09:56:53',0),('84d201f4-f9ff-706d-084f-5f469c9d372d','3304ef15-3d54-d514-0f52-5f469cfc6b17','9e240243-5b8b-3fd9-ebdd-5c6e1d4e4fd7',75,'2020-08-27 17:49:53',0),('8507143b-6f4f-577b-b925-5f47f171f4f9','69d63f4e-88ef-58c9-0a28-5f469b0dc456','c8db86d1-7f26-b8aa-5323-5c6e1d4f870e',89,'2020-08-28 09:56:53',0),('8537f9cc-149c-7e6f-3b17-5f47f1338c10','69d63f4e-88ef-58c9-0a28-5f469b0dc456','d096a892-9823-074d-edd4-5c6e1dfe248d',90,'2020-08-28 09:56:53',0),('8565108a-c6a8-da8b-f4cf-5f47f1ea2fc8','69d63f4e-88ef-58c9-0a28-5f469b0dc456','ced98a67-37f4-6707-c5ac-5c6e1da221c6',90,'2020-08-28 09:56:53',0),('85934392-7959-e118-4971-5f47f1d71dde','69d63f4e-88ef-58c9-0a28-5f469b0dc456','d3f3c098-4d7c-d2c6-54b3-5c6e1dc0a477',90,'2020-08-28 09:56:53',0),('85a182fa-0f9c-2099-176b-5f47f1a44f79','824bc684-1b3b-2184-338e-5f47f0ca9e93','623518f3-d355-4c06-6dbd-5c6e1dc6f618',0,'2020-08-27 17:44:57',1),('85c18b30-b831-e5d9-2931-5f47f1130087','69d63f4e-88ef-58c9-0a28-5f469b0dc456','d2429bba-7f54-24aa-1491-5c6e1db06ab6',90,'2020-08-28 09:56:53',0),('85e77992-2817-0877-ffed-5f47f15b9534','824bc684-1b3b-2184-338e-5f47f0ca9e93','67729b81-c610-e9b5-6626-5c6e1d6a9701',0,'2020-08-27 17:44:57',1),('8607fe72-0bb2-6061-4aa0-5f47f18d470d','69d63f4e-88ef-58c9-0a28-5f469b0dc456','cd02e82c-7eb8-8630-015d-5c6e1d54dd16',90,'2020-08-28 09:56:53',0),('86143d3f-eb0c-ec36-5c67-5f47f1bf0a4d','824bc684-1b3b-2184-338e-5f47f0ca9e93','65c01214-075a-23ad-1a49-5c6e1d0c7711',0,'2020-08-27 17:44:57',1),('86358c6a-9aa0-b635-a084-5f47f18df7c3','69d63f4e-88ef-58c9-0a28-5f469b0dc456','d5b4803d-53f2-521a-2ee9-5c6e1d8e681b',90,'2020-08-28 09:56:53',0),('864138de-7c77-3ddb-1ab5-5f47f16ed128','824bc684-1b3b-2184-338e-5f47f0ca9e93','6080d88e-9d8f-594a-949c-5c6e1de8ce23',0,'2020-08-27 17:44:57',1),('8661f166-285e-0711-5ac0-5f47f1e1e7bb','69d63f4e-88ef-58c9-0a28-5f469b0dc456','cb14dd05-873d-715e-92be-5c6e1d21e982',90,'2020-08-28 09:56:53',0),('866e5fd4-c013-741b-e95f-5f47f16b723b','824bc684-1b3b-2184-338e-5f47f0ca9e93','6929e76d-97ed-b6c4-d138-5c6e1d2fa293',0,'2020-08-27 17:44:57',1),('868f6917-1e33-3aed-6408-5f47f184f6a2','69d63f4e-88ef-58c9-0a28-5f469b0dc456','a18e13c0-7b00-9e5b-72c3-5c6e1d886c8c',89,'2020-08-28 09:56:53',0),('8698b453-d623-c568-fb93-5f47f1760575','824bc684-1b3b-2184-338e-5f47f0ca9e93','5ed3bcca-6841-1c42-dec8-5c6e1dcd1654',0,'2020-08-27 17:44:57',1),('86c97a13-1cdd-69ce-c15f-5f47f1ce4833','69d63f4e-88ef-58c9-0a28-5f469b0dc456','a8f890a5-c581-7a99-4005-5c6e1d7b2ffd',90,'2020-08-28 09:56:53',0),('86dd146b-a27e-0f68-4bf0-5f47f1bffa56','824bc684-1b3b-2184-338e-5f47f0ca9e93','4abd9300-75b3-c2d2-685e-5c6e1df785c7',0,'2020-08-27 17:44:57',1),('87098a36-8f77-2f59-9ff5-5f47f1cf5083','69d63f4e-88ef-58c9-0a28-5f469b0dc456','a711af50-e0eb-81dd-aec0-5c6e1d1b2ddd',90,'2020-08-28 09:56:53',0),('87174d75-28a9-6f68-ad4f-5f47f1ce44ee','824bc684-1b3b-2184-338e-5f47f0ca9e93','51468e5a-b69b-bd8d-77b6-5c6e1d52286e',0,'2020-08-27 17:44:57',1),('87366b1d-0618-f64f-a350-5f47f1412faa','69d63f4e-88ef-58c9-0a28-5f469b0dc456','ad0f4fee-d33e-2f3c-9f82-5c6e1d1e9a69',90,'2020-08-28 09:56:53',0),('8746b066-fbab-6a8d-7bf0-5f47f1a9e8d2','824bc684-1b3b-2184-338e-5f47f0ca9e93','4fa28488-be59-b43c-3a3f-5c6e1de2faca',0,'2020-08-27 17:44:57',1),('8762ad97-074d-3454-65bb-5f47f1b3a25a','69d63f4e-88ef-58c9-0a28-5f469b0dc456','ab2242ca-d4fd-9549-e791-5c6e1d385890',90,'2020-08-28 09:56:53',0),('87705a9c-f29e-4891-9340-5f47f187d4dc','824bc684-1b3b-2184-338e-5f47f0ca9e93','54928ee5-4276-ffbe-fdb1-5c6e1dc20efa',0,'2020-08-27 17:44:57',1),('878d56b2-be87-43a8-bc74-5f47f156cebc','69d63f4e-88ef-58c9-0a28-5f469b0dc456','a51ad6e5-d047-65d1-bc96-5c6e1d9b1f5c',90,'2020-08-28 09:56:53',0),('8799c49b-a35d-d041-9df0-5f47f1b17873','824bc684-1b3b-2184-338e-5f47f0ca9e93','52e9af88-fc3a-2da4-8afd-5c6e1d7802ed',0,'2020-08-27 17:44:57',1),('87ba15b4-5a27-3e15-7262-5f47f13800f8','69d63f4e-88ef-58c9-0a28-5f469b0dc456','af3813f0-504b-4051-224b-5c6e1dfd4d58',90,'2020-08-28 09:56:53',0),('87fe7b8c-239c-36fb-5f18-5f47f1b30289','69d63f4e-88ef-58c9-0a28-5f469b0dc456','a34e33e7-f029-be12-4f60-5c6e1d1ee705',90,'2020-08-28 09:56:53',0),('880e6095-5b2f-17cb-ffc7-5f47f1f8b83b','824bc684-1b3b-2184-338e-5f47f0ca9e93','4dff9ed3-fe53-4b2e-38fc-5c6e1d318481',0,'2020-08-27 17:44:57',1),('882d2015-ad70-f214-1792-5f47f12c130f','69d63f4e-88ef-58c9-0a28-5f469b0dc456','b62a2bfc-2615-5abd-0a60-5c6e1d86946a',89,'2020-08-28 09:56:53',0),('883cf1e3-d382-2d66-6039-5f47f177b930','824bc684-1b3b-2184-338e-5f47f0ca9e93','5638e0fd-5245-0cd9-ce3a-5c6e1de3594b',0,'2020-08-27 17:44:57',1),('88560a4e-c389-9467-2951-5f469c1a40e9','3304ef15-3d54-d514-0f52-5f469cfc6b17','e941f915-63c6-f813-7912-5c6e1d687657',89,'2020-08-27 17:49:53',0),('8859bb4b-e927-333c-8a61-5f47f1687f97','69d63f4e-88ef-58c9-0a28-5f469b0dc456','bd16a03b-186c-145a-c165-5c6e1d2bb8aa',90,'2020-08-28 09:56:53',0),('886712e3-1e2f-5862-203c-5f47f16016f5','824bc684-1b3b-2184-338e-5f47f0ca9e93','4c5e89e7-59ec-82d0-2835-5c6e1d1e856d',0,'2020-08-27 17:44:57',1),('889620c6-7652-d3a7-b8ee-5f47f1259888','824bc684-1b3b-2184-338e-5f47f0ca9e93','184cafd9-5da5-233b-61b2-5c6e1dcc1ae2',0,'2020-08-27 17:44:57',1),('88d22a2d-b1b7-1683-41dd-5f47f1fa53ef','69d63f4e-88ef-58c9-0a28-5f469b0dc456','bb63e112-5c92-af82-2ed4-5c6e1d722c85',90,'2020-08-28 09:56:53',0),('88d81304-7d7d-6ba0-8f4d-5f47f145e971','824bc684-1b3b-2184-338e-5f47f0ca9e93','1eca7007-450c-966f-c0f1-5c6e1d074aee',0,'2020-08-27 17:44:57',1),('88fcfc7c-20b7-fbc7-a15c-5f47f1d0d703','69d63f4e-88ef-58c9-0a28-5f469b0dc456','c06bc7c3-f4ae-9709-ad32-5c6e1df9919b',90,'2020-08-28 09:56:53',0),('8903126e-8eaf-f4cc-06a9-5f47f188878e','824bc684-1b3b-2184-338e-5f47f0ca9e93','1d2877da-72f4-f0e6-a6bc-5c6e1da8da72',0,'2020-08-27 17:44:57',1),('892825b7-9674-5046-9a34-5f47f11e30ff','69d63f4e-88ef-58c9-0a28-5f469b0dc456','bebe1a01-5b96-efc4-c366-5c6e1dfc050d',90,'2020-08-28 09:56:53',0),('89304471-29eb-b3ed-a125-5f47f1c9e26f','824bc684-1b3b-2184-338e-5f47f0ca9e93','22996860-ab57-6633-73f6-5c6e1d5dcb18',0,'2020-08-27 17:44:57',1),('8954ed5a-e06b-3b0e-4b14-5f47f171d4f5','69d63f4e-88ef-58c9-0a28-5f469b0dc456','b99c202c-a3f2-0e4c-117c-5c6e1d334507',90,'2020-08-28 09:56:53',0),('8959cbc0-c621-1c33-74d0-5f47f178de14','824bc684-1b3b-2184-338e-5f47f0ca9e93','206ab75e-6ea7-cf2a-57ea-5c6e1d9ebd40',0,'2020-08-27 17:44:57',1),('89830de6-c434-dea3-0692-5f47f10ede85','69d63f4e-88ef-58c9-0a28-5f469b0dc456','c216ae81-667d-57c6-1b66-5c6e1d66fedc',90,'2020-08-28 09:56:53',0),('89893be1-3212-bcc7-cbde-5f47f142abe3','824bc684-1b3b-2184-338e-5f47f0ca9e93','1b7cf5f1-f8ac-d35a-0532-5c6e1d6bd505',0,'2020-08-27 17:44:57',1),('89af188a-2fb0-cfce-d836-5f47f1ae70ec','69d63f4e-88ef-58c9-0a28-5f469b0dc456','b7d811b6-4b23-7df0-5897-5c6e1d72ffd6',90,'2020-08-28 09:56:53',0),('89cf6d6a-a555-d343-1b9a-5f47f197b1ce','824bc684-1b3b-2184-338e-5f47f0ca9e93','245a684c-7f66-e44a-a4c6-5c6e1dfe0a22',0,'2020-08-27 17:44:57',1),('89f10935-dbef-92d4-bf69-5f47f1b246a3','69d63f4e-88ef-58c9-0a28-5f469b0dc456','7a0c717c-743b-9dff-0e33-5c6e1d5c14d4',89,'2020-08-28 09:56:53',0),('89fafcbc-9f0c-c6d8-8bb6-5f47f169641f','824bc684-1b3b-2184-338e-5f47f0ca9e93','19e5dfc5-63d8-d00f-4a5c-5c6e1de57f20',0,'2020-08-27 17:44:57',1),('8a24eb2e-3c52-042c-a069-5f47f14665d4','824bc684-1b3b-2184-338e-5f47f0ca9e93','652163fe-3269-b1f4-9e3f-5c6e1d37dbcf',0,'2020-08-27 17:44:57',1),('8a286189-497b-a86e-a0d4-5f47f1899f62','69d63f4e-88ef-58c9-0a28-5f469b0dc456','80f9ae9e-3727-299f-afd5-5c6e1d0fa123',90,'2020-08-28 09:56:53',0),('8a58aabd-c24f-83f7-6ba6-5f47f100a5ad','69d63f4e-88ef-58c9-0a28-5f469b0dc456','7f33c032-982b-f7e2-c3de-5c6e1d0aa89d',90,'2020-08-28 09:56:53',0),('8a5f807a-2ab0-8e2d-9d87-5f47f15e183e','824bc684-1b3b-2184-338e-5f47f0ca9e93','6b7c675b-97bd-5d96-6453-5c6e1d4f434a',0,'2020-08-27 17:44:57',1),('8a879253-a333-197d-2cf1-5f47f16eff5d','69d63f4e-88ef-58c9-0a28-5f469b0dc456','845ceb37-c664-d7bb-14d7-5c6e1df0b770',90,'2020-08-28 09:56:53',0),('8a8bd1d8-0dfb-1cab-a97c-5f47f12bfd33','824bc684-1b3b-2184-338e-5f47f0ca9e93','69ea34b6-878a-0c7f-7878-5c6e1d97c3e4',0,'2020-08-27 17:44:57',1),('8ac79173-c573-d58f-7cfc-5f47f178c7ca','69d63f4e-88ef-58c9-0a28-5f469b0dc456','82a9c474-0dc1-c281-491e-5c6e1d7b27b4',90,'2020-08-28 09:56:53',0),('8ad21462-bf00-612c-6220-5f47f1a1e176','824bc684-1b3b-2184-338e-5f47f0ca9e93','6ecb3a28-bd02-04f4-879d-5c6e1dccaeb9',0,'2020-08-27 17:44:57',1),('8af2fbc0-c1eb-380c-f4b5-5f47f18c1f6c','69d63f4e-88ef-58c9-0a28-5f469b0dc456','7d797f14-23ec-eae0-8e4d-5c6e1dd6311f',90,'2020-08-28 09:56:53',0),('8b0166ee-51ad-63dc-d509-5f47f1691aca','824bc684-1b3b-2184-338e-5f47f0ca9e93','6d2d4783-4ec1-d9fc-0c70-5c6e1d1755c6',0,'2020-08-27 17:44:57',1),('8b1e8660-f7f9-80ed-1f78-5f47f1e3cf51','69d63f4e-88ef-58c9-0a28-5f469b0dc456','860c4f68-86d7-ef63-880b-5c6e1df3f6e6',90,'2020-08-28 09:56:53',0),('8b2f2962-d028-779c-a085-5f47f1861112','824bc684-1b3b-2184-338e-5f47f0ca9e93','68556931-3a69-e3fa-3a95-5c6e1d28b747',0,'2020-08-27 17:44:57',1),('8b4a556d-2917-4f85-91a7-5f47f1e6c9ab','69d63f4e-88ef-58c9-0a28-5f469b0dc456','7bcaac3e-0c7f-302d-e870-5c6e1dfb18f0',90,'2020-08-28 09:56:53',0),('8b63329a-d835-bf74-430b-5f47f1d432c4','824bc684-1b3b-2184-338e-5f47f0ca9e93','705b169d-be44-c4b1-1ded-5c6e1debbb34',0,'2020-08-27 17:44:57',1),('8b76ca19-ccb7-0059-f995-5f47f1f53965','69d63f4e-88ef-58c9-0a28-5f469b0dc456','8d5f0375-04e8-9374-548c-5c6e1d4185cb',89,'2020-08-28 09:56:53',0),('8b8c04a3-d42e-a5e9-e123-5f469cf27745','3304ef15-3d54-d514-0f52-5f469cfc6b17','f038848b-d6ea-eea1-82c9-5c6e1d8050b7',-99,'2020-08-27 17:49:53',0),('8b95b5b9-0266-ad74-afda-5f47f1f69ea4','824bc684-1b3b-2184-338e-5f47f0ca9e93','66c644d2-69ae-d214-fe80-5c6e1d9aafb1',0,'2020-08-27 17:44:57',1),('8bc001ed-2001-1b0a-cbce-5f47f1fd3fa2','69d63f4e-88ef-58c9-0a28-5f469b0dc456','94cd0212-7227-a254-b111-5c6e1d48d71c',90,'2020-08-28 09:56:53',0),('8be0eec1-37fe-aaad-db9e-5f47f134006b','824bc684-1b3b-2184-338e-5f47f0ca9e93','5470bd94-dff3-8a95-720a-5c6e1dbd7da5',0,'2020-08-27 17:44:57',1),('8bef80c3-3d1f-5b18-7187-5f47f1771b49','69d63f4e-88ef-58c9-0a28-5f469b0dc456','92f3f737-caa2-dd8a-8a13-5c6e1d4916b9',90,'2020-08-28 09:56:53',0),('8c0ff6e1-fa66-eab2-7f96-5f47f1b7eb2f','824bc684-1b3b-2184-338e-5f47f0ca9e93','5acb056c-34b1-a7a7-b1f6-5c6e1dee52c4',0,'2020-08-27 17:44:57',1),('8c1d9bd3-6894-1e1d-6da8-5f47f11ac88d','69d63f4e-88ef-58c9-0a28-5f469b0dc456','9867e242-ed91-e741-d623-5c6e1d746db4',90,'2020-08-28 09:56:53',0),('8c466800-927c-149d-8d05-5f47f1441a28','69d63f4e-88ef-58c9-0a28-5f469b0dc456','96a17d4d-5ab9-2ed8-c2a4-5c6e1d987a1c',90,'2020-08-28 09:56:53',0),('8c72a076-20a8-e141-a279-5f47f1580211','69d63f4e-88ef-58c9-0a28-5f469b0dc456','9114c47d-c20b-a4c8-c2a6-5c6e1da9acb1',90,'2020-08-28 09:56:53',0),('8c87e495-b48b-129b-d650-5f47f12b2a88','824bc684-1b3b-2184-338e-5f47f0ca9e93','593a5e01-2184-6ac9-0aef-5c6e1d7818e2',0,'2020-08-27 17:44:57',1),('8cb57da3-15ce-1d5c-b700-5f47f15cce6d','69d63f4e-88ef-58c9-0a28-5f469b0dc456','9a32ca04-50c7-66b7-3185-5c6e1d384a7b',90,'2020-08-28 09:56:53',0),('8cd6ce81-75b8-2233-255c-5f47f1c329a1','824bc684-1b3b-2184-338e-5f47f0ca9e93','5de87979-2cf3-acb0-b065-5c6e1dfdc2c3',0,'2020-08-27 17:44:57',1),('8ce471cc-6444-20c7-4dac-5f47f173653d','69d63f4e-88ef-58c9-0a28-5f469b0dc456','8f3ad6ec-99d2-f98e-7785-5c6e1db67802',90,'2020-08-28 09:56:53',0),('8d07ab54-7cf9-90ff-dec5-5f47f1ea78cb','824bc684-1b3b-2184-338e-5f47f0ca9e93','5c565cf0-d6e3-4173-5d2d-5c6e1d926139',0,'2020-08-27 17:44:57',1),('8d13aadd-bb16-3e64-2998-5f47f1c4590d','69d63f4e-88ef-58c9-0a28-5f469b0dc456','9d5add96-09e7-13e4-5533-5c6e1d5eabe4',89,'2020-08-28 09:56:53',0),('8d39aad4-c524-2f9d-4433-5f47f18a8111','824bc684-1b3b-2184-338e-5f47f0ca9e93','57ad2510-4b44-9372-fe6f-5c6e1d08759e',0,'2020-08-27 17:44:57',1),('8d3e861f-c3b6-4b63-87fe-5f47f15cd564','69d63f4e-88ef-58c9-0a28-5f469b0dc456','a3b6eade-aa75-fdfb-bb22-5c6e1d132305',90,'2020-08-28 09:56:53',0),('8d789807-4bc2-3a1d-2fff-5f47f105a026','824bc684-1b3b-2184-338e-5f47f0ca9e93','5f7ff9db-59ef-9e32-9104-5c6e1d3ee0bf',0,'2020-08-27 17:44:57',1),('8d7b80ae-75eb-74c0-7072-5f47f167c390','69d63f4e-88ef-58c9-0a28-5f469b0dc456','a221846d-edb3-d08b-955d-5c6e1de207c6',90,'2020-08-28 09:56:53',0),('8dbb7fc8-36ca-0570-1a17-5f47f1f3994a','69d63f4e-88ef-58c9-0a28-5f469b0dc456','a6eaafe1-3a6e-8a53-b119-5c6e1db76720',90,'2020-08-28 09:56:53',0),('8dc1e84c-fe80-df79-e3a2-5f47f1ccbda8','824bc684-1b3b-2184-338e-5f47f0ca9e93','5622f630-b2c6-dffe-a454-5c6e1db76a36',0,'2020-08-27 17:44:57',1),('8df12372-fa67-9ddd-460f-5f47f157353f','824bc684-1b3b-2184-338e-5f47f0ca9e93','6ce608cd-b55d-2703-a9c3-5c6e1d11ce48',0,'2020-08-27 17:44:57',1),('8e1d8567-0857-90d9-eced-5f47f14e6cf9','824bc684-1b3b-2184-338e-5f47f0ca9e93','d4d800a3-cd2a-ee02-8c27-5c6e1d23e7fc',0,'2020-08-27 17:44:57',1),('8e20dae6-8f5d-e137-c116-5f47f1d317be','69d63f4e-88ef-58c9-0a28-5f469b0dc456','a5507fba-e80d-d18b-7347-5c6e1d7ca48d',90,'2020-08-28 09:56:53',0),('8e4ab110-8fd6-f1d4-e373-5f47f1cc4208','824bc684-1b3b-2184-338e-5f47f0ca9e93','bb1e0579-51fc-26c1-c49f-5c6e1d30b0d2',0,'2020-08-27 17:44:57',1),('8e4c444d-1bd6-27f0-0f80-5f47f11187a6','69d63f4e-88ef-58c9-0a28-5f469b0dc456','a08e9c0b-d79a-1e4c-baed-5c6e1dd818bb',90,'2020-08-28 09:56:53',0),('8e76b799-86f3-f55a-ff0b-5f47f11a0867','824bc684-1b3b-2184-338e-5f47f0ca9e93','10849107-d2b4-4ba1-4f7e-5c6e1dd0ec65',0,'2020-08-27 17:44:57',1),('8e795959-bd6f-0118-b748-5f47f1e87743','69d63f4e-88ef-58c9-0a28-5f469b0dc456','a897f543-7d27-85ff-87fa-5c6e1dffec0f',90,'2020-08-28 09:56:53',0),('8ebffcc2-cf10-f314-c5a4-5f47f1ce7eda','824bc684-1b3b-2184-338e-5f47f0ca9e93','eea30100-cfbc-4f33-53e8-5c6e1d718073',0,'2020-08-27 17:44:57',1),('8ec20083-f137-4c74-c3c8-5f47f1558d61','69d63f4e-88ef-58c9-0a28-5f469b0dc456','9ef99bea-c305-cfbf-a4ba-5c6e1dc6b98d',90,'2020-08-28 09:56:53',0),('8eebc849-8e83-4b82-4113-5f47f160163f','824bc684-1b3b-2184-338e-5f47f0ca9e93','a1680948-76a8-e14b-1921-5c6e1ddc801a',0,'2020-08-27 17:44:57',1),('8ef1e749-03fc-8d6a-a199-5f47f10ef0a0','69d63f4e-88ef-58c9-0a28-5f469b0dc456','8c28ae8a-3b44-22b0-8c48-5c6e1db709ed',89,'2020-08-28 09:56:53',0),('8f16b42f-b24c-9cbd-f841-5f47f1a33500','824bc684-1b3b-2184-338e-5f47f0ca9e93','121aa5d9-ed75-7bb0-a8c8-5c6e1de8a86a',0,'2020-08-27 17:44:57',1),('8f407be6-b4e9-d8aa-5c18-5f47f1e5782b','824bc684-1b3b-2184-338e-5f47f0ca9e93','87360a24-2a17-a114-4da2-5c6e1dc660d3',0,'2020-08-27 17:44:57',1),('8f5a138b-959a-c01e-64ad-5f47f1ce2118','69d63f4e-88ef-58c9-0a28-5f469b0dc456','929da18d-66a1-fac9-8cbf-5c6e1d22072a',90,'2020-08-28 09:56:53',0),('8f6d3781-7cc8-deb5-7dd7-5f47f1f3bffb','824bc684-1b3b-2184-338e-5f47f0ca9e93','7aac0b6d-7b37-a210-a863-5c6e1dfd72eb',0,'2020-08-27 17:44:57',1),('8f94bd1d-7b13-e4cd-52ab-5f47f1a50547','69d63f4e-88ef-58c9-0a28-5f469b0dc456','90fd3e60-d40b-fcd4-fb55-5c6e1dae8fe0',90,'2020-08-28 09:56:53',0),('8fbbd0b6-6e1f-76c6-71f4-5f47f16fbec3','824bc684-1b3b-2184-338e-5f47f0ca9e93','8160c332-1250-2295-9dc4-5c6e1da2bf97',0,'2020-08-27 17:44:57',1),('8fdcecba-0123-e3e1-1002-5f47f14f8267','69d63f4e-88ef-58c9-0a28-5f469b0dc456','95ce0dd9-8872-02a1-0db3-5c6e1d9bcf73',90,'2020-08-28 09:56:53',0),('8fea3cc4-44e9-df00-050f-5f47f1dd5436','824bc684-1b3b-2184-338e-5f47f0ca9e93','7f9d27b7-f779-c812-a360-5c6e1d4959c4',0,'2020-08-27 17:44:57',1),('9006bbde-2e3e-bb19-da25-5f47f1fafd2e','69d63f4e-88ef-58c9-0a28-5f469b0dc456','94383006-21ad-710b-5dc1-5c6e1dfd3bb5',90,'2020-08-28 09:56:53',0),('90178366-5092-b92d-a842-5f47f1a2f3f8','824bc684-1b3b-2184-338e-5f47f0ca9e93','850c5ce2-4f77-8308-e59f-5c6e1da9a8f0',0,'2020-08-27 17:44:57',1),('90336b8e-a0f8-8ac1-ce8a-5f47f129f6df','69d63f4e-88ef-58c9-0a28-5f469b0dc456','8f5dfb14-8ef8-cee3-ccc8-5c6e1da591cc',90,'2020-08-28 09:56:53',0),('9043c0a4-762a-aac2-a983-5f47f1296518','824bc684-1b3b-2184-338e-5f47f0ca9e93','83497410-27b0-b41e-f49a-5c6e1df67a5f',0,'2020-08-27 17:44:57',1),('905a0a94-2edb-2fa0-6802-5f469c207d20','3304ef15-3d54-d514-0f52-5f469cfc6b17','ee843bb5-3944-a90b-1821-5c6e1dd16399',75,'2020-08-27 17:49:53',0),('90654bd6-df4c-c10e-6a35-5f47f1c49750','69d63f4e-88ef-58c9-0a28-5f469b0dc456','97680cfd-ea91-f40b-322a-5c6e1d972dd4',90,'2020-08-28 09:56:53',0),('9074e9b7-42b5-9c41-1bf7-5f47f1cd11b2','824bc684-1b3b-2184-338e-5f47f0ca9e93','7defbd7a-6478-d657-82e2-5c6e1ddffba3',0,'2020-08-27 17:44:57',1),('90b928a5-1d16-2c17-5d51-5f47f1b1b0db','69d63f4e-88ef-58c9-0a28-5f469b0dc456','8dc2b087-f5ec-cf51-7690-5c6e1d369edb',90,'2020-08-28 09:56:53',0),('90bdcbfa-f45f-3e0f-0fd5-5f47f117e47f','824bc684-1b3b-2184-338e-5f47f0ca9e93','86d7fbc5-843a-f9a3-a2f3-5c6e1d8c8ac7',0,'2020-08-27 17:44:57',1),('90fc09e0-0bc2-95bc-3cf9-5f47f17390b2','824bc684-1b3b-2184-338e-5f47f0ca9e93','7c4b47cf-121d-f430-d03f-5c6e1d0d9029',0,'2020-08-27 17:44:57',1),('9109eb61-bc37-3f4a-4086-5f47f1b43b3a','69d63f4e-88ef-58c9-0a28-5f469b0dc456','76c626c4-b247-8bde-6d8b-5c6e1d204ff2',89,'2020-08-28 09:56:53',0),('9128c73d-1c76-7af4-879b-5f47f1db16e5','824bc684-1b3b-2184-338e-5f47f0ca9e93','d41751ce-361d-6116-ba6a-5c6e1d93e0c2',0,'2020-08-27 17:44:57',1),('91483b7f-3a0e-3bd1-adff-5f47f1b75f7e','69d63f4e-88ef-58c9-0a28-5f469b0dc456','7d74c28a-6fd6-3816-ea14-5c6e1d65a241',90,'2020-08-28 09:56:53',0),('9159eeb3-9020-7892-b1ef-5f47f19ee36d','824bc684-1b3b-2184-338e-5f47f0ca9e93','dba571ad-f302-219d-5f27-5c6e1dfa7f00',0,'2020-08-27 17:44:57',1),('917d5f9e-057f-8db6-f2ed-5f47f199f37d','69d63f4e-88ef-58c9-0a28-5f469b0dc456','7bd8bdff-cee3-fb8f-84c6-5c6e1dcd0834',90,'2020-08-28 09:56:53',0),('919ce35b-e8ee-1e0f-1aa9-5f47f12f06e3','824bc684-1b3b-2184-338e-5f47f0ca9e93','d9e62f26-2a44-8731-467e-5c6e1de31273',0,'2020-08-27 17:44:57',1),('91cc7279-15b2-b371-f05f-5f47f1bcc284','824bc684-1b3b-2184-338e-5f47f0ca9e93','df22d385-fffb-07f6-4cad-5c6e1d05385c',0,'2020-08-27 17:44:57',1),('92054778-b6f5-0ca2-6ad9-5f47f13d0dbd','824bc684-1b3b-2184-338e-5f47f0ca9e93','dd53b539-625a-a770-e54b-5c6e1da3a732',0,'2020-08-27 17:44:57',1),('9232106e-7365-986d-89e4-5f47f116230a','824bc684-1b3b-2184-338e-5f47f0ca9e93','d82a4ba7-7cba-70f5-d7a4-5c6e1ddf0b3b',0,'2020-08-27 17:44:57',1),('925fecd4-e0d4-2b13-39d5-5f47f1bbbad8','824bc684-1b3b-2184-338e-5f47f0ca9e93','e137a998-bde5-ba95-c4c5-5c6e1d6dd872',0,'2020-08-27 17:44:57',1),('92addfcf-2814-c0c2-524c-5f47f1125e49','824bc684-1b3b-2184-338e-5f47f0ca9e93','d63d2d97-856c-40aa-a915-5c6e1d5908f3',0,'2020-08-27 17:44:57',1),('92e0a320-e3bb-9da3-7eab-5f47f196b387','824bc684-1b3b-2184-338e-5f47f0ca9e93','f0e63510-538e-a5f1-735f-5c6e1dee3384',0,'2020-08-27 17:44:57',1),('9313de0b-a40b-ecf9-7b39-5f47f1ac5e14','824bc684-1b3b-2184-338e-5f47f0ca9e93','3ef5062d-c353-a08c-c667-5c6e1d0d53b7',0,'2020-08-27 17:44:57',1),('934a9edc-ae43-de01-2b4c-5f47f15f8c47','824bc684-1b3b-2184-338e-5f47f0ca9e93','211e0ddd-c042-9ae3-e81f-5c6e1d5431d7',0,'2020-08-27 17:44:57',1),('93950b20-487a-6e7a-1767-5f469c511475','3304ef15-3d54-d514-0f52-5f469cfc6b17','f3ed8a4b-4202-a38e-95ab-5c6e1de1eb7b',75,'2020-08-27 17:49:53',0),('939807b6-d754-514b-908c-5f47f187a719','824bc684-1b3b-2184-338e-5f47f0ca9e93','79f90248-2bd9-8082-77c7-5c6e1d3272ae',0,'2020-08-27 17:44:57',1),('93ca53c9-a441-2bca-0a2f-5f47f13bff78','824bc684-1b3b-2184-338e-5f47f0ca9e93','5d500441-9b55-3adc-8e5a-5c6e1de5ab45',0,'2020-08-27 17:44:57',1),('93fca791-95c8-ab8c-c823-5f47f1b18459','824bc684-1b3b-2184-338e-5f47f0ca9e93','51900e3d-f193-7048-426c-5c6e1dfe209e',0,'2020-08-27 17:44:57',1),('942813b9-7670-f15d-a914-5f47f10579c2','824bc684-1b3b-2184-338e-5f47f0ca9e93','950504b8-1278-d2c0-1d12-5c6e1ddfb235',0,'2020-08-27 17:44:57',1),('945955c9-428f-5d12-716a-5f47f1cbea8a','824bc684-1b3b-2184-338e-5f47f0ca9e93','f29b36cd-339b-ecb7-97b9-5c6e1d60039c',0,'2020-08-27 17:44:57',1),('94a87135-6eb0-7952-6c79-5f47f194d851','824bc684-1b3b-2184-338e-5f47f0ca9e93','30d76631-82e8-b7fe-ddfa-5c6e1dd9abc2',0,'2020-08-27 17:44:57',1),('94d7b5be-9377-9b6e-c0e9-5f47f1371c62','824bc684-1b3b-2184-338e-5f47f0ca9e93','388d4a70-5bce-7181-208f-5c6e1d88cade',0,'2020-08-27 17:44:57',1),('9505fafd-8c84-fcfd-4d6d-5f47f1f9b3ae','824bc684-1b3b-2184-338e-5f47f0ca9e93','36bc1d33-71d2-bb7a-1bb4-5c6e1dcc38c1',0,'2020-08-27 17:44:57',1),('95333e5c-f125-6e52-a5ad-5f47f1c064d8','824bc684-1b3b-2184-338e-5f47f0ca9e93','3c5a66b1-961d-620d-103f-5c6e1d689c1c',0,'2020-08-27 17:44:57',1),('955d085b-cbc8-9fcd-2d02-5f47f1ff5071','824bc684-1b3b-2184-338e-5f47f0ca9e93','3a77715d-b123-819f-19ae-5c6e1db13f47',0,'2020-08-27 17:44:57',1),('9599015d-87f4-9037-b38d-5f47f11e1bd9','824bc684-1b3b-2184-338e-5f47f0ca9e93','34d441bb-0fe8-5906-d567-5c6e1dff5aef',0,'2020-08-27 17:44:57',1),('95c1ebd7-0fd8-f45a-595b-5f47f12ac1ff','824bc684-1b3b-2184-338e-5f47f0ca9e93','3e4fc88a-0fb7-4a6e-935e-5c6e1da1cf38',0,'2020-08-27 17:44:57',1),('95f4f36c-040d-79c0-af94-5f47f1de3762','824bc684-1b3b-2184-338e-5f47f0ca9e93','32e6c274-6c87-2818-4db5-5c6e1dfe853d',0,'2020-08-27 17:44:57',1),('9622ea16-ea28-4040-f3ab-5f47f1ad4129','824bc684-1b3b-2184-338e-5f47f0ca9e93','cf862ecd-de63-de1c-8ae3-5c6e1da03a50',0,'2020-08-27 17:44:57',1),('964e9b5c-49fb-cbca-62ea-5f47f14f22b2','824bc684-1b3b-2184-338e-5f47f0ca9e93','d602ff9a-88a3-20a1-36f0-5c6e1ddae899',0,'2020-08-27 17:44:57',1),('9667d008-a420-afbb-4b0f-5f47f156ddd4','69d63f4e-88ef-58c9-0a28-5f469b0dc456','80c063d9-4753-653b-4561-5c6e1d3c54db',90,'2020-08-28 09:56:53',0),('96965258-f736-2db2-8108-5f47f14bfa9a','824bc684-1b3b-2184-338e-5f47f0ca9e93','d46dedb9-a982-67a2-d70a-5c6e1da04d41',0,'2020-08-27 17:44:57',1),('96b19e7f-3f40-0859-300f-5f47f1905f12','69d63f4e-88ef-58c9-0a28-5f469b0dc456','7f16d69c-e29b-910f-4d1e-5c6e1d698cd8',90,'2020-08-28 09:56:53',0),('96c53095-2837-e923-3007-5f47f1974ce1','824bc684-1b3b-2184-338e-5f47f0ca9e93','d92f8b18-3f0e-d480-0183-5c6e1dea0580',0,'2020-08-27 17:44:57',1),('96e57de5-c93a-10f7-7798-5f47f1246bb3','69d63f4e-88ef-58c9-0a28-5f469b0dc456','7a370c0c-ffdd-d739-8f39-5c6e1dccc3c0',90,'2020-08-28 09:56:53',0),('96ef339a-4f2a-4d8d-b87f-5f47f1223094','824bc684-1b3b-2184-338e-5f47f0ca9e93','d79b79ce-5652-e699-c84a-5c6e1ddc8bc8',0,'2020-08-27 17:44:57',1),('97020ebc-2405-4eb6-9543-5f469c13cd69','3304ef15-3d54-d514-0f52-5f469cfc6b17','f1ee01c7-5136-1887-b4a6-5c6e1d1b29ab',90,'2020-08-27 17:49:53',0),('9717825b-fb23-76a9-caee-5f47f15b3766','824bc684-1b3b-2184-338e-5f47f0ca9e93','d2d19ea3-bf2c-67b9-87a6-5c6e1d2d2561',0,'2020-08-27 17:44:57',1),('972cf618-bb1d-fd60-6be5-5f47f161ac47','69d63f4e-88ef-58c9-0a28-5f469b0dc456','825eecdb-807d-fcea-97f9-5c6e1db6ed59',90,'2020-08-28 09:56:53',0),('974543fc-0cb4-3f7f-7a29-5f47f1e233f4','824bc684-1b3b-2184-338e-5f47f0ca9e93','dacaaa84-1cf7-efaa-60c3-5c6e1d807a84',0,'2020-08-27 17:44:57',1),('9763c171-eb87-5624-7aec-5f47f17a5500','69d63f4e-88ef-58c9-0a28-5f469b0dc456','78628393-5890-71b9-4644-5c6e1dbb5160',90,'2020-08-28 09:56:53',0),('9786b670-b6ff-43c8-a38d-5f47f1cf1385','824bc684-1b3b-2184-338e-5f47f0ca9e93','d122f516-ba83-0d8e-741a-5c6e1d51b454',0,'2020-08-27 17:44:57',1),('97a8882b-d6aa-022f-96a0-5f47f120ad1b','69d63f4e-88ef-58c9-0a28-5f469b0dc456','601fdecf-a626-35b3-9142-5c6e1d3b2ee3',89,'2020-08-28 09:56:53',0),('97be3bf4-223d-7c5b-cb2a-5f47f118055e','824bc684-1b3b-2184-338e-5f47f0ca9e93','c8db86d1-7f26-b8aa-5323-5c6e1d4f870e',0,'2020-08-27 17:44:57',1),('97e50cf0-469f-00e8-18b9-5f47f15189ca','69d63f4e-88ef-58c9-0a28-5f469b0dc456','6722c508-1381-f2ac-df11-5c6e1d531d47',90,'2020-08-28 09:56:53',0),('97e92679-3e09-5b62-bf04-5f47f1e0b9ee','824bc684-1b3b-2184-338e-5f47f0ca9e93','d096a892-9823-074d-edd4-5c6e1dfe248d',0,'2020-08-27 17:44:57',1),('9811f257-d024-2a4a-0511-5f47f1d13f56','824bc684-1b3b-2184-338e-5f47f0ca9e93','ced98a67-37f4-6707-c5ac-5c6e1da221c6',0,'2020-08-27 17:44:57',1),('982a5975-0b64-4308-86d9-5f47f14dadfc','69d63f4e-88ef-58c9-0a28-5f469b0dc456','65632a8e-c384-0ba2-6405-5c6e1d997e7f',90,'2020-08-28 09:56:53',0),('983dc4dd-35c6-765f-0b32-5f47f16f5066','824bc684-1b3b-2184-338e-5f47f0ca9e93','d3f3c098-4d7c-d2c6-54b3-5c6e1dc0a477',0,'2020-08-27 17:44:57',1),('9873760d-92b6-889a-1635-5f47f106ca68','69d63f4e-88ef-58c9-0a28-5f469b0dc456','6adabb34-e2ff-75ee-8f62-5c6e1d00e805',90,'2020-08-28 09:56:53',0),('98a3d754-6e4a-191e-ba97-5f47f174f8b2','69d63f4e-88ef-58c9-0a28-5f469b0dc456','68ee4e88-d2a3-9e0f-ea4a-5c6e1dc8db73',90,'2020-08-28 09:56:53',0),('98a72bb7-9112-c3ee-3b3b-5f47f1b5c8a7','824bc684-1b3b-2184-338e-5f47f0ca9e93','d2429bba-7f54-24aa-1491-5c6e1db06ab6',0,'2020-08-27 17:44:57',1),('98d246fc-59e1-7996-a80a-5f47f1b061d2','69d63f4e-88ef-58c9-0a28-5f469b0dc456','63a5cabe-966f-2e06-25f3-5c6e1d6ba561',90,'2020-08-28 09:56:53',0),('98df7ac0-e981-a3f4-1c7b-5f47f1c68478','824bc684-1b3b-2184-338e-5f47f0ca9e93','cd02e82c-7eb8-8630-015d-5c6e1d54dd16',0,'2020-08-27 17:44:57',1),('98ff959f-e56a-b025-2a4d-5f47f1a6c7df','69d63f4e-88ef-58c9-0a28-5f469b0dc456','6dc4083a-2d47-470b-8335-5c6e1d46b191',90,'2020-08-28 09:56:53',0),('990aa24a-383d-8443-2023-5f47f18562b0','824bc684-1b3b-2184-338e-5f47f0ca9e93','d5b4803d-53f2-521a-2ee9-5c6e1d8e681b',0,'2020-08-27 17:44:57',1),('992dd994-eb1c-87a0-08e5-5f47f1de4a3f','69d63f4e-88ef-58c9-0a28-5f469b0dc456','61ea6896-ce86-401e-d0bf-5c6e1d0f725e',90,'2020-08-28 09:56:53',0),('993bccfa-a588-1e37-34ca-5f47f1bed6c1','824bc684-1b3b-2184-338e-5f47f0ca9e93','cb14dd05-873d-715e-92be-5c6e1d21e982',0,'2020-08-27 17:44:57',1),('9972076d-75c9-25e7-4207-5f47f1895475','69d63f4e-88ef-58c9-0a28-5f469b0dc456','9c434503-c420-8ea4-fed4-5e909d093d3f',89,'2020-08-28 09:56:53',0),('99786f0a-45e7-f6d8-340d-5f47f1b285b1','824bc684-1b3b-2184-338e-5f47f0ca9e93','a18e13c0-7b00-9e5b-72c3-5c6e1d886c8c',0,'2020-08-27 17:44:57',1),('99a03567-ed67-bd87-754d-5f47f185fd90','69d63f4e-88ef-58c9-0a28-5f469b0dc456','a80b7337-2bdb-fb18-8a1c-5e909dbccfa9',90,'2020-08-28 09:56:53',0),('99a16a1a-5dbd-a1bc-c6c9-5f47f19f9438','824bc684-1b3b-2184-338e-5f47f0ca9e93','a8f890a5-c581-7a99-4005-5c6e1d7b2ffd',0,'2020-08-27 17:44:57',1),('99cda1c4-82bc-ae93-497e-5f47f1ef4f17','824bc684-1b3b-2184-338e-5f47f0ca9e93','a711af50-e0eb-81dd-aec0-5c6e1d1b2ddd',0,'2020-08-27 17:44:57',1),('99d0aa87-3a2a-19ac-61bd-5f47f1f39834','69d63f4e-88ef-58c9-0a28-5f469b0dc456','a4e20e36-1f69-ef86-e53f-5e909d53c1fd',90,'2020-08-28 09:56:53',0),('99f9ed45-340f-891a-5aa9-5f47f11e7215','824bc684-1b3b-2184-338e-5f47f0ca9e93','ad0f4fee-d33e-2f3c-9f82-5c6e1d1e9a69',0,'2020-08-27 17:44:57',1),('99fa2bad-4bda-7f43-4130-5f47f14989ff','69d63f4e-88ef-58c9-0a28-5f469b0dc456','ad806956-f797-6110-5c34-5e909d976f18',90,'2020-08-28 09:56:53',0),('9a24bb0a-02bc-8881-0245-5f47f1d61149','824bc684-1b3b-2184-338e-5f47f0ca9e93','ab2242ca-d4fd-9549-e791-5c6e1d385890',0,'2020-08-27 17:44:57',1),('9a25df6d-a13e-3ece-748f-5f47f12bffca','69d63f4e-88ef-58c9-0a28-5f469b0dc456','aacba1b3-9082-850d-7134-5e909daf8e6d',90,'2020-08-28 09:56:53',0),('9a692b7d-bf7c-1104-1fe7-5f47f1e09151','69d63f4e-88ef-58c9-0a28-5f469b0dc456','a208420e-202f-0a8e-21cc-5e909d8ce949',90,'2020-08-28 09:56:53',0),('9a6d7986-18ea-9780-4f95-5f47f13ac218','824bc684-1b3b-2184-338e-5f47f0ca9e93','a51ad6e5-d047-65d1-bc96-5c6e1d9b1f5c',0,'2020-08-27 17:44:57',1),('9a7d0254-3150-2c8d-4f02-5f469cb0a496','3304ef15-3d54-d514-0f52-5f469cfc6b17','ecd4a57b-0e61-6ae6-26be-5c6e1dfaf4d9',75,'2020-08-27 17:49:53',0),('9a955bad-2ca3-63d2-5c0b-5f47f15e94aa','69d63f4e-88ef-58c9-0a28-5f469b0dc456','b0990c1d-77d5-2980-28a5-5e909dd6a00f',90,'2020-08-28 09:56:53',0),('9a98ecfb-2634-f58f-c791-5f47f144818c','824bc684-1b3b-2184-338e-5f47f0ca9e93','af3813f0-504b-4051-224b-5c6e1dfd4d58',0,'2020-08-27 17:44:57',1),('9ac53c60-5b4a-157b-be96-5f47f17b5864','69d63f4e-88ef-58c9-0a28-5f469b0dc456','9f5c2b59-2fef-66ef-9bcb-5e909dafc23b',90,'2020-08-28 09:56:53',0),('9ac69a4a-54a6-6284-8660-5f47f16d4962','824bc684-1b3b-2184-338e-5f47f0ca9e93','a34e33e7-f029-be12-4f60-5c6e1d1ee705',0,'2020-08-27 17:44:57',1),('9af49e7a-57a8-614e-58d9-5f47f1da6d92','69d63f4e-88ef-58c9-0a28-5f469b0dc456','80f68222-9e49-e9fa-1164-5e909d2a6681',89,'2020-08-28 09:56:53',0),('9b214a85-9508-741f-675c-5f47f18f2824','69d63f4e-88ef-58c9-0a28-5f469b0dc456','8c37a3f8-e121-94b9-a6df-5e909d1dff74',90,'2020-08-28 09:56:53',0),('9b2e3c48-1bba-ad3d-f0ea-5f47f1097fa3','824bc684-1b3b-2184-338e-5f47f0ca9e93','b62a2bfc-2615-5abd-0a60-5c6e1d86946a',0,'2020-08-27 17:44:57',1),('9b6e1514-485a-992a-7ed3-5f47f12bc799','824bc684-1b3b-2184-338e-5f47f0ca9e93','bd16a03b-186c-145a-c165-5c6e1d2bb8aa',0,'2020-08-27 17:44:57',1),('9bd305be-3dd0-30cb-08d9-5f47f1e482b8','824bc684-1b3b-2184-338e-5f47f0ca9e93','bb63e112-5c92-af82-2ed4-5c6e1d722c85',0,'2020-08-27 17:44:57',1),('9bf93473-a10b-6e6a-2421-5f47f1c503d1','69d63f4e-88ef-58c9-0a28-5f469b0dc456','8996b5a7-524c-6482-4fd5-5e909dd4afa9',90,'2020-08-28 09:56:53',0),('9bfcc536-c36b-2b39-0d5c-5f47f1fbe680','824bc684-1b3b-2184-338e-5f47f0ca9e93','c06bc7c3-f4ae-9709-ad32-5c6e1df9919b',0,'2020-08-27 17:44:57',1),('9c285221-bb5b-5d8a-51ec-5f47f1e5c51b','824bc684-1b3b-2184-338e-5f47f0ca9e93','bebe1a01-5b96-efc4-c366-5c6e1dfc050d',0,'2020-08-27 17:44:57',1),('9c2ded1e-0665-7f30-168e-5f47f1229177','69d63f4e-88ef-58c9-0a28-5f469b0dc456','916ef2d8-8108-7977-e586-5e909d2cf1ca',90,'2020-08-28 09:56:53',0),('9c663d56-00d3-9441-ec45-5f47f16635fd','824bc684-1b3b-2184-338e-5f47f0ca9e93','b99c202c-a3f2-0e4c-117c-5c6e1d334507',0,'2020-08-27 17:44:57',1),('9c72a738-5c9d-6c12-976d-5f47f1928a53','69d63f4e-88ef-58c9-0a28-5f469b0dc456','8ed5f075-fc75-10d1-ad3b-5e909dd49592',90,'2020-08-28 09:56:53',0),('9c90068a-288f-dfc0-2933-5f47f1447872','824bc684-1b3b-2184-338e-5f47f0ca9e93','c216ae81-667d-57c6-1b66-5c6e1d66fedc',0,'2020-08-27 17:44:57',1),('9ca6b38b-e89e-1386-7d78-5f47f1ae4a5f','69d63f4e-88ef-58c9-0a28-5f469b0dc456','867e07d4-c36e-2ff7-cb57-5e909da7e961',90,'2020-08-28 09:56:53',0),('9cc9e22c-8b9b-3def-543a-5f47f176a3e6','824bc684-1b3b-2184-338e-5f47f0ca9e93','b7d811b6-4b23-7df0-5897-5c6e1d72ffd6',0,'2020-08-27 17:44:57',1),('9cd2d994-95fb-f21e-2abc-5f47f1076439','69d63f4e-88ef-58c9-0a28-5f469b0dc456','943de675-7b7d-dac8-7dfb-5e909d7e0319',90,'2020-08-28 09:56:53',0),('9cf51681-a1d6-9c41-120b-5f47f18f3b7b','824bc684-1b3b-2184-338e-5f47f0ca9e93','7a0c717c-743b-9dff-0e33-5c6e1d5c14d4',0,'2020-08-27 17:44:57',1),('9cfd69da-0941-c6a5-4813-5f47f177a65b','69d63f4e-88ef-58c9-0a28-5f469b0dc456','83aac233-eb13-15c1-fb5b-5e909d4c2678',90,'2020-08-28 09:56:53',0),('9d20c643-0b48-90d8-6206-5f47f198b15f','824bc684-1b3b-2184-338e-5f47f0ca9e93','80f9ae9e-3727-299f-afd5-5c6e1d0fa123',0,'2020-08-27 17:44:57',1),('9d2ce4f8-b910-7d4c-2143-5f47f13cf925','69d63f4e-88ef-58c9-0a28-5f469b0dc456','663299bc-dcf9-c549-4fa0-5e909d628646',89,'2020-08-28 09:56:53',0),('9d5f94fc-f44f-4e9a-b132-5f47f146be4c','824bc684-1b3b-2184-338e-5f47f0ca9e93','7f33c032-982b-f7e2-c3de-5c6e1d0aa89d',0,'2020-08-27 17:44:57',1),('9d81a7db-f35c-b2f0-4b88-5f47f1aa6271','69d63f4e-88ef-58c9-0a28-5f469b0dc456','70c3a57c-fbcd-1c89-5375-5e909d38029e',90,'2020-08-28 09:56:53',0),('9d87ccbe-c565-a5dd-1bd9-5f47f1e67853','824bc684-1b3b-2184-338e-5f47f0ca9e93','845ceb37-c664-d7bb-14d7-5c6e1df0b770',0,'2020-08-27 17:44:57',1),('9dadaeb5-62dd-1505-df19-5f47f1abf0bf','69d63f4e-88ef-58c9-0a28-5f469b0dc456','6e16736e-c057-92a9-2ae0-5e909d12be7e',90,'2020-08-28 09:56:53',0),('9db2f23e-9aa1-e8f3-7174-5f47f1e0d7c3','824bc684-1b3b-2184-338e-5f47f0ca9e93','82a9c474-0dc1-c281-491e-5c6e1d7b27b4',0,'2020-08-27 17:44:57',1),('9ddf8560-305e-a91f-740a-5f47f1a4d6c5','824bc684-1b3b-2184-338e-5f47f0ca9e93','7d797f14-23ec-eae0-8e4d-5c6e1dd6311f',0,'2020-08-27 17:44:57',1),('9df08109-5acb-75bc-0342-5f47f1e44178','69d63f4e-88ef-58c9-0a28-5f469b0dc456','76082a27-4de7-0e8c-58af-5e909d3f3bb7',90,'2020-08-28 09:56:53',0),('9e088877-9502-0173-f670-5f47f1cd9354','824bc684-1b3b-2184-338e-5f47f0ca9e93','860c4f68-86d7-ef63-880b-5c6e1df3f6e6',0,'2020-08-27 17:44:57',1),('9e1f678e-1f5f-4130-688c-5f47f1703a52','69d63f4e-88ef-58c9-0a28-5f469b0dc456','7369c6c7-8a9a-947c-561e-5e909df4b40a',90,'2020-08-28 09:56:53',0),('9e47c829-416a-11cd-3c8c-5f47f13d9b4c','824bc684-1b3b-2184-338e-5f47f0ca9e93','7bcaac3e-0c7f-302d-e870-5c6e1dfb18f0',0,'2020-08-27 17:44:57',1),('9e5f1440-f0bd-ba83-90f6-5f47f1589897','69d63f4e-88ef-58c9-0a28-5f469b0dc456','6b4e4fdc-6375-5146-7e8c-5e909d304fe3',90,'2020-08-28 09:56:53',0),('9e70e16e-dc67-caf1-bc16-5f47f16125a3','824bc684-1b3b-2184-338e-5f47f0ca9e93','8d5f0375-04e8-9374-548c-5c6e1d4185cb',0,'2020-08-27 17:44:57',1),('9e8c507b-8474-f61a-47e4-5f47f1e70b8f','69d63f4e-88ef-58c9-0a28-5f469b0dc456','78a44655-6d0b-4769-8398-5e909de2f14a',90,'2020-08-28 09:56:53',0),('9e9f5e23-a795-33d1-2480-5f47f1a9a14e','824bc684-1b3b-2184-338e-5f47f0ca9e93','94cd0212-7227-a254-b111-5c6e1d48d71c',0,'2020-08-27 17:44:57',1),('9eb88b08-98f0-ba5a-ab39-5f47f1dab056','69d63f4e-88ef-58c9-0a28-5f469b0dc456','68c1bad8-f8ac-08db-960c-5e909d713286',90,'2020-08-28 09:56:53',0),('9ed63005-c28a-87fa-6d41-5f47f1357fa4','824bc684-1b3b-2184-338e-5f47f0ca9e93','92f3f737-caa2-dd8a-8a13-5c6e1d4916b9',0,'2020-08-27 17:44:57',1),('9ee9173b-6a94-7595-9e6e-5f47f14b0dbe','69d63f4e-88ef-58c9-0a28-5f469b0dc456','284d5d27-c0b6-9f4a-7f21-5e909d653e5d',89,'2020-08-28 09:56:53',0),('9f04effb-67d4-0860-d24f-5f47f127a270','824bc684-1b3b-2184-338e-5f47f0ca9e93','9867e242-ed91-e741-d623-5c6e1d746db4',0,'2020-08-27 17:44:57',1),('9f1babcf-2309-194c-917c-5f47f14347a9','69d63f4e-88ef-58c9-0a28-5f469b0dc456','3a88b4c8-dd0b-3a95-43b1-5e909d542ac7',90,'2020-08-28 09:56:53',0),('9f534e75-8721-b7d5-b0f7-5f47f134ae4a','824bc684-1b3b-2184-338e-5f47f0ca9e93','96a17d4d-5ab9-2ed8-c2a4-5c6e1d987a1c',0,'2020-08-27 17:44:57',1),('9f5dc18a-a117-d3fa-5516-5f47f1f47f52','69d63f4e-88ef-58c9-0a28-5f469b0dc456','3760e1ef-f22f-8223-a180-5e909d815c59',90,'2020-08-28 09:56:53',0),('9f7ff12b-c5cc-a4c5-0fbd-5f47f13223cd','824bc684-1b3b-2184-338e-5f47f0ca9e93','9114c47d-c20b-a4c8-c2a6-5c6e1da9acb1',0,'2020-08-27 17:44:57',1),('9f8b9e02-f5ad-3022-d5ae-5f47f182092c','69d63f4e-88ef-58c9-0a28-5f469b0dc456','3fec8550-d616-39f3-2c24-5e909da3cebb',90,'2020-08-28 09:56:53',0),('9fabc520-f63d-c900-d775-5f47f1366197','824bc684-1b3b-2184-338e-5f47f0ca9e93','9a32ca04-50c7-66b7-3185-5c6e1d384a7b',0,'2020-08-27 17:44:57',1),('9fbdd6cf-0c74-7058-a108-5f47f1187caf','69d63f4e-88ef-58c9-0a28-5f469b0dc456','3d3b556a-48b1-9d71-2a7b-5e909d062e9e',90,'2020-08-28 09:56:53',0),('9fd5c207-e884-c7d8-270e-5f47f1e8c7f5','824bc684-1b3b-2184-338e-5f47f0ca9e93','8f3ad6ec-99d2-f98e-7785-5c6e1db67802',0,'2020-08-27 17:44:57',1),('9febb623-ae45-4d5e-a742-5f47f1712bd3','69d63f4e-88ef-58c9-0a28-5f469b0dc456','347e56bb-e0e3-1619-1016-5e909d1a0b25',90,'2020-08-28 09:56:53',0),('9ff507c5-380b-38c1-cd7f-5f469c1a791e','3304ef15-3d54-d514-0f52-5f469cfc6b17','181a0df8-d32e-3a78-63b5-5c6e1db02bf9',90,'2020-08-27 17:49:53',0),('a00c37b7-5af1-12fb-eb16-5f47f1e088a3','824bc684-1b3b-2184-338e-5f47f0ca9e93','9d5add96-09e7-13e4-5533-5c6e1d5eabe4',0,'2020-08-27 17:44:57',1),('a017e697-46a0-395f-5a76-5f47f1372d65','69d63f4e-88ef-58c9-0a28-5f469b0dc456','42972982-baf6-fdcd-9660-5e909d9bae21',90,'2020-08-28 09:56:53',0),('a048eac9-1c44-a7b4-e9bb-5f47f1ae501e','824bc684-1b3b-2184-338e-5f47f0ca9e93','a3b6eade-aa75-fdfb-bb22-5c6e1d132305',0,'2020-08-27 17:44:57',1),('a05df1f4-4e5d-67c6-faeb-5f47f14c71dc','69d63f4e-88ef-58c9-0a28-5f469b0dc456','31872bf6-c690-3e22-7268-5e909d387954',90,'2020-08-28 09:56:53',0),('a07df84c-9ea1-b704-b7c8-5f47f1f71202','824bc684-1b3b-2184-338e-5f47f0ca9e93','a221846d-edb3-d08b-955d-5c6e1de207c6',0,'2020-08-27 17:44:57',1),('a088ce3c-2995-a67d-1528-5f47f16e821e','69d63f4e-88ef-58c9-0a28-5f469b0dc456','4b17887f-a8cf-69f1-d0d3-5e909dc73ad9',89,'2020-08-28 09:56:53',0),('a0ae255b-90b1-74a5-fa7e-5f47f14f9f2d','824bc684-1b3b-2184-338e-5f47f0ca9e93','a6eaafe1-3a6e-8a53-b119-5c6e1db76720',0,'2020-08-27 17:44:57',1),('a0b30c2f-bc76-b732-9e24-5f47f17291fe','69d63f4e-88ef-58c9-0a28-5f469b0dc456','55d445e5-2ef1-8430-5d2f-5e909deca916',90,'2020-08-28 09:56:53',0),('a0dcaf5e-cb46-d54b-ea69-5f47f157ce76','824bc684-1b3b-2184-338e-5f47f0ca9e93','a5507fba-e80d-d18b-7347-5c6e1d7ca48d',0,'2020-08-27 17:44:57',1),('a0efbf9b-98fe-9d05-f598-5f47f12224ab','69d63f4e-88ef-58c9-0a28-5f469b0dc456','5312fe14-8b77-db19-15f8-5e909df9f318',90,'2020-08-28 09:56:53',0),('a1075c8d-8d2a-c85b-8798-5f47f1e28591','824bc684-1b3b-2184-338e-5f47f0ca9e93','a08e9c0b-d79a-1e4c-baed-5c6e1dd818bb',0,'2020-08-27 17:44:57',1),('a119786c-f819-d4bb-833b-5f47f136eb44','69d63f4e-88ef-58c9-0a28-5f469b0dc456','5b4ca0aa-8bd6-4cea-7241-5e909d8e5d85',90,'2020-08-28 09:56:53',0),('a1540665-2b46-9668-8732-5f47f1a79ad3','824bc684-1b3b-2184-338e-5f47f0ca9e93','a897f543-7d27-85ff-87fa-5c6e1dffec0f',0,'2020-08-27 17:44:57',1),('a15966ca-2c84-3ea1-8385-5f47f1dcec86','69d63f4e-88ef-58c9-0a28-5f469b0dc456','589d9e81-c2f0-7bd2-c4a6-5e909d891f4d',90,'2020-08-28 09:56:53',0),('a17eef0a-8891-e351-35f8-5f47f1b68b10','824bc684-1b3b-2184-338e-5f47f0ca9e93','9ef99bea-c305-cfbf-a4ba-5c6e1dc6b98d',0,'2020-08-27 17:44:57',1),('a1843e14-2666-1e5f-cbd5-5f47f151fd70','69d63f4e-88ef-58c9-0a28-5f469b0dc456','505e3bdd-18eb-ba6c-5ce8-5e909d8c96bf',90,'2020-08-28 09:56:53',0),('a1a963ae-9fd9-dee5-99b7-5f47f1bcfaaa','824bc684-1b3b-2184-338e-5f47f0ca9e93','8c28ae8a-3b44-22b0-8c48-5c6e1db709ed',0,'2020-08-27 17:44:57',1),('a1b2761e-3326-797e-c950-5f47f15c5467','69d63f4e-88ef-58c9-0a28-5f469b0dc456','5e3c3e09-2a8c-bc08-0096-5e909dd501e1',90,'2020-08-28 09:56:53',0),('a1d40366-d67e-1373-61f2-5f47f19484ae','824bc684-1b3b-2184-338e-5f47f0ca9e93','929da18d-66a1-fac9-8cbf-5c6e1d22072a',0,'2020-08-27 17:44:57',1),('a1e17974-e3ce-03cd-a447-5f47f1428e9f','69d63f4e-88ef-58c9-0a28-5f469b0dc456','4da6d5e4-5f2a-058f-67a4-5e909dfafc91',90,'2020-08-28 09:56:53',0),('a1ffa952-3b8a-6ec3-a597-5f47f12372e3','824bc684-1b3b-2184-338e-5f47f0ca9e93','90fd3e60-d40b-fcd4-fb55-5c6e1dae8fe0',0,'2020-08-27 17:44:57',1),('a20ecd69-baa4-d8ef-d156-5f47f1e16908','69d63f4e-88ef-58c9-0a28-5f469b0dc456','90b472dc-63cc-dc05-77bd-5c6e1d207f5b',89,'2020-08-28 09:56:53',0),('a23f1435-efdc-b451-70fd-5f47f187e167','824bc684-1b3b-2184-338e-5f47f0ca9e93','95ce0dd9-8872-02a1-0db3-5c6e1d9bcf73',0,'2020-08-27 17:44:57',1),('a255e5bf-ac76-a925-c222-5f47f17e2fd8','69d63f4e-88ef-58c9-0a28-5f469b0dc456','97aa67e1-2ce4-f1b7-30a3-5c6e1d102e97',90,'2020-08-28 09:56:53',0),('a26bcef8-9993-87a2-a8d9-5f47f10d5804','824bc684-1b3b-2184-338e-5f47f0ca9e93','94383006-21ad-710b-5dc1-5c6e1dfd3bb5',0,'2020-08-27 17:44:57',1),('a28754b6-934d-adab-2314-5f47f114f67f','69d63f4e-88ef-58c9-0a28-5f469b0dc456','95ac91c3-34ff-4358-5479-5c6e1df5f99e',90,'2020-08-28 09:56:53',0),('a29745bf-463a-a197-f304-5f47f186ffda','824bc684-1b3b-2184-338e-5f47f0ca9e93','8f5dfb14-8ef8-cee3-ccc8-5c6e1da591cc',0,'2020-08-27 17:44:57',1),('a2b5eab1-876b-f64d-1444-5f47f129a38c','69d63f4e-88ef-58c9-0a28-5f469b0dc456','9b6618c8-31c6-d4f8-0ee8-5c6e1d6493d2',90,'2020-08-28 09:56:53',0),('a2c11dbf-54f6-4a2e-c1bf-5f47f15613e0','824bc684-1b3b-2184-338e-5f47f0ca9e93','97680cfd-ea91-f40b-322a-5c6e1d972dd4',0,'2020-08-27 17:44:57',1),('a2e48bb5-abce-de9f-dc4e-5f47f1071c5c','69d63f4e-88ef-58c9-0a28-5f469b0dc456','99ab8d2d-89a3-9c76-44ba-5c6e1dbbb117',90,'2020-08-28 09:56:53',0),('a2ecb5d5-c4ab-5a29-7d19-5f47f1ad8ba2','824bc684-1b3b-2184-338e-5f47f0ca9e93','8dc2b087-f5ec-cf51-7690-5c6e1d369edb',0,'2020-08-27 17:44:57',1),('a32c2f19-c290-df49-e085-5f47f15d94a4','824bc684-1b3b-2184-338e-5f47f0ca9e93','76c626c4-b247-8bde-6d8b-5c6e1d204ff2',0,'2020-08-27 17:44:57',1),('a3325858-7288-3a54-d775-5f47f142cd7e','69d63f4e-88ef-58c9-0a28-5f469b0dc456','940d3876-ece1-f13c-45af-5c6e1d9f9019',90,'2020-08-28 09:56:53',0),('a35e355d-45c3-88dd-59ff-5f47f1093151','69d63f4e-88ef-58c9-0a28-5f469b0dc456','9d2871bf-a86f-48fc-4161-5c6e1d149e42',90,'2020-08-28 09:56:53',0),('a36768b4-9f20-f9dc-4942-5f47f1a84dc2','824bc684-1b3b-2184-338e-5f47f0ca9e93','7d74c28a-6fd6-3816-ea14-5c6e1d65a241',0,'2020-08-27 17:44:57',1),('a38fbced-1135-40de-3ee7-5f47f17eaed1','69d63f4e-88ef-58c9-0a28-5f469b0dc456','92665564-d5ba-d4a7-55d7-5c6e1de7d212',90,'2020-08-28 09:56:53',0),('a3bc0720-110e-1f28-b636-5f47f1db25e7','824bc684-1b3b-2184-338e-5f47f0ca9e93','7bd8bdff-cee3-fb8f-84c6-5c6e1dcd0834',0,'2020-08-27 17:44:57',1),('a3df04cb-ace4-5de1-23e9-5f469cc2458c','3304ef15-3d54-d514-0f52-5f469cfc6b17','eb1dc44d-6ed8-fa1b-6fbe-5c6e1d3d094a',75,'2020-08-27 17:49:53',0),('a3e63186-4306-4dd3-41dd-5f47f17463c2','824bc684-1b3b-2184-338e-5f47f0ca9e93','80c063d9-4753-653b-4561-5c6e1d3c54db',0,'2020-08-27 17:44:57',1),('a427e073-036b-573a-67d3-5f47f1573a9d','824bc684-1b3b-2184-338e-5f47f0ca9e93','7f16d69c-e29b-910f-4d1e-5c6e1d698cd8',0,'2020-08-27 17:44:57',1),('a453f283-ba12-042f-4c14-5f47f1dc4a2e','824bc684-1b3b-2184-338e-5f47f0ca9e93','7a370c0c-ffdd-d739-8f39-5c6e1dccc3c0',0,'2020-08-27 17:44:57',1),('a47d3f4e-4388-c317-7afd-5f47f191e671','824bc684-1b3b-2184-338e-5f47f0ca9e93','825eecdb-807d-fcea-97f9-5c6e1db6ed59',0,'2020-08-27 17:44:57',1),('a4b00913-b8a8-2f52-b0f9-5f47f195d543','824bc684-1b3b-2184-338e-5f47f0ca9e93','78628393-5890-71b9-4644-5c6e1dbb5160',0,'2020-08-27 17:44:57',1),('a4daf9f2-0cfc-36db-08ea-5f47f15431e0','824bc684-1b3b-2184-338e-5f47f0ca9e93','601fdecf-a626-35b3-9142-5c6e1d3b2ee3',0,'2020-08-27 17:44:57',1),('a50581c6-e18f-99f7-e662-5f47f1181499','824bc684-1b3b-2184-338e-5f47f0ca9e93','6722c508-1381-f2ac-df11-5c6e1d531d47',0,'2020-08-27 17:44:57',1),('a54391c5-f67e-b33c-3dba-5f47f16a464f','824bc684-1b3b-2184-338e-5f47f0ca9e93','65632a8e-c384-0ba2-6405-5c6e1d997e7f',0,'2020-08-27 17:44:57',1),('a5710f35-bb33-8ddc-be02-5f47f1c38d32','824bc684-1b3b-2184-338e-5f47f0ca9e93','6adabb34-e2ff-75ee-8f62-5c6e1d00e805',0,'2020-08-27 17:44:57',1),('a59ba81a-451e-f7d3-02e3-5f47f1de0739','824bc684-1b3b-2184-338e-5f47f0ca9e93','68ee4e88-d2a3-9e0f-ea4a-5c6e1dc8db73',0,'2020-08-27 17:44:57',1),('a5c52385-741d-dd93-14e3-5f47f1d62787','824bc684-1b3b-2184-338e-5f47f0ca9e93','63a5cabe-966f-2e06-25f3-5c6e1d6ba561',0,'2020-08-27 17:44:57',1),('a5f01786-042b-3be2-4e57-5f47f1353bcf','824bc684-1b3b-2184-338e-5f47f0ca9e93','6dc4083a-2d47-470b-8335-5c6e1d46b191',0,'2020-08-27 17:44:57',1),('a6313e97-7b16-daa7-2a5c-5f47f13122d8','824bc684-1b3b-2184-338e-5f47f0ca9e93','61ea6896-ce86-401e-d0bf-5c6e1d0f725e',0,'2020-08-27 17:44:57',1),('a65b0b1c-2b26-330b-907e-5f47f1d9fbeb','824bc684-1b3b-2184-338e-5f47f0ca9e93','9c434503-c420-8ea4-fed4-5e909d093d3f',0,'2020-08-27 17:44:57',1),('a685119c-9f3a-4f81-e35f-5f47f13c4fcc','824bc684-1b3b-2184-338e-5f47f0ca9e93','a80b7337-2bdb-fb18-8a1c-5e909dbccfa9',0,'2020-08-27 17:44:57',1),('a6b0918d-6b80-76be-144c-5f47f15efd6e','824bc684-1b3b-2184-338e-5f47f0ca9e93','a4e20e36-1f69-ef86-e53f-5e909d53c1fd',0,'2020-08-27 17:44:57',1),('a6dcd477-36eb-1e20-e466-5f47f14dd4cc','824bc684-1b3b-2184-338e-5f47f0ca9e93','ad806956-f797-6110-5c34-5e909d976f18',0,'2020-08-27 17:44:57',1),('a71c170e-ca19-3da8-63ee-5f47f16628a3','824bc684-1b3b-2184-338e-5f47f0ca9e93','aacba1b3-9082-850d-7134-5e909daf8e6d',0,'2020-08-27 17:44:57',1),('a7350984-adcf-cf8e-b8a3-5f469cbd7b08','3304ef15-3d54-d514-0f52-5f469cfc6b17','d4d93551-68c5-3088-cd6d-5c6e1dfc5050',89,'2020-08-27 17:49:53',0),('a746766c-fe86-2ac4-a22f-5f47f13a51db','824bc684-1b3b-2184-338e-5f47f0ca9e93','a208420e-202f-0a8e-21cc-5e909d8ce949',0,'2020-08-27 17:44:57',1),('a76fc82c-01e0-fcd3-0a83-5f47f1ad254d','824bc684-1b3b-2184-338e-5f47f0ca9e93','b0990c1d-77d5-2980-28a5-5e909dd6a00f',0,'2020-08-27 17:44:57',1),('a7a41140-0c93-50a6-95d8-5f47f1472d18','824bc684-1b3b-2184-338e-5f47f0ca9e93','9f5c2b59-2fef-66ef-9bcb-5e909dafc23b',0,'2020-08-27 17:44:57',1),('a7cebcb5-ce67-6e2e-fbb9-5f47f1abe17c','824bc684-1b3b-2184-338e-5f47f0ca9e93','80f68222-9e49-e9fa-1164-5e909d2a6681',0,'2020-08-27 17:44:57',1),('a815b276-d699-cac9-f615-5f47f1e70386','824bc684-1b3b-2184-338e-5f47f0ca9e93','8c37a3f8-e121-94b9-a6df-5e909d1dff74',0,'2020-08-27 17:44:57',1),('a841dbb4-6fcd-a886-53c8-5f47f138fab0','824bc684-1b3b-2184-338e-5f47f0ca9e93','8996b5a7-524c-6482-4fd5-5e909dd4afa9',0,'2020-08-27 17:44:57',1),('a86c84e1-e01c-51c4-cd6b-5f47f1341c79','824bc684-1b3b-2184-338e-5f47f0ca9e93','916ef2d8-8108-7977-e586-5e909d2cf1ca',0,'2020-08-27 17:44:57',1),('a89a55a2-7c27-25d0-d77d-5f47f10e7055','824bc684-1b3b-2184-338e-5f47f0ca9e93','8ed5f075-fc75-10d1-ad3b-5e909dd49592',0,'2020-08-27 17:44:57',1),('a8c8decb-b78f-9981-9c73-5f47f1cee5f8','824bc684-1b3b-2184-338e-5f47f0ca9e93','867e07d4-c36e-2ff7-cb57-5e909da7e961',0,'2020-08-27 17:44:57',1),('a9137810-8e50-f0fc-43f3-5f47f18bb78b','824bc684-1b3b-2184-338e-5f47f0ca9e93','943de675-7b7d-dac8-7dfb-5e909d7e0319',0,'2020-08-27 17:44:57',1),('a9443d69-ad04-87c5-3bd1-5f47f14888ad','824bc684-1b3b-2184-338e-5f47f0ca9e93','83aac233-eb13-15c1-fb5b-5e909d4c2678',0,'2020-08-27 17:44:57',1),('a972d0b0-3ece-f130-84c6-5f47f18b8c99','824bc684-1b3b-2184-338e-5f47f0ca9e93','663299bc-dcf9-c549-4fa0-5e909d628646',0,'2020-08-27 17:44:57',1),('a9a38f42-f227-33ff-a6ae-5f47f1f821e7','824bc684-1b3b-2184-338e-5f47f0ca9e93','70c3a57c-fbcd-1c89-5375-5e909d38029e',0,'2020-08-27 17:44:57',1),('a9d171db-c4dc-0f0d-b337-5f47f179b6db','824bc684-1b3b-2184-338e-5f47f0ca9e93','6e16736e-c057-92a9-2ae0-5e909d12be7e',0,'2020-08-27 17:44:57',1),('aa13b6ba-ce4d-c55d-713c-5f47f12445bc','824bc684-1b3b-2184-338e-5f47f0ca9e93','76082a27-4de7-0e8c-58af-5e909d3f3bb7',0,'2020-08-27 17:44:57',1),('aa220b13-f46d-7bf2-821f-5f469cfdd465','3304ef15-3d54-d514-0f52-5f469cfc6b17','dda1ad47-01b9-046e-cd89-5c6e1d09c51d',-99,'2020-08-27 17:49:53',0),('aa3fbb88-8607-3762-8f93-5f47f1a6a1b2','824bc684-1b3b-2184-338e-5f47f0ca9e93','7369c6c7-8a9a-947c-561e-5e909df4b40a',0,'2020-08-27 17:44:57',1),('aa6bbbd4-5dec-5d5c-94f9-5f47f12cb11c','824bc684-1b3b-2184-338e-5f47f0ca9e93','6b4e4fdc-6375-5146-7e8c-5e909d304fe3',0,'2020-08-27 17:44:57',1),('aa973c3a-6dbf-3c7a-029b-5f47f193d0fa','824bc684-1b3b-2184-338e-5f47f0ca9e93','78a44655-6d0b-4769-8398-5e909de2f14a',0,'2020-08-27 17:44:57',1),('aacaf01d-021b-a182-cd45-5f47f10e59a2','824bc684-1b3b-2184-338e-5f47f0ca9e93','68c1bad8-f8ac-08db-960c-5e909d713286',0,'2020-08-27 17:44:57',1),('ab175cfe-1570-8d2d-708d-5f47f19e8ef0','824bc684-1b3b-2184-338e-5f47f0ca9e93','284d5d27-c0b6-9f4a-7f21-5e909d653e5d',0,'2020-08-27 17:44:57',1),('ab46d7a8-3e44-6f4a-f243-5f47f1fa913f','824bc684-1b3b-2184-338e-5f47f0ca9e93','3a88b4c8-dd0b-3a95-43b1-5e909d542ac7',0,'2020-08-27 17:44:57',1),('ab7161e7-81e0-6d3e-96e9-5f47f15082d1','824bc684-1b3b-2184-338e-5f47f0ca9e93','3760e1ef-f22f-8223-a180-5e909d815c59',0,'2020-08-27 17:44:57',1),('abd91709-a117-335c-cc9c-5f47f11c433f','824bc684-1b3b-2184-338e-5f47f0ca9e93','3fec8550-d616-39f3-2c24-5e909da3cebb',0,'2020-08-27 17:44:57',1),('ac27c688-852a-31ef-d703-5f47f1dcce4f','824bc684-1b3b-2184-338e-5f47f0ca9e93','3d3b556a-48b1-9d71-2a7b-5e909d062e9e',0,'2020-08-27 17:44:57',1),('ac6412fe-6b8b-cf65-6195-5f47f13a507d','824bc684-1b3b-2184-338e-5f47f0ca9e93','347e56bb-e0e3-1619-1016-5e909d1a0b25',0,'2020-08-27 17:44:57',1),('aca05af4-90e3-47e7-943e-5f47f1d0ec68','824bc684-1b3b-2184-338e-5f47f0ca9e93','42972982-baf6-fdcd-9660-5e909d9bae21',0,'2020-08-27 17:44:57',1),('acdc4902-4bbd-78fc-4058-5f47f1cdd624','824bc684-1b3b-2184-338e-5f47f0ca9e93','31872bf6-c690-3e22-7268-5e909d387954',0,'2020-08-27 17:44:57',1),('ad3bd9e3-7608-5332-ac06-5f47f1b6e4a3','824bc684-1b3b-2184-338e-5f47f0ca9e93','4b17887f-a8cf-69f1-d0d3-5e909dc73ad9',0,'2020-08-27 17:44:57',1),('ad76f69e-bbf9-8801-0cee-5f47f1dbe75c','824bc684-1b3b-2184-338e-5f47f0ca9e93','55d445e5-2ef1-8430-5d2f-5e909deca916',0,'2020-08-27 17:44:57',1),('adba20e5-1e39-52f8-092c-5f47f1f1d6e9','824bc684-1b3b-2184-338e-5f47f0ca9e93','5312fe14-8b77-db19-15f8-5e909df9f318',0,'2020-08-27 17:44:57',1),('ae144b0f-973d-c659-3f16-5f47f1b43bf8','824bc684-1b3b-2184-338e-5f47f0ca9e93','5b4ca0aa-8bd6-4cea-7241-5e909d8e5d85',0,'2020-08-27 17:44:57',1),('ae4e8e92-9712-b6b4-7a48-5f47f13aeafc','824bc684-1b3b-2184-338e-5f47f0ca9e93','589d9e81-c2f0-7bd2-c4a6-5e909d891f4d',0,'2020-08-27 17:44:57',1),('ae82b272-a27b-3720-a6c6-5f47f18ee32d','824bc684-1b3b-2184-338e-5f47f0ca9e93','505e3bdd-18eb-ba6c-5ce8-5e909d8c96bf',0,'2020-08-27 17:44:57',1),('aeb9d14e-d626-2db9-8a49-5f47f113c48d','824bc684-1b3b-2184-338e-5f47f0ca9e93','5e3c3e09-2a8c-bc08-0096-5e909dd501e1',0,'2020-08-27 17:44:57',1),('aec30028-960d-4c1d-5b3a-5f469c6d904d','3304ef15-3d54-d514-0f52-5f469cfc6b17','dbd75848-1e97-c63d-0ba7-5c6e1d60ef8c',75,'2020-08-27 17:49:53',0),('af1b7a71-1d74-1c68-9d0e-5f47f1f21c2e','824bc684-1b3b-2184-338e-5f47f0ca9e93','4da6d5e4-5f2a-058f-67a4-5e909dfafc91',0,'2020-08-27 17:44:57',1),('af4a28f1-6710-22b5-a99d-5f47f116a07a','824bc684-1b3b-2184-338e-5f47f0ca9e93','90b472dc-63cc-dc05-77bd-5c6e1d207f5b',0,'2020-08-27 17:44:57',1),('af76b075-c224-728f-4e8d-5f47f135c887','824bc684-1b3b-2184-338e-5f47f0ca9e93','97aa67e1-2ce4-f1b7-30a3-5c6e1d102e97',0,'2020-08-27 17:44:57',1),('afa93cd6-6846-1115-d92a-5f47f1dbe1e1','824bc684-1b3b-2184-338e-5f47f0ca9e93','95ac91c3-34ff-4358-5479-5c6e1df5f99e',0,'2020-08-27 17:44:57',1),('aff11391-75dc-61db-58d3-5f47f100f46a','824bc684-1b3b-2184-338e-5f47f0ca9e93','9b6618c8-31c6-d4f8-0ee8-5c6e1d6493d2',0,'2020-08-27 17:44:57',1),('b0204e06-2610-5ffc-63f3-5f47f1369c09','824bc684-1b3b-2184-338e-5f47f0ca9e93','99ab8d2d-89a3-9c76-44ba-5c6e1dbbb117',0,'2020-08-27 17:44:57',1),('b052a925-1244-fd77-28fd-5f47f18b1d36','824bc684-1b3b-2184-338e-5f47f0ca9e93','940d3876-ece1-f13c-45af-5c6e1d9f9019',0,'2020-08-27 17:44:57',1),('b08c66de-3356-4bae-133d-5f47f1fff368','824bc684-1b3b-2184-338e-5f47f0ca9e93','9d2871bf-a86f-48fc-4161-5c6e1d149e42',0,'2020-08-27 17:44:57',1),('b0cb5893-f979-f31e-5e88-5f47f1a8661a','824bc684-1b3b-2184-338e-5f47f0ca9e93','92665564-d5ba-d4a7-55d7-5c6e1de7d212',0,'2020-08-27 17:44:57',1),('b1a9009a-2539-8520-e99a-5f469c9cdace','3304ef15-3d54-d514-0f52-5f469cfc6b17','e10e0f4f-3089-f6dc-da51-5c6e1d2fda64',75,'2020-08-27 17:49:53',0),('b4f40eb4-1470-be1c-0c56-5f469c198ba5','3304ef15-3d54-d514-0f52-5f469cfc6b17','df56ae76-a203-3777-2e68-5c6e1da9aa23',90,'2020-08-27 17:49:53',0),('b7f7018a-d1a2-3ae3-42e4-5f469c76b394','3304ef15-3d54-d514-0f52-5f469cfc6b17','da15b921-e3da-b9da-7692-5c6e1d338231',75,'2020-08-27 17:49:53',0),('bc640516-adfb-910e-b2bd-5f469c69b933','3304ef15-3d54-d514-0f52-5f469cfc6b17','e2be1f2e-bf7a-99e2-93a1-5c6e1d7b6bba',90,'2020-08-27 17:49:53',0),('c15c05d2-136d-ba29-f35a-5f469cfb5435','3304ef15-3d54-d514-0f52-5f469cfc6b17','d84dbda5-0435-3268-4441-5c6e1dec53c2',75,'2020-08-27 17:49:53',0),('c4500dcf-7a22-e5e0-3572-5f469cd3d263','3304ef15-3d54-d514-0f52-5f469cfc6b17','e64249f4-d1f2-0677-2be6-5c6e1d48a985',75,'2020-08-27 17:49:53',0),('c5510ecd-4c1c-8316-75c7-5f469c51cc26','3304ef15-3d54-d514-0f52-5f469cfc6b17','65807232-4fc2-aad5-52f8-5c6e1d60ad29',89,'2020-08-27 17:49:53',0),('c85600ca-b046-e330-27f6-5f469ccdff7b','3304ef15-3d54-d514-0f52-5f469cfc6b17','6c07fded-2f91-dcfd-0a5c-5c6e1d1e158f',-99,'2020-08-27 17:49:53',0),('c8a3516d-6cb2-087f-148c-5f469c456e0c','3304ef15-3d54-d514-0f52-5f469cfc6b17','access',89,'2020-08-27 17:49:53',0),('c913e85f-ef02-4e68-7c4b-5f469c2e00e0','3304ef15-3d54-d514-0f52-5f469cfc6b17','delete',-99,'2020-08-27 17:49:53',0),('ca16c9d4-b1b1-78ca-6701-5f469c73d6d8','3304ef15-3d54-d514-0f52-5f469cfc6b17','edit',75,'2020-08-27 17:49:53',0),('ca6b3317-4044-9cf0-55a7-5f469c594330','3304ef15-3d54-d514-0f52-5f469cfc6b17','export',75,'2020-08-27 17:49:53',0),('caa48095-0847-259e-f62f-5f469c0b7ca2','3304ef15-3d54-d514-0f52-5f469cfc6b17','import',90,'2020-08-27 17:49:53',0),('cadb2db1-e6f9-d440-fec9-5f469cc559d0','3304ef15-3d54-d514-0f52-5f469cfc6b17','list',75,'2020-08-27 17:49:53',0),('cb0a2c94-7f95-6da0-9b75-5f469c14f1f0','3304ef15-3d54-d514-0f52-5f469cfc6b17','massupdate',90,'2020-08-27 17:49:53',0),('cb592207-1de8-c7d1-8a61-5f469cef8990','3304ef15-3d54-d514-0f52-5f469cfc6b17','view',75,'2020-08-27 17:49:53',0),('cb8a74b6-326e-1cae-71b0-5f469cada6dc','3304ef15-3d54-d514-0f52-5f469cfc6b17','e4e294a9-b18d-ee54-645d-5c6e1dd1e8c6',89,'2020-08-27 17:49:53',0),('cbb73c15-b8b6-9667-7051-5f469c5e1ebb','3304ef15-3d54-d514-0f52-5f469cfc6b17','eb6ad108-3dbe-6c3f-6e09-5c6e1d42e30d',-99,'2020-08-27 17:49:53',0),('cbf49367-41f0-d930-a074-5f469c299f65','3304ef15-3d54-d514-0f52-5f469cfc6b17','e9c4c00e-5b31-7e5e-ed1c-5c6e1d771e86',75,'2020-08-27 17:49:53',0),('cc4a2611-3f38-185f-2c5b-5f469c4838ae','3304ef15-3d54-d514-0f52-5f469cfc6b17','eea809c6-5494-ebd8-ea2a-5c6e1d2a1bc3',75,'2020-08-27 17:49:53',0),('cc7f15f5-c656-0d21-5300-5f469c4a60d8','3304ef15-3d54-d514-0f52-5f469cfc6b17','ed085713-2fef-f3a6-d794-5c6e1d5116c5',90,'2020-08-27 17:49:53',0),('ccb2a422-8e26-4151-61b2-5f469c3deb17','3304ef15-3d54-d514-0f52-5f469cfc6b17','e81b1376-eea5-89f3-2af6-5c6e1d01ec62',75,'2020-08-27 17:49:53',0),('cce015e8-f0bd-8a2b-6ccc-5f469cc21734','3304ef15-3d54-d514-0f52-5f469cfc6b17','f04405bc-ba89-bfc6-c5a7-5c6e1db2a756',90,'2020-08-27 17:49:53',0),('cd050caf-1a10-b9fb-f921-5f469c62a62e','3304ef15-3d54-d514-0f52-5f469cfc6b17','6a6a210a-9b29-a92b-5117-5c6e1d3defe1',75,'2020-08-27 17:49:53',0),('cd1f67d3-3dda-401d-66a6-5f469c3da991','3304ef15-3d54-d514-0f52-5f469cfc6b17','e67f4d71-4b5b-0f8b-e816-5c6e1d6cbfdc',75,'2020-08-27 17:49:53',0),('cd650fba-cd52-e2f3-2a0d-5f469c6695c4','3304ef15-3d54-d514-0f52-5f469cfc6b17','9842d020-e157-2c63-4eca-5c6e1d7d10aa',89,'2020-08-27 17:49:53',0),('cd92e66d-918e-56ca-a947-5f469c478c67','3304ef15-3d54-d514-0f52-5f469cfc6b17','9eb0255f-c82c-8880-a4e3-5c6e1d003339',-99,'2020-08-27 17:49:53',0),('cdc1df07-648e-d0df-23da-5f469c1559da','3304ef15-3d54-d514-0f52-5f469cfc6b17','9d197071-f115-80c8-ddbe-5c6e1d19c577',75,'2020-08-27 17:49:53',0),('cdf80744-8079-e2b6-abb8-5f469c1e9ef0','3304ef15-3d54-d514-0f52-5f469cfc6b17','a263e131-bb1e-2f23-e69c-5c6e1db832c5',75,'2020-08-27 17:49:53',0),('ce4ed8b8-afb1-3911-f1ad-5f469c712d0c','3304ef15-3d54-d514-0f52-5f469cfc6b17','a053e50a-04cd-914e-fa5b-5c6e1d5600c1',90,'2020-08-27 17:49:53',0),('ce852c04-f9f9-8659-49be-5f469c41da0f','3304ef15-3d54-d514-0f52-5f469cfc6b17','9b8488a5-8b90-64fc-cc6a-5c6e1deafdb8',75,'2020-08-27 17:49:53',0),('cebc7175-8c3c-163f-de4a-5f469cad96bd','3304ef15-3d54-d514-0f52-5f469cfc6b17','a4160e91-0182-6aca-6626-5c6e1dd95415',90,'2020-08-27 17:49:53',0),('cef2d83e-ed79-9fda-4464-5f469c0aabb4','3304ef15-3d54-d514-0f52-5f469cfc6b17','99ed1f96-3c96-5191-aacb-5c6e1db0791e',75,'2020-08-27 17:49:53',0),('cf45e2c6-9b24-d02e-8fc1-5f469c3ff614','3304ef15-3d54-d514-0f52-5f469cfc6b17','2add29bf-94df-524f-816b-5c6e1df26606',89,'2020-08-27 17:49:53',0),('cf79d53a-8d6d-ceb7-f27f-5f469c382b76','3304ef15-3d54-d514-0f52-5f469cfc6b17','31f99891-140f-0f15-8210-5c6e1da1c586',-99,'2020-08-27 17:49:53',0),('cfb275b5-dd95-03bb-d566-5f469c2e1a0c','3304ef15-3d54-d514-0f52-5f469cfc6b17','304550da-9a68-1c5d-70b9-5c6e1d0308c6',75,'2020-08-27 17:49:53',0),('cfca07b9-cac7-12e8-de1c-5f469c815cae','3304ef15-3d54-d514-0f52-5f469cfc6b17','6f3ef736-d1b8-d29a-31bd-5c6e1d45d1ce',75,'2020-08-27 17:49:53',0),('cfee180d-23e5-d681-536a-5f469c763bec','3304ef15-3d54-d514-0f52-5f469cfc6b17','352832c1-13a4-6df3-47b0-5c6e1d95364b',75,'2020-08-27 17:49:53',0),('d0489f18-a721-e73f-3e18-5f469c5bb5e8','3304ef15-3d54-d514-0f52-5f469cfc6b17','33977c10-585e-e3e4-76b3-5c6e1d29503c',90,'2020-08-27 17:49:53',0),('d0835071-78d2-0233-03f9-5f469c28b782','3304ef15-3d54-d514-0f52-5f469cfc6b17','2e4a26b7-6b7b-55c2-11db-5c6e1d2fe155',75,'2020-08-27 17:49:53',0),('d0b8044c-6163-ef9d-6e52-5f469cef5875','3304ef15-3d54-d514-0f52-5f469cfc6b17','36b9e4fa-f86d-727e-fca7-5c6e1de8e18d',90,'2020-08-27 17:49:53',0),('d0fc63fd-ce8e-90a8-efee-5f469c275c86','3304ef15-3d54-d514-0f52-5f469cfc6b17','2c76062e-78dc-b6f6-36df-5c6e1d53d9e3',75,'2020-08-27 17:49:53',0),('d14987d5-4d72-9409-61fe-5f469cbfd749','3304ef15-3d54-d514-0f52-5f469cfc6b17','40a52983-5401-23cb-9769-5c6e1dc74de0',89,'2020-08-27 17:49:53',0),('d17acdb3-5e90-e7e5-d993-5f469ce74efb','3304ef15-3d54-d514-0f52-5f469cfc6b17','471273a9-50cb-30b6-1772-5c6e1d4750af',-99,'2020-08-27 17:49:53',0),('d1abee3e-4675-673d-06f2-5f469c241ec4','3304ef15-3d54-d514-0f52-5f469cfc6b17','4574d08f-6962-323a-85a8-5c6e1d5971c5',75,'2020-08-27 17:49:53',0),('d1e3c106-deb8-9cbe-9894-5f469c3e27c1','3304ef15-3d54-d514-0f52-5f469cfc6b17','4a57e684-d9e3-d7f2-83fd-5c6e1d1dc552',75,'2020-08-27 17:49:53',0),('d238e9f7-cb7f-abfc-7535-5f469cf5b01e','3304ef15-3d54-d514-0f52-5f469cfc6b17','48bad485-ae5a-7893-e9d9-5c6e1df2102e',90,'2020-08-27 17:49:53',0),('d26f9bca-0bb9-c1a7-dbb6-5f469cd321a5','3304ef15-3d54-d514-0f52-5f469cfc6b17','43dcc714-a359-b934-1906-5c6e1d8e6d82',75,'2020-08-27 17:49:53',0),('d2a45c31-605a-6b75-d395-5f469c8b210f','3304ef15-3d54-d514-0f52-5f469cfc6b17','4bf15a14-a85b-47a9-8349-5c6e1d3aa8fe',90,'2020-08-27 17:49:53',0),('d2da9ecb-3935-5810-d704-5f469c5fecc9','3304ef15-3d54-d514-0f52-5f469cfc6b17','42465f76-4843-8654-22a2-5c6e1db7e678',75,'2020-08-27 17:49:53',0),('d32805eb-4815-27e5-ea72-5f469cd781bc','3304ef15-3d54-d514-0f52-5f469cfc6b17','6da34898-2906-0ca9-4d9f-5c6e1d4a8e11',90,'2020-08-27 17:49:53',0),('d32daf06-5c98-a413-7185-5f469c1b71e7','3304ef15-3d54-d514-0f52-5f469cfc6b17','1b958cf9-ae40-438b-f8c5-5c6e1da93153',89,'2020-08-27 17:49:53',0),('d36196ea-1e68-ab2c-dbcc-5f469c116c81','3304ef15-3d54-d514-0f52-5f469cfc6b17','232e2ca6-d75b-da80-9dfe-5c6e1de86dd1',-99,'2020-08-27 17:49:53',0),('d394ed6a-a6ad-3715-82d8-5f469c825e1a','3304ef15-3d54-d514-0f52-5f469cfc6b17','2169ee86-765e-bd5e-d6ca-5c6e1d9ae49c',75,'2020-08-27 17:49:53',0),('d3ccc6f6-7103-b842-0d93-5f469c2f4c99','3304ef15-3d54-d514-0f52-5f469cfc6b17','26f60559-7804-b3e2-2c5a-5c6e1d08216c',75,'2020-08-27 17:49:53',0),('d4218359-69af-7bc5-c367-5f469c7a1c22','3304ef15-3d54-d514-0f52-5f469cfc6b17','2507c934-350d-93e7-6009-5c6e1d1a4ebf',90,'2020-08-27 17:49:53',0),('d454407d-2140-29c2-c403-5f469cabd553','3304ef15-3d54-d514-0f52-5f469cfc6b17','1f915e87-b617-1952-6ea3-5c6e1d890550',75,'2020-08-27 17:49:53',0),('d4974cfd-59de-ea73-b275-5f469c8be5c4','3304ef15-3d54-d514-0f52-5f469cfc6b17','28ff06fc-14b0-98ec-737f-5c6e1d816cc7',90,'2020-08-27 17:49:53',0),('d4cf04fb-e403-0d32-ef42-5f469c3b8e13','3304ef15-3d54-d514-0f52-5f469cfc6b17','1d94faaf-6dae-f882-8d0e-5c6e1dee4687',75,'2020-08-27 17:49:53',0),('d51cb0ba-524a-f783-6ec5-5f469cf92a61','3304ef15-3d54-d514-0f52-5f469cfc6b17','78bc3e90-a6f2-9b33-2c95-5c6e1d8c1327',89,'2020-08-27 17:49:53',0),('d54d795d-24e7-827d-e46e-5f469c0d3c7d','3304ef15-3d54-d514-0f52-5f469cfc6b17','7f2175cd-db29-b4f8-3753-5c6e1d46bab5',-99,'2020-08-27 17:49:53',0),('d57c8a98-745f-8047-6e6e-5f469cff7462','3304ef15-3d54-d514-0f52-5f469cfc6b17','7d8d7347-86e8-73a0-1348-5c6e1d86190b',75,'2020-08-27 17:49:53',0),('d5af5e0c-df29-55df-bd85-5f469c05d174','3304ef15-3d54-d514-0f52-5f469cfc6b17','824f5a15-53b1-4755-d578-5c6e1d0a0440',75,'2020-08-27 17:49:53',0),('d5f981ea-adf3-fd0c-a656-5f469cc1fa7d','3304ef15-3d54-d514-0f52-5f469cfc6b17','80b641fa-5eda-2510-e682-5c6e1dd24ed4',90,'2020-08-27 17:49:53',0),('d60a0c93-d8e2-16e7-d23d-5f469c8c294e','3304ef15-3d54-d514-0f52-5f469cfc6b17','68c51dc8-b17b-d512-dec0-5c6e1dfbb5b2',75,'2020-08-27 17:49:53',0),('d62a8a2d-7229-2b37-d1dc-5f469c3908ab','3304ef15-3d54-d514-0f52-5f469cfc6b17','7bf1dc51-d297-b1c0-0c9a-5c6e1d3778ab',75,'2020-08-27 17:49:53',0),('d65f71cd-3130-b3bb-8ea7-5f469c2fba8b','3304ef15-3d54-d514-0f52-5f469cfc6b17','83e9503e-4d1d-2241-0894-5c6e1d2d5ad3',90,'2020-08-27 17:49:53',0),('d6906099-4e41-b19f-4ce6-5f469c7176ca','3304ef15-3d54-d514-0f52-5f469cfc6b17','7a597959-caf4-8477-c2f8-5c6e1d6c7ccd',75,'2020-08-27 17:49:53',0),('d6be0c4b-6b63-5838-ad5b-5f469c2f8667','3304ef15-3d54-d514-0f52-5f469cfc6b17','ac28a8b7-049a-fc4a-e4de-5c6e1d1423a4',89,'2020-08-27 17:49:53',0),('d7077a23-3cc2-c33e-cec7-5f469ceec082','3304ef15-3d54-d514-0f52-5f469cfc6b17','b2e44f1d-2865-1aec-18e0-5c6e1d495fff',-99,'2020-08-27 17:49:53',0),('d7336f22-4ba5-e103-df6f-5f469c4f4e80','3304ef15-3d54-d514-0f52-5f469cfc6b17','b1418e90-c9f7-20cb-7771-5c6e1d6469aa',75,'2020-08-27 17:49:53',0),('d7633cfd-b5b3-aaa9-1cc1-5f469c4551ea','3304ef15-3d54-d514-0f52-5f469cfc6b17','b6261178-cb2f-3c4d-318a-5c6e1d71d570',75,'2020-08-27 17:49:53',0),('d7916214-d681-7d27-d172-5f469c419788','3304ef15-3d54-d514-0f52-5f469cfc6b17','b4845172-d53e-fede-4687-5c6e1da11cd4',90,'2020-08-27 17:49:53',0),('d7c1ee70-c482-e525-d04c-5f469c96e353','3304ef15-3d54-d514-0f52-5f469cfc6b17','af9542f4-0edd-1926-48b7-5c6e1d8f9893',75,'2020-08-27 17:49:53',0),('d814fda1-1006-d5dd-4847-5f469c3b7ad0','3304ef15-3d54-d514-0f52-5f469cfc6b17','b7c94ce9-b840-0559-bbb4-5c6e1d293842',90,'2020-08-27 17:49:53',0),('d84192d3-bed8-9a0b-1a28-5f469c544180','3304ef15-3d54-d514-0f52-5f469cfc6b17','ade60ebc-4744-3eb6-a3e1-5c6e1d94a89b',75,'2020-08-27 17:49:53',0),('d86bdebd-9631-049c-e361-5f469cd51c66','3304ef15-3d54-d514-0f52-5f469cfc6b17','be604b96-6736-7e81-6d15-5c6e1d5dab3e',89,'2020-08-27 17:49:53',0),('d89895ee-37ae-65a0-a357-5f469cd11e20','3304ef15-3d54-d514-0f52-5f469cfc6b17','c58c2f62-5135-6531-5629-5c6e1d7895af',-99,'2020-08-27 17:49:53',0),('d8c4e2ef-edf1-cc31-53f6-5f469c58817d','3304ef15-3d54-d514-0f52-5f469cfc6b17','c3a67730-0e9c-4380-7ead-5c6e1df50457',75,'2020-08-27 17:49:53',0),('d8e30ac6-2e12-e9d4-3d6f-5f469c621b0b','3304ef15-3d54-d514-0f52-5f469cfc6b17','70d8f01c-fd25-458c-37da-5c6e1de3ee6d',90,'2020-08-27 17:49:53',0),('d91025f7-abfc-d2fc-09ab-5f469c186597','3304ef15-3d54-d514-0f52-5f469cfc6b17','c95817c6-d1e2-2fea-2350-5c6e1d6aa3fe',75,'2020-08-27 17:49:53',0),('d93ca67f-6d94-4ce4-3b55-5f469ce61e8d','3304ef15-3d54-d514-0f52-5f469cfc6b17','c76b891a-5d75-28e1-7037-5c6e1d34fc61',90,'2020-08-27 17:49:53',0),('d9699b3a-335f-e9e5-f813-5f469cbee112','3304ef15-3d54-d514-0f52-5f469cfc6b17','c1cc01ac-a509-0cfd-5218-5c6e1d18f2cd',75,'2020-08-27 17:49:53',0),('d9963260-4e3c-b664-902d-5f469c29232a','3304ef15-3d54-d514-0f52-5f469cfc6b17','cb44e564-ea4c-4625-16e8-5c6e1d0a6ed8',90,'2020-08-27 17:49:53',0),('d9cb008e-cb7c-cde7-3ae7-5f469c7870d2','3304ef15-3d54-d514-0f52-5f469cfc6b17','c0085f74-6446-7bec-6b0f-5c6e1d2d824b',75,'2020-08-27 17:49:53',0),('da19774e-b13d-c23f-9a32-5f469cb11e44','3304ef15-3d54-d514-0f52-5f469cfc6b17','f1e9306e-dc78-fda0-504a-5c6e1d689e6e',89,'2020-08-27 17:49:53',0),('da4ffdf7-72ca-23e4-4aa5-5f469c28e7ee','3304ef15-3d54-d514-0f52-5f469cfc6b17','419b0ff8-8bd3-c967-1f1c-5c6e1d4e8fb9',-99,'2020-08-27 17:49:53',0),('da8471c9-520b-9f67-7010-5f469cc86837','3304ef15-3d54-d514-0f52-5f469cfc6b17','28140bad-21e3-58b2-5adf-5c6e1d2a37ec',75,'2020-08-27 17:49:53',0),('dab02241-7cb7-f2d8-39fb-5f469c75e284','3304ef15-3d54-d514-0f52-5f469cfc6b17','746106cb-0c40-293e-cdbe-5c6e1deb825e',75,'2020-08-27 17:49:53',0),('daf805af-28d7-62e0-882a-5f469c5906f4','3304ef15-3d54-d514-0f52-5f469cfc6b17','5b420296-9b9b-f422-444c-5c6e1d94f411',90,'2020-08-27 17:49:53',0),('db3aafdc-996a-c206-fe2e-5f469c01f121','3304ef15-3d54-d514-0f52-5f469cfc6b17','ef70059e-eb96-9eb7-e071-5c6e1d41766b',75,'2020-08-27 17:49:53',0),('db6e03ac-e8f1-e720-a183-5f469caec9eb','3304ef15-3d54-d514-0f52-5f469cfc6b17','8d830de8-f2d0-21b8-09cb-5c6e1d131f85',90,'2020-08-27 17:49:53',0),('dba47625-95c4-d98d-b0bf-5f469c3f3a18','3304ef15-3d54-d514-0f52-5f469cfc6b17','f384474f-3e33-9f86-bdf5-5c6e1d2ca1a1',75,'2020-08-27 17:49:53',0),('dbe82fa3-f8c5-dc48-3ae0-5f469c781754','3304ef15-3d54-d514-0f52-5f469cfc6b17','ce4558d9-c83f-511d-b67b-5c6e1dff0232',89,'2020-08-27 17:49:53',0),('dc1b2f8e-1f44-fe0c-e9d0-5f469c9306d5','3304ef15-3d54-d514-0f52-5f469cfc6b17','d50a007b-3691-7ac6-b4b7-5c6e1dce4545',-99,'2020-08-27 17:49:53',0),('dc4e0e39-890f-b0dd-7cdb-5f469c84e5f6','3304ef15-3d54-d514-0f52-5f469cfc6b17','d363a602-e3a2-4f7b-15d5-5c6e1d33de0e',75,'2020-08-27 17:49:53',0),('dc7a00c9-fc6f-1f84-f63d-5f469c560e81','3304ef15-3d54-d514-0f52-5f469cfc6b17','d86004d0-2d45-49de-b8ab-5c6e1d5b4881',75,'2020-08-27 17:49:53',0),('dcaaff90-1e7d-0c39-b332-5f469cfb13ff','3304ef15-3d54-d514-0f52-5f469cfc6b17','d6b13e01-c92c-477f-5199-5c6e1d956e32',90,'2020-08-27 17:49:53',0),('dcf45c72-a9fc-b0c4-7da4-5f469c74a7da','3304ef15-3d54-d514-0f52-5f469cfc6b17','d1b5a193-7424-54fb-d5d1-5c6e1d7ab7e5',75,'2020-08-27 17:49:53',0),('dd22653f-0b32-061e-a489-5f469ca145de','3304ef15-3d54-d514-0f52-5f469cfc6b17','da0a2316-6c11-6f86-04fb-5c6e1d12145f',90,'2020-08-27 17:49:53',0),('dd5194da-bb5d-5d56-478b-5f469c168c02','3304ef15-3d54-d514-0f52-5f469cfc6b17','d0031d26-18da-f0d3-e7d3-5c6e1d89ca77',75,'2020-08-27 17:49:53',0),('dd7f73c2-9caa-34b6-2834-5f469cf4c613','3304ef15-3d54-d514-0f52-5f469cfc6b17','e5c20eac-6559-6531-9fc1-5c6e1d07319c',89,'2020-08-27 17:49:53',0),('ddb14e94-def6-e88d-584f-5f469c0ba972','3304ef15-3d54-d514-0f52-5f469cfc6b17','15c2ddf5-d59a-f00b-e3fd-5c6e1dd8862b',-99,'2020-08-27 17:49:53',0),('ddfa0ade-4d2d-de4c-29f1-5f469c260d19','3304ef15-3d54-d514-0f52-5f469cfc6b17','672c3b21-fc37-cdc7-6c9b-5c6e1d56963c',75,'2020-08-27 17:49:53',0),('ddfa5587-6c74-3a9a-2ba2-5f469c48f359','3304ef15-3d54-d514-0f52-5f469cfc6b17','13c35bd9-edd9-8d1d-9148-5c6e1dd73cbe',75,'2020-08-27 17:49:53',0),('de27e10b-1911-bf8c-d03a-5f469c7bfa10','3304ef15-3d54-d514-0f52-5f469cfc6b17','195d5fd1-dcbe-f8c7-791b-5c6e1d95e515',75,'2020-08-27 17:49:53',0),('de5460a9-2b81-1c83-3c0c-5f469c99afd2','3304ef15-3d54-d514-0f52-5f469cfc6b17','17825d0b-9ae5-f818-8af6-5c6e1d44db2b',90,'2020-08-27 17:49:53',0),('de952736-c8c5-8d1d-b909-5f469c61b67b','3304ef15-3d54-d514-0f52-5f469cfc6b17','11e2c447-3cdb-c380-663b-5c6e1dbbbdef',75,'2020-08-27 17:49:53',0),('deddb426-4f8c-9448-96ba-5f469c9144ee','3304ef15-3d54-d514-0f52-5f469cfc6b17','1b120fe0-ce1d-c39b-038c-5c6e1d886a54',90,'2020-08-27 17:49:53',0),('df084aec-4138-0858-2c73-5f469cf32b5c','3304ef15-3d54-d514-0f52-5f469cfc6b17','101ad241-f96c-8797-b838-5c6e1df463e2',75,'2020-08-27 17:49:53',0),('df34a1b6-7108-e835-997a-5f469c2f3eab','3304ef15-3d54-d514-0f52-5f469cfc6b17','abd4ec8c-c93c-c1e6-def1-5c6e1ddf4c9a',89,'2020-08-27 17:49:53',0),('df63196b-61f8-f493-4584-5f469ce34b7e','3304ef15-3d54-d514-0f52-5f469cfc6b17','b28d3942-a8ed-93bf-21cd-5c6e1d0f6ae2',-99,'2020-08-27 17:49:53',0),('df9271d3-1ad2-67f9-2101-5f469c2d5b4a','3304ef15-3d54-d514-0f52-5f469cfc6b17','b0d731ac-5b19-327b-4e2a-5c6e1d3ad23b',75,'2020-08-27 17:49:53',0),('dfd8a690-dd42-6851-6d12-5f469c05d83e','3304ef15-3d54-d514-0f52-5f469cfc6b17','b5ca1507-2676-e260-08b9-5c6e1da38a6f',75,'2020-08-27 17:49:53',0),('e006daa3-cd70-ac3a-07bd-5f469c53e069','3304ef15-3d54-d514-0f52-5f469cfc6b17','b42943d9-bb9e-b64f-e274-5c6e1d1e54b2',90,'2020-08-27 17:49:53',0),('e03367a3-5726-e3ac-e705-5f469c11865c','3304ef15-3d54-d514-0f52-5f469cfc6b17','af31a49b-acde-7cfa-34f5-5c6e1dc79084',75,'2020-08-27 17:49:53',0),('e06b6bbd-a8b2-5370-c933-5f469c7f2d67','3304ef15-3d54-d514-0f52-5f469cfc6b17','b7652289-d6e5-41c6-96df-5c6e1dfb2f8a',90,'2020-08-27 17:49:53',0),('e096c634-008f-8db8-d679-5f469c5513ea','3304ef15-3d54-d514-0f52-5f469cfc6b17','ad856238-c136-2afa-933c-5c6e1d1cdec2',75,'2020-08-27 17:49:53',0),('e0a60b0a-fb1f-8ffe-ae8d-5f469cc47fb1','3304ef15-3d54-d514-0f52-5f469cfc6b17','2bb8a788-d9b2-317e-ee27-5c6e1d99e1e0',89,'2020-08-27 17:49:53',0),('e0e262b2-6f9a-8444-3c85-5f469c66360e','3304ef15-3d54-d514-0f52-5f469cfc6b17','e4fa58f2-e15d-1cf0-7dc4-5c6e1da17057',89,'2020-08-27 17:49:53',0),('e10f099c-d4fb-1c9f-2bdb-5f469c3fbbda','3304ef15-3d54-d514-0f52-5f469cfc6b17','eb68a252-8b3f-8d0e-b715-5c6e1ddb6365',-99,'2020-08-27 17:49:53',0),('e13d1874-72bf-38ca-bf6f-5f469c2940d3','3304ef15-3d54-d514-0f52-5f469cfc6b17','e9d42efb-bea8-e95b-c776-5c6e1d3696e6',75,'2020-08-27 17:49:53',0),('e16b7cb7-0910-97e1-537f-5f469c9a9fef','3304ef15-3d54-d514-0f52-5f469cfc6b17','ee981ca9-fd8d-da61-03ed-5c6e1d57bf84',75,'2020-08-27 17:49:53',0),('e1ce4226-61c8-4905-447c-5f469cc10491','3304ef15-3d54-d514-0f52-5f469cfc6b17','ecfefd1b-1800-f44f-1632-5c6e1db114d0',90,'2020-08-27 17:49:53',0),('e1fd8200-59bc-724a-354c-5f469c77ff1b','3304ef15-3d54-d514-0f52-5f469cfc6b17','e83709f6-9fef-96ff-7942-5c6e1d98ed5c',75,'2020-08-27 17:49:53',0),('e22f31f9-a189-2a2c-4a87-5f469c36738a','3304ef15-3d54-d514-0f52-5f469cfc6b17','f037714b-c744-9311-3833-5c6e1d91bac5',90,'2020-08-27 17:49:53',0),('e29262cf-76ac-35c8-a903-5f469c31a4d2','3304ef15-3d54-d514-0f52-5f469cfc6b17','e69dd3d3-0eab-3022-3735-5c6e1d14a076',75,'2020-08-27 17:49:53',0),('e2de2e98-4844-1a83-9b33-5f469cad4e78','3304ef15-3d54-d514-0f52-5f469cfc6b17','1b163909-2912-40f9-8708-5c6e1dad4ee9',89,'2020-08-27 17:49:53',0),('e30a4dfd-5041-c671-4800-5f469cb9617c','3304ef15-3d54-d514-0f52-5f469cfc6b17','21a4b930-240a-495d-674b-5c6e1da71eb7',-99,'2020-08-27 17:49:53',0),('e33854db-00c7-cde1-7ff8-5f469c13276d','3304ef15-3d54-d514-0f52-5f469cfc6b17','200a1c61-6924-8cd8-5af7-5c6e1d640527',75,'2020-08-27 17:49:53',0),('e36eaa67-dcc9-6dcb-98be-5f469cf50ec6','3304ef15-3d54-d514-0f52-5f469cfc6b17','24d68f78-a84f-a13d-7f4a-5c6e1d934bac',75,'2020-08-27 17:49:53',0),('e3a20200-6398-14d8-f363-5f469cd6752f','3304ef15-3d54-d514-0f52-5f469cfc6b17','35351234-40a5-72c4-4de3-5c6e1dacbb2e',-99,'2020-08-27 17:49:53',0),('e3c78c25-e545-2506-79c1-5f469cfc256b','3304ef15-3d54-d514-0f52-5f469cfc6b17','233a9995-db1a-2078-e61f-5c6e1df9a25d',90,'2020-08-27 17:49:53',0),('e3ff5564-1727-0471-b59c-5f469c284378','3304ef15-3d54-d514-0f52-5f469cfc6b17','1e6a83a2-afae-db99-ec4f-5c6e1d752ed9',75,'2020-08-27 17:49:53',0),('e437c4e0-aeba-4743-b8cc-5f469c1a13e2','3304ef15-3d54-d514-0f52-5f469cfc6b17','26783917-465d-4805-eb95-5c6e1d251635',90,'2020-08-27 17:49:53',0),('e47249c5-319f-ed99-9b69-5f469ce01860','3304ef15-3d54-d514-0f52-5f469cfc6b17','1cd1d38b-fce9-a981-1846-5c6e1d132836',75,'2020-08-27 17:49:53',0),('e4cfa92c-010e-7ba2-f23f-5f469c8b24b2','3304ef15-3d54-d514-0f52-5f469cfc6b17','af161148-77d1-f897-76a8-5c6e1d79ad82',89,'2020-08-27 17:49:53',0),('e50817d7-69ae-8b69-63ee-5f469ce68665','3304ef15-3d54-d514-0f52-5f469cfc6b17','b5b23ad8-066c-bd1f-1756-5c6e1dda06a1',-99,'2020-08-27 17:49:53',0),('e5435e65-156d-c9ca-2e85-5f469c03f529','3304ef15-3d54-d514-0f52-5f469cfc6b17','b40800b6-5ceb-b18f-2021-5c6e1ddf1c71',75,'2020-08-27 17:49:53',0),('e57d4ef1-1bbf-b0c6-9520-5f469c400422','3304ef15-3d54-d514-0f52-5f469cfc6b17','b8f6333c-264a-d56c-f3f7-5c6e1d6717b5',75,'2020-08-27 17:49:53',0),('e5dbe04b-d074-6420-4fb9-5f469c002470','3304ef15-3d54-d514-0f52-5f469cfc6b17','b755c057-7846-ef4e-48a4-5c6e1d5b0de2',90,'2020-08-27 17:49:53',0),('e6122170-b04f-023e-8c63-5f469c14bd8d','3304ef15-3d54-d514-0f52-5f469cfc6b17','b262f627-0c6c-c476-450e-5c6e1d7af1a7',75,'2020-08-27 17:49:53',0),('e6474f99-33d4-a6f0-386d-5f469c108a13','3304ef15-3d54-d514-0f52-5f469cfc6b17','baa3bd6a-799c-c7cb-8167-5c6e1de0778f',90,'2020-08-27 17:49:53',0),('e6980780-64d2-12ca-2c5e-5f469cd64acf','3304ef15-3d54-d514-0f52-5f469cfc6b17','338337d8-861f-20ab-eb8a-5c6e1d0794c7',75,'2020-08-27 17:49:53',0),('e6b47d29-4d7c-2b93-8efb-5f469cdfc05b','3304ef15-3d54-d514-0f52-5f469cfc6b17','b0c29e2e-087d-18f2-5b3c-5c6e1d67fc2d',75,'2020-08-27 17:49:53',0),('e6ec36a6-d1ac-1ea1-2f3e-5f469ce2367b','3304ef15-3d54-d514-0f52-5f469cfc6b17','52c2ed19-3cfd-a2a8-3cce-5c6e1d04c3d8',89,'2020-08-27 17:49:53',0),('e7300088-5372-1c93-7d57-5f469c05ba32','3304ef15-3d54-d514-0f52-5f469cfc6b17','593e1378-f97b-72e9-4a72-5c6e1d9089b4',-99,'2020-08-27 17:49:53',0),('e7709a53-a7af-5dc9-dac6-5f469c3ef516','3304ef15-3d54-d514-0f52-5f469cfc6b17','57a40c89-305e-8dc2-31a9-5c6e1d9922d3',75,'2020-08-27 17:49:53',0),('e7cb7893-8a4a-baba-888e-5f469cd48a8b','3304ef15-3d54-d514-0f52-5f469cfc6b17','5c7c1f01-024e-a89d-9299-5c6e1d9b9c91',75,'2020-08-27 17:49:53',0),('e8039799-9873-7d02-0ac4-5f469c646471','3304ef15-3d54-d514-0f52-5f469cfc6b17','5ad89177-2751-9788-e516-5c6e1d29c5ae',90,'2020-08-27 17:49:53',0),('e8406023-cf06-0e5f-4f05-5f469c03bdad','3304ef15-3d54-d514-0f52-5f469cfc6b17','5603f923-25a8-fdd2-328c-5c6e1de852fd',75,'2020-08-27 17:49:53',0),('e8a0d22c-ef0b-b3e2-9c3e-5f469c6ecb04','3304ef15-3d54-d514-0f52-5f469cfc6b17','5e1b71ea-7c87-c530-779a-5c6e1d9e0d17',90,'2020-08-27 17:49:53',0),('e8e62887-3a58-7429-4ea9-5f469ce5a3b9','3304ef15-3d54-d514-0f52-5f469cfc6b17','54651337-2a36-a3e0-cc3f-5c6e1dd7ff25',75,'2020-08-27 17:49:53',0),('e91dd667-17e0-24ec-4c31-5f469cd6690a','3304ef15-3d54-d514-0f52-5f469cfc6b17','5a116d0a-93ce-3f38-6b3c-5c6e1ddfb588',89,'2020-08-27 17:49:53',0),('e9460f92-9e7b-89ad-3727-5f469cbb118b','3304ef15-3d54-d514-0f52-5f469cfc6b17','3889a69a-c00b-8f99-585b-5c6e1d8b38cb',75,'2020-08-27 17:49:53',0),('e95880b1-ec9a-6fd3-6103-5f469c6c2e84','3304ef15-3d54-d514-0f52-5f469cfc6b17','60a6e40c-cd7c-6300-f332-5c6e1df4ba1d',-99,'2020-08-27 17:49:53',0),('e9b34c88-cd08-8e93-34c3-5f469c1ca14b','3304ef15-3d54-d514-0f52-5f469cfc6b17','5efb35f8-4d02-ff48-a0a1-5c6e1d8bd05a',75,'2020-08-27 17:49:53',0),('e9e8f8b5-b234-bd98-cbdf-5f469c2bc020','3304ef15-3d54-d514-0f52-5f469cfc6b17','6431abe6-fb9d-097a-5dc9-5c6e1d9b476d',75,'2020-08-27 17:49:53',0),('ea1e9690-dc8e-abc3-8801-5f469c1b9cbd','3304ef15-3d54-d514-0f52-5f469cfc6b17','6280ad76-9e85-2b59-5565-5c6e1daf4e8d',90,'2020-08-27 17:49:53',0),('eaea29d5-024f-cad6-c66c-5f469cfad180','3304ef15-3d54-d514-0f52-5f469cfc6b17','5d5e2201-0772-eea0-a538-5c6e1d9fbe40',75,'2020-08-27 17:49:53',0),('eb21bf6e-5102-9e45-3ab5-5f469cb5d9b7','3304ef15-3d54-d514-0f52-5f469cfc6b17','660bd7c0-0c8f-0395-4bc7-5c6e1d57fdbb',90,'2020-08-27 17:49:53',0),('eb4f521e-f72c-8698-6db9-5f469c2371b2','3304ef15-3d54-d514-0f52-5f469cfc6b17','5bbba79c-a0f1-e71e-ea73-5c6e1da0a9e2',75,'2020-08-27 17:49:53',0),('eb988a2f-7faa-9ab3-bbd2-5f469ce75e10','3304ef15-3d54-d514-0f52-5f469cfc6b17','96a6e41c-fd1d-2d14-efec-5c6e1ddcd204',89,'2020-08-27 17:49:53',0),('ebc987fd-4278-9cd2-3d2b-5f469ca045ff','3304ef15-3d54-d514-0f52-5f469cfc6b17','9e36c5da-d99c-4575-d041-5c6e1d1932a5',-99,'2020-08-27 17:49:53',0),('ebf49011-022e-d630-d822-5f469c86b6da','3304ef15-3d54-d514-0f52-5f469cfc6b17','9c9068c8-2889-4237-c4da-5c6e1d5fd6e0',75,'2020-08-27 17:49:53',0),('ec204a42-0aff-af5b-b4e7-5f469cc7e3a1','3304ef15-3d54-d514-0f52-5f469cfc6b17','a19598bd-ecb8-b87c-42dc-5c6e1d69db8f',75,'2020-08-27 17:49:53',0),('ec4c9cc3-5b3a-3a67-9e52-5f469caa97c2','3304ef15-3d54-d514-0f52-5f469cfc6b17','9febc0e8-571c-b6d9-033d-5c6e1dadd332',90,'2020-08-27 17:49:53',0),('ec974bc4-0358-1aeb-40e4-5f469c3f0a10','3304ef15-3d54-d514-0f52-5f469cfc6b17','9a9dea65-c49d-a612-b77a-5c6e1d860eb8',75,'2020-08-27 17:49:53',0),('ecc82a4d-90e2-4313-7986-5f469c5e1888','3304ef15-3d54-d514-0f52-5f469cfc6b17','a53a56c9-6206-9bb2-70a8-5c6e1dd996a7',90,'2020-08-27 17:49:53',0),('ecf4a4e9-6b99-f600-ca42-5f469cd7554d','3304ef15-3d54-d514-0f52-5f469cfc6b17','98b4e4cc-d8f5-e99a-60d3-5c6e1dc7787c',75,'2020-08-27 17:49:53',0),('ed1dd264-980f-37d7-de9e-5f469c1086b2','3304ef15-3d54-d514-0f52-5f469cfc6b17','82fe24f4-3ce6-a0a9-227d-5c6e1df8c135',89,'2020-08-27 17:49:53',0),('ed474ecb-ba46-c5a8-2061-5f469cef1ca5','3304ef15-3d54-d514-0f52-5f469cfc6b17','8a32b4ee-2ae3-f3c0-9e82-5c6e1d41a51c',-99,'2020-08-27 17:49:53',0),('ed929bc9-380b-6a50-6eb7-5f469c4e17e2','3304ef15-3d54-d514-0f52-5f469cfc6b17','88712337-2eb8-7e5f-af4c-5c6e1d94bd75',75,'2020-08-27 17:49:53',0),('edbeb2f1-3cfe-f2d7-e452-5f469c1585fc','3304ef15-3d54-d514-0f52-5f469cfc6b17','8da7244b-1fd0-52bc-6c70-5c6e1d8b6bee',75,'2020-08-27 17:49:53',0),('eded8068-2696-5c31-b8ec-5f469cd72f05','3304ef15-3d54-d514-0f52-5f469cfc6b17','8be9f5f8-465c-c9ea-d1bb-5c6e1dbecaff',90,'2020-08-27 17:49:53',0),('ee3a8fbe-08ad-a51a-877f-5f469c1ca3e0','3304ef15-3d54-d514-0f52-5f469cfc6b17','86b6537f-e557-3610-62c3-5c6e1dd8169c',75,'2020-08-27 17:49:53',0),('ee887744-31c4-563b-6eb4-5f469cecbe1d','3304ef15-3d54-d514-0f52-5f469cfc6b17','8f7040ab-20b9-1e82-89d6-5c6e1d5f3de2',90,'2020-08-27 17:49:53',0),('eeb96b71-ee5a-20b4-b18b-5f469cc9b943','3304ef15-3d54-d514-0f52-5f469cfc6b17','84d457d8-2ba5-48a6-5a1b-5c6e1d453ed0',75,'2020-08-27 17:49:53',0),('eeeac41f-239f-0193-3c3d-5f469cfcddc9','3304ef15-3d54-d514-0f52-5f469cfc6b17','23909eed-24e9-1383-65a6-5c6e1d4dc04a',89,'2020-08-27 17:49:53',0),('ef168c05-1a96-89eb-9588-5f469c3157f7','3304ef15-3d54-d514-0f52-5f469cfc6b17','2cf32cca-b9b9-409b-bd3e-5c6e1dfd8c3b',-99,'2020-08-27 17:49:53',0),('ef616297-c8eb-d77f-dc2a-5f469c1550de','3304ef15-3d54-d514-0f52-5f469cfc6b17','2ae367b2-742d-5ab8-1ea2-5c6e1db4adef',75,'2020-08-27 17:49:53',0),('ef8e61de-beb4-dac2-eb90-5f469c6580e9','3304ef15-3d54-d514-0f52-5f469cfc6b17','30b24203-0540-da78-baa4-5c6e1dd6ddce',75,'2020-08-27 17:49:53',0),('eff37753-cccd-58f2-e47b-5f469c335d0d','3304ef15-3d54-d514-0f52-5f469cfc6b17','2ebfecbf-817e-c0f6-494c-5c6e1d375b9d',90,'2020-08-27 17:49:53',0),('f02248c1-787e-fe3a-a936-5f469c2960e4','3304ef15-3d54-d514-0f52-5f469cfc6b17','273511c0-4119-a9e5-de4c-5c6e1d59d6dc',75,'2020-08-27 17:49:53',0),('f0674c5b-8b2c-ae2e-1ee9-5f469c4cec84','3304ef15-3d54-d514-0f52-5f469cfc6b17','3273e589-c6a6-c63a-53fc-5c6e1d017b90',90,'2020-08-27 17:49:53',0),('f092e86d-c08a-23fb-d33b-5f469cbf3410','3304ef15-3d54-d514-0f52-5f469cfc6b17','255f689c-30fa-9743-6b97-5c6e1d16820d',75,'2020-08-27 17:49:53',0),('f0c16398-e29f-f7e8-8107-5f469cdd66a9','3304ef15-3d54-d514-0f52-5f469cfc6b17','2d4c98ca-5d2e-6f0a-17fc-5c6e1d9fd7b6',89,'2020-08-27 17:49:53',0),('f0ec9952-b978-27c6-67bf-5f469c632442','3304ef15-3d54-d514-0f52-5f469cfc6b17','3516982c-ed8e-543f-3813-5c6e1d0bdfbd',-99,'2020-08-27 17:49:53',0),('f11a7fde-ab99-8be6-71ef-5f469c344ec0','3304ef15-3d54-d514-0f52-5f469cfc6b17','337b8214-09d6-962e-b12b-5c6e1deefec8',75,'2020-08-27 17:49:53',0),('f1615f94-9fe7-f340-fc9b-5f469c3850ce','3304ef15-3d54-d514-0f52-5f469cfc6b17','3852582d-e33f-175c-76b2-5c6e1d893051',75,'2020-08-27 17:49:53',0),('f1a56443-2c75-e4e2-67b8-5f469c8185c6','3304ef15-3d54-d514-0f52-5f469cfc6b17','36b63ee1-ab68-5783-63da-5c6e1de6b4e5',90,'2020-08-27 17:49:53',0),('f1d03842-be3e-dc18-5e87-5f469cc2adaa','3304ef15-3d54-d514-0f52-5f469cfc6b17','31cac616-99da-efcf-aa0f-5c6e1d3afd35',75,'2020-08-27 17:49:53',0),('f1fc1825-04da-9b93-3c01-5f469c048fa3','3304ef15-3d54-d514-0f52-5f469cfc6b17','39f45967-6ace-927e-9b01-5c6e1daa3eda',90,'2020-08-27 17:49:53',0),('f2294806-499a-7ec8-bac5-5f469c1850f4','3304ef15-3d54-d514-0f52-5f469cfc6b17','2efef55f-d14c-224a-1cc6-5c6e1d1d53cf',75,'2020-08-27 17:49:53',0),('f273e499-abae-176e-7d6c-5f469cf182b0','3304ef15-3d54-d514-0f52-5f469cfc6b17','4096a9d2-bd4a-eb01-973f-5c6e1d5a45e2',89,'2020-08-27 17:49:53',0),('f2a036e2-5a16-4529-b93c-5f469c26ab6b','3304ef15-3d54-d514-0f52-5f469cfc6b17','47288bba-4c05-a22a-8c63-5c6e1d3dc05c',-99,'2020-08-27 17:49:53',0),('f2caadef-701d-e447-9a81-5f469c623ae6','3304ef15-3d54-d514-0f52-5f469cfc6b17','458a5983-6996-5853-fe72-5c6e1d222fc5',75,'2020-08-27 17:49:53',0),('f2cb0902-3b6a-9034-18cc-5f469c618607','3304ef15-3d54-d514-0f52-5f469cfc6b17','36e993eb-3592-761c-b5b0-5c6e1d8e0760',90,'2020-08-27 17:49:53',0),('f2f738ab-bc03-338a-e3ee-5f469cf67eba','3304ef15-3d54-d514-0f52-5f469cfc6b17','4a657e01-e916-f669-a099-5c6e1d1e5567',75,'2020-08-27 17:49:53',0),('f32349ec-f870-e65f-0de8-5f469c062715','3304ef15-3d54-d514-0f52-5f469cfc6b17','48c6d617-d7a0-6fd8-c0a4-5c6e1d8a861c',90,'2020-08-27 17:49:53',0),('f3700cef-4931-7209-65e4-5f469cfd2736','3304ef15-3d54-d514-0f52-5f469cfc6b17','43e09d57-a48f-6375-1331-5c6e1d87f59a',75,'2020-08-27 17:49:53',0),('f3980861-e9eb-d743-5cf3-5f469cab6701','3304ef15-3d54-d514-0f52-5f469cfc6b17','4c0a451b-4439-f8d8-2592-5c6e1d1fe047',90,'2020-08-27 17:49:53',0),('f3c82995-c2ec-480e-49e6-5f469c241beb','3304ef15-3d54-d514-0f52-5f469cfc6b17','42416112-83ca-03b7-352a-5c6e1db0f5e5',75,'2020-08-27 17:49:53',0),('f3ff667f-9b00-a4aa-18b0-5f469c68546a','3304ef15-3d54-d514-0f52-5f469cfc6b17','db873033-8e64-6de0-193d-5c6e1d2f494c',89,'2020-08-27 17:49:53',0),('f5ee0c7a-356f-7cd1-5c5b-5f469c04b52f','3304ef15-3d54-d514-0f52-5f469cfc6b17','31c44a2d-47fa-fb4f-0b01-5c6e1d201232',75,'2020-08-27 17:49:53',0),('f95603ea-ebc1-7c5e-9668-5f469c81c375','3304ef15-3d54-d514-0f52-5f469cfc6b17','3a22e344-99f3-e402-1503-5c6e1daeca94',90,'2020-08-27 17:49:53',0),('fe160cc7-7f1e-c44d-56fe-5f469ca7b8b2','3304ef15-3d54-d514-0f52-5f469cfc6b17','2f3392ef-1a9a-f99d-c414-5c6e1d868dc9',75,'2020-08-27 17:49:53',0);
/*!40000 ALTER TABLE `acl_roles_actions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `acl_roles_users`
--

DROP TABLE IF EXISTS `acl_roles_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acl_roles_users` (
  `id` varchar(36) NOT NULL,
  `role_id` varchar(36) DEFAULT NULL,
  `user_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_aclrole_id` (`role_id`),
  KEY `idx_acluser_id` (`user_id`),
  KEY `idx_aclrole_user` (`role_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acl_roles_users`
--

LOCK TABLES `acl_roles_users` WRITE;
/*!40000 ALTER TABLE `acl_roles_users` DISABLE KEYS */;
INSERT INTO `acl_roles_users` VALUES ('b721b318-9de6-57e3-d9e1-5f47f1d59be2','69d63f4e-88ef-58c9-0a28-5f469b0dc456','b70243be-ca4c-b256-aff8-5f46980acf9f','2020-08-27 17:47:13',0),('c73f37d6-bd0a-4e62-fa59-5f469f918a16','3304ef15-3d54-d514-0f52-5f469cfc6b17','8d544213-bcac-b962-fc13-5f4698c75700','2020-08-26 17:44:19',0),('c7f650a2-0e40-395b-5e1c-5f469f677616','3304ef15-3d54-d514-0f52-5f469cfc6b17','1674d634-fb3a-8d8b-34c1-5f4698fed125','2020-08-26 17:44:19',0);
/*!40000 ALTER TABLE `acl_roles_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `address_book`
--

DROP TABLE IF EXISTS `address_book`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `address_book` (
  `assigned_user_id` char(36) NOT NULL,
  `bean` varchar(50) DEFAULT NULL,
  `bean_id` char(36) NOT NULL,
  KEY `ab_user_bean_idx` (`assigned_user_id`,`bean`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `address_book`
--

LOCK TABLES `address_book` WRITE;
/*!40000 ALTER TABLE `address_book` DISABLE KEYS */;
/*!40000 ALTER TABLE `address_book` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alerts`
--

DROP TABLE IF EXISTS `alerts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alerts` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `is_read` tinyint(1) DEFAULT NULL,
  `target_module` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `url_redirect` varchar(255) DEFAULT NULL,
  `reminder_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alerts`
--

LOCK TABLES `alerts` WRITE;
/*!40000 ALTER TABLE `alerts` DISABLE KEYS */;
/*!40000 ALTER TABLE `alerts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `am_projecttemplates`
--

DROP TABLE IF EXISTS `am_projecttemplates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_projecttemplates` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `status` varchar(100) DEFAULT 'Draft',
  `priority` varchar(100) DEFAULT 'High',
  `override_business_hours` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `am_projecttemplates`
--

LOCK TABLES `am_projecttemplates` WRITE;
/*!40000 ALTER TABLE `am_projecttemplates` DISABLE KEYS */;
/*!40000 ALTER TABLE `am_projecttemplates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `am_projecttemplates_audit`
--

DROP TABLE IF EXISTS `am_projecttemplates_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_projecttemplates_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_am_projecttemplates_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `am_projecttemplates_audit`
--

LOCK TABLES `am_projecttemplates_audit` WRITE;
/*!40000 ALTER TABLE `am_projecttemplates_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `am_projecttemplates_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `am_projecttemplates_contacts_1_c`
--

DROP TABLE IF EXISTS `am_projecttemplates_contacts_1_c`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_projecttemplates_contacts_1_c` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `am_projecttemplates_ida` varchar(36) DEFAULT NULL,
  `contacts_idb` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `am_projecttemplates_contacts_1_alt` (`am_projecttemplates_ida`,`contacts_idb`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `am_projecttemplates_contacts_1_c`
--

LOCK TABLES `am_projecttemplates_contacts_1_c` WRITE;
/*!40000 ALTER TABLE `am_projecttemplates_contacts_1_c` DISABLE KEYS */;
/*!40000 ALTER TABLE `am_projecttemplates_contacts_1_c` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `am_projecttemplates_project_1_c`
--

DROP TABLE IF EXISTS `am_projecttemplates_project_1_c`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_projecttemplates_project_1_c` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `am_projecttemplates_project_1am_projecttemplates_ida` varchar(36) DEFAULT NULL,
  `am_projecttemplates_project_1project_idb` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `am_projecttemplates_project_1_ida1` (`am_projecttemplates_project_1am_projecttemplates_ida`),
  KEY `am_projecttemplates_project_1_alt` (`am_projecttemplates_project_1project_idb`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `am_projecttemplates_project_1_c`
--

LOCK TABLES `am_projecttemplates_project_1_c` WRITE;
/*!40000 ALTER TABLE `am_projecttemplates_project_1_c` DISABLE KEYS */;
/*!40000 ALTER TABLE `am_projecttemplates_project_1_c` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `am_projecttemplates_users_1_c`
--

DROP TABLE IF EXISTS `am_projecttemplates_users_1_c`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_projecttemplates_users_1_c` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `am_projecttemplates_ida` varchar(36) DEFAULT NULL,
  `users_idb` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `am_projecttemplates_users_1_alt` (`am_projecttemplates_ida`,`users_idb`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `am_projecttemplates_users_1_c`
--

LOCK TABLES `am_projecttemplates_users_1_c` WRITE;
/*!40000 ALTER TABLE `am_projecttemplates_users_1_c` DISABLE KEYS */;
/*!40000 ALTER TABLE `am_projecttemplates_users_1_c` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `am_tasktemplates`
--

DROP TABLE IF EXISTS `am_tasktemplates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_tasktemplates` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `status` varchar(100) DEFAULT 'Not Started',
  `priority` varchar(100) DEFAULT 'High',
  `percent_complete` int(255) DEFAULT '0',
  `predecessors` int(255) DEFAULT NULL,
  `milestone_flag` tinyint(1) DEFAULT '0',
  `relationship_type` varchar(100) DEFAULT 'FS',
  `task_number` int(255) DEFAULT NULL,
  `order_number` int(255) DEFAULT NULL,
  `estimated_effort` int(255) DEFAULT NULL,
  `utilization` varchar(100) DEFAULT '0',
  `duration` int(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `am_tasktemplates`
--

LOCK TABLES `am_tasktemplates` WRITE;
/*!40000 ALTER TABLE `am_tasktemplates` DISABLE KEYS */;
/*!40000 ALTER TABLE `am_tasktemplates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `am_tasktemplates_am_projecttemplates_c`
--

DROP TABLE IF EXISTS `am_tasktemplates_am_projecttemplates_c`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_tasktemplates_am_projecttemplates_c` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `am_tasktemplates_am_projecttemplatesam_projecttemplates_ida` varchar(36) DEFAULT NULL,
  `am_tasktemplates_am_projecttemplatesam_tasktemplates_idb` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `am_tasktemplates_am_projecttemplates_ida1` (`am_tasktemplates_am_projecttemplatesam_projecttemplates_ida`),
  KEY `am_tasktemplates_am_projecttemplates_alt` (`am_tasktemplates_am_projecttemplatesam_tasktemplates_idb`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `am_tasktemplates_am_projecttemplates_c`
--

LOCK TABLES `am_tasktemplates_am_projecttemplates_c` WRITE;
/*!40000 ALTER TABLE `am_tasktemplates_am_projecttemplates_c` DISABLE KEYS */;
/*!40000 ALTER TABLE `am_tasktemplates_am_projecttemplates_c` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `am_tasktemplates_audit`
--

DROP TABLE IF EXISTS `am_tasktemplates_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `am_tasktemplates_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_am_tasktemplates_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `am_tasktemplates_audit`
--

LOCK TABLES `am_tasktemplates_audit` WRITE;
/*!40000 ALTER TABLE `am_tasktemplates_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `am_tasktemplates_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aobh_businesshours`
--

DROP TABLE IF EXISTS `aobh_businesshours`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aobh_businesshours` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `opening_hours` varchar(100) DEFAULT '1',
  `closing_hours` varchar(100) DEFAULT '1',
  `open_status` tinyint(1) DEFAULT NULL,
  `day` varchar(100) DEFAULT 'monday',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aobh_businesshours`
--

LOCK TABLES `aobh_businesshours` WRITE;
/*!40000 ALTER TABLE `aobh_businesshours` DISABLE KEYS */;
/*!40000 ALTER TABLE `aobh_businesshours` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aod_index`
--

DROP TABLE IF EXISTS `aod_index`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aod_index` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `last_optimised` datetime DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aod_index`
--

LOCK TABLES `aod_index` WRITE;
/*!40000 ALTER TABLE `aod_index` DISABLE KEYS */;
INSERT INTO `aod_index` VALUES ('1','Index','2019-02-21 03:40:01','2019-02-21 03:40:01','1','1',NULL,0,NULL,NULL,'modules/AOD_Index/Index/Index');
/*!40000 ALTER TABLE `aod_index` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aod_index_audit`
--

DROP TABLE IF EXISTS `aod_index_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aod_index_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_aod_index_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aod_index_audit`
--

LOCK TABLES `aod_index_audit` WRITE;
/*!40000 ALTER TABLE `aod_index_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `aod_index_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aod_indexevent`
--

DROP TABLE IF EXISTS `aod_indexevent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aod_indexevent` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `error` varchar(255) DEFAULT NULL,
  `success` tinyint(1) DEFAULT '0',
  `record_id` char(36) DEFAULT NULL,
  `record_module` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_record_module` (`record_module`),
  KEY `idx_record_id` (`record_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aod_indexevent`
--

LOCK TABLES `aod_indexevent` WRITE;
/*!40000 ALTER TABLE `aod_indexevent` DISABLE KEYS */;
INSERT INTO `aod_indexevent` VALUES ('18e44097-3f1d-f32a-d59c-5f44fa4a45ff','Nutri Test 4','2020-08-25 11:46:11','2020-08-28 09:24:33','1','1',NULL,0,'',NULL,1,'cf6e07ce-3763-a98c-69d1-5f44fa63cfd9','Opportunities'),('1a524cf8-4435-9f1f-92f7-5f3fadf403f9','Pharma Company','2020-08-21 11:18:40','2020-08-21 11:18:40','1','1',NULL,0,NULL,NULL,1,'c4910e0c-08a6-3d7d-d073-5f3fad5bff46','Accounts'),('1c0702be-9bc4-8cf5-4ea2-5f452d86a1f9','Test','2020-08-25 15:26:18','2020-08-25 15:27:28','1','1',NULL,0,'',NULL,1,'17a201b9-7f91-87b1-31b8-5f452d31bdd0','Tasks'),('1f12f213-db03-43e6-6db6-5f48db7eb453','Test Opp 2','2020-08-28 10:25:25','2020-08-28 10:25:25','8d544213-bcac-b962-fc13-5f4698c75700','8d544213-bcac-b962-fc13-5f4698c75700',NULL,0,NULL,NULL,1,'12c3340b-0f62-8d28-b800-5f48db0951f9','Opportunities'),('22871a9d-be3b-c124-b61a-5f3fadd817d5','Nutri Test 1','2020-08-21 11:19:51','2020-08-28 09:24:21','1','1',NULL,0,'',NULL,1,'170ab7e4-7129-fd99-34c8-5f3fad24bc03','Opportunities'),('2c3c64e7-0207-7e81-ee6e-5f48dda747ca','Test Opp 3','2020-08-28 10:31:57','2020-08-28 10:31:57','1674d634-fb3a-8d8b-34c1-5f4698fed125','1674d634-fb3a-8d8b-34c1-5f4698fed125',NULL,0,NULL,NULL,1,'1ee05743-c112-d3a1-163a-5f48dd532edf','Opportunities'),('3a53fb23-f0ea-592b-828a-5f43bf8b8df0','Susie Morgan','2020-08-24 13:24:51','2020-08-24 13:24:51','1','1',NULL,0,NULL,NULL,1,'2aab764c-a53d-2a20-e944-5f43bff03c55','Leads'),('405ded1f-ef05-889c-433f-5f4697385b69','Pfizer Global','2020-08-26 17:10:44','2020-08-26 17:18:16','1','1',NULL,0,'',NULL,1,'2978e526-e27b-1ce0-3822-5f46974d0157','Accounts'),('488c5094-0937-960f-e30a-5f48ddc20635','Tim China','2020-08-28 10:32:56','2020-08-28 10:32:56','1674d634-fb3a-8d8b-34c1-5f4698fed125','1674d634-fb3a-8d8b-34c1-5f4698fed125',NULL,0,NULL,NULL,1,'3977c53e-104f-b7c1-2ca8-5f48dd5f8c58','Leads'),('4dd7ea2f-d859-1dbc-beb9-5f48dcc41559','Sandra China','2020-08-28 10:30:05','2020-08-28 10:30:05','1674d634-fb3a-8d8b-34c1-5f4698fed125','1674d634-fb3a-8d8b-34c1-5f4698fed125',NULL,0,NULL,NULL,1,'3898ea01-ac05-7e9f-4dce-5f48dcaf2e89','Contacts'),('4de8133f-ee90-6739-a4f4-5f46973fb8d3','Pfizer-China','2020-08-26 17:11:02','2020-08-26 17:18:16','1','1',NULL,0,'',NULL,1,'4712d4f2-e7f2-593a-8243-5f469751802e','Accounts'),('50419f55-74a4-3f25-c18f-5e90b93c1899','TimeSheet Tracker','2020-04-10 18:20:59','2020-04-10 18:20:59','dc26b995-5550-b848-6f73-5e90aa679348','dc26b995-5550-b848-6f73-5e90aa679348',NULL,0,'',NULL,1,'4a7f0912-73d6-6c8f-fc5f-5e90b97df24f','sp_timesheet_tracker'),('50f70c0c-cb47-9a2c-2064-5f48da1ea955','Bill India','2020-08-28 10:22:42','2020-08-28 10:22:42','8d544213-bcac-b962-fc13-5f4698c75700','8d544213-bcac-b962-fc13-5f4698c75700',NULL,0,NULL,NULL,1,'e826f898-71d8-241f-2c0b-5f48da608dc4','Leads'),('599460c0-e187-af16-78f3-5f43becbe193','Pharma Test 2','2020-08-24 13:21:35','2020-08-25 18:08:55','1','1',NULL,0,'',NULL,1,'4c12f4da-751b-5429-80ea-5f43bed3071f','Opportunities'),('5d192044-53c3-714e-6015-5e90b7393741','Timesheet Ninja Updates','2020-04-10 18:16:16','2020-04-10 18:16:16','dc26b995-5550-b848-6f73-5e90aa679348','dc26b995-5550-b848-6f73-5e90aa679348',NULL,0,NULL,NULL,1,'5371d12a-d61b-9d7a-97a4-5e90b71aecf9','Project'),('622c0463-7657-9e33-82ac-5e90b9536e14','Auto TimeSheet Record','2020-04-10 18:20:59','2020-04-10 18:20:59','dc26b995-5550-b848-6f73-5e90aa679348','dc26b995-5550-b848-6f73-5e90aa679348',NULL,0,NULL,NULL,1,'5bcae400-0c26-d06b-4d28-5e90b93cf701','sp_Auto_TimeSheet_Time_Tracker'),('684e561b-48ba-9aab-2d9c-5f48da108fc3','Steve India','2020-08-28 10:22:07','2020-08-28 10:22:07','8d544213-bcac-b962-fc13-5f4698c75700','8d544213-bcac-b962-fc13-5f4698c75700',NULL,0,NULL,NULL,1,'5200556a-9070-b927-1928-5f48da876d80','Contacts'),('771342af-ebe4-ebd3-256a-5f48dadc2fec','Test Opp 1','2020-08-28 10:20:11','2020-08-28 10:20:11','8d544213-bcac-b962-fc13-5f4698c75700','8d544213-bcac-b962-fc13-5f4698c75700',NULL,0,NULL,NULL,1,'68b8f5a5-cd40-decc-cebb-5f48da51a3f2','Opportunities'),('798cfae8-7936-52f9-1bb4-5e90b801187c','CRM Experts Online','2020-04-10 18:19:13','2020-04-10 18:19:13','dc26b995-5550-b848-6f73-5e90aa679348','dc26b995-5550-b848-6f73-5e90aa679348',NULL,0,NULL,NULL,1,'72cb1f4a-5f55-b910-e731-5e90b810ad4d','Accounts'),('85325768-3939-f7d8-e91e-5f44f87e4d99','Ms. Susan Morgan','2020-08-25 11:41:26','2020-08-25 11:41:26','1','1',NULL,0,NULL,NULL,1,'4da4191b-796e-24f8-313a-5f44f8418e64','Contacts'),('8d3f98dc-ba81-4be2-8621-5f48dbeae8db','Steve India','2020-08-28 10:23:31','2020-08-28 10:23:31','8d544213-bcac-b962-fc13-5f4698c75700','8d544213-bcac-b962-fc13-5f4698c75700',NULL,0,NULL,NULL,1,'5200556a-9070-b927-1928-5f48da876d80','Contacts'),('9ca40990-3181-e88c-1411-5f43bc12fdae','Nutri Test 3','2020-08-24 13:10:50','2020-08-28 09:24:24','1','1',NULL,0,'',NULL,1,'f10d6a43-2a2f-d743-d2ce-5f43bca15ede','Opportunities'),('a2221856-6125-3ec4-3c19-5f46977df91f','Pfizer-India','2020-08-26 17:11:18','2020-08-26 17:18:16','1','1',NULL,0,'',NULL,1,'9ae1967b-3fc4-6421-9d77-5f4697488f9e','Accounts'),('a4780348-6fca-a784-9222-5f43bc609124','Nutri Test 2','2020-08-24 13:10:19','2020-08-25 11:48:30','1','1',NULL,0,'',NULL,1,'9583611d-7521-f3a8-6bcd-5f43bc7dfab0','Opportunities'),('b30352e3-09ec-feac-87cb-5f48ddc3e2dc','Test Opp 4','2020-08-28 10:32:19','2020-08-28 10:32:19','1674d634-fb3a-8d8b-34c1-5f4698fed125','1674d634-fb3a-8d8b-34c1-5f4698fed125',NULL,0,NULL,NULL,1,'a57b6fdf-beda-0f2f-164f-5f48dd72c10f','Opportunities'),('b78c5d9e-9f44-62ca-b2e7-5f3fadaa2909','Pharma Test 1','2020-08-21 11:19:15','2020-08-24 15:03:45','1','1',NULL,0,'',NULL,1,'ab99004a-eee8-20c6-f636-5f3fad54c0d0','Opportunities'),('bc0516c4-3c00-7cbf-fc18-5f48dc6511fb','Sandra China','2020-08-28 10:30:41','2020-08-28 10:30:41','1674d634-fb3a-8d8b-34c1-5f4698fed125','1674d634-fb3a-8d8b-34c1-5f4698fed125',NULL,0,NULL,NULL,1,'3898ea01-ac05-7e9f-4dce-5f48dcaf2e89','Contacts'),('bf542178-fb83-e9d1-5b0d-5f48dadd2b7f','Note Test','2020-08-28 10:21:41','2020-08-28 10:21:41','8d544213-bcac-b962-fc13-5f4698c75700','8d544213-bcac-b962-fc13-5f4698c75700',NULL,0,NULL,NULL,1,'b39d0ada-3a30-6236-aefe-5f48dadba3c3','Notes'),('d6140d94-9a12-ff37-5f51-5e90b8b6845e','TimeSheet: 2020-04-06 - 2020-04-12','2020-04-10 18:17:21','2020-04-10 18:17:40','dc26b995-5550-b848-6f73-5e90aa679348','dc26b995-5550-b848-6f73-5e90aa679348',NULL,0,'',NULL,1,'d196b8b5-f91f-1483-270b-5e90b8abf374','sp_Timesheet'),('e2b807a8-4655-e664-1f30-5e90b8f2748a','Test Tasks','2020-04-10 18:16:58','2020-04-10 18:16:58','dc26b995-5550-b848-6f73-5e90aa679348','dc26b995-5550-b848-6f73-5e90aa679348',NULL,0,NULL,NULL,1,'dbd3eee3-9951-a692-80f9-5e90b88bab0f','ProjectTask'),('e8cb96d7-d220-7cae-d7a5-5e90b80b827a','Timecard: 2020-04-06 - 2020-04-12','2020-04-10 18:18:13','2020-04-10 18:18:13','dc26b995-5550-b848-6f73-5e90aa679348','dc26b995-5550-b848-6f73-5e90aa679348',NULL,0,NULL,NULL,1,'e496045a-6d8c-89b7-bc75-5e90b833aba5','sp_Timeoff_TimeCard');
/*!40000 ALTER TABLE `aod_indexevent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aod_indexevent_audit`
--

DROP TABLE IF EXISTS `aod_indexevent_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aod_indexevent_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_aod_indexevent_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aod_indexevent_audit`
--

LOCK TABLES `aod_indexevent_audit` WRITE;
/*!40000 ALTER TABLE `aod_indexevent_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `aod_indexevent_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aok_knowledge_base_categories`
--

DROP TABLE IF EXISTS `aok_knowledge_base_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aok_knowledge_base_categories` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aok_knowledge_base_categories`
--

LOCK TABLES `aok_knowledge_base_categories` WRITE;
/*!40000 ALTER TABLE `aok_knowledge_base_categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `aok_knowledge_base_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aok_knowledge_base_categories_audit`
--

DROP TABLE IF EXISTS `aok_knowledge_base_categories_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aok_knowledge_base_categories_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_aok_knowledge_base_categories_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aok_knowledge_base_categories_audit`
--

LOCK TABLES `aok_knowledge_base_categories_audit` WRITE;
/*!40000 ALTER TABLE `aok_knowledge_base_categories_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `aok_knowledge_base_categories_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aok_knowledgebase`
--

DROP TABLE IF EXISTS `aok_knowledgebase`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aok_knowledgebase` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `status` varchar(100) DEFAULT 'Draft',
  `revision` varchar(255) DEFAULT NULL,
  `additional_info` text,
  `user_id_c` char(36) DEFAULT NULL,
  `user_id1_c` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aok_knowledgebase`
--

LOCK TABLES `aok_knowledgebase` WRITE;
/*!40000 ALTER TABLE `aok_knowledgebase` DISABLE KEYS */;
/*!40000 ALTER TABLE `aok_knowledgebase` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aok_knowledgebase_audit`
--

DROP TABLE IF EXISTS `aok_knowledgebase_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aok_knowledgebase_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_aok_knowledgebase_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aok_knowledgebase_audit`
--

LOCK TABLES `aok_knowledgebase_audit` WRITE;
/*!40000 ALTER TABLE `aok_knowledgebase_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `aok_knowledgebase_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aok_knowledgebase_categories`
--

DROP TABLE IF EXISTS `aok_knowledgebase_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aok_knowledgebase_categories` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `aok_knowledgebase_id` varchar(36) DEFAULT NULL,
  `aok_knowledge_base_categories_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `aok_knowledgebase_categories_alt` (`aok_knowledgebase_id`,`aok_knowledge_base_categories_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aok_knowledgebase_categories`
--

LOCK TABLES `aok_knowledgebase_categories` WRITE;
/*!40000 ALTER TABLE `aok_knowledgebase_categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `aok_knowledgebase_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aop_case_events`
--

DROP TABLE IF EXISTS `aop_case_events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aop_case_events` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `case_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aop_case_events`
--

LOCK TABLES `aop_case_events` WRITE;
/*!40000 ALTER TABLE `aop_case_events` DISABLE KEYS */;
/*!40000 ALTER TABLE `aop_case_events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aop_case_events_audit`
--

DROP TABLE IF EXISTS `aop_case_events_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aop_case_events_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_aop_case_events_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aop_case_events_audit`
--

LOCK TABLES `aop_case_events_audit` WRITE;
/*!40000 ALTER TABLE `aop_case_events_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `aop_case_events_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aop_case_updates`
--

DROP TABLE IF EXISTS `aop_case_updates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aop_case_updates` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `case_id` char(36) DEFAULT NULL,
  `contact_id` char(36) DEFAULT NULL,
  `internal` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aop_case_updates`
--

LOCK TABLES `aop_case_updates` WRITE;
/*!40000 ALTER TABLE `aop_case_updates` DISABLE KEYS */;
/*!40000 ALTER TABLE `aop_case_updates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aop_case_updates_audit`
--

DROP TABLE IF EXISTS `aop_case_updates_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aop_case_updates_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_aop_case_updates_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aop_case_updates_audit`
--

LOCK TABLES `aop_case_updates_audit` WRITE;
/*!40000 ALTER TABLE `aop_case_updates_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `aop_case_updates_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aor_charts`
--

DROP TABLE IF EXISTS `aor_charts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aor_charts` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `aor_report_id` char(36) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `x_field` int(11) DEFAULT NULL,
  `y_field` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aor_charts`
--

LOCK TABLES `aor_charts` WRITE;
/*!40000 ALTER TABLE `aor_charts` DISABLE KEYS */;
/*!40000 ALTER TABLE `aor_charts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aor_conditions`
--

DROP TABLE IF EXISTS `aor_conditions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aor_conditions` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `aor_report_id` char(36) DEFAULT NULL,
  `condition_order` int(255) DEFAULT NULL,
  `logic_op` varchar(255) DEFAULT NULL,
  `parenthesis` varchar(255) DEFAULT NULL,
  `module_path` longtext,
  `field` varchar(100) DEFAULT NULL,
  `operator` varchar(100) DEFAULT NULL,
  `value_type` varchar(100) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `parameter` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `aor_conditions_index_report_id` (`aor_report_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aor_conditions`
--

LOCK TABLES `aor_conditions` WRITE;
/*!40000 ALTER TABLE `aor_conditions` DISABLE KEYS */;
/*!40000 ALTER TABLE `aor_conditions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aor_fields`
--

DROP TABLE IF EXISTS `aor_fields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aor_fields` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `aor_report_id` char(36) DEFAULT NULL,
  `field_order` int(255) DEFAULT NULL,
  `module_path` longtext,
  `field` varchar(100) DEFAULT NULL,
  `display` tinyint(1) DEFAULT NULL,
  `link` tinyint(1) DEFAULT NULL,
  `label` varchar(255) DEFAULT NULL,
  `field_function` varchar(100) DEFAULT NULL,
  `sort_by` varchar(100) DEFAULT NULL,
  `format` varchar(100) DEFAULT NULL,
  `total` varchar(100) DEFAULT NULL,
  `sort_order` varchar(100) DEFAULT NULL,
  `group_by` tinyint(1) DEFAULT NULL,
  `group_order` varchar(100) DEFAULT NULL,
  `group_display` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `aor_fields_index_report_id` (`aor_report_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aor_fields`
--

LOCK TABLES `aor_fields` WRITE;
/*!40000 ALTER TABLE `aor_fields` DISABLE KEYS */;
/*!40000 ALTER TABLE `aor_fields` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aor_reports`
--

DROP TABLE IF EXISTS `aor_reports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aor_reports` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `report_module` varchar(100) DEFAULT NULL,
  `graphs_per_row` int(11) DEFAULT '2',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aor_reports`
--

LOCK TABLES `aor_reports` WRITE;
/*!40000 ALTER TABLE `aor_reports` DISABLE KEYS */;
/*!40000 ALTER TABLE `aor_reports` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aor_reports_audit`
--

DROP TABLE IF EXISTS `aor_reports_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aor_reports_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_aor_reports_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aor_reports_audit`
--

LOCK TABLES `aor_reports_audit` WRITE;
/*!40000 ALTER TABLE `aor_reports_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `aor_reports_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aor_scheduled_reports`
--

DROP TABLE IF EXISTS `aor_scheduled_reports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aor_scheduled_reports` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `schedule` varchar(100) DEFAULT NULL,
  `last_run` datetime DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `email_recipients` longtext,
  `aor_report_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aor_scheduled_reports`
--

LOCK TABLES `aor_scheduled_reports` WRITE;
/*!40000 ALTER TABLE `aor_scheduled_reports` DISABLE KEYS */;
/*!40000 ALTER TABLE `aor_scheduled_reports` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aos_contracts`
--

DROP TABLE IF EXISTS `aos_contracts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aos_contracts` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `reference_code` varchar(255) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `total_contract_value` decimal(26,6) DEFAULT NULL,
  `total_contract_value_usdollar` decimal(26,6) DEFAULT NULL,
  `currency_id` char(36) DEFAULT NULL,
  `status` varchar(100) DEFAULT 'Not Started',
  `customer_signed_date` date DEFAULT NULL,
  `company_signed_date` date DEFAULT NULL,
  `renewal_reminder_date` datetime DEFAULT NULL,
  `contract_type` varchar(100) DEFAULT 'Type',
  `contract_account_id` char(36) DEFAULT NULL,
  `opportunity_id` char(36) DEFAULT NULL,
  `contact_id` char(36) DEFAULT NULL,
  `call_id` char(36) DEFAULT NULL,
  `total_amt` decimal(26,6) DEFAULT NULL,
  `total_amt_usdollar` decimal(26,6) DEFAULT NULL,
  `subtotal_amount` decimal(26,6) DEFAULT NULL,
  `subtotal_amount_usdollar` decimal(26,6) DEFAULT NULL,
  `discount_amount` decimal(26,6) DEFAULT NULL,
  `discount_amount_usdollar` decimal(26,6) DEFAULT NULL,
  `tax_amount` decimal(26,6) DEFAULT NULL,
  `tax_amount_usdollar` decimal(26,6) DEFAULT NULL,
  `shipping_amount` decimal(26,6) DEFAULT NULL,
  `shipping_amount_usdollar` decimal(26,6) DEFAULT NULL,
  `shipping_tax` varchar(100) DEFAULT NULL,
  `shipping_tax_amt` decimal(26,6) DEFAULT NULL,
  `shipping_tax_amt_usdollar` decimal(26,6) DEFAULT NULL,
  `total_amount` decimal(26,6) DEFAULT NULL,
  `total_amount_usdollar` decimal(26,6) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aos_contracts`
--

LOCK TABLES `aos_contracts` WRITE;
/*!40000 ALTER TABLE `aos_contracts` DISABLE KEYS */;
/*!40000 ALTER TABLE `aos_contracts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aos_contracts_audit`
--

DROP TABLE IF EXISTS `aos_contracts_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aos_contracts_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_aos_contracts_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aos_contracts_audit`
--

LOCK TABLES `aos_contracts_audit` WRITE;
/*!40000 ALTER TABLE `aos_contracts_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `aos_contracts_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aos_contracts_documents`
--

DROP TABLE IF EXISTS `aos_contracts_documents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aos_contracts_documents` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `aos_contracts_id` varchar(36) DEFAULT NULL,
  `documents_id` varchar(36) DEFAULT NULL,
  `document_revision_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `aos_contracts_documents_alt` (`aos_contracts_id`,`documents_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aos_contracts_documents`
--

LOCK TABLES `aos_contracts_documents` WRITE;
/*!40000 ALTER TABLE `aos_contracts_documents` DISABLE KEYS */;
/*!40000 ALTER TABLE `aos_contracts_documents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aos_invoices`
--

DROP TABLE IF EXISTS `aos_invoices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aos_invoices` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `billing_account_id` char(36) DEFAULT NULL,
  `billing_contact_id` char(36) DEFAULT NULL,
  `billing_address_street` varchar(150) DEFAULT NULL,
  `billing_address_city` varchar(100) DEFAULT NULL,
  `billing_address_state` varchar(100) DEFAULT NULL,
  `billing_address_postalcode` varchar(20) DEFAULT NULL,
  `billing_address_country` varchar(255) DEFAULT NULL,
  `shipping_address_street` varchar(150) DEFAULT NULL,
  `shipping_address_city` varchar(100) DEFAULT NULL,
  `shipping_address_state` varchar(100) DEFAULT NULL,
  `shipping_address_postalcode` varchar(20) DEFAULT NULL,
  `shipping_address_country` varchar(255) DEFAULT NULL,
  `number` int(11) NOT NULL,
  `total_amt` decimal(26,6) DEFAULT NULL,
  `total_amt_usdollar` decimal(26,6) DEFAULT NULL,
  `subtotal_amount` decimal(26,6) DEFAULT NULL,
  `subtotal_amount_usdollar` decimal(26,6) DEFAULT NULL,
  `discount_amount` decimal(26,6) DEFAULT NULL,
  `discount_amount_usdollar` decimal(26,6) DEFAULT NULL,
  `tax_amount` decimal(26,6) DEFAULT NULL,
  `tax_amount_usdollar` decimal(26,6) DEFAULT NULL,
  `shipping_amount` decimal(26,6) DEFAULT NULL,
  `shipping_amount_usdollar` decimal(26,6) DEFAULT NULL,
  `shipping_tax` varchar(100) DEFAULT NULL,
  `shipping_tax_amt` decimal(26,6) DEFAULT NULL,
  `shipping_tax_amt_usdollar` decimal(26,6) DEFAULT NULL,
  `total_amount` decimal(26,6) DEFAULT NULL,
  `total_amount_usdollar` decimal(26,6) DEFAULT NULL,
  `currency_id` char(36) DEFAULT NULL,
  `quote_number` int(11) DEFAULT NULL,
  `quote_date` date DEFAULT NULL,
  `invoice_date` date DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `template_ddown_c` text,
  `subtotal_tax_amount` decimal(26,6) DEFAULT NULL,
  `subtotal_tax_amount_usdollar` decimal(26,6) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aos_invoices`
--

LOCK TABLES `aos_invoices` WRITE;
/*!40000 ALTER TABLE `aos_invoices` DISABLE KEYS */;
/*!40000 ALTER TABLE `aos_invoices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aos_invoices_audit`
--

DROP TABLE IF EXISTS `aos_invoices_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aos_invoices_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_aos_invoices_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aos_invoices_audit`
--

LOCK TABLES `aos_invoices_audit` WRITE;
/*!40000 ALTER TABLE `aos_invoices_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `aos_invoices_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aos_line_item_groups`
--

DROP TABLE IF EXISTS `aos_line_item_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aos_line_item_groups` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `total_amt` decimal(26,6) DEFAULT NULL,
  `total_amt_usdollar` decimal(26,6) DEFAULT NULL,
  `discount_amount` decimal(26,6) DEFAULT NULL,
  `discount_amount_usdollar` decimal(26,6) DEFAULT NULL,
  `subtotal_amount` decimal(26,6) DEFAULT NULL,
  `subtotal_amount_usdollar` decimal(26,6) DEFAULT NULL,
  `tax_amount` decimal(26,6) DEFAULT NULL,
  `tax_amount_usdollar` decimal(26,6) DEFAULT NULL,
  `subtotal_tax_amount` decimal(26,6) DEFAULT NULL,
  `subtotal_tax_amount_usdollar` decimal(26,6) DEFAULT NULL,
  `total_amount` decimal(26,6) DEFAULT NULL,
  `total_amount_usdollar` decimal(26,6) DEFAULT NULL,
  `parent_type` varchar(100) DEFAULT NULL,
  `parent_id` char(36) DEFAULT NULL,
  `number` int(11) DEFAULT NULL,
  `currency_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aos_line_item_groups`
--

LOCK TABLES `aos_line_item_groups` WRITE;
/*!40000 ALTER TABLE `aos_line_item_groups` DISABLE KEYS */;
INSERT INTO `aos_line_item_groups` VALUES ('aa5f5b97-d7eb-dbbc-88de-5f4e4d90f82e','Test Tubes','2020-09-01 13:34:11','2020-09-01 13:34:11','1','1',NULL,0,'1',0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,NULL,0.000000,0.000000,0.000000,'AOS_Quotes','9a1eb78a-984b-2d82-b4f9-5f4e4d1aa6e6',1,'-99');
/*!40000 ALTER TABLE `aos_line_item_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aos_line_item_groups_audit`
--

DROP TABLE IF EXISTS `aos_line_item_groups_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aos_line_item_groups_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_aos_line_item_groups_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aos_line_item_groups_audit`
--

LOCK TABLES `aos_line_item_groups_audit` WRITE;
/*!40000 ALTER TABLE `aos_line_item_groups_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `aos_line_item_groups_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aos_pdf_templates`
--

DROP TABLE IF EXISTS `aos_pdf_templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aos_pdf_templates` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` longtext,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '1',
  `type` varchar(100) DEFAULT NULL,
  `pdfheader` text,
  `pdffooter` text,
  `margin_left` int(255) DEFAULT '15',
  `margin_right` int(255) DEFAULT '15',
  `margin_top` int(255) DEFAULT '16',
  `margin_bottom` int(255) DEFAULT '16',
  `margin_header` int(255) DEFAULT '9',
  `margin_footer` int(255) DEFAULT '9',
  `page_size` varchar(100) DEFAULT NULL,
  `orientation` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aos_pdf_templates`
--

LOCK TABLES `aos_pdf_templates` WRITE;
/*!40000 ALTER TABLE `aos_pdf_templates` DISABLE KEYS */;
/*!40000 ALTER TABLE `aos_pdf_templates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aos_pdf_templates_audit`
--

DROP TABLE IF EXISTS `aos_pdf_templates_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aos_pdf_templates_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_aos_pdf_templates_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aos_pdf_templates_audit`
--

LOCK TABLES `aos_pdf_templates_audit` WRITE;
/*!40000 ALTER TABLE `aos_pdf_templates_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `aos_pdf_templates_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aos_product_categories`
--

DROP TABLE IF EXISTS `aos_product_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aos_product_categories` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `is_parent` tinyint(1) DEFAULT '0',
  `parent_category_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aos_product_categories`
--

LOCK TABLES `aos_product_categories` WRITE;
/*!40000 ALTER TABLE `aos_product_categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `aos_product_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aos_product_categories_audit`
--

DROP TABLE IF EXISTS `aos_product_categories_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aos_product_categories_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_aos_product_categories_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aos_product_categories_audit`
--

LOCK TABLES `aos_product_categories_audit` WRITE;
/*!40000 ALTER TABLE `aos_product_categories_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `aos_product_categories_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aos_products`
--

DROP TABLE IF EXISTS `aos_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aos_products` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `maincode` varchar(100) DEFAULT 'XXXX',
  `part_number` varchar(25) DEFAULT NULL,
  `category` varchar(100) DEFAULT NULL,
  `type` varchar(100) DEFAULT 'Good',
  `cost` decimal(26,6) DEFAULT NULL,
  `cost_usdollar` decimal(26,6) DEFAULT NULL,
  `currency_id` char(36) DEFAULT NULL,
  `price` decimal(26,6) DEFAULT NULL,
  `price_usdollar` decimal(26,6) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `contact_id` char(36) DEFAULT NULL,
  `product_image` varchar(255) DEFAULT NULL,
  `aos_product_category_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aos_products`
--

LOCK TABLES `aos_products` WRITE;
/*!40000 ALTER TABLE `aos_products` DISABLE KEYS */;
/*!40000 ALTER TABLE `aos_products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aos_products_audit`
--

DROP TABLE IF EXISTS `aos_products_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aos_products_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_aos_products_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aos_products_audit`
--

LOCK TABLES `aos_products_audit` WRITE;
/*!40000 ALTER TABLE `aos_products_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `aos_products_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aos_products_quotes`
--

DROP TABLE IF EXISTS `aos_products_quotes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aos_products_quotes` (
  `id` char(36) NOT NULL,
  `name` text,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `currency_id` char(36) DEFAULT NULL,
  `part_number` varchar(255) DEFAULT NULL,
  `item_description` text,
  `number` int(11) DEFAULT NULL,
  `product_qty` decimal(18,4) DEFAULT NULL,
  `product_cost_price` decimal(26,6) DEFAULT NULL,
  `product_cost_price_usdollar` decimal(26,6) DEFAULT NULL,
  `product_list_price` decimal(26,6) DEFAULT NULL,
  `product_list_price_usdollar` decimal(26,6) DEFAULT NULL,
  `product_discount` decimal(26,6) DEFAULT NULL,
  `product_discount_usdollar` decimal(26,6) DEFAULT NULL,
  `product_discount_amount` decimal(26,6) DEFAULT NULL,
  `product_discount_amount_usdollar` decimal(26,6) DEFAULT NULL,
  `discount` varchar(255) DEFAULT 'Percentage',
  `product_unit_price` decimal(26,6) DEFAULT NULL,
  `product_unit_price_usdollar` decimal(26,6) DEFAULT NULL,
  `vat_amt` decimal(26,6) DEFAULT NULL,
  `vat_amt_usdollar` decimal(26,6) DEFAULT NULL,
  `product_total_price` decimal(26,6) DEFAULT NULL,
  `product_total_price_usdollar` decimal(26,6) DEFAULT NULL,
  `vat` varchar(100) DEFAULT '5.0',
  `parent_type` varchar(100) DEFAULT NULL,
  `parent_id` char(36) DEFAULT NULL,
  `product_id` char(36) DEFAULT NULL,
  `group_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_aospq_par_del` (`parent_id`,`parent_type`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aos_products_quotes`
--

LOCK TABLES `aos_products_quotes` WRITE;
/*!40000 ALTER TABLE `aos_products_quotes` DISABLE KEYS */;
/*!40000 ALTER TABLE `aos_products_quotes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aos_products_quotes_audit`
--

DROP TABLE IF EXISTS `aos_products_quotes_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aos_products_quotes_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_aos_products_quotes_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aos_products_quotes_audit`
--

LOCK TABLES `aos_products_quotes_audit` WRITE;
/*!40000 ALTER TABLE `aos_products_quotes_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `aos_products_quotes_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aos_quotes`
--

DROP TABLE IF EXISTS `aos_quotes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aos_quotes` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `approval_issue` text,
  `billing_account_id` char(36) DEFAULT NULL,
  `billing_contact_id` char(36) DEFAULT NULL,
  `billing_address_street` varchar(150) DEFAULT NULL,
  `billing_address_city` varchar(100) DEFAULT NULL,
  `billing_address_state` varchar(100) DEFAULT NULL,
  `billing_address_postalcode` varchar(20) DEFAULT NULL,
  `billing_address_country` varchar(255) DEFAULT NULL,
  `shipping_address_street` varchar(150) DEFAULT NULL,
  `shipping_address_city` varchar(100) DEFAULT NULL,
  `shipping_address_state` varchar(100) DEFAULT NULL,
  `shipping_address_postalcode` varchar(20) DEFAULT NULL,
  `shipping_address_country` varchar(255) DEFAULT NULL,
  `expiration` date DEFAULT NULL,
  `number` int(11) DEFAULT NULL,
  `opportunity_id` char(36) DEFAULT NULL,
  `template_ddown_c` text,
  `total_amt` decimal(26,6) DEFAULT NULL,
  `total_amt_usdollar` decimal(26,6) DEFAULT NULL,
  `subtotal_amount` decimal(26,6) DEFAULT NULL,
  `subtotal_amount_usdollar` decimal(26,6) DEFAULT NULL,
  `discount_amount` decimal(26,6) DEFAULT NULL,
  `discount_amount_usdollar` decimal(26,6) DEFAULT NULL,
  `tax_amount` decimal(26,6) DEFAULT NULL,
  `tax_amount_usdollar` decimal(26,6) DEFAULT NULL,
  `shipping_amount` decimal(26,6) DEFAULT NULL,
  `shipping_amount_usdollar` decimal(26,6) DEFAULT NULL,
  `shipping_tax` varchar(100) DEFAULT NULL,
  `shipping_tax_amt` decimal(26,6) DEFAULT NULL,
  `shipping_tax_amt_usdollar` decimal(26,6) DEFAULT NULL,
  `total_amount` decimal(26,6) DEFAULT NULL,
  `total_amount_usdollar` decimal(26,6) DEFAULT NULL,
  `currency_id` char(36) DEFAULT NULL,
  `stage` varchar(100) DEFAULT 'Draft',
  `term` varchar(100) DEFAULT NULL,
  `terms_c` text,
  `approval_status` varchar(100) DEFAULT NULL,
  `invoice_status` varchar(100) DEFAULT 'Not Invoiced',
  `subtotal_tax_amount` decimal(26,6) DEFAULT NULL,
  `subtotal_tax_amount_usdollar` decimal(26,6) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aos_quotes`
--

LOCK TABLES `aos_quotes` WRITE;
/*!40000 ALTER TABLE `aos_quotes` DISABLE KEYS */;
INSERT INTO `aos_quotes` VALUES ('9a1eb78a-984b-2d82-b4f9-5f4e4d1aa6e6','Quote Test','2020-09-01 13:34:11','2020-09-01 13:34:11','1','1',NULL,0,'1','','','','','','','','','','','','','','2020-10-01',1,'170ab7e4-7129-fd99-34c8-5f3fad24bc03',NULL,2121.000000,2121.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,'0.0',0.000000,0.000000,0.000000,0.000000,'-99','Draft','Net 15',NULL,'Approved','Not Invoiced',NULL,0.000000);
/*!40000 ALTER TABLE `aos_quotes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aos_quotes_aos_invoices_c`
--

DROP TABLE IF EXISTS `aos_quotes_aos_invoices_c`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aos_quotes_aos_invoices_c` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `aos_quotes77d9_quotes_ida` varchar(36) DEFAULT NULL,
  `aos_quotes6b83nvoices_idb` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `aos_quotes_aos_invoices_alt` (`aos_quotes77d9_quotes_ida`,`aos_quotes6b83nvoices_idb`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aos_quotes_aos_invoices_c`
--

LOCK TABLES `aos_quotes_aos_invoices_c` WRITE;
/*!40000 ALTER TABLE `aos_quotes_aos_invoices_c` DISABLE KEYS */;
/*!40000 ALTER TABLE `aos_quotes_aos_invoices_c` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aos_quotes_audit`
--

DROP TABLE IF EXISTS `aos_quotes_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aos_quotes_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_aos_quotes_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aos_quotes_audit`
--

LOCK TABLES `aos_quotes_audit` WRITE;
/*!40000 ALTER TABLE `aos_quotes_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `aos_quotes_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aos_quotes_os_contracts_c`
--

DROP TABLE IF EXISTS `aos_quotes_os_contracts_c`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aos_quotes_os_contracts_c` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `aos_quotese81e_quotes_ida` varchar(36) DEFAULT NULL,
  `aos_quotes4dc0ntracts_idb` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `aos_quotes_aos_contracts_alt` (`aos_quotese81e_quotes_ida`,`aos_quotes4dc0ntracts_idb`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aos_quotes_os_contracts_c`
--

LOCK TABLES `aos_quotes_os_contracts_c` WRITE;
/*!40000 ALTER TABLE `aos_quotes_os_contracts_c` DISABLE KEYS */;
/*!40000 ALTER TABLE `aos_quotes_os_contracts_c` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aos_quotes_project_c`
--

DROP TABLE IF EXISTS `aos_quotes_project_c`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aos_quotes_project_c` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `aos_quotes1112_quotes_ida` varchar(36) DEFAULT NULL,
  `aos_quotes7207project_idb` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `aos_quotes_project_alt` (`aos_quotes1112_quotes_ida`,`aos_quotes7207project_idb`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aos_quotes_project_c`
--

LOCK TABLES `aos_quotes_project_c` WRITE;
/*!40000 ALTER TABLE `aos_quotes_project_c` DISABLE KEYS */;
/*!40000 ALTER TABLE `aos_quotes_project_c` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aow_actions`
--

DROP TABLE IF EXISTS `aow_actions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aow_actions` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `aow_workflow_id` char(36) DEFAULT NULL,
  `action_order` int(255) DEFAULT NULL,
  `action` varchar(100) DEFAULT NULL,
  `parameters` longtext,
  PRIMARY KEY (`id`),
  KEY `aow_action_index_workflow_id` (`aow_workflow_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aow_actions`
--

LOCK TABLES `aow_actions` WRITE;
/*!40000 ALTER TABLE `aow_actions` DISABLE KEYS */;
/*!40000 ALTER TABLE `aow_actions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aow_conditions`
--

DROP TABLE IF EXISTS `aow_conditions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aow_conditions` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `aow_workflow_id` char(36) DEFAULT NULL,
  `condition_order` int(255) DEFAULT NULL,
  `module_path` longtext,
  `field` varchar(100) DEFAULT NULL,
  `operator` varchar(100) DEFAULT NULL,
  `value_type` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `aow_conditions_index_workflow_id` (`aow_workflow_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aow_conditions`
--

LOCK TABLES `aow_conditions` WRITE;
/*!40000 ALTER TABLE `aow_conditions` DISABLE KEYS */;
/*!40000 ALTER TABLE `aow_conditions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aow_processed`
--

DROP TABLE IF EXISTS `aow_processed`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aow_processed` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `aow_workflow_id` char(36) DEFAULT NULL,
  `parent_id` char(36) DEFAULT NULL,
  `parent_type` varchar(100) DEFAULT NULL,
  `status` varchar(100) DEFAULT 'Pending',
  PRIMARY KEY (`id`),
  KEY `aow_processed_index_workflow` (`aow_workflow_id`,`status`,`parent_id`,`deleted`),
  KEY `aow_processed_index_status` (`status`),
  KEY `aow_processed_index_workflow_id` (`aow_workflow_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aow_processed`
--

LOCK TABLES `aow_processed` WRITE;
/*!40000 ALTER TABLE `aow_processed` DISABLE KEYS */;
/*!40000 ALTER TABLE `aow_processed` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aow_processed_aow_actions`
--

DROP TABLE IF EXISTS `aow_processed_aow_actions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aow_processed_aow_actions` (
  `id` varchar(36) NOT NULL,
  `aow_processed_id` varchar(36) DEFAULT NULL,
  `aow_action_id` varchar(36) DEFAULT NULL,
  `status` varchar(36) DEFAULT 'Pending',
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_aow_processed_aow_actions` (`aow_processed_id`,`aow_action_id`),
  KEY `idx_actid_del_freid` (`aow_action_id`,`deleted`,`aow_processed_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aow_processed_aow_actions`
--

LOCK TABLES `aow_processed_aow_actions` WRITE;
/*!40000 ALTER TABLE `aow_processed_aow_actions` DISABLE KEYS */;
/*!40000 ALTER TABLE `aow_processed_aow_actions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aow_workflow`
--

DROP TABLE IF EXISTS `aow_workflow`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aow_workflow` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `flow_module` varchar(100) DEFAULT NULL,
  `flow_run_on` varchar(100) DEFAULT '0',
  `status` varchar(100) DEFAULT 'Active',
  `run_when` varchar(100) DEFAULT 'Always',
  `multiple_runs` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `aow_workflow_index_status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aow_workflow`
--

LOCK TABLES `aow_workflow` WRITE;
/*!40000 ALTER TABLE `aow_workflow` DISABLE KEYS */;
/*!40000 ALTER TABLE `aow_workflow` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aow_workflow_audit`
--

DROP TABLE IF EXISTS `aow_workflow_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aow_workflow_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_aow_workflow_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aow_workflow_audit`
--

LOCK TABLES `aow_workflow_audit` WRITE;
/*!40000 ALTER TABLE `aow_workflow_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `aow_workflow_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bugs`
--

DROP TABLE IF EXISTS `bugs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bugs` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `bug_number` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `priority` varchar(100) DEFAULT NULL,
  `resolution` varchar(255) DEFAULT NULL,
  `work_log` text,
  `found_in_release` varchar(255) DEFAULT NULL,
  `fixed_in_release` varchar(255) DEFAULT NULL,
  `source` varchar(255) DEFAULT NULL,
  `product_category` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `bugsnumk` (`bug_number`),
  KEY `bug_number` (`bug_number`),
  KEY `idx_bug_name` (`name`),
  KEY `idx_bugs_assigned_user` (`assigned_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bugs`
--

LOCK TABLES `bugs` WRITE;
/*!40000 ALTER TABLE `bugs` DISABLE KEYS */;
/*!40000 ALTER TABLE `bugs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bugs_audit`
--

DROP TABLE IF EXISTS `bugs_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bugs_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_bugs_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bugs_audit`
--

LOCK TABLES `bugs_audit` WRITE;
/*!40000 ALTER TABLE `bugs_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `bugs_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `calls`
--

DROP TABLE IF EXISTS `calls`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calls` (
  `id` char(36) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `duration_hours` int(2) DEFAULT NULL,
  `duration_minutes` int(2) DEFAULT NULL,
  `date_start` datetime DEFAULT NULL,
  `date_end` datetime DEFAULT NULL,
  `parent_type` varchar(255) DEFAULT NULL,
  `status` varchar(100) DEFAULT 'Planned',
  `direction` varchar(100) DEFAULT NULL,
  `parent_id` char(36) DEFAULT NULL,
  `reminder_time` int(11) DEFAULT '-1',
  `email_reminder_time` int(11) DEFAULT '-1',
  `email_reminder_sent` tinyint(1) DEFAULT '0',
  `outlook_id` varchar(255) DEFAULT NULL,
  `repeat_type` varchar(36) DEFAULT NULL,
  `repeat_interval` int(3) DEFAULT '1',
  `repeat_dow` varchar(7) DEFAULT NULL,
  `repeat_until` date DEFAULT NULL,
  `repeat_count` int(7) DEFAULT NULL,
  `repeat_parent_id` char(36) DEFAULT NULL,
  `recurring_source` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_call_name` (`name`),
  KEY `idx_status` (`status`),
  KEY `idx_calls_date_start` (`date_start`),
  KEY `idx_calls_par_del` (`parent_id`,`parent_type`,`deleted`),
  KEY `idx_calls_assigned_del` (`deleted`,`assigned_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `calls`
--

LOCK TABLES `calls` WRITE;
/*!40000 ALTER TABLE `calls` DISABLE KEYS */;
/*!40000 ALTER TABLE `calls` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `calls_contacts`
--

DROP TABLE IF EXISTS `calls_contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calls_contacts` (
  `id` varchar(36) NOT NULL,
  `call_id` varchar(36) DEFAULT NULL,
  `contact_id` varchar(36) DEFAULT NULL,
  `required` varchar(1) DEFAULT '1',
  `accept_status` varchar(25) DEFAULT 'none',
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_con_call_call` (`call_id`),
  KEY `idx_con_call_con` (`contact_id`),
  KEY `idx_call_contact` (`call_id`,`contact_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `calls_contacts`
--

LOCK TABLES `calls_contacts` WRITE;
/*!40000 ALTER TABLE `calls_contacts` DISABLE KEYS */;
/*!40000 ALTER TABLE `calls_contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `calls_leads`
--

DROP TABLE IF EXISTS `calls_leads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calls_leads` (
  `id` varchar(36) NOT NULL,
  `call_id` varchar(36) DEFAULT NULL,
  `lead_id` varchar(36) DEFAULT NULL,
  `required` varchar(1) DEFAULT '1',
  `accept_status` varchar(25) DEFAULT 'none',
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_lead_call_call` (`call_id`),
  KEY `idx_lead_call_lead` (`lead_id`),
  KEY `idx_call_lead` (`call_id`,`lead_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `calls_leads`
--

LOCK TABLES `calls_leads` WRITE;
/*!40000 ALTER TABLE `calls_leads` DISABLE KEYS */;
/*!40000 ALTER TABLE `calls_leads` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `calls_reschedule`
--

DROP TABLE IF EXISTS `calls_reschedule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calls_reschedule` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `reason` varchar(100) DEFAULT NULL,
  `call_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `calls_reschedule`
--

LOCK TABLES `calls_reschedule` WRITE;
/*!40000 ALTER TABLE `calls_reschedule` DISABLE KEYS */;
/*!40000 ALTER TABLE `calls_reschedule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `calls_reschedule_audit`
--

DROP TABLE IF EXISTS `calls_reschedule_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calls_reschedule_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_calls_reschedule_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `calls_reschedule_audit`
--

LOCK TABLES `calls_reschedule_audit` WRITE;
/*!40000 ALTER TABLE `calls_reschedule_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `calls_reschedule_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `calls_users`
--

DROP TABLE IF EXISTS `calls_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calls_users` (
  `id` varchar(36) NOT NULL,
  `call_id` varchar(36) DEFAULT NULL,
  `user_id` varchar(36) DEFAULT NULL,
  `required` varchar(1) DEFAULT '1',
  `accept_status` varchar(25) DEFAULT 'none',
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_usr_call_call` (`call_id`),
  KEY `idx_usr_call_usr` (`user_id`),
  KEY `idx_call_users` (`call_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `calls_users`
--

LOCK TABLES `calls_users` WRITE;
/*!40000 ALTER TABLE `calls_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `calls_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `campaign_log`
--

DROP TABLE IF EXISTS `campaign_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campaign_log` (
  `id` char(36) NOT NULL,
  `campaign_id` char(36) DEFAULT NULL,
  `target_tracker_key` varchar(36) DEFAULT NULL,
  `target_id` varchar(36) DEFAULT NULL,
  `target_type` varchar(100) DEFAULT NULL,
  `activity_type` varchar(100) DEFAULT NULL,
  `activity_date` datetime DEFAULT NULL,
  `related_id` varchar(36) DEFAULT NULL,
  `related_type` varchar(100) DEFAULT NULL,
  `archived` tinyint(1) DEFAULT '0',
  `hits` int(11) DEFAULT '0',
  `list_id` char(36) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `more_information` varchar(100) DEFAULT NULL,
  `marketing_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_camp_tracker` (`target_tracker_key`),
  KEY `idx_camp_campaign_id` (`campaign_id`),
  KEY `idx_camp_more_info` (`more_information`),
  KEY `idx_target_id` (`target_id`),
  KEY `idx_target_id_deleted` (`target_id`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `campaign_log`
--

LOCK TABLES `campaign_log` WRITE;
/*!40000 ALTER TABLE `campaign_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `campaign_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `campaign_trkrs`
--

DROP TABLE IF EXISTS `campaign_trkrs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campaign_trkrs` (
  `id` char(36) NOT NULL,
  `tracker_name` varchar(255) DEFAULT NULL,
  `tracker_url` varchar(255) DEFAULT 'http://',
  `tracker_key` int(11) NOT NULL AUTO_INCREMENT,
  `campaign_id` char(36) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `is_optout` tinyint(1) DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `campaign_tracker_key_idx` (`tracker_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `campaign_trkrs`
--

LOCK TABLES `campaign_trkrs` WRITE;
/*!40000 ALTER TABLE `campaign_trkrs` DISABLE KEYS */;
/*!40000 ALTER TABLE `campaign_trkrs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `campaigns`
--

DROP TABLE IF EXISTS `campaigns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campaigns` (
  `id` char(36) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `tracker_key` int(11) NOT NULL AUTO_INCREMENT,
  `tracker_count` int(11) DEFAULT '0',
  `refer_url` varchar(255) DEFAULT 'http://',
  `tracker_text` varchar(255) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `impressions` int(11) DEFAULT '0',
  `currency_id` char(36) DEFAULT NULL,
  `budget` double DEFAULT NULL,
  `expected_cost` double DEFAULT NULL,
  `actual_cost` double DEFAULT NULL,
  `expected_revenue` double DEFAULT NULL,
  `campaign_type` varchar(100) DEFAULT NULL,
  `objective` text,
  `content` text,
  `frequency` varchar(100) DEFAULT NULL,
  `survey_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `camp_auto_tracker_key` (`tracker_key`),
  KEY `idx_campaign_name` (`name`),
  KEY `idx_survey_id` (`survey_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `campaigns`
--

LOCK TABLES `campaigns` WRITE;
/*!40000 ALTER TABLE `campaigns` DISABLE KEYS */;
/*!40000 ALTER TABLE `campaigns` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `campaigns_audit`
--

DROP TABLE IF EXISTS `campaigns_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campaigns_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_campaigns_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `campaigns_audit`
--

LOCK TABLES `campaigns_audit` WRITE;
/*!40000 ALTER TABLE `campaigns_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `campaigns_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cases`
--

DROP TABLE IF EXISTS `cases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cases` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `case_number` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `priority` varchar(100) DEFAULT NULL,
  `resolution` text,
  `work_log` text,
  `account_id` char(36) DEFAULT NULL,
  `state` varchar(100) DEFAULT 'Open',
  `contact_created_by_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `casesnumk` (`case_number`),
  KEY `case_number` (`case_number`),
  KEY `idx_case_name` (`name`),
  KEY `idx_account_id` (`account_id`),
  KEY `idx_cases_stat_del` (`assigned_user_id`,`status`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cases`
--

LOCK TABLES `cases` WRITE;
/*!40000 ALTER TABLE `cases` DISABLE KEYS */;
/*!40000 ALTER TABLE `cases` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cases_audit`
--

DROP TABLE IF EXISTS `cases_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cases_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_cases_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cases_audit`
--

LOCK TABLES `cases_audit` WRITE;
/*!40000 ALTER TABLE `cases_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `cases_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cases_bugs`
--

DROP TABLE IF EXISTS `cases_bugs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cases_bugs` (
  `id` varchar(36) NOT NULL,
  `case_id` varchar(36) DEFAULT NULL,
  `bug_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_cas_bug_cas` (`case_id`),
  KEY `idx_cas_bug_bug` (`bug_id`),
  KEY `idx_case_bug` (`case_id`,`bug_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cases_bugs`
--

LOCK TABLES `cases_bugs` WRITE;
/*!40000 ALTER TABLE `cases_bugs` DISABLE KEYS */;
/*!40000 ALTER TABLE `cases_bugs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cases_cstm`
--

DROP TABLE IF EXISTS `cases_cstm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cases_cstm` (
  `id_c` char(36) NOT NULL,
  `jjwg_maps_lng_c` float(11,8) DEFAULT '0.00000000',
  `jjwg_maps_lat_c` float(10,8) DEFAULT '0.00000000',
  `jjwg_maps_geocode_status_c` varchar(255) DEFAULT NULL,
  `jjwg_maps_address_c` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_c`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cases_cstm`
--

LOCK TABLES `cases_cstm` WRITE;
/*!40000 ALTER TABLE `cases_cstm` DISABLE KEYS */;
/*!40000 ALTER TABLE `cases_cstm` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `config`
--

DROP TABLE IF EXISTS `config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `config` (
  `category` varchar(32) DEFAULT NULL,
  `name` varchar(32) DEFAULT NULL,
  `value` text,
  KEY `idx_config_cat` (`category`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `config`
--

LOCK TABLES `config` WRITE;
/*!40000 ALTER TABLE `config` DISABLE KEYS */;
INSERT INTO `config` VALUES ('notify','fromaddress','do_not_reply@example.com'),('notify','fromname','SuiteCRM'),('notify','send_by_default','1'),('notify','on','1'),('notify','send_from_assigning_user','0'),('info','sugar_version','6.5.25'),('MySettings','tab','YTo0Mjp7czo0OiJIb21lIjtzOjQ6IkhvbWUiO3M6ODoiQWNjb3VudHMiO3M6ODoiQWNjb3VudHMiO3M6ODoiQ29udGFjdHMiO3M6ODoiQ29udGFjdHMiO3M6MTM6Ik9wcG9ydHVuaXRpZXMiO3M6MTM6Ik9wcG9ydHVuaXRpZXMiO3M6NToiTGVhZHMiO3M6NToiTGVhZHMiO3M6MTA6IkFPU19RdW90ZXMiO3M6MTA6IkFPU19RdW90ZXMiO3M6ODoiQ2FsZW5kYXIiO3M6ODoiQ2FsZW5kYXIiO3M6OToiRG9jdW1lbnRzIjtzOjk6IkRvY3VtZW50cyI7czo2OiJFbWFpbHMiO3M6NjoiRW1haWxzIjtzOjU6IlNwb3RzIjtzOjU6IlNwb3RzIjtzOjk6IkNhbXBhaWducyI7czo5OiJDYW1wYWlnbnMiO3M6NToiQ2FsbHMiO3M6NToiQ2FsbHMiO3M6ODoiTWVldGluZ3MiO3M6ODoiTWVldGluZ3MiO3M6NToiVGFza3MiO3M6NToiVGFza3MiO3M6NToiTm90ZXMiO3M6NToiTm90ZXMiO3M6MTI6IkFPU19JbnZvaWNlcyI7czoxMjoiQU9TX0ludm9pY2VzIjtzOjEzOiJBT1NfQ29udHJhY3RzIjtzOjEzOiJBT1NfQ29udHJhY3RzIjtzOjU6IkNhc2VzIjtzOjU6IkNhc2VzIjtzOjk6IlByb3NwZWN0cyI7czo5OiJQcm9zcGVjdHMiO3M6MTM6IlByb3NwZWN0TGlzdHMiO3M6MTM6IlByb3NwZWN0TGlzdHMiO3M6NzoiUHJvamVjdCI7czo3OiJQcm9qZWN0IjtzOjE5OiJBTV9Qcm9qZWN0VGVtcGxhdGVzIjtzOjE5OiJBTV9Qcm9qZWN0VGVtcGxhdGVzIjtzOjk6IkZQX2V2ZW50cyI7czo5OiJGUF9ldmVudHMiO3M6MTg6IkZQX0V2ZW50X0xvY2F0aW9ucyI7czoxODoiRlBfRXZlbnRfTG9jYXRpb25zIjtzOjEyOiJBT1NfUHJvZHVjdHMiO3M6MTI6IkFPU19Qcm9kdWN0cyI7czoyMjoiQU9TX1Byb2R1Y3RfQ2F0ZWdvcmllcyI7czoyMjoiQU9TX1Byb2R1Y3RfQ2F0ZWdvcmllcyI7czoxNzoiQU9TX1BERl9UZW1wbGF0ZXMiO3M6MTc6IkFPU19QREZfVGVtcGxhdGVzIjtzOjk6Impqd2dfTWFwcyI7czo5OiJqandnX01hcHMiO3M6MTI6Impqd2dfTWFya2VycyI7czoxMjoiamp3Z19NYXJrZXJzIjtzOjEwOiJqandnX0FyZWFzIjtzOjEwOiJqandnX0FyZWFzIjtzOjE4OiJqandnX0FkZHJlc3NfQ2FjaGUiO3M6MTg6Impqd2dfQWRkcmVzc19DYWNoZSI7czoxMToiQU9SX1JlcG9ydHMiO3M6MTE6IkFPUl9SZXBvcnRzIjtzOjEyOiJBT1dfV29ya0Zsb3ciO3M6MTI6IkFPV19Xb3JrRmxvdyI7czoxNzoiQU9LX0tub3dsZWRnZUJhc2UiO3M6MTc6IkFPS19Lbm93bGVkZ2VCYXNlIjtzOjI5OiJBT0tfS25vd2xlZGdlX0Jhc2VfQ2F0ZWdvcmllcyI7czoyOToiQU9LX0tub3dsZWRnZV9CYXNlX0NhdGVnb3JpZXMiO3M6MTQ6IkVtYWlsVGVtcGxhdGVzIjtzOjE0OiJFbWFpbFRlbXBsYXRlcyI7czo3OiJTdXJ2ZXlzIjtzOjc6IlN1cnZleXMiO3M6MTI6InNwX1RpbWVzaGVldCI7czoxMjoic3BfVGltZXNoZWV0IjtzOjIwOiJzcF90aW1lc2hlZXRfdHJhY2tlciI7czoyMDoic3BfdGltZXNoZWV0X3RyYWNrZXIiO3M6MzA6InNwX0F1dG9fVGltZVNoZWV0X1RpbWVfVHJhY2tlciI7czozMDoic3BfQXV0b19UaW1lU2hlZXRfVGltZV9UcmFja2VyIjtzOjE5OiJzdWhfdGltZXJfcmVjb3JkaW5nIjtzOjE5OiJzdWhfdGltZXJfcmVjb3JkaW5nIjtzOjE5OiJzcF9UaW1lb2ZmX1RpbWVDYXJkIjtzOjE5OiJzcF9UaW1lb2ZmX1RpbWVDYXJkIjt9'),('portal','on','0'),('tracker','Tracker','1'),('system','skypeout_on','1'),('sugarfeed','enabled','1'),('sugarfeed','module_UserFeed','1'),('sugarfeed','module_Cases','1'),('sugarfeed','module_Leads','1'),('sugarfeed','module_Opportunities','1'),('sugarfeed','module_Contacts','1'),('Update','CheckUpdates','manual'),('system','name','SuiteCRM'),('system','adminwizard','1'),('notify','allow_default_outbound','0'),('SugarOutfitters','timesheetninjaenterprise','YToyOntzOjg6Imxhc3RfcmFuIjtpOjE1OTc3NjI3ODQ7czoxMToibGFzdF9yZXN1bHQiO2E6Mjp7czo3OiJzdWNjZXNzIjtiOjA7czo2OiJyZXN1bHQiO3M6MTY6IktleSBpcyBpbmFjdGl2ZS4iO319'),('SugarOutfitters','lic_timesheetninjaenterprise','25804ff03df644aa16aea726d97137f0'),('proxy','on','0'),('proxy','host',''),('proxy','port',''),('proxy','auth','0'),('proxy','username',''),('proxy','password',''),('SugarOutfitters','kanbans','YToyOntzOjg6Imxhc3RfcmFuIjtpOjE1OTg2MDY2NDg7czoxMToibGFzdF9yZXN1bHQiO2E6Mjp7czo3OiJzdWNjZXNzIjtiOjE7czo2OiJyZXN1bHQiO2E6MTp7czo5OiJ2YWxpZGF0ZWQiO2I6MTt9fX0=');
/*!40000 ALTER TABLE `config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contacts`
--

DROP TABLE IF EXISTS `contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contacts` (
  `id` char(36) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `salutation` varchar(255) DEFAULT NULL,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `department` varchar(255) DEFAULT NULL,
  `do_not_call` tinyint(1) DEFAULT '0',
  `phone_home` varchar(100) DEFAULT NULL,
  `phone_mobile` varchar(100) DEFAULT NULL,
  `phone_work` varchar(100) DEFAULT NULL,
  `phone_other` varchar(100) DEFAULT NULL,
  `phone_fax` varchar(100) DEFAULT NULL,
  `lawful_basis` text,
  `date_reviewed` date DEFAULT NULL,
  `lawful_basis_source` varchar(100) DEFAULT NULL,
  `primary_address_street` varchar(150) DEFAULT NULL,
  `primary_address_city` varchar(100) DEFAULT NULL,
  `primary_address_state` varchar(100) DEFAULT NULL,
  `primary_address_postalcode` varchar(20) DEFAULT NULL,
  `primary_address_country` varchar(255) DEFAULT NULL,
  `alt_address_street` varchar(150) DEFAULT NULL,
  `alt_address_city` varchar(100) DEFAULT NULL,
  `alt_address_state` varchar(100) DEFAULT NULL,
  `alt_address_postalcode` varchar(20) DEFAULT NULL,
  `alt_address_country` varchar(255) DEFAULT NULL,
  `assistant` varchar(75) DEFAULT NULL,
  `assistant_phone` varchar(100) DEFAULT NULL,
  `lead_source` varchar(255) DEFAULT NULL,
  `reports_to_id` char(36) DEFAULT NULL,
  `birthdate` date DEFAULT NULL,
  `campaign_id` char(36) DEFAULT NULL,
  `joomla_account_id` varchar(255) DEFAULT NULL,
  `portal_account_disabled` tinyint(1) DEFAULT NULL,
  `portal_user_type` varchar(100) DEFAULT 'Single',
  PRIMARY KEY (`id`),
  KEY `idx_cont_last_first` (`last_name`,`first_name`,`deleted`),
  KEY `idx_contacts_del_last` (`deleted`,`last_name`),
  KEY `idx_cont_del_reports` (`deleted`,`reports_to_id`,`last_name`),
  KEY `idx_reports_to_id` (`reports_to_id`),
  KEY `idx_del_id_user` (`deleted`,`id`,`assigned_user_id`),
  KEY `idx_cont_assigned` (`assigned_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contacts`
--

LOCK TABLES `contacts` WRITE;
/*!40000 ALTER TABLE `contacts` DISABLE KEYS */;
INSERT INTO `contacts` VALUES ('3898ea01-ac05-7e9f-4dce-5f48dcaf2e89','2020-08-28 10:30:05','2020-08-28 10:30:41','1674d634-fb3a-8d8b-34c1-5f4698fed125','1674d634-fb3a-8d8b-34c1-5f4698fed125',NULL,0,'1674d634-fb3a-8d8b-34c1-5f4698fed125',NULL,'Sandra','China','CEO',NULL,NULL,0,NULL,NULL,'(312)123-1234',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,'',NULL,0,'Single'),('4da4191b-796e-24f8-313a-5f44f8418e64','2020-08-25 11:41:26','2020-08-25 11:41:26','1','1','',0,'1','Ms.','Susan','Morgan','COO',NULL,'',0,NULL,'','',NULL,'',NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,'Trade Show','',NULL,'',NULL,NULL,'Single'),('5200556a-9070-b927-1928-5f48da876d80','2020-08-28 10:22:07','2020-08-28 10:23:31','8d544213-bcac-b962-fc13-5f4698c75700','8d544213-bcac-b962-fc13-5f4698c75700',NULL,0,'8d544213-bcac-b962-fc13-5f4698c75700',NULL,'Steve','India',NULL,NULL,NULL,0,NULL,NULL,'(321)123-1234',NULL,NULL,NULL,NULL,NULL,NULL,'Delhi',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,'',NULL,0,'Single');
/*!40000 ALTER TABLE `contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contacts_audit`
--

DROP TABLE IF EXISTS `contacts_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contacts_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_contacts_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contacts_audit`
--

LOCK TABLES `contacts_audit` WRITE;
/*!40000 ALTER TABLE `contacts_audit` DISABLE KEYS */;
INSERT INTO `contacts_audit` VALUES ('8eec5cd2-970e-c785-50ad-5f48db799ab1','5200556a-9070-b927-1928-5f48da876d80','2020-08-28 10:23:31','8d544213-bcac-b962-fc13-5f4698c75700','phone_work','phone','','(321)123-1234',NULL,NULL),('bdbe4d23-de3c-ea3d-1ceb-5f48dcb2d8f9','3898ea01-ac05-7e9f-4dce-5f48dcaf2e89','2020-08-28 10:30:41','1674d634-fb3a-8d8b-34c1-5f4698fed125','phone_work','phone','','(312)123-1234',NULL,NULL);
/*!40000 ALTER TABLE `contacts_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contacts_bugs`
--

DROP TABLE IF EXISTS `contacts_bugs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contacts_bugs` (
  `id` varchar(36) NOT NULL,
  `contact_id` varchar(36) DEFAULT NULL,
  `bug_id` varchar(36) DEFAULT NULL,
  `contact_role` varchar(50) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_con_bug_con` (`contact_id`),
  KEY `idx_con_bug_bug` (`bug_id`),
  KEY `idx_contact_bug` (`contact_id`,`bug_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contacts_bugs`
--

LOCK TABLES `contacts_bugs` WRITE;
/*!40000 ALTER TABLE `contacts_bugs` DISABLE KEYS */;
/*!40000 ALTER TABLE `contacts_bugs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contacts_cases`
--

DROP TABLE IF EXISTS `contacts_cases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contacts_cases` (
  `id` varchar(36) NOT NULL,
  `contact_id` varchar(36) DEFAULT NULL,
  `case_id` varchar(36) DEFAULT NULL,
  `contact_role` varchar(50) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_con_case_con` (`contact_id`),
  KEY `idx_con_case_case` (`case_id`),
  KEY `idx_contacts_cases` (`contact_id`,`case_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contacts_cases`
--

LOCK TABLES `contacts_cases` WRITE;
/*!40000 ALTER TABLE `contacts_cases` DISABLE KEYS */;
/*!40000 ALTER TABLE `contacts_cases` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contacts_cstm`
--

DROP TABLE IF EXISTS `contacts_cstm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contacts_cstm` (
  `id_c` char(36) NOT NULL,
  `jjwg_maps_lng_c` float(11,8) DEFAULT '0.00000000',
  `jjwg_maps_lat_c` float(10,8) DEFAULT '0.00000000',
  `jjwg_maps_geocode_status_c` varchar(255) DEFAULT NULL,
  `jjwg_maps_address_c` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_c`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contacts_cstm`
--

LOCK TABLES `contacts_cstm` WRITE;
/*!40000 ALTER TABLE `contacts_cstm` DISABLE KEYS */;
INSERT INTO `contacts_cstm` VALUES ('3898ea01-ac05-7e9f-4dce-5f48dcaf2e89',0.00000000,0.00000000,'',''),('4da4191b-796e-24f8-313a-5f44f8418e64',0.00000000,0.00000000,NULL,NULL),('5200556a-9070-b927-1928-5f48da876d80',0.00000000,0.00000000,'','');
/*!40000 ALTER TABLE `contacts_cstm` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contacts_users`
--

DROP TABLE IF EXISTS `contacts_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contacts_users` (
  `id` varchar(36) NOT NULL,
  `contact_id` varchar(36) DEFAULT NULL,
  `user_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_con_users_con` (`contact_id`),
  KEY `idx_con_users_user` (`user_id`),
  KEY `idx_contacts_users` (`contact_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contacts_users`
--

LOCK TABLES `contacts_users` WRITE;
/*!40000 ALTER TABLE `contacts_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `contacts_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cron_remove_documents`
--

DROP TABLE IF EXISTS `cron_remove_documents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cron_remove_documents` (
  `id` varchar(36) NOT NULL,
  `bean_id` varchar(36) DEFAULT NULL,
  `module` varchar(25) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_cron_remove_document_bean_id` (`bean_id`),
  KEY `idx_cron_remove_document_stamp` (`date_modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cron_remove_documents`
--

LOCK TABLES `cron_remove_documents` WRITE;
/*!40000 ALTER TABLE `cron_remove_documents` DISABLE KEYS */;
/*!40000 ALTER TABLE `cron_remove_documents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `currencies`
--

DROP TABLE IF EXISTS `currencies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `currencies` (
  `id` char(36) NOT NULL,
  `name` varchar(36) DEFAULT NULL,
  `symbol` varchar(36) DEFAULT NULL,
  `iso4217` varchar(3) DEFAULT NULL,
  `conversion_rate` double DEFAULT '0',
  `status` varchar(100) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `created_by` char(36) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_currency_name` (`name`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `currencies`
--

LOCK TABLES `currencies` WRITE;
/*!40000 ALTER TABLE `currencies` DISABLE KEYS */;
/*!40000 ALTER TABLE `currencies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `custom_fields`
--

DROP TABLE IF EXISTS `custom_fields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `custom_fields` (
  `bean_id` varchar(36) DEFAULT NULL,
  `set_num` int(11) DEFAULT '0',
  `field0` varchar(255) DEFAULT NULL,
  `field1` varchar(255) DEFAULT NULL,
  `field2` varchar(255) DEFAULT NULL,
  `field3` varchar(255) DEFAULT NULL,
  `field4` varchar(255) DEFAULT NULL,
  `field5` varchar(255) DEFAULT NULL,
  `field6` varchar(255) DEFAULT NULL,
  `field7` varchar(255) DEFAULT NULL,
  `field8` varchar(255) DEFAULT NULL,
  `field9` varchar(255) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  KEY `idx_beanid_set_num` (`bean_id`,`set_num`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `custom_fields`
--

LOCK TABLES `custom_fields` WRITE;
/*!40000 ALTER TABLE `custom_fields` DISABLE KEYS */;
/*!40000 ALTER TABLE `custom_fields` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `document_revisions`
--

DROP TABLE IF EXISTS `document_revisions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `document_revisions` (
  `id` varchar(36) NOT NULL,
  `change_log` varchar(255) DEFAULT NULL,
  `document_id` varchar(36) DEFAULT NULL,
  `doc_id` varchar(100) DEFAULT NULL,
  `doc_type` varchar(100) DEFAULT NULL,
  `doc_url` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `file_ext` varchar(100) DEFAULT NULL,
  `file_mime_type` varchar(100) DEFAULT NULL,
  `revision` varchar(100) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `date_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `documentrevision_mimetype` (`file_mime_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `document_revisions`
--

LOCK TABLES `document_revisions` WRITE;
/*!40000 ALTER TABLE `document_revisions` DISABLE KEYS */;
/*!40000 ALTER TABLE `document_revisions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `documents`
--

DROP TABLE IF EXISTS `documents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `documents` (
  `id` char(36) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `document_name` varchar(255) DEFAULT NULL,
  `doc_id` varchar(100) DEFAULT NULL,
  `doc_type` varchar(100) DEFAULT 'Sugar',
  `doc_url` varchar(255) DEFAULT NULL,
  `active_date` date DEFAULT NULL,
  `exp_date` date DEFAULT NULL,
  `category_id` varchar(100) DEFAULT NULL,
  `subcategory_id` varchar(100) DEFAULT NULL,
  `status_id` varchar(100) DEFAULT NULL,
  `document_revision_id` varchar(36) DEFAULT NULL,
  `related_doc_id` char(36) DEFAULT NULL,
  `related_doc_rev_id` char(36) DEFAULT NULL,
  `is_template` tinyint(1) DEFAULT '0',
  `template_type` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_doc_cat` (`category_id`,`subcategory_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `documents`
--

LOCK TABLES `documents` WRITE;
/*!40000 ALTER TABLE `documents` DISABLE KEYS */;
/*!40000 ALTER TABLE `documents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `documents_accounts`
--

DROP TABLE IF EXISTS `documents_accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `documents_accounts` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `document_id` varchar(36) DEFAULT NULL,
  `account_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `documents_accounts_account_id` (`account_id`,`document_id`),
  KEY `documents_accounts_document_id` (`document_id`,`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `documents_accounts`
--

LOCK TABLES `documents_accounts` WRITE;
/*!40000 ALTER TABLE `documents_accounts` DISABLE KEYS */;
/*!40000 ALTER TABLE `documents_accounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `documents_bugs`
--

DROP TABLE IF EXISTS `documents_bugs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `documents_bugs` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `document_id` varchar(36) DEFAULT NULL,
  `bug_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `documents_bugs_bug_id` (`bug_id`,`document_id`),
  KEY `documents_bugs_document_id` (`document_id`,`bug_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `documents_bugs`
--

LOCK TABLES `documents_bugs` WRITE;
/*!40000 ALTER TABLE `documents_bugs` DISABLE KEYS */;
/*!40000 ALTER TABLE `documents_bugs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `documents_cases`
--

DROP TABLE IF EXISTS `documents_cases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `documents_cases` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `document_id` varchar(36) DEFAULT NULL,
  `case_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `documents_cases_case_id` (`case_id`,`document_id`),
  KEY `documents_cases_document_id` (`document_id`,`case_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `documents_cases`
--

LOCK TABLES `documents_cases` WRITE;
/*!40000 ALTER TABLE `documents_cases` DISABLE KEYS */;
/*!40000 ALTER TABLE `documents_cases` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `documents_contacts`
--

DROP TABLE IF EXISTS `documents_contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `documents_contacts` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `document_id` varchar(36) DEFAULT NULL,
  `contact_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `documents_contacts_contact_id` (`contact_id`,`document_id`),
  KEY `documents_contacts_document_id` (`document_id`,`contact_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `documents_contacts`
--

LOCK TABLES `documents_contacts` WRITE;
/*!40000 ALTER TABLE `documents_contacts` DISABLE KEYS */;
/*!40000 ALTER TABLE `documents_contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `documents_opportunities`
--

DROP TABLE IF EXISTS `documents_opportunities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `documents_opportunities` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `document_id` varchar(36) DEFAULT NULL,
  `opportunity_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_docu_opps_oppo_id` (`opportunity_id`,`document_id`),
  KEY `idx_docu_oppo_docu_id` (`document_id`,`opportunity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `documents_opportunities`
--

LOCK TABLES `documents_opportunities` WRITE;
/*!40000 ALTER TABLE `documents_opportunities` DISABLE KEYS */;
/*!40000 ALTER TABLE `documents_opportunities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eapm`
--

DROP TABLE IF EXISTS `eapm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eapm` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `application` varchar(100) DEFAULT 'webex',
  `api_data` text,
  `consumer_key` varchar(255) DEFAULT NULL,
  `consumer_secret` varchar(255) DEFAULT NULL,
  `oauth_token` varchar(255) DEFAULT NULL,
  `oauth_secret` varchar(255) DEFAULT NULL,
  `validated` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_app_active` (`assigned_user_id`,`application`,`validated`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eapm`
--

LOCK TABLES `eapm` WRITE;
/*!40000 ALTER TABLE `eapm` DISABLE KEYS */;
/*!40000 ALTER TABLE `eapm` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `email_addr_bean_rel`
--

DROP TABLE IF EXISTS `email_addr_bean_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email_addr_bean_rel` (
  `id` char(36) NOT NULL,
  `email_address_id` char(36) NOT NULL,
  `bean_id` char(36) NOT NULL,
  `bean_module` varchar(100) DEFAULT NULL,
  `primary_address` tinyint(1) DEFAULT '0',
  `reply_to_address` tinyint(1) DEFAULT '0',
  `date_created` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_email_address_id` (`email_address_id`),
  KEY `idx_bean_id` (`bean_id`,`bean_module`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `email_addr_bean_rel`
--

LOCK TABLES `email_addr_bean_rel` WRITE;
/*!40000 ALTER TABLE `email_addr_bean_rel` DISABLE KEYS */;
INSERT INTO `email_addr_bean_rel` VALUES ('396b0d76-7532-52d8-88e0-5e90aaef5222','3c6a0ba3-aa83-b707-11c3-5e90aabb73ca','dc26b995-5550-b848-6f73-5e90aa679348','Users',1,0,'2020-04-10 17:20:57','2020-04-10 17:20:57',0),('3bae4fa3-3b4e-baed-c677-5f43bf976b0c','3bd2bba2-b720-6cd4-339f-5f43bf8bd789','2aab764c-a53d-2a20-e944-5f43bff03c55','Leads',1,0,'2020-08-24 13:24:51','2020-08-24 13:24:51',0),('4a7bbe41-c189-585e-cfbb-5f48dd0cb5f9','4a9cd4dc-9d81-b2fc-52f9-5f48ddf8c032','3977c53e-104f-b7c1-2ca8-5f48dd5f8c58','Leads',1,0,'2020-08-28 10:32:56','2020-08-28 10:32:56',0),('87223dbd-a8b7-0419-5657-5f44f8e2fe0f','87468d55-8bb1-897f-ed8f-5f44f899d545','4da4191b-796e-24f8-313a-5f44f8418e64','Contacts',1,0,'2020-08-25 11:41:26','2020-08-25 11:41:26',0),('be9cd574-1653-680f-6e3f-5f48dcf8d06d','bec1f80e-524f-c2bc-b131-5f48dcb87f6e','3898ea01-ac05-7e9f-4dce-5f48dcaf2e89','Contacts',1,0,'2020-08-28 10:30:41','2020-08-28 10:30:41',0);
/*!40000 ALTER TABLE `email_addr_bean_rel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `email_addresses`
--

DROP TABLE IF EXISTS `email_addresses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email_addresses` (
  `id` char(36) NOT NULL,
  `email_address` varchar(255) DEFAULT NULL,
  `email_address_caps` varchar(255) DEFAULT NULL,
  `invalid_email` tinyint(1) DEFAULT '0',
  `opt_out` tinyint(1) DEFAULT '0',
  `confirm_opt_in` varchar(255) DEFAULT 'not-opt-in',
  `confirm_opt_in_date` datetime DEFAULT NULL,
  `confirm_opt_in_sent_date` datetime DEFAULT NULL,
  `confirm_opt_in_fail_date` datetime DEFAULT NULL,
  `confirm_opt_in_token` varchar(255) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_ea_caps_opt_out_invalid` (`email_address_caps`,`opt_out`,`invalid_email`),
  KEY `idx_ea_opt_out_invalid` (`email_address`,`opt_out`,`invalid_email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `email_addresses`
--

LOCK TABLES `email_addresses` WRITE;
/*!40000 ALTER TABLE `email_addresses` DISABLE KEYS */;
INSERT INTO `email_addresses` VALUES ('3bd2bba2-b720-6cd4-339f-5f43bf8bd789','Susie@pharmaceuticalco.com','SUSIE@PHARMACEUTICALCO.COM',0,0,'confirmed-opt-in',NULL,NULL,NULL,NULL,'2020-08-24 13:24:00','2020-08-25 11:42:45',0),('3c6a0ba3-aa83-b707-11c3-5e90aabb73ca','test@test.com','TEST@TEST.COM',0,0,'confirmed-opt-in',NULL,NULL,NULL,NULL,'2020-04-10 17:20:00','2020-09-01 16:57:58',0),('4a9cd4dc-9d81-b2fc-52f9-5f48ddf8c032','tim@pfizer.com','TIM@PFIZER.COM',0,0,'confirmed-opt-in',NULL,NULL,NULL,NULL,'2020-08-28 10:32:00','2020-08-28 10:32:57',0),('87468d55-8bb1-897f-ed8f-5f44f899d545','smorgan@pharmaco.com','SMORGAN@PHARMACO.COM',0,0,'confirmed-opt-in',NULL,NULL,NULL,NULL,'2020-08-25 11:41:00','2020-08-25 15:38:29',0),('bec1f80e-524f-c2bc-b131-5f48dcb87f6e','Sandra@pfizer.com','SANDRA@PFIZER.COM',0,0,'confirmed-opt-in',NULL,NULL,NULL,NULL,'2020-08-28 10:30:00','2020-08-28 10:32:56',0);
/*!40000 ALTER TABLE `email_addresses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `email_addresses_audit`
--

DROP TABLE IF EXISTS `email_addresses_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email_addresses_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_email_addresses_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `email_addresses_audit`
--

LOCK TABLES `email_addresses_audit` WRITE;
/*!40000 ALTER TABLE `email_addresses_audit` DISABLE KEYS */;
INSERT INTO `email_addresses_audit` VALUES ('358e0e58-86bf-6b36-29f3-5f48dca99dca','bec1f80e-524f-c2bc-b131-5f48dcb87f6e','2020-08-28 10:30:41','1674d634-fb3a-8d8b-34c1-5f4698fed125','confirm_opt_in','enum','not-opt-in','confirmed-opt-in',NULL,NULL),('3ed7ddd8-f6d7-c102-9981-5e90aa594707','3c6a0ba3-aa83-b707-11c3-5e90aabb73ca','2020-04-10 17:20:58','1','confirm_opt_in','enum','not-opt-in','confirmed-opt-in',NULL,NULL),('7a9f01fb-808b-b34f-b9c9-5f43bfd0c2f3','3bd2bba2-b720-6cd4-339f-5f43bf8bd789','2020-08-24 13:24:51','1','confirm_opt_in','enum','not-opt-in','confirmed-opt-in',NULL,NULL),('97f5d90d-82c9-3095-0aaf-5f48ddfb3ce9','4a9cd4dc-9d81-b2fc-52f9-5f48ddf8c032','2020-08-28 10:32:56','1674d634-fb3a-8d8b-34c1-5f4698fed125','confirm_opt_in','enum','not-opt-in','confirmed-opt-in',NULL,NULL),('bd53bbc5-2ef1-b5d5-81b3-5f44f8dbf898','87468d55-8bb1-897f-ed8f-5f44f899d545','2020-08-25 11:41:26','1','confirm_opt_in','enum','not-opt-in','confirmed-opt-in',NULL,NULL);
/*!40000 ALTER TABLE `email_addresses_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `email_cache`
--

DROP TABLE IF EXISTS `email_cache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email_cache` (
  `ie_id` char(36) DEFAULT NULL,
  `mbox` varchar(60) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `fromaddr` varchar(100) DEFAULT NULL,
  `toaddr` varchar(255) DEFAULT NULL,
  `senddate` datetime DEFAULT NULL,
  `message_id` varchar(255) DEFAULT NULL,
  `mailsize` int(10) unsigned DEFAULT NULL,
  `imap_uid` int(10) unsigned DEFAULT NULL,
  `msgno` int(10) unsigned DEFAULT NULL,
  `recent` tinyint(4) DEFAULT NULL,
  `flagged` tinyint(4) DEFAULT NULL,
  `answered` tinyint(4) DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  `seen` tinyint(4) DEFAULT NULL,
  `draft` tinyint(4) DEFAULT NULL,
  KEY `idx_ie_id` (`ie_id`),
  KEY `idx_mail_date` (`ie_id`,`mbox`,`senddate`),
  KEY `idx_mail_from` (`ie_id`,`mbox`,`fromaddr`),
  KEY `idx_mail_subj` (`subject`),
  KEY `idx_mail_to` (`toaddr`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `email_cache`
--

LOCK TABLES `email_cache` WRITE;
/*!40000 ALTER TABLE `email_cache` DISABLE KEYS */;
/*!40000 ALTER TABLE `email_cache` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `email_marketing`
--

DROP TABLE IF EXISTS `email_marketing`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email_marketing` (
  `id` char(36) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `from_name` varchar(100) DEFAULT NULL,
  `from_addr` varchar(100) DEFAULT NULL,
  `reply_to_name` varchar(100) DEFAULT NULL,
  `reply_to_addr` varchar(100) DEFAULT NULL,
  `inbound_email_id` varchar(36) DEFAULT NULL,
  `date_start` datetime DEFAULT NULL,
  `template_id` char(36) NOT NULL,
  `status` varchar(100) DEFAULT NULL,
  `campaign_id` char(36) DEFAULT NULL,
  `outbound_email_id` char(36) DEFAULT NULL,
  `all_prospect_lists` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_emmkt_name` (`name`),
  KEY `idx_emmkit_del` (`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `email_marketing`
--

LOCK TABLES `email_marketing` WRITE;
/*!40000 ALTER TABLE `email_marketing` DISABLE KEYS */;
/*!40000 ALTER TABLE `email_marketing` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `email_marketing_prospect_lists`
--

DROP TABLE IF EXISTS `email_marketing_prospect_lists`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email_marketing_prospect_lists` (
  `id` varchar(36) NOT NULL,
  `prospect_list_id` varchar(36) DEFAULT NULL,
  `email_marketing_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `email_mp_prospects` (`email_marketing_id`,`prospect_list_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `email_marketing_prospect_lists`
--

LOCK TABLES `email_marketing_prospect_lists` WRITE;
/*!40000 ALTER TABLE `email_marketing_prospect_lists` DISABLE KEYS */;
/*!40000 ALTER TABLE `email_marketing_prospect_lists` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `email_templates`
--

DROP TABLE IF EXISTS `email_templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email_templates` (
  `id` char(36) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `published` varchar(3) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `subject` varchar(255) DEFAULT NULL,
  `body` longtext,
  `body_html` longtext,
  `deleted` tinyint(1) DEFAULT NULL,
  `assigned_user_id` char(36) DEFAULT NULL,
  `text_only` tinyint(1) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_email_template_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `email_templates`
--

LOCK TABLES `email_templates` WRITE;
/*!40000 ALTER TABLE `email_templates` DISABLE KEYS */;
INSERT INTO `email_templates` VALUES ('1c82c6d0-91a1-a37d-c4e7-5c6e1df4da22','2019-02-21 03:39:38','2019-02-21 03:39:38','1','1','off','Case Closure','Template for informing a contact that their case has been closed.','$acase_name [CASE:$acase_case_number] closed','Hi $contact_first_name $contact_last_name,\n\n					   Your case $acase_name (# $acase_case_number) has been closed on $acase_date_entered\n					   Status:				$acase_status\n					   Reference:			$acase_case_number\n					   Resolution:			$acase_resolution','<p> Hi $contact_first_name $contact_last_name,</p>\n					    <p>Your case $acase_name (# $acase_case_number) has been closed on $acase_date_entered</p>\n					    <table border=\"0\"><tbody><tr><td>Status</td><td>$acase_status</td></tr><tr><td>Reference</td><td>$acase_case_number</td></tr><tr><td>Resolution</td><td>$acase_resolution</td></tr></tbody></table>',0,NULL,NULL,'system'),('2024f387-0aec-5f07-e521-5c6e1d5fa595','2019-02-21 03:39:38','2019-02-21 03:39:38','1','1','off','Joomla Account Creation','Template used when informing a contact that they\'ve been given an account on the joomla portal.','Support Portal Account Created','Hi $contact_name,\n					   An account has been created for you at $portal_address.\n					   You may login using this email address and the password $joomla_pass','<p>Hi $contact_name,</p>\n					    <p>An account has been created for you at <a href=\"$portal_address\">$portal_address</a>.</p>\n					    <p>You may login using this email address and the password $joomla_pass</p>',0,NULL,NULL,'system'),('2455fe2e-a5e3-b331-6c6e-5c6e1d4dd3f5','2019-02-21 03:39:38','2019-02-21 03:39:38','1','1','off','Case Creation','Template to send to a contact when a case is received from them.','$acase_name [CASE:$acase_case_number]','Hi $contact_first_name $contact_last_name,\n\n					   We\'ve received your case $acase_name (# $acase_case_number) on $acase_date_entered\n					   Status:		$acase_status\n					   Reference:	$acase_case_number\n					   Description:	$acase_description','<p> Hi $contact_first_name $contact_last_name,</p>\n					    <p>We\'ve received your case $acase_name (# $acase_case_number) on $acase_date_entered</p>\n					    <table border=\"0\"><tbody><tr><td>Status</td><td>$acase_status</td></tr><tr><td>Reference</td><td>$acase_case_number</td></tr><tr><td>Description</td><td>$acase_description</td></tr></tbody></table>',0,NULL,NULL,'system'),('2a8ed7be-5c7f-a9b5-9d15-5c6e1d00565b','2019-02-21 03:39:38','2019-02-21 03:39:38','1','1','off','Contact Case Update','Template to send to a contact when their case is updated.','$acase_name update [CASE:$acase_case_number]','Hi $user_first_name $user_last_name,\n\n					   You\'ve had an update to your case $acase_name (# $acase_case_number) on $aop_case_updates_date_entered:\n					       $contact_first_name $contact_last_name, said:\n					               $aop_case_updates_description','<p>Hi $contact_first_name $contact_last_name,</p>\n					    <p> </p>\n					    <p>You\'ve had an update to your case $acase_name (# $acase_case_number) on $aop_case_updates_date_entered:</p>\n					    <p><strong>$user_first_name $user_last_name said:</strong></p>\n					    <p style=\"padding-left:30px;\">$aop_case_updates_description</p>',0,NULL,NULL,'system'),('2e4525a1-e902-2ff4-f4c8-5c6e1d567dfc','2019-02-21 03:39:38','2019-02-21 03:39:38','1','1','off','User Case Update','Email template to send to a Sugar user when their case is updated.','$acase_name (# $acase_case_number) update','Hi $user_first_name $user_last_name,\n\n					   You\'ve had an update to your case $acase_name (# $acase_case_number) on $aop_case_updates_date_entered:\n					       $contact_first_name $contact_last_name, said:\n					               $aop_case_updates_description\n                        You may review this Case at:\n                            $sugarurl/index.php?module=Cases&action=DetailView&record=$acase_id;','<p>Hi $user_first_name $user_last_name,</p>\n					     <p> </p>\n					     <p>You\'ve had an update to your case $acase_name (# $acase_case_number) on $aop_case_updates_date_entered:</p>\n					     <p><strong>$contact_first_name $contact_last_name, said:</strong></p>\n					     <p style=\"padding-left:30px;\">$aop_case_updates_description</p>\n					     <p>You may review this Case at: $sugarurl/index.php?module=Cases&action=DetailView&record=$acase_id;</p>',0,NULL,NULL,'system'),('37edbec4-e1c2-2843-fa99-5c6e1df6c143','2013-05-24 14:31:45','2019-02-21 03:39:38','1','1','off','Event Invite Template','Default event invite template.','You have been invited to $fp_events_name','Dear $contact_name,\nYou have been invited to $fp_events_name on $fp_events_date_start to $fp_events_date_end\n$fp_events_description\nYours Sincerely,\n','\n<p>Dear $contact_name,</p>\n<p>You have been invited to $fp_events_name on $fp_events_date_start to $fp_events_date_end</p>\n<p>$fp_events_description</p>\n<p>If you would like to accept this invititation please click accept.</p>\n<p> $fp_events_link or $fp_events_link_declined</p>\n<p>Yours Sincerely,</p>\n',0,NULL,NULL,'system'),('6f4e863e-d913-1a2d-4ae7-5c6e1d0787a4','2019-02-21 03:39:38','2019-02-21 03:39:38','1','1','off','Confirmed Opt In','Email template to send to a contact to confirm they have opted in.','Confirm Opt In','Hi $contact_first_name $contact_last_name, \\n Please confirm that you have opted in by selecting the following link: $sugarurl/index.php?entryPoint=ConfirmOptIn&from=$emailaddress_email_address','<p>Hi $contact_first_name $contact_last_name,</p>\n             <p>\n                Please confirm that you have opted in by selecting the following link:\n                <a href=\"$sugarurl/index.php?entryPoint=ConfirmOptIn&from=$emailaddress_confirm_opt_in_token\">Opt In</a>\n             </p>',0,NULL,NULL,'system'),('b5771e47-70a8-9119-d31d-5c6e1dab0b63','2019-02-21 03:39:38','2019-02-21 03:39:38','1','1','off','System-generated password email','This template is used when the System Administrator sends a new password to a user.','New account information','\nHere is your account username and temporary password:\nUsername : $contact_user_user_name\nPassword : $contact_user_user_hash\n\n$config_site_url\n\nAfter you log in using the above password, you may be required to reset the password to one of your own choice.','<div><table width=\"550\"><tbody><tr><td><p>Here is your account username and temporary password:</p><p>Username : $contact_user_user_name </p><p>Password : $contact_user_user_hash </p><br /><p>$config_site_url</p><br /><p>After you log in using the above password, you may be required to reset the password to one of your own choice.</p>   </td>         </tr><tr><td></td>         </tr></tbody></table></div>',0,NULL,0,'system'),('b9419fa3-9850-5fa8-6f04-5c6e1d61ec80','2019-02-21 03:39:38','2019-02-21 03:39:38','1','1','off','Forgot Password email','This template is used to send a user a link to click to reset the user\'s account password.','Reset your account password','\nYou recently requested on $contact_user_pwd_last_changed to be able to reset your account password.\n\nClick on the link below to reset your password:\n\n$contact_user_link_guid','<div><table width=\"550\"><tbody><tr><td><p>You recently requested on $contact_user_pwd_last_changed to be able to reset your account password. </p><p>Click on the link below to reset your password:</p><p> $contact_user_link_guid </p>  </td>         </tr><tr><td></td>         </tr></tbody></table></div>',0,NULL,0,'system'),('bcc9b40c-0170-bf94-4fe3-5c6e1dbc8333','2019-02-21 03:39:38','2019-02-21 03:39:38','1','1','off','Two Factor Authentication email','This template is used to send a user a code for Two Factor Authentication.','Two Factor Authentication Code','Two Factor Authentication code is $code.','<div><table width=\"550\"><tbody><tr><td><p>Two Factor Authentication code is <b>$code</b>.</p>  </td>         </tr><tr><td></td>         </tr></tbody></table></div>',0,NULL,0,'system');
/*!40000 ALTER TABLE `email_templates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `emailman`
--

DROP TABLE IF EXISTS `emailman`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emailman` (
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `user_id` char(36) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `campaign_id` char(36) DEFAULT NULL,
  `marketing_id` char(36) DEFAULT NULL,
  `list_id` char(36) DEFAULT NULL,
  `send_date_time` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `in_queue` tinyint(1) DEFAULT '0',
  `in_queue_date` datetime DEFAULT NULL,
  `send_attempts` int(11) DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0',
  `related_id` char(36) DEFAULT NULL,
  `related_type` varchar(100) DEFAULT NULL,
  `related_confirm_opt_in` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_eman_list` (`list_id`,`user_id`,`deleted`),
  KEY `idx_eman_campaign_id` (`campaign_id`),
  KEY `idx_eman_relid_reltype_id` (`related_id`,`related_type`,`campaign_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `emailman`
--

LOCK TABLES `emailman` WRITE;
/*!40000 ALTER TABLE `emailman` DISABLE KEYS */;
/*!40000 ALTER TABLE `emailman` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `emails`
--

DROP TABLE IF EXISTS `emails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emails` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `orphaned` tinyint(1) DEFAULT NULL,
  `last_synced` datetime DEFAULT NULL,
  `date_sent_received` datetime DEFAULT NULL,
  `message_id` varchar(255) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `flagged` tinyint(1) DEFAULT NULL,
  `reply_to_status` tinyint(1) DEFAULT NULL,
  `intent` varchar(100) DEFAULT 'pick',
  `mailbox_id` char(36) DEFAULT NULL,
  `parent_type` varchar(100) DEFAULT NULL,
  `parent_id` char(36) DEFAULT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `category_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_email_name` (`name`),
  KEY `idx_message_id` (`message_id`),
  KEY `idx_email_parent_id` (`parent_id`),
  KEY `idx_email_assigned` (`assigned_user_id`,`type`,`status`),
  KEY `idx_email_cat` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `emails`
--

LOCK TABLES `emails` WRITE;
/*!40000 ALTER TABLE `emails` DISABLE KEYS */;
/*!40000 ALTER TABLE `emails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `emails_beans`
--

DROP TABLE IF EXISTS `emails_beans`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emails_beans` (
  `id` char(36) NOT NULL,
  `email_id` char(36) DEFAULT NULL,
  `bean_id` char(36) DEFAULT NULL,
  `bean_module` varchar(100) DEFAULT NULL,
  `campaign_data` text,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_emails_beans_bean_id` (`bean_id`),
  KEY `idx_emails_beans_email_bean` (`email_id`,`bean_id`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `emails_beans`
--

LOCK TABLES `emails_beans` WRITE;
/*!40000 ALTER TABLE `emails_beans` DISABLE KEYS */;
/*!40000 ALTER TABLE `emails_beans` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `emails_email_addr_rel`
--

DROP TABLE IF EXISTS `emails_email_addr_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emails_email_addr_rel` (
  `id` char(36) NOT NULL,
  `email_id` char(36) NOT NULL,
  `address_type` varchar(4) DEFAULT NULL,
  `email_address_id` char(36) NOT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_eearl_email_id` (`email_id`,`address_type`),
  KEY `idx_eearl_address_id` (`email_address_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `emails_email_addr_rel`
--

LOCK TABLES `emails_email_addr_rel` WRITE;
/*!40000 ALTER TABLE `emails_email_addr_rel` DISABLE KEYS */;
/*!40000 ALTER TABLE `emails_email_addr_rel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `emails_text`
--

DROP TABLE IF EXISTS `emails_text`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emails_text` (
  `email_id` char(36) NOT NULL,
  `from_addr` varchar(255) DEFAULT NULL,
  `reply_to_addr` varchar(255) DEFAULT NULL,
  `to_addrs` text,
  `cc_addrs` text,
  `bcc_addrs` text,
  `description` longtext,
  `description_html` longtext,
  `raw_source` longtext,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`email_id`),
  KEY `emails_textfromaddr` (`from_addr`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `emails_text`
--

LOCK TABLES `emails_text` WRITE;
/*!40000 ALTER TABLE `emails_text` DISABLE KEYS */;
/*!40000 ALTER TABLE `emails_text` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `favorites`
--

DROP TABLE IF EXISTS `favorites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `favorites` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `parent_id` char(36) DEFAULT NULL,
  `parent_type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `favorites`
--

LOCK TABLES `favorites` WRITE;
/*!40000 ALTER TABLE `favorites` DISABLE KEYS */;
/*!40000 ALTER TABLE `favorites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fields_meta_data`
--

DROP TABLE IF EXISTS `fields_meta_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fields_meta_data` (
  `id` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `vname` varchar(255) DEFAULT NULL,
  `comments` varchar(255) DEFAULT NULL,
  `help` varchar(255) DEFAULT NULL,
  `custom_module` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `len` int(11) DEFAULT NULL,
  `required` tinyint(1) DEFAULT '0',
  `default_value` varchar(255) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `audited` tinyint(1) DEFAULT '0',
  `massupdate` tinyint(1) DEFAULT '0',
  `duplicate_merge` smallint(6) DEFAULT '0',
  `reportable` tinyint(1) DEFAULT '1',
  `importable` varchar(255) DEFAULT NULL,
  `ext1` varchar(255) DEFAULT NULL,
  `ext2` varchar(255) DEFAULT NULL,
  `ext3` varchar(255) DEFAULT NULL,
  `ext4` text,
  PRIMARY KEY (`id`),
  KEY `idx_meta_id_del` (`id`,`deleted`),
  KEY `idx_meta_cm_del` (`custom_module`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fields_meta_data`
--

LOCK TABLES `fields_meta_data` WRITE;
/*!40000 ALTER TABLE `fields_meta_data` DISABLE KEYS */;
INSERT INTO `fields_meta_data` VALUES ('Accountsjjwg_maps_address_c','jjwg_maps_address_c','LBL_JJWG_MAPS_ADDRESS','Address','Address','Accounts','varchar',255,0,NULL,'2019-02-21 03:39:38',0,0,0,0,1,'true',NULL,'','',''),('Accountsjjwg_maps_geocode_status_c','jjwg_maps_geocode_status_c','LBL_JJWG_MAPS_GEOCODE_STATUS','Geocode Status','Geocode Status','Accounts','varchar',255,0,NULL,'2019-02-21 03:39:38',0,0,0,0,1,'true',NULL,'','',''),('Accountsjjwg_maps_lat_c','jjwg_maps_lat_c','LBL_JJWG_MAPS_LAT','','Latitude','Accounts','float',10,0,'0.00000000','2019-02-21 03:39:38',0,0,0,0,1,'true','8','','',''),('Accountsjjwg_maps_lng_c','jjwg_maps_lng_c','LBL_JJWG_MAPS_LNG','','Longitude','Accounts','float',11,0,'0.00000000','2019-02-21 03:39:38',0,0,0,0,1,'true','8','','',''),('Accountsnda_c','nda_c','LBL_NDA','','','Accounts','enum',100,0,'','2020-09-01 06:25:16',0,0,0,0,1,'true','nda_list','','',''),('Accountsnpr_c','npr_c','LBL_NPR','','','Accounts','enum',100,0,'','2020-09-01 06:23:05',0,0,0,0,1,'true','npr_list','','',''),('Accountspotential_impact_budget_20_c','potential_impact_budget_20_c','LBL_POTENTIAL_IMPACT_BUDGET_20','','','Accounts','float',18,0,'','2020-09-01 09:00:09',0,0,0,0,1,'true','2','','',''),('Accountspotential_impact_budget_21_c','potential_impact_budget_21_c','LBL_POTENTIAL_IMPACT_BUDGET_21','','','Accounts','float',18,0,'','2020-09-01 09:00:46',0,0,0,0,1,'true','2','','',''),('Accountspotential_impact_budget_ly_c','potential_impact_budget_ly_c','LBL_POTENTIAL_IMPACT_BUDGET_LY','','','Accounts','float',18,0,'','2020-09-01 08:59:28',0,0,0,0,1,'true','2','','',''),('Accountspotential_launch_c','potential_launch_c','LBL_POTENTIAL_LAUNCH','','','Accounts','date',NULL,0,'','2020-09-01 08:57:20',0,0,0,0,1,'true','','','',''),('Accountsproduction_site_c','production_site_c','LBL_PRODUCTION_SITE ','','','Accounts','varchar',255,0,'','2020-09-01 08:58:00',0,0,0,0,1,'true','','','',''),('Accountsprojects_won_full_yr_c','projects_won_full_yr_c','LBL_ PROJECTS_WON_FULL_YR','','','Accounts','float',18,0,'','2020-09-01 09:01:41',0,0,0,0,1,'true','2','','',''),('Accountssales_full_year_c','sales_full_year_c','LBL_SALES_FULL_YEAR','','','Accounts','float',18,0,'','2020-09-01 06:28:02',0,0,0,0,1,'true','2','','',''),('Accountssuccess_rate_c','success_rate_c','LBL_SUCCESS_RATE','','','Accounts','float',18,0,'','2020-09-01 06:28:45',0,0,0,0,1,'true','2','','',''),('Casesjjwg_maps_address_c','jjwg_maps_address_c','LBL_JJWG_MAPS_ADDRESS','Address','Address','Cases','varchar',255,0,NULL,'2019-02-21 03:39:38',0,0,0,0,1,'true',NULL,'','',''),('Casesjjwg_maps_geocode_status_c','jjwg_maps_geocode_status_c','LBL_JJWG_MAPS_GEOCODE_STATUS','Geocode Status','Geocode Status','Cases','varchar',255,0,NULL,'2019-02-21 03:39:38',0,0,0,0,1,'true',NULL,'','',''),('Casesjjwg_maps_lat_c','jjwg_maps_lat_c','LBL_JJWG_MAPS_LAT','','Latitude','Cases','float',10,0,'0.00000000','2019-02-21 03:39:38',0,0,0,0,1,'true','8','','',''),('Casesjjwg_maps_lng_c','jjwg_maps_lng_c','LBL_JJWG_MAPS_LNG','','Longitude','Cases','float',11,0,'0.00000000','2019-02-21 03:39:38',0,0,0,0,1,'true','8','','',''),('Contactsjjwg_maps_address_c','jjwg_maps_address_c','LBL_JJWG_MAPS_ADDRESS','Address','Address','Contacts','varchar',255,0,NULL,'2019-02-21 03:39:38',0,0,0,0,1,'true',NULL,'','',''),('Contactsjjwg_maps_geocode_status_c','jjwg_maps_geocode_status_c','LBL_JJWG_MAPS_GEOCODE_STATUS','Geocode Status','Geocode Status','Contacts','varchar',255,0,NULL,'2019-02-21 03:39:38',0,0,0,0,1,'true',NULL,'','',''),('Contactsjjwg_maps_lat_c','jjwg_maps_lat_c','LBL_JJWG_MAPS_LAT','','Latitude','Contacts','float',10,0,'0.00000000','2019-02-21 03:39:38',0,0,0,0,1,'true','8','','',''),('Contactsjjwg_maps_lng_c','jjwg_maps_lng_c','LBL_JJWG_MAPS_LNG','','Longitude','Contacts','float',11,0,'0.00000000','2019-02-21 03:39:38',0,0,0,0,1,'true','8','','',''),('Leadsjjwg_maps_address_c','jjwg_maps_address_c','LBL_JJWG_MAPS_ADDRESS','Address','Address','Leads','varchar',255,0,NULL,'2019-02-21 03:39:38',0,0,0,0,1,'true',NULL,'','',''),('Leadsjjwg_maps_geocode_status_c','jjwg_maps_geocode_status_c','LBL_JJWG_MAPS_GEOCODE_STATUS','Geocode Status','Geocode Status','Leads','varchar',255,0,NULL,'2019-02-21 03:39:38',0,0,0,0,1,'true',NULL,'','',''),('Leadsjjwg_maps_lat_c','jjwg_maps_lat_c','LBL_JJWG_MAPS_LAT','','Latitude','Leads','float',10,0,'0.00000000','2019-02-21 03:39:38',0,0,0,0,1,'true','8','','',''),('Leadsjjwg_maps_lng_c','jjwg_maps_lng_c','LBL_JJWG_MAPS_LNG','','Longitude','Leads','float',11,0,'0.00000000','2019-02-21 03:39:38',0,0,0,0,1,'true','8','','',''),('Meetingsjjwg_maps_address_c','jjwg_maps_address_c','LBL_JJWG_MAPS_ADDRESS','Address','Address','Meetings','varchar',255,0,NULL,'2019-02-21 03:39:38',0,0,0,0,1,'true',NULL,'','',''),('Meetingsjjwg_maps_geocode_status_c','jjwg_maps_geocode_status_c','LBL_JJWG_MAPS_GEOCODE_STATUS','Geocode Status','Geocode Status','Meetings','varchar',255,0,NULL,'2019-02-21 03:39:38',0,0,0,0,1,'true',NULL,'','',''),('Meetingsjjwg_maps_lat_c','jjwg_maps_lat_c','LBL_JJWG_MAPS_LAT','','Latitude','Meetings','float',10,0,'0.00000000','2019-02-21 03:39:38',0,0,0,0,1,'true','8','','',''),('Meetingsjjwg_maps_lng_c','jjwg_maps_lng_c','LBL_JJWG_MAPS_LNG','','Longitude','Meetings','float',11,0,'0.00000000','2019-02-21 03:39:38',0,0,0,0,1,'true','8','','',''),('Opportunitiesitem_description_materialnum_c','item_description_materialnum_c','LBL_ITEM_DESCRIPTION_MATERIALNUM','','','Opportunities','varchar',255,0,'','2020-09-01 09:04:00',0,0,0,0,1,'true','','','',''),('Opportunitiesjjwg_maps_address_c','jjwg_maps_address_c','LBL_JJWG_MAPS_ADDRESS','Address','Address','Opportunities','varchar',255,0,NULL,'2019-02-21 03:39:38',0,0,0,0,1,'true',NULL,'','',''),('Opportunitiesjjwg_maps_geocode_status_c','jjwg_maps_geocode_status_c','LBL_JJWG_MAPS_GEOCODE_STATUS','Geocode Status','Geocode Status','Opportunities','varchar',255,0,NULL,'2019-02-21 03:39:38',0,0,0,0,1,'true',NULL,'','',''),('Opportunitiesjjwg_maps_lat_c','jjwg_maps_lat_c','LBL_JJWG_MAPS_LAT','','Latitude','Opportunities','float',10,0,'0.00000000','2019-02-21 03:39:38',0,0,0,0,1,'true','8','','',''),('Opportunitiesjjwg_maps_lng_c','jjwg_maps_lng_c','LBL_JJWG_MAPS_LNG','','Longitude','Opportunities','float',11,0,'0.00000000','2019-02-21 03:39:38',0,0,0,0,1,'true','8','','',''),('Opportunitiesquantity_in_full_first_year_c','quantity_in_full_first_year_c','LBL_QUANTITY_IN_FULL_FIRST_YEAR ','','','Opportunities','float',18,0,'','2020-09-01 09:06:48',0,0,0,0,1,'true','2','','',''),('Opportunitiessales_stage_amount_c','sales_stage_amount_c','LBL_SALES_STAGE_AMOUNT',NULL,NULL,'Opportunities','currency',26,0,NULL,'2020-08-24 14:01:21',0,0,0,0,1,'true',NULL,NULL,NULL,NULL),('Projectjjwg_maps_address_c','jjwg_maps_address_c','LBL_JJWG_MAPS_ADDRESS','Address','Address','Project','varchar',255,0,NULL,'2019-02-21 03:39:38',0,0,0,0,1,'true',NULL,'','',''),('Projectjjwg_maps_geocode_status_c','jjwg_maps_geocode_status_c','LBL_JJWG_MAPS_GEOCODE_STATUS','Geocode Status','Geocode Status','Project','varchar',255,0,NULL,'2019-02-21 03:39:38',0,0,0,0,1,'true',NULL,'','',''),('Projectjjwg_maps_lat_c','jjwg_maps_lat_c','LBL_JJWG_MAPS_LAT','','Latitude','Project','float',10,0,'0.00000000','2019-02-21 03:39:38',0,0,0,0,1,'true','8','','',''),('Projectjjwg_maps_lng_c','jjwg_maps_lng_c','LBL_JJWG_MAPS_LNG','','Longitude','Project','float',11,0,'0.00000000','2019-02-21 03:39:38',0,0,0,0,1,'true','8','','',''),('Prospectsjjwg_maps_address_c','jjwg_maps_address_c','LBL_JJWG_MAPS_ADDRESS','Address','Address','Prospects','varchar',255,0,NULL,'2019-02-21 03:39:38',0,0,0,0,1,'true',NULL,'','',''),('Prospectsjjwg_maps_geocode_status_c','jjwg_maps_geocode_status_c','LBL_JJWG_MAPS_GEOCODE_STATUS','Geocode Status','Geocode Status','Prospects','varchar',255,0,NULL,'2019-02-21 03:39:38',0,0,0,0,1,'true',NULL,'','',''),('Prospectsjjwg_maps_lat_c','jjwg_maps_lat_c','LBL_JJWG_MAPS_LAT','','Latitude','Prospects','float',10,0,'0.00000000','2019-02-21 03:39:38',0,0,0,0,1,'true','8','','',''),('Prospectsjjwg_maps_lng_c','jjwg_maps_lng_c','LBL_JJWG_MAPS_LNG','','Longitude','Prospects','float',11,0,'0.00000000','2019-02-21 03:39:38',0,0,0,0,1,'true','8','','','');
/*!40000 ALTER TABLE `fields_meta_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `folders`
--

DROP TABLE IF EXISTS `folders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `folders` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `folder_type` varchar(25) DEFAULT NULL,
  `parent_folder` char(36) DEFAULT NULL,
  `has_child` tinyint(1) DEFAULT '0',
  `is_group` tinyint(1) DEFAULT '0',
  `is_dynamic` tinyint(1) DEFAULT '0',
  `dynamic_query` text,
  `assign_to_id` char(36) DEFAULT NULL,
  `created_by` char(36) NOT NULL,
  `modified_by` char(36) NOT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_parent_folder` (`parent_folder`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `folders`
--

LOCK TABLES `folders` WRITE;
/*!40000 ALTER TABLE `folders` DISABLE KEYS */;
/*!40000 ALTER TABLE `folders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `folders_rel`
--

DROP TABLE IF EXISTS `folders_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `folders_rel` (
  `id` char(36) NOT NULL,
  `folder_id` char(36) NOT NULL,
  `polymorphic_module` varchar(25) DEFAULT NULL,
  `polymorphic_id` char(36) NOT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_poly_module_poly_id` (`polymorphic_module`,`polymorphic_id`),
  KEY `idx_fr_id_deleted_poly` (`folder_id`,`deleted`,`polymorphic_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `folders_rel`
--

LOCK TABLES `folders_rel` WRITE;
/*!40000 ALTER TABLE `folders_rel` DISABLE KEYS */;
/*!40000 ALTER TABLE `folders_rel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `folders_subscriptions`
--

DROP TABLE IF EXISTS `folders_subscriptions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `folders_subscriptions` (
  `id` char(36) NOT NULL,
  `folder_id` char(36) NOT NULL,
  `assigned_user_id` char(36) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_folder_id_assigned_user_id` (`folder_id`,`assigned_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `folders_subscriptions`
--

LOCK TABLES `folders_subscriptions` WRITE;
/*!40000 ALTER TABLE `folders_subscriptions` DISABLE KEYS */;
/*!40000 ALTER TABLE `folders_subscriptions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fp_event_locations`
--

DROP TABLE IF EXISTS `fp_event_locations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fp_event_locations` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `address_city` varchar(100) DEFAULT NULL,
  `address_country` varchar(100) DEFAULT NULL,
  `address_postalcode` varchar(20) DEFAULT NULL,
  `address_state` varchar(100) DEFAULT NULL,
  `capacity` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fp_event_locations`
--

LOCK TABLES `fp_event_locations` WRITE;
/*!40000 ALTER TABLE `fp_event_locations` DISABLE KEYS */;
/*!40000 ALTER TABLE `fp_event_locations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fp_event_locations_audit`
--

DROP TABLE IF EXISTS `fp_event_locations_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fp_event_locations_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_fp_event_locations_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fp_event_locations_audit`
--

LOCK TABLES `fp_event_locations_audit` WRITE;
/*!40000 ALTER TABLE `fp_event_locations_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `fp_event_locations_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fp_event_locations_fp_events_1_c`
--

DROP TABLE IF EXISTS `fp_event_locations_fp_events_1_c`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fp_event_locations_fp_events_1_c` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `fp_event_locations_fp_events_1fp_event_locations_ida` varchar(36) DEFAULT NULL,
  `fp_event_locations_fp_events_1fp_events_idb` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fp_event_locations_fp_events_1_ida1` (`fp_event_locations_fp_events_1fp_event_locations_ida`),
  KEY `fp_event_locations_fp_events_1_alt` (`fp_event_locations_fp_events_1fp_events_idb`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fp_event_locations_fp_events_1_c`
--

LOCK TABLES `fp_event_locations_fp_events_1_c` WRITE;
/*!40000 ALTER TABLE `fp_event_locations_fp_events_1_c` DISABLE KEYS */;
/*!40000 ALTER TABLE `fp_event_locations_fp_events_1_c` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fp_events`
--

DROP TABLE IF EXISTS `fp_events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fp_events` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `duration_hours` int(3) DEFAULT NULL,
  `duration_minutes` int(2) DEFAULT NULL,
  `date_start` datetime DEFAULT NULL,
  `date_end` datetime DEFAULT NULL,
  `budget` decimal(26,6) DEFAULT NULL,
  `currency_id` char(36) DEFAULT NULL,
  `invite_templates` varchar(100) DEFAULT NULL,
  `accept_redirect` varchar(255) DEFAULT NULL,
  `decline_redirect` varchar(255) DEFAULT NULL,
  `activity_status_type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fp_events`
--

LOCK TABLES `fp_events` WRITE;
/*!40000 ALTER TABLE `fp_events` DISABLE KEYS */;
/*!40000 ALTER TABLE `fp_events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fp_events_audit`
--

DROP TABLE IF EXISTS `fp_events_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fp_events_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_fp_events_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fp_events_audit`
--

LOCK TABLES `fp_events_audit` WRITE;
/*!40000 ALTER TABLE `fp_events_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `fp_events_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fp_events_contacts_c`
--

DROP TABLE IF EXISTS `fp_events_contacts_c`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fp_events_contacts_c` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `fp_events_contactsfp_events_ida` varchar(36) DEFAULT NULL,
  `fp_events_contactscontacts_idb` varchar(36) DEFAULT NULL,
  `invite_status` varchar(25) DEFAULT 'Not Invited',
  `accept_status` varchar(25) DEFAULT 'No Response',
  `email_responded` int(2) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fp_events_contacts_alt` (`fp_events_contactsfp_events_ida`,`fp_events_contactscontacts_idb`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fp_events_contacts_c`
--

LOCK TABLES `fp_events_contacts_c` WRITE;
/*!40000 ALTER TABLE `fp_events_contacts_c` DISABLE KEYS */;
/*!40000 ALTER TABLE `fp_events_contacts_c` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fp_events_fp_event_delegates_1_c`
--

DROP TABLE IF EXISTS `fp_events_fp_event_delegates_1_c`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fp_events_fp_event_delegates_1_c` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `fp_events_fp_event_delegates_1fp_events_ida` varchar(36) DEFAULT NULL,
  `fp_events_fp_event_delegates_1fp_event_delegates_idb` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fp_events_fp_event_delegates_1_ida1` (`fp_events_fp_event_delegates_1fp_events_ida`),
  KEY `fp_events_fp_event_delegates_1_alt` (`fp_events_fp_event_delegates_1fp_event_delegates_idb`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fp_events_fp_event_delegates_1_c`
--

LOCK TABLES `fp_events_fp_event_delegates_1_c` WRITE;
/*!40000 ALTER TABLE `fp_events_fp_event_delegates_1_c` DISABLE KEYS */;
/*!40000 ALTER TABLE `fp_events_fp_event_delegates_1_c` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fp_events_fp_event_locations_1_c`
--

DROP TABLE IF EXISTS `fp_events_fp_event_locations_1_c`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fp_events_fp_event_locations_1_c` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `fp_events_fp_event_locations_1fp_events_ida` varchar(36) DEFAULT NULL,
  `fp_events_fp_event_locations_1fp_event_locations_idb` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fp_events_fp_event_locations_1_alt` (`fp_events_fp_event_locations_1fp_events_ida`,`fp_events_fp_event_locations_1fp_event_locations_idb`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fp_events_fp_event_locations_1_c`
--

LOCK TABLES `fp_events_fp_event_locations_1_c` WRITE;
/*!40000 ALTER TABLE `fp_events_fp_event_locations_1_c` DISABLE KEYS */;
/*!40000 ALTER TABLE `fp_events_fp_event_locations_1_c` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fp_events_leads_1_c`
--

DROP TABLE IF EXISTS `fp_events_leads_1_c`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fp_events_leads_1_c` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `fp_events_leads_1fp_events_ida` varchar(36) DEFAULT NULL,
  `fp_events_leads_1leads_idb` varchar(36) DEFAULT NULL,
  `invite_status` varchar(25) DEFAULT 'Not Invited',
  `accept_status` varchar(25) DEFAULT 'No Response',
  `email_responded` int(2) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fp_events_leads_1_alt` (`fp_events_leads_1fp_events_ida`,`fp_events_leads_1leads_idb`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fp_events_leads_1_c`
--

LOCK TABLES `fp_events_leads_1_c` WRITE;
/*!40000 ALTER TABLE `fp_events_leads_1_c` DISABLE KEYS */;
/*!40000 ALTER TABLE `fp_events_leads_1_c` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fp_events_prospects_1_c`
--

DROP TABLE IF EXISTS `fp_events_prospects_1_c`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fp_events_prospects_1_c` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `fp_events_prospects_1fp_events_ida` varchar(36) DEFAULT NULL,
  `fp_events_prospects_1prospects_idb` varchar(36) DEFAULT NULL,
  `invite_status` varchar(25) DEFAULT 'Not Invited',
  `accept_status` varchar(25) DEFAULT 'No Response',
  `email_responded` int(2) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fp_events_prospects_1_alt` (`fp_events_prospects_1fp_events_ida`,`fp_events_prospects_1prospects_idb`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fp_events_prospects_1_c`
--

LOCK TABLES `fp_events_prospects_1_c` WRITE;
/*!40000 ALTER TABLE `fp_events_prospects_1_c` DISABLE KEYS */;
/*!40000 ALTER TABLE `fp_events_prospects_1_c` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `import_maps`
--

DROP TABLE IF EXISTS `import_maps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `import_maps` (
  `id` char(36) NOT NULL,
  `name` varchar(254) DEFAULT NULL,
  `source` varchar(36) DEFAULT NULL,
  `enclosure` varchar(1) DEFAULT ' ',
  `delimiter` varchar(1) DEFAULT ',',
  `module` varchar(36) DEFAULT NULL,
  `content` text,
  `default_values` text,
  `has_header` tinyint(1) DEFAULT '1',
  `deleted` tinyint(1) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `assigned_user_id` char(36) DEFAULT NULL,
  `is_published` varchar(3) DEFAULT 'no',
  PRIMARY KEY (`id`),
  KEY `idx_owner_module_name` (`assigned_user_id`,`module`,`name`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `import_maps`
--

LOCK TABLES `import_maps` WRITE;
/*!40000 ALTER TABLE `import_maps` DISABLE KEYS */;
/*!40000 ALTER TABLE `import_maps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inbound_email`
--

DROP TABLE IF EXISTS `inbound_email`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inbound_email` (
  `id` varchar(36) NOT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `status` varchar(100) DEFAULT 'Active',
  `server_url` varchar(100) DEFAULT NULL,
  `email_user` varchar(100) DEFAULT NULL,
  `email_password` varchar(100) DEFAULT NULL,
  `port` int(5) DEFAULT NULL,
  `service` varchar(50) DEFAULT NULL,
  `mailbox` text,
  `delete_seen` tinyint(1) DEFAULT '0',
  `mailbox_type` varchar(10) DEFAULT NULL,
  `template_id` char(36) DEFAULT NULL,
  `stored_options` text,
  `group_id` char(36) DEFAULT NULL,
  `is_personal` tinyint(1) DEFAULT '0',
  `groupfolder_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inbound_email`
--

LOCK TABLES `inbound_email` WRITE;
/*!40000 ALTER TABLE `inbound_email` DISABLE KEYS */;
/*!40000 ALTER TABLE `inbound_email` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inbound_email_autoreply`
--

DROP TABLE IF EXISTS `inbound_email_autoreply`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inbound_email_autoreply` (
  `id` char(36) NOT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `autoreplied_to` varchar(100) DEFAULT NULL,
  `ie_id` char(36) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_ie_autoreplied_to` (`autoreplied_to`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inbound_email_autoreply`
--

LOCK TABLES `inbound_email_autoreply` WRITE;
/*!40000 ALTER TABLE `inbound_email_autoreply` DISABLE KEYS */;
/*!40000 ALTER TABLE `inbound_email_autoreply` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inbound_email_cache_ts`
--

DROP TABLE IF EXISTS `inbound_email_cache_ts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inbound_email_cache_ts` (
  `id` varchar(255) NOT NULL,
  `ie_timestamp` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inbound_email_cache_ts`
--

LOCK TABLES `inbound_email_cache_ts` WRITE;
/*!40000 ALTER TABLE `inbound_email_cache_ts` DISABLE KEYS */;
/*!40000 ALTER TABLE `inbound_email_cache_ts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jjwg_address_cache`
--

DROP TABLE IF EXISTS `jjwg_address_cache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jjwg_address_cache` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `lat` float(10,8) DEFAULT NULL,
  `lng` float(11,8) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jjwg_address_cache`
--

LOCK TABLES `jjwg_address_cache` WRITE;
/*!40000 ALTER TABLE `jjwg_address_cache` DISABLE KEYS */;
/*!40000 ALTER TABLE `jjwg_address_cache` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jjwg_address_cache_audit`
--

DROP TABLE IF EXISTS `jjwg_address_cache_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jjwg_address_cache_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_jjwg_address_cache_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jjwg_address_cache_audit`
--

LOCK TABLES `jjwg_address_cache_audit` WRITE;
/*!40000 ALTER TABLE `jjwg_address_cache_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `jjwg_address_cache_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jjwg_areas`
--

DROP TABLE IF EXISTS `jjwg_areas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jjwg_areas` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `coordinates` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jjwg_areas`
--

LOCK TABLES `jjwg_areas` WRITE;
/*!40000 ALTER TABLE `jjwg_areas` DISABLE KEYS */;
/*!40000 ALTER TABLE `jjwg_areas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jjwg_areas_audit`
--

DROP TABLE IF EXISTS `jjwg_areas_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jjwg_areas_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_jjwg_areas_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jjwg_areas_audit`
--

LOCK TABLES `jjwg_areas_audit` WRITE;
/*!40000 ALTER TABLE `jjwg_areas_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `jjwg_areas_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jjwg_maps`
--

DROP TABLE IF EXISTS `jjwg_maps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jjwg_maps` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `distance` float(9,4) DEFAULT NULL,
  `unit_type` varchar(100) DEFAULT 'mi',
  `module_type` varchar(100) DEFAULT 'Accounts',
  `parent_type` varchar(255) DEFAULT NULL,
  `parent_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jjwg_maps`
--

LOCK TABLES `jjwg_maps` WRITE;
/*!40000 ALTER TABLE `jjwg_maps` DISABLE KEYS */;
/*!40000 ALTER TABLE `jjwg_maps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jjwg_maps_audit`
--

DROP TABLE IF EXISTS `jjwg_maps_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jjwg_maps_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_jjwg_maps_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jjwg_maps_audit`
--

LOCK TABLES `jjwg_maps_audit` WRITE;
/*!40000 ALTER TABLE `jjwg_maps_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `jjwg_maps_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jjwg_maps_jjwg_areas_c`
--

DROP TABLE IF EXISTS `jjwg_maps_jjwg_areas_c`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jjwg_maps_jjwg_areas_c` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `jjwg_maps_5304wg_maps_ida` varchar(36) DEFAULT NULL,
  `jjwg_maps_41f2g_areas_idb` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `jjwg_maps_jjwg_areas_alt` (`jjwg_maps_5304wg_maps_ida`,`jjwg_maps_41f2g_areas_idb`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jjwg_maps_jjwg_areas_c`
--

LOCK TABLES `jjwg_maps_jjwg_areas_c` WRITE;
/*!40000 ALTER TABLE `jjwg_maps_jjwg_areas_c` DISABLE KEYS */;
/*!40000 ALTER TABLE `jjwg_maps_jjwg_areas_c` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jjwg_maps_jjwg_markers_c`
--

DROP TABLE IF EXISTS `jjwg_maps_jjwg_markers_c`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jjwg_maps_jjwg_markers_c` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `jjwg_maps_b229wg_maps_ida` varchar(36) DEFAULT NULL,
  `jjwg_maps_2e31markers_idb` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `jjwg_maps_jjwg_markers_alt` (`jjwg_maps_b229wg_maps_ida`,`jjwg_maps_2e31markers_idb`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jjwg_maps_jjwg_markers_c`
--

LOCK TABLES `jjwg_maps_jjwg_markers_c` WRITE;
/*!40000 ALTER TABLE `jjwg_maps_jjwg_markers_c` DISABLE KEYS */;
/*!40000 ALTER TABLE `jjwg_maps_jjwg_markers_c` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jjwg_markers`
--

DROP TABLE IF EXISTS `jjwg_markers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jjwg_markers` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `jjwg_maps_lat` float(10,8) DEFAULT '0.00000000',
  `jjwg_maps_lng` float(11,8) DEFAULT '0.00000000',
  `marker_image` varchar(100) DEFAULT 'company',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jjwg_markers`
--

LOCK TABLES `jjwg_markers` WRITE;
/*!40000 ALTER TABLE `jjwg_markers` DISABLE KEYS */;
/*!40000 ALTER TABLE `jjwg_markers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jjwg_markers_audit`
--

DROP TABLE IF EXISTS `jjwg_markers_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jjwg_markers_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_jjwg_markers_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jjwg_markers_audit`
--

LOCK TABLES `jjwg_markers_audit` WRITE;
/*!40000 ALTER TABLE `jjwg_markers_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `jjwg_markers_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `job_queue`
--

DROP TABLE IF EXISTS `job_queue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job_queue` (
  `assigned_user_id` char(36) DEFAULT NULL,
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `scheduler_id` char(36) DEFAULT NULL,
  `execute_time` datetime DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `resolution` varchar(20) DEFAULT NULL,
  `message` text,
  `target` varchar(255) DEFAULT NULL,
  `data` text,
  `requeue` tinyint(1) DEFAULT '0',
  `retry_count` tinyint(4) DEFAULT NULL,
  `failure_count` tinyint(4) DEFAULT NULL,
  `job_delay` int(11) DEFAULT NULL,
  `client` varchar(255) DEFAULT NULL,
  `percent_complete` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_status_scheduler` (`status`,`scheduler_id`),
  KEY `idx_status_time` (`status`,`execute_time`,`date_entered`),
  KEY `idx_status_entered` (`status`,`date_entered`),
  KEY `idx_status_modified` (`status`,`date_modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job_queue`
--

LOCK TABLES `job_queue` WRITE;
/*!40000 ALTER TABLE `job_queue` DISABLE KEYS */;
/*!40000 ALTER TABLE `job_queue` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `leads`
--

DROP TABLE IF EXISTS `leads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `leads` (
  `id` char(36) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `salutation` varchar(255) DEFAULT NULL,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `department` varchar(100) DEFAULT NULL,
  `do_not_call` tinyint(1) DEFAULT '0',
  `phone_home` varchar(100) DEFAULT NULL,
  `phone_mobile` varchar(100) DEFAULT NULL,
  `phone_work` varchar(100) DEFAULT NULL,
  `phone_other` varchar(100) DEFAULT NULL,
  `phone_fax` varchar(100) DEFAULT NULL,
  `lawful_basis` text,
  `date_reviewed` date DEFAULT NULL,
  `lawful_basis_source` varchar(100) DEFAULT NULL,
  `primary_address_street` varchar(150) DEFAULT NULL,
  `primary_address_city` varchar(100) DEFAULT NULL,
  `primary_address_state` varchar(100) DEFAULT NULL,
  `primary_address_postalcode` varchar(20) DEFAULT NULL,
  `primary_address_country` varchar(255) DEFAULT NULL,
  `alt_address_street` varchar(150) DEFAULT NULL,
  `alt_address_city` varchar(100) DEFAULT NULL,
  `alt_address_state` varchar(100) DEFAULT NULL,
  `alt_address_postalcode` varchar(20) DEFAULT NULL,
  `alt_address_country` varchar(255) DEFAULT NULL,
  `assistant` varchar(75) DEFAULT NULL,
  `assistant_phone` varchar(100) DEFAULT NULL,
  `converted` tinyint(1) DEFAULT '0',
  `refered_by` varchar(100) DEFAULT NULL,
  `lead_source` varchar(100) DEFAULT NULL,
  `lead_source_description` text,
  `status` varchar(100) DEFAULT NULL,
  `status_description` text,
  `reports_to_id` char(36) DEFAULT NULL,
  `account_name` varchar(255) DEFAULT NULL,
  `account_description` text,
  `contact_id` char(36) DEFAULT NULL,
  `account_id` char(36) DEFAULT NULL,
  `opportunity_id` char(36) DEFAULT NULL,
  `opportunity_name` varchar(255) DEFAULT NULL,
  `opportunity_amount` varchar(50) DEFAULT NULL,
  `campaign_id` char(36) DEFAULT NULL,
  `birthdate` date DEFAULT NULL,
  `portal_name` varchar(255) DEFAULT NULL,
  `portal_app` varchar(255) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_lead_acct_name_first` (`account_name`,`deleted`),
  KEY `idx_lead_last_first` (`last_name`,`first_name`,`deleted`),
  KEY `idx_lead_del_stat` (`last_name`,`status`,`deleted`,`first_name`),
  KEY `idx_lead_opp_del` (`opportunity_id`,`deleted`),
  KEY `idx_leads_acct_del` (`account_id`,`deleted`),
  KEY `idx_del_user` (`deleted`,`assigned_user_id`),
  KEY `idx_lead_assigned` (`assigned_user_id`),
  KEY `idx_lead_contact` (`contact_id`),
  KEY `idx_reports_to` (`reports_to_id`),
  KEY `idx_lead_phone_work` (`phone_work`),
  KEY `idx_leads_id_del` (`id`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `leads`
--

LOCK TABLES `leads` WRITE;
/*!40000 ALTER TABLE `leads` DISABLE KEYS */;
INSERT INTO `leads` VALUES ('2aab764c-a53d-2a20-e944-5f43bff03c55','2020-08-24 13:24:51','2020-08-24 13:24:51','1','1','',0,'1','','Susie','Morgan','COO',NULL,'Operations',0,NULL,'(262)951-0673','',NULL,'',NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,0,'','','','New','',NULL,'Pharmaceutical Company',NULL,'','','',NULL,'','',NULL,NULL,NULL,'http://'),('3977c53e-104f-b7c1-2ca8-5f48dd5f8c58','2020-08-28 10:32:56','2020-08-28 10:32:56','1674d634-fb3a-8d8b-34c1-5f4698fed125','1674d634-fb3a-8d8b-34c1-5f4698fed125',NULL,0,'1674d634-fb3a-8d8b-34c1-5f4698fed125',NULL,'Tim','China','',NULL,'',0,NULL,'','',NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'','',NULL,'New',NULL,NULL,'Pfizer-China',NULL,'','4712d4f2-e7f2-593a-8243-5f469751802e','',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('e826f898-71d8-241f-2c0b-5f48da608dc4','2020-08-28 10:22:41','2020-08-28 10:22:41','8d544213-bcac-b962-fc13-5f4698c75700','8d544213-bcac-b962-fc13-5f4698c75700',NULL,0,'8d544213-bcac-b962-fc13-5f4698c75700',NULL,'Bill','India','',NULL,'',0,NULL,'','',NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'Steve India','Trade Show',NULL,'New',NULL,NULL,'Pfizer-India',NULL,'','9ae1967b-3fc4-6421-9d77-5f4697488f9e','',NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `leads` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `leads_audit`
--

DROP TABLE IF EXISTS `leads_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `leads_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_leads_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `leads_audit`
--

LOCK TABLES `leads_audit` WRITE;
/*!40000 ALTER TABLE `leads_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `leads_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `leads_cstm`
--

DROP TABLE IF EXISTS `leads_cstm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `leads_cstm` (
  `id_c` char(36) NOT NULL,
  `jjwg_maps_lng_c` float(11,8) DEFAULT '0.00000000',
  `jjwg_maps_lat_c` float(10,8) DEFAULT '0.00000000',
  `jjwg_maps_geocode_status_c` varchar(255) DEFAULT NULL,
  `jjwg_maps_address_c` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_c`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `leads_cstm`
--

LOCK TABLES `leads_cstm` WRITE;
/*!40000 ALTER TABLE `leads_cstm` DISABLE KEYS */;
INSERT INTO `leads_cstm` VALUES ('2aab764c-a53d-2a20-e944-5f43bff03c55',0.00000000,0.00000000,NULL,NULL),('3977c53e-104f-b7c1-2ca8-5f48dd5f8c58',0.00000000,0.00000000,NULL,NULL),('e826f898-71d8-241f-2c0b-5f48da608dc4',0.00000000,0.00000000,NULL,NULL);
/*!40000 ALTER TABLE `leads_cstm` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `linked_documents`
--

DROP TABLE IF EXISTS `linked_documents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `linked_documents` (
  `id` varchar(36) NOT NULL,
  `parent_id` varchar(36) DEFAULT NULL,
  `parent_type` varchar(25) DEFAULT NULL,
  `document_id` varchar(36) DEFAULT NULL,
  `document_revision_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_parent_document` (`parent_type`,`parent_id`,`document_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `linked_documents`
--

LOCK TABLES `linked_documents` WRITE;
/*!40000 ALTER TABLE `linked_documents` DISABLE KEYS */;
/*!40000 ALTER TABLE `linked_documents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meetings`
--

DROP TABLE IF EXISTS `meetings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meetings` (
  `id` char(36) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `location` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `join_url` varchar(200) DEFAULT NULL,
  `host_url` varchar(400) DEFAULT NULL,
  `displayed_url` varchar(400) DEFAULT NULL,
  `creator` varchar(50) DEFAULT NULL,
  `external_id` varchar(50) DEFAULT NULL,
  `duration_hours` int(3) DEFAULT NULL,
  `duration_minutes` int(2) DEFAULT NULL,
  `date_start` datetime DEFAULT NULL,
  `date_end` datetime DEFAULT NULL,
  `parent_type` varchar(100) DEFAULT NULL,
  `status` varchar(100) DEFAULT 'Planned',
  `type` varchar(255) DEFAULT 'Sugar',
  `parent_id` char(36) DEFAULT NULL,
  `reminder_time` int(11) DEFAULT '-1',
  `email_reminder_time` int(11) DEFAULT '-1',
  `email_reminder_sent` tinyint(1) DEFAULT '0',
  `outlook_id` varchar(255) DEFAULT NULL,
  `sequence` int(11) DEFAULT '0',
  `repeat_type` varchar(36) DEFAULT NULL,
  `repeat_interval` int(3) DEFAULT '1',
  `repeat_dow` varchar(7) DEFAULT NULL,
  `repeat_until` date DEFAULT NULL,
  `repeat_count` int(7) DEFAULT NULL,
  `repeat_parent_id` char(36) DEFAULT NULL,
  `recurring_source` varchar(36) DEFAULT NULL,
  `gsync_id` varchar(1024) DEFAULT NULL,
  `gsync_lastsync` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_mtg_name` (`name`),
  KEY `idx_meet_par_del` (`parent_id`,`parent_type`,`deleted`),
  KEY `idx_meet_stat_del` (`assigned_user_id`,`status`,`deleted`),
  KEY `idx_meet_date_start` (`date_start`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meetings`
--

LOCK TABLES `meetings` WRITE;
/*!40000 ALTER TABLE `meetings` DISABLE KEYS */;
/*!40000 ALTER TABLE `meetings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meetings_contacts`
--

DROP TABLE IF EXISTS `meetings_contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meetings_contacts` (
  `id` varchar(36) NOT NULL,
  `meeting_id` varchar(36) DEFAULT NULL,
  `contact_id` varchar(36) DEFAULT NULL,
  `required` varchar(1) DEFAULT '1',
  `accept_status` varchar(25) DEFAULT 'none',
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_con_mtg_mtg` (`meeting_id`),
  KEY `idx_con_mtg_con` (`contact_id`),
  KEY `idx_meeting_contact` (`meeting_id`,`contact_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meetings_contacts`
--

LOCK TABLES `meetings_contacts` WRITE;
/*!40000 ALTER TABLE `meetings_contacts` DISABLE KEYS */;
/*!40000 ALTER TABLE `meetings_contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meetings_cstm`
--

DROP TABLE IF EXISTS `meetings_cstm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meetings_cstm` (
  `id_c` char(36) NOT NULL,
  `jjwg_maps_lng_c` float(11,8) DEFAULT '0.00000000',
  `jjwg_maps_lat_c` float(10,8) DEFAULT '0.00000000',
  `jjwg_maps_geocode_status_c` varchar(255) DEFAULT NULL,
  `jjwg_maps_address_c` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_c`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meetings_cstm`
--

LOCK TABLES `meetings_cstm` WRITE;
/*!40000 ALTER TABLE `meetings_cstm` DISABLE KEYS */;
/*!40000 ALTER TABLE `meetings_cstm` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meetings_leads`
--

DROP TABLE IF EXISTS `meetings_leads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meetings_leads` (
  `id` varchar(36) NOT NULL,
  `meeting_id` varchar(36) DEFAULT NULL,
  `lead_id` varchar(36) DEFAULT NULL,
  `required` varchar(1) DEFAULT '1',
  `accept_status` varchar(25) DEFAULT 'none',
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_lead_meeting_meeting` (`meeting_id`),
  KEY `idx_lead_meeting_lead` (`lead_id`),
  KEY `idx_meeting_lead` (`meeting_id`,`lead_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meetings_leads`
--

LOCK TABLES `meetings_leads` WRITE;
/*!40000 ALTER TABLE `meetings_leads` DISABLE KEYS */;
/*!40000 ALTER TABLE `meetings_leads` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meetings_users`
--

DROP TABLE IF EXISTS `meetings_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meetings_users` (
  `id` varchar(36) NOT NULL,
  `meeting_id` varchar(36) DEFAULT NULL,
  `user_id` varchar(36) DEFAULT NULL,
  `required` varchar(1) DEFAULT '1',
  `accept_status` varchar(25) DEFAULT 'none',
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_usr_mtg_mtg` (`meeting_id`),
  KEY `idx_usr_mtg_usr` (`user_id`),
  KEY `idx_meeting_users` (`meeting_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meetings_users`
--

LOCK TABLES `meetings_users` WRITE;
/*!40000 ALTER TABLE `meetings_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `meetings_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notes`
--

DROP TABLE IF EXISTS `notes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notes` (
  `assigned_user_id` char(36) DEFAULT NULL,
  `id` char(36) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `file_mime_type` varchar(100) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `parent_type` varchar(255) DEFAULT NULL,
  `parent_id` char(36) DEFAULT NULL,
  `contact_id` char(36) DEFAULT NULL,
  `portal_flag` tinyint(1) DEFAULT NULL,
  `embed_flag` tinyint(1) DEFAULT '0',
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_note_name` (`name`),
  KEY `idx_notes_parent` (`parent_id`,`parent_type`),
  KEY `idx_note_contact` (`contact_id`),
  KEY `idx_notes_assigned_del` (`deleted`,`assigned_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notes`
--

LOCK TABLES `notes` WRITE;
/*!40000 ALTER TABLE `notes` DISABLE KEYS */;
INSERT INTO `notes` VALUES ('8d544213-bcac-b962-fc13-5f4698c75700','b39d0ada-3a30-6236-aefe-5f48dadba3c3','2020-08-28 10:21:41','2020-08-28 10:21:41','8d544213-bcac-b962-fc13-5f4698c75700','8d544213-bcac-b962-fc13-5f4698c75700','Note Test',NULL,'','Accounts','9ae1967b-3fc4-6421-9d77-5f4697488f9e','',0,0,'Pfizer India prefers to receive quotes in USD.',0);
/*!40000 ALTER TABLE `notes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth2clients`
--

DROP TABLE IF EXISTS `oauth2clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth2clients` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `secret` varchar(4000) DEFAULT NULL,
  `redirect_url` varchar(255) DEFAULT NULL,
  `is_confidential` tinyint(1) DEFAULT '1',
  `allowed_grant_type` varchar(255) DEFAULT 'password',
  `duration_value` int(11) DEFAULT NULL,
  `duration_amount` int(11) DEFAULT NULL,
  `duration_unit` varchar(255) DEFAULT 'Duration Unit',
  `assigned_user_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth2clients`
--

LOCK TABLES `oauth2clients` WRITE;
/*!40000 ALTER TABLE `oauth2clients` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth2clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth2tokens`
--

DROP TABLE IF EXISTS `oauth2tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth2tokens` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `token_is_revoked` tinyint(1) DEFAULT NULL,
  `token_type` varchar(255) DEFAULT NULL,
  `access_token_expires` datetime DEFAULT NULL,
  `access_token` varchar(4000) DEFAULT NULL,
  `refresh_token` varchar(4000) DEFAULT NULL,
  `refresh_token_expires` datetime DEFAULT NULL,
  `grant_type` varchar(255) DEFAULT NULL,
  `state` varchar(1024) DEFAULT NULL,
  `client` char(36) DEFAULT NULL,
  `assigned_user_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth2tokens`
--

LOCK TABLES `oauth2tokens` WRITE;
/*!40000 ALTER TABLE `oauth2tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth2tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_consumer`
--

DROP TABLE IF EXISTS `oauth_consumer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_consumer` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `c_key` varchar(255) DEFAULT NULL,
  `c_secret` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ckey` (`c_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_consumer`
--

LOCK TABLES `oauth_consumer` WRITE;
/*!40000 ALTER TABLE `oauth_consumer` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_consumer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_nonce`
--

DROP TABLE IF EXISTS `oauth_nonce`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_nonce` (
  `conskey` varchar(32) NOT NULL,
  `nonce` varchar(32) NOT NULL,
  `nonce_ts` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`conskey`,`nonce`),
  KEY `oauth_nonce_keyts` (`conskey`,`nonce_ts`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_nonce`
--

LOCK TABLES `oauth_nonce` WRITE;
/*!40000 ALTER TABLE `oauth_nonce` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_nonce` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_tokens`
--

DROP TABLE IF EXISTS `oauth_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_tokens` (
  `id` char(36) NOT NULL,
  `secret` varchar(32) DEFAULT NULL,
  `tstate` varchar(1) DEFAULT NULL,
  `consumer` char(36) NOT NULL,
  `token_ts` bigint(20) DEFAULT NULL,
  `verify` varchar(32) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `callback_url` varchar(255) DEFAULT NULL,
  `assigned_user_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`,`deleted`),
  KEY `oauth_state_ts` (`tstate`,`token_ts`),
  KEY `constoken_key` (`consumer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_tokens`
--

LOCK TABLES `oauth_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `opportunities`
--

DROP TABLE IF EXISTS `opportunities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `opportunities` (
  `id` char(36) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `opportunity_type` varchar(255) DEFAULT NULL,
  `campaign_id` char(36) DEFAULT NULL,
  `lead_source` varchar(50) DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `amount_usdollar` double DEFAULT NULL,
  `currency_id` char(36) DEFAULT NULL,
  `date_closed` date DEFAULT NULL,
  `next_step` varchar(100) DEFAULT NULL,
  `sales_stage` varchar(255) DEFAULT NULL,
  `probability` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_opp_name` (`name`),
  KEY `idx_opp_assigned` (`assigned_user_id`),
  KEY `idx_opp_id_deleted` (`id`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `opportunities`
--

LOCK TABLES `opportunities` WRITE;
/*!40000 ALTER TABLE `opportunities` DISABLE KEYS */;
INSERT INTO `opportunities` VALUES ('12c3340b-0f62-8d28-b800-5f48db0951f9','Test Opp 2','2020-08-28 10:25:25','2020-08-28 10:25:25','8d544213-bcac-b962-fc13-5f4698c75700','8d544213-bcac-b962-fc13-5f4698c75700','',0,'8d544213-bcac-b962-fc13-5f4698c75700','New Business','','Employee',90000,90000,'-99','2020-09-16','','Negotiation/Review',75),('170ab7e4-7129-fd99-34c8-5f3fad24bc03','Nutri Test 1','2020-08-21 11:19:51','2020-08-28 09:24:20','1','1',NULL,0,'1',NULL,'','Trade Show',12000,12000,'-99','2020-08-27',NULL,'Perception Analysis',50),('1ee05743-c112-d3a1-163a-5f48dd532edf','Test Opp 3','2020-08-28 10:31:57','2020-08-28 10:31:57','1674d634-fb3a-8d8b-34c1-5f4698fed125','1674d634-fb3a-8d8b-34c1-5f4698fed125','',0,'1674d634-fb3a-8d8b-34c1-5f4698fed125','','','',9090,9090,'-99','2020-09-25','','Perception Analysis',50),('4c12f4da-751b-5429-80ea-5f43bed3071f','Pharma Test 2','2020-08-24 13:21:35','2020-08-25 18:08:55','1','1','Pharma company is considering doing a deal for xyz.',0,'dc26b995-5550-b848-6f73-5e90aa679348','Existing Business','','Partner',10000,10000,'-99','2020-08-25','Meet in-person','Closed Lost',0),('68b8f5a5-cd40-decc-cebb-5f48da51a3f2','Test Opp 1','2020-08-28 10:20:11','2020-08-28 10:20:11','8d544213-bcac-b962-fc13-5f4698c75700','8d544213-bcac-b962-fc13-5f4698c75700','',0,'8d544213-bcac-b962-fc13-5f4698c75700','Existing Business','','Self Generated',12200,12200,'-99','2020-08-31','Meeting 8/31','Needs Analysis',25),('9583611d-7521-f3a8-6bcd-5f43bc7dfab0','Nutri Test 2','2020-08-24 13:10:19','2020-08-25 11:48:30','1','1',NULL,0,'1','Existing Business','','Trade Show',4000,4000,'-99','2020-08-24',NULL,'Perception Analysis',50),('a57b6fdf-beda-0f2f-164f-5f48dd72c10f','Test Opp 4','2020-08-28 10:32:19','2020-08-28 10:32:19','1674d634-fb3a-8d8b-34c1-5f4698fed125','1674d634-fb3a-8d8b-34c1-5f4698fed125','',0,'1674d634-fb3a-8d8b-34c1-5f4698fed125','','','',80000,80000,'-99','2020-09-25','','Prospecting',10),('ab99004a-eee8-20c6-f636-5f3fad54c0d0','Pharma Test 1','2020-08-21 11:19:15','2020-08-24 15:03:45','1','1',NULL,0,'1','Existing Business','','Partner',60000,60000,'-99','2020-08-28',NULL,'Needs Analysis',25),('cf6e07ce-3763-a98c-69d1-5f44fa63cfd9','Nutri Test 4','2020-08-25 11:46:11','2020-08-28 09:24:33','1','1',NULL,0,'1','Existing Business','','Trade Show',12000,12000,'-99','2020-08-31','Schedule a call','Needs Analysis',25),('f10d6a43-2a2f-d743-d2ce-5f43bca15ede','Nutri Test 3','2020-08-24 13:10:49','2020-08-28 09:24:24','1','1',NULL,0,'1',NULL,'','Self Generated',8000,8000,'-99','2020-08-28',NULL,'Prospecting',10);
/*!40000 ALTER TABLE `opportunities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `opportunities_audit`
--

DROP TABLE IF EXISTS `opportunities_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `opportunities_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_opportunities_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `opportunities_audit`
--

LOCK TABLES `opportunities_audit` WRITE;
/*!40000 ALTER TABLE `opportunities_audit` DISABLE KEYS */;
INSERT INTO `opportunities_audit` VALUES ('12826525-4620-e85f-8aae-5f43d2489dc8','9583611d-7521-f3a8-6bcd-5f43bc7dfab0','2020-08-24 14:47:23','1','sales_stage','enum','Needs Analysis','Perception Analysis',NULL,NULL),('12d249dd-30c4-e0db-03ac-5f43d2a4d5c9','9583611d-7521-f3a8-6bcd-5f43bc7dfab0','2020-08-24 14:47:23','1','probability','int','25','50',NULL,NULL),('14b77723-d8f5-e51a-0580-5f3fb0b8cb86','ab99004a-eee8-20c6-f636-5f3fad54c0d0','2020-08-21 11:31:54','1','sales_stage','enum','Prospecting','Closed Lost',NULL,NULL),('17a07155-ab17-3879-ba60-5f4016312829','170ab7e4-7129-fd99-34c8-5f3fad24bc03','2020-08-21 18:46:27','1','sales_stage','enum','Closed Lost','Prospecting',NULL,NULL),('17ca63bc-0e18-ec8e-697c-5f43d2c40935','9583611d-7521-f3a8-6bcd-5f43bc7dfab0','2020-08-24 14:44:11','1','sales_stage','enum','Perception Analysis','Needs Analysis',NULL,NULL),('180fc5bd-9617-23fa-4ece-5f43d24f8ed6','9583611d-7521-f3a8-6bcd-5f43bc7dfab0','2020-08-24 14:44:11','1','probability','int','10','25',NULL,NULL),('18749cd8-d878-1f42-5193-5f43d6291142','f10d6a43-2a2f-d743-d2ce-5f43bca15ede','2020-08-24 15:02:04','1','sales_stage','enum','Prospecting','Closed Lost',NULL,NULL),('18acad62-ea66-2a40-b361-5f43d6a01d52','f10d6a43-2a2f-d743-d2ce-5f43bca15ede','2020-08-24 15:02:04','1','probability','int','10','0',NULL,NULL),('1a69ec5e-7823-72de-35a5-5f3fba82b2f2','ab99004a-eee8-20c6-f636-5f3fad54c0d0','2020-08-21 12:13:55','1','sales_stage','enum','Needs Analysis','Perception Analysis',NULL,NULL),('1b9ebc01-b418-7956-3724-5f43bcfda886','170ab7e4-7129-fd99-34c8-5f3fad24bc03','2020-08-24 13:11:39','1','sales_stage','enum','Prospecting','Needs Analysis',NULL,NULL),('276090c9-9602-c9bb-e2ad-5f3fb0c1a1eb','170ab7e4-7129-fd99-34c8-5f3fad24bc03','2020-08-21 11:32:01','1','sales_stage','enum','Prospecting','Closed Lost',NULL,NULL),('27c9a3ec-eea4-29e1-a539-5f43bd7a7236','f10d6a43-2a2f-d743-d2ce-5f43bca15ede','2020-08-24 13:13:36','1','sales_stage','enum','Negotiation/Review','Prospecting',NULL,NULL),('2d5204da-183b-7585-8931-5f43d761a68c','9583611d-7521-f3a8-6bcd-5f43bc7dfab0','2020-08-24 15:04:42','1','sales_stage','enum','Needs Analysis','Perception Analysis',NULL,NULL),('2ec2c603-2183-0553-ecb4-5f3fb08ec220','ab99004a-eee8-20c6-f636-5f3fad54c0d0','2020-08-21 11:32:04','1','sales_stage','enum','Closed Lost','Prospecting',NULL,NULL),('30453f41-5dbd-7d3b-db88-5f3fad198574','ab99004a-eee8-20c6-f636-5f3fad54c0d0','2020-08-21 11:20:03','1','sales_stage','enum','Closed Lost','Prospecting',NULL,NULL),('3b945c37-0d7d-9746-9b54-5f3fad6e5231','ab99004a-eee8-20c6-f636-5f3fad54c0d0','2020-08-21 11:20:20','1','sales_stage','enum','Prospecting','Needs Analysis',NULL,NULL),('3e8e2aff-ac40-0433-af6e-5f43bd3e7e0e','9583611d-7521-f3a8-6bcd-5f43bc7dfab0','2020-08-24 13:13:40','1','sales_stage','enum','Closed Won','Needs Analysis',NULL,NULL),('3f8f09a0-0002-978d-c8b8-5f48cdab736e','cf6e07ce-3763-a98c-69d1-5f44fa63cfd9','2020-08-28 09:24:33','1','sales_stage','enum','Prospecting','Needs Analysis',NULL,NULL),('408143ce-01cb-43a9-11aa-5f43d6586771','f10d6a43-2a2f-d743-d2ce-5f43bca15ede','2020-08-24 15:02:15','1','sales_stage','enum','Closed Lost','Prospecting',NULL,NULL),('40be961a-6df4-bc1d-b20f-5f43d6b1733d','f10d6a43-2a2f-d743-d2ce-5f43bca15ede','2020-08-24 15:02:15','1','probability','int','0','10',NULL,NULL),('43d5cae0-7613-cbe7-260f-5f43d7b1684d','4c12f4da-751b-5429-80ea-5f43bed3071f','2020-08-24 15:05:21','1','sales_stage','enum','Prospecting','Closed Lost',NULL,NULL),('45dd1f04-2176-ab7e-6d7e-5f40156b0c3f','170ab7e4-7129-fd99-34c8-5f3fad24bc03','2020-08-21 18:40:03','1','sales_stage','enum','Prospecting','Closed Lost',NULL,NULL),('460d1516-cd91-2119-b7a1-5f44fa9a93af','cf6e07ce-3763-a98c-69d1-5f44fa63cfd9','2020-08-25 11:47:44','1','sales_stage','enum','Closed Lost','Prospecting',NULL,NULL),('4b92020c-decd-b754-7140-5f43bcf8de2e','9583611d-7521-f3a8-6bcd-5f43bc7dfab0','2020-08-24 13:11:05','1','sales_stage','enum','Closed Lost','Perception Analysis',NULL,NULL),('4ead995e-45e7-dc62-f853-5f43d607955e','ab99004a-eee8-20c6-f636-5f3fad54c0d0','2020-08-24 15:03:45','1','sales_stage','enum','Prospecting','Needs Analysis',NULL,NULL),('51af6c6c-4e21-60cf-67c1-5f43d0666ed6','f10d6a43-2a2f-d743-d2ce-5f43bca15ede','2020-08-24 14:37:20','1','sales_stage','enum','Prospecting','Closed Lost',NULL,NULL),('51f06cab-13fb-e8d1-a71a-5f43d02d955f','f10d6a43-2a2f-d743-d2ce-5f43bca15ede','2020-08-24 14:37:20','1','probability','int','80','0',NULL,NULL),('5334829e-7c53-8135-2fd4-5f3fb0ddfb29','170ab7e4-7129-fd99-34c8-5f3fad24bc03','2020-08-21 11:31:50','1','sales_stage','enum','Closed Lost','Prospecting',NULL,NULL),('5b7e24b4-403d-8192-369d-5f44dba5be58','170ab7e4-7129-fd99-34c8-5f3fad24bc03','2020-08-25 09:34:00','1','sales_stage','enum','Prospecting','Needs Analysis',NULL,NULL),('6a9477fa-7f78-4f0f-832c-5f43d6763711','170ab7e4-7129-fd99-34c8-5f3fad24bc03','2020-08-24 15:01:02','1','sales_stage','enum','Prospecting','Closed Lost',NULL,NULL),('6add239f-cdb4-f910-f59d-5f43d68a80d7','170ab7e4-7129-fd99-34c8-5f3fad24bc03','2020-08-24 15:01:02','1','probability','int','10','0',NULL,NULL),('6d076308-a315-020f-4af0-5f3fb0567a92','170ab7e4-7129-fd99-34c8-5f3fad24bc03','2020-08-21 11:33:06','1','sales_stage','enum','Prospecting','Closed Lost',NULL,NULL),('6de046d8-5b4c-d609-d28a-5f43d70390c5','170ab7e4-7129-fd99-34c8-5f3fad24bc03','2020-08-24 15:05:02','1','sales_stage','enum','Closed Lost','Prospecting',NULL,NULL),('708a6e7c-becf-e84d-cfc3-5f43d4c894d4','9583611d-7521-f3a8-6bcd-5f43bc7dfab0','2020-08-24 14:51:48','1','sales_stage','enum','Perception Analysis','Needs Analysis',NULL,NULL),('70c08811-ac31-0338-56c0-5f43d42b49a9','9583611d-7521-f3a8-6bcd-5f43bc7dfab0','2020-08-24 14:51:48','1','probability','int','50','25',NULL,NULL),('740f68ff-4e3a-9bd6-c8dc-5f45533830dc','4c12f4da-751b-5429-80ea-5f43bed3071f','2020-08-25 18:08:55','1','sales_stage','enum','Prospecting','Closed Lost',NULL,NULL),('75d2f984-6b70-3763-88b2-5f43d04252af','170ab7e4-7129-fd99-34c8-5f3fad24bc03','2020-08-24 14:37:49','1','probability','int','0','10',NULL,NULL),('7ccce3da-3e18-4d10-7ec4-5f43bc9ede5c','170ab7e4-7129-fd99-34c8-5f3fad24bc03','2020-08-24 13:11:51','1','sales_stage','enum','Needs Analysis','Prospecting',NULL,NULL),('7f1586d8-f1c8-2fa1-c89a-5f3faead8a97','ab99004a-eee8-20c6-f636-5f3fad54c0d0','2020-08-21 11:23:31','1','sales_stage','enum','Needs Analysis','Prospecting',NULL,NULL),('81b3117a-6aa3-ce65-8990-5f48cd06cd2d','170ab7e4-7129-fd99-34c8-5f3fad24bc03','2020-08-28 09:24:20','1','sales_stage','enum','Needs Analysis','Perception Analysis',NULL,NULL),('8682cd63-c3a5-0cab-1927-5f43c57c2951','170ab7e4-7129-fd99-34c8-5f3fad24bc03','2020-08-24 13:50:45','1','sales_stage','enum','Needs Analysis','Prospecting',NULL,NULL),('9515c590-82bb-82c1-4486-5f43d2c3fffb','9583611d-7521-f3a8-6bcd-5f43bc7dfab0','2020-08-24 14:43:38','1','sales_stage','enum','Needs Analysis','Perception Analysis',NULL,NULL),('976ec6d7-2054-e162-ba5e-5f3fb0286f91','170ab7e4-7129-fd99-34c8-5f3fad24bc03','2020-08-21 11:32:48','1','sales_stage','enum','Closed Lost','Prospecting',NULL,NULL),('9cd777ce-bb4d-0d50-9196-5f44fa61bac3','cf6e07ce-3763-a98c-69d1-5f44fa63cfd9','2020-08-25 11:48:26','1','sales_stage','enum','Prospecting','Needs Analysis',NULL,NULL),('9d7257e8-d830-9707-ab73-5f44fabb20a8','cf6e07ce-3763-a98c-69d1-5f44fa63cfd9','2020-08-25 11:48:26','1','probability','int','10','25',NULL,NULL),('a64bfa47-ecf6-36c3-fa4f-5f43d015f197','ab99004a-eee8-20c6-f636-5f3fad54c0d0','2020-08-24 14:38:02','1','sales_stage','enum','Closed Lost','Prospecting',NULL,NULL),('a72d307c-ff6f-83f9-4726-5f43d05185ac','ab99004a-eee8-20c6-f636-5f3fad54c0d0','2020-08-24 14:38:02','1','probability','int','0','10',NULL,NULL),('c24f2fb1-5c0b-a8d9-08ce-5f45539f99f9','f10d6a43-2a2f-d743-d2ce-5f43bca15ede','2020-08-25 18:08:40','1','sales_stage','enum','Closed Lost','Prospecting',NULL,NULL),('c3c1f57f-a8d3-c994-9d00-5f48cd683008','f10d6a43-2a2f-d743-d2ce-5f43bca15ede','2020-08-28 09:24:24','1','sales_stage','enum','Needs Analysis','Prospecting',NULL,NULL),('c63b0120-c40d-181f-958b-5f48cd24ef36','f10d6a43-2a2f-d743-d2ce-5f43bca15ede','2020-08-28 09:24:17','1','sales_stage','enum','Prospecting','Needs Analysis',NULL,NULL),('cd350725-a980-5c9d-a149-5f3fbaceca39','ab99004a-eee8-20c6-f636-5f3fad54c0d0','2020-08-21 12:13:34','1','sales_stage','enum','Prospecting','Needs Analysis',NULL,NULL),('cf89d0c5-672e-d017-fe99-5f43ec9eeaab','f10d6a43-2a2f-d743-d2ce-5f43bca15ede','2020-08-24 16:35:23','1','sales_stage','enum','Prospecting','Closed Lost',NULL,NULL),('d2100d29-c576-7fa5-4d87-5f43d0944439','9583611d-7521-f3a8-6bcd-5f43bc7dfab0','2020-08-24 14:37:33','1','sales_stage','enum','Needs Analysis','Prospecting',NULL,NULL),('d5b40c4c-af82-76b5-9fd1-5f43d0254a31','9583611d-7521-f3a8-6bcd-5f43bc7dfab0','2020-08-24 14:37:33','1','probability','int','0','10',NULL,NULL),('d7d460f1-c9dc-b520-5111-5f43ec484b50','4c12f4da-751b-5429-80ea-5f43bed3071f','2020-08-24 16:36:20','1','sales_stage','enum','Closed Lost','Prospecting',NULL,NULL),('d7fd76d5-cfb8-4cc7-c3b1-5f43bc0ee037','ab99004a-eee8-20c6-f636-5f3fad54c0d0','2020-08-24 13:13:29','1','sales_stage','enum','Perception Analysis','Closed Lost',NULL,NULL),('deaebe67-e340-10d8-8a6d-5f43bc7903b5','9583611d-7521-f3a8-6bcd-5f43bc7dfab0','2020-08-24 13:11:11','1','sales_stage','enum','Perception Analysis','Closed Won',NULL,NULL),('e1a40076-e534-728d-ce28-5f43d60dcfca','f10d6a43-2a2f-d743-d2ce-5f43bca15ede','2020-08-24 15:01:16','1','sales_stage','enum','Closed Lost','Prospecting',NULL,NULL),('e1e26a30-129c-eb76-d6ca-5f43d6d555e6','f10d6a43-2a2f-d743-d2ce-5f43bca15ede','2020-08-24 15:01:16','1','probability','int','0','10',NULL,NULL),('e4f65b8b-d3e9-34f0-82cf-5f48cdc76bac','cf6e07ce-3763-a98c-69d1-5f44fa63cfd9','2020-08-28 09:24:30','1','sales_stage','enum','Needs Analysis','Prospecting',NULL,NULL),('ef25d394-674e-5d72-378e-5f43c5b9e720','170ab7e4-7129-fd99-34c8-5f3fad24bc03','2020-08-24 13:50:11','1','sales_stage','enum','Prospecting','Needs Analysis',NULL,NULL),('f0d88e42-7151-5f1c-88fc-5f3fba1eebbf','170ab7e4-7129-fd99-34c8-5f3fad24bc03','2020-08-21 12:13:40','1','sales_stage','enum','Closed Lost','Prospecting',NULL,NULL);
/*!40000 ALTER TABLE `opportunities_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `opportunities_contacts`
--

DROP TABLE IF EXISTS `opportunities_contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `opportunities_contacts` (
  `id` varchar(36) NOT NULL,
  `contact_id` varchar(36) DEFAULT NULL,
  `opportunity_id` varchar(36) DEFAULT NULL,
  `contact_role` varchar(50) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_con_opp_con` (`contact_id`),
  KEY `idx_con_opp_opp` (`opportunity_id`),
  KEY `idx_opportunities_contacts` (`opportunity_id`,`contact_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `opportunities_contacts`
--

LOCK TABLES `opportunities_contacts` WRITE;
/*!40000 ALTER TABLE `opportunities_contacts` DISABLE KEYS */;
/*!40000 ALTER TABLE `opportunities_contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `opportunities_cstm`
--

DROP TABLE IF EXISTS `opportunities_cstm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `opportunities_cstm` (
  `id_c` char(36) NOT NULL,
  `jjwg_maps_lng_c` float(11,8) DEFAULT '0.00000000',
  `jjwg_maps_lat_c` float(10,8) DEFAULT '0.00000000',
  `jjwg_maps_geocode_status_c` varchar(255) DEFAULT NULL,
  `jjwg_maps_address_c` varchar(255) DEFAULT NULL,
  `sales_stage_amount_c` decimal(26,6) DEFAULT NULL,
  `item_description_materialnum_c` varchar(255) DEFAULT NULL,
  `quantity_in_full_first_year_c` float(18,2) DEFAULT NULL,
  PRIMARY KEY (`id_c`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `opportunities_cstm`
--

LOCK TABLES `opportunities_cstm` WRITE;
/*!40000 ALTER TABLE `opportunities_cstm` DISABLE KEYS */;
INSERT INTO `opportunities_cstm` VALUES ('12c3340b-0f62-8d28-b800-5f48db0951f9',0.00000000,0.00000000,NULL,NULL,67500.000000,NULL,NULL),('170ab7e4-7129-fd99-34c8-5f3fad24bc03',0.00000000,0.00000000,'','',6000.000000,NULL,NULL),('1ee05743-c112-d3a1-163a-5f48dd532edf',0.00000000,0.00000000,NULL,NULL,4545.000000,NULL,NULL),('4c12f4da-751b-5429-80ea-5f43bed3071f',0.00000000,0.00000000,'','',0.000000,NULL,NULL),('68b8f5a5-cd40-decc-cebb-5f48da51a3f2',0.00000000,0.00000000,NULL,NULL,3050.000000,NULL,NULL),('9583611d-7521-f3a8-6bcd-5f43bc7dfab0',0.00000000,0.00000000,'','',2000.000000,NULL,NULL),('a57b6fdf-beda-0f2f-164f-5f48dd72c10f',0.00000000,0.00000000,NULL,NULL,8000.000000,NULL,NULL),('ab99004a-eee8-20c6-f636-5f3fad54c0d0',0.00000000,0.00000000,'','',15000.000000,NULL,NULL),('cf6e07ce-3763-a98c-69d1-5f44fa63cfd9',0.00000000,0.00000000,'','',3000.000000,NULL,NULL),('f10d6a43-2a2f-d743-d2ce-5f43bca15ede',0.00000000,0.00000000,'','',800.000000,NULL,NULL);
/*!40000 ALTER TABLE `opportunities_cstm` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `outbound_email`
--

DROP TABLE IF EXISTS `outbound_email`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `outbound_email` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `type` varchar(15) DEFAULT 'user',
  `user_id` char(36) NOT NULL,
  `smtp_from_name` varchar(255) DEFAULT NULL,
  `smtp_from_addr` varchar(255) DEFAULT NULL,
  `mail_sendtype` varchar(8) DEFAULT 'smtp',
  `mail_smtptype` varchar(20) DEFAULT 'other',
  `mail_smtpserver` varchar(100) DEFAULT NULL,
  `mail_smtpport` varchar(5) DEFAULT '0',
  `mail_smtpuser` varchar(100) DEFAULT NULL,
  `mail_smtppass` varchar(100) DEFAULT NULL,
  `mail_smtpauth_req` tinyint(1) DEFAULT '0',
  `mail_smtpssl` varchar(1) DEFAULT '0',
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `outbound_email`
--

LOCK TABLES `outbound_email` WRITE;
/*!40000 ALTER TABLE `outbound_email` DISABLE KEYS */;
INSERT INTO `outbound_email` VALUES ('86190b9b-6ca7-22da-5cb8-5c6e1d6e3db3','system','system','1',NULL,NULL,'SMTP','other','','25','','',1,'0',NULL,NULL,NULL,NULL,0,NULL);
/*!40000 ALTER TABLE `outbound_email` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `outbound_email_audit`
--

DROP TABLE IF EXISTS `outbound_email_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `outbound_email_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_outbound_email_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `outbound_email_audit`
--

LOCK TABLES `outbound_email_audit` WRITE;
/*!40000 ALTER TABLE `outbound_email_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `outbound_email_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project`
--

DROP TABLE IF EXISTS `project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project` (
  `id` char(36) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `assigned_user_id` char(36) DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `estimated_start_date` date DEFAULT NULL,
  `estimated_end_date` date DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `priority` varchar(255) DEFAULT NULL,
  `override_business_hours` tinyint(1) DEFAULT '0',
  `budget_hours` varchar(255) DEFAULT NULL,
  `account_id_c` char(36) DEFAULT NULL,
  `total_hours_pt` decimal(18,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project`
--

LOCK TABLES `project` WRITE;
/*!40000 ALTER TABLE `project` DISABLE KEYS */;
INSERT INTO `project` VALUES ('5371d12a-d61b-9d7a-97a4-5e90b71aecf9','2020-04-10 18:16:16','2020-04-10 18:16:16','dc26b995-5550-b848-6f73-5e90aa679348','dc26b995-5550-b848-6f73-5e90aa679348','dc26b995-5550-b848-6f73-5e90aa679348','Timesheet Ninja Updates',NULL,0,'2020-04-01','2020-04-30','In Review','high',0,NULL,NULL,6.00);
/*!40000 ALTER TABLE `project` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_contacts_1_c`
--

DROP TABLE IF EXISTS `project_contacts_1_c`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_contacts_1_c` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `project_contacts_1project_ida` varchar(36) DEFAULT NULL,
  `project_contacts_1contacts_idb` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `project_contacts_1_alt` (`project_contacts_1project_ida`,`project_contacts_1contacts_idb`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_contacts_1_c`
--

LOCK TABLES `project_contacts_1_c` WRITE;
/*!40000 ALTER TABLE `project_contacts_1_c` DISABLE KEYS */;
/*!40000 ALTER TABLE `project_contacts_1_c` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_cstm`
--

DROP TABLE IF EXISTS `project_cstm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_cstm` (
  `id_c` char(36) NOT NULL,
  `jjwg_maps_lng_c` float(11,8) DEFAULT '0.00000000',
  `jjwg_maps_lat_c` float(10,8) DEFAULT '0.00000000',
  `jjwg_maps_geocode_status_c` varchar(255) DEFAULT NULL,
  `jjwg_maps_address_c` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_c`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_cstm`
--

LOCK TABLES `project_cstm` WRITE;
/*!40000 ALTER TABLE `project_cstm` DISABLE KEYS */;
INSERT INTO `project_cstm` VALUES ('5371d12a-d61b-9d7a-97a4-5e90b71aecf9',0.00000000,0.00000000,NULL,NULL);
/*!40000 ALTER TABLE `project_cstm` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_sp_timesheet_tracker_1_c`
--

DROP TABLE IF EXISTS `project_sp_timesheet_tracker_1_c`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_sp_timesheet_tracker_1_c` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `project_sp_timesheet_tracker_1project_ida` varchar(36) DEFAULT NULL,
  `project_sp_timesheet_tracker_1sp_timesheet_tracker_idb` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `project_sp_timesheet_tracker_1_ida1` (`project_sp_timesheet_tracker_1project_ida`),
  KEY `project_sp_timesheet_tracker_1_alt` (`project_sp_timesheet_tracker_1sp_timesheet_tracker_idb`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_sp_timesheet_tracker_1_c`
--

LOCK TABLES `project_sp_timesheet_tracker_1_c` WRITE;
/*!40000 ALTER TABLE `project_sp_timesheet_tracker_1_c` DISABLE KEYS */;
INSERT INTO `project_sp_timesheet_tracker_1_c` VALUES ('5117f471-15d7-d53b-6c02-5e90b82e992d','2020-04-10 18:17:40',0,'5371d12a-d61b-9d7a-97a4-5e90b71aecf9','5082566c-5d34-5b12-bca7-5e90b8f556d4'),('51b5ecaa-4651-6c22-a259-5e90b86cf813','2020-04-10 18:17:40',0,'5371d12a-d61b-9d7a-97a4-5e90b71aecf9','51978fe1-1bba-14ff-37f4-5e90b8db54c4');
/*!40000 ALTER TABLE `project_sp_timesheet_tracker_1_c` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_task`
--

DROP TABLE IF EXISTS `project_task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_task` (
  `id` char(36) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `project_id` char(36) NOT NULL,
  `project_task_id` int(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `relationship_type` varchar(255) DEFAULT NULL,
  `description` text,
  `predecessors` text,
  `date_start` date DEFAULT NULL,
  `time_start` int(11) DEFAULT NULL,
  `time_finish` int(11) DEFAULT NULL,
  `date_finish` date DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `duration_unit` text,
  `actual_duration` int(11) DEFAULT NULL,
  `percent_complete` int(11) DEFAULT NULL,
  `date_due` date DEFAULT NULL,
  `time_due` time DEFAULT NULL,
  `parent_task_id` int(11) DEFAULT NULL,
  `assigned_user_id` char(36) DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `priority` varchar(255) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `milestone_flag` tinyint(1) DEFAULT NULL,
  `order_number` int(11) DEFAULT '1',
  `task_number` int(11) DEFAULT NULL,
  `estimated_effort` int(11) DEFAULT NULL,
  `actual_effort` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `utilization` int(11) DEFAULT '100',
  `is_billable` varchar(100) DEFAULT 'yes',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_task`
--

LOCK TABLES `project_task` WRITE;
/*!40000 ALTER TABLE `project_task` DISABLE KEYS */;
INSERT INTO `project_task` VALUES ('dbd3eee3-9951-a692-80f9-5e90b88bab0f','2020-04-10 18:16:58','2020-04-10 18:16:58','5371d12a-d61b-9d7a-97a4-5e90b71aecf9',NULL,'Test Tasks','Not Started','FS','',NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,0,NULL,NULL,NULL,'dc26b995-5550-b848-6f73-5e90aa679348','dc26b995-5550-b848-6f73-5e90aa679348','High','dc26b995-5550-b848-6f73-5e90aa679348',0,1,0,0,0,0,100,'yes');
/*!40000 ALTER TABLE `project_task` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_task_audit`
--

DROP TABLE IF EXISTS `project_task_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_task_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_project_task_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_task_audit`
--

LOCK TABLES `project_task_audit` WRITE;
/*!40000 ALTER TABLE `project_task_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `project_task_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_users_1_c`
--

DROP TABLE IF EXISTS `project_users_1_c`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_users_1_c` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `project_users_1project_ida` varchar(36) DEFAULT NULL,
  `project_users_1users_idb` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `project_users_1_alt` (`project_users_1project_ida`,`project_users_1users_idb`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_users_1_c`
--

LOCK TABLES `project_users_1_c` WRITE;
/*!40000 ALTER TABLE `project_users_1_c` DISABLE KEYS */;
INSERT INTO `project_users_1_c` VALUES ('5f765abd-304f-793b-3a27-5e90b7a0b983','2020-04-10 18:16:16',0,'5371d12a-d61b-9d7a-97a4-5e90b71aecf9','dc26b995-5550-b848-6f73-5e90aa679348');
/*!40000 ALTER TABLE `project_users_1_c` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects_accounts`
--

DROP TABLE IF EXISTS `projects_accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects_accounts` (
  `id` varchar(36) NOT NULL,
  `account_id` varchar(36) DEFAULT NULL,
  `project_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_proj_acct_proj` (`project_id`),
  KEY `idx_proj_acct_acct` (`account_id`),
  KEY `projects_accounts_alt` (`project_id`,`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects_accounts`
--

LOCK TABLES `projects_accounts` WRITE;
/*!40000 ALTER TABLE `projects_accounts` DISABLE KEYS */;
INSERT INTO `projects_accounts` VALUES ('5e5a81d4-7b57-11ea-a012-e261d5be06c3','','5371d12a-d61b-9d7a-97a4-5e90b71aecf9','2020-04-10 18:16:16',0);
/*!40000 ALTER TABLE `projects_accounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects_bugs`
--

DROP TABLE IF EXISTS `projects_bugs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects_bugs` (
  `id` varchar(36) NOT NULL,
  `bug_id` varchar(36) DEFAULT NULL,
  `project_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_proj_bug_proj` (`project_id`),
  KEY `idx_proj_bug_bug` (`bug_id`),
  KEY `projects_bugs_alt` (`project_id`,`bug_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects_bugs`
--

LOCK TABLES `projects_bugs` WRITE;
/*!40000 ALTER TABLE `projects_bugs` DISABLE KEYS */;
/*!40000 ALTER TABLE `projects_bugs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects_cases`
--

DROP TABLE IF EXISTS `projects_cases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects_cases` (
  `id` varchar(36) NOT NULL,
  `case_id` varchar(36) DEFAULT NULL,
  `project_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_proj_case_proj` (`project_id`),
  KEY `idx_proj_case_case` (`case_id`),
  KEY `projects_cases_alt` (`project_id`,`case_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects_cases`
--

LOCK TABLES `projects_cases` WRITE;
/*!40000 ALTER TABLE `projects_cases` DISABLE KEYS */;
/*!40000 ALTER TABLE `projects_cases` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects_contacts`
--

DROP TABLE IF EXISTS `projects_contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects_contacts` (
  `id` varchar(36) NOT NULL,
  `contact_id` varchar(36) DEFAULT NULL,
  `project_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_proj_con_proj` (`project_id`),
  KEY `idx_proj_con_con` (`contact_id`),
  KEY `projects_contacts_alt` (`project_id`,`contact_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects_contacts`
--

LOCK TABLES `projects_contacts` WRITE;
/*!40000 ALTER TABLE `projects_contacts` DISABLE KEYS */;
/*!40000 ALTER TABLE `projects_contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects_opportunities`
--

DROP TABLE IF EXISTS `projects_opportunities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects_opportunities` (
  `id` varchar(36) NOT NULL,
  `opportunity_id` varchar(36) DEFAULT NULL,
  `project_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_proj_opp_proj` (`project_id`),
  KEY `idx_proj_opp_opp` (`opportunity_id`),
  KEY `projects_opportunities_alt` (`project_id`,`opportunity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects_opportunities`
--

LOCK TABLES `projects_opportunities` WRITE;
/*!40000 ALTER TABLE `projects_opportunities` DISABLE KEYS */;
/*!40000 ALTER TABLE `projects_opportunities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects_products`
--

DROP TABLE IF EXISTS `projects_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects_products` (
  `id` varchar(36) NOT NULL,
  `product_id` varchar(36) DEFAULT NULL,
  `project_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_proj_prod_project` (`project_id`),
  KEY `idx_proj_prod_product` (`product_id`),
  KEY `projects_products_alt` (`project_id`,`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects_products`
--

LOCK TABLES `projects_products` WRITE;
/*!40000 ALTER TABLE `projects_products` DISABLE KEYS */;
/*!40000 ALTER TABLE `projects_products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prospect_list_campaigns`
--

DROP TABLE IF EXISTS `prospect_list_campaigns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prospect_list_campaigns` (
  `id` varchar(36) NOT NULL,
  `prospect_list_id` varchar(36) DEFAULT NULL,
  `campaign_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_pro_id` (`prospect_list_id`),
  KEY `idx_cam_id` (`campaign_id`),
  KEY `idx_prospect_list_campaigns` (`prospect_list_id`,`campaign_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prospect_list_campaigns`
--

LOCK TABLES `prospect_list_campaigns` WRITE;
/*!40000 ALTER TABLE `prospect_list_campaigns` DISABLE KEYS */;
/*!40000 ALTER TABLE `prospect_list_campaigns` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prospect_lists`
--

DROP TABLE IF EXISTS `prospect_lists`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prospect_lists` (
  `assigned_user_id` char(36) DEFAULT NULL,
  `id` char(36) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `list_type` varchar(100) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `description` text,
  `domain_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_prospect_list_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prospect_lists`
--

LOCK TABLES `prospect_lists` WRITE;
/*!40000 ALTER TABLE `prospect_lists` DISABLE KEYS */;
/*!40000 ALTER TABLE `prospect_lists` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prospect_lists_prospects`
--

DROP TABLE IF EXISTS `prospect_lists_prospects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prospect_lists_prospects` (
  `id` varchar(36) NOT NULL,
  `prospect_list_id` varchar(36) DEFAULT NULL,
  `related_id` varchar(36) DEFAULT NULL,
  `related_type` varchar(25) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_plp_pro_id` (`prospect_list_id`),
  KEY `idx_plp_rel_id` (`related_id`,`related_type`,`prospect_list_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prospect_lists_prospects`
--

LOCK TABLES `prospect_lists_prospects` WRITE;
/*!40000 ALTER TABLE `prospect_lists_prospects` DISABLE KEYS */;
/*!40000 ALTER TABLE `prospect_lists_prospects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prospects`
--

DROP TABLE IF EXISTS `prospects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prospects` (
  `id` char(36) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `salutation` varchar(255) DEFAULT NULL,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `department` varchar(255) DEFAULT NULL,
  `do_not_call` tinyint(1) DEFAULT '0',
  `phone_home` varchar(100) DEFAULT NULL,
  `phone_mobile` varchar(100) DEFAULT NULL,
  `phone_work` varchar(100) DEFAULT NULL,
  `phone_other` varchar(100) DEFAULT NULL,
  `phone_fax` varchar(100) DEFAULT NULL,
  `lawful_basis` text,
  `date_reviewed` date DEFAULT NULL,
  `lawful_basis_source` varchar(100) DEFAULT NULL,
  `primary_address_street` varchar(150) DEFAULT NULL,
  `primary_address_city` varchar(100) DEFAULT NULL,
  `primary_address_state` varchar(100) DEFAULT NULL,
  `primary_address_postalcode` varchar(20) DEFAULT NULL,
  `primary_address_country` varchar(255) DEFAULT NULL,
  `alt_address_street` varchar(150) DEFAULT NULL,
  `alt_address_city` varchar(100) DEFAULT NULL,
  `alt_address_state` varchar(100) DEFAULT NULL,
  `alt_address_postalcode` varchar(20) DEFAULT NULL,
  `alt_address_country` varchar(255) DEFAULT NULL,
  `assistant` varchar(75) DEFAULT NULL,
  `assistant_phone` varchar(100) DEFAULT NULL,
  `tracker_key` int(11) NOT NULL AUTO_INCREMENT,
  `birthdate` date DEFAULT NULL,
  `lead_id` char(36) DEFAULT NULL,
  `account_name` varchar(150) DEFAULT NULL,
  `campaign_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `prospect_auto_tracker_key` (`tracker_key`),
  KEY `idx_prospects_last_first` (`last_name`,`first_name`,`deleted`),
  KEY `idx_prospecs_del_last` (`last_name`,`deleted`),
  KEY `idx_prospects_id_del` (`id`,`deleted`),
  KEY `idx_prospects_assigned` (`assigned_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prospects`
--

LOCK TABLES `prospects` WRITE;
/*!40000 ALTER TABLE `prospects` DISABLE KEYS */;
/*!40000 ALTER TABLE `prospects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prospects_cstm`
--

DROP TABLE IF EXISTS `prospects_cstm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prospects_cstm` (
  `id_c` char(36) NOT NULL,
  `jjwg_maps_lng_c` float(11,8) DEFAULT '0.00000000',
  `jjwg_maps_lat_c` float(10,8) DEFAULT '0.00000000',
  `jjwg_maps_geocode_status_c` varchar(255) DEFAULT NULL,
  `jjwg_maps_address_c` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_c`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prospects_cstm`
--

LOCK TABLES `prospects_cstm` WRITE;
/*!40000 ALTER TABLE `prospects_cstm` DISABLE KEYS */;
/*!40000 ALTER TABLE `prospects_cstm` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `relationships`
--

DROP TABLE IF EXISTS `relationships`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `relationships` (
  `id` char(36) NOT NULL,
  `relationship_name` varchar(150) DEFAULT NULL,
  `lhs_module` varchar(100) DEFAULT NULL,
  `lhs_table` varchar(64) DEFAULT NULL,
  `lhs_key` varchar(64) DEFAULT NULL,
  `rhs_module` varchar(100) DEFAULT NULL,
  `rhs_table` varchar(64) DEFAULT NULL,
  `rhs_key` varchar(64) DEFAULT NULL,
  `join_table` varchar(64) DEFAULT NULL,
  `join_key_lhs` varchar(64) DEFAULT NULL,
  `join_key_rhs` varchar(64) DEFAULT NULL,
  `relationship_type` varchar(64) DEFAULT NULL,
  `relationship_role_column` varchar(64) DEFAULT NULL,
  `relationship_role_column_value` varchar(50) DEFAULT NULL,
  `reverse` tinyint(1) DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_rel_name` (`relationship_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `relationships`
--

LOCK TABLES `relationships` WRITE;
/*!40000 ALTER TABLE `relationships` DISABLE KEYS */;
INSERT INTO `relationships` VALUES ('10343f8b-2bcc-cdc6-8587-5f4e0fac119a','favorites_created_by','Users','users','id','Favorites','favorites','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('10603cbb-95f8-7691-7ec6-5f4e0f140d27','favorites_assigned_user','Users','users','id','Favorites','favorites','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('10f56907-c200-16a2-dcc1-5f4e0f9791b5','aok_knowledge_base_categories_modified_user','Users','users','id','AOK_Knowledge_Base_Categories','aok_knowledge_base_categories','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('1141fc02-8ab7-3805-cd4a-5f4e0fa091b4','aok_knowledge_base_categories_created_by','Users','users','id','AOK_Knowledge_Base_Categories','aok_knowledge_base_categories','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('1175a2e5-7183-c593-d89e-5f4e0f6d4045','aok_knowledge_base_categories_assigned_user','Users','users','id','AOK_Knowledge_Base_Categories','aok_knowledge_base_categories','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('1264f7ca-764a-0b81-5207-5f4e0f64d6b9','aok_knowledgebase_modified_user','Users','users','id','AOK_KnowledgeBase','aok_knowledgebase','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('12ab8c48-7508-87e4-6162-5f4e0fc15e40','aok_knowledgebase_created_by','Users','users','id','AOK_KnowledgeBase','aok_knowledgebase','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('12d4405f-f9cd-1d69-6c70-5f4e0fea0c8e','aok_knowledgebase_assigned_user','Users','users','id','AOK_KnowledgeBase','aok_knowledgebase','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('12febefa-b6a6-4c25-d1f8-5f4e0f4db12d','securitygroups_aok_knowledgebase','SecurityGroups','securitygroups','id','AOK_KnowledgeBase','aok_knowledgebase','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','AOK_KnowledgeBase',0,0),('14097b00-98c6-be49-03a3-5f4e0ff013cb','reminders_modified_user','Users','users','id','Reminders','reminders','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('144f434a-73f3-367b-2132-5f4e0f5c14a8','reminders_created_by','Users','users','id','Reminders','reminders','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('147c28cb-2def-4b36-c0ff-5f4e0f1a44fd','reminders_assigned_user','Users','users','id','Reminders','reminders','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('1532c0ae-a23f-5e27-2f63-5f4e0f25a3af','reminders_invitees_modified_user','Users','users','id','Reminders_Invitees','reminders_invitees','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('15706393-cb02-945d-2579-5f4e0f55f928','reminders_invitees_created_by','Users','users','id','Reminders_Invitees','reminders_invitees','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('15afc9e8-b130-c99e-cd71-5f4e0f43f242','reminders_invitees_assigned_user','Users','users','id','Reminders_Invitees','reminders_invitees','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('1672ec2c-9438-bf17-ab7d-5f4e0f83f92b','fp_events_modified_user','Users','users','id','FP_events','fp_events','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('16b2ef84-c863-55f2-5244-5f4e0f9fbf13','fp_events_created_by','Users','users','id','FP_events','fp_events','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('16df2eb4-99ee-f581-b027-5f4e0fb5ab4f','fp_events_assigned_user','Users','users','id','FP_events','fp_events','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('170d87a6-ea56-9c06-e55c-5f4e0f81eb22','securitygroups_fp_events','SecurityGroups','securitygroups','id','FP_events','fp_events','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','FP_events',0,0),('17bafd39-9afa-21fa-b0a3-5f4e0f82ba21','fp_event_locations_modified_user','Users','users','id','FP_Event_Locations','fp_event_locations','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('180202e0-fbc7-9f96-6632-5f4e0f01da45','fp_event_locations_created_by','Users','users','id','FP_Event_Locations','fp_event_locations','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('182c8429-3d06-b47d-e6b2-5f4e0fbf10fd','fp_event_locations_assigned_user','Users','users','id','FP_Event_Locations','fp_event_locations','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('18571b29-c7bb-b82e-4808-5f4e0f5828e2','securitygroups_fp_event_locations','SecurityGroups','securitygroups','id','FP_Event_Locations','fp_event_locations','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','FP_Event_Locations',0,0),('1893b0a7-161a-4070-29ec-5f4e0f88980b','optimistic_locking',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0),('18ba277a-9a89-d694-0ebc-5f4e0f6d9ab9','unified_search',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0),('1961d5b3-f21c-f6ac-9fdb-5f4e0f66780b','aod_indexevent_modified_user','Users','users','id','AOD_IndexEvent','aod_indexevent','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('19a8fdc5-ce62-1364-5083-5f4e0f41a949','aod_indexevent_created_by','Users','users','id','AOD_IndexEvent','aod_indexevent','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('19e4a5f2-34b7-8b95-c51c-5f4e0fd7c20e','aod_indexevent_assigned_user','Users','users','id','AOD_IndexEvent','aod_indexevent','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('1a2db82e-79c1-2997-0005-5f4e0f1eae9c','aod_index_modified_user','Users','users','id','AOD_Index','aod_index','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('1a75e709-2ace-36fe-6c4e-5f4e0f4ae8b0','aod_index_created_by','Users','users','id','AOD_Index','aod_index','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('1aa05c18-97f1-faab-70c8-5f4e0f81113f','aod_index_assigned_user','Users','users','id','AOD_Index','aod_index','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('1b3ec90c-82a1-f220-06f1-5f4e0ffc794b','aop_case_events_modified_user','Users','users','id','AOP_Case_Events','aop_case_events','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('1b88600a-9319-1068-887d-5f4e0f181b0f','aop_case_events_created_by','Users','users','id','AOP_Case_Events','aop_case_events','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('1bb386f8-8409-cf9f-3b41-5f4e0f150215','aop_case_events_assigned_user','Users','users','id','AOP_Case_Events','aop_case_events','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('1bf2587e-e85c-2f91-5bad-5f4e0f5cf618','cases_aop_case_events','Cases','cases','id','AOP_Case_Events','aop_case_events','case_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('1cb2a1a1-2c9c-a94d-c4d3-5f4e0f5f4e50','aop_case_updates_modified_user','Users','users','id','AOP_Case_Updates','aop_case_updates','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('1cf8e597-80df-52ef-68c4-5f4e0fe3a74a','aop_case_updates_created_by','Users','users','id','AOP_Case_Updates','aop_case_updates','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('1d2c8010-7e27-9bd5-9e90-5f4e0f5eb0f4','aop_case_updates_assigned_user','Users','users','id','AOP_Case_Updates','aop_case_updates','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('1d76a594-4ce2-329f-7d88-5f4e0fbaa7d2','cases_aop_case_updates','Cases','cases','id','AOP_Case_Updates','aop_case_updates','case_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('1db3083d-8023-40c6-ee30-5f4e0ff4cccc','aop_case_updates_notes','AOP_Case_Updates','aop_case_updates','id','Notes','notes','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','AOP_Case_Updates',0,0),('1e1f04f4-abac-b5d4-ba38-5f4e0f23b2c5','inbound_email_created_by','Users','users','id','InboundEmail','inbound_email','created_by',NULL,NULL,NULL,'one-to-one',NULL,NULL,0,0),('1e6f2e53-cb65-80ff-56b8-5f4e0fb095cb','aor_reports_modified_user','Users','users','id','AOR_Reports','aor_reports','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('1eb59588-b41b-113a-0eb0-5f4e0fc337ce','aor_reports_created_by','Users','users','id','AOR_Reports','aor_reports','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('1ef2a7b0-6eeb-0bb9-c729-5f4e0f5ace3d','aor_reports_assigned_user','Users','users','id','AOR_Reports','aor_reports','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('1f2300cd-0a0b-97c5-a09e-5f4e0fff6ffb','securitygroups_aor_reports','SecurityGroups','securitygroups','id','AOR_Reports','aor_reports','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','AOR_Reports',0,0),('1f62fa02-70b7-0336-c314-5f4e0f38a365','aor_reports_aor_fields','AOR_Reports','aor_reports','id','AOR_Fields','aor_fields','aor_report_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('1f8c9c28-26a3-f624-65cd-5f4e0f4bed3e','aor_reports_aor_conditions','AOR_Reports','aor_reports','id','AOR_Conditions','aor_conditions','aor_report_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('1fb8a758-e835-e3e9-6be0-5f4e0f1f10da','aor_scheduled_reports_aor_reports','AOR_Reports','aor_reports','id','AOR_Scheduled_Reports','aor_scheduled_reports','aor_report_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('20a1ab55-822e-4fca-efaf-5f4e0fff5327','aor_fields_modified_user','Users','users','id','AOR_Fields','aor_fields','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('20eadbf3-1c4b-b59a-7dad-5f4e0fa5445a','aor_fields_created_by','Users','users','id','AOR_Fields','aor_fields','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('21c47e43-d4c8-92ba-d09c-5f4e0fbdad9a','aor_charts_modified_user','Users','users','id','AOR_Charts','aor_charts','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('2206900c-782d-68c8-ace9-5f4e0fcedbd3','aor_charts_created_by','Users','users','id','AOR_Charts','aor_charts','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('2244b869-d78b-ed63-1c94-5f4e0fb5e176','aor_charts_aor_reports','AOR_Reports','aor_reports','id','AOR_Charts','aor_charts','aor_report_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('22b802cc-aa68-f5cc-7bbf-5f4e0f25f22a','inbound_email_modified_user_id','Users','users','id','InboundEmail','inbound_email','modified_user_id',NULL,NULL,NULL,'one-to-one',NULL,NULL,0,0),('22ffbf20-a158-cd65-6745-5f4e0fcac58e','aor_conditions_modified_user','Users','users','id','AOR_Conditions','aor_conditions','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('234308fb-e0f7-9dbf-9711-5f4e0fdf6bab','aor_conditions_created_by','Users','users','id','AOR_Conditions','aor_conditions','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('23ddf324-147a-dc79-0af7-5f4e0f334e03','aor_scheduled_reports_modified_user','Users','users','id','AOR_Scheduled_Reports','aor_scheduled_reports','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('240f4706-6218-7ffb-1ded-5f4e0f7faf6a','aor_scheduled_reports_created_by','Users','users','id','AOR_Scheduled_Reports','aor_scheduled_reports','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('257a3ea7-00ae-fafb-0300-5f4e0f46ae7f','aos_contracts_modified_user','Users','users','id','AOS_Contracts','aos_contracts','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('25c00026-d56f-44f3-2dff-5f4e0f532bbf','aos_contracts_created_by','Users','users','id','AOS_Contracts','aos_contracts','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('25e8c40f-f073-9987-379a-5f4e0f5ecd07','aos_contracts_assigned_user','Users','users','id','AOS_Contracts','aos_contracts','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('262880d3-9622-be05-891c-5f4e0f03c699','securitygroups_aos_contracts','SecurityGroups','securitygroups','id','AOS_Contracts','aos_contracts','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','AOS_Contracts',0,0),('2651fc13-d7b8-c6e2-547e-5f4e0f9b4a28','aos_contracts_tasks','AOS_Contracts','aos_contracts','id','Tasks','tasks','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','AOS_Contracts',0,0),('267cda1a-11fc-e4ee-6e27-5f4e0f324ae9','aos_contracts_notes','AOS_Contracts','aos_contracts','id','Notes','notes','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','AOS_Contracts',0,0),('26a6a595-32c4-be59-bc50-5f4e0f74eb6f','aos_contracts_meetings','AOS_Contracts','aos_contracts','id','Meetings','meetings','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','AOS_Contracts',0,0),('26de1db0-a1ca-7c7b-7397-5f4e0fb3c646','aos_contracts_calls','AOS_Contracts','aos_contracts','id','Calls','calls','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','AOS_Contracts',0,0),('2708f335-4c4d-b9bc-9d01-5f4e0ff9a7c5','aos_contracts_aos_products_quotes','AOS_Contracts','aos_contracts','id','AOS_Products_Quotes','aos_products_quotes','parent_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('27567604-75cd-8638-b8cb-5f4e0f255272','aos_contracts_aos_line_item_groups','AOS_Contracts','aos_contracts','id','AOS_Line_Item_Groups','aos_line_item_groups','parent_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('2879864e-320e-ee6b-ca83-5f4e0f6494f0','aos_invoices_modified_user','Users','users','id','AOS_Invoices','aos_invoices','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('28c8b050-73d7-0cbb-a0c8-5f4e0f09d98a','aos_invoices_created_by','Users','users','id','AOS_Invoices','aos_invoices','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('28f911d8-2e5e-680c-fc63-5f4e0fd18f60','aos_invoices_assigned_user','Users','users','id','AOS_Invoices','aos_invoices','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('29449d8c-a0a0-7559-7d1f-5f4e0f1faccc','securitygroups_aos_invoices','SecurityGroups','securitygroups','id','AOS_Invoices','aos_invoices','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','AOS_Invoices',0,0),('29726a19-9c74-6d1f-dff7-5f4e0f38204a','aos_invoices_aos_product_quotes','AOS_Invoices','aos_invoices','id','AOS_Products_Quotes','aos_products_quotes','parent_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('29a22b21-5394-6624-7b95-5f4e0f6a9ba8','aos_invoices_aos_line_item_groups','AOS_Invoices','aos_invoices','id','AOS_Line_Item_Groups','aos_line_item_groups','parent_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('2a713251-d3ba-e289-319e-5f4e0f921b8a','aos_pdf_templates_modified_user','Users','users','id','AOS_PDF_Templates','aos_pdf_templates','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('2aba15ea-5616-32bf-6a29-5f4e0fc2dd60','aos_pdf_templates_created_by','Users','users','id','AOS_PDF_Templates','aos_pdf_templates','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('2ae791cd-7b13-6d03-bd17-5f4e0f0a4d86','aos_pdf_templates_assigned_user','Users','users','id','AOS_PDF_Templates','aos_pdf_templates','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('2b272e0e-f218-2f2c-3505-5f4e0fdbec63','securitygroups_aos_pdf_templates','SecurityGroups','securitygroups','id','AOS_PDF_Templates','aos_pdf_templates','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','AOS_PDF_Templates',0,0),('2be90bc3-1bc0-1234-fad0-5f4e0fe94297','aos_product_categories_modified_user','Users','users','id','AOS_Product_Categories','aos_product_categories','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('2c2edbef-d6fd-cb9d-60de-5f4e0f827daa','aos_product_categories_created_by','Users','users','id','AOS_Product_Categories','aos_product_categories','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('2c5db9cd-4203-59ef-5bb5-5f4e0f42be37','aos_product_categories_assigned_user','Users','users','id','AOS_Product_Categories','aos_product_categories','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('2c89e34b-1e06-8405-f7b8-5f4e0fe047c1','securitygroups_aos_product_categories','SecurityGroups','securitygroups','id','AOS_Product_Categories','aos_product_categories','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','AOS_Product_Categories',0,0),('2cb541be-f316-5b06-6bd1-5f4e0fe2719b','sub_product_categories','AOS_Product_Categories','aos_product_categories','id','AOS_Product_Categories','aos_product_categories','parent_category_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('2d8b2636-afa0-0da0-892a-5f4e0fce0c59','aos_products_modified_user','Users','users','id','AOS_Products','aos_products','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('2dd1f05e-9410-e4bd-6200-5f4e0fc079cf','aos_products_created_by','Users','users','id','AOS_Products','aos_products','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('2e0fd7e9-a138-add5-351d-5f4e0ffe9e57','aos_products_assigned_user','Users','users','id','AOS_Products','aos_products','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('2e4b252c-1724-e287-00f4-5f4e0ffb0779','securitygroups_aos_products','SecurityGroups','securitygroups','id','AOS_Products','aos_products','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','AOS_Products',0,0),('2e757e95-3c36-e327-3d0e-5f4e0ff14029','product_categories','AOS_Product_Categories','aos_product_categories','id','AOS_Products','aos_products','aos_product_category_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('2f619fdd-6cee-e621-21f1-5f4e0f19b1e6','aos_products_quotes_modified_user','Users','users','id','AOS_Products_Quotes','aos_products_quotes','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('2fa82a68-cd93-cfa0-c58c-5f4e0fd32b48','aos_products_quotes_created_by','Users','users','id','AOS_Products_Quotes','aos_products_quotes','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('2fd39f77-aa7c-d503-c337-5f4e0fa06e70','aos_products_quotes_assigned_user','Users','users','id','AOS_Products_Quotes','aos_products_quotes','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('300e6e1f-157d-fe8e-7b68-5f4e0f65e142','aos_product_quotes_aos_products','AOS_Products','aos_products','id','AOS_Products_Quotes','aos_products_quotes','product_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('30950057-d46d-9d68-6b66-5f4e0f7f4f86','saved_search_assigned_user','Users','users','id','SavedSearch','saved_search','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('30edc84f-0dea-67da-456e-5f4e0fb76ebd','aos_line_item_groups_modified_user','Users','users','id','AOS_Line_Item_Groups','aos_line_item_groups','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('313199a0-186a-0e1e-f8ea-5f4e0f4f963e','aos_line_item_groups_created_by','Users','users','id','AOS_Line_Item_Groups','aos_line_item_groups','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('315a53f5-8430-34b3-cc91-5f4e0f3d5273','aos_line_item_groups_assigned_user','Users','users','id','AOS_Line_Item_Groups','aos_line_item_groups','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('3182e049-f5ac-c29e-4aed-5f4e0f0ed814','groups_aos_product_quotes','AOS_Line_Item_Groups','aos_line_item_groups','id','AOS_Products_Quotes','aos_products_quotes','group_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('32b58270-0312-1b4d-a4ef-5f4e0f7265d7','aos_quotes_modified_user','Users','users','id','AOS_Quotes','aos_quotes','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('32ff5575-af3e-5f94-05c6-5f4e0fcfcd58','aos_quotes_created_by','Users','users','id','AOS_Quotes','aos_quotes','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('3329bb81-2ba2-e95c-917b-5f4e0f02da3a','aos_quotes_assigned_user','Users','users','id','AOS_Quotes','aos_quotes','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('3355bab3-7e70-f88b-4ead-5f4e0fd708a6','securitygroups_aos_quotes','SecurityGroups','securitygroups','id','AOS_Quotes','aos_quotes','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','AOS_Quotes',0,0),('3380a17d-b632-ac6c-add7-5f4e0f3e8669','aos_quotes_aos_product_quotes','AOS_Quotes','aos_quotes','id','AOS_Products_Quotes','aos_products_quotes','parent_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('33ad0332-b59f-faf2-9568-5f4e0fed2907','aos_quotes_aos_line_item_groups','AOS_Quotes','aos_quotes','id','AOS_Line_Item_Groups','aos_line_item_groups','parent_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('3468e034-392c-5281-0573-5f4e0f279091','aow_actions_modified_user','Users','users','id','AOW_Actions','aow_actions','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('34a076c4-92a5-5382-cecd-5f4e0fc93fba','aow_actions_created_by','Users','users','id','AOW_Actions','aow_actions','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('35112bff-ec4f-5ab9-983c-5f4e0f0fd299','aow_workflow_modified_user','Users','users','id','AOW_WorkFlow','aow_workflow','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('35481218-0718-7398-b355-5f4e0f34c2b7','aow_workflow_created_by','Users','users','id','AOW_WorkFlow','aow_workflow','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('35831d29-687c-bc1d-be8c-5f4e0f4c74b6','aow_workflow_assigned_user','Users','users','id','AOW_WorkFlow','aow_workflow','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('35eab627-b6b7-f2bc-3bf2-5f4e0fb8a7f5','securitygroups_aow_workflow','SecurityGroups','securitygroups','id','AOW_WorkFlow','aow_workflow','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','AOW_WorkFlow',0,0),('36201efa-715e-3518-ba19-5f4e0ffc64d0','aow_workflow_aow_conditions','AOW_WorkFlow','aow_workflow','id','AOW_Conditions','aow_conditions','aow_workflow_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('364bd9ca-6548-9d9a-db0b-5f4e0fa84082','aow_workflow_aow_actions','AOW_WorkFlow','aow_workflow','id','AOW_Actions','aow_actions','aow_workflow_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('3675600f-25e6-4f2e-841d-5f4e0fc1eacb','aow_workflow_aow_processed','AOW_WorkFlow','aow_workflow','id','AOW_Processed','aow_processed','aow_workflow_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('37404c5f-b9fc-2d8d-c2b3-5f4e0fd912a5','aow_processed_modified_user','Users','users','id','AOW_Processed','aow_processed','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('3797d3aa-7a26-21c1-e35e-5f4e0fbd6a10','aow_processed_created_by','Users','users','id','AOW_Processed','aow_processed','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('38947008-ca64-9eb7-7e04-5f4e0ff6a2cc','aow_conditions_modified_user','Users','users','id','AOW_Conditions','aow_conditions','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('38e6a959-cce6-0165-35a9-5f4e0fcfa800','aow_conditions_created_by','Users','users','id','AOW_Conditions','aow_conditions','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('3a3dbed4-f841-a8aa-8c46-5f4e0f2fbbf7','jjwg_maps_modified_user','Users','users','id','jjwg_Maps','jjwg_maps','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('3a911ee8-2271-118c-76d2-5f4e0fb1b4c0','jjwg_maps_created_by','Users','users','id','jjwg_Maps','jjwg_maps','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('3ae2580b-6ca3-492f-8443-5f4e0f214470','jjwg_maps_assigned_user','Users','users','id','jjwg_Maps','jjwg_maps','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('3b1572c0-4e8f-e59b-7be4-5f4e0f99f36a','securitygroups_jjwg_maps','SecurityGroups','securitygroups','id','jjwg_Maps','jjwg_maps','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','jjwg_Maps',0,0),('3b446940-0c74-a6d7-64f2-5f4e0fde3831','jjwg_Maps_accounts','jjwg_Maps','jjwg_Maps','parent_id','Accounts','accounts','id',NULL,NULL,NULL,'one-to-many','parent_type','Accounts',0,0),('3b7a9ebf-38a0-b82b-421d-5f4e0f49e383','jjwg_Maps_contacts','jjwg_Maps','jjwg_Maps','parent_id','Contacts','contacts','id',NULL,NULL,NULL,'one-to-many','parent_type','Contacts',0,0),('3bce7a8e-94ea-7bfe-87bf-5f4e0f4205ac','jjwg_Maps_leads','jjwg_Maps','jjwg_Maps','parent_id','Leads','leads','id',NULL,NULL,NULL,'one-to-many','parent_type','Leads',0,0),('3c0150b4-3ae2-ca37-ac33-5f4e0fa496d7','jjwg_Maps_opportunities','jjwg_Maps','jjwg_Maps','parent_id','Opportunities','opportunities','id',NULL,NULL,NULL,'one-to-many','parent_type','Opportunities',0,0),('3c340dfd-7b5b-71fb-8657-5f4e0fe78d11','jjwg_Maps_cases','jjwg_Maps','jjwg_Maps','parent_id','Cases','cases','id',NULL,NULL,NULL,'one-to-many','parent_type','Cases',0,0),('3c6043bb-8494-fab2-05a9-5f4e0fed6759','jjwg_Maps_projects','jjwg_Maps','jjwg_Maps','parent_id','Project','project','id',NULL,NULL,NULL,'one-to-many','parent_type','Project',0,0),('3cb2b723-9d2f-936e-c01a-5f4e0fe4e872','jjwg_Maps_meetings','jjwg_Maps','jjwg_Maps','parent_id','Meetings','meetings','id',NULL,NULL,NULL,'one-to-many','parent_type','Meetings',0,0),('3ce63052-acb6-7769-ee1b-5f4e0fa3af51','jjwg_Maps_prospects','jjwg_Maps','jjwg_Maps','parent_id','Prospects','prospects','id',NULL,NULL,NULL,'one-to-many','parent_type','Prospects',0,0),('3dee88a0-c2ab-8e53-6d30-5f4e0fd77b36','jjwg_markers_modified_user','Users','users','id','jjwg_Markers','jjwg_markers','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('3e33c98c-7be8-69ca-1d55-5f4e0f371f54','jjwg_markers_created_by','Users','users','id','jjwg_Markers','jjwg_markers','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('3e7260c3-d7eb-bbb5-9eb8-5f4e0f5da4ae','jjwg_markers_assigned_user','Users','users','id','jjwg_Markers','jjwg_markers','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('3eb686bf-56b4-0c22-b593-5f4e0f3839b4','securitygroups_jjwg_markers','SecurityGroups','securitygroups','id','jjwg_Markers','jjwg_markers','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','jjwg_Markers',0,0),('3f90d48c-50e1-d02f-c075-5f4e0f97d742','jjwg_areas_modified_user','Users','users','id','jjwg_Areas','jjwg_areas','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('3fd74c0d-69a2-ab90-24d1-5f4e0fa8f0cd','jjwg_areas_created_by','Users','users','id','jjwg_Areas','jjwg_areas','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('4000a63f-4042-5f64-4222-5f4e0fa2cc55','jjwg_areas_assigned_user','Users','users','id','jjwg_Areas','jjwg_areas','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('402a6f07-513f-13ba-bafa-5f4e0f09a174','securitygroups_jjwg_areas','SecurityGroups','securitygroups','id','jjwg_Areas','jjwg_areas','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','jjwg_Areas',0,0),('40fc7a8c-2f58-05e6-f5a0-5f4e0fe144e0','jjwg_address_cache_modified_user','Users','users','id','jjwg_Address_Cache','jjwg_address_cache','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('41519d5c-736e-6bab-a4a8-5f4e0fff2bac','jjwg_address_cache_created_by','Users','users','id','jjwg_Address_Cache','jjwg_address_cache','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('419499c7-ae40-ba63-6e8b-5f4e0fd444c4','jjwg_address_cache_assigned_user','Users','users','id','jjwg_Address_Cache','jjwg_address_cache','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('4254ca91-30d0-830d-f1e7-5f4e0f3ba22a','calls_reschedule_modified_user','Users','users','id','Calls_Reschedule','calls_reschedule','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('429b6c37-18a4-67e8-2c34-5f4e0f689cae','calls_reschedule_created_by','Users','users','id','Calls_Reschedule','calls_reschedule','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('42c55c6d-7e27-4015-1159-5f4e0fe1c97c','calls_reschedule_assigned_user','Users','users','id','Calls_Reschedule','calls_reschedule','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('42fda9a9-27a6-6a0b-a875-5f4e0f277bd9','securitygroups_modified_user','Users','users','id','SecurityGroups','securitygroups','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('4345b3e1-e163-b075-ac03-5f4e0f45b40f','securitygroups_created_by','Users','users','id','SecurityGroups','securitygroups','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('43885844-6c60-273d-d26b-5f4e0fe9d18b','securitygroups_assigned_user','Users','users','id','SecurityGroups','securitygroups','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('443f7e3a-d173-4fc9-cf16-5f4e0f970096','outbound_email_modified_user','Users','users','id','OutboundEmailAccounts','outbound_email','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('449cc332-8bca-8a1f-11e6-5f4e0ffd0bb4','outbound_email_created_by','Users','users','id','OutboundEmailAccounts','outbound_email','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('44d56eff-84c9-e30b-b760-5f4e0fbcaea8','outbound_email_assigned_user','Users','users','id','OutboundEmailAccounts','outbound_email','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('458194bd-f21c-0fc3-f675-5f4e0f3f38f0','templatesectionline_modified_user','Users','users','id','TemplateSectionLine','templatesectionline','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('45ca11b7-9f96-b6d3-7ab3-5f4e0f5063e6','templatesectionline_created_by','Users','users','id','TemplateSectionLine','templatesectionline','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('461d0812-0781-1d62-11ac-5f4e0f2db91d','spots_modified_user','Users','users','id','Spots','spots','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('467fe234-316c-963f-a7ef-5f4e0f2e15cd','oauth2tokens_modified_user','Users','users','id','OAuth2Tokens','oauth2tokens','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('46c5ca63-62e2-9322-0c3a-5f4e0f175d14','oauth2tokens_created_by','Users','users','id','OAuth2Tokens','oauth2tokens','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('46f55a67-1353-54fa-ed6a-5f4e0fcd6efc','oauth2tokens_assigned_user','Users','users','id','OAuth2Tokens','oauth2tokens','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('47aa4c1c-5669-6668-cdc1-5f4e0f32fdfe','oauth2clients_modified_user','Users','users','id','OAuth2Clients','oauth2clients','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('47f081c7-6f5f-2a4e-d565-5f4e0f72a6ea','oauth2clients_created_by','Users','users','id','OAuth2Clients','oauth2clients','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('48230caa-fae3-022a-2718-5f4e0f82fa9b','oauth2clients_oauth2tokens','OAuth2Clients','oauth2clients','id','OAuth2Tokens','oauth2tokens','client',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('486c928c-5c0d-be5b-5c55-5f4e0f5d7863','oauth2clients_assigned_user','Users','users','id','OAuth2Clients','oauth2clients','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('49276518-5fe2-b4d4-df23-5f4e0f23d2a4','surveyresponses_modified_user','Users','users','id','SurveyResponses','surveyresponses','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('496cd239-8c46-e755-765e-5f4e0f05cdea','surveyresponses_created_by','Users','users','id','SurveyResponses','surveyresponses','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('4997d6de-b9ab-c911-8735-5f4e0f09e72e','surveyresponses_assigned_user','Users','users','id','SurveyResponses','surveyresponses','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('49d66416-d6d4-8606-501b-5f4e0f614bac','securitygroups_surveyresponses','SecurityGroups','securitygroups','id','SurveyResponses','surveyresponses','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','SurveyResponses',0,0),('4a015fb7-f6c8-8ba3-faf7-5f4e0f838ebd','surveyresponses_surveyquestionresponses','SurveyResponses','surveyresponses','id','SurveyQuestionResponses','surveyquestionresponses','surveyresponse_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('4ab9c026-c4f3-3d61-5afb-5f4e0fb48f81','surveys_modified_user','Users','users','id','Surveys','surveys','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('4ad10bdf-a252-8560-796c-5f4e0f5966d8','spots_created_by','Users','users','id','Spots','spots','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('4b0011f7-f7e2-d6ee-a0cb-5f4e0fd54af8','surveys_created_by','Users','users','id','Surveys','surveys','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('4b2dc57a-dbd1-c6f2-266b-5f4e0f7330ec','surveys_assigned_user','Users','users','id','Surveys','surveys','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('4b711ade-4277-58a0-3025-5f4e0f89d515','securitygroups_surveys','SecurityGroups','securitygroups','id','Surveys','surveys','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','Surveys',0,0),('4b9be0b0-b0a4-3b29-38d1-5f4e0ff793fe','surveys_surveyquestions','Surveys','surveys','id','SurveyQuestions','surveyquestions','survey_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('4bc3928e-a1a5-d9a4-2eaa-5f4e0f9e20aa','surveys_surveyresponses','Surveys','surveys','id','SurveyResponses','surveyresponses','survey_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('4bedb97d-d65a-1288-eb1a-5f4e0f046d64','surveys_campaigns','Surveys','surveys','id','Campaigns','campaigns','survey_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('4c973391-1ac5-24c2-24e5-5f4e0ff8c44c','surveyquestionresponses_modified_user','Users','users','id','SurveyQuestionResponses','surveyquestionresponses','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('4cdd4ccc-d1ff-68f8-cb54-5f4e0f7e85d9','surveyquestionresponses_created_by','Users','users','id','SurveyQuestionResponses','surveyquestionresponses','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('4d0aaeb3-abd8-c6e9-cc3f-5f4e0f79a38e','surveyquestionresponses_assigned_user','Users','users','id','SurveyQuestionResponses','surveyquestionresponses','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('4d5026af-d7d7-f63c-1c45-5f4e0f659fff','securitygroups_surveyquestionresponses','SecurityGroups','securitygroups','id','SurveyQuestionResponses','surveyquestionresponses','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','SurveyQuestionResponses',0,0),('4dc905c0-2482-bd1f-1219-5f4e0fe2003f','spots_assigned_user','Users','users','id','Spots','spots','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('4df96b37-74da-ed9b-9f19-5f4e0f970c93','surveyquestions_modified_user','Users','users','id','SurveyQuestions','surveyquestions','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('4e42d6c6-b063-2e8c-0e72-5f4e0f5db202','surveyquestions_created_by','Users','users','id','SurveyQuestions','surveyquestions','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('4e735b20-180d-fa40-434c-5f4e0f50a8ec','surveyquestions_assigned_user','Users','users','id','SurveyQuestions','surveyquestions','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('4e9fbe6a-789c-131f-b1d0-5f4e0fedbae5','securitygroups_surveyquestions','SecurityGroups','securitygroups','id','SurveyQuestions','surveyquestions','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','SurveyQuestions',0,0),('4ecf0ed3-14df-ee46-0140-5f4e0f7bffad','surveyquestions_surveyquestionoptions','SurveyQuestions','surveyquestions','id','SurveyQuestionOptions','surveyquestionoptions','survey_question_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('4f88966d-659b-3d5c-3fb9-5f4e0f2ad6aa','surveyquestionoptions_modified_user','Users','users','id','SurveyQuestionOptions','surveyquestionoptions','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('4fce3f33-53cf-ef2c-34f5-5f4e0f92c9af','surveyquestionoptions_created_by','Users','users','id','SurveyQuestionOptions','surveyquestionoptions','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('5007f652-3fa7-1b93-1cf6-5f4e0fb319a1','surveyquestionoptions_assigned_user','Users','users','id','SurveyQuestionOptions','surveyquestionoptions','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('505396da-e43c-98dd-ed84-5f4e0f1fbfa3','securitygroups_surveyquestionoptions','SecurityGroups','securitygroups','id','SurveyQuestionOptions','surveyquestionoptions','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','SurveyQuestionOptions',0,0),('51341f6c-40f1-d595-d9e2-5f4e0f4d1938','sp_timesheet_modified_user','Users','users','id','sp_Timesheet','sp_timesheet','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('51836435-f566-6cc7-0cd7-5f4e0f3a4fd7','sp_timesheet_created_by','Users','users','id','sp_Timesheet','sp_timesheet','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('51980d8e-cc92-b8f2-f281-5f4e0fc3151c','securitygroups_spots','SecurityGroups','securitygroups','id','Spots','spots','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','Spots',0,0),('51af0566-d7cf-3f34-937a-5f4e0f5559c3','sp_timesheet_assigned_user','Users','users','id','sp_Timesheet','sp_timesheet','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('52b9e8d4-7e86-39ba-fea6-5f4e0f90b03c','sp_timesheet_tracker_modified_user','Users','users','id','sp_timesheet_tracker','sp_timesheet_tracker','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('52fe7977-ccdb-e251-b27f-5f4e0f1f0d17','sp_timesheet_tracker_created_by','Users','users','id','sp_timesheet_tracker','sp_timesheet_tracker','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('53400a44-1ab5-9b53-048f-5f4e0f75fd98','sp_timesheet_tracker_assigned_user','Users','users','id','sp_timesheet_tracker','sp_timesheet_tracker','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('542884b0-0f06-aebc-163d-5f4e0f198b39','sp_auto_timesheet_time_tracker_modified_user','Users','users','id','sp_Auto_TimeSheet_Time_Tracker','sp_auto_timesheet_time_tracker','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('546c63d2-67b5-6d5f-8ece-5f4e0f05ffb4','sp_auto_timesheet_time_tracker_created_by','Users','users','id','sp_Auto_TimeSheet_Time_Tracker','sp_auto_timesheet_time_tracker','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('549ac1a7-5589-9b71-52ce-5f4e0f168004','sp_auto_timesheet_time_tracker_assigned_user','Users','users','id','sp_Auto_TimeSheet_Time_Tracker','sp_auto_timesheet_time_tracker','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('558ffb59-c46f-7628-88e8-5f4e0fa4cee6','suh_timer_recording_modified_user','Users','users','id','suh_timer_recording','suh_timer_recording','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('55e1e9d6-5dc1-e604-4720-5f4e0f5280f4','suh_timer_recording_created_by','Users','users','id','suh_timer_recording','suh_timer_recording','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('56467094-9240-5233-8051-5f4e0f875655','suh_timer_recording_assigned_user','Users','users','id','suh_timer_recording','suh_timer_recording','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('567bfc19-a777-eb28-7891-5f4e0fb7576b','securitygroups_suh_timer_recording','SecurityGroups','securitygroups','id','suh_timer_recording','suh_timer_recording','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','suh_timer_recording',0,0),('573f3111-1caf-6775-7601-5f4e0fe06b1e','sp_timeoff_timecard_modified_user','Users','users','id','sp_Timeoff_TimeCard','sp_timeoff_timecard','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('57989b8d-f10a-8d4c-8105-5f4e0f0d8412','sp_timeoff_timecard_created_by','Users','users','id','sp_Timeoff_TimeCard','sp_timeoff_timecard','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('57da28c4-07ff-01ab-af48-5f4e0f592512','sp_timeoff_timecard_assigned_user','Users','users','id','sp_Timeoff_TimeCard','sp_timeoff_timecard','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('58b2f6fb-fde1-115a-35bd-5f4e0fbb909f','accounts_bugs','Accounts','accounts','id','Bugs','bugs','id','accounts_bugs','account_id','bug_id','many-to-many',NULL,NULL,0,0),('592183ec-ee98-d6ab-5515-5f4e0f32924a','accounts_contacts','Accounts','accounts','id','Contacts','contacts','id','accounts_contacts','account_id','contact_id','many-to-many',NULL,NULL,0,0),('595f3ed4-a87f-32cb-88a9-5f4e0f794a77','accounts_opportunities','Accounts','accounts','id','Opportunities','opportunities','id','accounts_opportunities','account_id','opportunity_id','many-to-many',NULL,NULL,0,0),('59956285-e357-c752-399e-5f4e0f42af20','calls_contacts','Calls','calls','id','Contacts','contacts','id','calls_contacts','call_id','contact_id','many-to-many',NULL,NULL,0,0),('59dd1462-0ebb-e22b-3fdf-5f4e0fb075d2','calls_users','Calls','calls','id','Users','users','id','calls_users','call_id','user_id','many-to-many',NULL,NULL,0,0),('5a2b861d-7bc4-7fe2-812f-5f4e0ff24fb9','calls_leads','Calls','calls','id','Leads','leads','id','calls_leads','call_id','lead_id','many-to-many',NULL,NULL,0,0),('5a5a4153-60db-ce2c-9068-5f4e0fbfffde','cases_bugs','Cases','cases','id','Bugs','bugs','id','cases_bugs','case_id','bug_id','many-to-many',NULL,NULL,0,0),('5a8a8f49-cbc5-85b0-971c-5f4e0fbe1cf2','contacts_bugs','Contacts','contacts','id','Bugs','bugs','id','contacts_bugs','contact_id','bug_id','many-to-many',NULL,NULL,0,0),('5ac06b42-23b1-f11c-c03c-5f4e0f9fc1cc','contacts_cases','Contacts','contacts','id','Cases','cases','id','contacts_cases','contact_id','case_id','many-to-many',NULL,NULL,0,0),('5b277435-41c9-79f9-4d5f-5f4e0f1539bd','contacts_users','Contacts','contacts','id','Users','users','id','contacts_users','contact_id','user_id','many-to-many',NULL,NULL,0,0),('5b6a4d89-fa7e-3930-3c22-5f4e0feb2257','emails_bugs_rel','Emails','emails','id','Bugs','bugs','id','emails_beans','email_id','bean_id','many-to-many','bean_module','Bugs',0,0),('5b96c4c4-02ec-2dc7-ec4f-5f4e0fa39903','emails_cases_rel','Emails','emails','id','Cases','cases','id','emails_beans','email_id','bean_id','many-to-many','bean_module','Cases',0,0),('5be4b5d3-2968-7fd6-9d16-5f4e0fd05fac','emails_opportunities_rel','Emails','emails','id','Opportunities','opportunities','id','emails_beans','email_id','bean_id','many-to-many','bean_module','Opportunities',0,0),('5c2daa68-0c86-f52d-73f5-5f4e0ff852b3','emails_tasks_rel','Emails','emails','id','Tasks','tasks','id','emails_beans','email_id','bean_id','many-to-many','bean_module','Tasks',0,0),('5c58c3ae-2ffc-3418-2d4e-5f4e0f195f85','emails_users_rel','Emails','emails','id','Users','users','id','emails_beans','email_id','bean_id','many-to-many','bean_module','Users',0,0),('5c83cb71-6ce8-b296-a804-5f4e0fc3d27a','emails_project_task_rel','Emails','emails','id','ProjectTask','project_task','id','emails_beans','email_id','bean_id','many-to-many','bean_module','ProjectTask',0,0),('5cafc295-a1e2-e5ed-46c0-5f4e0f4d5ae5','emails_projects_rel','Emails','emails','id','Project','project','id','emails_beans','email_id','bean_id','many-to-many','bean_module','Project',0,0),('5d2fed86-8449-fd9f-337b-5f4e0f8e01fe','emails_prospects_rel','Emails','emails','id','Prospects','prospects','id','emails_beans','email_id','bean_id','many-to-many','bean_module','Prospects',0,0),('5d5bfba4-9df4-bc7f-7080-5f4e0f773d39','meetings_contacts','Meetings','meetings','id','Contacts','contacts','id','meetings_contacts','meeting_id','contact_id','many-to-many',NULL,NULL,0,0),('5d8a65d8-44d6-d525-0f87-5f4e0fe199e8','meetings_users','Meetings','meetings','id','Users','users','id','meetings_users','meeting_id','user_id','many-to-many',NULL,NULL,0,0),('5db98332-64d4-556f-4b4e-5f4e0f4cc2a4','meetings_leads','Meetings','meetings','id','Leads','leads','id','meetings_leads','meeting_id','lead_id','many-to-many',NULL,NULL,0,0),('5ddf0ea8-f446-9ee1-9052-5f4e0fc72f88','aobh_businesshours_modified_user','Users','users','id','AOBH_BusinessHours','aobh_businesshours','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('5e044d04-9f8c-12c0-c801-5f4e0f95391f','opportunities_contacts','Opportunities','opportunities','id','Contacts','contacts','id','opportunities_contacts','opportunity_id','contact_id','many-to-many',NULL,NULL,0,0),('5e330058-3384-acb4-3dc1-5f4e0fa5d774','prospect_list_campaigns','ProspectLists','prospect_lists','id','Campaigns','campaigns','id','prospect_list_campaigns','prospect_list_id','campaign_id','many-to-many',NULL,NULL,0,0),('5e611413-34cf-cfff-0665-5f4e0fad1de2','prospect_list_contacts','ProspectLists','prospect_lists','id','Contacts','contacts','id','prospect_lists_prospects','prospect_list_id','related_id','many-to-many','related_type','Contacts',0,0),('5e9dae97-ae78-f1b6-8763-5f4e0f9ef6ff','prospect_list_prospects','ProspectLists','prospect_lists','id','Prospects','prospects','id','prospect_lists_prospects','prospect_list_id','related_id','many-to-many','related_type','Prospects',0,0),('5ee620de-b9da-d17b-27c6-5f4e0fcb6a12','prospect_list_leads','ProspectLists','prospect_lists','id','Leads','leads','id','prospect_lists_prospects','prospect_list_id','related_id','many-to-many','related_type','Leads',0,0),('5f14bf5a-9eb1-eca3-d604-5f4e0f6fa065','prospect_list_users','ProspectLists','prospect_lists','id','Users','users','id','prospect_lists_prospects','prospect_list_id','related_id','many-to-many','related_type','Users',0,0),('5f3ec7d2-22aa-75c1-cc45-5f4e0f21ece2','prospect_list_accounts','ProspectLists','prospect_lists','id','Accounts','accounts','id','prospect_lists_prospects','prospect_list_id','related_id','many-to-many','related_type','Accounts',0,0),('5f693692-b8a5-64e8-4093-5f4e0f3221a5','roles_users','Roles','roles','id','Users','users','id','roles_users','role_id','user_id','many-to-many',NULL,NULL,0,0),('5fa76e46-32ad-1c36-0dad-5f4e0f8c25c3','projects_bugs','Project','project','id','Bugs','bugs','id','projects_bugs','project_id','bug_id','many-to-many',NULL,NULL,0,0),('5fe8aa2d-3b0c-e5a7-f581-5f4e0f4b5a39','projects_cases','Project','project','id','Cases','cases','id','projects_cases','project_id','case_id','many-to-many',NULL,NULL,0,0),('60168dc1-d9a5-4f4a-7e74-5f4e0fc230bf','projects_accounts','Project','project','id','Accounts','accounts','id','projects_accounts','project_id','account_id','many-to-many',NULL,NULL,0,0),('60403952-d95b-a1ec-2924-5f4e0f13719b','projects_contacts','Project','project','id','Contacts','contacts','id','projects_contacts','project_id','contact_id','many-to-many',NULL,NULL,0,0),('606a9a9f-5363-1c6d-ae47-5f4e0f2739dd','projects_opportunities','Project','project','id','Opportunities','opportunities','id','projects_opportunities','project_id','opportunity_id','many-to-many',NULL,NULL,0,0),('609846c2-aed7-3bbc-b6e3-5f4e0fb1f072','acl_roles_actions','ACLRoles','acl_roles','id','ACLActions','acl_actions','id','acl_roles_actions','role_id','action_id','many-to-many',NULL,NULL,0,0),('60d67637-d05a-1a18-aa03-5f4e0f84cac5','acl_roles_users','ACLRoles','acl_roles','id','Users','users','id','acl_roles_users','role_id','user_id','many-to-many',NULL,NULL,0,0),('60ffcce7-2750-fcbc-6a18-5f4e0fed19b8','email_marketing_prospect_lists','EmailMarketing','email_marketing','id','ProspectLists','prospect_lists','id','email_marketing_prospect_lists','email_marketing_id','prospect_list_id','many-to-many',NULL,NULL,0,0),('6128d377-40c8-505a-29b7-5f4e0f31f964','leads_documents','Leads','leads','id','Documents','documents','id','linked_documents','parent_id','document_id','many-to-many','parent_type','Leads',0,0),('61520e27-a65c-233e-a7f0-5f4e0f5cef78','documents_accounts','Documents','documents','id','Accounts','accounts','id','documents_accounts','document_id','account_id','many-to-many',NULL,NULL,0,0),('617d5f54-d237-d9e2-0f0d-5f4e0fe9f37e','documents_contacts','Documents','documents','id','Contacts','contacts','id','documents_contacts','document_id','contact_id','many-to-many',NULL,NULL,0,0),('61c0f1b8-9ea2-011f-7e65-5f4e0f365af7','documents_opportunities','Documents','documents','id','Opportunities','opportunities','id','documents_opportunities','document_id','opportunity_id','many-to-many',NULL,NULL,0,0),('61e00cf3-d5e6-37ca-6637-5f4e0ff3458e','aobh_businesshours_created_by','Users','users','id','AOBH_BusinessHours','aobh_businesshours','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('61edeb3d-9738-d03d-5468-5f4e0f8b5a6a','documents_cases','Documents','documents','id','Cases','cases','id','documents_cases','document_id','case_id','many-to-many',NULL,NULL,0,0),('621b5af3-8dd7-e472-f50f-5f4e0fb1a02c','documents_bugs','Documents','documents','id','Bugs','bugs','id','documents_bugs','document_id','bug_id','many-to-many',NULL,NULL,0,0),('62479706-9519-6b16-bddc-5f4e0f1a8e0f','aok_knowledgebase_categories','AOK_KnowledgeBase','aok_knowledgebase','id','AOK_Knowledge_Base_Categories','aok_knowledge_base_categories','id','aok_knowledgebase_categories','aok_knowledgebase_id','aok_knowledge_base_categories_id','many-to-many',NULL,NULL,0,0),('6275e4e9-24b2-5478-884d-5f4e0fd7583f','am_projecttemplates_project_1','AM_ProjectTemplates','am_projecttemplates','id','Project','project','id','am_projecttemplates_project_1_c','am_projecttemplates_project_1am_projecttemplates_ida','am_projecttemplates_project_1project_idb','many-to-many',NULL,NULL,0,0),('62bc30eb-a6c8-251c-b23e-5f4e0f4951d4','am_projecttemplates_contacts_1','AM_ProjectTemplates','am_projecttemplates','id','Contacts','contacts','id','am_projecttemplates_contacts_1_c','am_projecttemplates_ida','contacts_idb','many-to-many',NULL,NULL,0,0),('62e8b386-1314-ad11-eb9f-5f4e0f5c58ea','am_projecttemplates_users_1','AM_ProjectTemplates','am_projecttemplates','id','Users','users','id','am_projecttemplates_users_1_c','am_projecttemplates_ida','users_idb','many-to-many',NULL,NULL,0,0),('63143e99-2127-1b6a-7a6d-5f4e0f6e39ca','am_tasktemplates_am_projecttemplates','AM_ProjectTemplates','am_projecttemplates','id','AM_TaskTemplates','am_tasktemplates','id','am_tasktemplates_am_projecttemplates_c','am_tasktemplates_am_projecttemplatesam_projecttemplates_ida','am_tasktemplates_am_projecttemplatesam_tasktemplates_idb','many-to-many',NULL,NULL,0,0),('63435141-e119-66fd-d315-5f4e0f19b060','aos_contracts_documents','AOS_Contracts','aos_contracts','id','Documents','documents','id','aos_contracts_documents','aos_contracts_id','documents_id','many-to-many',NULL,NULL,0,0),('637fab70-9409-3f7c-e562-5f4e0f76ee9d','aos_quotes_aos_contracts','AOS_Quotes','aos_quotes','id','AOS_Contracts','aos_contracts','id','aos_quotes_os_contracts_c','aos_quotese81e_quotes_ida','aos_quotes4dc0ntracts_idb','many-to-many',NULL,NULL,0,0),('63c347af-e9d4-2ae5-2435-5f4e0f9f0da9','aos_quotes_aos_invoices','AOS_Quotes','aos_quotes','id','AOS_Invoices','aos_invoices','id','aos_quotes_aos_invoices_c','aos_quotes77d9_quotes_ida','aos_quotes6b83nvoices_idb','many-to-many',NULL,NULL,0,0),('63ef18fd-6e7c-f0a8-94b3-5f4e0f42a469','aos_quotes_project','AOS_Quotes','aos_quotes','id','Project','project','id','aos_quotes_project_c','aos_quotes1112_quotes_ida','aos_quotes7207project_idb','many-to-many',NULL,NULL,0,0),('64196783-af2e-bb19-2a25-5f4e0fcadf3d','aow_processed_aow_actions','AOW_Processed','aow_processed','id','AOW_Actions','aow_actions','id','aow_processed_aow_actions','aow_processed_id','aow_action_id','many-to-many',NULL,NULL,0,0),('6445ac35-94b3-f7d7-c7e7-5f4e0f4af39f','fp_event_locations_fp_events_1','FP_Event_Locations','fp_event_locations','id','FP_events','fp_events','id','fp_event_locations_fp_events_1_c','fp_event_locations_fp_events_1fp_event_locations_ida','fp_event_locations_fp_events_1fp_events_idb','many-to-many',NULL,NULL,0,0),('64705677-8a81-e3a1-4fd9-5f4e0f29bd38','fp_events_contacts','FP_events','fp_events','id','Contacts','contacts','id','fp_events_contacts_c','fp_events_contactsfp_events_ida','fp_events_contactscontacts_idb','many-to-many',NULL,NULL,0,0),('64b54731-5c3f-8a12-df14-5f4e0f7e1234','fp_events_fp_event_locations_1','FP_events','fp_events','id','FP_Event_Locations','fp_event_locations','id','fp_events_fp_event_locations_1_c','fp_events_fp_event_locations_1fp_events_ida','fp_events_fp_event_locations_1fp_event_locations_idb','many-to-many',NULL,NULL,0,0),('64e1937e-7f9d-284a-3230-5f4e0f0cd020','fp_events_leads_1','FP_events','fp_events','id','Leads','leads','id','fp_events_leads_1_c','fp_events_leads_1fp_events_ida','fp_events_leads_1leads_idb','many-to-many',NULL,NULL,0,0),('650beeba-a1f7-ba19-c8f9-5f4e0fd3f209','fp_events_prospects_1','FP_events','fp_events','id','Prospects','prospects','id','fp_events_prospects_1_c','fp_events_prospects_1fp_events_ida','fp_events_prospects_1prospects_idb','many-to-many',NULL,NULL,0,0),('653673cc-0d14-32b0-9011-5f4e0f75e1e9','jjwg_maps_jjwg_areas','jjwg_Maps','jjwg_maps','id','jjwg_Areas','jjwg_areas','id','jjwg_maps_jjwg_areas_c','jjwg_maps_5304wg_maps_ida','jjwg_maps_41f2g_areas_idb','many-to-many',NULL,NULL,0,0),('65685d90-7d1c-edd7-504b-5f4e0f78c0cc','jjwg_maps_jjwg_markers','jjwg_Maps','jjwg_maps','id','jjwg_Markers','jjwg_markers','id','jjwg_maps_jjwg_markers_c','jjwg_maps_b229wg_maps_ida','jjwg_maps_2e31markers_idb','many-to-many',NULL,NULL,0,0),('65ae20f6-71f9-25e3-d38d-5f4e0fae4ebd','project_contacts_1','Project','project','id','Contacts','contacts','id','project_contacts_1_c','project_contacts_1project_ida','project_contacts_1contacts_idb','many-to-many',NULL,NULL,0,0),('65ef589c-d471-297a-d2b6-5f4e0f8ff69f','project_users_1','Project','project','id','Users','users','id','project_users_1_c','project_users_1project_ida','project_users_1users_idb','many-to-many',NULL,NULL,0,0),('661b13bc-dea4-d250-badb-5f4e0f7119b4','securitygroups_acl_roles','SecurityGroups','securitygroups','id','ACLRoles','acl_roles','id','securitygroups_acl_roles','securitygroup_id','role_id','many-to-many',NULL,NULL,0,0),('671e8b68-7258-fa24-3567-5f4e0f9e5b37','securitygroups_project_task','SecurityGroups','securitygroups','id','ProjectTask','project_task','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','ProjectTask',0,0),('6765432d-b638-bb31-c83f-5f4e0f6afbae','securitygroups_prospect_lists','SecurityGroups','securitygroups','id','ProspectLists','prospect_lists','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','ProspectLists',0,0),('67c780d9-c61d-0beb-6dc4-5f4e0f080383','securitygroups_users','SecurityGroups','securitygroups','id','Users','users','id','securitygroups_users','securitygroup_id','user_id','many-to-many',NULL,NULL,0,0),('67f1bd9d-e738-457c-e44d-5f4e0f6ee46d','surveyquestionoptions_surveyquestionresponses','SurveyQuestionOptions','surveyquestionoptions','id','SurveyQuestionResponses','surveyquestionresponses','id','surveyquestionoptions_surveyquestionresponses','surveyq72c7options_ida','surveyq10d4sponses_idb','many-to-many',NULL,NULL,0,0),('681ca32f-6c30-2b34-fd15-5f4e0fb698ea','accounts_sp_timesheet_tracker_1','Accounts','accounts','id','sp_timesheet_tracker','sp_timesheet_tracker','id','accounts_sp_timesheet_tracker_1_c','accounts_sp_timesheet_tracker_1accounts_ida','accounts_sp_timesheet_tracker_1sp_timesheet_tracker_idb','many-to-many',NULL,NULL,0,0),('6846c887-2220-8ee0-b5f8-5f4e0f60468d','project_sp_timesheet_tracker_1','Project','project','id','sp_timesheet_tracker','sp_timesheet_tracker','id','project_sp_timesheet_tracker_1_c','project_sp_timesheet_tracker_1project_ida','project_sp_timesheet_tracker_1sp_timesheet_tracker_idb','many-to-many',NULL,NULL,0,0),('6f7a0260-8ba6-38db-abca-5f4e0fb4ffa8','sugarfeed_modified_user','Users','users','id','SugarFeed','sugarfeed','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('73c10406-50ee-d036-b779-5f4e0f330961','sugarfeed_created_by','Users','users','id','SugarFeed','sugarfeed','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('76c50c53-2584-97c1-ca29-5f4e0f9192e2','sugarfeed_assigned_user','Users','users','id','SugarFeed','sugarfeed','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('a2d80b7a-b616-c420-c746-5f4e0fbd22a1','eapm_modified_user','Users','users','id','EAPM','eapm','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('a5603c03-f8a6-b1b3-5b1b-5f4e0fac8f98','leads_modified_user','Users','users','id','Leads','leads','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('a5e5b67b-e7cd-dda7-3d3d-5f4e0f17d951','leads_created_by','Users','users','id','Leads','leads','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('a62791e1-2bb8-6286-0957-5f4e0f2979fe','leads_assigned_user','Users','users','id','Leads','leads','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('a6506843-3439-f8c5-9106-5f4e0f4e301c','securitygroups_leads','SecurityGroups','securitygroups','id','Leads','leads','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','Leads',0,0),('a683337c-c9f3-d198-0528-5f4e0fc9ecf9','leads_email_addresses','Leads','leads','id','EmailAddresses','email_addresses','id','email_addr_bean_rel','bean_id','email_address_id','many-to-many','bean_module','Leads',0,0),('a6bc1581-6739-d76a-6dcd-5f4e0fad853b','leads_email_addresses_primary','Leads','leads','id','EmailAddresses','email_addresses','id','email_addr_bean_rel','bean_id','email_address_id','many-to-many','primary_address','1',0,0),('a6e7c5f9-e5dc-e736-8254-5f4e0f20d525','lead_direct_reports','Leads','leads','id','Leads','leads','reports_to_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('a7278dff-ecec-bd8a-ef0c-5f4e0fb5a762','lead_tasks','Leads','leads','id','Tasks','tasks','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Leads',0,0),('a75115d3-efd1-98fc-7353-5f4e0fcf390b','lead_notes','Leads','leads','id','Notes','notes','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Leads',0,0),('a7530dcc-09be-9111-f774-5f4e0f685d54','eapm_created_by','Users','users','id','EAPM','eapm','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('a77fa827-755c-7a19-ca4a-5f4e0f18b6a4','lead_meetings','Leads','leads','id','Meetings','meetings','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Leads',0,0),('a7b1943c-f019-aada-7715-5f4e0ff30735','lead_calls','Leads','leads','id','Calls','calls','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Leads',0,0),('a7dec171-fcb8-4f80-bb7e-5f4e0fbd0303','lead_emails','Leads','leads','id','Emails','emails','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Leads',0,0),('a821377d-5446-c7f0-550d-5f4e0f79e9e2','lead_campaign_log','Leads','leads','id','CampaignLog','campaign_log','target_id',NULL,NULL,NULL,'one-to-many','target_type','Leads',0,0),('aa1e6bf1-7fd2-b449-8635-5f4e0fe6f8a4','cases_modified_user','Users','users','id','Cases','cases','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('aa280234-a7ab-8940-93ab-5f4e0f673416','eapm_assigned_user','Users','users','id','EAPM','eapm','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('aa65a2d1-c7a3-5655-04b3-5f4e0f6ae0cf','cases_created_by','Users','users','id','Cases','cases','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('aa8f46af-8cdd-b883-7c50-5f4e0f9dd536','cases_assigned_user','Users','users','id','Cases','cases','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('aab9d728-23d1-f405-e9bb-5f4e0f1ac96b','securitygroups_cases','SecurityGroups','securitygroups','id','Cases','cases','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','Cases',0,0),('ab05607c-6ced-1660-845f-5f4e0f01886b','case_calls','Cases','cases','id','Calls','calls','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Cases',0,0),('ab3dc061-6d10-68dc-6f3b-5f4e0f4a1fa0','case_tasks','Cases','cases','id','Tasks','tasks','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Cases',0,0),('ab685d01-db27-4874-a331-5f4e0f0c27bd','case_notes','Cases','cases','id','Notes','notes','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Cases',0,0),('ab9366e8-1ed8-ad3e-e0af-5f4e0f0e251b','case_meetings','Cases','cases','id','Meetings','meetings','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Cases',0,0),('abc4494d-9052-f5e1-d241-5f4e0f5baec8','case_emails','Cases','cases','id','Emails','emails','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Cases',0,0),('ac089234-c28d-bdba-318f-5f4e0f2b23ab','cases_created_contact','Contacts','contacts','id','Cases','cases','contact_created_by_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('ad99b3c2-d049-91b7-39f2-5f4e0ff3dc5a','bugs_modified_user','Users','users','id','Bugs','bugs','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('adea1349-a9ae-79bd-3369-5f4e0fe90773','bugs_created_by','Users','users','id','Bugs','bugs','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('ae2b3fb0-e985-94eb-c64e-5f4e0f7da2cc','bugs_assigned_user','Users','users','id','Bugs','bugs','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('ae745c88-6c98-4cf7-eaa5-5f4e0f6c2834','securitygroups_bugs','SecurityGroups','securitygroups','id','Bugs','bugs','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','Bugs',0,0),('aea31d25-b5c0-e9ee-4c3e-5f4e0f775a42','bug_tasks','Bugs','bugs','id','Tasks','tasks','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Bugs',0,0),('aef42d77-e9c9-76fe-cf5b-5f4e0fb2a0ac','bug_meetings','Bugs','bugs','id','Meetings','meetings','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Bugs',0,0),('af205d4a-6c9e-4f57-5eed-5f4e0f7dc243','bug_calls','Bugs','bugs','id','Calls','calls','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Bugs',0,0),('af5e2946-52d2-6724-e0d7-5f4e0f45f290','bug_emails','Bugs','bugs','id','Emails','emails','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Bugs',0,0),('afa003d4-0c07-e5d4-11c8-5f4e0fbdb1e5','bug_notes','Bugs','bugs','id','Notes','notes','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Bugs',0,0),('affc3af3-da0f-9953-d2fc-5f4e0f5f5d94','bugs_release','Releases','releases','id','Bugs','bugs','found_in_release',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('b034c0de-d5ad-e1fd-fc1c-5f4e0ff7263b','bugs_fixed_in_release','Releases','releases','id','Bugs','bugs','fixed_in_release',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('b08db8a8-bf17-2a1c-da2f-5f4e0fb0c495','user_direct_reports','Users','users','id','Users','users','reports_to_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('b0e34f6e-15c6-8da4-c2ee-5f4e0f319c06','users_email_addresses','Users','users','id','EmailAddresses','email_addresses','id','email_addr_bean_rel','bean_id','email_address_id','many-to-many','bean_module','Users',0,0),('b11a842f-954e-f0b2-66e0-5f4e0fd974de','users_email_addresses_primary','Users','users','id','EmailAddresses','email_addresses','id','email_addr_bean_rel','bean_id','email_address_id','many-to-many','primary_address','1',0,0),('b2650847-fd03-95a0-60eb-5f4e0feddd6a','campaignlog_contact','CampaignLog','campaign_log','related_id','Contacts','contacts','id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('b2c04199-28a5-6137-2c58-5f4e0f4165b1','campaignlog_lead','CampaignLog','campaign_log','related_id','Leads','leads','id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('b30fcd59-0cda-d698-c8fc-5f4e0f8ab9a9','campaignlog_created_opportunities','CampaignLog','campaign_log','related_id','Opportunities','opportunities','id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('b36717d5-fc75-5041-f68d-5f4e0f36f67e','campaignlog_targeted_users','CampaignLog','campaign_log','target_id','Users','users','id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('b3a3c2a7-9528-6701-08c7-5f4e0fba530d','campaignlog_sent_emails','CampaignLog','campaign_log','related_id','Emails','emails','id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('b5502669-b829-3552-ef36-5f4e0fc5a661','securitygroups_project','SecurityGroups','securitygroups','id','Project','project','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','Project',0,0),('b5b31736-1795-2fdf-e0f8-5f4e0f8832d1','projects_notes','Project','project','id','Notes','notes','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Project',0,0),('b6002abb-043b-889d-5aa2-5f4e0fa074ef','projects_tasks','Project','project','id','Tasks','tasks','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Project',0,0),('b63999c6-b859-ddf9-1cdd-5f4e0f1c568d','projects_meetings','Project','project','id','Meetings','meetings','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Project',0,0),('b674b2f7-553d-a8f9-fcff-5f4e0f68c77e','projects_calls','Project','project','id','Calls','calls','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Project',0,0),('b6cd8b62-f584-0926-bbeb-5f4e0ff230c0','projects_emails','Project','project','id','Emails','emails','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Project',0,0),('b704dc19-5846-2d57-a2d6-5f4e0fcbaadc','projects_project_tasks','Project','project','id','ProjectTask','project_task','project_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('b73d02b9-3fa5-98fc-8ffd-5f4e0f824887','projects_assigned_user','Users','users','id','Project','project','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('b7738200-baa3-7417-8bb4-5f4e0f01b5fc','projects_modified_user','Users','users','id','Project','project','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('b7cb0d03-fb52-91b2-2c67-5f4e0f6de937','projects_created_by','Users','users','id','Project','project','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('b7db0fc3-7b83-ca6e-1b85-5f4e0f6db01a','oauthkeys_modified_user','Users','users','id','OAuthKeys','oauth_consumer','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('ba30af6a-44b4-8e82-d0b8-5f4e0fed1ae8','securitygroups_projecttask','SecurityGroups','securitygroups','id','ProjectTask','project_task','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','ProjectTask',0,0),('ba97e576-c75a-b1fa-1a42-5f4e0f3c2eb9','project_tasks_notes','ProjectTask','project_task','id','Notes','notes','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','ProjectTask',0,0),('bab90361-bbe6-a030-6a0e-5f4e0f06acb1','oauthkeys_created_by','Users','users','id','OAuthKeys','oauth_consumer','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('baefb71b-22ee-6525-00c0-5f4e0f454398','project_tasks_tasks','ProjectTask','project_task','id','Tasks','tasks','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','ProjectTask',0,0),('bb253021-4512-4500-da30-5f4e0febdcf9','project_tasks_meetings','ProjectTask','project_task','id','Meetings','meetings','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','ProjectTask',0,0),('bb6116dc-c8ea-9422-5d9d-5f4e0f63a3ac','project_tasks_calls','ProjectTask','project_task','id','Calls','calls','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','ProjectTask',0,0),('bbbd37ff-64cc-95e5-b775-5f4e0f6b49ae','project_tasks_emails','ProjectTask','project_task','id','Emails','emails','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','ProjectTask',0,0),('bbf0c7a5-654d-742b-e652-5f4e0f20dbc9','project_tasks_assigned_user','Users','users','id','ProjectTask','project_task','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('bc220450-ea87-7bf1-c353-5f4e0fddc788','project_tasks_modified_user','Users','users','id','ProjectTask','project_task','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('bc58312e-12db-8c1b-e900-5f4e0f051b93','project_tasks_created_by','Users','users','id','ProjectTask','project_task','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('be98dea2-3d7c-5e10-c9d1-5f4e0faf0bdf','campaigns_modified_user','Users','users','id','Campaigns','campaigns','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('bed000c5-6443-999e-11ff-5f4e0f64bc00','oauthkeys_assigned_user','Users','users','id','OAuthKeys','oauth_consumer','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('bf0802f5-df67-828d-30c0-5f4e0f6119c8','campaigns_created_by','Users','users','id','Campaigns','campaigns','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('bf400be8-d57e-35d7-6d63-5f4e0f3ca8a9','campaigns_assigned_user','Users','users','id','Campaigns','campaigns','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('bf95d70f-7d78-2953-2623-5f4e0f3cef0a','securitygroups_campaigns','SecurityGroups','securitygroups','id','Campaigns','campaigns','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','Campaigns',0,0),('bfcc41e2-2303-5eb7-8cb6-5f4e0f9bfa0b','campaign_accounts','Campaigns','campaigns','id','Accounts','accounts','campaign_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('bffc5ddf-d212-ae68-e028-5f4e0f6c691c','campaign_contacts','Campaigns','campaigns','id','Contacts','contacts','campaign_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('c0288d90-fcda-4ee5-e6f0-5f4e0fa22793','campaign_leads','Campaigns','campaigns','id','Leads','leads','campaign_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('c0586370-2bec-745b-6b4d-5f4e0f79b4b1','campaign_prospects','Campaigns','campaigns','id','Prospects','prospects','campaign_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('c0a17d21-1451-4ee8-b3c6-5f4e0fbdf91b','campaign_opportunities','Campaigns','campaigns','id','Opportunities','opportunities','campaign_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('c0cfbbb9-c27e-26da-886d-5f4e0f81cb31','campaign_email_marketing','Campaigns','campaigns','id','EmailMarketing','email_marketing','campaign_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('c0fdcf8f-3c4f-96b7-ccd8-5f4e0f436851','campaign_emailman','Campaigns','campaigns','id','EmailMan','emailman','campaign_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('c1314aa8-8624-ad81-be46-5f4e0fa36f87','campaign_campaignlog','Campaigns','campaigns','id','CampaignLog','campaign_log','campaign_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('c1b17288-2cd9-7c20-faa9-5f4e0f7ce0d1','campaign_assigned_user','Users','users','id','Campaigns','campaigns','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('c1e44d5c-3082-abb7-f788-5f4e0f61c012','campaign_modified_user','Users','users','id','Campaigns','campaigns','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('c2212f61-7fe0-247a-283e-5f4e0f1bad17','surveyresponses_campaigns','Campaigns','campaigns','id','SurveyResponses','surveyresponses','campaign_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('c32951e9-4e01-140c-d37a-5f4e0f36882f','prospectlists_assigned_user','Users','users','id','prospectlists','prospect_lists','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('c3843149-62d9-7577-4082-5f4e0f153074','securitygroups_prospectlists','SecurityGroups','securitygroups','id','ProspectLists','prospect_lists','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','ProspectLists',0,0),('c5e144f7-2f83-5eb4-5a95-5f4e0fc81ce4','prospects_modified_user','Users','users','id','Prospects','prospects','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('c63733ab-692d-c7a7-ae98-5f4e0f26c1ae','prospects_created_by','Users','users','id','Prospects','prospects','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('c6846f3b-eb78-ea33-0eee-5f4e0ffbafb4','prospects_assigned_user','Users','users','id','Prospects','prospects','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('c6c195ca-7df6-35fc-5e82-5f4e0ff3048f','securitygroups_prospects','SecurityGroups','securitygroups','id','Prospects','prospects','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','Prospects',0,0),('c6fc079b-a778-071d-5a91-5f4e0f9a686b','prospects_email_addresses','Prospects','prospects','id','EmailAddresses','email_addresses','id','email_addr_bean_rel','bean_id','email_address_id','many-to-many','bean_module','Prospects',0,0),('c7604419-70a6-3ade-d0d3-5f4e0f97249f','prospects_email_addresses_primary','Prospects','prospects','id','EmailAddresses','email_addresses','id','email_addr_bean_rel','bean_id','email_address_id','many-to-many','primary_address','1',0,0),('c7afd381-6f4a-0777-60c0-5f4e0fa577bf','prospect_tasks','Prospects','prospects','id','Tasks','tasks','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Prospects',0,0),('c7dda821-4ddf-aa9b-9e8d-5f4e0f013231','prospect_notes','Prospects','prospects','id','Notes','notes','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Prospects',0,0),('c808f8b6-e625-b4c8-1b4d-5f4e0fa4f23a','prospect_meetings','Prospects','prospects','id','Meetings','meetings','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Prospects',0,0),('c851cddd-0795-c370-c8e2-5f4e0f0e67e2','prospect_calls','Prospects','prospects','id','Calls','calls','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Prospects',0,0),('c8825762-052d-6fe4-d5da-5f4e0f0ff031','prospect_emails','Prospects','prospects','id','Emails','emails','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Prospects',0,0),('c8b3eb65-1d1a-aa95-eb7b-5f4e0fbb3621','prospect_campaign_log','Prospects','prospects','id','CampaignLog','campaign_log','target_id',NULL,NULL,NULL,'one-to-many','target_type','Prospects',0,0),('c8c104f3-3ee3-b297-725c-5f4e0f60ab9a','consumer_tokens','OAuthKeys','oauth_consumer','id','OAuthTokens','oauth_tokens','consumer',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('ca2f489c-9d2a-6747-8cdf-5f4e0fcf4d01','email_template_email_marketings','EmailTemplates','email_templates','id','EmailMarketing','email_marketing','template_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('cae7c999-f955-fecc-c657-5f4e0fe646f1','campaign_campaigntrakers','Campaigns','campaigns','id','CampaignTrackers','campaign_trkrs','campaign_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('ccde029d-456d-4c58-b12c-5f4e0f57e392','oauthtokens_assigned_user','Users','users','id','OAuthTokens','oauth_tokens','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('cdb96dd9-ebf6-475a-1174-5f4e0f548ad6','schedulers_created_by_rel','Users','users','id','Schedulers','schedulers','created_by',NULL,NULL,NULL,'one-to-one',NULL,NULL,0,0),('ce089345-3d38-4baa-7448-5f4e0f91311e','schedulers_modified_user_id_rel','Users','users','id','Schedulers','schedulers','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('ce4b0c24-db37-adda-662c-5f4e0ffa4b2b','schedulers_jobs_rel','Schedulers','schedulers','id','SchedulersJobs','job_queue','scheduler_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('ce88b4b0-a2db-0ff6-e54c-5f4e0fbc8984','schedulersjobs_assigned_user','Users','users','id','SchedulersJobs','job_queue','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('d05abe0a-d505-1457-db10-5f4e0f327556','contacts_modified_user','Users','users','id','Contacts','contacts','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('d0ab483f-275d-81f4-88ee-5f4e0f5eb86a','contacts_created_by','Users','users','id','Contacts','contacts','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('d0dd7cab-5a19-1329-339b-5f4e0fece459','contacts_assigned_user','Users','users','id','Contacts','contacts','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('d1215efc-4927-9bc7-c897-5f4e0f28310b','securitygroups_contacts','SecurityGroups','securitygroups','id','Contacts','contacts','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','Contacts',0,0),('d15c02e6-9637-2469-5de9-5f4e0fbffa5a','contacts_email_addresses','Contacts','contacts','id','EmailAddresses','email_addresses','id','email_addr_bean_rel','bean_id','email_address_id','many-to-many','bean_module','Contacts',0,0),('d1879b39-a803-8328-803c-5f4e0ffd5bfb','contacts_email_addresses_primary','Contacts','contacts','id','EmailAddresses','email_addresses','id','email_addr_bean_rel','bean_id','email_address_id','many-to-many','primary_address','1',0,0),('d1b562df-7caa-89be-da34-5f4e0fab5542','contact_direct_reports','Contacts','contacts','id','Contacts','contacts','reports_to_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('d1e56def-64a4-bf9f-6712-5f4e0f10fe3e','contact_leads','Contacts','contacts','id','Leads','leads','contact_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('d22a254d-428c-4c95-68f7-5f4e0f604833','contact_notes','Contacts','contacts','id','Notes','notes','contact_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('d255d165-e5bc-78a1-f3bd-5f4e0f163160','contact_tasks','Contacts','contacts','id','Tasks','tasks','contact_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('d2802ee4-2970-0b7b-d7c0-5f4e0fca6626','contact_tasks_parent','Contacts','contacts','id','Tasks','tasks','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Contacts',0,0),('d2b12b1a-9234-83b0-7f0a-5f4e0fd4782b','contact_notes_parent','Contacts','contacts','id','Notes','notes','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Contacts',0,0),('d2e12170-c900-808f-7e37-5f4e0f73644a','contact_campaign_log','Contacts','contacts','id','CampaignLog','campaign_log','target_id',NULL,NULL,NULL,'one-to-many','target_type','Contacts',0,0),('d3229faa-426c-0d13-1fd3-5f4e0fbd9eb8','contact_aos_quotes','Contacts','contacts','id','AOS_Quotes','aos_quotes','billing_contact_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('d34d34ba-0432-a5b0-f853-5f4e0f08a791','contact_aos_invoices','Contacts','contacts','id','AOS_Invoices','aos_invoices','billing_contact_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('d37695bc-81ae-9de9-3bc5-5f4e0f7d5a61','contact_aos_contracts','Contacts','contacts','id','AOS_Contracts','aos_contracts','contact_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('d39f984e-366b-2578-2998-5f4e0f9c0831','contacts_aop_case_updates','Contacts','contacts','id','AOP_Case_Updates','aop_case_updates','contact_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('d6f4b2b0-2871-84bf-45cc-5f4e0f7935e5','accounts_modified_user','Users','users','id','Accounts','accounts','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('d74f70ec-da71-e112-a857-5f4e0f451d52','accounts_created_by','Users','users','id','Accounts','accounts','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('d77c628c-3b9c-2bc2-b866-5f4e0fdb2544','accounts_assigned_user','Users','users','id','Accounts','accounts','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('d7b3006d-c10b-6db0-6b43-5f4e0f077650','securitygroups_accounts','SecurityGroups','securitygroups','id','Accounts','accounts','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','Accounts',0,0),('d7fe2a21-1788-0e09-88d6-5f4e0fc7184b','accounts_email_addresses','Accounts','accounts','id','EmailAddresses','email_addresses','id','email_addr_bean_rel','bean_id','email_address_id','many-to-many','bean_module','Accounts',0,0),('d80a0ebb-a265-739a-df91-5f4e0f7e57dd','am_projecttemplates_modified_user','Users','users','id','AM_ProjectTemplates','am_projecttemplates','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('d82cad99-3928-2056-a8cf-5f4e0f9c7687','accounts_email_addresses_primary','Accounts','accounts','id','EmailAddresses','email_addresses','id','email_addr_bean_rel','bean_id','email_address_id','many-to-many','primary_address','1',0,0),('d8736551-2e9e-d1bc-7ded-5f4e0ffbc4ef','member_accounts','Accounts','accounts','id','Accounts','accounts','parent_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('d89d9d3f-8b19-87c7-a87e-5f4e0fec74c5','account_cases','Accounts','accounts','id','Cases','cases','account_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('d8de9fc7-4330-51d7-bcca-5f4e0ff8c390','account_tasks','Accounts','accounts','id','Tasks','tasks','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Accounts',0,0),('d90a59be-2404-2819-50df-5f4e0f95387e','account_notes','Accounts','accounts','id','Notes','notes','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Accounts',0,0),('d9333f6d-1374-ce72-06ca-5f4e0f414afa','account_meetings','Accounts','accounts','id','Meetings','meetings','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Accounts',0,0),('d95e097f-c708-c6f8-0c32-5f4e0f73394b','account_calls','Accounts','accounts','id','Calls','calls','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Accounts',0,0),('d994fda1-058f-7858-8255-5f4e0f8daa8b','account_emails','Accounts','accounts','id','Emails','emails','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Accounts',0,0),('d9c7e616-9503-c962-cd6f-5f4e0fe69e28','account_leads','Accounts','accounts','id','Leads','leads','account_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('da0b3fbe-9d38-eaa6-9941-5f4e0f36bde2','account_campaign_log','Accounts','accounts','id','CampaignLog','campaign_log','target_id',NULL,NULL,NULL,'one-to-many','target_type','Accounts',0,0),('da3917d9-0d81-756b-2f86-5f4e0f921bca','account_aos_quotes','Accounts','accounts','id','AOS_Quotes','aos_quotes','billing_account_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('da6240a3-4f39-8b7a-e693-5f4e0fae9f69','account_aos_invoices','Accounts','accounts','id','AOS_Invoices','aos_invoices','billing_account_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('da8c6747-821a-3aaf-0bea-5f4e0fcf12ac','account_aos_contracts','Accounts','accounts','id','AOS_Contracts','aos_contracts','contract_account_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('dc2dcfb0-9cf2-9755-3b0f-5f4e0f0e0d72','opportunities_modified_user','Users','users','id','Opportunities','opportunities','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('dc752009-c44e-cbc9-8769-5f4e0fe5de5c','opportunities_created_by','Users','users','id','Opportunities','opportunities','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('dc800029-e14b-586e-b27a-5f4e0f74dd3e','am_projecttemplates_created_by','Users','users','id','AM_ProjectTemplates','am_projecttemplates','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('dca135bb-a723-af7d-1680-5f4e0fcd96c8','opportunities_assigned_user','Users','users','id','Opportunities','opportunities','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('dce1cd27-f894-fc79-6622-5f4e0f47649e','securitygroups_opportunities','SecurityGroups','securitygroups','id','Opportunities','opportunities','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','Opportunities',0,0),('dd0c0522-2f58-44fe-8976-5f4e0f2216d4','opportunity_calls','Opportunities','opportunities','id','Calls','calls','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Opportunities',0,0),('dd33e1dc-e978-56ad-c0cb-5f4e0f4ae5d2','opportunity_meetings','Opportunities','opportunities','id','Meetings','meetings','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Opportunities',0,0),('dd5bf18c-86ce-7bfe-9f3a-5f4e0f122c7b','opportunity_tasks','Opportunities','opportunities','id','Tasks','tasks','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Opportunities',0,0),('dd84dc0f-b1a0-2d91-36cf-5f4e0f30e460','opportunity_notes','Opportunities','opportunities','id','Notes','notes','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Opportunities',0,0),('ddc67279-0391-4cca-10f5-5f4e0fbc701f','opportunity_emails','Opportunities','opportunities','id','Emails','emails','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Opportunities',0,0),('ddeff79b-c305-d743-29dd-5f4e0f200f9e','opportunity_leads','Opportunities','opportunities','id','Leads','leads','opportunity_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('de187706-db88-b147-2c86-5f4e0f85f392','opportunity_currencies','Opportunities','opportunities','currency_id','Currencies','currencies','id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('de3fea04-5103-9ce7-847d-5f4e0f66723d','opportunities_campaign','Campaigns','campaigns','id','Opportunities','opportunities','campaign_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('de68aea5-a824-8fcb-7a35-5f4e0f3f0935','opportunity_aos_quotes','Opportunities','opportunities','id','AOS_Quotes','aos_quotes','opportunity_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('de94f2e6-1cdf-5d39-2385-5f4e0fe0d0b9','opportunity_aos_contracts','Opportunities','opportunities','id','AOS_Contracts','aos_contracts','opportunity_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('df5201ab-238f-c8da-363c-5f4e0fe11d81','am_projecttemplates_assigned_user','Users','users','id','AM_ProjectTemplates','am_projecttemplates','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('e03c6daf-9762-1862-ee30-5f4e0fd50107','securitygroups_emailtemplates','SecurityGroups','securitygroups','id','EmailTemplates','email_templates','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','EmailTemplates',0,0),('e081aa9e-d5bb-226f-f1f4-5f4e0fe46aa9','emailtemplates_assigned_user','Users','users','id','EmailTemplates','email_templates','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('e170e4c2-85f5-dfaf-803d-5f4e0f640cd8','notes_assigned_user','Users','users','id','Notes','notes','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('e1b8a8c9-c03c-25bb-5fd0-5f4e0f655665','securitygroups_notes','SecurityGroups','securitygroups','id','Notes','notes','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','Notes',0,0),('e1e4e10b-7ea0-2393-6e92-5f4e0f39efeb','notes_modified_user','Users','users','id','Notes','notes','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('e20dc9a1-4171-c03b-4e7a-5f4e0ff6ad3a','notes_created_by','Users','users','id','Notes','notes','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('e30f7d61-cdaf-f49a-a60f-5f4e0fb2225b','calls_modified_user','Users','users','id','Calls','calls','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('e3516fa5-2e62-df63-f18d-5f4e0ff615aa','calls_created_by','Users','users','id','Calls','calls','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('e37e3296-55a0-900e-f6d6-5f4e0f0719dc','calls_assigned_user','Users','users','id','Calls','calls','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('e3bc5bf3-b7e6-c92c-8035-5f4e0f71a860','securitygroups_calls','SecurityGroups','securitygroups','id','Calls','calls','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','Calls',0,0),('e3ea9a80-a949-3852-b4a8-5f4e0fb782b0','calls_notes','Calls','calls','id','Notes','notes','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Calls',0,0),('e4183177-8d75-f7ba-c7d7-5f4e0f272118','calls_reschedule','Calls','calls','id','Calls_Reschedule','calls_reschedule','call_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('e533e1a2-bf9e-7e88-1788-5f4e0f26b974','emails_modified_user','Users','users','id','Emails','emails','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('e57bd987-b589-5ba1-7be0-5f4e0f5fac07','emails_created_by','Users','users','id','Emails','emails','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('e5b78550-b3c0-f287-77da-5f4e0fc4011a','emails_assigned_user','Users','users','id','Emails','emails','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('e5dfb518-032d-c1a1-f3b5-5f4e0f89c85a','securitygroups_emails','SecurityGroups','securitygroups','id','Emails','emails','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','Emails',0,0),('e609109e-1e19-e8cd-0acc-5f4e0fbd5fdc','emails_notes_rel','Emails','emails','id','Notes','notes','parent_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('e632a0fc-d7db-e6d7-b35e-5f4e0f2d69e2','emails_contacts_rel','Emails','emails','id','Contacts','contacts','id','emails_beans','email_id','bean_id','many-to-many','bean_module','Contacts',0,0),('e660ea03-3f56-1cad-1239-5f4e0fb44368','emails_accounts_rel','Emails','emails','id','Accounts','accounts','id','emails_beans','email_id','bean_id','many-to-many','bean_module','Accounts',0,0),('e6b829b4-b87b-50ca-9f14-5f4e0fcf1b21','emails_leads_rel','Emails','emails','id','Leads','leads','id','emails_beans','email_id','bean_id','many-to-many','bean_module','Leads',0,0),('e6f0e544-fbc6-bbde-9d4c-5f4e0fa43859','emails_aos_contracts_rel','Emails','emails','id','AOS_Contracts','aos_contracts','id','emails_beans','email_id','bean_id','many-to-many','bean_module','AOS_Contracts',0,0),('e72c3fc9-9e89-d719-a275-5f4e0f9e2c04','emails_meetings_rel','Emails','emails','id','Meetings','meetings','parent_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('e8594ca2-5ba6-5e38-118e-5f4e0fb4e7a1','meetings_modified_user','Users','users','id','Meetings','meetings','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('e8a17f23-730f-6a83-9069-5f4e0f17d688','meetings_created_by','Users','users','id','Meetings','meetings','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('e8d69381-e6cc-0bc9-3228-5f4e0fe01ed5','meetings_assigned_user','Users','users','id','Meetings','meetings','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('e905e3ab-1492-58f7-6355-5f4e0f9668ec','securitygroups_meetings','SecurityGroups','securitygroups','id','Meetings','meetings','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','Meetings',0,0),('e9328e72-e836-02f8-fc49-5f4e0fb969f1','meetings_notes','Meetings','meetings','id','Notes','notes','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Meetings',0,0),('ea567e81-de6d-6f39-16ae-5f4e0fc6e867','tasks_modified_user','Users','users','id','Tasks','tasks','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('ea9efc69-a6ed-c387-8180-5f4e0f6aaa61','tasks_created_by','Users','users','id','Tasks','tasks','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('eac913c3-f360-4c45-4437-5f4e0fbfe2c7','tasks_assigned_user','Users','users','id','Tasks','tasks','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('eafd314a-8670-0916-964b-5f4e0fcc7a51','securitygroups_tasks','SecurityGroups','securitygroups','id','Tasks','tasks','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','Tasks',0,0),('eb31041c-cb0d-55bd-01bb-5f4e0f737314','tasks_notes','Tasks','tasks','id','Notes','notes','parent_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('ec600e1d-54cd-7865-ec53-5f4e0f57fab0','am_tasktemplates_modified_user','Users','users','id','AM_TaskTemplates','am_tasktemplates','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('ef13b222-f21e-c026-bc7d-5f4e0f8f53c0','alerts_modified_user','Users','users','id','Alerts','alerts','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('ef6499a8-e87e-b9c5-7fa3-5f4e0f898b5c','alerts_created_by','Users','users','id','Alerts','alerts','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('ef99f4a6-e360-379b-668b-5f4e0f71c75e','alerts_assigned_user','Users','users','id','Alerts','alerts','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('f0d36a4e-87c7-bfec-d7ed-5f4e0fbe4f19','documents_modified_user','Users','users','id','Documents','documents','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('f11c6cde-cd33-d55e-3c6b-5f4e0f8c2436','documents_created_by','Users','users','id','Documents','documents','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('f15d3af9-e604-5a13-6e8a-5f4e0f0770b2','documents_assigned_user','Users','users','id','Documents','documents','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('f185e40b-a9b1-3f3b-4c5b-5f4e0f35e1fe','securitygroups_documents','SecurityGroups','securitygroups','id','Documents','documents','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','Documents',0,0),('f19a07a4-9a12-81b4-3bbd-5f4e0f601f21','am_tasktemplates_created_by','Users','users','id','AM_TaskTemplates','am_tasktemplates','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('f1ae07be-5c26-e1b3-16d5-5f4e0f9eae81','document_revisions','Documents','documents','id','DocumentRevisions','document_revisions','document_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('f284c4ff-c39d-cf43-7940-5f4e0f87fdc9','revisions_created_by','Users','users','id','DocumentRevisions','document_revisions','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('f4360864-2e41-00ab-7e87-5f4e0fbcdec3','am_tasktemplates_assigned_user','Users','users','id','AM_TaskTemplates','am_tasktemplates','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('feb10126-868b-d41e-0249-5f4e0f66c707','favorites_modified_user','Users','users','id','Favorites','favorites','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0);
/*!40000 ALTER TABLE `relationships` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `releases`
--

DROP TABLE IF EXISTS `releases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `releases` (
  `id` char(36) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `list_order` int(4) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_releases` (`name`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `releases`
--

LOCK TABLES `releases` WRITE;
/*!40000 ALTER TABLE `releases` DISABLE KEYS */;
/*!40000 ALTER TABLE `releases` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reminders`
--

DROP TABLE IF EXISTS `reminders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reminders` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `popup` tinyint(1) DEFAULT NULL,
  `email` tinyint(1) DEFAULT NULL,
  `email_sent` tinyint(1) DEFAULT NULL,
  `timer_popup` varchar(32) DEFAULT NULL,
  `timer_email` varchar(32) DEFAULT NULL,
  `related_event_module` varchar(32) DEFAULT NULL,
  `related_event_module_id` char(36) NOT NULL,
  `date_willexecute` int(60) DEFAULT '-1',
  `popup_viewed` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_reminder_name` (`name`),
  KEY `idx_reminder_deleted` (`deleted`),
  KEY `idx_reminder_related_event_module` (`related_event_module`),
  KEY `idx_reminder_related_event_module_id` (`related_event_module_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reminders`
--

LOCK TABLES `reminders` WRITE;
/*!40000 ALTER TABLE `reminders` DISABLE KEYS */;
/*!40000 ALTER TABLE `reminders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reminders_invitees`
--

DROP TABLE IF EXISTS `reminders_invitees`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reminders_invitees` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `reminder_id` char(36) NOT NULL,
  `related_invitee_module` varchar(32) DEFAULT NULL,
  `related_invitee_module_id` char(36) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_reminder_invitee_name` (`name`),
  KEY `idx_reminder_invitee_assigned_user_id` (`assigned_user_id`),
  KEY `idx_reminder_invitee_reminder_id` (`reminder_id`),
  KEY `idx_reminder_invitee_related_invitee_module` (`related_invitee_module`),
  KEY `idx_reminder_invitee_related_invitee_module_id` (`related_invitee_module_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reminders_invitees`
--

LOCK TABLES `reminders_invitees` WRITE;
/*!40000 ALTER TABLE `reminders_invitees` DISABLE KEYS */;
/*!40000 ALTER TABLE `reminders_invitees` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` char(36) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `name` varchar(150) DEFAULT NULL,
  `description` text,
  `modules` text,
  `deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_role_id_del` (`id`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles_modules`
--

DROP TABLE IF EXISTS `roles_modules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles_modules` (
  `id` varchar(36) NOT NULL,
  `role_id` varchar(36) DEFAULT NULL,
  `module_id` varchar(36) DEFAULT NULL,
  `allow` tinyint(1) DEFAULT '0',
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_role_id` (`role_id`),
  KEY `idx_module_id` (`module_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles_modules`
--

LOCK TABLES `roles_modules` WRITE;
/*!40000 ALTER TABLE `roles_modules` DISABLE KEYS */;
/*!40000 ALTER TABLE `roles_modules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles_users`
--

DROP TABLE IF EXISTS `roles_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles_users` (
  `id` varchar(36) NOT NULL,
  `role_id` varchar(36) DEFAULT NULL,
  `user_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_ru_role_id` (`role_id`),
  KEY `idx_ru_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles_users`
--

LOCK TABLES `roles_users` WRITE;
/*!40000 ALTER TABLE `roles_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `roles_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `saved_search`
--

DROP TABLE IF EXISTS `saved_search`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `saved_search` (
  `id` char(36) NOT NULL,
  `name` varchar(150) DEFAULT NULL,
  `search_module` varchar(150) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `assigned_user_id` char(36) DEFAULT NULL,
  `contents` text,
  `description` text,
  PRIMARY KEY (`id`),
  KEY `idx_desc` (`name`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `saved_search`
--

LOCK TABLES `saved_search` WRITE;
/*!40000 ALTER TABLE `saved_search` DISABLE KEYS */;
/*!40000 ALTER TABLE `saved_search` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schedulers`
--

DROP TABLE IF EXISTS `schedulers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schedulers` (
  `id` varchar(36) NOT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `job` varchar(255) DEFAULT NULL,
  `date_time_start` datetime DEFAULT NULL,
  `date_time_end` datetime DEFAULT NULL,
  `job_interval` varchar(100) DEFAULT NULL,
  `time_from` time DEFAULT NULL,
  `time_to` time DEFAULT NULL,
  `last_run` datetime DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `catch_up` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `idx_schedule` (`date_time_start`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schedulers`
--

LOCK TABLES `schedulers` WRITE;
/*!40000 ALTER TABLE `schedulers` DISABLE KEYS */;
INSERT INTO `schedulers` VALUES ('13e60732-95c7-c286-1c4d-57713d1e7aae',0,'2016-06-27 14:51:42','2016-06-27 14:51:42','1','1','TimeSheet - No Activity for Project Tasks','function::no_Activity_For_Project_Tasks','2005-01-01 15:00:00',NULL,'0::0::*::*::*',NULL,NULL,NULL,'Active',1),('907b2280-f4f9-0ecb-f92a-5c6e1dc7fd04',0,'2019-02-21 03:39:38','2019-02-21 03:39:38','1','1','Process Workflow Tasks','function::processAOW_Workflow','2015-01-01 08:45:01',NULL,'*::*::*::*::*',NULL,NULL,NULL,'Active',1),('92eaedd1-3a71-7c36-604d-5c6e1de0a65b',0,'2019-02-21 03:39:38','2019-02-21 03:39:38','1','1','Run Report Generation Scheduled Tasks','function::aorRunScheduledReports','2015-01-01 12:00:01',NULL,'*::*::*::*::*',NULL,NULL,NULL,'Active',1),('9561683d-a850-8035-8eeb-5c6e1d8ca92e',0,'2019-02-21 03:39:38','2019-02-21 03:39:38','1','1','Prune Tracker Tables','function::trimTracker','2015-01-01 06:45:01',NULL,'0::2::1::*::*',NULL,NULL,NULL,'Active',1),('977fe694-01e4-79d7-c428-5c6e1d8de102',0,'2019-02-21 03:39:38','2019-02-21 03:39:38','1','1','Check Inbound Mailboxes','function::pollMonitoredInboxesAOP','2015-01-01 08:30:01',NULL,'*::*::*::*::*',NULL,NULL,NULL,'Active',0),('99a94b37-7b36-e47d-9b47-5c6e1d16f5ae',0,'2019-02-21 03:39:38','2019-02-21 03:39:38','1','1','Run Nightly Process Bounced Campaign Emails','function::pollMonitoredInboxesForBouncedCampaignEmails','2015-01-01 09:45:01',NULL,'0::2-6::*::*::*',NULL,NULL,NULL,'Active',1),('9bc94d43-2e54-45cb-15f2-5c6e1d5f7c36',0,'2019-02-21 03:39:38','2019-02-21 03:39:38','1','1','Run Nightly Mass Email Campaigns','function::runMassEmailCampaign','2015-01-01 16:45:01',NULL,'0::2-6::*::*::*',NULL,NULL,NULL,'Active',1),('9de650f6-f064-5fa3-e02b-5c6e1d6d9a32',0,'2019-02-21 03:39:38','2019-02-21 03:39:38','1','1','Prune Database on 1st of Month','function::pruneDatabase','2015-01-01 09:30:01',NULL,'0::4::1::*::*',NULL,NULL,NULL,'Inactive',0),('9ffcfcbd-4534-38fd-c777-5c6e1daac236',0,'2019-02-21 03:39:38','2019-02-21 03:39:38','1','1','Perform Lucene Index','function::aodIndexUnindexed','2015-01-01 07:30:01',NULL,'0::0::*::*::*',NULL,NULL,NULL,'Active',0),('a20dfa2e-1129-6127-1618-5c6e1dbc5861',0,'2019-02-21 03:39:38','2019-02-21 03:39:38','1','1','Optimise AOD Index','function::aodOptimiseIndex','2015-01-01 06:45:01',NULL,'0::*/3::*::*::*',NULL,NULL,NULL,'Active',0),('a421ee12-883f-cbb3-f55f-5c6e1d1a0ba5',0,'2019-02-21 03:39:38','2019-02-21 03:39:38','1','1','Run Email Reminder Notifications','function::sendEmailReminders','2015-01-01 12:15:01',NULL,'*::*::*::*::*',NULL,NULL,NULL,'Active',0),('a63b4f0e-861d-cc28-3852-5c6e1d793ea4',0,'2019-02-21 03:39:38','2019-02-21 03:39:38','1','1','Clean Jobs Queue','function::cleanJobQueue','2015-01-01 07:15:01',NULL,'0::5::*::*::*',NULL,NULL,NULL,'Active',0),('a865675d-5253-aa7d-bd6b-5c6e1d7266c9',0,'2019-02-21 03:39:38','2019-02-21 03:39:38','1','1','Removal of documents from filesystem','function::removeDocumentsFromFS','2015-01-01 15:15:01',NULL,'0::3::1::*::*',NULL,NULL,NULL,'Active',0),('aace67fb-ffb5-a776-505f-5c6e1decf130',0,'2019-02-21 03:39:38','2019-02-21 03:39:38','1','1','Prune SuiteCRM Feed Tables','function::trimSugarFeeds','2015-01-01 09:15:01',NULL,'0::2::1::*::*',NULL,NULL,NULL,'Active',1),('acfd5a64-1ca2-fe82-b878-5c6e1d753cdc',0,'2019-02-21 03:39:38','2019-02-21 03:39:38','1','1','Google Calendar Sync','function::syncGoogleCalendar','2015-01-01 16:15:01',NULL,'*/15::*::*::*::*',NULL,NULL,NULL,'Active',0);
/*!40000 ALTER TABLE `schedulers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `securitygroups`
--

DROP TABLE IF EXISTS `securitygroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `securitygroups` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `noninheritable` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `securitygroups`
--

LOCK TABLES `securitygroups` WRITE;
/*!40000 ALTER TABLE `securitygroups` DISABLE KEYS */;
INSERT INTO `securitygroups` VALUES ('7eded74d-f508-52c4-76da-5f469c3b4b2a','Manager Global','2020-08-26 17:33:40','2020-08-27 17:47:48','1','1',NULL,0,'b70243be-ca4c-b256-aff8-5f46980acf9f',0);
/*!40000 ALTER TABLE `securitygroups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `securitygroups_acl_roles`
--

DROP TABLE IF EXISTS `securitygroups_acl_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `securitygroups_acl_roles` (
  `id` char(36) NOT NULL,
  `securitygroup_id` char(36) DEFAULT NULL,
  `role_id` char(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `securitygroups_acl_roles`
--

LOCK TABLES `securitygroups_acl_roles` WRITE;
/*!40000 ALTER TABLE `securitygroups_acl_roles` DISABLE KEYS */;
INSERT INTO `securitygroups_acl_roles` VALUES ('2b6f9a9b-a09e-d124-f469-5f469da407af','7eded74d-f508-52c4-76da-5f469c3b4b2a','69d63f4e-88ef-58c9-0a28-5f469b0dc456','2020-08-27 17:47:19',1),('50bc2c96-e093-d222-92aa-5f47f1ca7840','7eded74d-f508-52c4-76da-5f469c3b4b2a','824bc684-1b3b-2184-338e-5f47f0ca9e93','2020-08-27 17:44:53',1);
/*!40000 ALTER TABLE `securitygroups_acl_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `securitygroups_audit`
--

DROP TABLE IF EXISTS `securitygroups_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `securitygroups_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_securitygroups_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `securitygroups_audit`
--

LOCK TABLES `securitygroups_audit` WRITE;
/*!40000 ALTER TABLE `securitygroups_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `securitygroups_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `securitygroups_default`
--

DROP TABLE IF EXISTS `securitygroups_default`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `securitygroups_default` (
  `id` char(36) NOT NULL,
  `securitygroup_id` char(36) DEFAULT NULL,
  `module` varchar(50) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `securitygroups_default`
--

LOCK TABLES `securitygroups_default` WRITE;
/*!40000 ALTER TABLE `securitygroups_default` DISABLE KEYS */;
/*!40000 ALTER TABLE `securitygroups_default` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `securitygroups_records`
--

DROP TABLE IF EXISTS `securitygroups_records`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `securitygroups_records` (
  `id` char(36) NOT NULL,
  `securitygroup_id` char(36) DEFAULT NULL,
  `record_id` char(36) DEFAULT NULL,
  `module` char(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_securitygroups_records_mod` (`module`,`deleted`,`record_id`,`securitygroup_id`),
  KEY `idx_securitygroups_records_del` (`deleted`,`record_id`,`module`,`securitygroup_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `securitygroups_records`
--

LOCK TABLES `securitygroups_records` WRITE;
/*!40000 ALTER TABLE `securitygroups_records` DISABLE KEYS */;
INSERT INTO `securitygroups_records` VALUES ('0e2e14ca-e918-11ea-8a2e-327d913864cf','7eded74d-f508-52c4-76da-5f469c3b4b2a','68b8f5a5-cd40-decc-cebb-5f48da51a3f2','Opportunities','2020-08-28 00:00:00',NULL,NULL,0),('43ffd99d-e918-11ea-8a2e-327d913864cf','7eded74d-f508-52c4-76da-5f469c3b4b2a','b39d0ada-3a30-6236-aefe-5f48dadba3c3','Notes','2020-08-28 00:00:00',NULL,NULL,0),('53476f3a-e918-11ea-8a2e-327d913864cf','7eded74d-f508-52c4-76da-5f469c3b4b2a','5200556a-9070-b927-1928-5f48da876d80','Contacts','2020-08-28 00:00:00',NULL,NULL,0),('67e7daba-e918-11ea-8a2e-327d913864cf','7eded74d-f508-52c4-76da-5f469c3b4b2a','e826f898-71d8-241f-2c0b-5f48da608dc4','Leads','2020-08-28 00:00:00',NULL,NULL,0),('7021aa43-e919-11ea-8a2e-327d913864cf','7eded74d-f508-52c4-76da-5f469c3b4b2a','3898ea01-ac05-7e9f-4dce-5f48dcaf2e89','Contacts','2020-08-28 00:00:00',NULL,NULL,0),('b2cdf51d-e919-11ea-8a2e-327d913864cf','7eded74d-f508-52c4-76da-5f469c3b4b2a','1ee05743-c112-d3a1-163a-5f48dd532edf','Opportunities','2020-08-28 00:00:00',NULL,NULL,0),('c03ef6e0-e919-11ea-8a2e-327d913864cf','7eded74d-f508-52c4-76da-5f469c3b4b2a','a57b6fdf-beda-0f2f-164f-5f48dd72c10f','Opportunities','2020-08-28 00:00:00',NULL,NULL,0),('c9200846-e918-11ea-8a2e-327d913864cf','7eded74d-f508-52c4-76da-5f469c3b4b2a','12c3340b-0f62-8d28-b800-5f48db0951f9','Opportunities','2020-08-28 00:00:00',NULL,NULL,0),('d60afc77-e919-11ea-8a2e-327d913864cf','7eded74d-f508-52c4-76da-5f469c3b4b2a','3977c53e-104f-b7c1-2ca8-5f48dd5f8c58','Leads','2020-08-28 00:00:00',NULL,NULL,0);
/*!40000 ALTER TABLE `securitygroups_records` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `securitygroups_users`
--

DROP TABLE IF EXISTS `securitygroups_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `securitygroups_users` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `securitygroup_id` varchar(36) DEFAULT NULL,
  `user_id` varchar(36) DEFAULT NULL,
  `primary_group` tinyint(1) DEFAULT NULL,
  `noninheritable` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `securitygroups_users_idxa` (`securitygroup_id`),
  KEY `securitygroups_users_idxb` (`user_id`),
  KEY `securitygroups_users_idxc` (`user_id`,`deleted`,`securitygroup_id`,`id`),
  KEY `securitygroups_users_idxd` (`user_id`,`deleted`,`securitygroup_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `securitygroups_users`
--

LOCK TABLES `securitygroups_users` WRITE;
/*!40000 ALTER TABLE `securitygroups_users` DISABLE KEYS */;
INSERT INTO `securitygroups_users` VALUES ('7a5a0670-8441-b3d4-e455-5f47f198fea8','2020-08-27 17:47:48',0,'7eded74d-f508-52c4-76da-5f469c3b4b2a','8d544213-bcac-b962-fc13-5f4698c75700',NULL,0),('89e60e2a-9b9d-3d52-d329-5f47f10e131e','2020-08-27 17:47:48',0,'7eded74d-f508-52c4-76da-5f469c3b4b2a','1674d634-fb3a-8d8b-34c1-5f4698fed125',NULL,0),('a4ea092b-93b3-79ba-31aa-5f47f1ed2941','2020-08-27 17:47:48',0,'7eded74d-f508-52c4-76da-5f469c3b4b2a','b70243be-ca4c-b256-aff8-5f46980acf9f',NULL,0);
/*!40000 ALTER TABLE `securitygroups_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sp_auto_timesheet_time_tracker`
--

DROP TABLE IF EXISTS `sp_auto_timesheet_time_tracker`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sp_auto_timesheet_time_tracker` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `parent_type` varchar(255) DEFAULT NULL,
  `parent_id` char(36) DEFAULT NULL,
  `sp_timesheet_id_c` char(36) DEFAULT NULL,
  `sp_timesheet_tracker_id_c` char(36) DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `processed` tinyint(1) DEFAULT '0',
  `manual_entry` tinyint(1) DEFAULT '0',
  `hours` decimal(10,2) DEFAULT '0.00',
  `selected_module_id` varchar(255) DEFAULT NULL,
  `user_id_c` char(36) DEFAULT NULL,
  `last_updated_at` datetime DEFAULT NULL,
  `last_send_email_at` datetime DEFAULT NULL,
  `related_project_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sp_auto_timesheet_time_tracker`
--

LOCK TABLES `sp_auto_timesheet_time_tracker` WRITE;
/*!40000 ALTER TABLE `sp_auto_timesheet_time_tracker` DISABLE KEYS */;
INSERT INTO `sp_auto_timesheet_time_tracker` VALUES ('5bcae400-0c26-d06b-4d28-5e90b93cf701','Auto TimeSheet Record','2020-04-10 18:20:59','2020-04-10 18:22:11','dc26b995-5550-b848-6f73-5e90aa679348','dc26b995-5550-b848-6f73-5e90aa679348',NULL,0,'','ProjectTask','dbd3eee3-9951-a692-80f9-5e90b88bab0f','d196b8b5-f91f-1483-270b-5e90b8abf374','4a7f0912-73d6-6c8f-fc5f-5e90b97df24f','2020-04-10 18:20:00','2020-04-10 18:20:00',1,0,0.00,'5371d12a-d61b-9d7a-97a4-5e90b71aecf9','dc26b995-5550-b848-6f73-5e90aa679348',NULL,NULL,'');
/*!40000 ALTER TABLE `sp_auto_timesheet_time_tracker` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sp_auto_timesheet_time_tracker_audit`
--

DROP TABLE IF EXISTS `sp_auto_timesheet_time_tracker_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sp_auto_timesheet_time_tracker_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_sp_auto_timesheet_time_tracker_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sp_auto_timesheet_time_tracker_audit`
--

LOCK TABLES `sp_auto_timesheet_time_tracker_audit` WRITE;
/*!40000 ALTER TABLE `sp_auto_timesheet_time_tracker_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `sp_auto_timesheet_time_tracker_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sp_timecard_tracker`
--

DROP TABLE IF EXISTS `sp_timecard_tracker`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sp_timecard_tracker` (
  `id` char(36) NOT NULL,
  `timecard_id` char(36) DEFAULT NULL,
  `position` char(6) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `track_date0` date DEFAULT NULL,
  `hours0` float(18,2) DEFAULT NULL,
  `track_date1` date DEFAULT NULL,
  `hours1` float(18,2) DEFAULT NULL,
  `track_date2` date DEFAULT NULL,
  `hours2` float(18,2) DEFAULT NULL,
  `track_date3` date DEFAULT NULL,
  `hours3` float(18,2) DEFAULT NULL,
  `track_date4` date DEFAULT NULL,
  `hours4` float(18,2) DEFAULT NULL,
  `track_date5` date DEFAULT NULL,
  `hours5` float(18,2) DEFAULT NULL,
  `track_date6` date DEFAULT NULL,
  `hours6` float(18,2) DEFAULT NULL,
  `project_id_c` char(36) DEFAULT NULL,
  `projecttask_id_c` char(36) DEFAULT NULL,
  `sp_auto_timesheet_tracker_id` char(36) DEFAULT NULL,
  `user_id_c` char(36) DEFAULT NULL,
  `timesheet_id` varchar(36) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sp_timecard_tracker`
--

LOCK TABLES `sp_timecard_tracker` WRITE;
/*!40000 ALTER TABLE `sp_timecard_tracker` DISABLE KEYS */;
/*!40000 ALTER TABLE `sp_timecard_tracker` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sp_timeoff_timecard`
--

DROP TABLE IF EXISTS `sp_timeoff_timecard`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sp_timeoff_timecard` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `track_date_from` date DEFAULT NULL,
  `track_date_to` date DEFAULT NULL,
  `user_id_c` char(36) DEFAULT NULL,
  `hours` float(18,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sp_timeoff_timecard`
--

LOCK TABLES `sp_timeoff_timecard` WRITE;
/*!40000 ALTER TABLE `sp_timeoff_timecard` DISABLE KEYS */;
INSERT INTO `sp_timeoff_timecard` VALUES ('e496045a-6d8c-89b7-bc75-5e90b833aba5','Timecard: 2020-04-06 - 2020-04-12','2020-04-10 18:18:13','2020-04-10 18:18:13','dc26b995-5550-b848-6f73-5e90aa679348','dc26b995-5550-b848-6f73-5e90aa679348',NULL,0,'dc26b995-5550-b848-6f73-5e90aa679348','2020-04-06','2020-04-12','dc26b995-5550-b848-6f73-5e90aa679348',5.00);
/*!40000 ALTER TABLE `sp_timeoff_timecard` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sp_timeoff_timecard_audit`
--

DROP TABLE IF EXISTS `sp_timeoff_timecard_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sp_timeoff_timecard_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_sp_timeoff_timecard_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sp_timeoff_timecard_audit`
--

LOCK TABLES `sp_timeoff_timecard_audit` WRITE;
/*!40000 ALTER TABLE `sp_timeoff_timecard_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `sp_timeoff_timecard_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sp_timesheet`
--

DROP TABLE IF EXISTS `sp_timesheet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sp_timesheet` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `track_date_from` date DEFAULT NULL,
  `track_date_to` date DEFAULT NULL,
  `user_id_c` char(36) DEFAULT NULL,
  `hours` float(18,2) DEFAULT NULL,
  `module` varchar(100) DEFAULT NULL,
  `related_module` varchar(100) DEFAULT NULL,
  `related_module_relationship` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sp_timesheet`
--

LOCK TABLES `sp_timesheet` WRITE;
/*!40000 ALTER TABLE `sp_timesheet` DISABLE KEYS */;
INSERT INTO `sp_timesheet` VALUES ('d196b8b5-f91f-1483-270b-5e90b8abf374','TimeSheet: 2020-04-06 - 2020-04-12','2020-04-10 18:17:21','2020-04-10 18:17:40','dc26b995-5550-b848-6f73-5e90aa679348','dc26b995-5550-b848-6f73-5e90aa679348',NULL,0,'dc26b995-5550-b848-6f73-5e90aa679348','2020-04-06','2020-04-12','dc26b995-5550-b848-6f73-5e90aa679348',6.03,'Project','ProjectTask','projecttask');
/*!40000 ALTER TABLE `sp_timesheet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sp_timesheet_audit`
--

DROP TABLE IF EXISTS `sp_timesheet_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sp_timesheet_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_sp_timesheet_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sp_timesheet_audit`
--

LOCK TABLES `sp_timesheet_audit` WRITE;
/*!40000 ALTER TABLE `sp_timesheet_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `sp_timesheet_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sp_timesheet_tracker`
--

DROP TABLE IF EXISTS `sp_timesheet_tracker`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sp_timesheet_tracker` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `track_date` date DEFAULT NULL,
  `soft_deleted` tinyint(1) DEFAULT '0',
  `hours` float(18,2) DEFAULT NULL,
  `project_id_c` char(36) DEFAULT NULL,
  `projecttask_id_c` char(36) DEFAULT NULL,
  `sp_auto_timesheet_tracker_id` char(36) DEFAULT NULL,
  `user_id_c` char(36) DEFAULT NULL,
  `timesheet_id` varchar(36) DEFAULT NULL,
  `parent_type` varchar(255) DEFAULT NULL,
  `parent_id` char(36) DEFAULT NULL,
  `related_record_url` varchar(255) DEFAULT NULL,
  `related_module` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sp_timesheet_tracker`
--

LOCK TABLES `sp_timesheet_tracker` WRITE;
/*!40000 ALTER TABLE `sp_timesheet_tracker` DISABLE KEYS */;
INSERT INTO `sp_timesheet_tracker` VALUES ('4a7f0912-73d6-6c8f-fc5f-5e90b97df24f','TimeSheet Tracker','2020-04-10 18:20:59','2020-04-10 18:20:59','dc26b995-5550-b848-6f73-5e90aa679348','dc26b995-5550-b848-6f73-5e90aa679348',NULL,0,NULL,'2020-04-10',0,0.03,'5371d12a-d61b-9d7a-97a4-5e90b71aecf9','dbd3eee3-9951-a692-80f9-5e90b88bab0f',NULL,'dc26b995-5550-b848-6f73-5e90aa679348','d196b8b5-f91f-1483-270b-5e90b8abf374','Project','5371d12a-d61b-9d7a-97a4-5e90b71aecf9','<a href=\"index.php?module=ProjectTask&action=DetailView&record=dbd3eee3-9951-a692-80f9-5e90b88bab0f\">Test Tasks</a>',NULL),('5082566c-5d34-5b12-bca7-5e90b8f556d4',NULL,'2020-04-10 18:17:40','2020-04-10 18:17:40','dc26b995-5550-b848-6f73-5e90aa679348','dc26b995-5550-b848-6f73-5e90aa679348','Hello',0,NULL,'2020-04-06',0,2.00,'5371d12a-d61b-9d7a-97a4-5e90b71aecf9','dbd3eee3-9951-a692-80f9-5e90b88bab0f',NULL,'dc26b995-5550-b848-6f73-5e90aa679348','d196b8b5-f91f-1483-270b-5e90b8abf374','Project','5371d12a-d61b-9d7a-97a4-5e90b71aecf9','<a href=\"index.php?module=ProjectTask&action=DetailView&record=dbd3eee3-9951-a692-80f9-5e90b88bab0f\">Test Tasks</a>',NULL),('51978fe1-1bba-14ff-37f4-5e90b8db54c4',NULL,'2020-04-10 18:17:40','2020-04-10 18:17:40','dc26b995-5550-b848-6f73-5e90aa679348','dc26b995-5550-b848-6f73-5e90aa679348','2020-04-07',0,NULL,'2020-04-07',0,4.00,'5371d12a-d61b-9d7a-97a4-5e90b71aecf9','dbd3eee3-9951-a692-80f9-5e90b88bab0f',NULL,'dc26b995-5550-b848-6f73-5e90aa679348','d196b8b5-f91f-1483-270b-5e90b8abf374','Project','5371d12a-d61b-9d7a-97a4-5e90b71aecf9','<a href=\"index.php?module=ProjectTask&action=DetailView&record=dbd3eee3-9951-a692-80f9-5e90b88bab0f\">Test Tasks</a>',NULL),('7ccb2d3a-f838-f4e2-deaf-5e90b8eec802',NULL,'2020-04-10 18:17:35','2020-04-10 18:17:35','dc26b995-5550-b848-6f73-5e90aa679348','dc26b995-5550-b848-6f73-5e90aa679348','Hello',1,NULL,'2020-04-06',1,2.00,'5371d12a-d61b-9d7a-97a4-5e90b71aecf9','dbd3eee3-9951-a692-80f9-5e90b88bab0f',NULL,'dc26b995-5550-b848-6f73-5e90aa679348','d196b8b5-f91f-1483-270b-5e90b8abf374','Project','5371d12a-d61b-9d7a-97a4-5e90b71aecf9','<a href=\"index.php?module=ProjectTask&action=DetailView&record=dbd3eee3-9951-a692-80f9-5e90b88bab0f\">Test Tasks</a>',NULL),('d8c9d276-5018-d8f6-03b7-5e90b8650aef',NULL,'2020-04-10 18:17:21','2020-04-10 18:17:21','dc26b995-5550-b848-6f73-5e90aa679348','dc26b995-5550-b848-6f73-5e90aa679348','2020-04-06',1,NULL,'2020-04-06',1,1.00,'5371d12a-d61b-9d7a-97a4-5e90b71aecf9','dbd3eee3-9951-a692-80f9-5e90b88bab0f',NULL,'dc26b995-5550-b848-6f73-5e90aa679348','d196b8b5-f91f-1483-270b-5e90b8abf374','Project','5371d12a-d61b-9d7a-97a4-5e90b71aecf9','<a href=\"index.php?module=ProjectTask&action=DetailView&record=dbd3eee3-9951-a692-80f9-5e90b88bab0f\">Test Tasks</a>',NULL);
/*!40000 ALTER TABLE `sp_timesheet_tracker` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sp_timesheet_tracker_audit`
--

DROP TABLE IF EXISTS `sp_timesheet_tracker_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sp_timesheet_tracker_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_sp_timesheet_tracker_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sp_timesheet_tracker_audit`
--

LOCK TABLES `sp_timesheet_tracker_audit` WRITE;
/*!40000 ALTER TABLE `sp_timesheet_tracker_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `sp_timesheet_tracker_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `spots`
--

DROP TABLE IF EXISTS `spots`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `spots` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `config` longtext,
  `type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `spots`
--

LOCK TABLES `spots` WRITE;
/*!40000 ALTER TABLE `spots` DISABLE KEYS */;
/*!40000 ALTER TABLE `spots` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sugarfeed`
--

DROP TABLE IF EXISTS `sugarfeed`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sugarfeed` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `related_module` varchar(100) DEFAULT NULL,
  `related_id` char(36) DEFAULT NULL,
  `link_url` varchar(255) DEFAULT NULL,
  `link_type` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sgrfeed_date` (`date_entered`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sugarfeed`
--

LOCK TABLES `sugarfeed` WRITE;
/*!40000 ALTER TABLE `sugarfeed` DISABLE KEYS */;
INSERT INTO `sugarfeed` VALUES ('1152179c-47af-cb22-9499-5f44fab7036e','<b>{this.CREATED_BY}</b> {SugarFeed.CREATED_OPPORTUNITY} [Opportunities:cf6e07ce-3763-a98c-69d1-5f44fa63cfd9:Nutri Test 4] {SugarFeed.WITH} [Accounts:72cb1f4a-5f55-b910-e731-5e90b810ad4d:CRM Experts Online] {SugarFeed.FOR_AMOUNT} $12,000.00','2020-08-25 11:46:11','2020-08-25 11:46:11','1','1',NULL,0,'1','Opportunities','cf6e07ce-3763-a98c-69d1-5f44fa63cfd9',NULL,NULL),('15a40f78-a880-81d8-cbf0-5f48db384cd9','<b>{this.CREATED_BY}</b> {SugarFeed.CREATED_OPPORTUNITY} [Opportunities:12c3340b-0f62-8d28-b800-5f48db0951f9:Test Opp 2] {SugarFeed.WITH} [Accounts:9ae1967b-3fc4-6421-9d77-5f4697488f9e:Pfizer-India] {SugarFeed.FOR_AMOUNT} $90,000.00','2020-08-28 10:25:25','2020-08-28 10:25:25','8d544213-bcac-b962-fc13-5f4698c75700','8d544213-bcac-b962-fc13-5f4698c75700',NULL,0,'8d544213-bcac-b962-fc13-5f4698c75700','Opportunities','12c3340b-0f62-8d28-b800-5f48db0951f9',NULL,NULL),('19f8a712-64fb-f3f2-7ada-5f3fadbea15b','<b>{this.CREATED_BY}</b> {SugarFeed.CREATED_OPPORTUNITY} [Opportunities:170ab7e4-7129-fd99-34c8-5f3fad24bc03:Nutri Test 1] {SugarFeed.WITH} [Accounts:c4910e0c-08a6-3d7d-d073-5f3fad5bff46:Pharma Company] {SugarFeed.FOR_AMOUNT} $12,000.00','2020-08-21 11:19:51','2020-08-21 11:19:51','1','1',NULL,0,'1','Opportunities','170ab7e4-7129-fd99-34c8-5f3fad24bc03',NULL,NULL),('1fe002e8-60f2-e4a0-0ce0-5f43bc1669a1','<b>{this.CREATED_BY}</b> {SugarFeed.CREATED_OPPORTUNITY} [Opportunities:f10d6a43-2a2f-d743-d2ce-5f43bca15ede:Nutri Test 3] {SugarFeed.WITH} [Accounts:c4910e0c-08a6-3d7d-d073-5f3fad5bff46:Pharma Company] {SugarFeed.FOR_AMOUNT} $8,000.00','2020-08-24 13:10:49','2020-08-24 13:10:49','1','1',NULL,0,'1','Opportunities','f10d6a43-2a2f-d743-d2ce-5f43bca15ede',NULL,NULL),('214c3b16-74cc-837a-bd93-5f48dd07d859','<b>{this.CREATED_BY}</b> {SugarFeed.CREATED_OPPORTUNITY} [Opportunities:1ee05743-c112-d3a1-163a-5f48dd532edf:Test Opp 3] {SugarFeed.WITH} [Accounts:4712d4f2-e7f2-593a-8243-5f469751802e:Pfizer-China] {SugarFeed.FOR_AMOUNT} $9,090.00','2020-08-28 10:31:57','2020-08-28 10:31:57','1674d634-fb3a-8d8b-34c1-5f4698fed125','1674d634-fb3a-8d8b-34c1-5f4698fed125',NULL,0,'1674d634-fb3a-8d8b-34c1-5f4698fed125','Opportunities','1ee05743-c112-d3a1-163a-5f48dd532edf',NULL,NULL),('3142b428-5501-3f52-0781-5f43bf79b1d0','<b>{this.CREATED_BY}</b> {SugarFeed.CREATED_LEAD} [Leads:2aab764c-a53d-2a20-e944-5f43bff03c55:Susie Morgan]','2020-08-24 13:24:51','2020-08-24 13:24:51','1','1',NULL,0,'1','Leads','2aab764c-a53d-2a20-e944-5f43bff03c55',NULL,NULL),('3ef0ff4d-1f47-2281-fc39-5f48dd3de859','<b>{this.CREATED_BY}</b> {SugarFeed.CREATED_LEAD} [Leads:3977c53e-104f-b7c1-2ca8-5f48dd5f8c58:Tim China]','2020-08-28 10:32:56','2020-08-28 10:32:56','1674d634-fb3a-8d8b-34c1-5f4698fed125','1674d634-fb3a-8d8b-34c1-5f4698fed125',NULL,0,'1674d634-fb3a-8d8b-34c1-5f4698fed125','Leads','3977c53e-104f-b7c1-2ca8-5f48dd5f8c58',NULL,NULL),('41bced8b-16d3-dfa5-cf3d-5f48dc32d95b','<b>{this.CREATED_BY}</b> {SugarFeed.CREATED_CONTACT} [Contacts:3898ea01-ac05-7e9f-4dce-5f48dcaf2e89:Sandra China]','2020-08-28 10:30:05','2020-08-28 10:30:05','1674d634-fb3a-8d8b-34c1-5f4698fed125','1674d634-fb3a-8d8b-34c1-5f4698fed125',NULL,0,'1674d634-fb3a-8d8b-34c1-5f4698fed125','Contacts','3898ea01-ac05-7e9f-4dce-5f48dcaf2e89',NULL,NULL),('4e726714-d505-2904-9143-5f43be5913b3','<b>{this.CREATED_BY}</b> {SugarFeed.CREATED_OPPORTUNITY} [Opportunities:4c12f4da-751b-5429-80ea-5f43bed3071f:Pharma Test 2] {SugarFeed.WITH} [Accounts:c4910e0c-08a6-3d7d-d073-5f3fad5bff46:Pharma Company] {SugarFeed.FOR_AMOUNT} $10,000.00','2020-08-24 13:21:35','2020-08-24 13:21:35','1','1',NULL,0,'dc26b995-5550-b848-6f73-5e90aa679348','Opportunities','4c12f4da-751b-5429-80ea-5f43bed3071f',NULL,NULL),('565f361a-dfde-8805-21e4-5f44f8a17eff','<b>{this.CREATED_BY}</b> {SugarFeed.CREATED_CONTACT} [Contacts:4da4191b-796e-24f8-313a-5f44f8418e64:Susan Morgan]','2020-08-25 11:41:26','2020-08-25 11:41:26','1','1',NULL,0,'1','Contacts','4da4191b-796e-24f8-313a-5f44f8418e64',NULL,NULL),('58d12e62-cacd-12e6-8c57-5f48da120f69','<b>{this.CREATED_BY}</b> {SugarFeed.CREATED_CONTACT} [Contacts:5200556a-9070-b927-1928-5f48da876d80:Steve India]','2020-08-28 10:22:07','2020-08-28 10:22:07','8d544213-bcac-b962-fc13-5f4698c75700','8d544213-bcac-b962-fc13-5f4698c75700',NULL,0,'8d544213-bcac-b962-fc13-5f4698c75700','Contacts','5200556a-9070-b927-1928-5f48da876d80',NULL,NULL),('6b102ccd-e700-9d15-4f22-5f48da35e428','<b>{this.CREATED_BY}</b> {SugarFeed.CREATED_OPPORTUNITY} [Opportunities:68b8f5a5-cd40-decc-cebb-5f48da51a3f2:Test Opp 1] {SugarFeed.WITH} [Accounts:9ae1967b-3fc4-6421-9d77-5f4697488f9e:Pfizer-India] {SugarFeed.FOR_AMOUNT} $12,200.00','2020-08-28 10:20:11','2020-08-28 10:20:11','8d544213-bcac-b962-fc13-5f4698c75700','8d544213-bcac-b962-fc13-5f4698c75700',NULL,0,'8d544213-bcac-b962-fc13-5f4698c75700','Opportunities','68b8f5a5-cd40-decc-cebb-5f48da51a3f2',NULL,NULL),('98d553c9-3eb9-cd1a-ba61-5f43bc6ca07a','<b>{this.CREATED_BY}</b> {SugarFeed.CREATED_OPPORTUNITY} [Opportunities:9583611d-7521-f3a8-6bcd-5f43bc7dfab0:Nutri Test 2] {SugarFeed.WITH} [Accounts:c4910e0c-08a6-3d7d-d073-5f3fad5bff46:Pharma Company] {SugarFeed.FOR_AMOUNT} $4,000.00','2020-08-24 13:10:19','2020-08-24 13:10:19','1','1',NULL,0,'1','Opportunities','9583611d-7521-f3a8-6bcd-5f43bc7dfab0',NULL,NULL),('a8406fbc-0e80-d39d-52e7-5f48dddc12b0','<b>{this.CREATED_BY}</b> {SugarFeed.CREATED_OPPORTUNITY} [Opportunities:a57b6fdf-beda-0f2f-164f-5f48dd72c10f:Test Opp 4] {SugarFeed.WITH} [Accounts:4712d4f2-e7f2-593a-8243-5f469751802e:Pfizer-China] {SugarFeed.FOR_AMOUNT} $80,000.00','2020-08-28 10:32:19','2020-08-28 10:32:19','1674d634-fb3a-8d8b-34c1-5f4698fed125','1674d634-fb3a-8d8b-34c1-5f4698fed125',NULL,0,'1674d634-fb3a-8d8b-34c1-5f4698fed125','Opportunities','a57b6fdf-beda-0f2f-164f-5f48dd72c10f',NULL,NULL),('af8696d2-a86a-0483-3678-5f3fad44e579','<b>{this.CREATED_BY}</b> {SugarFeed.CREATED_OPPORTUNITY} [Opportunities:ab99004a-eee8-20c6-f636-5f3fad54c0d0:Pharma Test 1] {SugarFeed.WITH} [Accounts:c4910e0c-08a6-3d7d-d073-5f3fad5bff46:Pharma Company] {SugarFeed.FOR_AMOUNT} $60,000.00','2020-08-21 11:19:15','2020-08-21 11:19:15','1','1',NULL,0,'1','Opportunities','ab99004a-eee8-20c6-f636-5f3fad54c0d0',NULL,NULL),('d132fda7-aa9e-a4a5-8c4b-5f43bc8e55dd','<b>{this.CREATED_BY}</b> {SugarFeed.WON_OPPORTUNITY} [Opportunities:9583611d-7521-f3a8-6bcd-5f43bc7dfab0:Nutri Test 2] {SugarFeed.WITH} [Accounts:c4910e0c-08a6-3d7d-d073-5f3fad5bff46:Pharma Company] {SugarFeed.FOR_AMOUNT} $4,000.00','2020-08-24 13:11:11','2020-08-24 13:11:11','1','1',NULL,0,'1','Opportunities','9583611d-7521-f3a8-6bcd-5f43bc7dfab0',NULL,NULL),('ee3895ba-d33a-211a-c6a4-5f48daa53a71','<b>{this.CREATED_BY}</b> {SugarFeed.CREATED_LEAD} [Leads:e826f898-71d8-241f-2c0b-5f48da608dc4:Bill India]','2020-08-28 10:22:41','2020-08-28 10:22:41','8d544213-bcac-b962-fc13-5f4698c75700','8d544213-bcac-b962-fc13-5f4698c75700',NULL,0,'8d544213-bcac-b962-fc13-5f4698c75700','Leads','e826f898-71d8-241f-2c0b-5f48da608dc4',NULL,NULL);
/*!40000 ALTER TABLE `sugarfeed` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `suh_timer_recording`
--

DROP TABLE IF EXISTS `suh_timer_recording`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `suh_timer_recording` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `parent_type` varchar(255) DEFAULT NULL,
  `parent_id` char(36) DEFAULT NULL,
  `auto_time_tracker_id` char(36) DEFAULT NULL,
  `timer_ended` tinyint(1) DEFAULT '0',
  `timer_pause` tinyint(1) DEFAULT '0',
  `time_spend` int(11) DEFAULT NULL,
  `sec_spend` int(5) DEFAULT NULL,
  `minutes_updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `suh_timer_recording`
--

LOCK TABLES `suh_timer_recording` WRITE;
/*!40000 ALTER TABLE `suh_timer_recording` DISABLE KEYS */;
INSERT INTO `suh_timer_recording` VALUES ('63a85abb-a937-e84a-7337-5e90b937eb9d','ProjectTask2020-04-10 18:20:59','2020-04-10 18:20:59','2020-04-10 06:22:09',NULL,'dc26b995-5550-b848-6f73-5e90aa679348',NULL,0,'dc26b995-5550-b848-6f73-5e90aa679348','2020-04-10 18:20:59','2020-04-10 18:22:11','ProjectTask','dbd3eee3-9951-a692-80f9-5e90b88bab0f','5bcae400-0c26-d06b-4d28-5e90b93cf701',1,0,2,4,'2020-04-10 06:22:05');
/*!40000 ALTER TABLE `suh_timer_recording` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `suh_timer_recording_audit`
--

DROP TABLE IF EXISTS `suh_timer_recording_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `suh_timer_recording_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_suh_timer_recording_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `suh_timer_recording_audit`
--

LOCK TABLES `suh_timer_recording_audit` WRITE;
/*!40000 ALTER TABLE `suh_timer_recording_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `suh_timer_recording_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `surveyquestionoptions`
--

DROP TABLE IF EXISTS `surveyquestionoptions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `surveyquestionoptions` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `sort_order` int(255) DEFAULT NULL,
  `survey_question_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `surveyquestionoptions`
--

LOCK TABLES `surveyquestionoptions` WRITE;
/*!40000 ALTER TABLE `surveyquestionoptions` DISABLE KEYS */;
/*!40000 ALTER TABLE `surveyquestionoptions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `surveyquestionoptions_audit`
--

DROP TABLE IF EXISTS `surveyquestionoptions_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `surveyquestionoptions_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_surveyquestionoptions_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `surveyquestionoptions_audit`
--

LOCK TABLES `surveyquestionoptions_audit` WRITE;
/*!40000 ALTER TABLE `surveyquestionoptions_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `surveyquestionoptions_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `surveyquestionoptions_surveyquestionresponses`
--

DROP TABLE IF EXISTS `surveyquestionoptions_surveyquestionresponses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `surveyquestionoptions_surveyquestionresponses` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `surveyq72c7options_ida` varchar(36) DEFAULT NULL,
  `surveyq10d4sponses_idb` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `surveyquestionoptions_surveyquestionresponses_alt` (`surveyq72c7options_ida`,`surveyq10d4sponses_idb`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `surveyquestionoptions_surveyquestionresponses`
--

LOCK TABLES `surveyquestionoptions_surveyquestionresponses` WRITE;
/*!40000 ALTER TABLE `surveyquestionoptions_surveyquestionresponses` DISABLE KEYS */;
/*!40000 ALTER TABLE `surveyquestionoptions_surveyquestionresponses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `surveyquestionresponses`
--

DROP TABLE IF EXISTS `surveyquestionresponses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `surveyquestionresponses` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `answer` text,
  `answer_bool` tinyint(1) DEFAULT NULL,
  `answer_datetime` datetime DEFAULT NULL,
  `surveyquestion_id` char(36) DEFAULT NULL,
  `surveyresponse_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `surveyquestionresponses`
--

LOCK TABLES `surveyquestionresponses` WRITE;
/*!40000 ALTER TABLE `surveyquestionresponses` DISABLE KEYS */;
/*!40000 ALTER TABLE `surveyquestionresponses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `surveyquestionresponses_audit`
--

DROP TABLE IF EXISTS `surveyquestionresponses_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `surveyquestionresponses_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_surveyquestionresponses_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `surveyquestionresponses_audit`
--

LOCK TABLES `surveyquestionresponses_audit` WRITE;
/*!40000 ALTER TABLE `surveyquestionresponses_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `surveyquestionresponses_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `surveyquestions`
--

DROP TABLE IF EXISTS `surveyquestions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `surveyquestions` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `sort_order` int(255) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `happiness_question` tinyint(1) DEFAULT NULL,
  `survey_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `surveyquestions`
--

LOCK TABLES `surveyquestions` WRITE;
/*!40000 ALTER TABLE `surveyquestions` DISABLE KEYS */;
/*!40000 ALTER TABLE `surveyquestions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `surveyquestions_audit`
--

DROP TABLE IF EXISTS `surveyquestions_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `surveyquestions_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_surveyquestions_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `surveyquestions_audit`
--

LOCK TABLES `surveyquestions_audit` WRITE;
/*!40000 ALTER TABLE `surveyquestions_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `surveyquestions_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `surveyresponses`
--

DROP TABLE IF EXISTS `surveyresponses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `surveyresponses` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `happiness` int(11) DEFAULT NULL,
  `email_response_sent` tinyint(1) DEFAULT NULL,
  `account_id` char(36) DEFAULT NULL,
  `campaign_id` char(36) DEFAULT NULL,
  `contact_id` char(36) DEFAULT NULL,
  `survey_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `surveyresponses`
--

LOCK TABLES `surveyresponses` WRITE;
/*!40000 ALTER TABLE `surveyresponses` DISABLE KEYS */;
/*!40000 ALTER TABLE `surveyresponses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `surveyresponses_audit`
--

DROP TABLE IF EXISTS `surveyresponses_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `surveyresponses_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_surveyresponses_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `surveyresponses_audit`
--

LOCK TABLES `surveyresponses_audit` WRITE;
/*!40000 ALTER TABLE `surveyresponses_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `surveyresponses_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `surveys`
--

DROP TABLE IF EXISTS `surveys`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `surveys` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `status` varchar(100) DEFAULT 'Draft',
  `submit_text` varchar(255) DEFAULT 'Submit',
  `satisfied_text` varchar(255) DEFAULT 'Satisfied',
  `neither_text` varchar(255) DEFAULT 'Neither Satisfied nor Dissatisfied',
  `dissatisfied_text` varchar(255) DEFAULT 'Dissatisfied',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `surveys`
--

LOCK TABLES `surveys` WRITE;
/*!40000 ALTER TABLE `surveys` DISABLE KEYS */;
/*!40000 ALTER TABLE `surveys` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `surveys_audit`
--

DROP TABLE IF EXISTS `surveys_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `surveys_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_surveys_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `surveys_audit`
--

LOCK TABLES `surveys_audit` WRITE;
/*!40000 ALTER TABLE `surveys_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `surveys_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tasks`
--

DROP TABLE IF EXISTS `tasks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tasks` (
  `id` char(36) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `status` varchar(100) DEFAULT 'Not Started',
  `date_due_flag` tinyint(1) DEFAULT '0',
  `date_due` datetime DEFAULT NULL,
  `date_start_flag` tinyint(1) DEFAULT '0',
  `date_start` datetime DEFAULT NULL,
  `parent_type` varchar(255) DEFAULT NULL,
  `parent_id` char(36) DEFAULT NULL,
  `contact_id` char(36) DEFAULT NULL,
  `priority` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_tsk_name` (`name`),
  KEY `idx_task_con_del` (`contact_id`,`deleted`),
  KEY `idx_task_par_del` (`parent_id`,`parent_type`,`deleted`),
  KEY `idx_task_assigned` (`assigned_user_id`),
  KEY `idx_task_status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tasks`
--

LOCK TABLES `tasks` WRITE;
/*!40000 ALTER TABLE `tasks` DISABLE KEYS */;
INSERT INTO `tasks` VALUES ('17a201b9-7f91-87b1-31b8-5f452d31bdd0','Test','2020-08-25 15:26:18','2020-08-25 15:27:28','1','1','Test',0,'1','Not Started',0,'2020-08-25 11:00:00',0,'2020-08-25 11:00:00','Opportunities','170ab7e4-7129-fd99-34c8-5f3fad24bc03','','High');
/*!40000 ALTER TABLE `tasks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `templatesectionline`
--

DROP TABLE IF EXISTS `templatesectionline`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `templatesectionline` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `thumbnail` varchar(255) DEFAULT NULL,
  `grp` varchar(255) DEFAULT NULL,
  `ord` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `templatesectionline`
--

LOCK TABLES `templatesectionline` WRITE;
/*!40000 ALTER TABLE `templatesectionline` DISABLE KEYS */;
INSERT INTO `templatesectionline` VALUES ('6c2941fa-0f55-2e47-ba00-5f43ed51825b','Headline','2020-08-24 16:39:52','2020-08-24 16:39:52','1','1','<h1>Add your headline here..</h1>',0,'include/javascript/mozaik/tpls/default/thumbs/headline.png',NULL,1),('6f0d111a-91d3-5a9c-0b8c-5f43ed4cfebd','Content','2020-08-24 16:39:52','2020-08-24 16:39:52','1','1','<h2>Title</h2><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam tempus odio ante, in feugiat ex pretium eu. In pharetra tincidunt urna et malesuada. Etiam aliquet auctor justo eu placerat. In nec sollicitudin enim. Nulla facilisi. In viverra velit turpis, et lobortis nunc eleifend id. Curabitur semper tincidunt vulputate. Nullam fermentum pellentesque ullamcorper.</p>',0,'include/javascript/mozaik/tpls/default/thumbs/content1.png',NULL,2),('746d1323-cccc-41c5-8184-5f43edb53ffa','Content with two columns','2020-08-24 16:39:52','2020-08-24 16:39:52','1','1','<table style=\"width:100%;\"><tbody><tr><td><h2>Title</h2></td><td><h2>Title</h2></td></tr><tr><td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam tempus odio ante, in feugiat ex pretium eu. In pharetra tincidunt urna et malesuada. Etiam aliquet auctor justo eu placerat. In nec sollicitudin enim. Nulla facilisi. In viverra velit turpis, et lobortis nunc eleifend id. Curabitur semper tincidunt vulputate. Nullam fermentum pellentesque ullamcorper.</td><td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam tempus odio ante, in feugiat ex pretium eu. In pharetra tincidunt urna et malesuada. Etiam aliquet auctor justo eu placerat. In nec sollicitudin enim. Nulla facilisi. In viverra velit turpis, et lobortis nunc eleifend id. Curabitur semper tincidunt vulputate. Nullam fermentum pellentesque ullamcorper.</td></tr></tbody></table>',0,'include/javascript/mozaik/tpls/default/thumbs/content2.png',NULL,3),('773be6b3-6e0c-aae2-0574-5f43edbd0ffc','Content with three columns','2020-08-24 16:39:52','2020-08-24 16:39:52','1','1','<table style=\"width:100%;\"><tbody><tr><td><h2>Title</h2></td><td><h2>Title</h2></td><td><h2>Title</h2></td></tr><tr><td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam tempus odio ante, in feugiat ex pretium eu. In pharetra tincidunt urna et malesuada. Etiam aliquet auctor justo eu placerat. In nec sollicitudin enim. Nulla facilisi. In viverra velit turpis, et lobortis nunc eleifend id. Curabitur semper tincidunt vulputate. Nullam fermentum pellentesque ullamcorper.</td><td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam tempus odio ante, in feugiat ex pretium eu. In pharetra tincidunt urna et malesuada. Etiam aliquet auctor justo eu placerat. In nec sollicitudin enim. Nulla facilisi. In viverra velit turpis, et lobortis nunc eleifend id. Curabitur semper tincidunt vulputate. Nullam fermentum pellentesque ullamcorper.</td><td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam tempus odio ante, in feugiat ex pretium eu. In pharetra tincidunt urna et malesuada. Etiam aliquet auctor justo eu placerat. In nec sollicitudin enim. Nulla facilisi. In viverra velit turpis, et lobortis nunc eleifend id. Curabitur semper tincidunt vulputate. Nullam fermentum pellentesque ullamcorper.</td></tr></tbody></table>',0,'include/javascript/mozaik/tpls/default/thumbs/content3.png',NULL,4),('7a7efe5c-7331-933c-aab4-5f43ed15c82d','Content with left image','2020-08-24 16:39:52','2020-08-24 16:39:52','1','1','<table style=\"width:100%;\"><tbody><tr><td><img src=\"http://192.241.195.232/include/javascript/mozaik/tpls/default/images/sample.jpg\" width=\"130\" alt=\"sample.jpg\" /></td><td><h2>Title</h2>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam tempus odio ante, in feugiat ex pretium eu. In pharetra tincidunt urna et malesuada. Etiam aliquet auctor justo eu placerat. In nec sollicitudin enim. Nulla facilisi. In viverra velit turpis, et lobortis nunc eleifend id. Curabitur semper tincidunt vulputate. Nullam fermentum pellentesque ullamcorper.</td></tr></tbody></table>',0,'include/javascript/mozaik/tpls/default/thumbs/image1left.png',NULL,5),('7d4acd19-9227-1747-2ecd-5f43ed082200','Content with right image','2020-08-24 16:39:52','2020-08-24 16:39:52','1','1','<table style=\"width:100%;\"><tbody><tr><td><h2>Title</h2>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam tempus odio ante, in feugiat ex pretium eu. In pharetra tincidunt urna et malesuada. Etiam aliquet auctor justo eu placerat. In nec sollicitudin enim. Nulla facilisi. In viverra velit turpis, et lobortis nunc eleifend id. Curabitur semper tincidunt vulputate. Nullam fermentum pellentesque ullamcorper.</td><td><img src=\"http://192.241.195.232/include/javascript/mozaik/tpls/default/images/sample.jpg\" width=\"130\" alt=\"sample.jpg\" /></td></tr></tbody></table>',0,'include/javascript/mozaik/tpls/default/thumbs/image1right.png',NULL,6),('800a9101-3e06-c77e-5f68-5f43ed4208c6','Content with two image','2020-08-24 16:39:52','2020-08-24 16:39:52','1','1','<table style=\"width:100%;\"><tbody><tr><td><img src=\"http://192.241.195.232/include/javascript/mozaik/tpls/default/images/sample.jpg\" width=\"130\" alt=\"sample.jpg\" /></td><td><h2>Title</h2>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam tempus odio ante, in feugiat ex pretium eu. In pharetra tincidunt urna et malesuada. Etiam aliquet auctor justo eu placerat. In nec sollicitudin enim. Nulla facilisi. In viverra velit turpis, et lobortis nunc eleifend id. Curabitur semper tincidunt vulputate. Nullam fermentum pellentesque ullamcorper.</td><td><img src=\"http://192.241.195.232/include/javascript/mozaik/tpls/default/images/sample.jpg\" width=\"130\" alt=\"sample.jpg\" /></td><td><h2>Title</h2>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam tempus odio ante, in feugiat ex pretium eu. In pharetra tincidunt urna et malesuada. Etiam aliquet auctor justo eu placerat. In nec sollicitudin enim. Nulla facilisi. In viverra velit turpis, et lobortis nunc eleifend id. Curabitur semper tincidunt vulputate. Nullam fermentum pellentesque ullamcorper.</td></tr></tbody></table>',0,'include/javascript/mozaik/tpls/default/thumbs/image2.png',NULL,7),('82dd243d-b95e-dedf-1f90-5f43edd9c7fc','Content with three image','2020-08-24 16:39:52','2020-08-24 16:39:52','1','1','<table style=\"width:100%;\"><tbody><tr><td><img src=\"http://192.241.195.232/include/javascript/mozaik/tpls/default/images/sample.jpg\" alt=\"sample.jpg\" /></td><td><img src=\"http://192.241.195.232/include/javascript/mozaik/tpls/default/images/sample.jpg\" alt=\"sample.jpg\" /></td><td><img src=\"http://192.241.195.232/include/javascript/mozaik/tpls/default/images/sample.jpg\" alt=\"sample.jpg\" /></td></tr><tr><td><h2>Title</h2>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam tempus odio ante, in feugiat ex pretium eu. In pharetra tincidunt urna et malesuada. Etiam aliquet auctor justo eu placerat. In nec sollicitudin enim. Nulla facilisi. In viverra velit turpis, et lobortis nunc eleifend id. Curabitur semper tincidunt vulputate. Nullam fermentum pellentesque ullamcorper.</td><td><h2>Title</h2>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam tempus odio ante, in feugiat ex pretium eu. In pharetra tincidunt urna et malesuada. Etiam aliquet auctor justo eu placerat. In nec sollicitudin enim. Nulla facilisi. In viverra velit turpis, et lobortis nunc eleifend id. Curabitur semper tincidunt vulputate. Nullam fermentum pellentesque ullamcorper.</td><td><h2>Title</h2>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam tempus odio ante, in feugiat ex pretium eu. In pharetra tincidunt urna et malesuada. Etiam aliquet auctor justo eu placerat. In nec sollicitudin enim. Nulla facilisi. In viverra velit turpis, et lobortis nunc eleifend id. Curabitur semper tincidunt vulputate. Nullam fermentum pellentesque ullamcorper.</td></tr></tbody></table>',0,'include/javascript/mozaik/tpls/default/thumbs/image3.png',NULL,8),('85610e89-29b2-23f5-3f0e-5f43ed2d3496','Footer','2020-08-24 16:39:52','2020-08-24 16:39:52','1','1','<p class=\"footer\">Take your footer contents and information here..</p>',0,'include/javascript/mozaik/tpls/default/thumbs/footer.png',NULL,9);
/*!40000 ALTER TABLE `templatesectionline` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tracker`
--

DROP TABLE IF EXISTS `tracker`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tracker` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `monitor_id` char(36) NOT NULL,
  `user_id` varchar(36) DEFAULT NULL,
  `module_name` varchar(255) DEFAULT NULL,
  `item_id` varchar(36) DEFAULT NULL,
  `item_summary` varchar(255) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `action` varchar(255) DEFAULT NULL,
  `session_id` varchar(36) DEFAULT NULL,
  `visible` tinyint(1) DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_tracker_iid` (`item_id`),
  KEY `idx_tracker_userid_vis_id` (`user_id`,`visible`,`id`),
  KEY `idx_tracker_userid_itemid_vis` (`user_id`,`item_id`,`visible`),
  KEY `idx_tracker_monitor_id` (`monitor_id`),
  KEY `idx_tracker_date_modified` (`date_modified`)
) ENGINE=InnoDB AUTO_INCREMENT=283 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tracker`
--

LOCK TABLES `tracker` WRITE;
/*!40000 ALTER TABLE `tracker` DISABLE KEYS */;
INSERT INTO `tracker` VALUES (19,'154cc3a2-bb8a-113b-c11d-5e90b82f249d','dc26b995-5550-b848-6f73-5e90aa679348','sp_Timeoff_TimeCard','e496045a-6d8c-89b7-bc75-5e90b833aba5','Timecard: 2020-04-06 - 2020-04-12','2020-04-10 18:18:14','detailview','5tkt2vcjbdqv9nbm465mmommht',1,0),(20,'9e95fb0b-f083-d7a4-5bcc-5e90b80e2bf8','dc26b995-5550-b848-6f73-5e90aa679348','Accounts','72cb1f4a-5f55-b910-e731-5e90b810ad4d','CRM Experts Online','2020-04-10 18:19:13','detailview','5tkt2vcjbdqv9nbm465mmommht',0,0),(21,'1d842651-415f-cb05-6605-5e90b8e6ac62','dc26b995-5550-b848-6f73-5e90aa679348','Project','5371d12a-d61b-9d7a-97a4-5e90b71aecf9','Timesheet Ninja Updates','2020-04-10 18:19:40','detailview','5tkt2vcjbdqv9nbm465mmommht',1,0),(24,'a475bed1-3cea-8ac5-6d45-5e90b9969b8c','dc26b995-5550-b848-6f73-5e90aa679348','ProjectTask','dbd3eee3-9951-a692-80f9-5e90b88bab0f','Test Tasks','2020-04-10 18:22:11','detailview','5tkt2vcjbdqv9nbm465mmommht',1,0),(25,'7484e2f6-cba0-af78-78e3-5e90b9071a78','dc26b995-5550-b848-6f73-5e90aa679348','Users','dc26b995-5550-b848-6f73-5e90aa679348','Timesheet Test User','2020-04-10 18:22:19','editview','5tkt2vcjbdqv9nbm465mmommht',1,0),(30,'8d89ff31-c0fa-dc17-2047-5e90ba7afb27','dc26b995-5550-b848-6f73-5e90aa679348','sp_Timesheet','d196b8b5-f91f-1483-270b-5e90b8abf374','TimeSheet: 2020-04-06 - 2020-04-12','2020-04-10 18:28:28','detailview','43pijmv10fb2b6htbtav02uf84',1,0),(41,'2f5ce5d4-7143-25d7-8297-5f3bec26252a','1','Accounts','72cb1f4a-5f55-b910-e731-5e90b810ad4d','CRM Experts Online','2020-08-18 14:59:20','detailview','7br5ja97t3fm4c1t9j2vhvv97j',0,0),(53,'41f08b8a-c10b-7c6e-efcf-5f43b1c1cb8c','1','Users','dc26b995-5550-b848-6f73-5e90aa679348','Timesheet Test User','2020-08-24 12:24:55','detailview','lraqr6lq8ttbqrbcpp2326b9ut',1,0),(106,'7ee23941-90ff-cab6-f615-5f43ec96ba7c','1','Opportunities','4c12f4da-751b-5429-80ea-5f43bed3071f','Pharma Test 2','2020-08-24 16:36:23','detailview','tn052i19m47vcq2troav3bqd0k',1,0),(107,'48b2808f-4ba2-153a-8a00-5f43ed44332a','1','TemplateSectionLine','85610e89-29b2-23f5-3f0e-5f43ed2d3496','Footer','2020-08-24 16:39:52','save','2aa77p6k29kidhkodijkk6toe2',1,0),(117,'41ce508c-06d6-e15c-7efb-5f44f9a319a3','1','Leads','2aab764c-a53d-2a20-e944-5f43bff03c55','Susie Morgan','2020-08-25 11:42:34','detailview','tkentcehj3dnr647paljg7rf2i',1,0),(121,'cb168297-1934-209f-4b77-5f44faef093d','1','Opportunities','cf6e07ce-3763-a98c-69d1-5f44fa63cfd9','Nutri Test 4','2020-08-25 11:48:26','detailview','tkentcehj3dnr647paljg7rf2i',1,0),(129,'7be670fa-9d07-b090-4480-5f4530cf3bb3','1','Contacts','4da4191b-796e-24f8-313a-5f44f8418e64','Ms. Susan Morgan','2020-08-25 15:38:29','detailview','62cqtca73tfktssc0fck3cidc7',1,0),(130,'35c0c6a7-c66d-6481-d581-5f45560fce6d','1','Opportunities','ab99004a-eee8-20c6-f636-5f3fad54c0d0','Pharma Test 1','2020-08-25 18:20:21','detailview','a2lq8376fqknut1dbs3mjhgm63',1,0),(132,'1582463d-7d3f-fd60-30a8-5f45586a1db1','1','Tasks','17a201b9-7f91-87b1-31b8-5f452d31bdd0','Test','2020-08-25 18:29:39','editview','a2lq8376fqknut1dbs3mjhgm63',1,0),(139,'b6a31ffd-6e52-5d80-c3ef-5f46474001ec','1','Accounts','c4910e0c-08a6-3d7d-d073-5f3fad5bff46','Pharma Company','2020-08-26 11:31:02','detailview','rfooj59f9a0710dqjql9lgaqfm',0,0),(187,'840fb139-a9c7-19d6-580d-5f47f0986d4a','1','ACLRoles','824bc684-1b3b-2184-338e-5f47f0ca9e93','Test','2020-08-27 17:44:23','save','psiothel39dftdo7a2p15v7kba',0,0),(189,'bcd1d7f5-3fc8-12ca-697e-5f47f1ba7a9c','1','ACLRoles','824bc684-1b3b-2184-338e-5f47f0ca9e93','Test','2020-08-27 17:44:45','detailview','psiothel39dftdo7a2p15v7kba',0,0),(198,'c281274a-30f6-86be-a551-5f47f19672ab','1','SecurityGroups','7eded74d-f508-52c4-76da-5f469c3b4b2a','Manager Global','2020-08-27 17:47:27','detailview','psiothel39dftdo7a2p15v7kba',1,0),(204,'1aa55bcf-0f13-3c40-008f-5f48caeab91d','1','ACLRoles','3304ef15-3d54-d514-0f52-5f469cfc6b17','Local','2020-08-28 09:12:07','detailview','s2slk96bt20odg9hi7a1gnvh0q',1,0),(221,'f32f8b03-3aa6-65ac-c2af-5f48d40df5b7','1','Users','1','Administrator','2020-08-28 09:54:08','editview','na0i3smn9cg3iphij4adjv0k6v',1,0),(222,'f2784134-f4e7-2d92-d718-5f48d49decf6','1','ACLRoles','69d63f4e-88ef-58c9-0a28-5f469b0dc456','Manager','2020-08-28 09:54:26','detailview','na0i3smn9cg3iphij4adjv0k6v',1,0),(223,'f3000fbf-1169-9568-a320-5f48d716283b','b70243be-ca4c-b256-aff8-5f46980acf9f','Users','b70243be-ca4c-b256-aff8-5f46980acf9f','Chris K','2020-08-28 10:05:59','editview','5682q0aut3r8bmijfercfh0g4o',1,0),(229,'5ac98329-d9e0-12ed-6783-5f48da8f298f','8d544213-bcac-b962-fc13-5f4698c75700','Contacts','684e561b-48ba-9aab-2d9c-5f48da108fc3','Steve India','2020-08-28 10:22:07','detailview','opak36m1htudlfm09816i84t3f',1,0),(233,'c1d8c769-ea9f-a4e9-4004-5f48db606c5c','8d544213-bcac-b962-fc13-5f4698c75700','Contacts','5200556a-9070-b927-1928-5f48da876d80','Steve India','2020-08-28 10:23:31','detailview','opak36m1htudlfm09816i84t3f',1,0),(237,'5bb5032c-fcc5-d211-4453-5f48db9a88cf','8d544213-bcac-b962-fc13-5f4698c75700','Accounts','9ae1967b-3fc4-6421-9d77-5f4697488f9e','Pfizer-India','2020-08-28 10:25:25','detailview','opak36m1htudlfm09816i84t3f',1,0),(239,'43ea9243-2448-4457-6e0f-5f48dc901bf5','1674d634-fb3a-8d8b-34c1-5f4698fed125','Contacts','4dd7ea2f-d859-1dbc-beb9-5f48dcc41559','Sandra China','2020-08-28 10:30:05','detailview','js0p2f7pgkno8osjmjq3v5krsf',1,0),(242,'f0a50e44-4536-2692-96b4-5f48dcd18396','1674d634-fb3a-8d8b-34c1-5f4698fed125','Contacts','3898ea01-ac05-7e9f-4dce-5f48dcaf2e89','Sandra China','2020-08-28 10:30:41','detailview','js0p2f7pgkno8osjmjq3v5krsf',1,0),(246,'7d803a48-5e75-4e68-80e7-5f48dd095057','1674d634-fb3a-8d8b-34c1-5f4698fed125','Accounts','4712d4f2-e7f2-593a-8243-5f469751802e','Pfizer-China','2020-08-28 10:32:56','detailview','js0p2f7pgkno8osjmjq3v5krsf',1,0),(249,'efac19d3-f3d6-e9a4-c8df-5f48e12b9c93','1','Opportunities','68b8f5a5-cd40-decc-cebb-5f48da51a3f2','Test Opp 1','2020-08-28 10:51:01','detailview','9e4s43nn7pjmi3kjsgmvvdhbjd',1,0),(252,'1a24b378-5b24-49e1-191d-5f48ed655848','b70243be-ca4c-b256-aff8-5f46980acf9f','Accounts','9ae1967b-3fc4-6421-9d77-5f4697488f9e','Pfizer-India','2020-08-28 11:40:03','detailview','2gebjpd67c3qvk9fqn93qflhtk',1,0),(253,'236b0c53-c726-70cf-dbe1-5f493194fbfb','1','Opportunities','f10d6a43-2a2f-d743-d2ce-5f43bca15ede','Nutri Test 3','2020-08-28 16:30:32','detailview','qireso7l4tpb8rjfq0dts0baks',1,0),(263,'3c1c0bcf-171e-f777-a7ed-5f493c19f7b6','1','Users','1674d634-fb3a-8d8b-34c1-5f4698fed125','Rick M','2020-08-28 17:18:15','detailview','mtdg2q332rrt54kg6bq9ovn104',1,0),(264,'71a6c93a-4b5f-485e-4599-5f493c6209bd','1','Users','b70243be-ca4c-b256-aff8-5f46980acf9f','Chris K','2020-08-28 17:20:13','detailview','mtdg2q332rrt54kg6bq9ovn104',1,0),(265,'26b9765e-3506-9477-a371-5f4e33bc861d','1','Opportunities','a57b6fdf-beda-0f2f-164f-5f48dd72c10f','Test Opp 4','2020-09-01 11:41:59','detailview','fo8r8i2hb9t6610m6d6dn0jktt',1,0),(269,'30fef086-f232-5b09-0316-5f4e4d15000c','1','Opportunities','170ab7e4-7129-fd99-34c8-5f3fad24bc03','Nutri Test 1','2020-09-01 13:34:28','detailview','ni1t99ce050l70sat3h983c5qv',1,0),(270,'dfbc320d-3afd-2a7a-a8c6-5f4e4d372e3b','1','Opportunities','9583611d-7521-f3a8-6bcd-5f43bc7dfab0','Nutri Test 2','2020-09-01 13:34:39','detailview','ni1t99ce050l70sat3h983c5qv',1,0),(272,'7acb9b97-101d-8d93-e4e6-5f4e4e1786f8','1','AOS_Quotes','9a1eb78a-984b-2d82-b4f9-5f4e4d1aa6e6','Quote Test','2020-09-01 13:36:09','editview','ni1t99ce050l70sat3h983c5qv',1,0),(274,'3bda43b2-dea5-3a4a-5d75-5f4e4e9e8b94','b70243be-ca4c-b256-aff8-5f46980acf9f','Accounts','2978e526-e27b-1ce0-3822-5f46974d0157','Pfizer Global','2020-09-01 13:38:08','detailview','4omvv425moh4sv9frhhqn2rama',1,0),(275,'eda766fd-f363-882e-b7a2-5f4e7bb66ed5','1','Accounts','2978e526-e27b-1ce0-3822-5f46974d0157','Pfizer Global','2020-09-01 16:48:33','detailview','5g9lme1fnchbfb51lau7hbut7f',1,0),(278,'18e1ae65-7e40-6f9e-c268-5f4e7dd22a33','1','Users','8d544213-bcac-b962-fc13-5f4698c75700','Susan Chung','2020-09-01 16:58:20','editview','5g9lme1fnchbfb51lau7hbut7f',1,0),(280,'ede950b8-f600-e824-f107-5f4e7d27ce33','1','Users','8d544213-bcac-b962-fc13-5f4698c75700','Susan Chung','2020-09-01 16:58:41','editview','s6f2vhnc4ot25eu8r9na7r81jr',1,0),(281,'c4de0623-70bf-cb7c-5ef1-5f4e7ec8a7e7','1','Accounts','4712d4f2-e7f2-593a-8243-5f469751802e','Pfizer-China','2020-09-01 17:02:07','detailview','5g9lme1fnchbfb51lau7hbut7f',1,0),(282,'a49d0eb0-98df-1dec-d504-5f4e7e6df65f','1','Accounts','9ae1967b-3fc4-6421-9d77-5f4697488f9e','Pfizer-India','2020-09-01 17:02:10','detailview','5g9lme1fnchbfb51lau7hbut7f',1,0);
/*!40000 ALTER TABLE `tracker` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `upgrade_history`
--

DROP TABLE IF EXISTS `upgrade_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `upgrade_history` (
  `id` char(36) NOT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `md5sum` varchar(32) DEFAULT NULL,
  `type` varchar(30) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `version` varchar(64) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `id_name` varchar(255) DEFAULT NULL,
  `manifest` longtext,
  `date_entered` datetime DEFAULT NULL,
  `enabled` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `upgrade_history_md5_uk` (`md5sum`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `upgrade_history`
--

LOCK TABLES `upgrade_history` WRITE;
/*!40000 ALTER TABLE `upgrade_history` DISABLE KEYS */;
INSERT INTO `upgrade_history` VALUES ('7fb99d80-0e50-f1d9-4e1a-5f3f6ff638fd','upload/upgrades/module/10.18.18M-Kanban-View-Ninja-for-SuiteCRM-MonthlyLics-Recent.zip','4e30af7ef9e19479f988617b83dea398','module','installed','2.2','Kanban View Ninja for SuiteCRM','Kanban View v2 for all modules','','YTozOntzOjg6Im1hbmlmZXN0IjthOjEzOntpOjA7YToxOntzOjI1OiJhY2NlcHRhYmxlX3N1Z2FyX3ZlcnNpb25zIjthOjA6e319aToxO2E6MTp7czoyNDoiYWNjZXB0YWJsZV9zdWdhcl9mbGF2b3JzIjthOjM6e2k6MDtzOjI6IkNFIjtpOjE7czozOiJQUk8iO2k6MjtzOjM6IkVOVCI7fX1zOjY6InJlYWRtZSI7czoxNDoiU2VlIFJlYWRNZS50eHQiO3M6Mzoia2V5IjtzOjEyOiJDUk1FeHBlcnRzTlkiO3M6NjoiYXV0aG9yIjtzOjE2OiJDUk1FeHBlcnRzTlkuQ29tIjtzOjExOiJkZXNjcmlwdGlvbiI7czozMDoiS2FuYmFuIFZpZXcgdjIgZm9yIGFsbCBtb2R1bGVzIjtzOjQ6Imljb24iO3M6MDoiIjtzOjE2OiJpc191bmluc3RhbGxhYmxlIjtiOjE7czo0OiJuYW1lIjtzOjMwOiJLYW5iYW4gVmlldyBOaW5qYSBmb3IgU3VpdGVDUk0iO3M6MTQ6InB1Ymxpc2hlZF9kYXRlIjtzOjE5OiIyMDE4LTAzLTMwIDAwOjAwOjAwIjtzOjQ6InR5cGUiO3M6NjoibW9kdWxlIjtzOjc6InZlcnNpb24iO3M6MzoiMi4yIjtzOjEzOiJyZW1vdmVfdGFibGVzIjtzOjY6InByb21wdCI7fXM6MTE6Imluc3RhbGxkZWZzIjthOjM6e3M6NDoiY29weSI7YTozOntpOjA7YToyOntzOjQ6ImZyb20iO3M6MTc6IjxiYXNlcGF0aD4vY3VzdG9tIjtzOjI6InRvIjtzOjY6ImN1c3RvbSI7fWk6MTthOjI6e3M6NDoiZnJvbSI7czoxODoiPGJhc2VwYXRoPi9pbmNsdWRlIjtzOjI6InRvIjtzOjc6ImluY2x1ZGUiO31pOjI7YToyOntzOjQ6ImZyb20iO3M6MTg6IjxiYXNlcGF0aD4vbGljZW5zZSI7czoyOiJ0byI7czoxNDoibW9kdWxlcy9rYW5iYW4iO319czoxNToiYWN0aW9uX3ZpZXdfbWFwIjthOjE6e2k6MDthOjI6e3M6NDoiZnJvbSI7czo3NToiPGJhc2VwYXRoPi9saWNlbnNlX2FkbWluL2FjdGlvbnZpZXdtYXAva2FuYmFuTGljZW5zZUFkZG9uX2FjdGlvbnZpZXdtYXAucGhwIjtzOjk6InRvX21vZHVsZSI7czo2OiJrYW5iYW4iO319czoxNDoiYWRtaW5pc3RyYXRpb24iO2E6MTp7aTowO2E6Mjp7czo0OiJmcm9tIjtzOjU4OiI8YmFzZXBhdGg+L2xpY2Vuc2VfYWRtaW4vbWVudS9rYW5iYW5MaWNlbnNlQWRkb25fYWRtaW4ucGhwIjtzOjI6InRvIjtzOjUxOiJtb2R1bGVzL0FkbWluaXN0cmF0aW9uL2thbmJhbkxpY2Vuc2VBZGRvbl9hZG1pbi5waHAiO319fXM6MTY6InVwZ3JhZGVfbWFuaWZlc3QiO3M6MDoiIjt9','2020-08-21 06:54:33',1),('b01c9534-f37e-c217-fde2-5e909deed8cf','upload/upgrades/module/Timesheet-Ninja-Licensed-Version-With-Default-Timesheet-Settings.zip','5e88f44d7dc6f8b62de6e719cdb63427','module','installed','3.0','Timesheet Ninja Enterprise - Version 3.0','Service Push, LLC','timesheet_licensed_v2','YTozOntzOjg6Im1hbmlmZXN0IjthOjEzOntpOjA7YToxOntzOjI1OiJhY2NlcHRhYmxlX3N1Z2FyX3ZlcnNpb25zIjthOjE6e3M6MTM6InJlZ2V4X21hdGNoZXMiO2E6Mjp7aTowO3M6NToiNi41LioiO2k6MTtzOjU6IjcuKi4qIjt9fX1pOjE7YToxOntzOjI0OiJhY2NlcHRhYmxlX3N1Z2FyX2ZsYXZvcnMiO2E6Mzp7aTowO3M6MjoiQ0UiO2k6MTtzOjM6IlBSTyI7aToyO3M6MzoiRU5UIjt9fXM6NjoicmVhZG1lIjtzOjA6IiI7czozOiJrZXkiO3M6MjE6InRpbWVzaGVldF9saWNlbnNlZF92MiI7czo2OiJhdXRob3IiO3M6MTc6IlNlcnZpY2UgUHVzaCwgTExDIjtzOjExOiJkZXNjcmlwdGlvbiI7czoxNzoiU2VydmljZSBQdXNoLCBMTEMiO3M6NDoiaWNvbiI7czowOiIiO3M6MTY6ImlzX3VuaW5zdGFsbGFibGUiO2I6MTtzOjQ6Im5hbWUiO3M6NDA6IlRpbWVzaGVldCBOaW5qYSBFbnRlcnByaXNlIC0gVmVyc2lvbiAzLjAiO3M6MTQ6InB1Ymxpc2hlZF9kYXRlIjtzOjE5OiIyMDE5LTAxLTI5IDA5OjAwOjAwIjtzOjQ6InR5cGUiO3M6NjoibW9kdWxlIjtzOjc6InZlcnNpb24iO3M6MzoiMy4wIjtzOjEzOiJyZW1vdmVfdGFibGVzIjtzOjY6InByb21wdCI7fXM6MTE6Imluc3RhbGxkZWZzIjthOjEzOntzOjI6ImlkIjtzOjIxOiJ0aW1lc2hlZXRfbGljZW5zZWRfdjIiO3M6NToiYmVhbnMiO2E6NTp7aTowO2E6NDp7czo2OiJtb2R1bGUiO3M6MTI6InNwX1RpbWVzaGVldCI7czo1OiJjbGFzcyI7czoxMjoic3BfVGltZXNoZWV0IjtzOjQ6InBhdGgiO3M6Mzc6Im1vZHVsZXMvc3BfVGltZXNoZWV0L3NwX1RpbWVzaGVldC5waHAiO3M6MzoidGFiIjtiOjE7fWk6MTthOjQ6e3M6NjoibW9kdWxlIjtzOjIwOiJzcF90aW1lc2hlZXRfdHJhY2tlciI7czo1OiJjbGFzcyI7czoyMDoic3BfdGltZXNoZWV0X3RyYWNrZXIiO3M6NDoicGF0aCI7czo1MzoibW9kdWxlcy9zcF90aW1lc2hlZXRfdHJhY2tlci9zcF90aW1lc2hlZXRfdHJhY2tlci5waHAiO3M6MzoidGFiIjtiOjE7fWk6MjthOjQ6e3M6NjoibW9kdWxlIjtzOjMwOiJzcF9BdXRvX1RpbWVTaGVldF9UaW1lX1RyYWNrZXIiO3M6NToiY2xhc3MiO3M6MzA6InNwX0F1dG9fVGltZVNoZWV0X1RpbWVfVHJhY2tlciI7czo0OiJwYXRoIjtzOjczOiJtb2R1bGVzL3NwX0F1dG9fVGltZVNoZWV0X1RpbWVfVHJhY2tlci9zcF9BdXRvX1RpbWVTaGVldF9UaW1lX1RyYWNrZXIucGhwIjtzOjM6InRhYiI7YjoxO31pOjM7YTo0OntzOjY6Im1vZHVsZSI7czoxOToic3VoX3RpbWVyX3JlY29yZGluZyI7czo1OiJjbGFzcyI7czoxOToic3VoX3RpbWVyX3JlY29yZGluZyI7czo0OiJwYXRoIjtzOjUxOiJtb2R1bGVzL3N1aF90aW1lcl9yZWNvcmRpbmcvc3VoX3RpbWVyX3JlY29yZGluZy5waHAiO3M6MzoidGFiIjtiOjE7fWk6NDthOjQ6e3M6NjoibW9kdWxlIjtzOjE5OiJzcF9UaW1lb2ZmX1RpbWVDYXJkIjtzOjU6ImNsYXNzIjtzOjE5OiJzcF9UaW1lb2ZmX1RpbWVDYXJkIjtzOjQ6InBhdGgiO3M6NTE6Im1vZHVsZXMvc3BfVGltZW9mZl9UaW1lQ2FyZC9zcF9UaW1lb2ZmX1RpbWVDYXJkLnBocCI7czozOiJ0YWIiO2I6MTt9fXM6MTA6ImxheW91dGRlZnMiO2E6MDp7fXM6MTM6InJlbGF0aW9uc2hpcHMiO2E6MDp7fXM6OToiaW1hZ2VfZGlyIjtzOjE2OiI8YmFzZXBhdGg+L2ljb25zIjtzOjQ6ImNvcHkiO2E6OTA6e2k6MDthOjI6e3M6NDoiZnJvbSI7czo0NDoiPGJhc2VwYXRoPi9TdWdhck1vZHVsZXMvbW9kdWxlcy9zcF9UaW1lc2hlZXQiO3M6MjoidG8iO3M6MjA6Im1vZHVsZXMvc3BfVGltZXNoZWV0Ijt9aToxO2E6Mjp7czo0OiJmcm9tIjtzOjUyOiI8YmFzZXBhdGg+L1N1Z2FyTW9kdWxlcy9tb2R1bGVzL3NwX3RpbWVzaGVldF90cmFja2VyIjtzOjI6InRvIjtzOjI4OiJtb2R1bGVzL3NwX3RpbWVzaGVldF90cmFja2VyIjt9aToyO2E6Mjp7czo0OiJmcm9tIjtzOjgwOiI8YmFzZXBhdGg+L2N1c3RvbS9FeHRlbnNpb24vbW9kdWxlcy9Vc2Vycy9FeHQvVmFyZGVmcy9zdWdhcmZpZWxkX2hvdXJseV9yYXRlLnBocCI7czoyOiJ0byI7czo2OToiY3VzdG9tL0V4dGVuc2lvbi9tb2R1bGVzL1VzZXJzL0V4dC9WYXJkZWZzL3N1Z2FyZmllbGRfaG91cmx5X3JhdGUucGhwIjt9aTozO2E6Mjp7czo0OiJmcm9tIjtzOjc1OiI8YmFzZXBhdGg+L2N1c3RvbS9FeHRlbnNpb24vbW9kdWxlcy9Vc2Vycy9FeHQvTGFuZ3VhZ2UvZW5fdXMuaG91cmx5UmF0ZS5waHAiO3M6MjoidG8iO3M6NjQ6ImN1c3RvbS9FeHRlbnNpb24vbW9kdWxlcy9Vc2Vycy9FeHQvTGFuZ3VhZ2UvZW5fdXMuaG91cmx5UmF0ZS5waHAiO31pOjQ7YToyOntzOjQ6ImZyb20iO3M6NjI6IjxiYXNlcGF0aD4vY3VzdG9tL21vZHVsZXMvc3BfdGltZXNoZWV0X3RyYWNrZXIvbG9naWNfaG9va3MucGhwIjtzOjI6InRvIjtzOjUxOiJjdXN0b20vbW9kdWxlcy9zcF90aW1lc2hlZXRfdHJhY2tlci9sb2dpY19ob29rcy5waHAiO31pOjU7YToyOntzOjQ6ImZyb20iO3M6ODA6IjxiYXNlcGF0aD4vY3VzdG9tL21vZHVsZXMvc3BfdGltZXNoZWV0X3RyYWNrZXIvc3BfdGltZXNoZWV0X3RyYWNrZXJMb2dpY0hvb2sucGhwIjtzOjI6InRvIjtzOjY5OiJjdXN0b20vbW9kdWxlcy9zcF90aW1lc2hlZXRfdHJhY2tlci9zcF90aW1lc2hlZXRfdHJhY2tlckxvZ2ljSG9vay5waHAiO31pOjY7YToyOntzOjQ6ImZyb20iO3M6NzA6IjxiYXNlcGF0aD4vY3VzdG9tL0V4dGVuc2lvbi9hcHBsaWNhdGlvbi9FeHQvVXRpbHMvcGRmX2V4cG9ydF91dGlscy5waHAiO3M6MjoidG8iO3M6NTk6ImN1c3RvbS9FeHRlbnNpb24vYXBwbGljYXRpb24vRXh0L1V0aWxzL3BkZl9leHBvcnRfdXRpbHMucGhwIjt9aTo3O2E6Mjp7czo0OiJmcm9tIjtzOjg4OiI8YmFzZXBhdGg+L2N1c3RvbS9FeHRlbnNpb24vYXBwbGljYXRpb24vRXh0L0VudHJ5UG9pbnRSZWdpc3RyeS9FeHBvcnRfcGRmX0VudHJ5UG9pbnQucGhwIjtzOjI6InRvIjtzOjc3OiJjdXN0b20vRXh0ZW5zaW9uL2FwcGxpY2F0aW9uL0V4dC9FbnRyeVBvaW50UmVnaXN0cnkvRXhwb3J0X3BkZl9FbnRyeVBvaW50LnBocCI7fWk6ODthOjI6e3M6NDoiZnJvbSI7czo2MjoiPGJhc2VwYXRoPi9TdWdhck1vZHVsZXMvbW9kdWxlcy9zcF9BdXRvX1RpbWVTaGVldF9UaW1lX1RyYWNrZXIiO3M6MjoidG8iO3M6Mzg6Im1vZHVsZXMvc3BfQXV0b19UaW1lU2hlZXRfVGltZV9UcmFja2VyIjt9aTo5O2E6Mjp7czo0OiJmcm9tIjtzOjUyOiI8YmFzZXBhdGg+L2N1c3RvbS9pbmNsdWRlL2F1dG9fdGltZV9zaGVldF90cmFja2VyLmpzIjtzOjI6InRvIjtzOjQxOiJjdXN0b20vaW5jbHVkZS9hdXRvX3RpbWVfc2hlZXRfdHJhY2tlci5qcyI7fWk6MTA7YToyOntzOjQ6ImZyb20iO3M6ODY6IjxiYXNlcGF0aD4vY3VzdG9tL21vZHVsZXMvUHJvamVjdFRhc2svcHJvamVjdFRhc2tMb2dpY0hvb2tJbXBsZW1lbnRhdGlvblRpbWVTaGVldHMucGhwIjtzOjI6InRvIjtzOjc1OiJjdXN0b20vbW9kdWxlcy9Qcm9qZWN0VGFzay9wcm9qZWN0VGFza0xvZ2ljSG9va0ltcGxlbWVudGF0aW9uVGltZVNoZWV0cy5waHAiO31pOjExO2E6Mjp7czo0OiJmcm9tIjtzOjk1OiI8YmFzZXBhdGg+L2N1c3RvbS9FeHRlbnNpb24vbW9kdWxlcy9TY2hlZHVsZXJzL0V4dC9TY2hlZHVsZWRUYXNrcy9jYWxjdWxhdGVBdXRvVHJhY2tUaW1pbmdzLnBocCI7czoyOiJ0byI7czo4NDoiY3VzdG9tL0V4dGVuc2lvbi9tb2R1bGVzL1NjaGVkdWxlcnMvRXh0L1NjaGVkdWxlZFRhc2tzL2NhbGN1bGF0ZUF1dG9UcmFja1RpbWluZ3MucGhwIjt9aToxMjthOjI6e3M6NDoiZnJvbSI7czo5NToiPGJhc2VwYXRoPi9jdXN0b20vRXh0ZW5zaW9uL21vZHVsZXMvU2NoZWR1bGVycy9FeHQvTGFuZ3VhZ2UvZW5fdXMuY2FsY3VsYXRlQXV0b1RyYWNrVGltaW5ncy5waHAiO3M6MjoidG8iO3M6ODQ6ImN1c3RvbS9FeHRlbnNpb24vbW9kdWxlcy9TY2hlZHVsZXJzL0V4dC9MYW5ndWFnZS9lbl91cy5jYWxjdWxhdGVBdXRvVHJhY2tUaW1pbmdzLnBocCI7fWk6MTM7YToyOntzOjQ6ImZyb20iO3M6NDY6IjxiYXNlcGF0aD4vaWNvbnMvZGVmYXVsdC9pbWFnZXMvVGltZXNoZWV0cy5naWYiO3M6MjoidG8iO3M6MzU6InRoZW1lcy9TdWl0ZVIvaW1hZ2VzL1RpbWVzaGVldHMuZ2lmIjt9aToxNDthOjI6e3M6NDoiZnJvbSI7czo3MToiPGJhc2VwYXRoPi9jdXN0b20vRXh0ZW5zaW9uL2FwcGxpY2F0aW9uL0V4dC9VdGlscy90aW1lX3NoZWV0X3BvcF91cC5waHAiO3M6MjoidG8iO3M6NjA6ImN1c3RvbS9FeHRlbnNpb24vYXBwbGljYXRpb24vRXh0L1V0aWxzL3RpbWVfc2hlZXRfcG9wX3VwLnBocCI7fWk6MTU7YToyOntzOjQ6ImZyb20iO3M6NDI6IjxiYXNlcGF0aD4vaW5jbHVkZS9qYXZhc2NyaXB0L2pzQWxlcnRzLnBocCI7czoyOiJ0byI7czozODoiY3VzdG9tL2luY2x1ZGUvamF2YXNjcmlwdC9qc0FsZXJ0cy5waHAiO31pOjE2O2E6Mjp7czo0OiJmcm9tIjtzOjkxOiI8YmFzZXBhdGg+L2N1c3RvbS9FeHRlbnNpb24vbW9kdWxlcy9TY2hlZHVsZXJzL0V4dC9TY2hlZHVsZWRUYXNrcy9lbWFpbEF1dG9UcmFja1RpbWluZ3MucGhwIjtzOjI6InRvIjtzOjgwOiJjdXN0b20vRXh0ZW5zaW9uL21vZHVsZXMvU2NoZWR1bGVycy9FeHQvU2NoZWR1bGVkVGFza3MvZW1haWxBdXRvVHJhY2tUaW1pbmdzLnBocCI7fWk6MTc7YToyOntzOjQ6ImZyb20iO3M6OTU6IjxiYXNlcGF0aD4vY3VzdG9tL0V4dGVuc2lvbi9tb2R1bGVzL1NjaGVkdWxlcnMvRXh0L1NjaGVkdWxlZFRhc2tzL25vQWN0aXZpdHlGb3JQcm9qZWN0VGFza3MucGhwIjtzOjI6InRvIjtzOjg0OiJjdXN0b20vRXh0ZW5zaW9uL21vZHVsZXMvU2NoZWR1bGVycy9FeHQvU2NoZWR1bGVkVGFza3Mvbm9BY3Rpdml0eUZvclByb2plY3RUYXNrcy5waHAiO31pOjE4O2E6Mjp7czo0OiJmcm9tIjtzOjg2OiI8YmFzZXBhdGg+L2N1c3RvbS9FeHRlbnNpb24vbW9kdWxlcy9Qcm9qZWN0VGFzay9FeHQvVmFyZGVmcy9zdWdhcmZpZWxkX2lzX2JpbGxhYmxlLnBocCI7czoyOiJ0byI7czo3NToiY3VzdG9tL0V4dGVuc2lvbi9tb2R1bGVzL1Byb2plY3RUYXNrL0V4dC9WYXJkZWZzL3N1Z2FyZmllbGRfaXNfYmlsbGFibGUucGhwIjt9aToxOTthOjI6e3M6NDoiZnJvbSI7czo4MjoiPGJhc2VwYXRoPi9jdXN0b20vRXh0ZW5zaW9uL21vZHVsZXMvUHJvamVjdFRhc2svRXh0L0xhbmd1YWdlL2VuX3VzLmlzX2JpbGxhYmxlLnBocCI7czoyOiJ0byI7czo3MToiY3VzdG9tL0V4dGVuc2lvbi9tb2R1bGVzL1Byb2plY3RUYXNrL0V4dC9MYW5ndWFnZS9lbl91cy5pc19iaWxsYWJsZS5waHAiO31pOjIwO2E6Mjp7czo0OiJmcm9tIjtzOjc0OiI8YmFzZXBhdGg+L2N1c3RvbS9FeHRlbnNpb24vYXBwbGljYXRpb24vRXh0L0xhbmd1YWdlL2VuX3VzLmlzX2JpbGxhYmxlLnBocCI7czoyOiJ0byI7czo2MzoiY3VzdG9tL0V4dGVuc2lvbi9hcHBsaWNhdGlvbi9FeHQvTGFuZ3VhZ2UvZW5fdXMuaXNfYmlsbGFibGUucGhwIjt9aToyMTthOjI6e3M6NDoiZnJvbSI7czo0NjoiPGJhc2VwYXRoPi9jdXN0b20vbW9kdWxlcy9zcF90aW1lc2hlZXRfdHJhY2tlciI7czoyOiJ0byI7czozNToiY3VzdG9tL21vZHVsZXMvc3BfdGltZXNoZWV0X3RyYWNrZXIiO31pOjIyO2E6Mjp7czo0OiJmcm9tIjtzOjYzOiI8YmFzZXBhdGg+L21ldGFkYXRhL2FjY291bnRzX3NwX3RpbWVzaGVldF90cmFja2VyXzFNZXRhRGF0YS5waHAiO3M6MjoidG8iO3M6NTk6ImN1c3RvbS9tZXRhZGF0YS9hY2NvdW50c19zcF90aW1lc2hlZXRfdHJhY2tlcl8xTWV0YURhdGEucGhwIjt9aToyMzthOjI6e3M6NDoiZnJvbSI7czo2MjoiPGJhc2VwYXRoPi9tZXRhZGF0YS9wcm9qZWN0X3NwX3RpbWVzaGVldF90cmFja2VyXzFNZXRhRGF0YS5waHAiO3M6MjoidG8iO3M6NTg6ImN1c3RvbS9tZXRhZGF0YS9wcm9qZWN0X3NwX3RpbWVzaGVldF90cmFja2VyXzFNZXRhRGF0YS5waHAiO31pOjI0O2E6Mjp7czo0OiJmcm9tIjtzOjk1OiI8YmFzZXBhdGg+L2N1c3RvbS9FeHRlbnNpb24vYXBwbGljYXRpb24vRXh0L1RhYmxlRGljdGlvbmFyeS9hY2NvdW50c19zcF90aW1lc2hlZXRfdHJhY2tlcl8xLnBocCI7czoyOiJ0byI7czo4NDoiY3VzdG9tL0V4dGVuc2lvbi9hcHBsaWNhdGlvbi9FeHQvVGFibGVEaWN0aW9uYXJ5L2FjY291bnRzX3NwX3RpbWVzaGVldF90cmFja2VyXzEucGhwIjt9aToyNTthOjI6e3M6NDoiZnJvbSI7czo5NDoiPGJhc2VwYXRoPi9jdXN0b20vRXh0ZW5zaW9uL2FwcGxpY2F0aW9uL0V4dC9UYWJsZURpY3Rpb25hcnkvcHJvamVjdF9zcF90aW1lc2hlZXRfdHJhY2tlcl8xLnBocCI7czoyOiJ0byI7czo4MzoiY3VzdG9tL0V4dGVuc2lvbi9hcHBsaWNhdGlvbi9FeHQvVGFibGVEaWN0aW9uYXJ5L3Byb2plY3Rfc3BfdGltZXNoZWV0X3RyYWNrZXJfMS5waHAiO31pOjI2O2E6Mjp7czo0OiJmcm9tIjtzOjEwMToiPGJhc2VwYXRoPi9jdXN0b20vRXh0ZW5zaW9uL21vZHVsZXMvQWNjb3VudHMvRXh0L1ZhcmRlZnMvYWNjb3VudHNfc3BfdGltZXNoZWV0X3RyYWNrZXJfMV9BY2NvdW50cy5waHAiO3M6MjoidG8iO3M6OTA6ImN1c3RvbS9FeHRlbnNpb24vbW9kdWxlcy9BY2NvdW50cy9FeHQvVmFyZGVmcy9hY2NvdW50c19zcF90aW1lc2hlZXRfdHJhY2tlcl8xX0FjY291bnRzLnBocCI7fWk6Mjc7YToyOntzOjQ6ImZyb20iO3M6ODY6IjxiYXNlcGF0aD4vY3VzdG9tL0V4dGVuc2lvbi9tb2R1bGVzL0FjY291bnRzL0V4dC9WYXJkZWZzL3N1Z2FyZmllbGRfdG90YWxfaG91cnNfYXQucGhwIjtzOjI6InRvIjtzOjc1OiJjdXN0b20vRXh0ZW5zaW9uL21vZHVsZXMvQWNjb3VudHMvRXh0L1ZhcmRlZnMvc3VnYXJmaWVsZF90b3RhbF9ob3Vyc19hdC5waHAiO31pOjI4O2E6Mjp7czo0OiJmcm9tIjtzOjEwNDoiPGJhc2VwYXRoPi9jdXN0b20vRXh0ZW5zaW9uL21vZHVsZXMvQWNjb3VudHMvRXh0L0xheW91dGRlZnMvYWNjb3VudHNfc3BfdGltZXNoZWV0X3RyYWNrZXJfMV9BY2NvdW50cy5waHAiO3M6MjoidG8iO3M6OTM6ImN1c3RvbS9FeHRlbnNpb24vbW9kdWxlcy9BY2NvdW50cy9FeHQvTGF5b3V0ZGVmcy9hY2NvdW50c19zcF90aW1lc2hlZXRfdHJhY2tlcl8xX0FjY291bnRzLnBocCI7fWk6MzA7YToyOntzOjQ6ImZyb20iO3M6MTA1OiI8YmFzZXBhdGg+L2N1c3RvbS9FeHRlbnNpb24vbW9kdWxlcy9BY2NvdW50cy9FeHQvTGFuZ3VhZ2UvZW5fdXMuY3VzdG9tYWNjb3VudHNfc3BfdGltZXNoZWV0X3RyYWNrZXJfMS5waHAiO3M6MjoidG8iO3M6OTQ6ImN1c3RvbS9FeHRlbnNpb24vbW9kdWxlcy9BY2NvdW50cy9FeHQvTGFuZ3VhZ2UvZW5fdXMuY3VzdG9tYWNjb3VudHNfc3BfdGltZXNoZWV0X3RyYWNrZXJfMS5waHAiO31pOjMxO2E6Mjp7czo0OiJmcm9tIjtzOjk4OiI8YmFzZXBhdGg+L2N1c3RvbS9FeHRlbnNpb24vbW9kdWxlcy9Qcm9qZWN0L0V4dC9WYXJkZWZzL3Byb2plY3Rfc3BfdGltZXNoZWV0X3RyYWNrZXJfMV9Qcm9qZWN0LnBocCI7czoyOiJ0byI7czo4NzoiY3VzdG9tL0V4dGVuc2lvbi9tb2R1bGVzL1Byb2plY3QvRXh0L1ZhcmRlZnMvcHJvamVjdF9zcF90aW1lc2hlZXRfdHJhY2tlcl8xX1Byb2plY3QucGhwIjt9aTozMjthOjI6e3M6NDoiZnJvbSI7czo4NToiPGJhc2VwYXRoPi9jdXN0b20vRXh0ZW5zaW9uL21vZHVsZXMvUHJvamVjdC9FeHQvVmFyZGVmcy9zdWdhcmZpZWxkX3RvdGFsX2hvdXJzX3B0LnBocCI7czoyOiJ0byI7czo3NDoiY3VzdG9tL0V4dGVuc2lvbi9tb2R1bGVzL1Byb2plY3QvRXh0L1ZhcmRlZnMvc3VnYXJmaWVsZF90b3RhbF9ob3Vyc19wdC5waHAiO31pOjMzO2E6Mjp7czo0OiJmcm9tIjtzOjEwMToiPGJhc2VwYXRoPi9jdXN0b20vRXh0ZW5zaW9uL21vZHVsZXMvUHJvamVjdC9FeHQvTGF5b3V0ZGVmcy9wcm9qZWN0X3NwX3RpbWVzaGVldF90cmFja2VyXzFfUHJvamVjdC5waHAiO3M6MjoidG8iO3M6OTA6ImN1c3RvbS9FeHRlbnNpb24vbW9kdWxlcy9Qcm9qZWN0L0V4dC9MYXlvdXRkZWZzL3Byb2plY3Rfc3BfdGltZXNoZWV0X3RyYWNrZXJfMV9Qcm9qZWN0LnBocCI7fWk6MzU7YToyOntzOjQ6ImZyb20iO3M6MTAzOiI8YmFzZXBhdGg+L2N1c3RvbS9FeHRlbnNpb24vbW9kdWxlcy9Qcm9qZWN0L0V4dC9MYW5ndWFnZS9lbl91cy5jdXN0b21wcm9qZWN0X3NwX3RpbWVzaGVldF90cmFja2VyXzEucGhwIjtzOjI6InRvIjtzOjkyOiJjdXN0b20vRXh0ZW5zaW9uL21vZHVsZXMvUHJvamVjdC9FeHQvTGFuZ3VhZ2UvZW5fdXMuY3VzdG9tcHJvamVjdF9zcF90aW1lc2hlZXRfdHJhY2tlcl8xLnBocCI7fWk6Mzc7YToyOntzOjQ6ImZyb20iO3M6NTE6IjxiYXNlcGF0aD4vY3VzdG9tL2luY2x1ZGUvdGNwZGYvZm9udHMvaGVsdmV0aWNhLnBocCI7czoyOiJ0byI7czozMzoiaW5jbHVkZS90Y3BkZi9mb250cy9oZWx2ZXRpY2EucGhwIjt9aTozODthOjI6e3M6NDoiZnJvbSI7czo1NDoiPGJhc2VwYXRoPi9jdXN0b20vbW9kdWxlcy9zcF9UaW1lc2hlZXQvbG9naWNfaG9va3MucGhwIjtzOjI6InRvIjtzOjQzOiJjdXN0b20vbW9kdWxlcy9zcF9UaW1lc2hlZXQvbG9naWNfaG9va3MucGhwIjt9aTozOTthOjI6e3M6NDoiZnJvbSI7czo2NDoiPGJhc2VwYXRoPi9jdXN0b20vbW9kdWxlcy9zcF9UaW1lc2hlZXQvc3BfVGltZXNoZWV0TG9naWNIb29rLnBocCI7czoyOiJ0byI7czo1MzoiY3VzdG9tL21vZHVsZXMvc3BfVGltZXNoZWV0L3NwX1RpbWVzaGVldExvZ2ljSG9vay5waHAiO31pOjQwO2E6Mjp7czo0OiJmcm9tIjtzOjYzOiI8YmFzZXBhdGg+L2N1c3RvbS9tb2R1bGVzL0Nhc2VzL2Nhc2VMb2dpY0hvb2tJbXBsZW1lbnRhdGlvbi5waHAiO3M6MjoidG8iO3M6NTI6ImN1c3RvbS9tb2R1bGVzL0Nhc2VzL2Nhc2VMb2dpY0hvb2tJbXBsZW1lbnRhdGlvbi5waHAiO31pOjQxO2E6Mjp7czo0OiJmcm9tIjtzOjUzOiI8YmFzZXBhdGg+L2N1c3RvbS90aGVtZXMvU3VpdGU3L3RwbHMvX2dsb2JhbExpbmtzLnRwbCI7czoyOiJ0byI7czo0MjoiY3VzdG9tL3RoZW1lcy9TdWl0ZTcvdHBscy9fZ2xvYmFsTGlua3MudHBsIjt9aTo0MjthOjI6e3M6NDoiZnJvbSI7czo1ODoiPGJhc2VwYXRoPi9jdXN0b20vdGhlbWVzL1N1aXRlUi90cGxzL19oZWFkZXJNb2R1bGVMaXN0LnRwbCI7czoyOiJ0byI7czo0NzoiY3VzdG9tL3RoZW1lcy9TdWl0ZVIvdHBscy9faGVhZGVyTW9kdWxlTGlzdC50cGwiO31pOjQzO2E6Mjp7czo0OiJmcm9tIjtzOjUxOiI8YmFzZXBhdGg+L1N1Z2FyTW9kdWxlcy9tb2R1bGVzL3N1aF90aW1lcl9yZWNvcmRpbmciO3M6MjoidG8iO3M6Mjc6Im1vZHVsZXMvc3VoX3RpbWVyX3JlY29yZGluZyI7fWk6NDQ7YToyOntzOjQ6ImZyb20iO3M6MzI6IjxiYXNlcGF0aD4vY3VzdG9tL3RpbWVyUG9wdXAucGhwIjtzOjI6InRvIjtzOjU4OiJjdXN0b20vRXh0ZW5zaW9uL2FwcGxpY2F0aW9uL0V4dC9Mb2dpY0hvb2tzL3RpbWVyUG9wdXAucGhwIjt9aTo0NTthOjI6e3M6NDoiZnJvbSI7czozNzoiPGJhc2VwYXRoPi9jdXN0b20vaW5jbHVkZS90aW1lcl9wb3B1cCI7czoyOiJ0byI7czoyNjoiY3VzdG9tL2luY2x1ZGUvdGltZXJfcG9wdXAiO31pOjQ2O2E6Mjp7czo0OiJmcm9tIjtzOjgzOiI8YmFzZXBhdGg+L2N1c3RvbS9tb2R1bGVzL3NwX0F1dG9fVGltZVNoZWV0X1RpbWVfVHJhY2tlci9Ib29rcy91cGRhdGVfdGltZXNoZWV0LnBocCI7czoyOiJ0byI7czo3MjoiY3VzdG9tL21vZHVsZXMvc3BfQXV0b19UaW1lU2hlZXRfVGltZV9UcmFja2VyL0hvb2tzL3VwZGF0ZV90aW1lc2hlZXQucGhwIjt9aTo0NzthOjI6e3M6NDoiZnJvbSI7czoxODoiPGJhc2VwYXRoPi9saWNlbnNlIjtzOjI6InRvIjtzOjIyOiJtb2R1bGVzL1RpbWVzaGVldE5pbmphIjt9aTo0ODthOjI6e3M6NDoiZnJvbSI7czo1MToiPGJhc2VwYXRoPi9TdWdhck1vZHVsZXMvbW9kdWxlcy9zcF9UaW1lb2ZmX1RpbWVDYXJkIjtzOjI6InRvIjtzOjI3OiJtb2R1bGVzL3NwX1RpbWVvZmZfVGltZUNhcmQiO31pOjQ5O2E6Mjp7czo0OiJmcm9tIjtzOjYxOiI8YmFzZXBhdGg+L2N1c3RvbS9tb2R1bGVzL3NwX1RpbWVvZmZfVGltZUNhcmQvbG9naWNfaG9va3MucGhwIjtzOjI6InRvIjtzOjUwOiJjdXN0b20vbW9kdWxlcy9zcF9UaW1lb2ZmX1RpbWVDYXJkL2xvZ2ljX2hvb2tzLnBocCI7fWk6NTA7YToyOntzOjQ6ImZyb20iO3M6Nzg6IjxiYXNlcGF0aD4vY3VzdG9tL21vZHVsZXMvc3BfVGltZW9mZl9UaW1lQ2FyZC9zcF9UaW1lb2ZmX1RpbWVDYXJkTG9naWNIb29rLnBocCI7czoyOiJ0byI7czo2NzoiY3VzdG9tL21vZHVsZXMvc3BfVGltZW9mZl9UaW1lQ2FyZC9zcF9UaW1lb2ZmX1RpbWVDYXJkTG9naWNIb29rLnBocCI7fWk6NTE7YToyOntzOjQ6ImZyb20iO3M6NjA6IjxiYXNlcGF0aD4vY3VzdG9tL21vZHVsZXMvc3BfVGltZW9mZl9UaW1lQ2FyZC9jb250cm9sbGVyLnBocCI7czoyOiJ0byI7czo0OToiY3VzdG9tL21vZHVsZXMvc3BfVGltZW9mZl9UaW1lQ2FyZC9jb250cm9sbGVyLnBocCI7fWk6NTI7YToyOntzOjQ6ImZyb20iO3M6ODI6IjxiYXNlcGF0aD4vY3VzdG9tL0V4dGVuc2lvbi9hcHBsaWNhdGlvbi9FeHQvRW50cnlQb2ludFJlZ2lzdHJ5L3RpbWVzaGVldF9uaW5qYS5waHAiO3M6MjoidG8iO3M6NzE6ImN1c3RvbS9FeHRlbnNpb24vYXBwbGljYXRpb24vRXh0L0VudHJ5UG9pbnRSZWdpc3RyeS90aW1lc2hlZXRfbmluamEucGhwIjt9aTo1MzthOjI6e3M6NDoiZnJvbSI7czozMjoiPGJhc2VwYXRoPi9jdXN0b20vaW5jbHVkZS9jaGFydHMiO3M6MjoidG8iO3M6MjE6ImN1c3RvbS9pbmNsdWRlL2NoYXJ0cyI7fWk6NTQ7YToyOntzOjQ6ImZyb20iO3M6NzI6IjxiYXNlcGF0aD4vY3VzdG9tL0V4dGVuc2lvbi9tb2R1bGVzL1Byb2plY3QvRXh0L1ZhcmRlZnMvYnVkZ2V0X2hvdXJzLnBocCI7czoyOiJ0byI7czo2MToiY3VzdG9tL0V4dGVuc2lvbi9tb2R1bGVzL1Byb2plY3QvRXh0L1ZhcmRlZnMvYnVkZ2V0X2hvdXJzLnBocCI7fWk6NTU7YToyOntzOjQ6ImZyb20iO3M6Njk6IjxiYXNlcGF0aD4vY3VzdG9tL21vZHVsZXMvc3BfdGltZXNoZWV0X3RyYWNrZXIvdXBkVGltZXNoZWV0UXJ5Q2xzLnBocCI7czoyOiJ0byI7czo1ODoiY3VzdG9tL21vZHVsZXMvc3BfdGltZXNoZWV0X3RyYWNrZXIvdXBkVGltZXNoZWV0UXJ5Q2xzLnBocCI7fWk6NTY7YToyOntzOjQ6ImZyb20iO3M6OTg6IjxiYXNlcGF0aD4vY3VzdG9tL0V4dGVuc2lvbi9tb2R1bGVzL3NwX0F1dG9fVGltZVNoZWV0X1RpbWVfVHJhY2tlci9FeHQvVmFyZGVmcy9yZWxhdGVkX3Byb2plY3QucGhwIjtzOjI6InRvIjtzOjg3OiJjdXN0b20vRXh0ZW5zaW9uL21vZHVsZXMvc3BfQXV0b19UaW1lU2hlZXRfVGltZV9UcmFja2VyL0V4dC9WYXJkZWZzL3JlbGF0ZWRfcHJvamVjdC5waHAiO31pOjU3O2E6Mjp7czo0OiJmcm9tIjtzOjgyOiI8YmFzZXBhdGg+L2N1c3RvbS9FeHRlbnNpb24vbW9kdWxlcy9zcF9UaW1lc2hlZXQvRXh0L1ZhcmRlZnMvbWFzc191cGRhdGVfZmllbGQucGhwIjtzOjI6InRvIjtzOjcxOiJjdXN0b20vRXh0ZW5zaW9uL21vZHVsZXMvc3BfVGltZXNoZWV0L0V4dC9WYXJkZWZzL21hc3NfdXBkYXRlX2ZpZWxkLnBocCI7fWk6NTg7YToyOntzOjQ6ImZyb20iO3M6NDk6IjxiYXNlcGF0aD4vY3VzdG9tL2luY2x1ZGUvY2hhcnRzL2N1c3RvbV9zY3JpcHQuanMiO3M6MjoidG8iO3M6Mzg6ImN1c3RvbS9pbmNsdWRlL2NoYXJ0cy9jdXN0b21fc2NyaXB0LmpzIjt9aTo1OTthOjI6e3M6NDoiZnJvbSI7czo2NDoiPGJhc2VwYXRoPi9jdXN0b20vaW5jbHVkZS9TdWdhckZpZWxkcy9GaWVsZHMvUGFyZW50L0VkaXRWaWV3LnRwbCI7czoyOiJ0byI7czo1MzoiY3VzdG9tL2luY2x1ZGUvU3VnYXJGaWVsZHMvRmllbGRzL1BhcmVudC9FZGl0Vmlldy50cGwiO31pOjYwO2E6Mjp7czo0OiJmcm9tIjtzOjUyOiI8YmFzZXBhdGg+L2N1c3RvbS9tb2R1bGVzL0FjY291bnRzL3VwZEFjY291bnRDbHMucGhwIjtzOjI6InRvIjtzOjQxOiJjdXN0b20vbW9kdWxlcy9BY2NvdW50cy91cGRBY2NvdW50Q2xzLnBocCI7fWk6NjE7YToyOntzOjQ6ImZyb20iO3M6NjE6IjxiYXNlcGF0aD4vY3VzdG9tL21vZHVsZXMvUHJvamVjdC9tZXRhZGF0YS9kZXRhaWx2aWV3ZGVmcy5waHAiO3M6MjoidG8iO3M6NTQ6ImN1c3RvbS9tb2R1bGVzL1Byb2plY3QvbWV0YWRhdGEvZGV0YWlsdmlld2RlZnNfYmFrLnBocCI7fWk6NjI7YToyOntzOjQ6ImZyb20iO3M6NTk6IjxiYXNlcGF0aD4vY3VzdG9tL21vZHVsZXMvUHJvamVjdC9tZXRhZGF0YS9lZGl0dmlld2RlZnMucGhwIjtzOjI6InRvIjtzOjUyOiJjdXN0b20vbW9kdWxlcy9Qcm9qZWN0L21ldGFkYXRhL2VkaXR2aWV3ZGVmc19iYWsucGhwIjt9aTo2MzthOjI6e3M6NDoiZnJvbSI7czo3ODoiPGJhc2VwYXRoPi9jdXN0b20vbW9kdWxlcy9zcF9BdXRvX1RpbWVTaGVldF9UaW1lX1RyYWNrZXIvSG9va3MvcmVkaXJlY3RDbHMucGhwIjtzOjI6InRvIjtzOjY3OiJjdXN0b20vbW9kdWxlcy9zcF9BdXRvX1RpbWVTaGVldF9UaW1lX1RyYWNrZXIvSG9va3MvcmVkaXJlY3RDbHMucGhwIjt9aTo2NDthOjI6e3M6NDoiZnJvbSI7czo4MjoiPGJhc2VwYXRoPi9jdXN0b20vbW9kdWxlcy9zcF9BdXRvX1RpbWVTaGVldF9UaW1lX1RyYWNrZXIvbWV0YWRhdGEvZWRpdHZpZXdkZWZzLnBocCI7czoyOiJ0byI7czo3MToiY3VzdG9tL21vZHVsZXMvc3BfQXV0b19UaW1lU2hlZXRfVGltZV9UcmFja2VyL21ldGFkYXRhL2VkaXR2aWV3ZGVmcy5waHAiO31pOjY1O2E6Mjp7czo0OiJmcm9tIjtzOjg0OiI8YmFzZXBhdGg+L2N1c3RvbS9tb2R1bGVzL3NwX0F1dG9fVGltZVNoZWV0X1RpbWVfVHJhY2tlci9tZXRhZGF0YS9kZXRhaWx2aWV3ZGVmcy5waHAiO3M6MjoidG8iO3M6NzM6ImN1c3RvbS9tb2R1bGVzL3NwX0F1dG9fVGltZVNoZWV0X1RpbWVfVHJhY2tlci9tZXRhZGF0YS9kZXRhaWx2aWV3ZGVmcy5waHAiO31pOjY2O2E6Mjp7czo0OiJmcm9tIjtzOjc4OiI8YmFzZXBhdGg+L2N1c3RvbS9tb2R1bGVzL3NwX0F1dG9fVGltZVNoZWV0X1RpbWVfVHJhY2tlci92aWV3cy92aWV3LmRldGFpbC5waHAiO3M6MjoidG8iO3M6Njc6ImN1c3RvbS9tb2R1bGVzL3NwX0F1dG9fVGltZVNoZWV0X1RpbWVfVHJhY2tlci92aWV3cy92aWV3LmRldGFpbC5waHAiO31pOjY3O2E6Mjp7czo0OiJmcm9tIjtzOjc2OiI8YmFzZXBhdGg+L2N1c3RvbS9tb2R1bGVzL3NwX0F1dG9fVGltZVNoZWV0X1RpbWVfVHJhY2tlci92aWV3cy92aWV3LmVkaXQucGhwIjtzOjI6InRvIjtzOjY1OiJjdXN0b20vbW9kdWxlcy9zcF9BdXRvX1RpbWVTaGVldF9UaW1lX1RyYWNrZXIvdmlld3Mvdmlldy5lZGl0LnBocCI7fWk6Njg7YToyOntzOjQ6ImZyb20iO3M6Nzc6IjxiYXNlcGF0aD4vY3VzdG9tL21vZHVsZXMvc3BfQXV0b19UaW1lU2hlZXRfVGltZV9UcmFja2VyL2dlUHJvamVjdERldGFpbHMucGhwIjtzOjI6InRvIjtzOjY2OiJjdXN0b20vbW9kdWxlcy9zcF9BdXRvX1RpbWVTaGVldF9UaW1lX1RyYWNrZXIvZ2VQcm9qZWN0RGV0YWlscy5waHAiO31pOjY5O2E6Mjp7czo0OiJmcm9tIjtzOjcyOiI8YmFzZXBhdGg+L2N1c3RvbS9tb2R1bGVzL3NwX0F1dG9fVGltZVNoZWV0X1RpbWVfVHJhY2tlci9sb2dpY19ob29rcy5waHAiO3M6MjoidG8iO3M6NjE6ImN1c3RvbS9tb2R1bGVzL3NwX0F1dG9fVGltZVNoZWV0X1RpbWVfVHJhY2tlci9sb2dpY19ob29rcy5waHAiO31pOjcwO2E6Mjp7czo0OiJmcm9tIjtzOjU4OiI8YmFzZXBhdGg+L2N1c3RvbS9tb2R1bGVzL3NwX1RpbWVzaGVldC9tb250aGx5X3JlcG9ydHMucGhwIjtzOjI6InRvIjtzOjQ3OiJjdXN0b20vbW9kdWxlcy9zcF9UaW1lc2hlZXQvbW9udGhseV9yZXBvcnRzLnBocCI7fWk6NzE7YToyOntzOjQ6ImZyb20iO3M6Njc6IjxiYXNlcGF0aD4vY3VzdG9tL21vZHVsZXMvc3BfVGltZXNoZWV0L3ZpZXdzL3ZpZXcudmlzdWFscmVwb3J0cy5waHAiO3M6MjoidG8iO3M6NTY6ImN1c3RvbS9tb2R1bGVzL3NwX1RpbWVzaGVldC92aWV3cy92aWV3LnZpc3VhbHJlcG9ydHMucGhwIjt9aTo3MjthOjI6e3M6NDoiZnJvbSI7czo2NjoiPGJhc2VwYXRoPi9jdXN0b20vbW9kdWxlcy9zcF90aW1lc2hlZXRfdHJhY2tlci92aWV3cy92aWV3Lmxpc3QucGhwIjtzOjI6InRvIjtzOjU1OiJjdXN0b20vbW9kdWxlcy9zcF90aW1lc2hlZXRfdHJhY2tlci92aWV3cy92aWV3Lmxpc3QucGhwIjt9aTo3MzthOjI6e3M6NDoiZnJvbSI7czo1OToiPGJhc2VwYXRoPi9jdXN0b20vbW9kdWxlcy9zcF90aW1lc2hlZXRfdHJhY2tlci9ob29rX3N1bS5waHAiO3M6MjoidG8iO3M6NDg6ImN1c3RvbS9tb2R1bGVzL3NwX3RpbWVzaGVldF90cmFja2VyL2hvb2tfc3VtLnBocCI7fWk6NzQ7YToyOntzOjQ6ImZyb20iO3M6NjI6IjxiYXNlcGF0aD4vY3VzdG9tL21vZHVsZXMvc3BfdGltZXNoZWV0X3RyYWNrZXIvbG9naWNfaG9va3MucGhwIjtzOjI6InRvIjtzOjUxOiJjdXN0b20vbW9kdWxlcy9zcF90aW1lc2hlZXRfdHJhY2tlci9sb2dpY19ob29rcy5waHAiO31pOjc1O2E6Mjp7czo0OiJmcm9tIjtzOjUxOiI8YmFzZXBhdGg+L2N1c3RvbS9tb2R1bGVzL1Byb2plY3QvcmVsQWNjb3VudENscy5waHAiO3M6MjoidG8iO3M6NDA6ImN1c3RvbS9tb2R1bGVzL1Byb2plY3QvcmVsQWNjb3VudENscy5waHAiO31pOjc2O2E6Mjp7czo0OiJmcm9tIjtzOjUzOiI8YmFzZXBhdGg+L2N1c3RvbS9tb2R1bGVzL1Byb2plY3Qvdmlld3Mvdmlldy5lZGl0LnBocCI7czoyOiJ0byI7czo0NjoiY3VzdG9tL21vZHVsZXMvUHJvamVjdC92aWV3cy92aWV3LmVkaXRfYmFrLnBocCI7fWk6Nzc7YToyOntzOjQ6ImZyb20iO3M6NTQ6IjxiYXNlcGF0aD4vY3VzdG9tL21vZHVsZXMvQWNjb3VudHMvdmlld3Mvdmlldy5lZGl0LnBocCI7czoyOiJ0byI7czo0NzoiY3VzdG9tL21vZHVsZXMvQWNjb3VudHMvdmlld3Mvdmlldy5lZGl0X2Jhay5waHAiO31pOjc4O2E6Mjp7czo0OiJmcm9tIjtzOjUxOiI8YmFzZXBhdGg+L2N1c3RvbS9tb2R1bGVzL1Byb2plY3QvZGlzcGxheUFjY0Nscy5waHAiO3M6MjoidG8iO3M6NDA6ImN1c3RvbS9tb2R1bGVzL1Byb2plY3QvZGlzcGxheUFjY0Nscy5waHAiO31pOjc5O2E6Mjp7czo0OiJmcm9tIjtzOjUyOiI8YmFzZXBhdGg+L2N1c3RvbS9tb2R1bGVzL0FjY291bnRzL3RvdGFsSG91cnNjbHMucGhwIjtzOjI6InRvIjtzOjQxOiJjdXN0b20vbW9kdWxlcy9BY2NvdW50cy90b3RhbEhvdXJzY2xzLnBocCI7fWk6ODA7YToyOntzOjQ6ImZyb20iO3M6NTc6IjxiYXNlcGF0aD4vY3VzdG9tL21vZHVsZXMvVXNlcnMvc2V0RGVmYXVsdFNldHRpbmdzQ2xzLnBocCI7czoyOiJ0byI7czo0NjoiY3VzdG9tL21vZHVsZXMvVXNlcnMvc2V0RGVmYXVsdFNldHRpbmdzQ2xzLnBocCI7fWk6ODE7YToyOntzOjQ6ImZyb20iO3M6NjY6IjxiYXNlcGF0aD4vY3VzdG9tL0V4dGVuc2lvbi9hcHBsaWNhdGlvbi9FeHQvVXRpbHMvZ2V0VXNlcnNMaXN0LnBocCI7czoyOiJ0byI7czo1NToiY3VzdG9tL0V4dGVuc2lvbi9hcHBsaWNhdGlvbi9FeHQvVXRpbHMvZ2V0VXNlcnNMaXN0LnBocCI7fWk6ODI7YToyOntzOjQ6ImZyb20iO3M6OTI6IjxiYXNlcGF0aD4vY3VzdG9tL0V4dGVuc2lvbi9tb2R1bGVzL1NjaGVkdWxlcnMvRXh0L0xhbmd1YWdlL2VuX3VzLmhvdXJzTWlzc2luZ09uUHJvamVjdHMucGhwIjtzOjI6InRvIjtzOjgxOiJjdXN0b20vRXh0ZW5zaW9uL21vZHVsZXMvU2NoZWR1bGVycy9FeHQvTGFuZ3VhZ2UvZW5fdXMuaG91cnNNaXNzaW5nT25Qcm9qZWN0cy5waHAiO31pOjgzO2E6Mjp7czo0OiJmcm9tIjtzOjkxOiI8YmFzZXBhdGg+L2N1c3RvbS9FeHRlbnNpb24vbW9kdWxlcy9TY2hlZHVsZXJzL0V4dC9TY2hlZHVsZWRUYXNrcy9ob3Vyc01pc3NpbmdPblByb2plY3QucGhwIjtzOjI6InRvIjtzOjgwOiJjdXN0b20vRXh0ZW5zaW9uL21vZHVsZXMvU2NoZWR1bGVycy9FeHQvU2NoZWR1bGVkVGFza3MvaG91cnNNaXNzaW5nT25Qcm9qZWN0LnBocCI7fWk6ODQ7YToyOntzOjQ6ImZyb20iO3M6OTk6IjxiYXNlcGF0aD4vY3VzdG9tL0V4dGVuc2lvbi9tb2R1bGVzL1VzZXJzL0V4dC9WYXJkZWZzL3N1Z2FyZmllbGRfdGltZXNoZWV0X2NvbmZpZ3VyYXRpb25fZmllbGRzLnBocCI7czoyOiJ0byI7czo4ODoiY3VzdG9tL0V4dGVuc2lvbi9tb2R1bGVzL1VzZXJzL0V4dC9WYXJkZWZzL3N1Z2FyZmllbGRfdGltZXNoZWV0X2NvbmZpZ3VyYXRpb25fZmllbGRzLnBocCI7fWk6ODU7YToyOntzOjQ6ImZyb20iO3M6NTg6IjxiYXNlcGF0aD4vY3VzdG9tL21vZHVsZXMvc3BfVGltZXNoZWV0L3ZpZXdzL3ZpZXcuZWRpdC5waHAiO3M6MjoidG8iO3M6NDc6ImN1c3RvbS9tb2R1bGVzL3NwX1RpbWVzaGVldC92aWV3cy92aWV3LmVkaXQucGhwIjt9aTo4NjthOjI6e3M6NDoiZnJvbSI7czo2MDoiPGJhc2VwYXRoPi9jdXN0b20vbW9kdWxlcy9zcF9UaW1lc2hlZXQvdmlld3Mvdmlldy5kZXRhaWwucGhwIjtzOjI6InRvIjtzOjQ5OiJjdXN0b20vbW9kdWxlcy9zcF9UaW1lc2hlZXQvdmlld3Mvdmlldy5kZXRhaWwucGhwIjt9aTo4NzthOjI6e3M6NDoiZnJvbSI7czo1MzoiPGJhc2VwYXRoPi9jdXN0b20vbW9kdWxlcy9Vc2Vycy92aWV3cy92aWV3LmRldGFpbC5waHAiO3M6MjoidG8iO3M6NDI6ImN1c3RvbS9tb2R1bGVzL1VzZXJzL3ZpZXdzL3ZpZXcuZGV0YWlsLnBocCI7fWk6ODg7YToyOntzOjQ6ImZyb20iO3M6NTE6IjxiYXNlcGF0aD4vY3VzdG9tL21vZHVsZXMvVXNlcnMvdmlld3Mvdmlldy5lZGl0LnBocCI7czoyOiJ0byI7czo0MDoiY3VzdG9tL21vZHVsZXMvVXNlcnMvdmlld3Mvdmlldy5lZGl0LnBocCI7fWk6ODk7YToyOntzOjQ6ImZyb20iO3M6NTU6IjxiYXNlcGF0aD4vY3VzdG9tL21vZHVsZXMvVXNlcnMvbGFuZ3VhZ2UvZW5fdXMubGFuZy5waHAiO3M6MjoidG8iO3M6NDQ6ImN1c3RvbS9tb2R1bGVzL1VzZXJzL2xhbmd1YWdlL2VuX3VzLmxhbmcucGhwIjt9aTo5MDthOjI6e3M6NDoiZnJvbSI7czo1OToiPGJhc2VwYXRoPi9jdXN0b20vbW9kdWxlcy9Vc2Vycy9tZXRhZGF0YS9kZXRhaWx2aWV3ZGVmcy5waHAiO3M6MjoidG8iO3M6NDg6ImN1c3RvbS9tb2R1bGVzL1VzZXJzL21ldGFkYXRhL2RldGFpbHZpZXdkZWZzLnBocCI7fWk6OTE7YToyOntzOjQ6ImZyb20iO3M6NTc6IjxiYXNlcGF0aD4vY3VzdG9tL21vZHVsZXMvVXNlcnMvbWV0YWRhdGEvZWRpdHZpZXdkZWZzLnBocCI7czoyOiJ0byI7czo0NjoiY3VzdG9tL21vZHVsZXMvVXNlcnMvbWV0YWRhdGEvZWRpdHZpZXdkZWZzLnBocCI7fWk6OTI7YToyOntzOjQ6ImZyb20iO3M6NTg6IjxiYXNlcGF0aD4vY3VzdG9tL21vZHVsZXMvVXNlcnMvc3BUaW1lc2hlZXRTZXR0aW5nSG9vay5waHAiO3M6MjoidG8iO3M6NDc6ImN1c3RvbS9tb2R1bGVzL1VzZXJzL3NwVGltZXNoZWV0U2V0dGluZ0hvb2sucGhwIjt9fXM6MTE6InByZV9leGVjdXRlIjthOjE6e2k6MDtzOjM0OiI8YmFzZXBhdGg+L3NjcmlwdHMvcHJlX2V4ZWN1dGUucGhwIjt9czoxNDoicG9zdF91bmluc3RhbGwiO2E6MTp7aTowO3M6Mzc6IjxiYXNlcGF0aD4vc2NyaXB0cy9wb3N0X3VuaW5zdGFsbC5waHAiO31zOjEyOiJwb3N0X2V4ZWN1dGUiO2E6MTp7aTowO3M6MzU6IjxiYXNlcGF0aD4vc2NyaXB0cy9wb3N0X2V4ZWN1dGUucGhwIjt9czo4OiJsYW5ndWFnZSI7YToyOntpOjA7YTozOntzOjQ6ImZyb20iO3M6NTk6IjxiYXNlcGF0aD4vU3VnYXJNb2R1bGVzL2xhbmd1YWdlL2FwcGxpY2F0aW9uL2VuX3VzLmxhbmcucGhwIjtzOjk6InRvX21vZHVsZSI7czoxMToiYXBwbGljYXRpb24iO3M6ODoibGFuZ3VhZ2UiO3M6NToiZW5fdXMiO31pOjE7YTozOntzOjQ6ImZyb20iO3M6NTg6IjxiYXNlcGF0aD4vbGljZW5zZV9hZG1pbi9sYW5ndWFnZS9lbl91cy5UaW1lc2hlZXROaW5qYS5waHAiO3M6OToidG9fbW9kdWxlIjtzOjE0OiJBZG1pbmlzdHJhdGlvbiI7czo4OiJsYW5ndWFnZSI7czo1OiJlbl91cyI7fX1zOjE0OiJhZG1pbmlzdHJhdGlvbiI7YToxOntpOjA7YToyOntzOjQ6ImZyb20iO3M6NTQ6IjxiYXNlcGF0aD4vbGljZW5zZV9hZG1pbi9tZW51L1RpbWVzaGVldE5pbmphX2FkbWluLnBocCI7czoyOiJ0byI7czo0NzoibW9kdWxlcy9BZG1pbmlzdHJhdGlvbi9UaW1lc2hlZXROaW5qYV9hZG1pbi5waHAiO319czoxNToiYWN0aW9uX3ZpZXdfbWFwIjthOjI6e2k6MDthOjI6e3M6NDoiZnJvbSI7czo3MToiPGJhc2VwYXRoPi9saWNlbnNlX2FkbWluL2FjdGlvbnZpZXdtYXAvVGltZXNoZWV0TmluamFfYWN0aW9udmlld21hcC5waHAiO3M6OToidG9fbW9kdWxlIjtzOjE0OiJUaW1lc2hlZXROaW5qYSI7fWk6MTthOjI6e3M6NDoiZnJvbSI7czo4OToiPGJhc2VwYXRoPi9TdWdhck1vZHVsZXMvbW9kdWxlcy9zcF9UaW1lc2hlZXQvQWN0aW9uVmlld01hcC9zcF90aW1lc2hlZXRfYWN0aW9udmlld21hcC5waHAiO3M6OToidG9fbW9kdWxlIjtzOjEyOiJzcF9UaW1lc2hlZXQiO319czoxMToibG9naWNfaG9va3MiO2E6MTA6e2k6MDthOjc6e3M6NjoibW9kdWxlIjtzOjExOiJQcm9qZWN0VGFzayI7czo0OiJob29rIjtzOjE0OiJhZnRlcl91aV9mcmFtZSI7czo1OiJvcmRlciI7aTo5OTtzOjExOiJkZXNjcmlwdGlvbiI7czowOiIiO3M6NDoiZmlsZSI7czo3NToiY3VzdG9tL21vZHVsZXMvUHJvamVjdFRhc2svcHJvamVjdFRhc2tMb2dpY0hvb2tJbXBsZW1lbnRhdGlvblRpbWVTaGVldHMucGhwIjtzOjU6ImNsYXNzIjtzOjQ0OiJwcm9qZWN0VGFza0xvZ2ljSG9va0ltcGxlbWVudGF0aW9uVGltZVNoZWV0cyI7czo4OiJmdW5jdGlvbiI7czoyMToiYWZ0ZXJfdWlfZnJhbWVfbWV0aG9kIjt9aToxO2E6Nzp7czo2OiJtb2R1bGUiO3M6NToiQ2FzZXMiO3M6NDoiaG9vayI7czoxNDoiYWZ0ZXJfdWlfZnJhbWUiO3M6NToib3JkZXIiO2k6OTk7czoxMToiZGVzY3JpcHRpb24iO3M6MDoiIjtzOjQ6ImZpbGUiO3M6NTI6ImN1c3RvbS9tb2R1bGVzL0Nhc2VzL2Nhc2VMb2dpY0hvb2tJbXBsZW1lbnRhdGlvbi5waHAiO3M6NToiY2xhc3MiO3M6Mjc6ImNhc2VMb2dpY0hvb2tJbXBsZW1lbnRhdGlvbiI7czo4OiJmdW5jdGlvbiI7czoyMToiYWZ0ZXJfdWlfZnJhbWVfbWV0aG9kIjt9aToyO2E6Nzp7czo2OiJtb2R1bGUiO3M6MzA6InNwX0F1dG9fVGltZVNoZWV0X1RpbWVfVHJhY2tlciI7czo0OiJob29rIjtzOjExOiJiZWZvcmVfc2F2ZSI7czo1OiJvcmRlciI7aTo5OTtzOjExOiJkZXNjcmlwdGlvbiI7czoxNjoidXBkYXRlIHRpbWVzaGVldCI7czo0OiJmaWxlIjtzOjcyOiJjdXN0b20vbW9kdWxlcy9zcF9BdXRvX1RpbWVTaGVldF9UaW1lX1RyYWNrZXIvSG9va3MvdXBkYXRlX3RpbWVzaGVldC5waHAiO3M6NToiY2xhc3MiO3M6MTU6IlVwZGF0ZVRpbWVzaGVldCI7czo4OiJmdW5jdGlvbiI7czoyMzoidXBkYXRlX3RpbWVzaGVldF9tZXRob2QiO31pOjM7YTo3OntzOjY6Im1vZHVsZSI7czo1OiJVc2VycyI7czo0OiJob29rIjtzOjExOiJhZnRlcl9sb2dpbiI7czo1OiJvcmRlciI7aTo5OTtzOjExOiJkZXNjcmlwdGlvbiI7czozMDoidXBkYXRlIHRpbWVzaGVldCBkZWZhdWx0IHF1ZXJ5IjtzOjQ6ImZpbGUiO3M6NTg6ImN1c3RvbS9tb2R1bGVzL3NwX3RpbWVzaGVldF90cmFja2VyL3VwZFRpbWVzaGVldFFyeUNscy5waHAiO3M6NToiY2xhc3MiO3M6MTg6InVwZFRpbWVzaGVldFFyeUNscyI7czo4OiJmdW5jdGlvbiI7czoxOToidXBkVGltZXNoZWV0UXJ5RnVuYyI7fWk6NDthOjc6e3M6NjoibW9kdWxlIjtzOjc6IlByb2plY3QiO3M6NDoiaG9vayI7czoxMDoiYWZ0ZXJfc2F2ZSI7czo1OiJvcmRlciI7aToxMDI7czoxMToiZGVzY3JpcHRpb24iO3M6MzA6ImFkZCByZWxhdGlvbnNoaXAgd2l0aCBhY2NvdW50cyI7czo0OiJmaWxlIjtzOjQwOiJjdXN0b20vbW9kdWxlcy9Qcm9qZWN0L3JlbEFjY291bnRDbHMucGhwIjtzOjU6ImNsYXNzIjtzOjEzOiJyZWxBY2NvdW50Q2xzIjtzOjg6ImZ1bmN0aW9uIjtzOjE0OiJyZWxBY2NvdW50RnVuYyI7fWk6NTthOjc6e3M6NjoibW9kdWxlIjtzOjA6IiI7czo0OiJob29rIjtzOjE1OiJhZnRlcl91aV9mb290ZXIiO3M6NToib3JkZXIiO2k6MTAxO3M6MTE6ImRlc2NyaXB0aW9uIjtzOjQxOiJkaXNwbGF5IHN1bSBvZiBob3VycyBpbiB0aW1lc2hlZXQgdHJhY2tlciI7czo0OiJmaWxlIjtzOjQ4OiJjdXN0b20vbW9kdWxlcy9zcF90aW1lc2hlZXRfdHJhY2tlci9ob29rX3N1bS5waHAiO3M6NToiY2xhc3MiO3M6ODoiSG9va19zdW0iO3M6ODoiZnVuY3Rpb24iO3M6MTE6ImRpc3BsYXlfc3VtIjt9aTo2O2E6Nzp7czo2OiJtb2R1bGUiO3M6NzoiUHJvamVjdCI7czo0OiJob29rIjtzOjE0OiJhZnRlcl91aV9mcmFtZSI7czo1OiJvcmRlciI7aToxMjE7czoxMToiZGVzY3JpcHRpb24iO3M6MzM6IkFjY291bnQgZmllbGQgZGlzcGxheSBvbiBlZGl0dmlldyI7czo0OiJmaWxlIjtzOjQwOiJjdXN0b20vbW9kdWxlcy9Qcm9qZWN0L2Rpc3BsYXlBY2NDbHMucGhwIjtzOjU6ImNsYXNzIjtzOjEzOiJkaXNwbGF5QWNjQ2xzIjtzOjg6ImZ1bmN0aW9uIjtzOjE0OiJkaXNwbGF5QWNjRnVuYyI7fWk6NzthOjc6e3M6NjoibW9kdWxlIjtzOjg6IkFjY291bnRzIjtzOjQ6Imhvb2siO3M6MTQ6ImFmdGVyX3VpX2ZyYW1lIjtzOjU6Im9yZGVyIjtpOjE0NztzOjExOiJkZXNjcmlwdGlvbiI7czozOToiVG90YWwgaG91cnMgZGlzYWJsZWQgb24gYWNjb3VudHMgbW9kdWxlIjtzOjQ6ImZpbGUiO3M6NDE6ImN1c3RvbS9tb2R1bGVzL0FjY291bnRzL3RvdGFsSG91cnNjbHMucGhwIjtzOjU6ImNsYXNzIjtzOjEzOiJ0b3RhbEhvdXJzY2xzIjtzOjg6ImZ1bmN0aW9uIjtzOjEzOiJ0b3RhbEhvdXJGdW5jIjt9aTo4O2E6Nzp7czo2OiJtb2R1bGUiO3M6NToiVXNlcnMiO3M6NDoiaG9vayI7czoxMToiYWZ0ZXJfbG9naW4iO3M6NToib3JkZXIiO2k6MTAxO3M6MTE6ImRlc2NyaXB0aW9uIjtzOjU0OiJTZXR0aW5nIGRlZmF1bHQgdGltZXNoZWV0IHNldHRpbmdzIGZvciB1c2VyIGlmIG5vdCBzZXQiO3M6NDoiZmlsZSI7czo0NjoiY3VzdG9tL21vZHVsZXMvVXNlcnMvc2V0RGVmYXVsdFNldHRpbmdzQ2xzLnBocCI7czo1OiJjbGFzcyI7czoyMToic2V0RGVmYXVsdFNldHRpbmdzQ2xzIjtzOjg6ImZ1bmN0aW9uIjtzOjIyOiJzZXREZWZhdWx0U2V0dGluZ3NGdW5jIjt9aTo5O2E6Nzp7czo2OiJtb2R1bGUiO3M6NToiVXNlcnMiO3M6NDoiaG9vayI7czoxMDoiYWZ0ZXJfc2F2ZSI7czo1OiJvcmRlciI7aToxMDE7czoxMToiZGVzY3JpcHRpb24iO3M6MzA6IlVwZGF0ZSBjb25maWcgZm9yIHNwX3RpbWVzaGVldCI7czo0OiJmaWxlIjtzOjQ3OiJjdXN0b20vbW9kdWxlcy9Vc2Vycy9zcFRpbWVzaGVldFNldHRpbmdIb29rLnBocCI7czo1OiJjbGFzcyI7czoxODoic3BUaW1lc2hlZXRTZXR0aW5nIjtzOjg6ImZ1bmN0aW9uIjtzOjE5OiJ1cGRhdGVDb25maWdFbnRyaWVzIjt9fX1zOjE2OiJ1cGdyYWRlX21hbmlmZXN0IjtzOjA6IiI7fQ==','2020-04-10 16:25:28',1);
/*!40000 ALTER TABLE `upgrade_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_preferences`
--

DROP TABLE IF EXISTS `user_preferences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_preferences` (
  `id` char(36) NOT NULL,
  `category` varchar(50) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `assigned_user_id` char(36) DEFAULT NULL,
  `contents` longtext,
  PRIMARY KEY (`id`),
  KEY `idx_userprefnamecat` (`assigned_user_id`,`category`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_preferences`
--

LOCK TABLES `user_preferences` WRITE;
/*!40000 ALTER TABLE `user_preferences` DISABLE KEYS */;
INSERT INTO `user_preferences` VALUES ('1029903a-0adb-e161-5b11-5e90b9b278ba','Home2_LEAD_2f1323e5-af55-aa99-300b-5e90b96113d5',0,'2020-04-10 18:24:59','2020-04-10 18:24:59','dc26b995-5550-b848-6f73-5e90aa679348','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),('1402001a-4e55-10b3-9d42-5e90b7ce62a3','Home2_LEAD_a7b02f3c-1ffa-3903-40fd-5e90b7d9aa7c',0,'2020-04-10 18:14:36','2020-04-10 18:22:21','dc26b995-5550-b848-6f73-5e90aa679348','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),('153e0fea-8779-4b3a-f3b8-5f4e4e74414a','AOS_Quotes2_AOS_QUOTES',0,'2020-09-01 13:35:56','2020-09-01 13:35:56','1','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),('15ce5e46-abbb-5645-19a8-5e90b9e52f85','ProjectTask',0,'2020-04-10 18:22:20','2020-04-10 18:22:21','dc26b995-5550-b848-6f73-5e90aa679348','YTowOnt9'),('1a03871e-1317-1aa6-0441-5f4698739c02','Emails',0,'2020-08-26 17:13:20','2020-08-28 10:06:01','b70243be-ca4c-b256-aff8-5f46980acf9f','YTowOnt9'),('20598759-bea4-5b20-f01f-5f46980a5b47','global',0,'2020-08-26 17:13:48','2020-08-28 16:56:43','1674d634-fb3a-8d8b-34c1-5f4698fed125','YTo0Mzp7czoyMDoiY2FsZW5kYXJfcHVibGlzaF9rZXkiO3M6MDoiIjtzOjEyOiJtYWlsbWVyZ2Vfb24iO3M6Mjoib24iO3M6MTY6InN3YXBfbGFzdF92aWV3ZWQiO3M6MDoiIjtzOjE0OiJzd2FwX3Nob3J0Y3V0cyI7czowOiIiO3M6MTk6Im5hdmlnYXRpb25fcGFyYWRpZ20iO3M6MjoiZ20iO3M6MjA6InNvcnRfbW9kdWxlc19ieV9uYW1lIjtzOjA6IiI7czoxMzoic3VicGFuZWxfdGFicyI7czoyOiJvbiI7czoyNToiY291bnRfY29sbGFwc2VkX3N1YnBhbmVscyI7czowOiIiO3M6MTQ6Im1vZHVsZV9mYXZpY29uIjtzOjA6IiI7czo5OiJoaWRlX3RhYnMiO2E6MDp7fXM6MTE6InJlbW92ZV90YWJzIjthOjA6e31zOjc6Im5vX29wcHMiO3M6Mzoib2ZmIjtzOjEzOiJyZW1pbmRlcl90aW1lIjtzOjQ6IjE4MDAiO3M6MTk6ImVtYWlsX3JlbWluZGVyX3RpbWUiO3M6NDoiMzYwMCI7czoxNjoicmVtaW5kZXJfY2hlY2tlZCI7czoxOiIwIjtzOjIyOiJlbWFpbF9yZW1pbmRlcl9jaGVja2VkIjtzOjE6IjAiO3M6ODoidGltZXpvbmUiO3M6MTI6IkFzaWEvQ29sb21ibyI7czoyOiJ1dCI7czoxOiIxIjtzOjg6ImN1cnJlbmN5IjtzOjM6Ii05OSI7czozNToiZGVmYXVsdF9jdXJyZW5jeV9zaWduaWZpY2FudF9kaWdpdHMiO3M6MToiMiI7czoxMToibnVtX2dycF9zZXAiO3M6MToiLCI7czo3OiJkZWNfc2VwIjtzOjE6Ii4iO3M6NDoiZmRvdyI7czoxOiIwIjtzOjU6ImRhdGVmIjtzOjU6Im0vZC9ZIjtzOjU6InRpbWVmIjtzOjQ6Img6aWEiO3M6MjY6ImRlZmF1bHRfbG9jYWxlX25hbWVfZm9ybWF0IjtzOjU6InMgZiBsIjtzOjE2OiJleHBvcnRfZGVsaW1pdGVyIjtzOjE6IiwiO3M6MjI6ImRlZmF1bHRfZXhwb3J0X2NoYXJzZXQiO3M6NToiVVRGLTgiO3M6MTQ6InVzZV9yZWFsX25hbWVzIjtzOjI6Im9uIjtzOjE3OiJtYWlsX3NtdHBhdXRoX3JlcSI7czowOiIiO3M6MTI6Im1haWxfc210cHNzbCI7aTowO3M6MTU6ImVtYWlsX2xpbmtfdHlwZSI7czo1OiJzdWdhciI7czoxMToiZWRpdG9yX3R5cGUiO3M6NjoibW96YWlrIjtzOjE3OiJlbWFpbF9zaG93X2NvdW50cyI7aTowO3M6ODoic3VidGhlbWUiO3M6NDoiRGF3biI7czoxNToibG9naW5leHBpcmF0aW9uIjtzOjE6IjAiO3M6NzoibG9ja291dCI7czowOiIiO3M6MTE6ImxvZ2luZmFpbGVkIjtzOjE6IjAiO3M6MjE6InNwX3RpbWVzaGVldF90cmFja2VyUSI7YTo2OntzOjE4OiJ1c2VyX2lkX2NfYWR2YW5jZWQiO3M6MzY6IjE2NzRkNjM0LWZiM2EtOGQ4Yi0zNGMxLTVmNDY5OGZlZDEyNSI7czoxOToidHJhY2tfdXNlcl9hZHZhbmNlZCI7czo2OiJSaWNrIE0iO3M6NjoibW9kdWxlIjtzOjIwOiJzcF90aW1lc2hlZXRfdHJhY2tlciI7czoxMzoic2VhcmNoRm9ybVRhYiI7czoxNToiYWR2YW5jZWRfc2VhcmNoIjtzOjU6InF1ZXJ5IjtzOjQ6InRydWUiO3M6NjoiYWN0aW9uIjtzOjU6ImluZGV4Ijt9czoxMDoidXNlcl90aGVtZSI7czo2OiJTdWl0ZVAiO3M6MTk6InRoZW1lX2N1cnJlbnRfZ3JvdXAiO3M6MzoiQWxsIjtzOjk6IkFjY291bnRzUSI7YToxOntzOjEzOiJzZWFyY2hGb3JtVGFiIjtzOjEyOiJiYXNpY19zZWFyY2giO31zOjY6IlRhc2tzUSI7YToxOntzOjEzOiJzZWFyY2hGb3JtVGFiIjtzOjEyOiJiYXNpY19zZWFyY2giO319'),('20d23889-d9b3-6cc6-9e53-5f44f8efe60a','Home2_BUG',0,'2020-08-25 11:38:54','2020-08-25 11:38:54','1','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),('217a6f62-09e0-0f3a-40a7-5e90b727cc47','Project2_PROJECT',0,'2020-04-10 18:15:50','2020-04-10 18:22:21','dc26b995-5550-b848-6f73-5e90aa679348','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),('22b42fee-49c2-1bc3-d9a5-5f469833e44e','GoogleSync',0,'2020-08-26 17:13:48','2020-08-28 16:56:43','1674d634-fb3a-8d8b-34c1-5f4698fed125','YToxOntzOjg6InN5bmNHQ2FsIjtpOjA7fQ=='),('2dbf5f9c-cb25-0560-8772-5f3becbc6a43','Contacts2_CONTACT',0,'2020-08-18 14:59:08','2020-08-18 14:59:08','1','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),('2dc95617-afcb-12a5-b487-5e90b8704401','sp_Timesheet2_SP_TIMESHEET',0,'2020-04-10 18:17:06','2020-04-10 18:22:21','dc26b995-5550-b848-6f73-5e90aa679348','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),('2edbf189-02b0-78e5-85dd-5f48d5043901','Accounts2_ACCOUNT',0,'2020-08-28 09:58:31','2020-08-28 09:58:31','1674d634-fb3a-8d8b-34c1-5f4698fed125','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),('2f4004fb-b727-1c96-453c-5f4e4ed97c90','AOS_Products2_AOS_PRODUCTS',0,'2020-09-01 13:35:43','2020-09-01 13:35:43','1','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),('2f834601-79d9-4fc7-83bb-5f469fee2a99','Home',0,'2020-08-26 17:45:25','2020-08-26 17:45:25','8d544213-bcac-b962-fc13-5f4698c75700','YToyOntzOjg6ImRhc2hsZXRzIjthOjY6e3M6MzY6IjIyMzM1MjBmLTA0Y2MtYzY0Yy01NzMyLTVmNDY5ZjJhM2JiZCI7YTo0OntzOjk6ImNsYXNzTmFtZSI7czoxNjoiU3VnYXJGZWVkRGFzaGxldCI7czo2OiJtb2R1bGUiO3M6OToiU3VnYXJGZWVkIjtzOjExOiJmb3JjZUNvbHVtbiI7aToxO3M6MTI6ImZpbGVMb2NhdGlvbiI7czo2NDoibW9kdWxlcy9TdWdhckZlZWQvRGFzaGxldHMvU3VnYXJGZWVkRGFzaGxldC9TdWdhckZlZWREYXNobGV0LnBocCI7fXM6MzY6IjIyNDNiYWYzLTUyZmItOWQ2MS05YTNmLTVmNDY5Zjg1ZWIwYyI7YTo1OntzOjk6ImNsYXNzTmFtZSI7czoxNDoiTXlDYWxsc0Rhc2hsZXQiO3M6NjoibW9kdWxlIjtzOjU6IkNhbGxzIjtzOjExOiJmb3JjZUNvbHVtbiI7aTowO3M6MTI6ImZpbGVMb2NhdGlvbiI7czo1NjoibW9kdWxlcy9DYWxscy9EYXNobGV0cy9NeUNhbGxzRGFzaGxldC9NeUNhbGxzRGFzaGxldC5waHAiO3M6Nzoib3B0aW9ucyI7YTowOnt9fXM6MzY6IjIyNTIzZTg3LTg3NjktMDg3Ny0zYmU3LTVmNDY5ZjhmZGNiMiI7YTo1OntzOjk6ImNsYXNzTmFtZSI7czoxNzoiTXlNZWV0aW5nc0Rhc2hsZXQiO3M6NjoibW9kdWxlIjtzOjg6Ik1lZXRpbmdzIjtzOjExOiJmb3JjZUNvbHVtbiI7aTowO3M6MTI6ImZpbGVMb2NhdGlvbiI7czo2NToibW9kdWxlcy9NZWV0aW5ncy9EYXNobGV0cy9NeU1lZXRpbmdzRGFzaGxldC9NeU1lZXRpbmdzRGFzaGxldC5waHAiO3M6Nzoib3B0aW9ucyI7YTowOnt9fXM6MzY6IjIyNjA2MTQwLWJjNjQtN2E2NC04ZjRjLTVmNDY5ZmUwOGJmOCI7YTo1OntzOjk6ImNsYXNzTmFtZSI7czoyMjoiTXlPcHBvcnR1bml0aWVzRGFzaGxldCI7czo2OiJtb2R1bGUiO3M6MTM6Ik9wcG9ydHVuaXRpZXMiO3M6MTE6ImZvcmNlQ29sdW1uIjtpOjA7czoxMjoiZmlsZUxvY2F0aW9uIjtzOjgwOiJtb2R1bGVzL09wcG9ydHVuaXRpZXMvRGFzaGxldHMvTXlPcHBvcnR1bml0aWVzRGFzaGxldC9NeU9wcG9ydHVuaXRpZXNEYXNobGV0LnBocCI7czo3OiJvcHRpb25zIjthOjA6e319czozNjoiMjI3ODg1ODctNzFiZS1jMTJkLWVmYWYtNWY0NjlmNThkZmY2IjthOjU6e3M6OToiY2xhc3NOYW1lIjtzOjE3OiJNeUFjY291bnRzRGFzaGxldCI7czo2OiJtb2R1bGUiO3M6ODoiQWNjb3VudHMiO3M6MTE6ImZvcmNlQ29sdW1uIjtpOjA7czoxMjoiZmlsZUxvY2F0aW9uIjtzOjY1OiJtb2R1bGVzL0FjY291bnRzL0Rhc2hsZXRzL015QWNjb3VudHNEYXNobGV0L015QWNjb3VudHNEYXNobGV0LnBocCI7czo3OiJvcHRpb25zIjthOjA6e319czozNjoiMjI4NTg3ZDctNzRkZS0yYjc0LTQwNzEtNWY0NjlmNTdkMGVlIjthOjU6e3M6OToiY2xhc3NOYW1lIjtzOjE0OiJNeUxlYWRzRGFzaGxldCI7czo2OiJtb2R1bGUiO3M6NToiTGVhZHMiO3M6MTE6ImZvcmNlQ29sdW1uIjtpOjA7czoxMjoiZmlsZUxvY2F0aW9uIjtzOjU2OiJtb2R1bGVzL0xlYWRzL0Rhc2hsZXRzL015TGVhZHNEYXNobGV0L015TGVhZHNEYXNobGV0LnBocCI7czo3OiJvcHRpb25zIjthOjA6e319fXM6NToicGFnZXMiO2E6MTp7aTowO2E6Mzp7czo3OiJjb2x1bW5zIjthOjI6e2k6MDthOjI6e3M6NToid2lkdGgiO3M6MzoiNjAlIjtzOjg6ImRhc2hsZXRzIjthOjU6e2k6MDtzOjM2OiIyMjQzYmFmMy01MmZiLTlkNjEtOWEzZi01ZjQ2OWY4NWViMGMiO2k6MTtzOjM2OiIyMjUyM2U4Ny04NzY5LTA4NzctM2JlNy01ZjQ2OWY4ZmRjYjIiO2k6MjtzOjM2OiIyMjYwNjE0MC1iYzY0LTdhNjQtOGY0Yy01ZjQ2OWZlMDhiZjgiO2k6MztzOjM2OiIyMjc4ODU4Ny03MWJlLWMxMmQtZWZhZi01ZjQ2OWY1OGRmZjYiO2k6NDtzOjM2OiIyMjg1ODdkNy03NGRlLTJiNzQtNDA3MS01ZjQ2OWY1N2QwZWUiO319aToxO2E6Mjp7czo1OiJ3aWR0aCI7czozOiI0MCUiO3M6ODoiZGFzaGxldHMiO2E6MTp7aTowO3M6MzY6IjIyMzM1MjBmLTA0Y2MtYzY0Yy01NzMyLTVmNDY5ZjJhM2JiZCI7fX19czoxMDoibnVtQ29sdW1ucyI7czoxOiIzIjtzOjE0OiJwYWdlVGl0bGVMYWJlbCI7czoyMDoiTEJMX0hPTUVfUEFHRV8xX05BTUUiO319fQ=='),('32438744-368b-38ac-906e-5f469f7f5cc2','Home2_CALL',0,'2020-08-26 17:45:25','2020-08-26 17:45:25','8d544213-bcac-b962-fc13-5f4698c75700','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),('3517f276-ee52-7080-e3d6-5f469f6cefa3','Home2_MEETING',0,'2020-08-26 17:45:25','2020-08-26 17:45:25','8d544213-bcac-b962-fc13-5f4698c75700','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),('35624454-f309-a73a-e2dd-5f48d46c6408','Home',0,'2020-08-28 09:53:13','2020-08-28 10:06:01','b70243be-ca4c-b256-aff8-5f46980acf9f','YToyOntzOjg6ImRhc2hsZXRzIjthOjY6e3M6MzY6IjI3OGQ2MzM2LTQ3ODUtNTBjYS0yN2Y3LTVmNDhkNGU1N2Y5MyI7YTo0OntzOjk6ImNsYXNzTmFtZSI7czoxNjoiU3VnYXJGZWVkRGFzaGxldCI7czo2OiJtb2R1bGUiO3M6OToiU3VnYXJGZWVkIjtzOjExOiJmb3JjZUNvbHVtbiI7aToxO3M6MTI6ImZpbGVMb2NhdGlvbiI7czo2NDoibW9kdWxlcy9TdWdhckZlZWQvRGFzaGxldHMvU3VnYXJGZWVkRGFzaGxldC9TdWdhckZlZWREYXNobGV0LnBocCI7fXM6MzY6IjI3OWM4ZTRhLWM1MGUtM2FmOC1kMWQ0LTVmNDhkNGEyYjQzYiI7YTo1OntzOjk6ImNsYXNzTmFtZSI7czoxNDoiTXlDYWxsc0Rhc2hsZXQiO3M6NjoibW9kdWxlIjtzOjU6IkNhbGxzIjtzOjExOiJmb3JjZUNvbHVtbiI7aTowO3M6MTI6ImZpbGVMb2NhdGlvbiI7czo1NjoibW9kdWxlcy9DYWxscy9EYXNobGV0cy9NeUNhbGxzRGFzaGxldC9NeUNhbGxzRGFzaGxldC5waHAiO3M6Nzoib3B0aW9ucyI7YTowOnt9fXM6MzY6IjI3YWEwZjhiLWQ1YTUtYmIzMy1hZjc1LTVmNDhkNDliZWJiZCI7YTo1OntzOjk6ImNsYXNzTmFtZSI7czoxNzoiTXlNZWV0aW5nc0Rhc2hsZXQiO3M6NjoibW9kdWxlIjtzOjg6Ik1lZXRpbmdzIjtzOjExOiJmb3JjZUNvbHVtbiI7aTowO3M6MTI6ImZpbGVMb2NhdGlvbiI7czo2NToibW9kdWxlcy9NZWV0aW5ncy9EYXNobGV0cy9NeU1lZXRpbmdzRGFzaGxldC9NeU1lZXRpbmdzRGFzaGxldC5waHAiO3M6Nzoib3B0aW9ucyI7YTowOnt9fXM6MzY6IjI3YjNjMjI0LTdkNzctMjczMS0xMzU5LTVmNDhkNDVlMTQ3ZSI7YTo1OntzOjk6ImNsYXNzTmFtZSI7czoyMjoiTXlPcHBvcnR1bml0aWVzRGFzaGxldCI7czo2OiJtb2R1bGUiO3M6MTM6Ik9wcG9ydHVuaXRpZXMiO3M6MTE6ImZvcmNlQ29sdW1uIjtpOjA7czoxMjoiZmlsZUxvY2F0aW9uIjtzOjgwOiJtb2R1bGVzL09wcG9ydHVuaXRpZXMvRGFzaGxldHMvTXlPcHBvcnR1bml0aWVzRGFzaGxldC9NeU9wcG9ydHVuaXRpZXNEYXNobGV0LnBocCI7czo3OiJvcHRpb25zIjthOjA6e319czozNjoiMjdjMTYxMjYtMjkwNC1kM2Y1LTlhZWItNWY0OGQ0MmY2OTlmIjthOjU6e3M6OToiY2xhc3NOYW1lIjtzOjE3OiJNeUFjY291bnRzRGFzaGxldCI7czo2OiJtb2R1bGUiO3M6ODoiQWNjb3VudHMiO3M6MTE6ImZvcmNlQ29sdW1uIjtpOjA7czoxMjoiZmlsZUxvY2F0aW9uIjtzOjY1OiJtb2R1bGVzL0FjY291bnRzL0Rhc2hsZXRzL015QWNjb3VudHNEYXNobGV0L015QWNjb3VudHNEYXNobGV0LnBocCI7czo3OiJvcHRpb25zIjthOjA6e319czozNjoiMjdjZTEwYjMtOWU3Ny05ZWUwLWM2NTItNWY0OGQ0ZDI4YWNiIjthOjU6e3M6OToiY2xhc3NOYW1lIjtzOjE0OiJNeUxlYWRzRGFzaGxldCI7czo2OiJtb2R1bGUiO3M6NToiTGVhZHMiO3M6MTE6ImZvcmNlQ29sdW1uIjtpOjA7czoxMjoiZmlsZUxvY2F0aW9uIjtzOjU2OiJtb2R1bGVzL0xlYWRzL0Rhc2hsZXRzL015TGVhZHNEYXNobGV0L015TGVhZHNEYXNobGV0LnBocCI7czo3OiJvcHRpb25zIjthOjA6e319fXM6NToicGFnZXMiO2E6MTp7aTowO2E6Mzp7czo3OiJjb2x1bW5zIjthOjI6e2k6MDthOjI6e3M6NToid2lkdGgiO3M6MzoiNjAlIjtzOjg6ImRhc2hsZXRzIjthOjU6e2k6MDtzOjM2OiIyNzljOGU0YS1jNTBlLTNhZjgtZDFkNC01ZjQ4ZDRhMmI0M2IiO2k6MTtzOjM2OiIyN2FhMGY4Yi1kNWE1LWJiMzMtYWY3NS01ZjQ4ZDQ5YmViYmQiO2k6MjtzOjM2OiIyN2IzYzIyNC03ZDc3LTI3MzEtMTM1OS01ZjQ4ZDQ1ZTE0N2UiO2k6MztzOjM2OiIyN2MxNjEyNi0yOTA0LWQzZjUtOWFlYi01ZjQ4ZDQyZjY5OWYiO2k6NDtzOjM2OiIyN2NlMTBiMy05ZTc3LTllZTAtYzY1Mi01ZjQ4ZDRkMjhhY2IiO319aToxO2E6Mjp7czo1OiJ3aWR0aCI7czozOiI0MCUiO3M6ODoiZGFzaGxldHMiO2E6MTp7aTowO3M6MzY6IjI3OGQ2MzM2LTQ3ODUtNTBjYS0yN2Y3LTVmNDhkNGU1N2Y5MyI7fX19czoxMDoibnVtQ29sdW1ucyI7czoxOiIzIjtzOjE0OiJwYWdlVGl0bGVMYWJlbCI7czoyMDoiTEJMX0hPTUVfUEFHRV8xX05BTUUiO319fQ=='),('36d09e1b-9743-9062-dabb-5c6e1d673b24','Home2_LEAD_693012ca-7758-e54c-ec76-5c6e1df36f82',0,'2019-02-21 03:40:07','2019-02-21 03:40:07','1','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),('376b4e4e-f67e-9213-f1f0-5f469f16e2b3','Home2_OPPORTUNITY',0,'2020-08-26 17:45:25','2020-08-26 17:45:25','8d544213-bcac-b962-fc13-5f4698c75700','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),('382af83c-a58b-2b37-7676-5f48d415d4b7','Home2_CALL',0,'2020-08-28 09:53:13','2020-08-28 10:06:01','b70243be-ca4c-b256-aff8-5f46980acf9f','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),('38cdd229-eb04-b8d8-d8e2-5f48d3ef0b75','Emails',0,'2020-08-28 09:50:31','2020-09-01 16:58:45','8d544213-bcac-b962-fc13-5f4698c75700','YTowOnt9'),('3a3cefbb-9113-7319-061e-5f48d4495235','Home2_MEETING',0,'2020-08-28 09:53:13','2020-08-28 10:06:01','b70243be-ca4c-b256-aff8-5f46980acf9f','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),('3a46c6f3-4afb-3fb6-1bb6-5f469ffa3864','Home2_ACCOUNT',0,'2020-08-26 17:45:25','2020-08-26 17:45:25','8d544213-bcac-b962-fc13-5f4698c75700','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),('3c7d9e7a-f822-996f-4772-5f469f87847c','Home2_LEAD',0,'2020-08-26 17:45:25','2020-08-26 17:45:25','8d544213-bcac-b962-fc13-5f4698c75700','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),('3c89c357-e380-de57-0fba-5f48d4b41d60','Home2_OPPORTUNITY',0,'2020-08-28 09:53:13','2020-08-28 10:06:01','b70243be-ca4c-b256-aff8-5f46980acf9f','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),('3ee16991-cabf-d4b4-8197-5f48d45b7681','Home2_ACCOUNT',0,'2020-08-28 09:53:13','2020-08-28 10:06:01','b70243be-ca4c-b256-aff8-5f46980acf9f','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),('3f6d74f0-8d2a-ad42-9faf-5f469f633a63','Home2_SUGARFEED',0,'2020-08-26 17:45:25','2020-08-26 17:45:25','8d544213-bcac-b962-fc13-5f4698c75700','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),('41763fe5-4016-25d5-9552-5f48d474496f','Home2_LEAD',0,'2020-08-28 09:53:13','2020-08-28 10:06:01','b70243be-ca4c-b256-aff8-5f46980acf9f','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),('43a4aebb-9b1c-c6d0-b5b3-5f48d45eedb0','Home2_SUGARFEED',0,'2020-08-28 09:53:13','2020-08-28 10:06:01','b70243be-ca4c-b256-aff8-5f46980acf9f','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),('4826fcc6-55d8-bded-6491-5f45583ec1fd','FP_events2_FP_EVENTS',0,'2020-08-25 18:30:24','2020-08-25 18:30:24','1','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),('4e0f2fc0-6ad8-c8ec-b6b1-5e90b7d5e01e','Emails',0,'2020-04-10 18:12:56','2020-04-10 18:49:57','dc26b995-5550-b848-6f73-5e90aa679348','YTowOnt9'),('4f7c7c6a-1d72-e9ca-ab31-5f47ad35b284','Dashboard',0,'2020-08-27 12:57:23','2020-08-28 09:13:41','1','YTowOnt9'),('55aad71c-168f-5563-1cf4-5f3fadc18a0b','Campaigns2_CAMPAIGN',0,'2020-08-21 11:19:09','2020-08-21 11:19:09','1','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),('5b01a4df-9561-4e2e-b577-5f48d548738c','Home',0,'2020-08-28 09:58:24','2020-08-28 09:58:24','1674d634-fb3a-8d8b-34c1-5f4698fed125','YToyOntzOjg6ImRhc2hsZXRzIjthOjY6e3M6MzY6IjUwYWJmNGI1LTNhOGItODkwOC03OGQwLTVmNDhkNWQxN2I0YiI7YTo0OntzOjk6ImNsYXNzTmFtZSI7czoxNjoiU3VnYXJGZWVkRGFzaGxldCI7czo2OiJtb2R1bGUiO3M6OToiU3VnYXJGZWVkIjtzOjExOiJmb3JjZUNvbHVtbiI7aToxO3M6MTI6ImZpbGVMb2NhdGlvbiI7czo2NDoibW9kdWxlcy9TdWdhckZlZWQvRGFzaGxldHMvU3VnYXJGZWVkRGFzaGxldC9TdWdhckZlZWREYXNobGV0LnBocCI7fXM6MzY6IjUwYmFmOGJkLTQ3MjYtNTQ5ZS0xMzdmLTVmNDhkNTk5MGE2YSI7YTo1OntzOjk6ImNsYXNzTmFtZSI7czoxNDoiTXlDYWxsc0Rhc2hsZXQiO3M6NjoibW9kdWxlIjtzOjU6IkNhbGxzIjtzOjExOiJmb3JjZUNvbHVtbiI7aTowO3M6MTI6ImZpbGVMb2NhdGlvbiI7czo1NjoibW9kdWxlcy9DYWxscy9EYXNobGV0cy9NeUNhbGxzRGFzaGxldC9NeUNhbGxzRGFzaGxldC5waHAiO3M6Nzoib3B0aW9ucyI7YTowOnt9fXM6MzY6IjUwYzY0ZGE1LWQzNDUtODcyZi03ZTk1LTVmNDhkNTk1YTk5NyI7YTo1OntzOjk6ImNsYXNzTmFtZSI7czoxNzoiTXlNZWV0aW5nc0Rhc2hsZXQiO3M6NjoibW9kdWxlIjtzOjg6Ik1lZXRpbmdzIjtzOjExOiJmb3JjZUNvbHVtbiI7aTowO3M6MTI6ImZpbGVMb2NhdGlvbiI7czo2NToibW9kdWxlcy9NZWV0aW5ncy9EYXNobGV0cy9NeU1lZXRpbmdzRGFzaGxldC9NeU1lZXRpbmdzRGFzaGxldC5waHAiO3M6Nzoib3B0aW9ucyI7YTowOnt9fXM6MzY6IjUwZDNlYjRhLWE3ODItNjQ0ZC1mMzI1LTVmNDhkNTJmZGE3MSI7YTo1OntzOjk6ImNsYXNzTmFtZSI7czoyMjoiTXlPcHBvcnR1bml0aWVzRGFzaGxldCI7czo2OiJtb2R1bGUiO3M6MTM6Ik9wcG9ydHVuaXRpZXMiO3M6MTE6ImZvcmNlQ29sdW1uIjtpOjA7czoxMjoiZmlsZUxvY2F0aW9uIjtzOjgwOiJtb2R1bGVzL09wcG9ydHVuaXRpZXMvRGFzaGxldHMvTXlPcHBvcnR1bml0aWVzRGFzaGxldC9NeU9wcG9ydHVuaXRpZXNEYXNobGV0LnBocCI7czo3OiJvcHRpb25zIjthOjA6e319czozNjoiNTBlMzM2NDItODZiOC0wMDViLTlkYzctNWY0OGQ1MWM5NGNiIjthOjU6e3M6OToiY2xhc3NOYW1lIjtzOjE3OiJNeUFjY291bnRzRGFzaGxldCI7czo2OiJtb2R1bGUiO3M6ODoiQWNjb3VudHMiO3M6MTE6ImZvcmNlQ29sdW1uIjtpOjA7czoxMjoiZmlsZUxvY2F0aW9uIjtzOjY1OiJtb2R1bGVzL0FjY291bnRzL0Rhc2hsZXRzL015QWNjb3VudHNEYXNobGV0L015QWNjb3VudHNEYXNobGV0LnBocCI7czo3OiJvcHRpb25zIjthOjA6e319czozNjoiNTBmODMxMmYtMmMxOS1mN2M5LTRjMGUtNWY0OGQ1NjVmOGU4IjthOjU6e3M6OToiY2xhc3NOYW1lIjtzOjE0OiJNeUxlYWRzRGFzaGxldCI7czo2OiJtb2R1bGUiO3M6NToiTGVhZHMiO3M6MTE6ImZvcmNlQ29sdW1uIjtpOjA7czoxMjoiZmlsZUxvY2F0aW9uIjtzOjU2OiJtb2R1bGVzL0xlYWRzL0Rhc2hsZXRzL015TGVhZHNEYXNobGV0L015TGVhZHNEYXNobGV0LnBocCI7czo3OiJvcHRpb25zIjthOjA6e319fXM6NToicGFnZXMiO2E6MTp7aTowO2E6Mzp7czo3OiJjb2x1bW5zIjthOjI6e2k6MDthOjI6e3M6NToid2lkdGgiO3M6MzoiNjAlIjtzOjg6ImRhc2hsZXRzIjthOjU6e2k6MDtzOjM2OiI1MGJhZjhiZC00NzI2LTU0OWUtMTM3Zi01ZjQ4ZDU5OTBhNmEiO2k6MTtzOjM2OiI1MGM2NGRhNS1kMzQ1LTg3MmYtN2U5NS01ZjQ4ZDU5NWE5OTciO2k6MjtzOjM2OiI1MGQzZWI0YS1hNzgyLTY0NGQtZjMyNS01ZjQ4ZDUyZmRhNzEiO2k6MztzOjM2OiI1MGUzMzY0Mi04NmI4LTAwNWItOWRjNy01ZjQ4ZDUxYzk0Y2IiO2k6NDtzOjM2OiI1MGY4MzEyZi0yYzE5LWY3YzktNGMwZS01ZjQ4ZDU2NWY4ZTgiO319aToxO2E6Mjp7czo1OiJ3aWR0aCI7czozOiI0MCUiO3M6ODoiZGFzaGxldHMiO2E6MTp7aTowO3M6MzY6IjUwYWJmNGI1LTNhOGItODkwOC03OGQwLTVmNDhkNWQxN2I0YiI7fX19czoxMDoibnVtQ29sdW1ucyI7czoxOiIzIjtzOjE0OiJwYWdlVGl0bGVMYWJlbCI7czoyMDoiTEJMX0hPTUVfUEFHRV8xX05BTUUiO319fQ=='),('5d2504a7-e194-1ada-3b9c-5f3c051e5a84','Opportunities2_OPPORTUNITY',0,'2020-08-18 16:46:28','2020-08-24 17:07:30','1','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czo0OiJuYW1lIjtzOjk6InNvcnRPcmRlciI7czozOiJBU0MiO319'),('5f597f26-91fa-903b-77f6-5f48d597c002','Home2_CALL',0,'2020-08-28 09:58:24','2020-08-28 09:58:24','1674d634-fb3a-8d8b-34c1-5f4698fed125','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),('5f83012c-07af-4178-d99a-5e90aa55cfff','global',0,'2020-04-10 17:20:57','2020-04-10 18:50:15','dc26b995-5550-b848-6f73-5e90aa679348','YTo0ODp7czoyMDoiY2FsZW5kYXJfcHVibGlzaF9rZXkiO3M6MDoiIjtzOjEyOiJtYWlsbWVyZ2Vfb24iO3M6Mjoib24iO3M6MTY6InN3YXBfbGFzdF92aWV3ZWQiO3M6MDoiIjtzOjE0OiJzd2FwX3Nob3J0Y3V0cyI7czowOiIiO3M6MTk6Im5hdmlnYXRpb25fcGFyYWRpZ20iO3M6MjoiZ20iO3M6MjA6InNvcnRfbW9kdWxlc19ieV9uYW1lIjtzOjA6IiI7czoxMzoic3VicGFuZWxfdGFicyI7czoyOiJvbiI7czoyNToiY291bnRfY29sbGFwc2VkX3N1YnBhbmVscyI7czowOiIiO3M6MTQ6Im1vZHVsZV9mYXZpY29uIjtzOjA6IiI7czo5OiJoaWRlX3RhYnMiO2E6MDp7fXM6MTE6InJlbW92ZV90YWJzIjthOjA6e31zOjc6Im5vX29wcHMiO3M6Mzoib2ZmIjtzOjEzOiJyZW1pbmRlcl90aW1lIjtzOjQ6IjE4MDAiO3M6MTk6ImVtYWlsX3JlbWluZGVyX3RpbWUiO3M6NDoiMzYwMCI7czoxNjoicmVtaW5kZXJfY2hlY2tlZCI7czoxOiIwIjtzOjIyOiJlbWFpbF9yZW1pbmRlcl9jaGVja2VkIjtzOjE6IjAiO3M6ODoidGltZXpvbmUiO3M6MTY6IkFtZXJpY2EvTmV3X1lvcmsiO3M6MjoidXQiO3M6MToiMSI7czo4OiJjdXJyZW5jeSI7czozOiItOTkiO3M6MzU6ImRlZmF1bHRfY3VycmVuY3lfc2lnbmlmaWNhbnRfZGlnaXRzIjtzOjE6IjIiO3M6MTE6Im51bV9ncnBfc2VwIjtzOjE6IiwiO3M6NzoiZGVjX3NlcCI7czoxOiIuIjtzOjQ6ImZkb3ciO3M6MToiMCI7czo1OiJkYXRlZiI7czo1OiJtL2QvWSI7czo1OiJ0aW1lZiI7czo0OiJoOmlhIjtzOjI2OiJkZWZhdWx0X2xvY2FsZV9uYW1lX2Zvcm1hdCI7czo1OiJzIGYgbCI7czoxNjoiZXhwb3J0X2RlbGltaXRlciI7czoxOiIsIjtzOjIyOiJkZWZhdWx0X2V4cG9ydF9jaGFyc2V0IjtzOjU6IlVURi04IjtzOjE0OiJ1c2VfcmVhbF9uYW1lcyI7czoyOiJvbiI7czoxNzoibWFpbF9zbXRwYXV0aF9yZXEiO3M6MDoiIjtzOjEyOiJtYWlsX3NtdHBzc2wiO2k6MDtzOjE1OiJlbWFpbF9saW5rX3R5cGUiO3M6NToic3VnYXIiO3M6MTE6ImVkaXRvcl90eXBlIjtzOjY6Im1vemFpayI7czoxNzoiZW1haWxfc2hvd19jb3VudHMiO2k6MDtzOjg6InN1YnRoZW1lIjtzOjQ6IkRhd24iO3M6MTE6ImxvZ2luZmFpbGVkIjtzOjE6IjAiO3M6MTU6ImxvZ2luZXhwaXJhdGlvbiI7czoxOiIwIjtzOjc6ImxvY2tvdXQiO3M6MDoiIjtzOjEwOiJ1c2VyX3RoZW1lIjtzOjY6IlN1aXRlUCI7czoxOToidGhlbWVfY3VycmVudF9ncm91cCI7czozOiJBbGwiO3M6ODoiUHJvamVjdFEiO2E6MTp7czoxMzoic2VhcmNoRm9ybVRhYiI7czoxNToiYWR2YW5jZWRfc2VhcmNoIjt9czoxMjoiUHJvamVjdFRhc2tRIjthOjE6e3M6MTM6InNlYXJjaEZvcm1UYWIiO3M6MTI6ImJhc2ljX3NlYXJjaCI7fXM6MTM6InNwX1RpbWVzaGVldFEiO2E6MTp7czoxMzoic2VhcmNoRm9ybVRhYiI7czoxMjoiYmFzaWNfc2VhcmNoIjt9czo5OiJBY2NvdW50c1EiO2E6MTp7czoxMzoic2VhcmNoRm9ybVRhYiI7czoxMjoiYmFzaWNfc2VhcmNoIjt9czoxMzoiQU9TX1Byb2R1Y3RzUSI7YToxOntzOjEzOiJzZWFyY2hGb3JtVGFiIjtzOjEyOiJiYXNpY19zZWFyY2giO31zOjIwOiJBTV9Qcm9qZWN0VGVtcGxhdGVzUSI7YToxOntzOjEzOiJzZWFyY2hGb3JtVGFiIjtzOjEyOiJiYXNpY19zZWFyY2giO31zOjk6Ik1lZXRpbmdzUSI7YToxOntzOjEzOiJzZWFyY2hGb3JtVGFiIjtzOjEyOiJiYXNpY19zZWFyY2giO31zOjIxOiJzcF90aW1lc2hlZXRfdHJhY2tlclEiO2E6Njp7czoxODoidXNlcl9pZF9jX2FkdmFuY2VkIjtzOjM2OiJkYzI2Yjk5NS01NTUwLWI4NDgtNmY3My01ZTkwYWE2NzkzNDgiO3M6MTk6InRyYWNrX3VzZXJfYWR2YW5jZWQiO3M6MTk6IlRpbWVzaGVldCBUZXN0IFVzZXIiO3M6NjoibW9kdWxlIjtzOjIwOiJzcF90aW1lc2hlZXRfdHJhY2tlciI7czoxMzoic2VhcmNoRm9ybVRhYiI7czoxNToiYWR2YW5jZWRfc2VhcmNoIjtzOjU6InF1ZXJ5IjtzOjQ6InRydWUiO3M6NjoiYWN0aW9uIjtzOjU6ImluZGV4Ijt9fQ=='),('61cc3bb6-26e5-09e0-8ddf-5e90b81de056','Accounts2_ACCOUNT',0,'2020-04-10 18:18:58','2020-04-10 18:22:21','dc26b995-5550-b848-6f73-5e90aa679348','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),('6287c533-4dc2-38aa-33a6-5e90b85b5331','ProjectTask2_PROJECTTASK',0,'2020-04-10 18:16:34','2020-04-10 18:22:21','dc26b995-5550-b848-6f73-5e90aa679348','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),('633ad293-4c6a-7169-d575-5f48d567b9ed','Home2_MEETING',0,'2020-08-28 09:58:24','2020-08-28 09:58:24','1674d634-fb3a-8d8b-34c1-5f4698fed125','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),('66a30360-7f30-ccbf-a7f8-5e90b06325e9','sp_Timesheet2_SP_TIMESHEET',0,'2020-04-10 17:46:16','2020-04-10 17:46:16','1','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),('66d793ab-39cc-5cd9-b39d-5f48d5a71080','Home2_OPPORTUNITY',0,'2020-08-28 09:58:24','2020-08-28 09:58:24','1674d634-fb3a-8d8b-34c1-5f4698fed125','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),('6aa1d0e7-a830-17d5-8bc8-5f48d5c230c6','Home2_ACCOUNT',0,'2020-08-28 09:58:24','2020-08-28 09:58:24','1674d634-fb3a-8d8b-34c1-5f4698fed125','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),('6cf30cbf-89ac-3a93-7c98-5f43c67e218c','Cases2_CASE',0,'2020-08-24 13:55:39','2020-08-24 13:55:39','1','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),('6d21da3e-c374-2833-9ec6-5f48d517f01c','Home2_LEAD',0,'2020-08-28 09:58:24','2020-08-28 09:58:24','1674d634-fb3a-8d8b-34c1-5f4698fed125','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),('6d3b3b81-8a51-621e-aa09-5f469b251ac8','ACLRoles2_ACLROLE',0,'2020-08-26 17:29:16','2020-08-28 09:13:41','1','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),('6fe47e90-6d33-67e6-a1f6-5f48d5b47859','Home2_SUGARFEED',0,'2020-08-28 09:58:24','2020-08-28 09:58:24','1674d634-fb3a-8d8b-34c1-5f4698fed125','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),('74b5f0f2-b330-2302-58f9-5e90b2bce180','Emails',0,'2020-04-10 17:52:39','2020-08-28 09:13:41','1','YTowOnt9'),('7a2f512c-4b16-ef8b-0e3c-5f469f27ccc8','Accounts2_ACCOUNT',0,'2020-08-26 17:45:30','2020-08-26 17:45:30','8d544213-bcac-b962-fc13-5f4698c75700','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),('7a40660c-fc31-58f5-e62e-5f4558326480','FP_Event_Locations2_FP_EVENT_LOCATIONS',0,'2020-08-25 18:30:53','2020-08-25 18:30:53','1','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),('8ca3c5dc-dbbb-7af5-f681-5c6e1df4d4a5','global',0,'2019-02-21 03:39:38','2020-09-01 13:35:56','1','YTo0Mzp7czoyMDoiY2FsZW5kYXJfcHVibGlzaF9rZXkiO3M6MzY6IjhiNDUzZGUyLTE0MzctMmI4YS1iMDk1LTVjNmUxZGY2ZTYyNyI7czoxMjoibWFpbG1lcmdlX29uIjtzOjM6Im9mZiI7czoxNjoic3dhcF9sYXN0X3ZpZXdlZCI7czowOiIiO3M6MTQ6InN3YXBfc2hvcnRjdXRzIjtzOjA6IiI7czoxOToibmF2aWdhdGlvbl9wYXJhZGlnbSI7czoyOiJnbSI7czoyMDoic29ydF9tb2R1bGVzX2J5X25hbWUiO3M6MDoiIjtzOjEzOiJzdWJwYW5lbF90YWJzIjtzOjA6IiI7czoyNToiY291bnRfY29sbGFwc2VkX3N1YnBhbmVscyI7czowOiIiO3M6MTQ6Im1vZHVsZV9mYXZpY29uIjtzOjA6IiI7czo5OiJoaWRlX3RhYnMiO2E6MDp7fXM6Nzoibm9fb3BwcyI7czozOiJvZmYiO3M6ODoidGltZXpvbmUiO3M6MzoiVVRDIjtzOjI6InV0IjtzOjE6IjEiO3M6MTU6Im1haWxfc210cHNlcnZlciI7czowOiIiO3M6MTM6Im1haWxfc210cHBvcnQiO3M6MjoiMjUiO3M6MTM6Im1haWxfc210cHVzZXIiO3M6MDoiIjtzOjEzOiJtYWlsX3NtdHBwYXNzIjtzOjA6IiI7czoxNDoidXNlX3JlYWxfbmFtZXMiO3M6Mzoib2ZmIjtzOjE3OiJtYWlsX3NtdHBhdXRoX3JlcSI7czoxOiIxIjtzOjEyOiJtYWlsX3NtdHBzc2wiO2k6MTtzOjE3OiJlbWFpbF9zaG93X2NvdW50cyI7aTowO3M6MTA6InVzZXJfdGhlbWUiO3M6NjoiU3VpdGVQIjtzOjE5OiJ0aGVtZV9jdXJyZW50X2dyb3VwIjtzOjM6IkFsbCI7czoxMToiU2NoZWR1bGVyc1EiO2E6MTp7czoxMzoic2VhcmNoRm9ybVRhYiI7czoxMjoiYmFzaWNfc2VhcmNoIjt9czo2OiJVc2Vyc1EiO2E6MTp7czoxMzoic2VhcmNoRm9ybVRhYiI7czoxNToiYWR2YW5jZWRfc2VhcmNoIjt9czoxMzoic3BfVGltZXNoZWV0USI7YToxOntzOjEzOiJzZWFyY2hGb3JtVGFiIjtzOjEyOiJiYXNpY19zZWFyY2giO31zOjEwOiJFbXBsb3llZXNRIjthOjQ6e3M6NjoibW9kdWxlIjtzOjk6IkVtcGxveWVlcyI7czo2OiJhY3Rpb24iO3M6NToiaW5kZXgiO3M6NToicXVlcnkiO3M6NDoidHJ1ZSI7czoxMzoic2VhcmNoRm9ybVRhYiI7czoxMjoiYmFzaWNfc2VhcmNoIjt9czoxMToiZWRpdG9yX3R5cGUiO3M6NjoibW96YWlrIjtzOjk6IkNvbnRhY3RzUSI7YToxOntzOjEzOiJzZWFyY2hGb3JtVGFiIjtzOjEyOiJiYXNpY19zZWFyY2giO31zOjk6IkFjY291bnRzUSI7YToxOntzOjEzOiJzZWFyY2hGb3JtVGFiIjtzOjE1OiJhZHZhbmNlZF9zZWFyY2giO31zOjE0OiJPcHBvcnR1bml0aWVzUSI7YToyOntzOjE0OiJkaXNwbGF5Q29sdW1ucyI7czoxMTA6Ik5BTUV8QUNDT1VOVF9OQU1FfFNBTEVTX1NUQUdFfFNBTEVTX1NUQUdFX0FNT1VOVF9DfEFNT1VOVF9VU0RPTExBUnxEQVRFX0NMT1NFRHxBU1NJR05FRF9VU0VSX05BTUV8REFURV9FTlRFUkVEIjtzOjEzOiJzZWFyY2hGb3JtVGFiIjtzOjE1OiJhZHZhbmNlZF9zZWFyY2giO31zOjEwOiJDYW1wYWlnbnNRIjthOjE6e3M6MTM6InNlYXJjaEZvcm1UYWIiO3M6MTU6ImFkdmFuY2VkX3NlYXJjaCI7fXM6NjoiTGVhZHNRIjthOjE6e3M6MTM6InNlYXJjaEZvcm1UYWIiO3M6MTI6ImJhc2ljX3NlYXJjaCI7fXM6MTE6ImxvZ2luZmFpbGVkIjtzOjE6IjAiO3M6OToiTWVldGluZ3NRIjthOjE6e3M6MTM6InNlYXJjaEZvcm1UYWIiO3M6MTI6ImJhc2ljX3NlYXJjaCI7fXM6NjoiVGFza3NRIjthOjE6e3M6MTM6InNlYXJjaEZvcm1UYWIiO3M6MTI6ImJhc2ljX3NlYXJjaCI7fXM6MTA6IkZQX2V2ZW50c1EiO2E6MTp7czoxMzoic2VhcmNoRm9ybVRhYiI7czoxMjoiYmFzaWNfc2VhcmNoIjt9czoxOToiRlBfRXZlbnRfTG9jYXRpb25zUSI7YToxOntzOjEzOiJzZWFyY2hGb3JtVGFiIjtzOjE1OiJhZHZhbmNlZF9zZWFyY2giO31zOjk6IkFDTFJvbGVzUSI7YToxOntzOjEzOiJzZWFyY2hGb3JtVGFiIjtzOjE1OiJhZHZhbmNlZF9zZWFyY2giO31zOjE1OiJTZWN1cml0eUdyb3Vwc1EiO2E6MTp7czoxMzoic2VhcmNoRm9ybVRhYiI7czoxNToiYWR2YW5jZWRfc2VhcmNoIjt9czoxODoiQU9TX1BERl9UZW1wbGF0ZXNRIjthOjE6e3M6MTM6InNlYXJjaEZvcm1UYWIiO3M6MTI6ImJhc2ljX3NlYXJjaCI7fXM6MTM6IkFPU19Qcm9kdWN0c1EiO2E6MTp7czoxMzoic2VhcmNoRm9ybVRhYiI7czoxNToiYWR2YW5jZWRfc2VhcmNoIjt9czoxMToiQU9TX1F1b3Rlc1EiO2E6MTp7czoxMzoic2VhcmNoRm9ybVRhYiI7czoxMjoiYmFzaWNfc2VhcmNoIjt9fQ=='),('8e0eacfc-75d5-69fd-0dfd-5c6e1d55dc1c','GoogleSync',0,'2019-02-21 03:39:38','2020-08-28 09:13:41','1','YToxOntzOjg6InN5bmNHQ2FsIjtpOjA7fQ=='),('8ecddf48-1f08-a9e3-dc9c-5f3bec6263e2','Accounts2_ACCOUNT',0,'2020-08-18 14:59:13','2020-08-28 09:13:41','1','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),('93ee8d9c-e13a-4bc2-0871-5f48ca9d00f1','ACLRoles',0,'2020-08-28 09:13:40','2020-08-28 09:13:41','1','YTowOnt9'),('94a906ee-3123-a4a4-b9c0-5f44f77cd64e','Home2_EMAIL',0,'2020-08-25 11:35:06','2020-08-28 09:13:41','1','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),('967d2bba-8a9a-af7c-2ac5-5f48ca1bcd61','Users',0,'2020-08-28 09:13:40','2020-08-28 09:13:41','1','YTowOnt9'),('96be3327-780f-73a8-4b33-5f48da7af19f','Contacts2_CONTACT',0,'2020-08-28 10:21:12','2020-08-28 10:21:12','8d544213-bcac-b962-fc13-5f4698c75700','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),('96dee528-95f4-571a-f4e5-5f4558bdb33d','Tasks2_TASK',0,'2020-08-25 18:29:51','2020-08-25 18:29:51','1','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),('97a5542b-b6ec-1af2-78da-5f46982c8218','global',0,'2020-08-26 17:14:28','2020-09-01 16:58:45','8d544213-bcac-b962-fc13-5f4698c75700','YTo0NDp7czoyMDoiY2FsZW5kYXJfcHVibGlzaF9rZXkiO3M6MDoiIjtzOjEyOiJtYWlsbWVyZ2Vfb24iO3M6Mjoib24iO3M6MTY6InN3YXBfbGFzdF92aWV3ZWQiO3M6MDoiIjtzOjE0OiJzd2FwX3Nob3J0Y3V0cyI7czowOiIiO3M6MTk6Im5hdmlnYXRpb25fcGFyYWRpZ20iO3M6MjoiZ20iO3M6MjA6InNvcnRfbW9kdWxlc19ieV9uYW1lIjtzOjA6IiI7czoxMzoic3VicGFuZWxfdGFicyI7czoyOiJvbiI7czoyNToiY291bnRfY29sbGFwc2VkX3N1YnBhbmVscyI7czowOiIiO3M6MTQ6Im1vZHVsZV9mYXZpY29uIjtzOjA6IiI7czo5OiJoaWRlX3RhYnMiO2E6MDp7fXM6MTE6InJlbW92ZV90YWJzIjthOjA6e31zOjc6Im5vX29wcHMiO3M6Mzoib2ZmIjtzOjEzOiJyZW1pbmRlcl90aW1lIjtzOjQ6IjE4MDAiO3M6MTk6ImVtYWlsX3JlbWluZGVyX3RpbWUiO3M6NDoiMzYwMCI7czoxNjoicmVtaW5kZXJfY2hlY2tlZCI7czoxOiIwIjtzOjIyOiJlbWFpbF9yZW1pbmRlcl9jaGVja2VkIjtzOjE6IjAiO3M6ODoidGltZXpvbmUiO3M6MTI6IkFzaWEvQ29sb21ibyI7czoyOiJ1dCI7czoxOiIxIjtzOjg6ImN1cnJlbmN5IjtzOjM6Ii05OSI7czozNToiZGVmYXVsdF9jdXJyZW5jeV9zaWduaWZpY2FudF9kaWdpdHMiO3M6MToiMiI7czoxMToibnVtX2dycF9zZXAiO3M6MToiLCI7czo3OiJkZWNfc2VwIjtzOjE6Ii4iO3M6NDoiZmRvdyI7czoxOiIwIjtzOjU6ImRhdGVmIjtzOjU6Im0vZC9ZIjtzOjU6InRpbWVmIjtzOjQ6Img6aWEiO3M6MjY6ImRlZmF1bHRfbG9jYWxlX25hbWVfZm9ybWF0IjtzOjU6InMgZiBsIjtzOjE2OiJleHBvcnRfZGVsaW1pdGVyIjtzOjE6IiwiO3M6MjI6ImRlZmF1bHRfZXhwb3J0X2NoYXJzZXQiO3M6NToiVVRGLTgiO3M6MTQ6InVzZV9yZWFsX25hbWVzIjtzOjI6Im9uIjtzOjE3OiJtYWlsX3NtdHBhdXRoX3JlcSI7czowOiIiO3M6MTI6Im1haWxfc210cHNzbCI7aTowO3M6MTU6ImVtYWlsX2xpbmtfdHlwZSI7czo1OiJzdWdhciI7czoxMToiZWRpdG9yX3R5cGUiO3M6NjoibW96YWlrIjtzOjE3OiJlbWFpbF9zaG93X2NvdW50cyI7aTowO3M6ODoic3VidGhlbWUiO3M6NDoiRGF3biI7czoxNToibG9naW5leHBpcmF0aW9uIjtzOjE6IjAiO3M6NzoibG9ja291dCI7czowOiIiO3M6MTE6ImxvZ2luZmFpbGVkIjtzOjE6IjAiO3M6MjE6InNwX3RpbWVzaGVldF90cmFja2VyUSI7YTo2OntzOjE4OiJ1c2VyX2lkX2NfYWR2YW5jZWQiO3M6MzY6IjhkNTQ0MjEzLWJjYWMtYjk2Mi1mYzEzLTVmNDY5OGM3NTcwMCI7czoxOToidHJhY2tfdXNlcl9hZHZhbmNlZCI7czoxMToiU3VzYW4gQ2h1bmciO3M6NjoibW9kdWxlIjtzOjIwOiJzcF90aW1lc2hlZXRfdHJhY2tlciI7czoxMzoic2VhcmNoRm9ybVRhYiI7czoxNToiYWR2YW5jZWRfc2VhcmNoIjtzOjU6InF1ZXJ5IjtzOjQ6InRydWUiO3M6NjoiYWN0aW9uIjtzOjU6ImluZGV4Ijt9czoxMDoidXNlcl90aGVtZSI7czo2OiJTdWl0ZVAiO3M6MTk6InRoZW1lX2N1cnJlbnRfZ3JvdXAiO3M6MzoiQWxsIjtzOjk6IkFjY291bnRzUSI7YToxOntzOjEzOiJzZWFyY2hGb3JtVGFiIjtzOjEyOiJiYXNpY19zZWFyY2giO31zOjY6IlRhc2tzUSI7YToxOntzOjEzOiJzZWFyY2hGb3JtVGFiIjtzOjEyOiJiYXNpY19zZWFyY2giO31zOjk6IkNvbnRhY3RzUSI7YToxOntzOjEzOiJzZWFyY2hGb3JtVGFiIjtzOjE1OiJhZHZhbmNlZF9zZWFyY2giO319'),('9a1889e9-d79a-3ee0-dd4d-5f46982a187a','GoogleSync',0,'2020-08-26 17:14:28','2020-09-01 16:58:45','8d544213-bcac-b962-fc13-5f4698c75700','YToxOntzOjg6InN5bmNHQ2FsIjtpOjA7fQ=='),('9b670fab-758f-e69c-27d7-5e90aa6cc911','GoogleSync',0,'2020-04-10 17:20:57','2020-04-10 18:49:57','dc26b995-5550-b848-6f73-5e90aa679348','YToxOntzOjg6InN5bmNHQ2FsIjtpOjA7fQ=='),('a6d5e566-7201-747d-5b98-5f48d7a299ba','Dashboard',0,'2020-08-28 10:06:01','2020-08-28 10:06:01','b70243be-ca4c-b256-aff8-5f46980acf9f','YTowOnt9'),('a90e7973-5cea-8a73-d803-5e90aa4627d9','Users2_USER',0,'2020-04-10 17:18:23','2020-04-10 17:18:23','1','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),('a9c7b4f9-716a-bb8e-209a-5f48d4f9a43c','Home2_LEAD_27ce10b3-9e77-9ee0-c652-5f48d4d28acb',0,'2020-08-28 09:53:14','2020-08-28 10:06:01','b70243be-ca4c-b256-aff8-5f46980acf9f','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),('a9ceb74b-95e4-4b80-c9fb-5f44f8a8ca85','Home2_BUG_ddd72dc6-5e29-e86e-adca-5f44f8c8b4ed',0,'2020-08-25 11:38:54','2020-08-25 11:38:54','1','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),('a9f70433-5aee-7723-633c-5f4828b11d25','AOS_PDF_Templates2_AOS_PDF_TEMPLATES',0,'2020-08-27 21:42:10','2020-08-27 21:42:10','1','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),('aefac7fa-c987-17dc-eded-5f48d3495ae4','Emails',0,'2020-08-28 09:50:40','2020-08-28 16:56:43','1674d634-fb3a-8d8b-34c1-5f4698fed125','YTowOnt9'),('afef0ed6-632e-3436-f48b-5f469f489deb','Home2_LEAD_228587d7-74de-2b74-4071-5f469f57d0ee',0,'2020-08-26 17:45:26','2020-08-26 17:45:26','8d544213-bcac-b962-fc13-5f4698c75700','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),('b155defe-58ba-0d0c-8cea-5e909c948464','Schedulers2_SCHEDULER',0,'2020-04-10 16:21:13','2020-04-10 16:21:13','1','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),('b19cab6f-c6b0-6503-5708-5c6e1dffebfb','Home',0,'2019-02-21 03:40:01','2020-08-28 09:13:41','1','YToyOntzOjg6ImRhc2hsZXRzIjthOjY6e3M6MzY6IjY2YTUxZGM1LWFkYjctNGIwYi1mYTIzLTVjNmUxZGNiMGRiYSI7YTo1OntzOjk6ImNsYXNzTmFtZSI7czoxNDoiTXlDYWxsc0Rhc2hsZXQiO3M6NjoibW9kdWxlIjtzOjU6IkNhbGxzIjtzOjExOiJmb3JjZUNvbHVtbiI7aTowO3M6MTI6ImZpbGVMb2NhdGlvbiI7czo1NjoibW9kdWxlcy9DYWxscy9EYXNobGV0cy9NeUNhbGxzRGFzaGxldC9NeUNhbGxzRGFzaGxldC5waHAiO3M6Nzoib3B0aW9ucyI7YTowOnt9fXM6MzY6IjY3OTYyYjUyLWVhNjctMGYzYi01NWRiLTVjNmUxZGFmMWQzMyI7YTo1OntzOjk6ImNsYXNzTmFtZSI7czoxNzoiTXlNZWV0aW5nc0Rhc2hsZXQiO3M6NjoibW9kdWxlIjtzOjg6Ik1lZXRpbmdzIjtzOjExOiJmb3JjZUNvbHVtbiI7aTowO3M6MTI6ImZpbGVMb2NhdGlvbiI7czo2NToibW9kdWxlcy9NZWV0aW5ncy9EYXNobGV0cy9NeU1lZXRpbmdzRGFzaGxldC9NeU1lZXRpbmdzRGFzaGxldC5waHAiO3M6Nzoib3B0aW9ucyI7YTowOnt9fXM6MzY6IjY4OThkNzYwLWY0MGItYmUyZS1lMmJiLTVjNmUxZDhhNDQ2MSI7YTo1OntzOjk6ImNsYXNzTmFtZSI7czoxNzoiTXlBY2NvdW50c0Rhc2hsZXQiO3M6NjoibW9kdWxlIjtzOjg6IkFjY291bnRzIjtzOjExOiJmb3JjZUNvbHVtbiI7aTowO3M6MTI6ImZpbGVMb2NhdGlvbiI7czo2NToibW9kdWxlcy9BY2NvdW50cy9EYXNobGV0cy9NeUFjY291bnRzRGFzaGxldC9NeUFjY291bnRzRGFzaGxldC5waHAiO3M6Nzoib3B0aW9ucyI7YTowOnt9fXM6MzY6ImM3ZmY0NjZiLTcyMzUtZTEyMC0xNDQxLTVmNDRmNzQ1NTgwYyI7YTo0OntzOjk6ImNsYXNzTmFtZSI7czoxNToiTXlFbWFpbHNEYXNobGV0IjtzOjY6Im1vZHVsZSI7czo2OiJFbWFpbHMiO3M6Nzoib3B0aW9ucyI7YTowOnt9czoxMjoiZmlsZUxvY2F0aW9uIjtzOjU5OiJtb2R1bGVzL0VtYWlscy9EYXNobGV0cy9NeUVtYWlsc0Rhc2hsZXQvTXlFbWFpbHNEYXNobGV0LnBocCI7fXM6MzY6ImMyZGE4ODI5LTBmYjMtZmQ3MS1mYWVlLTVmNDRmODVlZGRjZSI7YTo0OntzOjk6ImNsYXNzTmFtZSI7czoxNzoiTXlNZWV0aW5nc0Rhc2hsZXQiO3M6NjoibW9kdWxlIjtzOjg6Ik1lZXRpbmdzIjtzOjc6Im9wdGlvbnMiO2E6MDp7fXM6MTI6ImZpbGVMb2NhdGlvbiI7czo2NToibW9kdWxlcy9NZWV0aW5ncy9EYXNobGV0cy9NeU1lZXRpbmdzRGFzaGxldC9NeU1lZXRpbmdzRGFzaGxldC5waHAiO31zOjM2OiJiNjY1ODc5MC1mOTdjLWI0NmMtYTk2ZS01ZjQ1MmQxODE5NmQiO2E6NDp7czo5OiJjbGFzc05hbWUiO3M6MTU6IkNhbGVuZGFyRGFzaGxldCI7czo2OiJtb2R1bGUiO3M6ODoiQ2FsZW5kYXIiO3M6Nzoib3B0aW9ucyI7YTowOnt9czoxMjoiZmlsZUxvY2F0aW9uIjtzOjYxOiJtb2R1bGVzL0NhbGVuZGFyL0Rhc2hsZXRzL0NhbGVuZGFyRGFzaGxldC9DYWxlbmRhckRhc2hsZXQucGhwIjt9fXM6NToicGFnZXMiO2E6MTp7aTowO2E6Mzp7czo3OiJjb2x1bW5zIjthOjI6e2k6MDthOjI6e3M6NToid2lkdGgiO3M6MzoiNjAlIjtzOjg6ImRhc2hsZXRzIjthOjQ6e2k6MDtzOjM2OiJjMmRhODgyOS0wZmIzLWZkNzEtZmFlZS01ZjQ0Zjg1ZWRkY2UiO2k6MTtzOjM2OiI2Nzk2MmI1Mi1lYTY3LTBmM2ItNTVkYi01YzZlMWRhZjFkMzMiO2k6MjtzOjM2OiJjN2ZmNDY2Yi03MjM1LWUxMjAtMTQ0MS01ZjQ0Zjc0NTU4MGMiO2k6MztzOjM2OiI2ODk4ZDc2MC1mNDBiLWJlMmUtZTJiYi01YzZlMWQ4YTQ0NjEiO319aToxO2E6Mjp7czo1OiJ3aWR0aCI7czozOiI0MCUiO3M6ODoiZGFzaGxldHMiO2E6Mjp7aTowO3M6MzY6IjY2YTUxZGM1LWFkYjctNGIwYi1mYTIzLTVjNmUxZGNiMGRiYSI7aToxO3M6MzY6ImI2NjU4NzkwLWY5N2MtYjQ2Yy1hOTZlLTVmNDUyZDE4MTk2ZCI7fX19czoxMDoibnVtQ29sdW1ucyI7czoxOiIzIjtzOjE0OiJwYWdlVGl0bGVMYWJlbCI7czoyMDoiTEJMX0hPTUVfUEFHRV8xX05BTUUiO319fQ=='),('b366b239-4e3e-836f-05a7-5e90b7c7cd85','Home',0,'2020-04-10 18:14:35','2020-04-10 18:24:58','dc26b995-5550-b848-6f73-5e90aa679348','YToyOntzOjg6ImRhc2hsZXRzIjthOjY6e3M6MzY6IjJlZGZlODgxLWZiZDgtN2Y5OS0yZWU0LTVlOTBiOWViYTE1OSI7YTo0OntzOjk6ImNsYXNzTmFtZSI7czoxNjoiU3VnYXJGZWVkRGFzaGxldCI7czo2OiJtb2R1bGUiO3M6OToiU3VnYXJGZWVkIjtzOjExOiJmb3JjZUNvbHVtbiI7aToxO3M6MTI6ImZpbGVMb2NhdGlvbiI7czo2NDoibW9kdWxlcy9TdWdhckZlZWQvRGFzaGxldHMvU3VnYXJGZWVkRGFzaGxldC9TdWdhckZlZWREYXNobGV0LnBocCI7fXM6MzY6IjJlZWMyYjA0LWYyZDQtZWUyMi1kZWE4LTVlOTBiOWU3NGYxOSI7YTo1OntzOjk6ImNsYXNzTmFtZSI7czoxNDoiTXlDYWxsc0Rhc2hsZXQiO3M6NjoibW9kdWxlIjtzOjU6IkNhbGxzIjtzOjExOiJmb3JjZUNvbHVtbiI7aTowO3M6MTI6ImZpbGVMb2NhdGlvbiI7czo1NjoibW9kdWxlcy9DYWxscy9EYXNobGV0cy9NeUNhbGxzRGFzaGxldC9NeUNhbGxzRGFzaGxldC5waHAiO3M6Nzoib3B0aW9ucyI7YTowOnt9fXM6MzY6IjJlZjY4ODhlLTc2YmYtMTRiNC01YjY2LTVlOTBiOWQ2NzBhOCI7YTo1OntzOjk6ImNsYXNzTmFtZSI7czoxNzoiTXlNZWV0aW5nc0Rhc2hsZXQiO3M6NjoibW9kdWxlIjtzOjg6Ik1lZXRpbmdzIjtzOjExOiJmb3JjZUNvbHVtbiI7aTowO3M6MTI6ImZpbGVMb2NhdGlvbiI7czo2NToibW9kdWxlcy9NZWV0aW5ncy9EYXNobGV0cy9NeU1lZXRpbmdzRGFzaGxldC9NeU1lZXRpbmdzRGFzaGxldC5waHAiO3M6Nzoib3B0aW9ucyI7YTowOnt9fXM6MzY6IjJlZmVjOTQ4LTA0OTQtNGY0MC1iYmQxLTVlOTBiOTVmNDhiYSI7YTo1OntzOjk6ImNsYXNzTmFtZSI7czoyMjoiTXlPcHBvcnR1bml0aWVzRGFzaGxldCI7czo2OiJtb2R1bGUiO3M6MTM6Ik9wcG9ydHVuaXRpZXMiO3M6MTE6ImZvcmNlQ29sdW1uIjtpOjA7czoxMjoiZmlsZUxvY2F0aW9uIjtzOjgwOiJtb2R1bGVzL09wcG9ydHVuaXRpZXMvRGFzaGxldHMvTXlPcHBvcnR1bml0aWVzRGFzaGxldC9NeU9wcG9ydHVuaXRpZXNEYXNobGV0LnBocCI7czo3OiJvcHRpb25zIjthOjA6e319czozNjoiMmYwYTUxODItNjRjYi03NWE2LWY0ZjgtNWU5MGI5MDgyMDc0IjthOjU6e3M6OToiY2xhc3NOYW1lIjtzOjE3OiJNeUFjY291bnRzRGFzaGxldCI7czo2OiJtb2R1bGUiO3M6ODoiQWNjb3VudHMiO3M6MTE6ImZvcmNlQ29sdW1uIjtpOjA7czoxMjoiZmlsZUxvY2F0aW9uIjtzOjY1OiJtb2R1bGVzL0FjY291bnRzL0Rhc2hsZXRzL015QWNjb3VudHNEYXNobGV0L015QWNjb3VudHNEYXNobGV0LnBocCI7czo3OiJvcHRpb25zIjthOjA6e319czozNjoiMmYxMzIzZTUtYWY1NS1hYTk5LTMwMGItNWU5MGI5NjExM2Q1IjthOjU6e3M6OToiY2xhc3NOYW1lIjtzOjE0OiJNeUxlYWRzRGFzaGxldCI7czo2OiJtb2R1bGUiO3M6NToiTGVhZHMiO3M6MTE6ImZvcmNlQ29sdW1uIjtpOjA7czoxMjoiZmlsZUxvY2F0aW9uIjtzOjU2OiJtb2R1bGVzL0xlYWRzL0Rhc2hsZXRzL015TGVhZHNEYXNobGV0L015TGVhZHNEYXNobGV0LnBocCI7czo3OiJvcHRpb25zIjthOjA6e319fXM6NToicGFnZXMiO2E6MTp7aTowO2E6Mzp7czo3OiJjb2x1bW5zIjthOjI6e2k6MDthOjI6e3M6NToid2lkdGgiO3M6MzoiNjAlIjtzOjg6ImRhc2hsZXRzIjthOjU6e2k6MDtzOjM2OiIyZWVjMmIwNC1mMmQ0LWVlMjItZGVhOC01ZTkwYjllNzRmMTkiO2k6MTtzOjM2OiIyZWY2ODg4ZS03NmJmLTE0YjQtNWI2Ni01ZTkwYjlkNjcwYTgiO2k6MjtzOjM2OiIyZWZlYzk0OC0wNDk0LTRmNDAtYmJkMS01ZTkwYjk1ZjQ4YmEiO2k6MztzOjM2OiIyZjBhNTE4Mi02NGNiLTc1YTYtZjRmOC01ZTkwYjkwODIwNzQiO2k6NDtzOjM2OiIyZjEzMjNlNS1hZjU1LWFhOTktMzAwYi01ZTkwYjk2MTEzZDUiO319aToxO2E6Mjp7czo1OiJ3aWR0aCI7czozOiI0MCUiO3M6ODoiZGFzaGxldHMiO2E6MTp7aTowO3M6MzY6IjJlZGZlODgxLWZiZDgtN2Y5OS0yZWU0LTVlOTBiOWViYTE1OSI7fX19czoxMDoibnVtQ29sdW1ucyI7czoxOiIzIjtzOjE0OiJwYWdlVGl0bGVMYWJlbCI7czoyMDoiTEJMX0hPTUVfUEFHRV8xX05BTUUiO319fQ=='),('b47c8934-3448-669e-035b-5c6e1d09b3b1','Home2_CALL',0,'2019-02-21 03:40:01','2020-08-28 09:13:41','1','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),('b4cfafbb-7a76-906e-ea66-5e909d8ddc15','ETag',0,'2020-04-10 16:25:28','2020-08-21 06:54:33','1','YToxOntzOjEyOiJtYWluTWVudUVUYWciO2k6Mjt9'),('b6d919ec-618a-5450-dac5-5e90b7efee99','Home2_CALL',0,'2020-04-10 18:14:35','2020-04-10 18:22:21','dc26b995-5550-b848-6f73-5e90aa679348','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),('b8397d97-ef25-8def-12d1-5c6e1d75cc3e','Home2_MEETING',0,'2019-02-21 03:40:01','2020-08-28 09:13:41','1','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),('b972c3a0-1471-2b00-5a49-5e90b795556e','Home2_MEETING',0,'2020-04-10 18:14:35','2020-04-10 18:22:21','dc26b995-5550-b848-6f73-5e90aa679348','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),('bb13cf46-6f6e-2f92-7c4f-5c6e1dceea9b','Home2_OPPORTUNITY',0,'2019-02-21 03:40:01','2019-02-21 03:40:01','1','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),('bc07c0b0-49ea-8d2b-e803-5e90b7c7fcb1','Home2_OPPORTUNITY',0,'2020-04-10 18:14:35','2020-04-10 18:22:21','dc26b995-5550-b848-6f73-5e90aa679348','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),('bed94a00-fa10-3417-6c34-5e90b7842b1f','Home2_ACCOUNT',0,'2020-04-10 18:14:35','2020-04-10 18:22:21','dc26b995-5550-b848-6f73-5e90aa679348','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),('c0768295-04ad-6671-b28a-5c6e1d1a45dd','Home2_ACCOUNT',0,'2019-02-21 03:40:01','2020-08-28 09:13:41','1','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),('c14b79ea-fde9-6641-447e-5e90b80cb170','AOS_Products2_AOS_PRODUCTS',0,'2020-04-10 18:19:30','2020-04-10 18:22:21','dc26b995-5550-b848-6f73-5e90aa679348','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),('c1c69c6e-b182-1ae2-208c-5e90b71a4d85','Home2_LEAD',0,'2020-04-10 18:14:35','2020-04-10 18:22:21','dc26b995-5550-b848-6f73-5e90aa679348','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),('c4116926-e1fd-2858-df8f-5f43f360a506','Opportunities',0,'2020-08-24 17:07:30','2020-08-24 17:07:30','1','YToxOntzOjIyOiJMaXN0Vmlld0Rpc3BsYXlDb2x1bW5zIjthOjA6e319'),('c460ee3d-154e-9969-3cb3-5e90b7182926','Home2_SUGARFEED',0,'2020-04-10 18:14:35','2020-04-10 18:22:21','dc26b995-5550-b848-6f73-5e90aa679348','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),('c5872488-249f-4ed4-67ba-5f46984cf62c','global',0,'2020-08-26 17:12:53','2020-08-28 11:38:24','b70243be-ca4c-b256-aff8-5f46980acf9f','YTo0Mzp7czoyMDoiY2FsZW5kYXJfcHVibGlzaF9rZXkiO3M6MDoiIjtzOjEyOiJtYWlsbWVyZ2Vfb24iO3M6Mjoib24iO3M6MTY6InN3YXBfbGFzdF92aWV3ZWQiO3M6MDoiIjtzOjE0OiJzd2FwX3Nob3J0Y3V0cyI7czowOiIiO3M6MTk6Im5hdmlnYXRpb25fcGFyYWRpZ20iO3M6MjoiZ20iO3M6MjA6InNvcnRfbW9kdWxlc19ieV9uYW1lIjtzOjA6IiI7czoxMzoic3VicGFuZWxfdGFicyI7czoyOiJvbiI7czoyNToiY291bnRfY29sbGFwc2VkX3N1YnBhbmVscyI7czowOiIiO3M6MTQ6Im1vZHVsZV9mYXZpY29uIjtzOjA6IiI7czo5OiJoaWRlX3RhYnMiO2E6MDp7fXM6MTE6InJlbW92ZV90YWJzIjthOjA6e31zOjc6Im5vX29wcHMiO3M6Mzoib2ZmIjtzOjEzOiJyZW1pbmRlcl90aW1lIjtpOjE4MDA7czoxOToiZW1haWxfcmVtaW5kZXJfdGltZSI7aTozNjAwO3M6MTY6InJlbWluZGVyX2NoZWNrZWQiO3M6MToiMCI7czoyMjoiZW1haWxfcmVtaW5kZXJfY2hlY2tlZCI7czoxOiIwIjtzOjg6InRpbWV6b25lIjtzOjEyOiJBc2lhL0NvbG9tYm8iO3M6MjoidXQiO2k6MTtzOjg6ImN1cnJlbmN5IjtzOjM6Ii05OSI7czozNToiZGVmYXVsdF9jdXJyZW5jeV9zaWduaWZpY2FudF9kaWdpdHMiO3M6MToiMiI7czoxMToibnVtX2dycF9zZXAiO3M6MToiLCI7czo3OiJkZWNfc2VwIjtzOjE6Ii4iO3M6NDoiZmRvdyI7czoxOiIwIjtzOjU6ImRhdGVmIjtzOjU6Im0vZC9ZIjtzOjU6InRpbWVmIjtzOjQ6Img6aWEiO3M6MjY6ImRlZmF1bHRfbG9jYWxlX25hbWVfZm9ybWF0IjtzOjU6InMgZiBsIjtzOjE2OiJleHBvcnRfZGVsaW1pdGVyIjtzOjE6IiwiO3M6MjI6ImRlZmF1bHRfZXhwb3J0X2NoYXJzZXQiO3M6NToiVVRGLTgiO3M6MTQ6InVzZV9yZWFsX25hbWVzIjtzOjI6Im9uIjtzOjE3OiJtYWlsX3NtdHBhdXRoX3JlcSI7czowOiIiO3M6MTI6Im1haWxfc210cHNzbCI7aTowO3M6MTU6ImVtYWlsX2xpbmtfdHlwZSI7czo1OiJzdWdhciI7czoxMToiZWRpdG9yX3R5cGUiO3M6NjoibW96YWlrIjtzOjE3OiJlbWFpbF9zaG93X2NvdW50cyI7aTowO3M6ODoic3VidGhlbWUiO3M6NDoiRGF3biI7czoxNToibG9naW5leHBpcmF0aW9uIjtzOjE6IjAiO3M6NzoibG9ja291dCI7czowOiIiO3M6MTE6ImxvZ2luZmFpbGVkIjtzOjE6IjAiO3M6MjE6InNwX3RpbWVzaGVldF90cmFja2VyUSI7YTo2OntzOjE4OiJ1c2VyX2lkX2NfYWR2YW5jZWQiO3M6MzY6ImI3MDI0M2JlLWNhNGMtYjI1Ni1hZmY4LTVmNDY5ODBhY2Y5ZiI7czoxOToidHJhY2tfdXNlcl9hZHZhbmNlZCI7czo3OiJDaHJpcyBLIjtzOjY6Im1vZHVsZSI7czoyMDoic3BfdGltZXNoZWV0X3RyYWNrZXIiO3M6MTM6InNlYXJjaEZvcm1UYWIiO3M6MTU6ImFkdmFuY2VkX3NlYXJjaCI7czo1OiJxdWVyeSI7czo0OiJ0cnVlIjtzOjY6ImFjdGlvbiI7czo1OiJpbmRleCI7fXM6MTA6InVzZXJfdGhlbWUiO3M6NjoiU3VpdGVQIjtzOjE5OiJ0aGVtZV9jdXJyZW50X2dyb3VwIjtzOjM6IkFsbCI7czo5OiJBY2NvdW50c1EiO2E6MTp7czoxMzoic2VhcmNoRm9ybVRhYiI7czoxMjoiYmFzaWNfc2VhcmNoIjt9czoxNDoiT3Bwb3J0dW5pdGllc1EiO2E6MTp7czoxMzoic2VhcmNoRm9ybVRhYiI7czoxMjoiYmFzaWNfc2VhcmNoIjt9fQ=='),('c60f6430-9c14-110d-487a-5c6e1d3b813a','Home2_LEAD',0,'2019-02-21 03:40:01','2019-02-21 03:40:01','1','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),('c7eb84a3-bb8a-614f-efaf-5f4698fcb024','GoogleSync',0,'2020-08-26 17:12:53','2020-08-28 10:06:01','b70243be-ca4c-b256-aff8-5f46980acf9f','YToxOntzOjg6InN5bmNHQ2FsIjtpOjA7fQ=='),('c8d9b741-17a2-d169-808e-5c6e1d97283d','Home2_SUGARFEED',0,'2019-02-21 03:40:01','2019-02-21 03:40:01','1','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),('c9ad1032-c976-b36d-5b82-5f48d52e8c8f','Home2_LEAD_50f8312f-2c19-f7c9-4c0e-5f48d565f8e8',0,'2020-08-28 09:58:25','2020-08-28 09:58:25','1674d634-fb3a-8d8b-34c1-5f4698fed125','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),('cb972034-4e2b-2b82-c286-5e90b284ad9b','Employees2_EMPLOYEE',0,'2020-04-10 17:51:40','2020-04-10 17:52:39','1','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),('d3086dee-d706-6a25-cd92-5f469c001338','SecurityGroups2_SECURITYGROUP',0,'2020-08-26 17:33:13','2020-08-26 17:33:13','1','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),('daa1bccc-b626-cdf0-5660-5f48ec011639','Opportunities2_OPPORTUNITY',0,'2020-08-28 11:38:24','2020-08-28 11:38:24','b70243be-ca4c-b256-aff8-5f46980acf9f','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),('daadea91-54b5-2c48-ac19-5f43bf9a9c89','Leads2_LEAD',0,'2020-08-24 13:23:21','2020-08-24 13:23:21','1','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),('e7aee9bb-838f-068c-9682-5e90b9063df8','Dashboard',0,'2020-04-10 18:22:20','2020-04-10 18:22:21','dc26b995-5550-b848-6f73-5e90aa679348','YTowOnt9'),('e87c66f6-92a3-94ae-3649-5f48d4de3414','Accounts2_ACCOUNT',0,'2020-08-28 09:53:19','2020-08-28 10:06:01','b70243be-ca4c-b256-aff8-5f46980acf9f','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),('ef957f1f-27f6-a34a-c5b3-5f469960f25a','Accounts',0,'2020-08-26 17:18:05','2020-08-28 10:51:42','1','YToxOntzOjE0OiJzdWJwYW5lbExheW91dCI7YToxNzp7aTowO3M6ODoiYWNjb3VudHMiO2k6MTtzOjEzOiJvcHBvcnR1bml0aWVzIjtpOjI7czoxMDoiYWN0aXZpdGllcyI7aTozO3M6NzoiaGlzdG9yeSI7aTo0O3M6OToiZG9jdW1lbnRzIjtpOjU7czo4OiJjb250YWN0cyI7aTo2O3M6OToiY2FtcGFpZ25zIjtpOjc7czo1OiJsZWFkcyI7aTo4O3M6NToiY2FzZXMiO2k6OTtzOjE4OiJhY2NvdW50X2Fvc19xdW90ZXMiO2k6MTA7czoyMDoiYWNjb3VudF9hb3NfaW52b2ljZXMiO2k6MTE7czoyMToiYWNjb3VudF9hb3NfY29udHJhY3RzIjtpOjEyO3M6Mjc6InByb2R1Y3RzX3NlcnZpY2VzX3B1cmNoYXNlZCI7aToxMztzOjMxOiJhY2NvdW50c19zcF90aW1lc2hlZXRfdHJhY2tlcl8xIjtpOjE0O3M6NDoiYnVncyI7aToxNTtzOjc6InByb2plY3QiO2k6MTY7czoxNDoic2VjdXJpdHlncm91cHMiO319'),('f3464777-e8f7-6b95-f9ad-5e90b850a6ba','AM_ProjectTemplates2_AM_PROJECTTEMPLATES',0,'2020-04-10 18:20:09','2020-04-10 18:22:21','dc26b995-5550-b848-6f73-5e90aa679348','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ==');
/*!40000 ALTER TABLE `user_preferences` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` char(36) NOT NULL,
  `user_name` varchar(60) DEFAULT NULL,
  `user_hash` varchar(255) DEFAULT NULL,
  `system_generated_password` tinyint(1) DEFAULT NULL,
  `pwd_last_changed` datetime DEFAULT NULL,
  `authenticate_id` varchar(100) DEFAULT NULL,
  `sugar_login` tinyint(1) DEFAULT '1',
  `first_name` varchar(30) DEFAULT NULL,
  `last_name` varchar(30) DEFAULT NULL,
  `is_admin` tinyint(1) DEFAULT '0',
  `external_auth_only` tinyint(1) DEFAULT '0',
  `receive_notifications` tinyint(1) DEFAULT '1',
  `description` text,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `department` varchar(50) DEFAULT NULL,
  `phone_home` varchar(50) DEFAULT NULL,
  `phone_mobile` varchar(50) DEFAULT NULL,
  `phone_work` varchar(50) DEFAULT NULL,
  `phone_other` varchar(50) DEFAULT NULL,
  `phone_fax` varchar(50) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `address_street` varchar(150) DEFAULT NULL,
  `address_city` varchar(100) DEFAULT NULL,
  `address_state` varchar(100) DEFAULT NULL,
  `address_country` varchar(100) DEFAULT NULL,
  `address_postalcode` varchar(20) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `portal_only` tinyint(1) DEFAULT '0',
  `show_on_employees` tinyint(1) DEFAULT '1',
  `employee_status` varchar(100) DEFAULT NULL,
  `messenger_id` varchar(100) DEFAULT NULL,
  `messenger_type` varchar(100) DEFAULT NULL,
  `reports_to_id` char(36) DEFAULT NULL,
  `is_group` tinyint(1) DEFAULT NULL,
  `factor_auth` tinyint(1) DEFAULT NULL,
  `factor_auth_interface` varchar(255) DEFAULT NULL,
  `hourly_rate` decimal(26,6) DEFAULT '0.000000',
  `timesheet_lock` varchar(20) DEFAULT 'none',
  `require_description` tinyint(1) DEFAULT '0',
  `reminder_to_enter_time` tinyint(1) DEFAULT '0',
  `reminder_time_limit` decimal(18,2) DEFAULT '0.00',
  `users_to_get_reminder` text,
  `sp_timesheet_module` text,
  `sp_timesheet_related_module` text,
  `user_preferred_timesheet_view` text,
  PRIMARY KEY (`id`),
  KEY `idx_user_name` (`user_name`,`is_group`,`status`,`last_name`,`first_name`,`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES ('1','admin','$1$tYUT.9CC$GWseh7RNOtnco6jsBzNUr/',0,NULL,NULL,1,NULL,'Administrator',1,0,1,NULL,'2019-02-21 03:39:38','2019-02-21 03:39:38','1','','Administrator',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Active',NULL,NULL,NULL,NULL,NULL,0,0,1,'Active',NULL,NULL,NULL,0,NULL,NULL,0.000000,'none',0,0,0.00,NULL,NULL,NULL,NULL),('1674d634-fb3a-8d8b-34c1-5f4698fed125','Rick','$1$JlEFUd.a$ibGW2lrJV86qjvi4iJHEE/',0,'2020-08-28 09:50:00',NULL,1,'Rick','M',0,0,1,NULL,'2020-08-26 17:13:48','2020-08-28 16:56:43','1','1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Active',NULL,NULL,NULL,NULL,NULL,0,0,1,'Active',NULL,NULL,'b70243be-ca4c-b256-aff8-5f46980acf9f',0,0,NULL,0.000000,'none',0,0,0.00,NULL,'Home',NULL,'^Daily^,^Weekly^'),('8d544213-bcac-b962-fc13-5f4698c75700','Susan','$1$bRASpcUm$IRJLgAvEouko1c6xlZfbJ.',0,'2020-08-28 09:50:00',NULL,1,'Susan','Chung',0,0,1,NULL,'2020-08-26 17:14:28','2020-08-28 16:55:10','1','1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Active',NULL,NULL,NULL,NULL,NULL,0,0,1,'Active',NULL,NULL,'b70243be-ca4c-b256-aff8-5f46980acf9f',0,0,NULL,0.000000,'none',0,0,0.00,NULL,'Home',NULL,'^Daily^,^Weekly^'),('b70243be-ca4c-b256-aff8-5f46980acf9f','Chris','$1$PxF9RcNO$t/t9iXgs6Uao67ULUfHMJ/',0,'2020-08-28 09:51:02',NULL,1,'Chris','K',0,0,1,NULL,'2020-08-26 17:12:53','2020-08-28 09:51:02','1','1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Active',NULL,NULL,NULL,NULL,NULL,0,0,1,'Active',NULL,NULL,'',0,0,NULL,0.000000,'none',0,0,0.00,NULL,'Home',NULL,'^Daily^,^Weekly^'),('dc26b995-5550-b848-6f73-5e90aa679348','timesheetuser','$1$dKnljyZ.$kgrazqyM55sG3BZ4jclwv.',0,'2020-04-10 18:14:00',NULL,1,'Timesheet','Test User',0,0,1,NULL,'2020-04-10 17:20:57','2020-04-10 18:49:57','1','1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Active',NULL,NULL,NULL,NULL,NULL,0,0,1,'Active',NULL,NULL,'',0,0,NULL,0.000000,'week',1,1,0.00,'^1^','Project','Project_projecttask','^Weekly^');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_feeds`
--

DROP TABLE IF EXISTS `users_feeds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_feeds` (
  `user_id` varchar(36) DEFAULT NULL,
  `feed_id` varchar(36) DEFAULT NULL,
  `rank` int(11) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  KEY `idx_ud_user_id` (`user_id`,`feed_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_feeds`
--

LOCK TABLES `users_feeds` WRITE;
/*!40000 ALTER TABLE `users_feeds` DISABLE KEYS */;
/*!40000 ALTER TABLE `users_feeds` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_last_import`
--

DROP TABLE IF EXISTS `users_last_import`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_last_import` (
  `id` char(36) NOT NULL,
  `assigned_user_id` char(36) DEFAULT NULL,
  `import_module` varchar(36) DEFAULT NULL,
  `bean_type` varchar(36) DEFAULT NULL,
  `bean_id` char(36) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_user_id` (`assigned_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_last_import`
--

LOCK TABLES `users_last_import` WRITE;
/*!40000 ALTER TABLE `users_last_import` DISABLE KEYS */;
/*!40000 ALTER TABLE `users_last_import` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_password_link`
--

DROP TABLE IF EXISTS `users_password_link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_password_link` (
  `id` char(36) NOT NULL,
  `username` varchar(36) DEFAULT NULL,
  `date_generated` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_password_link`
--

LOCK TABLES `users_password_link` WRITE;
/*!40000 ALTER TABLE `users_password_link` DISABLE KEYS */;
/*!40000 ALTER TABLE `users_password_link` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_signatures`
--

DROP TABLE IF EXISTS `users_signatures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_signatures` (
  `id` char(36) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `user_id` varchar(36) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `signature` text,
  `signature_html` text,
  PRIMARY KEY (`id`),
  KEY `idx_usersig_uid` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_signatures`
--

LOCK TABLES `users_signatures` WRITE;
/*!40000 ALTER TABLE `users_signatures` DISABLE KEYS */;
/*!40000 ALTER TABLE `users_signatures` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vcals`
--

DROP TABLE IF EXISTS `vcals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vcals` (
  `id` char(36) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `user_id` char(36) NOT NULL,
  `type` varchar(100) DEFAULT NULL,
  `source` varchar(100) DEFAULT NULL,
  `content` text,
  PRIMARY KEY (`id`),
  KEY `idx_vcal` (`type`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vcals`
--

LOCK TABLES `vcals` WRITE;
/*!40000 ALTER TABLE `vcals` DISABLE KEYS */;
/*!40000 ALTER TABLE `vcals` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'suitecrm'
--

--
-- Dumping routines for database 'suitecrm'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-09-01 17:46:11
