<?php
/**
 * Copyright (c) SalesAgility Ltd.  All Rights Reserved.
 * Licensed under the GPL3 License.  See License in the project root
 * for license information.
 *
 * @package Outlook Addin for SuiteCRM
 * @copyright 2020 SalesAgility Ltd http://www.salesagility.com
 * @author SalesAgility <info@salesagility.com
 *
 */

namespace SalesAgility\Outlook;

use BeanFactory;
use SalesAgility\Outlook\SuiteStore\SuiteStoreAPI;
use SalesAgility\Outlook\SuiteStore\SuiteStoreService;

require_once "custom/include/SA_Outlook/vendor/autoload.php";

class Outlook
{
    /**
     * @var array
     */
    private $settings;

    /**
     * @var bool
     */
    private $licenced;

    /**
     * @var string
     */
    private $key = 'sa_outlook';

    /**
     * @var int
     */
    private $period = 86400;


    public function __construct()
    {
        $this->settings = $this->loadSettings();
        $this->licenced = $this->isValidLicence();
    }

    /**
     * @return array
     */
    public function getSettings()
    {
        return $this->settings;
    }

    /**
     * @return bool
     */
    public function isLicenced()
    {
        return $this->licenced;
    }

    /**
     * @return SuiteStoreService
     */
    public function getStoreService()
    {
        return new SuiteStoreService(new SuiteStoreAPI($this->LoadStoreConfig()),  $this->licenced);
    }

    /**
     * @param string $key
     */
    public function validate($key)
    {
        $store = $this->getStoreService();
        $result = $store->doValidate($key);
        $store->updateStatus($result['success']);

        $this->settings['validation_time'] = time();
        $this->settings['is_valid'] = $result['success'] ? true : false;
        if ($result['success']) {
            $this->settings['sa_outlook_license_key'] = $key;
        } else {
            $this->settings['sa_outlook_license_key'] = '';
        }
        $this->saveSettings($this->settings);

        return $result;
    }


    /**
     * @param array $settings
     */
    public function saveSettings($settings)
    {
        $store = blowfishEncode(blowfishGetKey($this->key), json_encode($settings));
        $administration = BeanFactory::getBean('Administration');
        $administration->saveSetting($this->key,'settings', $store);
    }

    /**
     * @return array
     */
    private function loadSettings()
    {
        $key = $this->key . '_settings';
        $administration = BeanFactory::getBean('Administration');
        $administration->retrieveSettings();

        if (isset($administration->settings[$key])) {
            return json_decode(blowfishDecode(blowfishGetKey($this->key), $administration->settings[$key]), true);
        }

        return [];

    }

    /**
     * @return bool
     */
    private function isValidLicence()
    {
        if (!empty($this->settings['sa_outlook_license_key'])
            && ((!empty($this->settings['validation_time'])
                    && $this->settings['validation_time'] + $this->period >= time())
                || empty($this->settings['validation_time']))
        ) {
            $this->validate($this->settings['sa_outlook_license_key']);
        }

        if (!empty($this->settings['is_valid'])) {
            return true;
        }

        return false;
    }

    /**
     * @return array
     */
    private function LoadStoreConfig()
    {
        $storeConfig = [];
        if(file_exists('custom/include/SA_Outlook/SuiteStore/config.php')){
            include 'custom/include/SA_Outlook/SuiteStore/config.php';
        }
        return $storeConfig;
    }

}
