<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

$license_strings = array (
    'LBL_STEPS_TO_LOCATE_KEY_TITLE' => 'Para localizar a chave',
    'LBL_STEPS_TO_LOCATE_KEY_1' => '1. Entre para',
    'LBL_STEPS_TO_LOCATE_KEY_2' => '2. Ir a Account->Purchases',
    'LBL_STEPS_TO_LOCATE_KEY_3' => '3. Localize a chave para a compra deste add-on',
    'LBL_STEPS_TO_LOCATE_KEY_4' => '4. Cole na caixa de Chave de Licen�a a seguir',
    'LBL_STEPS_TO_LOCATE_KEY_5' => '5. Clique em "Validate"',
    'LBL_LICENSE_KEY' => 'Chave de Licen�a',
    'LBL_CURRENT_USERS' => 'Atual Contagem Usu�rio',
    'LBL_LICENSED_USERS' => 'Licensed Contagem Usu�rio',
    'LBL_VALIDATE_LABEL' => 'Validar',
    'LBL_VALIDATED_LABEL' => 'Validado',
);

