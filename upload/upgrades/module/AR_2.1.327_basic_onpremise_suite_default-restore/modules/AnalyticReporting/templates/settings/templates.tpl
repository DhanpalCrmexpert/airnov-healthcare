
{include file="modules/$moduleName/templates/settings/pro_notification.tpl"}

<h2 style="color:#333;font-size:1.5em;text-align:left;">{$MOD.templateManagement}</h2>
<label style="font-size:1.4em;">{$MOD.uploadNewTemplate}</label>
<div>
    <table width="350" border="0" cellpadding="1" cellspacing="1" class="box">
        <tr>
            <td width="246">
                <input type="file" disabled>
            </td>
            <td width="80"><input class="ar-button green" type="submit" class="box" value=" Upload " disabled></td>
        </tr>
    </table>
</div>

<br/><br/>

<label style="font-size:1.4em;">{$MOD.editExistingTemplate}</label>
<table>
    <thead>
    <tr>
        <th>{$MOD.label_title}</th>
        <th>{$MOD.updateTemplate}</th>
        <th></th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    </tbody>
</table>

<br/><br/>

<label style="font-size:1.4em;">{$MOD.label_google_drive_setup}</label>
<div>
    <div>
        <div>
            <label>
                <input type="checkbox" name="googleSyncEnabled" disabled /> {$MOD.label_enable_sync_with_google_drive}
            </label>
        </div>
    </div>
</div>

