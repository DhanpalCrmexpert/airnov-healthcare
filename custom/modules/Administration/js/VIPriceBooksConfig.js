/*********************************************************************************
 * This file is part of package Price Books.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Price Books is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/

if(enable == 0){
	$('#price_books_link').hide();
}else if(enable == 1){
	$('#price_books_link').show();
}
//Enabled Price Book Feature
$('#enabled_price_books').on('change', function() {
	
	var check_value;
	if($(this).is(':checked')){
		check_value = 1;
	}else{
	  	check_value = 0;
	}

	$.ajax({
      	url : 'index.php?entryPoint=VIPriceBooksConfigEnable',
      	type: 'POST',
      	data : {val : check_value},
      	success : function(data) {
		    if(check_value == 1) {
			    alert(SUGAR.language.get('Administration','LBL_PRICE_BOOKS_ACTIVATED'));
			  	$('#price_books_link').show();
		    }else{
		    	alert(SUGAR.language.get('Administration','LBL_PRICE_BOOKS_DEACTIVATED'));
		    	$('#price_books_link').hide();
		    }
     	}
  	});
});