<?php
$mod_strings['LBL_OUTLOOK_USERS_SUBPANEL_TITLE'] = 'Outlook Users';
$mod_strings['LBL_SA_OUTLOOK_SYNC_DATE'] = 'Last Outlook Sync';
$mod_strings['LBL_SA_OUTLOOK_USER_SETTINGS'] = 'Outlook Sync Settings';
$mod_strings['LBL_SA_OUTLOOK_USER_ENABLE'] = 'Enable Outlook Sync';
$mod_strings['LBL_SA_OUTLOOK_USER_IS_LICENSED'] = 'Licensed For Outlook';
$mod_strings['LBL_SA_OUTLOOK_USER_FOLDERS'] = 'Synced folders';
