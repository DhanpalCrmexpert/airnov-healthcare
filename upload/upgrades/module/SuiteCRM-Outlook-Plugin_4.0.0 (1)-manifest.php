<?php
$manifest = [
    'name' => 'SuiteCRM Outlook Plugin',
    'type' => 'module',
    'author' => 'SalesAgility',
    'version' => '4.0.0',
    'published_date' => '2020-08-31',
    'is_uninstallable' => true,
    'remove_tables' => false,
    'description' => 'Official SuiteCRM Outlook Plugin',
    'acceptable_suitecrm_versions' => [
        'regex_matches' => [
            '7.10.*',
            '7.11.*'
        ],
    ],
];

$installdefs = [
    'id' => 'SA_Outlook',
    'post_uninstall' => ['<basepath>/scripts/post_uninstall.php'],
    'copy' => [
        '0' => [
            'from' => '<basepath>/plugin/app/',
            'to' => 'public/plugins/SA_Outlook/',
        ],
        '1' => [
            'from' => '<basepath>/plugin/base/',
            'to' => 'custom/include/SA_Outlook/',
        ],
        '5' => [
            'from' => '<basepath>/plugin/overrides/application/Ext/Api/V8/Config/routes.php',
            'to' => 'custom/application/Ext/Api/V8/Config/routes.php',
        ],
        '6' => [
            'from' => '<basepath>/plugin/overrides/application/Ext/Api/V8/controllers.php',
            'to' => 'custom/application/Ext/Api/V8/controllers.php',
        ],
        '7' => [
            'from' => '<basepath>/plugin/overrides/Extension/application/Ext/EntryPointRegistry/SA_Outlook_EntryPoints.php',
            'to' => 'custom/Extension/application/Ext/EntryPointRegistry/SA_Outlook_EntryPoints.php',
        ],
        '8' => [
            'from' => '<basepath>/plugin/overrides/Extension/application/Ext/Language/en_us.sa_outlook.php',
            'to' => 'custom/Extension/application/Ext/Language/en_us.sa_outlook.php',
        ],
        '9' => [
            'from' => '<basepath>/plugin/overrides/Extension/modules/Administration/Ext/Administration/OutlookAddinConfig.php',
            'to' => 'custom/Extension/modules/Administration/Ext/Administration/OutlookAddinConfig.php',
        ],
        '10' => [
            'from' => '<basepath>/plugin/overrides/Extension/modules/Administration/Ext/Language/en_us.OutlookAddinConfig.php',
            'to' => 'custom/Extension/modules/Administration/Ext/Language/en_us.OutlookAddinConfig.php',
        ],
        '11' => [
            'from' => '<basepath>/plugin/overrides/Extension/modules/Schedulers/Ext/Language/en_us.OutlookSyncScheduler.php',
            'to' => 'custom/Extension/modules/Schedulers/Ext/Language/en_us.OutlookSyncScheduler.php',
        ],
        '12' => [
            'from' => '<basepath>/plugin/overrides/Extension/modules/Schedulers/Ext/ScheduledTasks/OutlookSyncScheduler.php',
            'to' => 'custom/Extension/modules/Schedulers/Ext/ScheduledTasks/OutlookSyncScheduler.php',
        ],
        '13' => [
            'from' => '<basepath>/plugin/overrides/Extension/modules/Users/Ext/Language/en_us.sa_outlook_fields.php',
            'to' => 'custom/Extension/modules/Users/Ext/Language/en_us.sa_outlook_fields.php',
        ],
        '14' => [
            'from' => '<basepath>/plugin/overrides/Extension/modules/Users/Ext/Layoutdefs/outlookAdmin.php',
            'to' => 'custom/Extension/modules/Users/Ext/Layoutdefs/outlookAdmin.php',
        ],
        '15' => [
            'from' => '<basepath>/plugin/overrides/Extension/modules/Users/Ext/LogicHooks/sa_outlook_update_folders.php',
            'to' => 'custom/Extension/modules/Users/Ext/LogicHooks/sa_outlook_update_folders.php',
        ],
        '16' => [
            'from' => '<basepath>/plugin/overrides/modules/Administration/OutlookAddinConfig.php',
            'to' => 'custom/modules/Administration/OutlookAddinConfig.php',
        ],
        '17' => [
            'from' => '<basepath>/plugin/overrides/modules/Users/services/OnUpdateFolders.php',
            'to' => 'custom/modules/Users/services/OnUpdateFolders.php',
        ],
        '18' => [
            'from' => '<basepath>/plugin/overrides/modules/Users/tpls/EditViewFooter.tpl',
            'to' => 'custom/modules/Users/tpls/EditViewFooter.tpl',
        ],
        '19' => [
            'from' => '<basepath>/plugin/overrides/modules/Users/tpls/EditViewHeader.tpl',
            'to' => 'custom/modules/Users/tpls/EditViewHeader.tpl',
        ],
        '20' => [
            'from' => '<basepath>/plugin/overrides/modules/Users/views/view.edit.php',
            'to' => 'custom/modules/Users/views/view.edit.php',
        ],

    ],
    'vardefs' => [
        '0' => [
            'from' => '<basepath>/plugin/overrides/Extension/modules/Users/Ext/Vardefs/sa_outlook_fields.php',
            'to_module' => 'Users',
        ],
    ],
];
