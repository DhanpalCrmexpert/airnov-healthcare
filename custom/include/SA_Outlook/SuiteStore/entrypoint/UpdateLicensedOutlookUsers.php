<?php
/**
 * Copyright (c) SalesAgility Ltd.  All Rights Reserved.
 * Licensed under the GPL3 License.  See License in the project root
 * for license information.
 *
 * @package Outlook Addin for SuiteCRM
 * @copyright 2020 SalesAgility Ltd http://www.salesagility.com
 * @author SalesAgility <info@salesagility.com
 *
 */

if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

require_once 'custom/include/SA_Outlook/Outlook.php';

$outlook = new SalesAgility\Outlook\Outlook();

$users = $_REQUEST['users'];
$update = filter_var($_REQUEST['update'], FILTER_VALIDATE_BOOLEAN);

$suiteStoreService = $outlook->getStoreService();
$settings = $outlook->getSettings();
$result = $suiteStoreService->updateValidUsers($users, $settings, $update);

$json = getJSONobj();
$result = $json::encode($result);
print($result);
