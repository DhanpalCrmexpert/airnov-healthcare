<?php
/**
 * Copyright (c) SalesAgility Ltd.  All Rights Reserved.
 * Licensed under the GPL3 License.  See License in the project root
 * for license information.
 *
 * @package Outlook Addin for SuiteCRM
 * @copyright 2020 SalesAgility Ltd http://www.salesagility.com
 * @author SalesAgility <info@salesagility.com
 *
 */

use Api\V8\BeanDecorator\BeanManager;
use Api\V8\Controller\BaseController;
use Slim\Http\Request;
use Slim\Http\Response;

class SAOutlookAPIController extends BaseController
{
    /*
     * @Var BeanManager
     */
    protected $beanManager;
    protected $moduleMap = [
        'Other' => ['name'],
        'Person' => ['first_name', 'last_name']
    ];

    public function __construct(BeanManager $beanManager)
    {
        $this->beanManager = $beanManager;
    }

    public function mapModules($moduleArray)
    {
        $returnArray = [];
        foreach ($moduleArray as $mod) {
            $bean = BeanFactory::newBean($mod);
            if($bean instanceof File) {
                $returnArray[$mod] = [];
            } elseif ($bean instanceof Person) {
                $returnArray[$mod] = $this->moduleMap['Person'];
            } else {
                $returnArray[$mod] = $this->moduleMap['Other'];
            }
        }
        return $returnArray;
    }

    public function search(Request $request, Response $response, array $args)
    {
        $email = $request->getParam('email');
        $name = $request->getParam('name');
        if (!empty($request->getParam('modules'))) {
            $modules = explode(',', $request->getParam('modules'));
            $searchModules = $this->mapModules($modules);
        } else {
//            If no modules in params, default to all
            $searchModules = $this->moduleMap;
        }

        $results = [];

        foreach ($searchModules as $module => $def) {
            if (empty($def)) {
                continue;
            }
            $b = BeanFactory::getBean($module);
            $results[$module] = [];

            $where = [];
            foreach ($def as $d) {
                foreach (explode(" ", $name) as $bit) {
                    $where[] = $b->table_name . "." . $d . " LIKE '%" . $b->db->quote($bit) . "%'";
                }
            }
            $res = $this->beanManager->getList($module)->where(implode(" OR ", $where))->fetch();
            $beans = $res->getBeans();
            $i = 0;
            foreach ($beans as $bean) {
                $results[$bean->module_name][$i]['name'] = $bean->name;
                $results[$bean->module_name][$i]['id'] = $bean->id;
                if ($bean->emailAddress) {
                    $results[$bean->module_name][$i]['email'] = $bean->emailAddress->getPrimaryAddress($bean);
                }
                if($bean->account_name) {
                    $results[$bean->module_name][$i]['account_name'] = $bean->account_name;
                }
                $i++;
            }
        }
        $se = new SugarEmailAddress();
        $beans = $se->getBeansByEmailAddress($email);
        foreach ($beans as $bean) {
            if (!array_key_exists($bean->module_name, $searchModules)) {
                continue;
            }
            switch ($bean->module_name) {
                case 'Accounts' :
                    $results[$bean->module_name][] = [
                        "name" => $bean->name,
                        "email" => $bean->email1,
                        "id" => $bean->id
                    ];
                    break;
                default :
                    $results[$bean->module_name][] = [
                        "account_name" => $bean->account_name,
                        "name" => $bean->name,
                        "email" => $bean->email1,
                        "id" => $bean->id
                    ];
            }
        }
        return $this->generateResponse($response, $results, 200);
    }
}
