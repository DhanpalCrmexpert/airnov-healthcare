<?php

if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

global $current_user, $sugar_config;

$timer_record_id = $_POST['timer_record_id'];

stopAutoTrackTimer($timer_record_id);

function stopAutoTrackTimer($timer_record_id){
	global $db;
	$auto_time_res = $db->query("select auto_time_tracker_id from suh_timer_recording where id='".$timer_record_id."'");
	$auto_time = $db->fetchByAssoc($auto_time_res);
	if(isset($auto_time['auto_time_tracker_id'])){
		$auto_timesheet_time_tracker_obj = BeanFactory::getBean('sp_Auto_TimeSheet_Time_Tracker', $auto_time['auto_time_tracker_id']);
		if(!empty($auto_timesheet_time_tracker_obj->id)){
			$auto_timesheet_time_tracker_obj->processed = 1;
			$auto_timesheet_time_tracker_obj->save();
			$related_record_id = $auto_timesheet_time_tracker_obj->parent_id;
			$selected_record_id =$auto_timesheet_time_tracker_obj->selected_module_id; 

			stopTimerRecording($related_record_id, $selected_record_id, $timer_record_id);
			echo "Auto Time tracker stopped successfully.";
		}
	}
	exit;
}


function stopTimerRecording($related_record_id, $selected_record_id, $timer_record_id){
	global $db;
	$end_date = gmdate("Y-m-d H:i:s");
	$select_res = $db->query("select id,time_spend from suh_timer_recording  WHERE id='".$timer_record_id."'", true);
	$row = $db->fetchByAssoc($select_res);
	if(isset($row['id'])){
		$sql = "UPDATE suh_timer_recording set timer_ended=1, end_date='".$end_date."' where id='".$row['id']."'";
		$db->query($sql, true);
		setSpTimeTracker($related_record_id, $selected_record_id, $row['time_spend']);
	}
}

function setSpTimeTracker($related_record_id, $selected_record_id, $time_spent){
	global $db, $current_user;
	$condition = " AND projecttask_id_c='{$related_record_id}' AND parent_id = '{$selected_record_id}' ";
	$sql = "select id, hours, timesheet_id, track_date from sp_timesheet_tracker where deleted=0 AND user_id_c='{$current_user->id}' {$condition} order by date_modified desc limit 1";

	$sql_res = $db->query($sql, true);
	$row = $db->fetchByAssoc($sql_res);
	if(isset($row['id'])){
		$time_part = $row['hours'];
		$minutes = (int)$time_spent;
		$time_to_save = number_format($time_part + ($minutes/60), 4);
		$db->query("UPDATE sp_timesheet_tracker set hours = '{$time_to_save}' where id = '".$row['id']."'");
		if(isset($row['timesheet_id'])){
			$sql_timesheet = "UPDATE sp_timesheet set hours=(SELECT sum(hours) hours FROM sp_timesheet_tracker WHERE deleted = 0 AND user_id_c = '{$current_user->id}' AND timesheet_id = '".$row['timesheet_id']."') where id = '".$row['timesheet_id']."' and deleted=0";
			$db->query($sql_timesheet, true);
		}
	}
}

?>