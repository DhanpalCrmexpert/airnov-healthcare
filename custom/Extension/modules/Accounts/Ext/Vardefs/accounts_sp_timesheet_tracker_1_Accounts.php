<?php
// created: 2016-09-26 21:38:56
$dictionary["Account"]["fields"]["accounts_sp_timesheet_tracker_1"] = array (
  'name' => 'accounts_sp_timesheet_tracker_1',
  'type' => 'link',
  'relationship' => 'accounts_sp_timesheet_tracker_1',
  'source' => 'non-db',
  'module' => 'sp_timesheet_tracker',
  'bean_name' => 'sp_timesheet_tracker',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_SP_TIMESHEET_TRACKER_1_FROM_SP_TIMESHEET_TRACKER_TITLE',
);
