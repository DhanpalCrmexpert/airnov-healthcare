<?php
/*********************************************************************************
 * This file is part of package Price Books.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Price Books is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
$mod_strings['LBL_PRICE_BOOKS'] = "Price Books";
$mod_strings['LBL_PRICE_BOOKS_DESCRIPTION'] = "Manage Price Books of Products for particular Contacts/Accounts.";
$mod_strings['LBL_ENABLE_PRICE_BOOKS'] = "Enable Price Books Feature";
$mod_strings['LBL_PRICE_BOOKS_ACTIVATED'] = "Price Books Feature Activated Successfully.";
$mod_strings['LBL_PRICE_BOOKS_DEACTIVATED']= "Price Books Feature Deactivated Successfully.";
$mod_strings['LBL_CLICK_HERE'] = "Click Here";
$mod_strings['LBL_MANAGE_PRICEBOOK'] = "To Manage Price Books";
//license
$mod_strings['LBL_LICENCE_ACTIVE_LABEL'] = "VIPriceBooksLicenseAddon is no longer active due to the following reason:";
$mod_strings['LBL_LICENCE_ISSUES'] = "Users will have limited to no access until the issue has been addressed";
$mod_strings['LBL_CLICK_HERE'] = "Click Here";
$mod_strings['LBL_LICENCE_ACTIVE'] = "VIPriceBooksLicenseAddon is no longer active";
$mod_strings['LBL_RENEW_LICENCE'] = "Please renew your subscription or check your license configuration.";
$mod_strings['LBL_UPDATE_LICENSE'] = "Update License";
?>