/*********************************************************************************
 * This file is part of package Price Books.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Price Books is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
$('.primary').attr("onclick","customSave('primary')");
$('.primary').attr("type","button");
$('.saveAndContinue').attr("onclick","customSave('saveAndContinue')");
$('.saveAndContinue').attr("type","button");
    
//Add Custom Save Method
function customSave(elementName){
    var productEntryCount = $('#productEntryCount').val();
    if(productEntryCount > 0){
        var flag = false;
        var quantityFlag = false;
        var productNameFlag = false;
        var listPriceFlag = false;
        var productPriceFlag = false;
        var addQuantityFlag = false;

        for(var i=1;i<=productEntryCount;i++){
            var productName = $('#product_name'+i).val();
            var listPrice = $('#list_price'+i).val();
            var productPrice = $('#product_price'+i).val();
            var priceType  = $('#price_type'+i).val();

            if(productName == "" && productPrice == "" && listPrice == "" ){
                flag = true;
            }else if(productName == ''){
                productNameFlag = true;
            }else if(productPrice == ''){
                productPriceFlag = true;
            }else if(listPrice == ''){
                listPriceFlag = true;
            }

            if(priceType == "2"){
                var quantityCount = $('#percenPriceEntryCount'+i).val();
            }
            if(priceType == "3"){
                var quantityCount = $('#dollarPriceEntryCount'+i).val();
            }
            if(priceType == "2" || priceType == "3"){
                if(quantityCount > 0 ){
                    for(var k=1;k<=quantityCount;k++){
                        from_quantity  = $('input[name="from_quantity'+i+'_'+k+'"]').val();
                        to_quantity = $('input[name="to_quantity'+i+'_'+k+'"]').val();
                        differential = $('input[name="differential'+i+'_'+k+'"]').val();
                        if(from_quantity == '' || to_quantity == '' || differential == ''){
                            quantityFlag = true;
                        }
                    }
                }else{
                    addQuantityFlag = true;
                }   
            }
        }//end of for loop

        if(flag){
            alert(SUGAR.language.get('VI_Price_Books','LBL_PRODUCT_FIELDS_VALIDATION'));
        }else if(productNameFlag){
            alert(SUGAR.language.get('VI_Price_Books','LBL_PRODUCT_VALID'));
        }else if(productPriceFlag){
            alert(SUGAR.language.get('VI_Price_Books','LBL_PRODCUT_PRICE_VALID'));
        }else if(listPriceFlag){
            alert(SUGAR.language.get('VI_Price_Books','LBL_LIST_PRICE_VALID'));
        }else if(addQuantityFlag){
            alert(SUGAR.language.get('VI_Price_Books','LBL_QUANTITY_VALID_RANGE'));
        }else if(quantityFlag){
            alert(SUGAR.language.get('VI_Price_Books','LBL_QUANTITY_FIELD_VALIDATION'));
        }else{
           getSaveClickMethod(elementName)
        }
    }else{
        getSaveClickMethod(elementName)
    }  
}//end of function
function getSaveClickMethod(elementName){
    if(elementName == "primary"){
        $('.primary').attr("onclick","var _form = document.getElementById('EditView'); _form.action.value='Save'; if(check_form('EditView'))SUGAR.ajaxUI.submitForm(_form);return false;"); 
        $('.primary').click();
    }else if(elementName == "saveAndContinue"){
        $('.saveAndContinue').attr("onclick","SUGAR.saveAndContinue(this)");
        $('.saveAndContinue').click();
    }
}//end of function
