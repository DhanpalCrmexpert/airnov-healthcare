<?php

/**
 * Dashboard edit view
 */
class AnalyticReportingViewDashboard extends SugarView
{
    public function display()
    {
        global $sugar_flavor;
        $config = new ARConfigUtils(new ARConfigService());
        if ($config->isBasicPlan()) {
            echo $this->ss->fetch('modules/AnalyticReporting/templates/dashboard.tpl');
            die;
        }

        $this->ss->assign('MOD', PlatformConnector::getModuleTranslations());
        $this->ss->assign('APP', PlatformConnector::getAppTranslations());
        $this->ss->assign('DICTIONARY', json_encode(PlatformConnector::getModuleTranslations())); // #5286 - JSON object of translations (should merge with app_strings)
        $this->ss->assign('sugar_flavor', $sugar_flavor); // #6161
        $this->ss->assign('systemType', PlatformConnector::getSystemType()); // #6161
        $this->ss->assign('site_url', SugarConfig::getInstance()->get('site_url')); // #6161

        // Auto assign all view object map items to smarty
        foreach($this->view_object_map as $key => $value) {
            $this->ss->assign($key, $value);
        }

        echo $this->ss->fetch('modules/AnalyticReporting/templates/dashboard.tpl');
    }
}