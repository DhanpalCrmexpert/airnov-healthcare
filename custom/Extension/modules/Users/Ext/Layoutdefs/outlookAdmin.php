<?php

$layout_defs['outlookAdmin']['subpanel_setup']['outlook_users'] = [
    'order' => 1,
    'module' => 'Users',
    'subpanel_name' => 'default',
    'get_subpanel_data' => 'function:getOutlookUsers',
    'function_parameters' => [
        'import_function_file' => 'custom/include/SA_Outlook/admin/utils.php',
    ],
    'title_key' => 'LBL_OUTLOOK_USERS_SUBPANEL_TITLE',
    'top_buttons' => [
        [
            'widget_class' => 'SubPanelTopSelectButton',
            'popup_module' => 'Users',
            'mode' => 'MultiSelect'
        ],
    ],
];