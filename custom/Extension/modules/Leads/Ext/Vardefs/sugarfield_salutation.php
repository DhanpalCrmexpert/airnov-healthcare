<?php
 // created: 2020-11-18 17:10:57
$dictionary['Lead']['fields']['salutation']['len']=100;
$dictionary['Lead']['fields']['salutation']['required']=true;
$dictionary['Lead']['fields']['salutation']['inline_edit']=true;
$dictionary['Lead']['fields']['salutation']['comments']='Contact salutation (e.g., Mr, Ms)';
$dictionary['Lead']['fields']['salutation']['merge_filter']='disabled';

 ?>