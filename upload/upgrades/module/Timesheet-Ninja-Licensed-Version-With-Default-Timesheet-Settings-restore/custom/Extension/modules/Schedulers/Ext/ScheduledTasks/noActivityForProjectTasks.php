<?php 
array_push($job_strings, 'no_Activity_For_Project_Tasks');

function no_Activity_For_Project_Tasks(){
	global $db;
	$current_date = date("Y-m-d");
	$getProjectTaskQry = "SELECT pt.id as project_task_id, pt.name as project_task_name, pt.assigned_user_id FROM project_task pt WHERE pt.status IN('Not Started', 'Draft') AND pt.is_billable = 'yes' AND pt.deleted = 0";
	
	$resProjectTask = $db->query($getProjectTaskQry);
	while($rowProjectTaskId = $db->fetchByAssoc($resProjectTask))
	{
		$project_task_id = $rowProjectTaskId['project_task_id'];
		$assigned_user_id = $rowProjectTaskId['assigned_user_id'];
		$project_task_name = $rowProjectTaskId['project_task_name'];
		
		$timesheet_tracker_obj = new sp_timesheet_tracker();
		$timesheet_tracker_obj->retrieve_by_string_fields(array('track_date' => $current_date, 'projecttask_id_c' => $project_task_id));
		
		if(empty($timesheet_tracker_obj->id) || $timesheet_tracker_obj->hours<=0 ){
			sendEmailNotication($assigned_user_id, $project_task_id, $project_task_name);
		}
	}
	return true;
}

function sendEmailNotication($assigned_user_id, $record_id, $record_name){
	global $sugar_config;
	require_once('modules/EmailTemplates/EmailTemplate.php');
	
	$user_obj = BeanFactory::getBean("Users", $assigned_user_id);
	$email_address = $user_obj->email1;
	if(empty($email_address)){
		return;
	}
			
	$objTemplate = new EmailTemplate();
	$objTemplate->retrieve_by_string_fields(array('name' => 'Project Task - No Activity'));
	
	$link = $sugar_config['site_url'].'/index.php?action=DetailView&module=ProjectTask&record='. $record_id;
	
	if (!empty($objTemplate->id)) {
		
		$subject = $objTemplate->subject;
		$subject = str_replace('{RELATED_RECORD}', $record_name, $subject);
		
		$body_html = $objTemplate->body_html;
		$body = $objTemplate->body;
	
		$body_html = str_replace('{LINK}', $link, $body_html);
		$body = str_replace('{LINK}', $link, $body);
		
		$body_html = str_replace('{CURRENT_USER}', $user_obj->name, $body_html);
		$body = str_replace('{CURRENT_USER}', $user_obj->name, $body);
		
		$body_html = str_replace('{RELATED_RECORD}', $record_name, $body_html);
		$body = str_replace('{RELATED_RECORD}', $record_name, $body);
	}
	else{
		$subject = "No Activity on Project Task ".$record_name;
		
		$body_html = "Hi ".$user_obj->name. ",<br/> <br/>
					
					There is no time tracking activity for Project Task '".$record_name."'.<br/>
					You can start the timer by redirecting to this link: ".$link." <br/> <br/>

					Thank you.<br/> <br/>

					Timesheets Professional System.<br/> <br/>";
		$body = $body_html;
	}
	
	require_once("include/SugarPHPMailer.php");
	$note_msg=new SugarPHPMailer();
	$admin = new Administration();
	$admin->retrieveSettings();
	$note_msg->Subject = $subject;
	$note_msg->prepForOutbound();
	$note_msg->setMailerForSystem();
	
	$note_msg->IsHTML(true);
	$note_msg->From     = $admin->settings['notify_fromaddress'];
	$note_msg->FromName = $admin->settings['notify_fromname'];
	$note_msg->AddAddress($email_address);
	$note_msg->Body = from_html($body_html);
	$note_msg->AltBody = from_html($body);
	
	if (!$note_msg->Send()){
		 $GLOBALS['log']->info("Could not send email: " . $note_msg->ErrorInfo);
	}
}

?>