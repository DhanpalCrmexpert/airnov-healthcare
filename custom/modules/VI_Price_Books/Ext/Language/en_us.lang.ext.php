<?php 
 //WARNING: The contents of this file are auto-generated


/*********************************************************************************
 * This file is part of package Price Books.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Price Books is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
$mod_strings['LBL_ACTIVE'] = 'Active';
$mod_strings['LBL_PERCENTAGE_MARK'] = 'Percentage Mark';
$mod_strings['LBL_TYPE'] = 'Price Type';
$mod_strings['LBL_ROUND_OFF'] = 'Round Off To';
$mod_strings['LBL_ACCOUNT_NAME'] = 'Account Name';
$mod_strings['LBL_ACCOUNT_ID'] = 'Account ID';
$mod_strings['LBL_ACCOUNTS'] = 'Accounts';
$mod_strings['LBL_CONTACT_NAME'] = 'Contact Name';
$mod_strings['LBL_CONTACT_ID'] = 'Contact ID';
$mod_strings['LBL_CONTACTS'] = 'Contacts';
$mod_strings['LBL_FLAT_PRICE'] = 'Flat Price';
$mod_strings['LBL_DIFFERENTIAL_PERCENTAGE'] = 'Differential By(%)';
$mod_strings['LBL_DIFFERENTIAL_DOLLAR'] = 'Differential By($)';
$mod_strings['LBL_PRICE_BOOKS'] = 'Price Books';
$mod_strings['LBL_NAME'] = 'Name';
$mod_strings['LBL_PRICE_TYPE'] = 'Price Type';
$mod_strings['LBL_ASSIGNED_TO'] = 'Assigned To';
$mod_strings['LBL_NO_RECORDS'] = 'No Records Found';
$mod_strings['LBL_MARK_UP'] = 'MarkUp(Add)';
$mod_strings['LBL_MARK_DOWN'] = 'MarkDown(Sub)';
$mod_strings['LBL_ADD_QUANTITY'] = 'Add Quantity';
$mod_strings['LBL_PRODUCT_NAME'] = 'Product Name';
$mod_strings['LBL_PRODUCT_PRICE'] = 'Product Price';
$mod_strings['LBL_LIST_PRICE'] = 'List Price';
$mod_strings['LBL_ADD_PRODUCT'] = 'Add Product';
$mod_strings['LBL_YES'] = 'Yes';
$mod_strings['LBL_NO'] = 'No';
$mod_strings['LBL_SELECT_PRODUCT_MSG'] = "Please Select Product before clicking on the Price Books!!!";
$mod_strings['LBL_SELECT_PRICEBOOK_MSG'] = "Please Select Only One Price Book.";
$mod_strings['LBL_PRODUCT_VALIDATION'] = 'Please Select Another Product. This Product is Already Selected.';
$mod_strings['LBL_QUANTITY_VALID_MSG'] = "Invalid Value:";
$mod_strings['LBL_FROM_QUANTITY'] = "From Quantity";
$mod_strings['LBL_TO_QUANTITY'] = "To Quantity";
$mod_strings['LBL_DIFFERENTIAL'] = "Differential";
$mod_strings['LBL_PRICE_BOOKS_ACCESS'] = "Please Enable Price Books Feature for accessing Price Books Module.";
$mod_strings['LBL_CLICK_HERE'] = "Click Here";
$mod_strings['LBL_ENABLE_LABEL'] = "for Enable it.";
$mod_strings['LBL_FROM_QUANTITY_VALID'] = "Please Enter Greater From Quantity Value Than Previous To Quantity Value.";
$mod_strings['LBL_TO_QUANTITY_VALID'] = "Please Enter Greater To Quantity Value Than From Quantity Value.";
$mod_strings['LBL_PRODUCT_LIST_PRICE_VALID'] = "Please Add Product and List Price for Add Quantity.";
$mod_strings['LBL_PRODCUT_PRICE_VALID'] = "Please Add Product Price for Product.";
$mod_strings['LBL_LIST_PRICE_VALID'] = "Please Add List Price for Product.";
$mod_strings['LBL_QUANTITY_VALID_RANGE'] = "Please Add Quantity Range for Product.";
$mod_strings['LBL_PRODUCT_VALID'] = "Please Add Product for Add Quantity.";
$mod_strings['LBL_PRODUCT_FIELDS_VALIDATION'] = "Please Add Product Name and List Price for Price Books.";
$mod_strings['LBL_QUANTITY_FIELD_VALIDATION']="Please Fill Quantity Range for Empty Slabs.";
$mod_strings['LBL_EDITVIEW_PANEL1'] = "Price Books Details";
$mod_strings['LBL_DETAILVIEW_PANEL1'] = "Price Books Details";
 
?>