<?php
/*********************************************************************************
 * This file is part of package Price Books.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Price Books is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
$mod_strings['LBL_ACTIVE'] = 'Активный';
$mod_strings['LBL_PERCENTAGE_MARK'] = 'Процентная метка';
$mod_strings['LBL_TYPE'] = 'Тип цены';
$mod_strings['LBL_ROUND_OFF'] = 'Округлить до';
$mod_strings['LBL_ACCOUNT_NAME'] = 'Имя учетной записи';
$mod_strings['LBL_ACCOUNT_ID'] = 'Идентификатор учетной записи';
$mod_strings['LBL_ACCOUNTS'] = 'Аккаунты';
$mod_strings['LBL_CONTACT_NAME'] = 'Имя контакта';
$mod_strings['LBL_CONTACT_ID'] = 'ID контакта';
$mod_strings['LBL_CONTACTS'] = 'Контакты';
$mod_strings['LBL_FLAT_PRICE'] = 'Плоская цена';
$mod_strings['LBL_DIFFERENTIAL_PERCENTAGE'] = 'Дифференциальный по (%)';
$mod_strings['LBL_DIFFERENTIAL_DOLLAR'] = 'Дифференциальный по ($)';
$mod_strings['LBL_PRICE_BOOKS'] = 'Ценовые книги';
$mod_strings['LBL_NAME'] = 'Имя';
$mod_strings['LBL_PRICE_TYPE'] = 'Тип цены';
$mod_strings['LBL_ASSIGNED_TO'] = 'Назначено';
$mod_strings['LBL_NO_RECORDS'] = 'Записи не найдены';
$mod_strings['LBL_MARK_UP'] = 'MarkUp (Add)';
$mod_strings['LBL_MARK_DOWN'] = 'MarkDown (Sub)';
$mod_strings['LBL_ADD_QUANTITY'] = 'Добавить количество';
$mod_strings['LBL_PRODUCT_NAME'] = 'Название продукта';
$mod_strings['LBL_PRODUCT_PRICE'] = 'Цена продукта';
$mod_strings['LBL_LIST_PRICE'] = 'Прайс-лист';
$mod_strings['LBL_ADD_PRODUCT'] = 'Добавить продукт';
$mod_strings['LBL_YES'] = 'Да';
$mod_strings['LBL_NO'] = 'Нет';
$mod_strings['LBL_SELECT_PRODUCT_MSG'] = "Пожалуйста, выберите продукт, прежде чем нажимать на Книги цен !!!";
$mod_strings['LBL_SELECT_PRICEBOOK_MSG'] = "Пожалуйста, выберите только одну книгу цен.";
$mod_strings['LBL_PRODUCT_VALIDATION'] = 'Пожалуйста, выберите другой продукт. Этот продукт уже выбран. ';
$mod_strings['LBL_QUANTITY_VALID_MSG'] = "Неверное значение:";
$mod_strings['LBL_FROM_QUANTITY'] = "Из количества";
$mod_strings['LBL_TO_QUANTITY'] = "На количество";
$mod_strings['LBL_DIFFERENTIAL'] = "Дифференциал";
$mod_strings['LBL_PRICE_BOOKS_ACCESS'] = "Пожалуйста, включите функцию ценовых книг для доступа к модулю ценовых книг.";
$mod_strings['LBL_CLICK_HERE'] = "Нажмите здесь";
$mod_strings['LBL_ENABLE_LABEL'] = "для включения.";
$mod_strings['LBL_FROM_QUANTITY_VALID'] = "Пожалуйста, введите больше от количественного значения, предшествующего количественному значению.";
$mod_strings['LBL_TO_QUANTITY_VALID'] = "Пожалуйста, введите значение больше, чем значение количества.";
$mod_strings['LBL_PRODUCT_LIST_PRICE_VALID'] = "Пожалуйста, добавьте продукт и указанную цену для добавления количества.";
$mod_strings['LBL_PRODCUT_PRICE_VALID'] = "Пожалуйста, добавьте цену за продукт.";
$mod_strings['LBL_LIST_PRICE_VALID'] = "Пожалуйста, добавьте прайс-лист на продукт.";
$mod_strings['LBL_QUANTITY_VALID_RANGE'] = "Пожалуйста, добавьте количественный диапазон для продукта.";
$mod_strings['LBL_PRODUCT_VALID'] = "Пожалуйста, добавьте продукт для добавления количества.";
$mod_strings['LBL_PRODUCT_FIELDS_VALIDATION'] = "Пожалуйста, добавьте название продукта и прейскурантную цену для прайс-листов.";
$mod_strings['LBL_QUANTITY_FIELD_VALIDATION'] = "Пожалуйста, заполните диапазон количеств для пустых слябов.";
$mod_strings['LBL_EDITVIEW_PANEL1'] = "Детали ценовых книг";
$mod_strings['LBL_DETAILVIEW_PANEL1'] = "Детали ценовых книг";
?> 