
{include file="modules/$moduleName/templates/settings/pro_notification.tpl"}

<div style="color: grey">
    <h2>{$MOD.label_report_state_settings}</h2>
    <div class="ar-settings-table">
        <table>
            <tr>
                <th style="width:50%"></th>
                <th>{$MOD.label_expanded}</th>
                <th>{$MOD.label_collapsed}</th>
            </tr>
            <tr>
                <td>{$MOD.label_report_editor}</td>
                <td><input type="radio" checked disabled></td>
                <td><input type="radio" disabled></td>
            </tr>
            <tr>
                <td>{$MOD.label_report}</td>
                <td><input type="radio" checked disabled></td>
                <td><input type="radio" disabled></td>
            </tr>
            <tr>
                <td>{$MOD.label_chart_editor}</td>
                <td><input type="radio" checked disabled></td>
                <td><input type="radio" disabled></td>
            </tr>
            <tr>
                <td>{$MOD.label_chart}</td>
                <td><input type="radio" checked disabled></td>
                <td><input type="radio" disabled></td>
            </tr>
        </table>
    </div>
</div>