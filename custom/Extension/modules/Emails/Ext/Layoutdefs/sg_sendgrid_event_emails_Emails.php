<?php
 // created: 2019-03-30 13:22:29
$layout_defs["Emails"]["subpanel_setup"]['sg_sendgrid_event_emails'] = array (
  'order' => 100,
  'module' => 'sg_SendGrid_Event',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SG_SENDGRID_EVENT_EMAILS_FROM_SG_SENDGRID_EVENT_TITLE',
  'get_subpanel_data' => 'sg_sendgrid_event_emails',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
