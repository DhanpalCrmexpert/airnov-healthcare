<?php
/**
 * Copyright (c) SalesAgility Ltd.  All Rights Reserved.
 * Licensed under the GPL3 License.  See License in the project root
 * for license information.
 *
 * @package Outlook Addin for SuiteCRM
 * @copyright 2020 SalesAgility Ltd http://www.salesagility.com
 * @author SalesAgility <info@salesagility.com
 *
 */

namespace SalesAgility\Outlook\Sync;

use DateTime;
use Exception;
use GuzzleHttp\Client;
use Microsoft\Graph\Graph;
use Microsoft\Graph\Http\GraphRequest;
use Microsoft\Graph\Model;
use Microsoft\Graph\Model\FileAttachment;
use SalesAgility\Outlook\Outlook;

class OutlookSyncAPI implements SyncAPI
{
    /**
     * @var Graph $graph
     */
    private $graph;

    private $token;

    private $pageLimit = 100;

    public function connect()
    {
        $outlook = new Outlook();
        $config = $outlook->getSettings();

        if (empty($config) || !$outlook->isLicenced()) {
            return false;
        }
        $guzzle = new Client();
        $tenantId = $config['graph_tenant_id'];
        $clientId = $config['graph_client_id'];
        $clientSecret = $config['graph_client_secret'];
        $url = 'https://login.microsoftonline.com/' . $tenantId . '/oauth2/token?api-version=1.0';
        $token = json_decode($guzzle->post($url, [
            'form_params' => [
                'client_id' => $clientId,
                'client_secret' => $clientSecret,
                'resource' => 'https://graph.microsoft.com/',
                'grant_type' => 'client_credentials',
            ],
        ])->getBody()->getContents(), false);
        if (empty($token)) {
            return false;
        }
        $accessToken = $token->access_token;
        $this->token = $token;
        $this->graph = new Graph();
        $this->graph->setAccessToken($accessToken);

        return true;
    }

    public function getUsers()
    {
        $users = [];
        try {
            /** @var GraphRequest $req */
            $req = $this->graph->createRequest("GET", "/users")
                ->setReturnType(Model\User::class)
                ->setPageSize($this->pageLimit);
            while (!$req->isEnd()) {
                $users = array_merge($users, $req->getPage());
            }
        } catch (Exception $e) {
            $GLOBALS['log']->error('[Outlook Graph API] ' . $e->getMessage());
        }

        return $users;
    }

    public function getFolders($userPrincipalName)
    {
        $userPrincipalName = urlencode($userPrincipalName);
        $folders = [];
        try {
            /** @var GraphRequest $req */
            $req = $this->graph->createCollectionRequest("GET", "/users/" . $userPrincipalName . "/mailFolders")
                ->setReturnType(Model\MailFolder::class)
                ->setPageSize($this->pageLimit);
            while (!$req->isEnd()) {
                $folders = array_merge($folders, $req->getPage());
            }
        } catch (Exception $e) {
            $GLOBALS['log']->error('[Outlook Graph API] ' . $e->getMessage());
        }

        return $folders;
    }

    /**
     * @param $userPrincipalName
     * @param string $folder
     * @param DateTime|null $since
     * @return Model\Message[]
     */
    public function getEmails($userPrincipalName, $folder = "", DateTime $since = null)
    {
        $userPrincipalName = urlencode($userPrincipalName);
        $url = "/users/" . $userPrincipalName;
        if ($folder) {
            $url .= "/mailFolders/" . urlencode($folder);
        }
        $url .= "/messages";
        if ($since) {
            //2017-07-01T08:00:00Z
            $sinceText = $since->format("Y-m-d\TH:i:s\Z");
            $url .= '?$filter=receivedDateTime%20ge%20' . $sinceText;
        }
        echo $url . "\n";
        $emails = [];
        try {
            /** @var GraphRequest $req */
            $req = $this->graph->createCollectionRequest("GET", $url)
                ->setReturnType(Model\Message::class)
                ->setPageSize($this->pageLimit);
            while (!$req->isEnd()) {
                $emails = array_merge($emails, $req->getPage());
            }
        } catch (Exception $e) {
            $GLOBALS['log']->error('[Outlook Graph API] ' . $e->getMessage());
        }

        return $emails;
    }

    /**
     * @param $userPrincipalName
     * @param $emailId
     * @return FileAttachment[]
     */
    public function getAttachments($userPrincipalName, $emailId)
    {
        $userPrincipalName = urlencode($userPrincipalName);
        $emailId = urlencode($emailId);
        $url = "/users/" . $userPrincipalName . '/messages/' . $emailId . '/attachments/';
        $attachments = [];
        try {
            /** @var GraphRequest $req */
            $req = $this->graph->createCollectionRequest('GET', $url)
                ->setReturnType(Model\FileAttachment::class)
                ->setPageSize($this->pageLimit);
            while (!$req->isEnd()) {
                $attachments = array_merge($attachments, $req->getPage());
            }
        } catch (Exception $e) {
            $GLOBALS['log']->error('[Outlook Graph API] ' . $e->getMessage());
        }
        return $attachments;
    }
}
