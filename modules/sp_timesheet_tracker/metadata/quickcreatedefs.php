<?php
$module_name = 'sp_timesheet_tracker';
$viewdefs [$module_name] = 
array (
  'QuickCreate' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 'name',
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'project',
            'studio' => 'visible',
            'label' => 'LBL_PROJECT',
          ),
          1 => 
          array (
            'name' => 'project_tasks',
            'studio' => 'visible',
            'label' => 'LBL_PROJECT_TASKS',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'track_date',
            'label' => 'LBL_TRACK_DATE',
          ),
          1 => 
          array (
            'name' => 'hours',
            'label' => 'LBL_HOURS',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'track_user',
            'studio' => 'visible',
            'label' => 'LBL_TRACK_USER',
          ),
          1 => 
          array (
            'name' => 'description',
            'comment' => 'Full text of the note',
            'label' => 'LBL_DESCRIPTION',
          ),
        ),
        4 => 
        array (
          0 => 'assigned_user_name',
        ),
      ),
    ),
  ),
);
?>
