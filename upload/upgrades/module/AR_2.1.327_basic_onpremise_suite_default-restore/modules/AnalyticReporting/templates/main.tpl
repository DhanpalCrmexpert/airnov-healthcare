<script src="modules/{$moduleName}/assets/js/jquery/jquery-1.8.3.min.js"></script>
<script src="modules/{$moduleName}/assets/js/legacy/startwith.js"></script>
<script type="text/javascript">
    jQuery.noConflict();
</script>

<script src="modules/{$moduleName}/assets/js/jstree/jstree.min.js"></script>
<script src="modules/{$moduleName}/assets/js/jstree/jstreegrid.min.js"></script>

<script src="modules/{$moduleName}/assets/js/jquery-ui/jquery-ui.min.js"></script>
<link href="modules/{$moduleName}/assets/css/jquery-ui/redmond/jquery-ui.css" rel="stylesheet" type="text/css" media="all" />

<script src="modules/{$moduleName}/assets/js/ractive/Ractive.min.js"></script>

<link href="modules/{$moduleName}/assets/css/jquery.select2.css" rel="stylesheet" type="text/css" media="all" />
<link href="modules/{$moduleName}/assets/css/jstree/style.min.css" rel="stylesheet" type="text/css" media="all"  />
<link href="modules/{$moduleName}/assets/css/{$moduleName}.css" rel="stylesheet" type="text/css" media="all" />

<script>
    {literal}
    var ReportData = {
        options: {
            includeDetails: false
        },
        reportAccessUsers: {/literal}{$REPORTACCESSUSERS}{literal},
        reportScheduleUsers:{/literal}{$REPORTSCHEDULEUSERS}{literal},
        users:{/literal}{$REPORTUSERS}{literal},
    }

var del_rep = {/literal}{$DELETABLE_REPORTS|@json_encode}{literal};//#3879 - array of reports that the current user is able to move between folders
var translated_labels = {/literal}{$DICTIONARY}{literal}; // #5286 - Refactored to JSON object
var csrf = {/literal}{$csrf}{literal};
var $reportTree = {/literal}{$REPORTTREE}{literal};
var $categories = {/literal}{$CATEGORIES}{literal};
var userPermissions = {/literal}{$userPermissions}{literal};
var moduleName = "AnalyticReporting";
var $urls = {/literal}{$urlsJSON}{literal};
var $searchCriteria = "{/literal}{$SEARCHCRITERIA}{literal}";
var $reportTreeAdminAccess = {/literal}{$reportTreeAdminAccess}{literal};
var $isAdmin = {/literal}{$ISADMIN}{literal};
var $isBasic = {/literal}{$ISBASIC}{literal};
var envConfig = { isBasic: $isBasic };
var $toggleHidden = {/literal}{$TOGGLEHIDDEN};

</script>

{* Include underscore.js and Ractive.js templates *}
{include file="modules/$moduleName/templates/templates.tpl"}

<script src="modules/{$moduleName}/assets/js/underscore.js"></script>
<script src="modules/{$moduleName}/assets/js/jquery/jquery.select2.min.js"></script>
<script src="modules/{$moduleName}/assets/js/ractive/ractive-decorators-select2.min.js"></script>
<!-- env:dev --#>
<script src="modules/{$moduleName}/assets/js/components/reports/shared.js"></script>
<script src="modules/{$moduleName}/assets/js/components/shared.js"></script>
<script src="modules/{$moduleName}/assets/js/components/reportManager.js"></script>
<script src="modules/{$moduleName}/assets/js/components/manager/accessManager.js"></script>
<script src="modules/{$moduleName}/assets/js/components/manager/scheduleManager.js"></script>
<!-- env:dev:end -->
<!-- env:prod -->
<script src="modules/{$moduleName}/assets/js/main.min.js?v=2.1.327"></script>
<!-- env:prod:end -->

<!-- TODO: Should include in main.min.js-->
<script src="modules/{$moduleName}/assets/js/main.js"></script>

<!-- ADVANCED REPORTS CONTAINER -->
<div id="ar-container">
    <div id="ar-toolbar">
        {if $ISADMIN eq true || $reportTreeAdminAccess eq true}
        <a href="javascript:void(0);" data-action="addCategory" data-id="0">{$MOD.label_add_folder}</a>
        {/if}

        {include file="modules/$moduleName/templates/elements/massSchedule.tpl"}

        {if $ISADMIN eq true || $reportBuilderPublic eq true}
            <a href="{$urls.reportBuilder}" >{$MOD.reportBuilder}</a>
        {/if}
        <form id="searchreports" action="{$urls.index}" method="POST" style=" display:inline!important;">
            <input type="hidden" name="search" value="1">
            <input type="text" name="searchCriteria" placeholder="{$MOD.label_search_placeholder}" value="{$SEARCHCRITERIA}" />
            <input type="checkbox" style="vertical-align: middle" name="matchAll" id="searchMatchAll" value="true" {$matchAll} />
            <label style="margin-right: 5px;" for="searchMatchAll">{$MOD.label_matchall}</label>
            <input class="ar-button" type="submit" name="submit" value="{$MOD.label_search}" />
            {if isset($SEARCHCRITERIA)}
                <a class="ar-button" href="{$urls.index}">{$MOD.label_cancel}</a>
            {/if}
        </form>

        {if $ISADMIN eq true} {* #5885 Hide settings link from non-admin users *}
            <a style="float:right" href="{$urls.settings}" >Reporting Tool Settings{$MOD.label_settings}</a>
            <br />
            {include file="modules/$moduleName/templates/elements/reportImportNExport.tpl"}
        {/if}
    </div>

    <div id="modalWindow" style="display:none;"></div>
    <div id="report-tree"></div>
</div>
<!-- // ADVANCED REPORTS CONTAINER -->