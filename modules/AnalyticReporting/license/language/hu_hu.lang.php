<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

$license_strings = array (
    'LBL_STEPS_TO_LOCATE_KEY_TITLE' => 'Keresse meg a kulcs',
    'LBL_STEPS_TO_LOCATE_KEY_1' => '1. Bel�p�s',
    'LBL_STEPS_TO_LOCATE_KEY_2' => '2. Menj a Account->Purchases',
    'LBL_STEPS_TO_LOCATE_KEY_3' => '3. Keresse meg a gombot a beszerz�se a kieg�sz�to',
    'LBL_STEPS_TO_LOCATE_KEY_4' => '4. Illessze be a enged�ly kulcs al�bbi mezobe',
    'LBL_STEPS_TO_LOCATE_KEY_5' => '5. Kattints "Validate"',
    'LBL_LICENSE_KEY' => 'enged�ly kulcs',
    'LBL_CURRENT_USERS' => 'Aktu�lis felhaszn�l� Count',
    'LBL_LICENSED_USERS' => 'Licenccel rendelkezo felhaszn�l� Count',
    'LBL_VALIDATE_LABEL' => '�rv�nyes�t',
    'LBL_VALIDATED_LABEL' => 'valid�lt',
);

