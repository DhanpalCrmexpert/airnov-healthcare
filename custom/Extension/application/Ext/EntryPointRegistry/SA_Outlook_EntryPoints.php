<?php
/**
 * Copyright (c) SalesAgility Ltd.  All Rights Reserved.
 * Licensed under the GPL3 License.  See License in the project root
 * for license information.
 *
 * @package Outlook Addin for SuiteCRM
 * @copyright 2020 SalesAgility Ltd http://www.salesagility.com
 * @author SalesAgility <info@salesagility.com
 *
 */

$entry_point_registry['downloadOutlookManifest'] = [
    'file' => 'custom/include/SA_Outlook/admin/entrypoint/DownloadOutlookManifest.php',
    'auth' => true
];
$entry_point_registry['updateLicensedOutlookUsers'] = [
    'file' => 'custom/include/SA_Outlook/SuiteStore/entrypoint/UpdateLicensedOutlookUsers.php',
    'auth' => true
];
$entry_point_registry['validateOutlookKey'] = [
    'file' => 'custom/include/SA_Outlook/SuiteStore/entrypoint/ValidateOutlookKey.php',
    'auth' => true
];
$entry_point_registry['outlookAddinModules'] = [
    'file' => 'custom/include/SA_Outlook/admin/entrypoint/OutlookAddinModules.php',
    'auth' => false
];
