<?php
require_once 'custom/include/SA_Outlook/API/SAOutlookAPIController.php';
return [SAOutlookAPIController::class => function(Slim\Container $container) {
    return new SAOutlookAPIController(  $container->get(\Api\V8\BeanDecorator\BeanManager::class));
}];
