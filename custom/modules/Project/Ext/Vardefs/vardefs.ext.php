<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2019-02-21 03:39:46
$dictionary['Project']['fields']['jjwg_maps_lat_c']['inline_edit']=1;

 

 // created: 2019-02-21 03:39:46
$dictionary['Project']['fields']['jjwg_maps_address_c']['inline_edit']=1;

 

$dictionary["Project"]["fields"]["budget_hours"] = array (
 'name' => 'budget_hours',
      'vname' => 'Budgeted Hours',
      'type' => 'varchar',
      'studio' =>  array (
        'listview' => true,
        'detailview' => true,
        'editview' => true,
      ),
);
$dictionary["Project"]["fields"]["accounts_c"] = array (
      'inline_edit' => '1',
      'labelValue' => 'Account',
      'required' => true,
      'source' => 'non-db',
      'name' => 'accounts_c',
      'vname' => 'Account',
      'type' => 'relate',
      'massupdate' => '0',
      'default' => NULL,
      'no_default' => false,
      'comments' => '',
      'help' => '',
      'importable' => 'true',
      'duplicate_merge' => 'disabled',
      'duplicate_merge_dom_value' => '0',
      'audited' => false,
      'reportable' => true,
      'unified_search' => false,
      'merge_filter' => 'disabled',
      'len' => '255',
      'size' => '20',
      'id_name' => 'account_id_c',
      'ext2' => 'Accounts',
      'module' => 'Accounts',
      'rname' => 'name',
      'quicksearch' => 'enabled',
      'studio' => 'visible',
      'id' => 'Projectaccounts_c',
    );
$dictionary["Project"]["fields"]["account_id_c"] = array (
      'inline_edit' => 1,
      'required' => false,
      'name' => 'account_id_c',
      'vname' => 'Account ID',
      'type' => 'id',
      'massupdate' => '0',
      'default' => NULL,
      'no_default' => false,
      'comments' => '',
      'help' => '',
      'importable' => 'true',
      'duplicate_merge' => 'disabled',
      'duplicate_merge_dom_value' => '0',
      'audited' => false,
      'reportable' => false,
      'unified_search' => false,
      'merge_filter' => 'disabled',
      'len' => '36',
      'size' => '20',
      'id' => 'Projectaccount_id_c',
    );

// created: 2016-09-26 21:04:57
$dictionary["Project"]["fields"]["project_sp_timesheet_tracker_1"] = array (
  'name' => 'project_sp_timesheet_tracker_1',
  'type' => 'link',
  'relationship' => 'project_sp_timesheet_tracker_1',
  'source' => 'non-db',
  'module' => 'sp_timesheet_tracker',
  'bean_name' => 'sp_timesheet_tracker',
  'side' => 'right',
  'vname' => 'LBL_PROJECT_SP_TIMESHEET_TRACKER_1_FROM_SP_TIMESHEET_TRACKER_TITLE',
);


 // created: 2016-08-15 22:08:49
$dictionary['Project']['fields']['total_hours_pt'] = array(
	  'labelValue' => 'Total Hours',
      'required' => false,
      'name' => 'total_hours_pt',
      'vname' => 'LBL_TOTAL_HOURS_PT',
      'type' => 'decimal',
      'massupdate' => '0',
      'default' => '',
      'no_default' => false,
      'comments' => '',
      'help' => '',
      'importable' => 'true',
      'duplicate_merge' => 'disabled',
      'duplicate_merge_dom_value' => '0',
      'audited' => false,
      'reportable' => true,
      'unified_search' => false,
      'merge_filter' => 'disabled',
      'len' => '18',
      'size' => '20',
      'enable_range_search' => false,
      'precision' => '2',
);

 

 // created: 2019-02-21 03:39:46
$dictionary['Project']['fields']['jjwg_maps_geocode_status_c']['inline_edit']=1;

 

 // created: 2019-02-21 03:39:46
$dictionary['Project']['fields']['jjwg_maps_lng_c']['inline_edit']=1;

 
?>