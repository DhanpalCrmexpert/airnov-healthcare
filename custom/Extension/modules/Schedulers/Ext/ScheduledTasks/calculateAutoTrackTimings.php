<?php
array_push($job_strings, 'calculate_Auto_Track_Timings');

function calculate_Auto_Track_Timings(){
	global $db, $timedate;
	$timesheet_ids = array();
	
	$current_date_time = $timedate->get_gmt_db_datetime();
	
	$getTimeSheetQry = "Select * from sp_auto_timesheet_time_tracker where deleted = 0 and processed = 0";
	$resTimeSheet = $db->query($getTimeSheetQry);
	while($rowTimeSheetId = $db->fetchByAssoc($resTimeSheet))
	{
		$auto_track_id = $rowTimeSheetId['id'];
		$end_date = $rowTimeSheetId['end_date'];
		$parent_type = $rowTimeSheetId['parent_type'];
		$parent_id = $rowTimeSheetId['parent_id'];
		$selected_module_id = $rowTimeSheetId['selected_module_id'];
		$account_id = $selected_module_id;
		
		$timesheet_tracker_id = $rowTimeSheetId['sp_timesheet_tracker_id_c'];
		
		$hours = getHours($end_date, $current_date_time);
		
		$timesheet_tracker_obj = BeanFactory::getBean('sp_timesheet_tracker', $timesheet_tracker_id);
		
		if(!empty($timesheet_tracker_obj->id)){
			$timesheet_ids[$timesheet_tracker_obj->timesheet_id] = $timesheet_tracker_obj->timesheet_id;
		
			$timesheet_tracker_obj->hours = $timesheet_tracker_obj->hours + $hours;
			$timesheet_tracker_obj->save();
		}
		
		if(!empty($account_id)){
			$tt_id = $timesheet_tracker_obj->id;
			$at_id = create_guid();
			$acc_tt_id = "";
			
			$get_acc_tt_qry = "Select id from accounts_sp_timesheet_tracker_1_c where accounts_sp_timesheet_tracker_1accounts_ida = '$account_id' and accounts_sp_timesheet_tracker_1sp_timesheet_tracker_idb = '$tt_id' and deleted = 0";
			$res_acc_tt_qry = $db->query($get_acc_tt_qry);
			while($row_acc_tt_qry = $db->fetchByAssoc($res_acc_tt_qry))
			{
				$acc_tt_id =  $row_acc_tt_qry["id"];
			}
			if(empty($acc_tt_id)){
				$query_account_timesheet = "INSERT into accounts_sp_timesheet_tracker_1_c (id, date_modified, deleted, accounts_sp_timesheet_tracker_1accounts_ida, accounts_sp_timesheet_tracker_1sp_timesheet_tracker_idb) VALUES ('$at_id', '$current_date_time', '0', '$account_id', '$tt_id')";
				$db->query($query_account_timesheet);
			}
		}
		
		$db->query("update sp_auto_timesheet_time_tracker set end_date = '$current_date_time' where id = '$auto_track_id'");
	}
	
	foreach($timesheet_ids as $timesheet_id){
		$getHoursQry = "Select SUM(hours) as total_hours from sp_timesheet_tracker where deleted = 0 and timesheet_id = '$timesheet_id'";
		$resHours = $db->query($getHoursQry);
		while($rowHour= $db->fetchByAssoc($resHours))
		{
			$total_hours = $rowHour['total_hours'];
			$update_hours = "UPDATE sp_timesheet set hours = '$total_hours' where id = '$timesheet_id' and deleted = 0";
			$db->query($update_hours);
		}
	}
	return true;
}
	
	function getHours($start_date, $current_date_time){
		
		$res = get_time_difference( $start_date, $current_date_time );
		
		$hours = ($res['days'] * 8) + ($res['hours']) + ($res['minutes'] / 60);
		
		return $hours = round($hours, 2);
	}
	
	function get_time_difference( $start, $end )
	{
		$uts['start']      =    strtotime( $start );
		$uts['end']        =    strtotime( $end );
		
		if( $uts['start']!==-1 && $uts['end']!==-1 )
		{
			if( $uts['end'] >= $uts['start'] )
			{
				$diff    =    $uts['end'] - $uts['start'];
				if( $days=intval((floor($diff/86400))) )
					$diff = $diff % 86400;
				if( $hours=intval((floor($diff/3600))) )
					$diff = $diff % 3600;
				if( $minutes=intval((floor($diff/60))) )
					$diff = $diff % 60;
				$diff    =    intval( $diff );            
				return( array('days'=>$days, 'hours'=>$hours, 'minutes'=>$minutes, 'seconds'=>$diff) );
			}
			else
			{
				trigger_error( "Ending date/time is earlier than the start date/time", E_USER_WARNING );
			}
		}
		else
		{
			trigger_error( "Invalid date/time data detected", E_USER_WARNING );
		}
		return( false );
	}

?>