<?php
/***CONFIGURATOR***/
$sugar_config['disable_persistent_connections'] = false;
$sugar_config['timesheet_module_1'] = 'Project';
$sugar_config['related_module_relationship_1'] = 'projecttask';
$sugar_config['related_module_1'] = 'ProjectTask';
$sugar_config['search']['defaultEngine'] = 'BasicAndAodEngine';
$sugar_config['disableAjaxUI'] = true;
$sugar_config['timesheet_module_dc26b995-5550-b848-6f73-5e90aa679348'] = 'Project';
$sugar_config['related_module_relationship_dc26b995-5550-b848-6f73-5e90aa679348'] = 'projecttask';
$sugar_config['related_module_dc26b995-5550-b848-6f73-5e90aa679348'] = 'ProjectTask';
$sugar_config['verify_client_ip'] = false;
$sugar_config['default_module_favicon'] = false;
$sugar_config['dashlet_auto_refresh_min'] = '30';
$sugar_config['stack_trace_errors'] = false;
$sugar_config['outfitters_licenses']['kanbans'] = '1520f2600abd9b54fe7cf6b3c7b5cf90';
$sugar_config['outfitters_licenses']['gmsyncaddon'] = '29d4df06b202ac2b4fc7abf19c2d749a';
$sugar_config['timesheet_module_8d544213-bcac-b962-fc13-5f4698c75700'] = 'Project';
$sugar_config['related_module_relationship_8d544213-bcac-b962-fc13-5f4698c75700'] = 'projecttask';
$sugar_config['related_module_8d544213-bcac-b962-fc13-5f4698c75700'] = 'ProjectTask';
$sugar_config['timesheet_module_b70243be-ca4c-b256-aff8-5f46980acf9f'] = 'Project';
$sugar_config['related_module_relationship_b70243be-ca4c-b256-aff8-5f46980acf9f'] = 'projecttask';
$sugar_config['related_module_b70243be-ca4c-b256-aff8-5f46980acf9f'] = 'ProjectTask';
$sugar_config['timesheet_module_1674d634-fb3a-8d8b-34c1-5f4698fed125'] = 'Project';
$sugar_config['related_module_relationship_1674d634-fb3a-8d8b-34c1-5f4698fed125'] = 'projecttask';
$sugar_config['related_module_1674d634-fb3a-8d8b-34c1-5f4698fed125'] = 'ProjectTask';
$sugar_config['logger']['level'] = 'fatal';
$sugar_config['email_allow_send_as_user'] = true;
$sugar_config['email_xss'] = 'YToxMzp7czo2OiJhcHBsZXQiO3M6NjoiYXBwbGV0IjtzOjQ6ImJhc2UiO3M6NDoiYmFzZSI7czo1OiJlbWJlZCI7czo1OiJlbWJlZCI7czo0OiJmb3JtIjtzOjQ6ImZvcm0iO3M6NToiZnJhbWUiO3M6NToiZnJhbWUiO3M6ODoiZnJhbWVzZXQiO3M6ODoiZnJhbWVzZXQiO3M6NjoiaWZyYW1lIjtzOjY6ImlmcmFtZSI7czo2OiJpbXBvcnQiO3M6ODoiXD9pbXBvcnQiO3M6NToibGF5ZXIiO3M6NToibGF5ZXIiO3M6NDoibGluayI7czo0OiJsaW5rIjtzOjY6Im9iamVjdCI7czo2OiJvYmplY3QiO3M6MzoieG1wIjtzOjM6InhtcCI7czo2OiJzY3JpcHQiO3M6Njoic2NyaXB0Ijt9';
$sugar_config['email_enable_confirm_opt_in'] = 'not-opt-in';
$sugar_config['timesheet_module_3ae49bd3-b3a0-8361-83af-5f9701491a9d'] = 'Project';
$sugar_config['related_module_relationship_3ae49bd3-b3a0-8361-83af-5f9701491a9d'] = 'projecttask';
$sugar_config['related_module_3ae49bd3-b3a0-8361-83af-5f9701491a9d'] = 'ProjectTask';
$sugar_config['timesheet_module_b400dd85-1ff8-9526-9e6d-5f9702dfdb81'] = 'Project';
$sugar_config['related_module_relationship_b400dd85-1ff8-9526-9e6d-5f9702dfdb81'] = 'projecttask';
$sugar_config['related_module_b400dd85-1ff8-9526-9e6d-5f9702dfdb81'] = 'ProjectTask';
$sugar_config['addAjaxBannedModules'][0] = 'AnalyticReporting';
/***CONFIGURATOR***/