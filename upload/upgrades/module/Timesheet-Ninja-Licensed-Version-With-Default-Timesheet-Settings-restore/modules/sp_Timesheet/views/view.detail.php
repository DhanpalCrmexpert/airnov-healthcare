<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

class sp_TimesheetViewDetail extends ViewDetail
{
	public function __construct() {
		parent::ViewDetail();
	}
	
    function display()
    {
		parent::display();
		global $current_user, $sugar_config, $app_list_strings;
		$user_id = $current_user->id;
		$current_user_id = $current_user->id;
		
		//////////////////
		
		$siteUrl = $sugar_config['site_url'];
		require_once('modules/TimesheetNinja/license/OutfittersLicense.php');
		$validate_license = OutfittersLicense::isValid('TimesheetNinja');
		if($validate_license !== true) {
		if(is_admin($current_user)) {
			SugarApplication::appendErrorMessage('Timesheet is no longer active due to the following reason: '.$validate_license.' Users will have limited to no access until the issue has been addressed.');
		}
		echo '<p class="error">Timesheet is no longer active Please renew your subscription or check your license configuration.Will be redirect in seconds.....</p>';
		header("refresh:2;url=$siteUrl");
		//functionality may be altered here in response to the key failing to validate
		}
		echo "</br>";
		
		
		//////////////////
		
		if(!empty($this->bean->id)){
			$timeSheetDates = $this->getTimesheetDates($this->bean->id);
			$week_start_date = $timeSheetDates['track_date_from'];
			$result['start'] = date("Y-m-d",strtotime($timeSheetDates['track_date_from']));
			$result['end'] = date("Y-m-d",strtotime($timeSheetDates['track_date_to']));
			$dates = $this->getWeekDates($result['start'], $result['end']);
		}
		$bean_id = $this->bean->id;
		if(!empty($bean_id)){
			$selected_module = $this->bean->module;
			$related_module = $this->bean->related_module;
			$user_id = $this->bean->user_id_c;
			$current_user_id = $this->bean->user_id_c;
		}
		else{
			$selected_module = $sugar_config['timesheet_module_'.$current_user_id];
			$related_module = $sugar_config['related_module_'.$current_user_id];
		}
		
		$selected_module_lable = $app_list_strings['moduleList'][$selected_module];
		$related_module_lable = $app_list_strings['moduleList'][$related_module];
			
		$projects_list = $this->getProjectsList($result['start'], $result['end'], $user_id, $selected_module, $related_module);
		
		$projects = $projects_list['project_list'];
		$custom_projects = $projects_list['custom_array'];
		$html = '
		<script src="modules/sp_Timesheet/js/timesheet.js"></script>';
		

		$html .= '<br/><br/><br/>
		<div class "detail view  detail508 expanded">
		<table id = "table_detail" style="border-collapse: separate;  border-spacing: 0 1em;">';
		$html .= '<tr>';
		$html .= '<td></td>';
		$sum_all_projects = array();
		for($k=0; $k<7; $k++){
			$html .= '<td>'.date("D m/d",strtotime($dates[$k])).'</td>';
			$html .= '<td></td>';
			$sum_all_projects[$dates[$k]] = 0;
		}
		$html .= '<td colspan="2"><strong>Total</strong></td>';
		$html .= '<td colspan="2"><strong>Project Hours</strong></td>';
		$html .= "</tr>";
		$found = true;
		$per_project_sum = 0;
		for($i=0; $i<count($projects); $i++){
			$project_name = $projects[$i]['project_name'];
			$project_id = $projects[$i]['project_id'];
			$quoted_project_id = "'$project_id'";
			$html .= '<tr>';
			$html .= '<td style="width: 275px;"><b>'.$selected_module_lable.': </b> <a href="index.php?module='.$selected_module.'&action=DetailView&record='.$project_id.'" target="_blank">'.$project_name.'</a></td>';
			$row_project_sum = 0;
			for($k=0; $k<7; $k++){
				$field_name = 'project_'.$project_id.'_'.$dates[$k];
				$class_name = 'class_'.$dates[$k];
				$quoted_date = $dates[$k];
				$quoted_date = "'$quoted_date'";
				if(!empty($projects[$i]['project_time_sheet_tracker'][$field_name])) {
					$html .= '<td style = "text-align:center"><strong><div style = "background-color:#dcdcdc">'.$projects[$i]['project_time_sheet_tracker'][$field_name].'<div><strong></td>';
				} else {
					$html .= '<td style = "text-align:center"><strong>'.$projects[$i]['project_time_sheet_tracker'][$field_name].'<strong></td>';
				}
				$html .= '<td style="width: 30px;"></td>';
				
				$sum_all_projects[$dates[$k]] += $projects[$i]['project_time_sheet_tracker'][$field_name];
				$row_project_sum = $row_project_sum + $projects[$i]['project_time_sheet_tracker'][$field_name];
				
				$per_project_sum = $per_project_sum + $projects[$i]['project_time_sheet_tracker'][$field_name];
				$total_hours = $total_hours + $projects[$i]['project_time_sheet_tracker'][$field_name];
			}
			$project_class = 'project_'.$project_id;
			$field_name = 'sumproject_'.$project_id;
			if (!empty($row_project_sum) && $row_project_sum != 0) {
				$html .= '<td style = "text-align:center"><strong><div style = "background-color:#dcdcdc" class="'.$project_class.' "name="'.$field_name.'" id="'.$field_name.'" >'.$row_project_sum.'</div> </strong> </td>';
			} else {
				$html .= '<td style = "text-align:center"><strong><div class="'.$project_class.' "name="'.$field_name.'" id="'.$field_name.'" >'.$row_project_sum.'</div> </strong> </td>';
			}
			$html .= '<td style="width: 30px;"></td>';
			$additional_hours = 0.0;
			foreach ($projects[$i]['project_time_sheet_tracker']['custom'] as $project) {
				$additional_hours += (float)$project['hours'];
			}
			for($pt=0; $pt<count($projects[$i]['project_tasks_list']); $pt++){
				$html .= '<tr>';
				$project_task_name = $projects[$i]['project_tasks_list'][$pt]['project_task_name'];
				$project_task_id = $projects[$i]['project_tasks_list'][$pt]['project_task_id'];
				$quoted_project_task_id = "'$project_task_id'";
				$html .= '<td style="width: 275px;"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$related_module_lable.':</b> <a href="index.php?module='.$related_module.'&action=DetailView&record='.$project_task_id.'" target="_blank">'.$project_task_name.'</a></td>';
				$row_project_task_sum = 0;
				for($k=0; $k<7; $k++){
					$field_name = 'projecttask_'.$project_id.'_'.$project_task_id.'_'.$dates[$k];
					$quoted_date = $dates[$k];
					$quoted_date = "'$quoted_date'";
					$class_name = 'class_'.$dates[$k];
					if (!empty($projects[$i]['project_time_sheet_tracker'][$field_name])) {
						$html .= '<td style = "text-align:center"><strong><div style = "background-color:#dcdcdc">'.$projects[$i]['project_time_sheet_tracker'][$field_name].'<div></strong></td>';
					} else {
						$html .= '<td style = "text-align:center"><strong>'.$projects[$i]['project_time_sheet_tracker'][$field_name].'</strong></td>';
					}
					$html .= '<td style="width: 30px;"></td>';
					$row_project_task_sum = $row_project_task_sum + $projects[$i]['project_time_sheet_tracker'][$field_name];
					$sum_all_projects[$dates[$k]] += $projects[$i]['project_time_sheet_tracker'][$field_name];
					
					$per_project_sum = $per_project_sum + $projects[$i]['project_time_sheet_tracker'][$field_name];
					$total_hours = $total_hours + $projects[$i]['project_time_sheet_tracker'][$field_name];
				}
				$field_name = $field_name = 'sumprojecttask_'.$project_id.'_'.$project_task_id;
				$project_class = 'project_'.$project_id;
				if (!empty($row_project_task_sum) && $row_project_task_sum != 0) {
					$html .= '<td style = "text-align:center"><strong><div style = "background-color:#dcdcdc" class="'.$project_class.'" name="'.$field_name.'" id="'.$field_name.'">'.$row_project_task_sum.'</div></strong> </td>';
				} else {
					$html .= '<td style = "text-align:center"><strong><div class="'.$project_class.'" name="'.$field_name.'" id="'.$field_name.'">'.$row_project_task_sum.'</div></strong> </td>';
				}
				$html .= '<td style="width: 30px;"></td>';
				
				if($pt+1 == count($projects[$i]['project_tasks_list'])){
					$field_name = 'projecthours_'.$project_id;
					$temp=$per_project_sum+$additional_hours;
					if (!empty($temp) && ($per_project_sum+$additional_hours) != 0) {
						$html .= '<td style = "text-align:center"><strong><div style = "background-color:#dcdcdc" name="'.$field_name.'" id="'.$field_name.'" >'.($per_project_sum+$additional_hours).'</div> </strong> </td>';
					} else {
						$html .= '<td style = "text-align:center"><strong><div name="'.$field_name.'" id="'.$field_name.'" >'.($per_project_sum+$additional_hours).'</div> </strong> </td>';
					}
					$html .= '<td style="width: 30px;"></td>';
					$html .= "</tr>";
				}
				$found = false;
			}
			if($found == true){
				$field_name = 'projecthours_'.$project_id;
				if (!empty($per_project_sum) && $per_project_sum !=0) {
					$html .= '<td style = "text-align:center"><strong><div style = "background-color:#dcdcdc" name="'.$field_name.'" id="'.$field_name.'" >'.$per_project_sum.'</div></strong>  </td>';
				} else {
					$html .= '<td><strong><div name="'.$field_name.'" id="'.$field_name.'" >'.$per_project_sum.'</div></strong>  </td>';
				}
				$html .= '<td style="width: 30px;"></td>';
			}
			$per_project_sum = 0;
			$found = true;
			$html .= '<tr ><td colspan=18><hr><td></tr>';
		}
		$temp_array = array();
		foreach ($custom_projects as $key => $value) {
			$key_parts = explode('_', $key);
			if(isset($temp_array[$key_parts['0']])){
				$temp_array[$key_parts['0']]['track_date'][$value['track_date']] = array('hours' => $value['hours']);
			}else{
				$temp_array[$key_parts['0']] = array(
					'related_module' => $value['related_module'],
            		'related_module_record' => $value['related_module_record'],
		            'track_date' => array($value['track_date'] => array('hours' => $value['hours'])),
				);  
			}
		}
		foreach ($temp_array as $project) {
			$html .= "<tr>";
			$project_bean = BeanFactory::getBean($project['related_module'], $project['related_module_record']);
			if(isset($project_bean->id)){
				$html .= "<td><b>".$project['related_module'].": </b><a href='index.php?module=".$project['related_module']."&action=DetailView&record=".$project['related_module_record']."' target='_blank'>{$project_bean->name}</a></td>";
			}
			$total_cstm = 0.0;
			foreach ($dates as $date) {
				if(isset($project['track_date'][$date])){
					$html .= "<td><strong>".$project['track_date'][$date]['hours']."</strong></td>";
					$total_cstm += (float)$project['track_date'][$date]['hours'];
					$sum_all_projects[$date] += (float)$project['track_date'][$date]['hours'];
					$total_hours += (float)$project['track_date'][$date]['hours'];
				}else{
					$html .= "<td><strong></strong></td>";
				}
				$html .= "<td style='width:30px;'>&nbsp;</td>";
			}
			$html .= "<td><strong>{$total_cstm}</strong></td>";
			$html .= "</tr>";
		}
		$html .= "<tr>
		<td style='width: 275px;'><strong>Total</strong></td>";
		for($k=0; $k<7; $k++){
				$field_name = 'sumallprojects_'.$dates[$k];
				$field_val = $sum_all_projects[$dates[$k]];
				if (!empty($field_val) && $field_val != 0) {
					$html .= '<td style = "text-align:center"><strong ><div style = "background-color:#dcdcdc" class="per_day_hours" name="'.$field_name.'" id="'.$field_name.'">'.$field_val.'</div></strong></td>';
				} else {
					$html .= '<td style = "text-align:center"><strong><div class="per_day_hours" name="'.$field_name.'" id="'.$field_name.'">'.$field_val.'</div></strong></td>';
				}
				$html .= '<td style="width: 30px;"></td>';
		}
		if (!empty($total_hours) && $total_hours !=0) {
			$html .= "<td style='width: 30px;text-align:center'><strong><div style = 'background-color:#dcdcdc' id='total_hours' name='total_hours' >$total_hours</div></strong></td>
		</tr></table></form>";
		} else {
			$html .= "<td style='width: 30px;text-align:center'><strong><div id='total_hours' name='total_hours' >$total_hours</div></strong></td>
		</tr></table></form>";
		}
		echo $html;
	}
	
	function getProjectsList($start_date, $end_date, $user_id, $selected_module, $related_module){
		global $db, $current_user;
		
		$current_user_id = $user_id;
		$selected_module_bean = BeanFactory::getBean($selected_module);
		$module_table_name = $selected_module_bean->table_name;
		
		$status_where = "";
		if($selected_module == "Project" || $selected_module == "ProjectTask"){
			$status_where = "and status not in('Completed', 'Closed', 'On_Hold')";
		}
		
		$i = 0;
		$project_list = array();
		if($selected_module == "Project"){
			$getProjectQry = "Select DISTINCT p.id, p.name, p.date_entered from project p INNER JOIN project_task pt on pt.project_id = p.id where p.deleted = 0 and pt.deleted = 0 and p.status not in('Completed', 'Closed', 'On_Hold') and pt.assigned_user_id = '$current_user_id' ORDER BY p.date_entered DESC";
		}else if($selected_module == "Accounts"){
			$getProjectQry = "Select DISTINCT a.id, a.name from accounts a INNER JOIN cases c ON a.id = c.account_id where a.deleted = 0 and c.deleted = 0 and c.assigned_user_id = '$current_user_id' $status_where ";
		}else{
			$getProjectQry = "Select * from $module_table_name p where p.deleted = 0 and assigned_user_id = '$current_user_id' $status_where ORDER BY p.date_entered DESC";
		}
		$resProject= $db->query($getProjectQry);
		while($rowProjectId = $db->fetchByAssoc($resProject))
		{
			$project_list[$i]['project_id'] = $rowProjectId['id'];
			if(isset($rowProjectId['name'])){
				$project_list[$i]['project_name'] = $rowProjectId['name'];
			}
			else{
				$project_list[$i]['project_name'] = $rowProjectId['first_name']." ".$rowProjectId['last_name'];
			}
			$timeSheetResult = $this->getProjectTimeTracked($rowProjectId['id'], $start_date, $end_date, $user_id);
			$project_list[$i]['project_tasks_list'] = $this->getProjectTasksList($rowProjectId['id'], $selected_module, $related_module, $current_user_id);
			$project_list[$i]['project_time_sheet_tracker'] = $timeSheetResult['project_time_sheet_list'];
			$i++;
		}
		$custom_array = array();
		$timesheet_id_detail = $this->bean->id;
		$getCustomProjectTimeSheetQry = "Select * from sp_timesheet_tracker where deleted = 0 and track_date >= '$start_date' and track_date <= '$end_date' and user_id_c = '$user_id' and timesheet_id = '$timesheet_id_detail' ORDER BY track_date ASC";
		$resCustomProjectTimeSheet = $db->query($getCustomProjectTimeSheetQry);
		while($rowCustomProjectTimeSheetId = $db->fetchByAssoc($resCustomProjectTimeSheet))
		{
			$temp_array = array();
			$project_task_id = $rowCustomProjectTimeSheetId['projecttask_id_c'];
			if(!empty($rowCustomProjectTimeSheetId['related_module'])){
				$temp_array = array(
					'hours' => $rowCustomProjectTimeSheetId['hours'],
					'track_date' => $rowCustomProjectTimeSheetId['track_date'],
					'related_module' => $rowCustomProjectTimeSheetId['related_module'],
					'related_module_record' => $project_task_id,
				);
				$custom_array[$project_task_id."_".$rowCustomProjectTimeSheetId['track_date']] = $temp_array;
			}
		}
		return array('project_list' => $project_list, 'custom_array' => $custom_array);
	}
	
	function getProjectTimeTracked($project_id, $start_date, $end_date, $user_id){
		global $db;
		$i = 0;
		$timesheet_id_detail = $this->bean->id;
		
		$project_time_sheet_list = array();
		$getProjectTimeSheetQry = "Select * from sp_timesheet_tracker where deleted = 0 and project_id_c='$project_id'  and track_date >= '$start_date' and track_date <= '$end_date' and user_id_c = '$user_id' and timesheet_id = '$timesheet_id_detail' ORDER BY track_date ASC";
		$resProjectTimeSheet = $db->query($getProjectTimeSheetQry);
		while($rowProjectTimeSheetId = $db->fetchByAssoc($resProjectTimeSheet))
		{
			$project_task_id = $rowProjectTimeSheetId['projecttask_id_c'];
			$track_date = $rowProjectTimeSheetId['track_date'];
			$hours = $rowProjectTimeSheetId['hours'];
			
			if(empty($project_task_id)){
				$index = "project_". $project_id ."_". $track_date;
			}
			else{
				$index = "projecttask_". $project_id. "_". $project_task_id ."_". $track_date;
			}
			$project_time_sheet_list[$index] = $hours;
		}
		return array('project_time_sheet_list' => $project_time_sheet_list);
	}
	
	function getProjectTasksList($selected_module_id, $selected_module, $related_module, $current_user_id){
		global $db, $sugar_config, $current_user;
		
		$i = 0;
		$project_task_list = array();
		
		$bean_id = $this->bean->id;
		if(!empty($bean_id)){
			$relationship_name = $this->bean->related_module_relationship;
		}
		else{
			$relationship_name = $sugar_config['related_module_relationship_'.$current_user_id];
		}
		
		$selected_module_bean = BeanFactory::getBean($selected_module, $selected_module_id);
		
		$selected_module_bean->load_relationship($relationship_name);
		$related_module_records = $selected_module_bean->$relationship_name->getBeans();
		foreach($related_module_records as $related_module_record){
			if($this->validateStatus($related_module_record, $related_module) && $related_module_record->assigned_user_id == $current_user_id){
				$project_task_list[$i]['project_task_id'] = $related_module_record->id;
				$project_task_list[$i]['project_task_name'] = $related_module_record->name;
				$i++;
			}
		}
		return $project_task_list;
	}
	
	public function validateStatus($related_module_record, $related_module){
		if($related_module == "Project" || $related_module == "ProjectTask")
		{
			if($related_module_record->status == "Completed" || $related_module_record->status == "Closed" || $related_module_record->status == "On_Hold")
			{
				return false;
			}
		}
		return true;
	}
	
	function getDates($date){
	  $week = date("W",strtotime($date));
	  $year = date("Y",strtotime($date));
	  $dto = new DateTime();
	  $result['start'] = $dto->setISODate($year, $week, 1)->format('Y-m-d');
	  $result['end'] = $dto->setISODate($year, $week, 7)->format('Y-m-d');
	  return $result;
	}
	
	public function getWeekDates($start_date, $end_date){
		$start_timestamp = strtotime($start_date);
		$end_timestamp   = strtotime($end_date);
		$dates = array();
		while ($start_timestamp <= $end_timestamp) {
			$dates[] = date('Y-m-d', $start_timestamp);
			$start_timestamp = strtotime('+1 day', $start_timestamp);
		}
		return $dates;
	}
	
	public function getNextPreviousDate($date, $how_much_days){
		$week_start_date = strtotime($date);
		$week_start_date = strtotime($how_much_days, $week_start_date);
		return $week_start_date = date('Y-m-d', $week_start_date);
	}
	
	public function getTimesheetDates($timesheet_id){
		global $db;
		
		$resultTimesheet = array();
		$query = "Select * from sp_timesheet t where t.deleted = 0 and t.id = '$timesheet_id'";
		
		$result= $db->query($query);
		while($row = $db->fetchByAssoc($result))
		{
			$resultTimesheet["track_date_from"] = $row["track_date_from"];	
			$resultTimesheet["track_date_to"] = $row["track_date_to"];	
		}
		return $resultTimesheet;
	}
}
