<?php
// created: 2019-03-30 13:22:29
$dictionary["sg_SendGrid_Event"]["fields"]["sg_sendgrid_event_emails"] = array (
  'name' => 'sg_sendgrid_event_emails',
  'type' => 'link',
  'relationship' => 'sg_sendgrid_event_emails',
  'source' => 'non-db',
  'module' => 'Emails',
  'bean_name' => 'Email',
  'vname' => 'LBL_SG_SENDGRID_EVENT_EMAILS_FROM_EMAILS_TITLE',
  'id_name' => 'sg_sendgrid_event_emailsemails_ida',
);
$dictionary["sg_SendGrid_Event"]["fields"]["sg_sendgrid_event_emails_name"] = array (
  'name' => 'sg_sendgrid_event_emails_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SG_SENDGRID_EVENT_EMAILS_FROM_EMAILS_TITLE',
  'save' => true,
  'id_name' => 'sg_sendgrid_event_emailsemails_ida',
  'link' => 'sg_sendgrid_event_emails',
  'table' => 'emails',
  'module' => 'Emails',
  'rname' => 'name',
);
$dictionary["sg_SendGrid_Event"]["fields"]["sg_sendgrid_event_emailsemails_ida"] = array (
  'name' => 'sg_sendgrid_event_emailsemails_ida',
  'type' => 'link',
  'relationship' => 'sg_sendgrid_event_emails',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_SG_SENDGRID_EVENT_EMAILS_FROM_SG_SENDGRID_EVENT_TITLE',
);
