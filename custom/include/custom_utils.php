<?php
function getKeyRecords(){
	global $db;
	 require_once 'modules/ACLRoles/ACLRole.php';
	 $child_user_ids = array();
     //Get the current user's role
     $objACLRole = new ACLRole();
     $roles = $objACLRole->getUserRoles($GLOBALS['current_user']->id);
	 if (in_array('Manager', $roles)) {
		 
		$sql = "select id from users where deleted = 0 and reports_to_id is not null and reports_to_id !=''";
		$result = $GLOBALS['db']->query($sql);

		while($row = $GLOBALS['db']->fetchByAssoc($result) )
		{
				//Use $row['id'] to grab the id fields value
				$child_user_ids[] = $row['id'];
		}
	 }
	 return $child_user_ids;
}
function getAllChildAccounts($accountId){
	$sql = "select id from accounts where accounts.parent_id = '" . $accountId . "'";
	$result = $GLOBALS['db']->query($sql);

		while($row = $GLOBALS['db']->fetchByAssoc($result) )
		{
				//Use $row['id'] to grab the id fields value
				$child_user_ids[] = $row['id'];
		}
	$child_user_ids[] = $accountId;
	$accounts_id = "'" . implode( "','",$child_user_ids) . "'";
	return $accounts_id;
}
function CheckIsManager(){
	global $db;
	 require_once 'modules/ACLRoles/ACLRole.php';
	 $child_user_ids = array();
     //Get the current user's role
     $objACLRole = new ACLRole();
     $roles = $objACLRole->getUserRoles($GLOBALS['current_user']->id);
	 if (in_array('Manager - Admin', $roles)){
		 return true;
	 }
	 return false;
}
