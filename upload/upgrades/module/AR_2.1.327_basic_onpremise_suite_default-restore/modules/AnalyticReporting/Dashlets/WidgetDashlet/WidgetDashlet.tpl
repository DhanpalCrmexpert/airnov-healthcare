<html>
<head>
    <title>{$CHARTTITLE}</title>
</head>
<body>

<style>
{literal}
body, html {
    font-family: "Open Sans", "Helvetica Neue", Arial, sans-serif;
    font-size:12px;
    line-height: 18px;
}
/* Center SVG element into div */
.ar-chart {
    height:100%;
    min-width: 100px;
    min-height: 100px;
    overflow: hidden;
}
.dashboard .thumbnail.dashlet:not(.collapsed) .ar-chart {
    height:450px;
    margin:8px;
}
.sidebar-content .thumbnail.dashlet:not(.collapsed) .ar-chart {
    height:350px;
    margin:8px;
}
.nv-funnelChart svg text {
    fill: #fff !important;
}
.ar-chart > svg {
    display:block !important;
    margin:auto !important;
    max-height:none;
    height:100%;
}
.ar-chart svg:not(.nvd3-svg) {
    width: auto;
    max-height:none;
}
.leaflet-overlay-pane svg {
    width:auto !important;
    height:auto !important;
}
/* Fix visibility for gauge chart and align to center */
svg.nvd3-svg {
    overflow: visible !important;
    display: block;
    margin:0 auto;
}
{/literal}
</style>

{* Use Sugar/Suite default jQuery (for MapChart) *}
<script src="include/javascript/jquery/jquery-min.js"></script>
<script src="modules/AnalyticReporting/assets/js/widget/widgetApp.min.js"></script>

<div id="ar-chart" class="ar-chart"></div>

<script type="text/javascript">
    var reportData = {$REPORTDATA};

    {literal}

    var widgetId = getWidgetId();

    // Set ar-chart element unique
    $el = document.getElementById("ar-chart");
    $el.setAttribute("id", $el.getAttribute("id") + widgetId);
    var store = window.ARWidgetApp($el.getAttribute("id"), reportData, $el.offsetWidth, $el.offsetHeight);

    wrapWidgetTitleInLink(reportData);

    /**
     * Wrap widget title in link to report
     */
    function wrapWidgetTitleInLink(reportData)
    {
        var parent = window.parent.document;
        if(!parent) {
            return false;
        }

        try {
            var headerId = "#dashlet_header_"+ getWidgetId();
            var titleQuery = headerId + " h3 span:last-child"
            // Retrieve <span>Widget title</span>
            var titleDom = parent.querySelector(titleQuery);

            // create wrapper container
            var linkWrapper = document.createElement('a');
            linkWrapper.setAttribute('href', reportData.config.urls.report + reportData.id);
            linkWrapper.setAttribute('target', "_blank");
            linkWrapper.setAttribute('style', "color:#fff")
            linkWrapper.innerText = titleDom.innerText;
            // Clear existing title
            titleDom.innerHTML = '';
            // Add title wrapped in link to old title place
            titleDom.appendChild(linkWrapper);
        } catch(e) {
            console.log("Can't wrapWidgetTitleInLink: ", e);
        }
    }

    /**
     * Try to get ID from iFrame data-id attribute, if set
     * @returns {*}
     */
    function getWidgetId() {
        var id;
        var frameEl = window.frameElement;
        if (frameEl) {
            id = frameEl.dataset.id;
        }

        return id;
    }
{/literal}
</script>

</body>
</html>