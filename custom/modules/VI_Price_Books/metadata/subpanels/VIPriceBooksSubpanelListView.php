<?php
/*********************************************************************************
 * This file is part of package Price Books.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Price Books is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
$subpanel_layout['list_fields'] = array (
    'name' => array (
        'vname' => 'LBL_NAME',
        'widget_class' => 'SubPanelDetailViewLink',
        'width' => '45%',
        'default' => true,
    ),
    'account_name' => array (
        'name' => 'account_name',
        'module' => 'Accounts',
        'target_record_key' => 'account_id',
        'target_module' => 'Accounts',
        'widget_class' => 'SubPanelDetailViewLink',
        'vname' => 'LBL_ACCOUNT_NAME',
        'width' => '45%',
        'default' => true,
    ),
    'account_id' => array(
        'usage' => 'query_only',
    ),
    'contact_name' => array (
        'name' => 'contact_name',
        'module' => 'Contacts',
        'target_record_key' => 'contact_id',
        'target_module' => 'Contacts',
        'widget_class' => 'SubPanelDetailViewLink',
        'vname' => 'LBL_CONTACT_NAME',
        'width' => '45%',
        'default' => true,
    ),
    'contact_id' => array(
        'usage' => 'query_only',
    ),
    'edit_button' => array (
        'vname' => 'LBL_EDIT_BUTTON',
        'widget_class' => 'SubPanelEditButton',
        'module' => 'VI_Price_Books',
        'width' => '4%',
        'default' => true,
    ),
    'date_modified' => array (
        'vname' => 'LBL_DATE_MODIFIED',
        'width' => '45%',
        'default' => true,
    ),
    'remove_button' => array (
        'vname' => 'LBL_REMOVE',
        'widget_class' => 'SubPanelRemoveButton',
        'module' => 'VI_Price_Books',
        'width' => '5%',
        'default' => true,
    ),
);