<?php
/*********************************************************************************
 * This file is part of package Price Books.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Price Books is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
$layout_defs['Contacts']['subpanel_setup']['contact_vi_price_books'] = array (
    'order' => 99,
    'module' => 'VI_Price_Books',
    'subpanel_name' => 'VIPriceBooksSubpanelListView',
    'sort_order' => 'asc',
    'sort_by' => 'date_entered',
    'title_key' => 'LBL_PRICE_BOOKS_LABEL',
    'get_subpanel_data' => 'vi_price_books', 
    'top_buttons' => array(
    array('widget_class' => 'SubPanelTopCreateButton'),
        array('widget_class' => 'SubPanelTopSelectButton', 'popup_module' => 'VI_Price_Books'),
    ),
);

?>