<?php

 return array (
  'Opportunities' => 
  array (
    'field' => 'sales_stage',
    'limit' => '10000',
    'views' => 
    array (
      0 => 'Closed Lost',
      1 => 'Prospecting',
      2 => 'Needs Analysis',
      3 => 'Perception Analysis',
      4 => 'Negotiation/Review',
      5 => 'Closed Won',
    ),
  ),
);