<?php
$GLOBALS['app_list_strings']['sales_stage_dom']=array (
  'Closed Lost' => 'Ideataion: Initial contact(0%)',
  'Prospecting' => 'Price Quote: Product Design(10%)',
  'Needs Analysis' => 'Product Qualification: Customer agreement proposal(25%)',
  'Perception Analysis' => 'Production Process Qualification: Sampling(50%)',
  'Negotiation/Review' => 'Performance Qualification: First Order(75%)',
  'Closed Won' => 'Won(100%)',
  'Qualification' => 'Qualification',
  'Value Proposition' => 'Value Proposition',
  'Id. Decision Makers' => 'Identifying Decision Makers',
  'Proposal/Price Quote' => 'Proposal/Price Quote',
);
$GLOBALS['app_list_strings']['sales_stage_dom']=array (
  'Closed Lost' => 'Ideataion: Initial contact (0%)',
  'Prospecting' => 'Price Quote: Product Design (10%)',
  'Needs Analysis' => 'Product Qualification: Customer agreement proposal (25%)',
  'Perception Analysis' => 'Production Process Qualification: Sampling (50%)',
  'Negotiation/Review' => 'Performance Qualification: First Order (75%)',
  'Closed Won' => 'Won (100%)',
  'Qualification' => 'Qualification',
  'Value Proposition' => 'Value Proposition',
  'Id. Decision Makers' => 'Identifying Decision Makers',
  'Proposal/Price Quote' => 'Proposal/Price Quote',
);
$GLOBALS['app_list_strings']['sales_stage_dom']=array (
  'Closed Lost' => 'Ideataion: Initial contact (0%)',
  'Prospecting' => 'Price Quote: Product Design (10%)',
  'Needs Analysis' => 'Product Qualification: Customer agreement proposal (25%)',
  'Perception Analysis' => 'Production Process Qualification: Sampling (50%)',
  'Negotiation/Review' => 'Performance Qualification: First Order (75%)',
  'Closed Won' => 'Won (100%)',
);
$GLOBALS['app_list_strings']['npr_list']=array (
  '' => '',
  'Yes' => 'Yes',
  'No' => 'No',
);
$GLOBALS['app_list_strings']['nda_list']=array (
  '' => '',
  'Yes' => 'Yes',
  'No' => 'No',
);
$GLOBALS['app_list_strings']['sales_stage_dom']=array (
  'Closed Lost' => 'Ideataion: Initial contact (0)',
  'Prospecting' => 'Price Quote: Product Design (10)',
  'Needs Analysis' => 'Product Qualification: Customer agreement proposal (25)',
  'Perception Analysis' => 'Production Process Qualification: Sampling (50)',
  'Negotiation/Review' => 'Performance Qualification: First Order (75)',
  'Closed Won' => 'Won (100)',
);
$GLOBALS['app_list_strings']['sales_stage_dom']=array (
  'Closed Lost' => 'Ideataion: Initial Contract (0)',
  'Prospecting' => 'Price Quote: Product Design (10)',
  'Needs Analysis' => 'Product Qualification: Customer agreement proposal (25)',
  'Perception Analysis' => 'Production Process Qualification: Sampling (50)',
  'Negotiation/Review' => 'Performance Qualification: First Order (75)',
  'Closed Won' => 'Won (100)',
);
$GLOBALS['app_list_strings']['account_type_dom']=array (
  '' => '',
  'Analyst' => 'Key Account',
  'Competitor' => 'Local Account',
  'Customer' => 'Normal Account',
);

$GLOBALS['app_list_strings']['sales_stage_dom']=array (
  '' => '',
  'Closed Lost' => 'Ideation: Initial Contact',
  'Prospecting' => 'Price Quote: Product Design',
  'Needs Analysis' => 'Product Qualification: Customer agreement proposal',
  'Perception Analysis' => 'Production Process Qualification: Sampling',
  'Negotiation/Review' => 'Performance Qualification: First Order',
  'Closed Won' => 'Won',
);
$app_list_strings['moduleList']['Leads']='Prospects';
$app_list_strings['moduleListSingular']['Leads']='Prospect';
$app_list_strings['record_type_display']['Leads']='Prospect';
$app_list_strings['parent_type_display']['Leads']='Prospect';
$app_list_strings['record_type_display_notes']['Leads']='Prospect';
$app_list_strings['moduleList']['Accounts']='Customers';
$app_list_strings['moduleListSingular']['Accounts']='Customer';
$app_list_strings['record_type_display']['Accounts']='Customer';
$app_list_strings['parent_type_display']['Accounts']='Customer';
$app_list_strings['record_type_display_notes']['Accounts']='Customer';
$GLOBALS['app_list_strings']['customer_type_list']=array (
  '' => '',
  'Customer' => 'Customer',
  'Prospect' => 'Prospect',
);
$GLOBALS['app_list_strings']['relationship_quality_list']=array (
  '' => '',
  'Strong' => 'Strong',
  'Medium' => 'Medium',
  'Weak' => 'Weak',
);
$GLOBALS['app_list_strings']['department_ahp_list']=array (
  '' => '',
  'Admin' => 'Admin',
  'Change_Control' => 'Change Control',
  'Design_Centre' => 'Design Centre',
  'Engineering' => 'Engineering',
  'Finance' => 'Finance',
  'HSE_and_Regulatory' => 'HSE and Regulatory',
  'Marketing' => 'Marketing',
  'Other' => 'Other',
  'Procurement' => 'Procurement',
  'Production' => 'Production',
  'Quality' => 'Quality',
  'Sales' => 'Sales',
  'Supply_Chain' => 'Supply Chain',
  'Technology_RandD' => 'Technology / R&D',
);

$GLOBALS['app_list_strings']['salutation_dom']=array (
  '' => '',
  'Mr.' => 'Mr.',
  'Ms.' => 'Ms.',
  'Mrs.' => 'Mrs.',
  'Miss' => 'Miss',
  'Dr.' => 'Dr.',
  'Prof.' => 'Prof.',
  'Ing.' => 'Ing.',
  'Lic.' => 'Lic.',
);
$GLOBALS['app_list_strings']['opportunity_status_list']=array (
  '' => '',
  'Lost' => 'Lost',
  'Open' => 'Open',
  'Won' => 'Won',
);
$GLOBALS['app_list_strings']['opportunity_type_dom']=array (
  '' => '',
  'Existing Business' => 'Existing Business',
  'New Business' => 'New Business',
  'Substitution' => 'Substitution',
);
$GLOBALS['app_list_strings']['region_ship_to_list']=array (
  '' => '',
  'AMERICAS' => 'AMERICAS',
  'APAC' => 'APAC',
  'EMEA' => 'EMEA',
  'INDIA' => 'INDIA',
);
$GLOBALS['app_list_strings']['market_segment_list']=array (
  '' => '',
  'U1_PHARMA' => 'U1-PHARMA',
  'U2_NUTRA' => 'U2 - NUTRA',
  'U3_DIAG_MED' => 'U3 - DIAG/MED',
  'U4_DISTRIBUTOR' => 'U4 - DISTRIBUTOR',
  'U5_OTHER' => 'U5 - OTHER',
);
$GLOBALS['app_list_strings']['currency_list']=array (
  '' => '',
  'USD' => 'USD',
);
$GLOBALS['app_list_strings']['customer_account_group_list']=array (
  '' => '',
  'Sold_to_party' => 'Sold-to party',
  'Ship_to_party' => 'Ship-to party',
  'Payer' => 'Payer',
);
$GLOBALS['app_list_strings']['competitor_list']=array (
  '' => '',
  'Camen_Quimica_S_A_de_C_V' => 'Camen Quimica, S.A. de C.V.',
  'CSP_Technologies_Inc' => 'CSP Technologies, Inc',
  'Desiccare_Inc' => 'Desiccare, Inc.',
  'Dry_Pak_Industries' => 'Dry Pak Industries',
  'Flow_Dry_Technology_Inc' => 'Flow Dry Technology Inc.',
  'Multisorb_Technologies_Inc' => 'Multisorb Technologies Inc.',
  'Sanner_GmbH' => 'Sanner GmbH',
  'Unknown' => 'Unknown',
  'Wisepac_Active_Packaging_Components_Co_Ltd' => 'Wisepac Active Packaging Components Co., Ltd',
);
$GLOBALS['app_list_strings']['target_year_list']=array (
  '' => '',
  'First_Year' => 'First Year',
  'Second_Year' => 'Second Year',
  'Third_Year' => 'Third Year',
);
$GLOBALS['app_list_strings']['associated_year_list']=array (
  '' => '',
  2019 => '2019',
  2020 => '2020',
  2021 => '2021',
  2022 => '2022',
  2023 => '2023',
  2024 => '2024',
  2025 => '2025',
  2026 => '2026',
  2027 => '2027',
  2028 => '2028',
  2029 => '2029',
  2030 => '2030',
);
$app_list_strings['moduleList']['Cases']='QNS';
$app_list_strings['moduleListSingular']['Cases']='QNS';
$app_list_strings['record_type_display']['Cases']='QNS';
$app_list_strings['parent_type_display']['Cases']='QNS';
$app_list_strings['record_type_display_notes']['Cases']='QNS';