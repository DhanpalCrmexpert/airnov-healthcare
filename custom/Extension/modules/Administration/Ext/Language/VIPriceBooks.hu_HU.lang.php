<?php
/*********************************************************************************
 * This file is part of package Price Books.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Price Books is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
$mod_strings['LBL_PRICE_BOOKS'] = "Árkönyvek";
$mod_strings['LBL_PRICE_BOOKS_DESCRIPTION'] = "Bizonyos kapcsolatok / fiókok termékkönyveinek kezelése.";
$mod_strings['LBL_ENABLE_PRICE_BOOKS'] = "Enable Price Books Feature";
$mod_strings['LBL_PRICE_BOOKS_ACTIVATED'] = "Az árkönyvek szolgáltatás aktiválva sikerült.";
$mod_strings['LBL_PRICE_BOOKS_DEACTIVATED'] = "Az árkönyvek szolgáltatás sikeresen deaktiválva.";
$mod_strings['LBL_CLICK_HERE'] = "Kattintson ide";
$mod_strings['LBL_MANAGE_PRICEBOOK'] = "Az árkönyvek kezelése";
//license
$mod_strings['LBL_LICENCE_ACTIVE_LABEL'] = "VIPriceBooksLicenseAddon már nem aktív a következő ok miatt:";
$mod_strings['LBL_LICENCE_ISSUES'] = "A felhasználóknak a hozzáférés korlátozása korlátozódik, amíg a problémát nem oldják meg";
$mod_strings['LBL_CLICK_HERE'] = "Kattintson ide";
$mod_strings['LBL_LICENCE_ACTIVE'] = "VIPriceBooksLicenseAddon már nem aktív";
$mod_strings['LBL_RENEW_LICENCE'] = "Kérjük, újítsa meg előfizetését vagy ellenőrizze a licenc konfigurációját.";
$mod_strings['LBL_UPDATE_LICENSE'] = "Frissítse az engedélyt";
?>