<?php
/**
 * Copyright (c) SalesAgility Ltd.  All Rights Reserved.
 * Licensed under the GPL3 License.  See License in the project root
 * for license information.
 *
 * @package Outlook Addin for SuiteCRM
 * @copyright 2020 SalesAgility Ltd http://www.salesagility.com
 * @author SalesAgility <info@salesagility.com
 *
 */

$storeConfig = array (
    'base_api_url' => 'https://store.suitecrm.com/api/v1',
    'config_name' => 'sa_outlook_plugin',
    'pub_key' => 'b8794235718652747b82fd713deac078'
);
