<?php
/*********************************************************************************
 * This file is part of package Price Books.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Price Books is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
$manifest = array (
   0 => 
  array (
    'acceptable_sugar_versions' => 
    array (
      0 => '',
    ),
  ),
  1 => 
  array (
    'acceptable_sugar_flavors' => 
    array (
      0 => 'CE',
      1 => 'PRO',
      2 => 'ENT',
    ),
  ),
  'key' => '',
  'name' => 'Price Books',
  'description' => 'Price Books',
  'author' => 'Variance Infotech PVT LTD',
  'version' => 'v1.0',
  'is_uninstallable' => true,
  'published_date' => '2020-07-17 19:00:00',
  'type' => 'module',
  'readme' => '',
  'icon' => '',
  'remove_tables' => 'prompt',
);

$installdefs = array (
  'id' => 'price books',
  'beans' => 
    array (
        array (
            'module' => 'VIPriceBooksLicenseAddon',
            'class' => 'VIPriceBooksLicenseAddon',
            'path' => 'modules/VIPriceBooksLicenseAddon/VIPriceBooksLicenseAddon.php',
            'tab' => false,
        ),
        array (
            'module' => 'VI_Price_Books',
            'class' => 'VI_Price_Books',
            'path' => 'modules/VI_Price_Books/VI_Price_Books.php',
            'tab' => true,
        ),
    ),
  'post_execute' => array(  0 =>  '<basepath>/scripts/post_execute.php',),
  'post_install' => array(  0 =>  '<basepath>/scripts/post_install.php',),
  'post_uninstall' => array( 0 =>  '<basepath>/scripts/post_uninstall.php',),
  'pre_execute' => array(  0 =>  '<basepath>/scripts/pre_execute.php',),
    'copy' => array (
        0 => array (
            'from' => '<basepath>/custom/Extension/application/Ext/EntryPointRegistry/VIPriceBooksEntryPoint.php',
            'to' => 'custom/Extension/application/Ext/EntryPointRegistry/VIPriceBooksEntryPoint.php',
        ),
        1 => array (
            'from' => '<basepath>/custom/Extension/application/Ext/LogicHooks/VIPriceBooksLogicHook.php',
            'to' => 'custom/Extension/application/Ext/LogicHooks/VIPriceBooksLogicHook.php',
        ),
        2 => array (
            'from' => '<basepath>/custom/Extension/modules/Accounts/Ext/Language/vi_pricebooks_accounts.de_DE.lang.php',
            'to' => 'custom/Extension/modules/Accounts/Ext/Language/vi_pricebooks_accounts.de_DE.lang.php',
        ),
        3 => array (
            'from' => '<basepath>/custom/Extension/modules/Accounts/Ext/Language/vi_pricebooks_accounts.en_us.lang.php',
            'to' => 'custom/Extension/modules/Accounts/Ext/Language/vi_pricebooks_accounts.en_us.lang.php',
        ),
        4 => array (
            'from' => '<basepath>/custom/Extension/modules/Accounts/Ext/Language/vi_pricebooks_accounts.es_ES.lang.php',
            'to' => 'custom/Extension/modules/Accounts/Ext/Language/vi_pricebooks_accounts.es_ES.lang.php',
        ),
        5 => array (
            'from' => '<basepath>/custom/Extension/modules/Accounts/Ext/Language/vi_pricebooks_accounts.fr_FR.lang.php',
            'to' => 'custom/Extension/modules/Accounts/Ext/Language/vi_pricebooks_accounts.fr_FR.lang.php',
        ),
        6 => array (
            'from' => '<basepath>/custom/Extension/modules/Accounts/Ext/Language/vi_pricebooks_accounts.hu_HU.lang.php',
            'to' => 'custom/Extension/modules/Accounts/Ext/Language/vi_pricebooks_accounts.hu_HU.lang.php',
        ),
        7 => array (
            'from' => '<basepath>/custom/Extension/modules/Accounts/Ext/Language/vi_pricebooks_accounts.it_IT.lang.php',
            'to' => 'custom/Extension/modules/Accounts/Ext/Language/vi_pricebooks_accounts.it_IT.lang.php',
        ),
        8 => array (
            'from' => '<basepath>/custom/Extension/modules/Accounts/Ext/Language/vi_pricebooks_accounts.nl_NL.lang.php',
            'to' => 'custom/Extension/modules/Accounts/Ext/Language/vi_pricebooks_accounts.nl_NL.lang.php',
        ),
        9 => array (
          'from' => '<basepath>/custom/Extension/modules/Accounts/Ext/Language/vi_pricebooks_accounts.pt_BR.lang.php',
          'to' => 'custom/Extension/modules/Accounts/Ext/Language/vi_pricebooks_accounts.pt_BR.lang.php',
        ),
        10 => array (
            'from' => '<basepath>/custom/Extension/modules/Accounts/Ext/Language/vi_pricebooks_accounts.ru_RU.lang.php',
            'to' => 'custom/Extension/modules/Accounts/Ext/Language/vi_pricebooks_accounts.ru_RU.lang.php',
        ),
        11 => array (
            'from' => '<basepath>/custom/Extension/modules/Accounts/Ext/Layoutdefs/VIPriceBookssubpanelsdef.php',
            'to' => 'custom/Extension/modules/Accounts/Ext/Layoutdefs/VIPriceBookssubpanelsdef.php',
        ),
        12 => array (
            'from' => '<basepath>/custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_accounts_pricebooks.php',
            'to' => 'custom/Extension/modules/Accounts/Ext/Vardefs/sugarfield_accounts_pricebooks.php',
        ), 
        13 => array (
            'from' => '<basepath>/custom/Extension/modules/Administration/Ext/ActionViewMap/VIPriceBooksAction_View_Map.ext.php',
            'to' => 'custom/Extension/modules/Administration/Ext/ActionViewMap/VIPriceBooksAction_View_Map.ext.php',
        ), 
        14 => array (
            'from' => '<basepath>/custom/Extension/modules/Administration/Ext/Administration/VIPriceBooksAdministration.ext.php',
            'to' => 'custom/Extension/modules/Administration/Ext/Administration/VIPriceBooksAdministration.ext.php',
        ), 
        15 => array (
            'from' => '<basepath>/custom/Extension/modules/Administration/Ext/Language/VIPriceBooks.de_DE.lang.php',
            'to' => 'custom/Extension/modules/Administration/Ext/Language/VIPriceBooks.de_DE.lang.php',
        ),
        16 => array (
            'from' => '<basepath>/custom/Extension/modules/Administration/Ext/Language/VIPriceBooks.en_us.lang.php',
            'to' => 'custom/Extension/modules/Administration/Ext/Language/VIPriceBooks.en_us.lang.php',
        ),
        17 => array (
            'from' => '<basepath>/custom/Extension/modules/Administration/Ext/Language/VIPriceBooks.es_ES.lang.php',
            'to' => 'custom/Extension/modules/Administration/Ext/Language/VIPriceBooks.es_ES.lang.php',
        ),
        18 => array (
            'from' => '<basepath>/custom/Extension/modules/Administration/Ext/Language/VIPriceBooks.fr_FR.lang.php',
            'to' => 'custom/Extension/modules/Administration/Ext/Language/VIPriceBooks.fr_FR.lang.php',
        ),
        19 => array (
            'from' => '<basepath>/custom/Extension/modules/Administration/Ext/Language/VIPriceBooks.hu_HU.lang.php',
            'to' => 'custom/Extension/modules/Administration/Ext/Language/VIPriceBooks.hu_HU.lang.php',
        ),
        20 => array (
            'from' => '<basepath>/custom/Extension/modules/Administration/Ext/Language/VIPriceBooks.it_IT.lang.php',
            'to' => 'custom/Extension/modules/Administration/Ext/Language/VIPriceBooks.it_IT.lang.php',
        ),
        21 => array (
            'from' => '<basepath>/custom/Extension/modules/Administration/Ext/Language/VIPriceBooks.nl_NL.lang.php',
            'to' => 'custom/Extension/modules/Administration/Ext/Language/VIPriceBooks.nl_NL.lang.php',
        ),
        22 => array (
            'from' => '<basepath>/custom/Extension/modules/Administration/Ext/Language/VIPriceBooks.pt_BR.lang.php',
            'to' => 'custom/Extension/modules/Administration/Ext/Language/VIPriceBooks.pt_BR.lang.php',
        ),
        23 => array (
            'from' => '<basepath>/custom/Extension/modules/Administration/Ext/Language/VIPriceBooks.ru_RU.lang.php',
            'to' => 'custom/Extension/modules/Administration/Ext/Language/VIPriceBooks.ru_RU.lang.php',
        ),
        24 => array (
            'from' => '<basepath>/custom/Extension/modules/Administration/Ext/Language/VIPriceBooks.ua_UA.lang.php',
            'to' => 'custom/Extension/modules/Administration/Ext/Language/VIPriceBooks.ua_UA.lang.php',
        ),
        25 => array (
            'from' => '<basepath>/custom/Extension/modules/Contacts/Ext/Language/vi_pricebooks_contacts.de_DE.lang.php',
            'to' => 'custom/Extension/modules/Contacts/Ext/Language/vi_pricebooks_contacts.de_DE.lang.php',
        ),
        26 => array (
            'from' => '<basepath>/custom/Extension/modules/Contacts/Ext/Language/vi_pricebooks_contacts.en_us.lang.php',
            'to' => 'custom/Extension/modules/Contacts/Ext/Language/vi_pricebooks_contacts.en_us.lang.php',
        ),
        27 => array (
            'from' => '<basepath>/custom/Extension/modules/Contacts/Ext/Language/vi_pricebooks_contacts.es_ES.lang.php',
            'to' => 'custom/Extension/modules/Contacts/Ext/Language/vi_pricebooks_contacts.es_ES.lang.php',
        ),
        28 => array (
            'from' => '<basepath>/custom/Extension/modules/Contacts/Ext/Language/vi_pricebooks_contacts.fr_FR.lang.php',
            'to' => 'custom/Extension/modules/Contacts/Ext/Language/vi_pricebooks_contacts.fr_FR.lang.php',
        ),
        29 => array (
            'from' => '<basepath>/custom/Extension/modules/Contacts/Ext/Language/vi_pricebooks_contacts.hu_HU.lang.php',
            'to' => 'custom/Extension/modules/Contacts/Ext/Language/vi_pricebooks_contacts.hu_HU.lang.php',
        ),
        30 => array (
            'from' => '<basepath>/custom/Extension/modules/Contacts/Ext/Language/vi_pricebooks_contacts.it_IT.lang.php',
            'to' => 'custom/Extension/modules/Contacts/Ext/Language/vi_pricebooks_contacts.it_IT.lang.php',
        ),
        31 => array (
            'from' => '<basepath>/custom/Extension/modules/Contacts/Ext/Language/vi_pricebooks_contacts.nl_NL.lang.php',
            'to' => 'custom/Extension/modules/Contacts/Ext/Language/vi_pricebooks_contacts.nl_NL.lang.php',
        ),
        32 => array (
            'from' => '<basepath>/custom/Extension/modules/Contacts/Ext/Layoutdefs/VIPriceBookssubpanelsdef.php',
            'to' => 'custom/Extension/modules/Contacts/Ext/Layoutdefs/VIPriceBookssubpanelsdef.php',
        ),
        33 => array (
            'from' => '<basepath>/custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_contacts_pricebooks.php',
            'to' => 'custom/Extension/modules/Contacts/Ext/Vardefs/sugarfield_contacts_pricebooks.php',
        ),
        34 => array (
            'from' => '<basepath>/custom/Extension/modules/VI_Price_Books/Ext/Language/VI_Price_Books.de_DE.lang.php',
            'to' => 'custom/Extension/modules/VI_Price_Books/Ext/Language/VI_Price_Books.de_DE.lang.php',
        ),
        35 => array (
            'from' => '<basepath>/custom/Extension/modules/VI_Price_Books/Ext/Language/VI_Price_Books.en_us.lang.php',
            'to' => 'custom/Extension/modules/VI_Price_Books/Ext/Language/VI_Price_Books.en_us.lang.php',
        ),
        36 => array (
            'from' => '<basepath>/custom/Extension/modules/VI_Price_Books/Ext/Language/VI_Price_Books.es_ES.lang.php',
            'to' => 'custom/Extension/modules/VI_Price_Books/Ext/Language/VI_Price_Books.es_ES.lang.php',
        ),
        37 => array (
            'from' => '<basepath>/custom/Extension/modules/VI_Price_Books/Ext/Language/VI_Price_Books.fr_FR.lang.php',
            'to' => 'custom/Extension/modules/VI_Price_Books/Ext/Language/VI_Price_Books.fr_FR.lang.php',
        ),
        38 => array (
            'from' => '<basepath>/custom/Extension/modules/VI_Price_Books/Ext/Language/VI_Price_Books.hu_HU.lang.php',
            'to' => 'custom/Extension/modules/VI_Price_Books/Ext/Language/VI_Price_Books.hu_HU.lang.php',
        ),
        39 => array (
            'from' => '<basepath>/custom/Extension/modules/VI_Price_Books/Ext/Language/VI_Price_Books.it_IT.lang.php',
            'to' => 'custom/Extension/modules/VI_Price_Books/Ext/Language/VI_Price_Books.it_IT.lang.php',
        ),
        40 => array (
            'from' => '<basepath>/custom/Extension/modules/VI_Price_Books/Ext/Language/VI_Price_Books.nl_NL.lang.php',
            'to' => 'custom/Extension/modules/VI_Price_Books/Ext/Language/VI_Price_Books.nl_NL.lang.php',
        ),
        41 => array (
            'from' => '<basepath>/custom/Extension/modules/VI_Price_Books/Ext/Language/VI_Price_Books.pt_BR.lang.php',
            'to' => 'custom/Extension/modules/VI_Price_Books/Ext/Language/VI_Price_Books.pt_BR.lang.php',
        ),
        42 => array (
            'from' => '<basepath>/custom/Extension/modules/VI_Price_Books/Ext/Language/VI_Price_Books.ru_RU.lang.php',
            'to' => 'custom/Extension/modules/VI_Price_Books/Ext/Language/VI_Price_Books.ru_RU.lang.php',
        ),
        43 => array (
            'from' => '<basepath>/custom/Extension/modules/VI_Price_Books/Ext/Language/VI_Price_Books.ua_UA.lang.php',
            'to' => 'custom/Extension/modules/VI_Price_Books/Ext/Language/VI_Price_Books.ua_UA.lang.php',
        ),
        44 => array (
            'from' => '<basepath>/custom/Extension/modules/VI_Price_Books/Ext/Vardefs/sugarfield_active_c.php',
            'to' => 'custom/Extension/modules/VI_Price_Books/Ext/Vardefs/sugarfield_active_c.php',
        ),
        45 => array (
            'from' => '<basepath>/custom/Extension/modules/VI_Price_Books/Ext/Vardefs/sugarfield_pricebooks_accounts.php',
            'to' => 'custom/Extension/modules/VI_Price_Books/Ext/Vardefs/sugarfield_pricebooks_accounts.php',
        ),
        46 => array (
            'from' => '<basepath>/custom/Extension/modules/VI_Price_Books/Ext/Vardefs/sugarfield_pricebooks_contacts.php',
            'to' => 'custom/Extension/modules/VI_Price_Books/Ext/Vardefs/sugarfield_pricebooks_contacts.php',
        ),
        47 => array (
            'from' => '<basepath>/custom/Extension/modules/VIPriceBooksLicenseAddon/',
            'to' => 'custom/Extension/modules/VIPriceBooksLicenseAddon/',
        ),
        48 => array (
            'from' => '<basepath>/custom/include/VIPriceBooks/css/VIPriceBooksLineItems.css',
            'to' => 'custom/include/VIPriceBooks/css/VIPriceBooksLineItems.css',
        ),
        49 => array (
            'from' => '<basepath>/custom/include/VIPriceBooks/css/VIPriceBooksLogicHook.css',
            'to' => 'custom/include/VIPriceBooks/css/VIPriceBooksLogicHook.css',
        ),
        50 => array (
            'from' => '<basepath>/custom/include/VIPriceBooks/js/VIPriceBooksEditViewCheckCondition.js',
            'to' => 'custom/include/VIPriceBooks/js/VIPriceBooksEditViewCheckCondition.js',
        ),
        51 => array (
            'from' => '<basepath>/custom/include/VIPriceBooks/js/VIPriceBooksLineItems.js',
            'to' => 'custom/include/VIPriceBooks/js/VIPriceBooksLineItems.js',
        ),
        52 => array (
            'from' => '<basepath>/custom/include/VIPriceBooks/js/VIPriceBooksLogicHook.js',
            'to' => 'custom/include/VIPriceBooks/js/VIPriceBooksLogicHook.js',
        ),
        53 => array (
            'from' => '<basepath>/custom/include/VIPriceBooks/VIPriceBooks.php',
            'to' => 'custom/include/VIPriceBooks/VIPriceBooks.php',
        ),
        54 => array (
            'from' => '<basepath>/custom/include/VIPriceBooks/VIPriceBooksInvoiceQuotes.php',
            'to' => 'custom/include/VIPriceBooks/VIPriceBooksInvoiceQuotes.php',
        ),
        55 => array (
            'from' => '<basepath>/custom/modules/Administration/css/VIPriceBooksConfig.css',
            'to' => 'custom/modules/Administration/css/VIPriceBooksConfig.css',
        ),
        56 => array (
            'from' => '<basepath>/custom/modules/Administration/js/VIPriceBooksConfig.js',
            'to' => 'custom/modules/Administration/js/VIPriceBooksConfig.js',
        ),
        57 => array (
            'from' => '<basepath>/custom/modules/Administration/tpl/vi_pricebooksconfig.tpl',
            'to' => 'custom/modules/Administration/tpl/vi_pricebooksconfig.tpl',
        ),
        58 => array (
            'from' => '<basepath>/custom/modules/Administration/tpl/vi_pricebooksconfig.tpl',
            'to' => 'custom/modules/Administration/tpl/vi_pricebooksconfig.tpl',
        ),
        59 => array (
            'from' => '<basepath>/custom/modules/Administration/views/view.vi_pricebooksconfig.php',
            'to' => 'custom/modules/Administration/views/view.vi_pricebooksconfig.php',
        ),
        60 => array (
            'from' => '<basepath>/custom/modules/VI_Price_Books/',
            'to' => 'custom/modules/VI_Price_Books/',
        ),
        61 => array (
            'from' => '<basepath>/custom/modules/VIPriceBooksLicenseAddon',
            'to' => 'custom/modules/VIPriceBooksLicenseAddon',
        ),
        62 => array (
            'from' => '<basepath>/custom/themes/Suite7/images/PriceBookIcon.png',
            'to' => 'custom/themes/Suite7/images/PriceBookIcon.png',
        ),
        63 => array (
            'from' => '<basepath>/custom/themes/Suite7/images/PriceBooks.png',
            'to' => 'custom/themes/Suite7/images/PriceBooks.png',
        ),
        64 => array (
            'from' => '<basepath>/custom/themes/Suite7/images/PriceBooks.svg',
            'to' => 'custom/themes/Suite7/images/PriceBooks.svg',
        ),
        65 => array (
            'from' => '<basepath>/custom/VIPriceBooks/VIPriceBooksAddProducts.php',
            'to' => 'custom/VIPriceBooks/VIPriceBooksAddProducts.php',
        ),
        66 => array (
            'from' => '<basepath>/custom/VIPriceBooks/VIPriceBooksConfigEnable.php',
            'to' => 'custom/VIPriceBooks/VIPriceBooksConfigEnable.php',
        ),
        67 => array (
            'from' => '<basepath>/custom/VIPriceBooks/VIPriceBooksFunction.php',
            'to' => 'custom/VIPriceBooks/VIPriceBooksFunction.php',
        ),
        68 => array (
            'from' => '<basepath>/custom/VIPriceBooks/VIPriceBooksGetData.php',
            'to' => 'custom/VIPriceBooks/VIPriceBooksGetData.php',
        ),
        69 => array (
            'from' => '<basepath>/custom/VIPriceBooks/VIPriceBooksGetProductPrice.php',
            'to' => 'custom/VIPriceBooks/VIPriceBooksGetProductPrice.php',
        ),
        70 => array (
            'from' => '<basepath>/custom/VIPriceBooks/VIPriceBooksGetUpdatedListPrice.php',
            'to' => 'custom/VIPriceBooks/VIPriceBooksGetUpdatedListPrice.php',
        ),
        71 =>  array (
          'from' => '<basepath>/modules/VIPriceBooksLicenseAddon/',
          'to' => 'modules/VIPriceBooksLicenseAddon/',
        ),
        72 =>  array (
          'from' => '<basepath>/custom/Extension/application/Ext/Include/Price_Books.php',
          'to' => 'custom/Extension/application/Ext/Include/Price_Books.php',
        ),
        73 =>  array (
          'from' => '<basepath>/custom/Extension/application/Ext/Language/en_us.Price_Books.php',
          'to' => 'custom/Extension/application/Ext/Language/en_us.Price_Books.php',
        ),
        74 => array (
          'from' => '<basepath>/modules/VI_Price_Books/',
          'to' => 'modules/VI_Price_Books/',
        ),
    ),
);
?>