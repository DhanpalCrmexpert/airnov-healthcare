<?php
/*********************************************************************************
 * This file is part of package Price Books.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Price Books is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
$mod_strings['LBL_ACTIVE'] = 'Aktiv';
$mod_strings['LBL_PERCENTAGE_MARK'] = 'Prozentzeichen';
$mod_strings['LBL_TYPE'] = 'Preistyp';
$mod_strings['LBL_ROUND_OFF'] = 'Round Off To';
$mod_strings['LBL_ACCOUNT_NAME'] = 'Kontoname';
$mod_strings['LBL_ACCOUNT_ID'] = 'Konto-ID';
$mod_strings['LBL_ACCOUNTS'] = 'Accounts';
$mod_strings['LBL_CONTACT_NAME'] = 'Kontaktname';
$mod_strings['LBL_CONTACT_ID'] = 'Kontakt-ID';
$mod_strings['LBL_CONTACTS'] = 'Kontakte';
$mod_strings['LBL_FLAT_PRICE'] = 'Flat Price';
$mod_strings['LBL_DIFFERENTIAL_PERCENTAGE'] = 'Differential By (%)';
$mod_strings['LBL_DIFFERENTIAL_DOLLAR'] = 'Differential By ($)';
$mod_strings['LBL_PRICE_BOOKS'] = 'Preisbücher';
$mod_strings['LBL_NAME'] = 'Name';
$mod_strings['LBL_PRICE_TYPE'] = 'Preistyp';
$mod_strings['LBL_ASSIGNED_TO'] = 'Zugewiesen an';
$mod_strings['LBL_NO_RECORDS'] = 'Keine Datensätze gefunden';
$mod_strings['LBL_MARK_UP'] = 'MarkUp (Add)';
$mod_strings['LBL_MARK_DOWN'] = 'MarkDown (Sub)';
$mod_strings['LBL_ADD_QUANTITY'] = 'Menge hinzufügen';
$mod_strings['LBL_PRODUCT_NAME'] = 'Produktname';
$mod_strings['LBL_PRODUCT_PRICE'] = 'Produktpreis';
$mod_strings['LBL_LIST_PRICE'] = 'Listenpreis';
$mod_strings['LBL_ADD_PRODUCT'] = 'Produkt hinzufügen';
$mod_strings['LBL_YES'] = 'Ja';
$mod_strings['LBL_NO'] = 'Nein';
$mod_strings['LBL_SELECT_PRODUCT_MSG'] = "Bitte Produkt auswählen, bevor Sie auf die Preisbücher klicken !!!";
$mod_strings['LBL_SELECT_PRICEBOOK_MSG'] = "Bitte nur ein Preisbuch auswählen.";
$mod_strings['LBL_PRODUCT_VALIDATION'] = 'Bitte wählen Sie ein anderes Produkt aus. Dieses Produkt ist bereits ausgewählt. ';
$mod_strings['LBL_QUANTITY_VALID_MSG'] = "Ungültiger Wert:";
$mod_strings['LBL_FROM_QUANTITY'] = "From Quantity";
$mod_strings['LBL_TO_QUANTITY'] = "Zur Menge";
$mod_strings['LBL_DIFFERENTIAL'] = "Differential";
$mod_strings['LBL_PRICE_BOOKS_ACCESS'] = "Bitte aktivieren Sie die Preisbuchfunktion für den Zugriff auf das Preisbuchmodul.";
$mod_strings['LBL_CLICK_HERE'] = "Hier klicken";
$mod_strings['LBL_ENABLE_LABEL'] = "zum Aktivieren.";
$mod_strings['LBL_FROM_QUANTITY_VALID'] = "Bitte geben Sie einen größeren Wert vom Mengenwert als vor dem Mengenwert ein.";
$mod_strings['LBL_TO_QUANTITY_VALID'] = "Bitte geben Sie einen größeren Wert als den Mengenwert ein.";
$mod_strings['LBL_PRODUCT_LIST_PRICE_VALID'] = "Bitte Produkt und Listenpreis hinzufügen, um Menge hinzuzufügen.";
$mod_strings['LBL_PRODCUT_PRICE_VALID'] = "Bitte Produktpreis für Produkt hinzufügen.";
$mod_strings['LBL_LIST_PRICE_VALID'] = "Bitte Listenpreis für Produkt hinzufügen.";
$mod_strings['LBL_QUANTITY_VALID_RANGE'] = "Mengenbereich für Produkt hinzufügen.";
$mod_strings['LBL_PRODUCT_VALID'] = "Bitte Produkt hinzufügen, um Menge hinzuzufügen.";
$mod_strings['LBL_PRODUCT_FIELDS_VALIDATION'] = "Bitte Produktnamen und Listenpreis für Preisbücher hinzufügen.";
$mod_strings['LBL_QUANTITY_FIELD_VALIDATION'] = "Bitte füllen Sie den Mengenbereich für leere Platten.";
$mod_strings['LBL_EDITVIEW_PANEL1'] = "Preisbuchdetails";
$mod_strings['LBL_DETAILVIEW_PANEL1'] = "Preisbuchdetails";
?> 