<!-- TEMPLATES -->
{literal}
<script id='reportBuilderOptionsTemplate' type='text/ractive'>
    <div class="report-builder-settings">

    <h2>{{t.reportBuilder}}</h2>
        <p>
        <label for="report-category">{{t.label_report_folder}}</label>
        <select id="report-category" value='{{selectedCategory}}'>
            {{#types.categoryTypes}}
                <option value='{{name}}' disabled="{{disabled}}">{{title}}</option>
            {{/types.categoryTypes}}
        </select>
        </p>
        <p>
            <label for="report-title">{{t.label_report_name}}</label>
            <input id="report-title" value='{{reportTitle}}'/>
        </p>
        <p>
            <label for="report-description">{{t.label_report_description}}</label>
            <input id="report-description" value='{{reportDescription}}'/>
        </p>
        <p>
        <label for="report-type">{{t.reportType}}</label>
        <select id="report-type" value='{{selectedReportType}}'>
            {{#types.reportTypes}}
                <option value='{{name}}' disabled="{{.disabled}}">{{title}}</option>
            {{/types.reportTypes}}
        </select>
        </p>
        {/literal}{if $ISBASIC}{literal}
        <div style="color: grey">
        <div class="custom-tables ar-clearfix">
            <label for="enable-audit-tables">{{t.addAuditTables}} <linkToProVersion /></label>
            <input id="enable-audit-tables" type='checkbox' disabled> ({{t.addAuditTablesInfo}})
        </div>
        <div class="custom-tables ar-clearfix">
            <label for="enable-custom-tables">{{t.addCustomTables}} <linkToProVersion /></label>
            <input id="enable-custom-tables" type='checkbox' disabled>
        </div>
        <div class="custom-tables ar-clearfix">
            <label for="use-hidden-groups">{{t.useHiddenGroups}} <linkToProVersion /></label>
            <input id="use-hidden-groups" type='checkbox' disabled> ({{t.useHiddenGroupsInfo}})
        </div>
        <div class="custom-tables ar-clearfix">
            <label for="module-id-grouping">{{t.moduleIdGroupingSummary}} <linkToProVersion /></label>
            <input id="module-id-grouping" type='checkbox' disabled> ({{t.moduleIdGroupingSummaryInfo}})
        </div>
        <div class="custom-tables ar-clearfix">
            <label for="module-id-grouping">{{t.moduleIdGroupingDetailed}} <linkToProVersion /></label>
            <input id="module-id-grouping" type='checkbox' disabled> ({{t.moduleIdGroupingDetailedInfo}})
        </div>
        </div>
        {/literal}{else}{literal}
        <div class="custom-tables ar-clearfix">
            <label for="enable-audit-tables">{{t.addAuditTables}}</label>
            <input id="enable-audit-tables" type='checkbox' checked='{{customOptions.enableAudit}}'> ({{t.addAuditTablesInfo}})
        </div>
        <div class="custom-tables ar-clearfix">
            <label for="enable-custom-tables">{{t.addCustomTables}}</label>
            <input id="enable-custom-tables" type='checkbox' checked='{{customOptions.enableCustomTables}}'>
        </div>
        <div class="custom-tables ar-clearfix">
            <label for="use-hidden-groups">{{t.useHiddenGroups}}</label>
            <input id="use-hidden-groups" type='checkbox' checked='{{customOptions.useHiddenGroups}}'> ({{t.useHiddenGroupsInfo}})
        </div>
        <div class="custom-tables ar-clearfix">
            <label for="module-id-grouping">{{t.moduleIdGroupingSummary}}</label>
            <input id="module-id-grouping" type='checkbox' checked='{{customOptions.moduleIdGroupingSummary}}'> ({{t.moduleIdGroupingSummaryInfo}})
        </div>
        <div class="custom-tables ar-clearfix">
            <label for="module-id-grouping">{{t.moduleIdGroupingDetailed}}</label>
            <input id="module-id-grouping" type='checkbox' checked='{{customOptions.moduleIdGroupingDetailed}}'> ({{t.moduleIdGroupingDetailedInfo}})
        </div>
        {/literal}{/if}{literal}
    </div>

    <div class="report-builder-module-tree">
        <ul>
            {{#.selectedModules}}
               {{#simple}}
                    <reportBuilderModuleSimple moduleName='{{moduleName}}' children='{{children}}' depth='{{depth+1}}' />
               {{/}}
               {{^simple}}
                    <reportBuilderModule moduleName='{{moduleName}}' children='{{children}}' depth='{{depth+1}}' enableAudit='{{customOptions.enableAudit}}' />
               {{/}}
            {{/.selectedModules}}
        </ul>
    </div>

     <p>
        {{#mode == "create"}}
            <button class="ar-button green" on-click='saveReport'>{{t.label_save}}</button>
        {{/}}
        {{#mode == "update"}}
        {/literal}{if $ISBASIC}{literal}
            <button class="ar-button green" disabled >{{t.updateCurrentReport}} <linkToProVersion /></button>
        {/literal}{else}{literal}
            <button class="ar-button green" on-click='updateCurrentReport'>{{t.updateCurrentReport}}</button>
        {/literal}{/if}{literal}
        {{/}}
        {{# internal.response.statusCode == 1}}
            <span class="status-success">{{internal.response.statusMessage}}</span>
            <a target="_blank" href="{{internal.reportUrl}}{{internal.response.insertedReportId}}">{{t.openReport}}</a>
        {{/}}
        {{# internal.response.statusCode > 1}}
            <span class="status-error">{{internal.response.statusMessage}}</span>
        {{/}}
    </p>


</script>

<script id='reportBuilderModule' type='text/ractive'>
<li>
    <img class="addModule" on-click='addModule' src="modules/{{systemModuleName}}/assets/img/cross_g.png">
    {{#depth > 1}}
        {{#editable != false}}<img class="removeModule" on-click='removeModule' src="modules/{{systemModuleName}}/assets/img/cross.png">{{/}}
        {{#editable == false}}<div class="cross-placeholder"></div>{{/}}
    {{/}}

    <select {{#editable == false}}disabled="disabled"{{/}} value='{{moduleName}}' title='{{moduleName}}::{{#types.moduleTypes}}{{#moduleName == name}}{{title}}{{/}}{{/types.moduleTypes}}'  style="width:auto;">
        {{#types.moduleTypes}}
            <option value='{{name}}' title='{{title}}'>{{title}}</option>
        {{/types.moduleTypes}}
    </select>

    {{#depth > 1}}
        <select value='{{moduleFieldName}}' title='{{moduleFieldName}}::{{#moduleFieldTypesComputed[moduleName]}}{{#moduleFieldName == name}}{{title}}{{/}}{{/moduleFieldTypesComputed[moduleName]}}' style="width:auto;">
            {{#moduleFieldTypesComputed[moduleName]}}
                <option value='{{name}}' disabled="{{disabled}}" title='{{title}}'>{{title}}</option>
            {{/moduleFieldTypesComputed[moduleName]}}
        </select>

        <strong> = </strong>
        <strong>{{parentModuleName}}</strong>
        <select value='{{parentModuleFieldName}}' title='{{parentModuleFieldName}}::{{#moduleFieldTypesComputed[parentModuleName]}}{{#parentModuleFieldName == name}}{{title}}{{/}}{{/moduleFieldTypesComputed[parentModuleName]}}' style="width:auto;">
            {{#moduleFieldTypesComputed[parentModuleName]}}
                <option value='{{name}}' disabled="{{disabled}}" title='{{title}}'>{{title}}</option>
            {{/moduleFieldTypesComputed[parentModuleName]}}
        </select>

        {{# manyToManyTableTypesComputed.length > 0}}
            <select value='{{relation}}' title='{{relation}}' style="width:auto;">
                {{#types.relationTypes}}
                    <option value='{{name}}' disabled="{{disabled}}" title='{{title}}'>{{title}}</option>
                {{/types.relationTypes}}
            </select>
        {{/}}


        {{#relation == 'manyToMany'}}
            <br/>
            <div class="many-to-many-settings">
                <select value='{{manyToMany.tableName}}' title='{{manyToMany.tableName}}' style="width:auto;">
                    {{#manyToManyTableTypesComputed}}
                        <option value='{{tableName}}' disabled="{{disabled}}" title='{{tableName}}'>{{tableName}}</option>
                    {{/}}
                </select>

                <select value='{{manyToMany.subModuleColumnName}}' title='{{manyToMany.subModuleColumnName}}' style="width:auto;">
                    {{#selectedManyToManyTable.columns}}
                        <option value='{{name}}' disabled="{{disabled}}" title='{{title}}'>{{title}}</option>
                    {{/}}
                </select>

                <span class="its-hide-text">
                    <strong> = </strong>
                    <strong>{{parentModuleName}}</strong>
                </span>

                <select value='{{manyToMany.mainModuleColumnName}}' title='{{manyToMany.mainModuleColumnName}}' style="width:auto;">
                    {{#selectedManyToManyTable.columns}}
                        <option value='{{name}}' disabled="{{disabled}}" title='{{title}}'>{{title}}</option>
                    {{/}}
                </select>

                {{#selectedManyToManyTable.relModule == true}}
                    <label>
                        <input type="checkbox" checked='{{manyToMany.roleRequired}}' title="Use relationship role" style="vertical-align:middle"/>
                        Use relationship role
                    </label>

                    {{#manyToMany.roleRequired}}
                    <select value='{{manyToMany.roleIndex}}' title='{{manyToMany.roleIndex}}' style="width:auto;">
                        {{#selectedManyToManyTable.roles:key}}
                            <option value='{{key}}' disabled="{{disabled}}" title='{{key}}'>{{key}}</option>
                        {{/}}
                    </select>
                    <strong> = </strong>
                    <select value='{{manyToMany.roleValue}}' title='{{manyToMany.roleValue}}' style="width:auto;">
                        {{#selectedManyToManyTable.roles[manyToMany.roleIndex].values}}
                            <option value='{{name}}' disabled="{{disabled}}" title='{{title}}'>{{title}}</option>
                        {{/}}
                    </select>

                    <!--<input {{#editable == false}}disabled="disabled"{{/}} value='{{manyToMany.roleValue}}'>-->
                    {{/manyToMany.roleRequired}}
                {{/}}

            </div>
        {{/}}

    {{/}}

</li>


{{#.children}}
 <li>
    <ul>
        <reportBuilderModule
            moduleName='{{moduleName}}'
            moduleFieldName='{{moduleFieldName}}'
            children='{{children}}'
            depth='{{depth+1}}'
            parentModuleName='{{parentModuleName}}'
            parentModuleFieldName='{{parentModuleFieldName}}'
            relation='{{relation}}'
            manyToMany='{{manyToMany || {}}}'
            editable = '{{editable}}'
            enableAudit = '{{enableAudit}}'
        />
    </ul>
</li>
{{/.children}}
</script>

<script id='reportBuilderModuleSimple' type='text/ractive'>
<li>
    <img class="addModule" on-click='addModule' src="modules/{{systemModuleName}}/assets/img/cross_g.png">
    {{#depth > 1}}
        <img class="removeModule" on-click='removeModule' src="modules/{{systemModuleName}}/assets/img/cross.png">
    {{/}}

    {{#depth == 1}}
        <select value='{{moduleName}}' title='{{moduleName}}::{{#types.moduleTypes}}{{#moduleName == name}}{{title}}{{/}}{{/types.moduleTypes}}' style="width:auto;">
            {{#types.moduleTypes}}
                <option value='{{name}}' title='{{title}}'>{{title}}</option>
            {{/types.moduleTypes}}
        </select>
    {{/}}

    {{#depth > 1}}
        <select value='{{selectedChild}}' title='{{availableChildren[selectedChild].name}}::{{availableChildren[selectedChild].title}}' style="width:auto;">
            {{#availableChildren:i}}
                <option value='{{i}}' title='{{title}}'>{{title}}</option>
            {{/availableChildren}}
        </select>


        {{# availableChildren[selectedChild].relationships.length > 1}}
             <select value='{{selectedRelation}}' title='{{selectedRelation}}' style="width:auto;">
                {{ #availableChildren[selectedChild].relationships:i}}
                    <option value='{{i}}' title='{{title}}'>{{title}}</option>
                {{/}}
            </select>
        {{/}}

    {{/}}

</li>


{{#.children}}
 <li>
    <ul>
        <reportBuilderModuleSimple
            moduleName='{{moduleName}}'
            moduleFieldName='{{moduleFieldName}}'
            children='{{children}}'
            depth='{{depth+1}}'
            parentModuleName='{{parentModuleName}}'
            parentModuleFieldName='{{parentModuleFieldName}}'
            relation='{{relation}}'
            manyToMany='{{manyToMany}}'
        />
    </ul>
</li>
{{/.children}}
</script>

<script id='reportBuilderSettingsEditorTemplate' type='text/ractive'>
    <p>
        <button class="ar-button green" on-click='saveReport'>{{t.label_save}}</button>
        {{# internal.response.statusCode == 1}}
            <span class="status-success">{{internal.response.statusMessage}}</span>
            <a target="_blank" href="{{internal.reportUrl}}{{internal.response.insertedReportId}}">{{t.openReport}}</a>
        {{/}}
        {{# internal.response.statusCode == 2}}
            <span class="status-error">{{internal.response.statusMessage}}</span>
        {{/}}
    </p>
    <table>
        <tr>
            <th>Module Name</th>
            <th>Many To Many Table Name</th>
            <th>Applicable modules</th>
            <th>Columns</th>
            <th>Multiple modules in same table</th>
        </tr>

        {{#types.moduleTypes}}
            <tr>
                <td><img class="addTable" on-click='addModule' src="modules/{{systemModuleName}}/assets/img/cross_g.png">{{title}}</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>

            {{#types.manyToManyTypes[name]}}
                <tr>
                    <td><img class="removeTable" on-click='removeModule' src="modules/{{systemModuleName}}/assets/img/cross.png"></td>
                    <td><input placeholder='Many to many table name' value='{{tableName}}'></td>
                    <td>
                        {{#applicableModules:i}}
                            <span>{{applicableModules[i]}} </span>
                        {{/}}

                        <select decorator='select2' value='{{applicableModules}}' multiple >
                            {{#types.moduleTypes}}
                                <option value='{{name}}' disabled="{{disabled}}">{{title}}</option>
                            {{/}}
                        </select>



                    </td>
                    <td>
                        {{#columns}}
                            {{name}}
                        {{/}}
                    </td>
                    <td><input type='checkbox' checked='{{relModule}}'></td>
                </tr>
            {{/}}
        {{/}}
    <table>
</script>

{/literal}