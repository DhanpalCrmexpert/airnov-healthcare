<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * This file is part of package Price Books.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Price Books is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
require_once('custom/VIPriceBooks/VIPriceBooksFunction.php');
class VIPriceBooksGetUpdatedListPrice{
    public function __construct(){
    	$this->getUpdatedListPrice();
    } 
    //Select Price Book Price Value 
    public function getUpdatedListPrice(){
		$priceBooksId = $_REQUEST['id'];
        $productId = $_REQUEST['productId'];
        $quantity = $_REQUEST['quantity'];

        $tablename = 'vi_pricebook_productentry';
        $fieldNames = array("productentry");
        $where = array('pricebooks_id' => $priceBooksId[0]);
        
        //Get Product Entry Data
        $selProductQuery = getPriceBooksRecord($tablename,$fieldNames,$where);
        $selProductRows = $GLOBALS['db']->fetchOne($selProductQuery);
        $productEntrydata = html_entity_decode($selProductRows['productentry']);
        $productEntrydata = json_decode($productEntrydata);
       
        $price = 0;
        
        foreach($productEntrydata as $key=>$productValues){
            foreach($productValues as $key=>$values){
                $productData = (array)$values;
                if($productData['product_id']== $productId){
                    $listPrice = $productData['list_price'];
                    if($productData['price_type'] == "2" || $productData['price_type'] == "3"){
                        if(isset($productData['price_entry'])){
                            foreach($productData['price_entry'] as $key => $productValues){
                                $priceData = (array)$productValues;
                                $fromQuantity = $priceData['from_quantity'];
                                $toQuantity = $priceData['to_quantity'];
                                $differential = $priceData['differential'];

                                if(($quantity >= $fromQuantity) && ($quantity <= $toQuantity)){  
                                    if($productData['price_type'] == '2'){
                                        $differentialPriceVal = $listPrice * $differential / 100;
                                    }else if($productData['price_type'] == '3'){
                                        $differentialPriceVal = $differential;
                                    }
                                    if($productData['percentage_mark'] == '1'){
                                        $listPrice = $listPrice + $differentialPriceVal;
                                    }else{
                                        $listPrice = $listPrice - $differentialPriceVal;
                                    } 
                                    if($productData['round_off'] == "1"){
                                        if(is_float($listPrice)){
                                            $price = round($listPrice);
                                        }else{
                                            $price = $listPrice;
                                        }
                                    }else{
                                        $price = $listPrice;
                                    }
                                }else{
                                    $price = $listPrice;
                                }
                            }//end of foreach
                            echo $price;
                        }
                    }elseif($productData['price_type'] == "1"){
                        $price = $listPrice;
                        echo $price;
                    }//end of elseif
                }//end of if
            }//end of foreach
        }//end of foreach 
    }//end of function
}//end of class
new VIPriceBooksGetUpdatedListPrice();
?>