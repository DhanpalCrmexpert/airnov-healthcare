<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

function export_custom_pdf($type, $records = null, $members = false, $sample=false) {
    global $locale;
    global $beanList;
    global $beanFiles;
    global $current_user;
    global $app_strings;
    global $app_list_strings;
    global $timedate;
    global $mod_strings;
    global $current_language;
    $sampleRecordNum = 5;

    //Array of fields that should not be exported, and are only used for logic
    $remove_from_members = array("ea_deleted", "ear_deleted", "primary_address");
    $focus = 0;

    $bean = $beanList[$type];
    require_once($beanFiles[$bean]);
    $focus = new $bean;
    $searchFields = array();
    $db = DBManagerFactory::getInstance();

    if($records) {
        $records = explode(',', $records);
        $records = "'" . implode("','", $records) . "'";
        $where = "{$focus->table_name}.id in ($records)";
    } elseif (isset($_REQUEST['all']) ) {
        $where = '';
    } else {
        if(!empty($_REQUEST['current_post'])) {
            $ret_array = generateSearchWhere($type, $_REQUEST['current_post']);

            $where = $ret_array['where'];
            $searchFields = $ret_array['searchFields'];
        } else {
            $where = '';
        }
    }
    $order_by = "";
    if($focus->bean_implements('ACL')){
        /* if(!ACLController::checkAccess($focus->module_dir, 'export', true)){
            ACLController::displayNoAccess();
            sugar_die('');
        } */
        if(ACLController::requireOwner($focus->module_dir, 'export')){
            if(!empty($where)){
                $where .= ' AND ';
            }
            $where .= $focus->getOwnerWhere($current_user->id);
        }
		/* BEGIN - SECURITY GROUPS */
    	if(ACLController::requireSecurityGroup($focus->module_dir, 'export') )
    	{
			require_once('modules/SecurityGroups/SecurityGroup.php');
    		global $current_user;
    		$owner_where = $focus->getOwnerWhere($current_user->id);
	    	$group_where = SecurityGroup::getGroupWhere($focus->table_name,$focus->module_dir,$current_user->id);
	    	if(!empty($owner_where)) {
				if(empty($where))
	    		{
	    			$where = " (".  $owner_where." or ".$group_where.")";
	    		} else {
	    			$where .= " AND (".  $owner_where." or ".$group_where.")";
	    		}
			} else {
				if(!empty($where)){
					$where .= ' AND ';
				}
				$where .= $group_where;
			}
    	}
    	/* END - SECURITY GROUPS */

    }
    // Export entire list was broken because the where clause already has "where" in it
    // and when the query is built, it has a "where" as well, so the query was ill-formed.
    // Eliminating the "where" here so that the query can be constructed correctly.
    if($members == true){
           $query = $focus->create_export_members_query($records);
    }else{
        $beginWhere = substr(trim($where), 0, 5);
        if ($beginWhere == "where")
            $where = substr(trim($where), 5, strlen($where));

        $query = $focus->create_export_query($order_by,$where);
    }

    $result = '';
    $populate = false;
    if($sample) {
       $result = $db->limitQuery($query, 0, $sampleRecordNum, true, $app_strings['ERR_EXPORT_TYPE'].$type.": <BR>.".$query);
        if( $focus->_get_num_rows_in_query($query)<1 ){
            $populate = true;
        }
    }
    else {
        $result = $db->query($query, true, $app_strings['ERR_EXPORT_TYPE'].$type.": <BR>.".$query);
    }


    $fields_array = $db->getFieldsArray($result,true);

    //set up the order on the header row
    $fields_array = get_field_order_mapping($focus->module_dir, $fields_array);



    //set up labels to be used for the header row
    $field_labels = array();
    foreach($fields_array as $key=>$dbname){
        //Remove fields that are only used for logic
        if($members && (in_array($dbname, $remove_from_members)))
            continue;
        
        //default to the db name of label does not exist
        $field_labels[$key] = translateForExport($dbname,$focus);
    }
    $user_agent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '';
    if ($locale->getExportCharset() == 'UTF-8' &&
        ! preg_match('/macintosh|mac os x|mac_powerpc/i', $user_agent)) // Bug 60377 - Mac Excel doesn't support UTF-8
    {
        //Bug 55520 - add BOM to the exporting CSV so any symbols are displayed correctly in Excel
        $BOM = "\xEF\xBB\xBF";
        $content = $BOM;
    }
    else
    {
        $content = '';
    }

    // setup the "header" line with proper delimiters
    $content .= "\"".implode("\"".getDelimiter()."\"", array_values($field_labels))."\"\r\n";
    $pre_id = '';

    if($populate){
        //this is a sample request with no data, so create fake datarows
         $content .= returnFakeDataRow($focus,$fields_array,$sampleRecordNum);
    }else{
        $records = array();

        //process retrieved record
    	while($val = $db->fetchByAssoc($result, false)) {

        	if ($members)
			$focus = BeanFactory::getBean($val['related_type']);
		else
		{ // field order mapping is not applied for member-exports, as they include multiple modules
            //order the values in the record array
            $val = get_field_order_mapping($focus->module_dir,$val);
		}

            $new_arr = array();
		if($members){
			if($pre_id == $val['id'])
				continue;
			if($val['ea_deleted']==1 || $val['ear_deleted']==1){
				$val['primary_email_address'] = '';
			}
			unset($val['ea_deleted']);
			unset($val['ear_deleted']);
			unset($val['primary_address']);
		}
		$pre_id = $val['id'];

		foreach ($val as $key => $value)
		{
            //getting content values depending on their types
            $fieldNameMapKey = $fields_array[$key];

            if (isset($focus->field_name_map[$fieldNameMapKey])  && $focus->field_name_map[$fieldNameMapKey]['type'])
            {
                $fieldType = $focus->field_name_map[$fieldNameMapKey]['type'];
                switch ($fieldType)
                {
                    //if our value is a currency field, then apply the users locale
                    case 'currency':
                        require_once('modules/Currencies/Currency.php');
                        $value = currency_format_number($value);
                        break;

                    //if our value is a datetime field, then apply the users locale
                    case 'datetime':
                    case 'datetimecombo':
                        $value = $timedate->to_display_date_time($db->fromConvert($value, 'datetime'));
                        $value = preg_replace('/([pm|PM|am|AM]+)/', ' \1', $value);
                        break;

                    //kbrill Bug #16296
                    case 'date':
                        $value = $timedate->to_display_date($db->fromConvert($value, 'date'), false);
                        break;

                    // Bug 32463 - Properly have multienum field translated into something useful for the client
                    case 'multienum':
			$valueArray = unencodeMultiEnum($value);

                        if (isset($focus->field_name_map[$fields_array[$key]]['options']) && isset($app_list_strings[$focus->field_name_map[$fields_array[$key]]['options']]) )
                        {
                            foreach ($valueArray as $multikey => $multivalue )
                            {
                                if (isset($app_list_strings[$focus->field_name_map[$fields_array[$key]]['options']][$multivalue]) )
                                {
                                    $valueArray[$multikey] = $app_list_strings[$focus->field_name_map[$fields_array[$key]]['options']][$multivalue];
                                }
                            }
                        }
			$value = implode(",",$valueArray);

                        break;

		case 'enum':
			if (	isset($focus->field_name_map[$fields_array[$key]]['options']) && 
				isset($app_list_strings[$focus->field_name_map[$fields_array[$key]]['options']]) &&
				isset($app_list_strings[$focus->field_name_map[$fields_array[$key]]['options']][$value])
			)
				$value = $app_list_strings[$focus->field_name_map[$fields_array[$key]]['options']][$value];

			break;
                }
            }


            // Keep as $key => $value for post-processing
            $new_arr[$key] = preg_replace("/\"/","\"\"", $value);
        }

        // Use Bean ID as key for records
        $records[$pre_id] = $new_arr;
    }

        // Check if we're going to export non-primary emails
        if ($focus->hasEmails() && in_array('email_addresses_non_primary', $fields_array))
        {
            // $records keys are bean ids
            $keys = array_keys($records);

            // Split the ids array into chunks of size 100
            $chunks = array_chunk($keys, 100);

            foreach ($chunks as $chunk)
            {
                // Pick all the non-primary mails for the chunk
                $query =
                    "
                      SELECT eabr.bean_id, ea.email_address
                      FROM email_addr_bean_rel eabr
                      LEFT JOIN email_addresses ea ON ea.id = eabr.email_address_id
                      WHERE eabr.bean_module = '{$focus->module_dir}'
                      AND eabr.primary_address = '0'
                      AND eabr.bean_id IN ('" . implode("', '", $chunk) . "')
                      AND eabr.deleted != '1'
                      ORDER BY eabr.bean_id, eabr.reply_to_address, eabr.primary_address DESC
                    ";

                $result = $db->query($query, true, $app_strings['ERR_EXPORT_TYPE'] . $type . ": <BR>." . $query);

                while ($val = $db->fetchByAssoc($result, false)) {
                    if (empty($records[$val['bean_id']]['email_addresses_non_primary'])) {
                        $records[$val['bean_id']]['email_addresses_non_primary'] = $val['email_address'];
                    } else {
                        // No custom non-primary mail delimeter yet, use semi-colon
                        $records[$val['bean_id']]['email_addresses_non_primary'] .= ';' . $val['email_address'];
                    }
                }
            }
        }
	}
	$return_data = array();
	$return_data['labels'] = $field_labels;
	$return_data['records'] = $records;
	return $return_data;

}
?>
