<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once('include/MVC/View/views/view.detail.php');

class LeadsViewDetail extends ViewDetail {		
	function display() 
	{	
		global $current_user,$db;
		if(CheckIsManager() || $current_user->is_admin=='1'){}
		else
			echo '<style type="text/css">#convert_lead_button {display:none;}</style>';
		parent::display();
	}
}
?>