<?php
// created: 2020-11-19 07:31:39
$mod_strings = array (
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Date Created',
  'LBL_DATE_MODIFIED' => 'Date Modified',
  'LBL_MODIFIED' => 'Modified By',
  'LBL_MODIFIED_NAME' => 'Modified By Name',
  'LBL_CREATED' => 'Created By',
  'LBL_DESCRIPTION' => 'Description',
  'LBL_DELETED' => 'Deleted',
  'LBL_NAME' => 'Name',
  'LBL_CREATED_USER' => 'Created by User',
  'LBL_MODIFIED_USER' => 'Modified by User',
  'LBL_LIST_NAME' => 'Name',
  'LBL_EDIT_BUTTON' => 'Edit',
  'LBL_REMOVE' => 'Remove',
  'LBL_ASCENDING' => 'Ascending',
  'LBL_DESCENDING' => 'Descending',
  'LBL_OPT_IN' => 'Opt In',
  'LBL_OPT_IN_PENDING_EMAIL_NOT_SENT' => 'Pending Confirm opt in, Confirm opt in not sent',
  'LBL_OPT_IN_PENDING_EMAIL_SENT' => 'Pending Confirm opt in, Confirm opt in sent',
  'LBL_OPT_IN_CONFIRMED' => 'Opted in',
  'LBL_ASSIGNED_TO_ID' => 'Assigned User Id',
  'LBL_ASSIGNED_TO_NAME' => 'Assigned to',
  'LBL_MODIFIED_ID' => 'Modified By Id',
  'LBL_CREATED_ID' => 'Created By Id',
  'LBL_LIST_FORM_TITLE' => 'Timesheet Pro Reporting List',
  'LBL_MODULE_NAME' => 'Timesheet Pro Reporting',
  'LBL_MODULE_TITLE' => 'Timesheet Pro Reporting',
  'LBL_HOMEPAGE_TITLE' => 'My Timesheet Pro Reporting',
  'LNK_NEW_RECORD' => 'Create Timesheet Pro Reporting',
  'LNK_LIST' => 'View Timesheet Pro Reporting',
  'LNK_IMPORT_SP_TIMESHEET_TRACKER' => 'Import Timesheet Pro Reporting',
  'LBL_SEARCH_FORM_TITLE' => 'Search Timesheet Pro Reporting',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'View History',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activities',
  'LBL_SP_TIMESHEET_TRACKER_SUBPANEL_TITLE' => 'Timesheet Pro Reporting',
  'LBL_NEW_FORM_TITLE' => 'New Timesheet Pro Reporting',
  'LBL_TRACK_DATE' => 'Date',
  'LBL_HOURS' => 'Hours',
  'LBL_PROJECT_PROJECT_ID' => 'Project (related Project ID)',
  'LBL_PROJECT' => 'Project',
  'LBL_PROJECT_TASKS_PROJECTTASK_ID' => 'Project Tasks (related Project Task ID)',
  'LBL_PROJECT_TASKS' => 'Project Tasks',
  'LBL_TRACK_USER_USER_ID' => 'User (related User ID)',
  'LBL_TRACK_USER' => 'User',
  'LBL_PARENT_NAME' => 'Related Module',
  'LBL_PARENT_TYPE' => 'Parent',
  'LBL_RELATED_RECORD_URL' => 'Related Record',
  'LBL_EXPORT' => 'Export to CSV',
  'LBL_EXPORT_PDF' => 'Export to PDF',
  'LBL_CREATE_INVOICE' => 'Create Invoice',
  'LBL_RELATED_MODULE' => 'Related Module',
);