<?php
// Do not store anything in this file that is not part of the array or the hook version.  This file will	
// be automatically rebuilt in the future. 
 $hook_version = 1; 
$hook_array = Array(); 
// position, file, function 
$hook_array['before_save'] = Array(); 
$hook_array['before_save'][] = Array(99, 'update timesheet', 'custom/modules/sp_Auto_TimeSheet_Time_Tracker/Hooks/update_timesheet.php','UpdateTimesheet', 'update_timesheet_method'); 

$hook_array['after_save'] = Array(); 
$hook_array['after_save'][] = Array(101, 'redirect', 'custom/modules/sp_Auto_TimeSheet_Time_Tracker/Hooks/redirectCls.php','redirectCls', 'redirectFunc'); 

//$hook_array['before_save'][] = Array(121, 'update accounts total hours', 'custom/modules/sp_Auto_TimeSheet_Time_Tracker/Hooks/redirectCls.php','redirectCls', 'updAccountHoursFunc'); 
?>