<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.

 * SuiteCRM is an extension to SugarCRM Community Edition developed by Salesagility Ltd.
 * Copyright (C) 2011 - 2014 Salesagility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for  technical reasons, the Appropriate Legal Notices must
 * display the words  "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 ********************************************************************************/

/*********************************************************************************

 * Description:  TODO: To be written.
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/
require_once('modules/sp_Timesheet/sp_Timesheet.php');

global $current_user, $db, $sugar_config;

$_POST = getRealPOST();
$custom_project = array();
foreach ($_POST as $key => $value) {
	if(stripos($key, 'customproject_')!==false){
		$custom_project[$key] = $value;
		unset($_POST[$key]);
	}
}		
$week_start = $_POST['week_start'];
$week_end = $_POST['week_end'];
$hours = $_POST['hours'];
if(isset($_POST["current_user_id"]) && !empty($_POST["current_user_id"])){
	$user_id = $_POST["current_user_id"];
	$current_user_id = $_POST["current_user_id"];
}
else{
	$user_id = $current_user->id;
	$current_user_id = $current_user->id;
}

$total_hours = 0;

if(!empty($_POST['record'])){
	$selected_module = $_POST['selected_module'];
	$related_module = $_POST['related_module'];
	$related_module_relationship = $_POST['related_module_relationship'];
}
else{
	$selected_module = $sugar_config['timesheet_module_'.$current_user_id];
	$related_module = $sugar_config['related_module_'.$current_user_id];
	$related_module_relationship = $sugar_config['related_module_relationship_'.$current_user_id];
}
$sp_Timesheet = new sp_Timesheet();

$sp_Timesheet->retrieve_by_string_fields(array(
		'user_id_c' => $user_id,
		'track_date_from' => $week_start,
		'track_date_to'=>$week_end,
		'module'=>$selected_module,
		'related_module'=>$related_module,
));

$sp_Timesheet->name = 'TimeSheet: '.$week_start.' - '.$week_end;
$sp_Timesheet->track_date_from = $week_start;
$sp_Timesheet->track_date_to = $week_end;
$sp_Timesheet->user_id_c = $user_id;
$sp_Timesheet->hours = $total_hours;
$sp_Timesheet->module = $selected_module;
$sp_Timesheet->related_module = $related_module;
$sp_Timesheet->related_module_relationship = $related_module_relationship;
$sp_Timesheet->assigned_user_id = $current_user_id;
$sp_Timesheet->save();
$timesheet_id = $sp_Timesheet->id;
// 
$sp_Timesheet->handleSave($total_hours, $timesheet_id, $current_user_id);

$sp_Timesheet->hours = $total_hours;
$sp_Timesheet->id = $timesheet_id;
$sp_Timesheet->save();

foreach ($custom_project as $key => $value) {
	$key_parts = explode('_', $key);
	if(empty($value['0']) || empty($key_parts['1'])){
		continue;
	}
	$sql_res = $db->query("SELECT id from sp_timesheet_tracker where id='".$key_parts['1']."' AND track_date='".$key_parts['3']."'", true);
	$sql_row = $db->fetchByAssoc($sql_res);
	if(isset($sql_row['id']) && !empty($sql_row['id'])){
		$stt_res = $db->query("UPDATE sp_timesheet_tracker set hours=".$value['0']." where id = '".$sql_row['id']."'", true);
		$astt_res = $db->query("UPDATE sp_auto_timesheet_time_tracker set hours=".$value['0']." where id = '".$key_parts['2']."'", true);
	}else{
		$timesheet_tracker = BeanFactory::getBean('sp_timesheet_tracker');
		$timesheet_tracker->id = create_guid();
		$timesheet_tracker->new_with_id = true;
		$timesheet_tracker->name = "Timesheet Tracker";
		$timesheet_tracker->track_date = $key_parts['3'];
		$timesheet_tracker->user_id_c = $current_user->id;
		$timesheet_tracker->description = $value['1'];
		$timesheet_tracker->timesheet_id = $timesheet_id;
		$timesheet_tracker_id = $timesheet_tracker->save();

		$auto_timesheet_tracker = BeanFactory::getBean('sp_Auto_TimeSheet_Time_Tracker', $key_parts['2']);
		$auto_timesheet_tracker->manual_entry = true;
		$auto_timesheet_tracker->hours = $value['0'];
		$auto_timesheet_tracker->processed = false;
		$auto_timesheet_tracker->description = $value['1'];
		$auto_timesheet_tracker->sp_timesheet_tracker_id_c = $timesheet_tracker_id;
		$auto_timesheet_tracker->save();
	}
}

header("Location: index.php?module=sp_Timesheet&action=DetailView&record=".$timesheet_id);
exit;



//Function to fix up PHP's messing up POST input containing dots, etc.
function getRealPOST() {
	$pairs = explode("&", file_get_contents("php://input"));
    $vars = array();
    foreach ($pairs as $pair) {
		$nv = explode("=", $pair);
		$real_name = $nv[0];
		$name = urldecode($nv[0]);
		
		if( strpos( $real_name, "%5B%5D" ) !== false ) {
			$name = substr($name, 0, -2);
			$real_name_count = count($vars[$name]);
			$vars[$name][$real_name_count] = urldecode($nv[1]);
		}
		else{
			 $value = urldecode($nv[1]);
			 $vars[$name] = $value;
		}
	}
    return $vars;
}
?>