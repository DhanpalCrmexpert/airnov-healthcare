<?php
$mod_strings['LBL_CALCULATE_AUTO_TRACK_TIMINGS'] = 'TimeSheet: Auto Track Timings';
$mod_strings['LBL_SEND_EMAIL_FOR_AUTO_TRACK_TIMINGS'] = 'TimeSheet: Send Email Notifications for Auto Track Timings';
$mod_strings['LBL_NO_ACTIVITY_FOR_PROJECT_TASKS'] = 'TimeSheet: No Activity For Project Tasks';
