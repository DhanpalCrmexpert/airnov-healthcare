<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.

 * SuiteCRM is an extension to SugarCRM Community Edition developed by Salesagility Ltd.
 * Copyright (C) 2011 - 2014 Salesagility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for  technical reasons, the Appropriate Legal Notices must
 * display the words  "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 ********************************************************************************/

/*********************************************************************************

 * Description:  TODO: To be written.
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/
require_once('modules/sp_Timeoff_TimeCard/sp_Timeoff_TimeCard.php');

global $current_user, $db, $sugar_config;

$_POST = getRealPOST();
$custom_project = array();



$week_start = $_POST['week_start'];
$week_end = $_POST['week_end'];
$hours = $_POST['hours'];
if(isset($_POST["current_user_id"]) && !empty($_POST["current_user_id"])){
	$user_id = $_POST["current_user_id"];
	$current_user_id = $_POST["current_user_id"];
}
else{
	$user_id = $current_user->id;
	$current_user_id = $current_user->id;
}

$total_hours = 0;

$sp_Timeoff_TimeCard = new sp_Timeoff_TimeCard();

$sp_Timeoff_TimeCard->retrieve_by_string_fields(array(
		'user_id_c' => $user_id,
		'track_date_from' => $week_start,
		'track_date_to'=>$week_end,
		
));
$dstart=0;
/* print_r($sp_Timeoff_TimeCard);
die;
 */
$sp_Timeoff_TimeCard->name = 'Timecard: '.$week_start.' - '.$week_end;
$sp_Timeoff_TimeCard->track_date_from = $week_start;
$sp_Timeoff_TimeCard->track_date_to = $week_end;
$sp_Timeoff_TimeCard->user_id_c = $user_id;
$sp_Timeoff_TimeCard->hours = $total_hours;

$sp_Timeoff_TimeCard->related_module_relationship = $related_module_relationship;
$sp_Timeoff_TimeCard->assigned_user_id = $current_user_id;
$sp_Timeoff_TimeCard->save();
 $sp_Timeoff_TimeCard_id = $sp_Timeoff_TimeCard->id;
// 



$total=0;

for($i=0;$i<7;$i++)
{
		$timecard=array();
		$dstart=$week_start;
		for($j=0;$j<7;$j++)
		{
		
			  $field="time_".$i."_".$dstart.'_'.$j;
			$m=0;
			
			
			
			
			 $timecard[$j]['hours']='';
			 $timecard[$j]['track_date']='';
			 if(!empty($_POST[$field][0]))
				$timecard[$j]['hours']=$_POST[$field][0];
			 else
				$timecard[$j]['hours']=0;
			
			 $timecard[$j]['track_date']=$_POST[$field][1];
			
			$total+=$timecard[$j]['hours'];
			
			$dstart=date('Y-m-d', strtotime('+1 day', strtotime($dstart)));
		}
		
	if(empty($_POST['timecard_tracker_id'][$i]))
	{
		if(!empty($_POST['timecard_type['.$i.']'])){
					
			echo	 $sql_tracker="INSERT INTO `sp_timecard_tracker` (`id`,  `timecard_id`,  `position`,  `name`,  `date_entered`,  
			`date_modified`,  `modified_user_id`,  `created_by`,   `deleted`,  `assigned_user_id`,  `track_date0`,
			  `hours0`,  `track_date1`,  `hours1`,  `track_date2`,  `hours2`,  `track_date3`,  `hours3`,  `track_date4`,  `hours4`,  
			  `track_date5`,  `hours5`,  `track_date6`,  `hours6`,    `user_id_c`) VALUES
			 (UUID(),'".$sp_Timeoff_TimeCard_id."','".$i."', '".$_POST['timecard_type['.$i.']']."',NOW(),NOW(),'".$current_user_id."','".$current_user_id."',    
			 
			 0,    '".$current_user_id."',  '".  $timecard[0]['track_date']."',  '".  $timecard[0]['hours']."',    '".  $timecard[1]['track_date']."', 
			 
			 '".  $timecard[1]['hours']."', '".  $timecard[2]['track_date']."',   '".  $timecard[2]['hours']."',     '".  $timecard[3]['track_date']."', 
			 
			 '".  $timecard[3]['hours']."', '".  $timecard[4]['track_date']."',  '".  $timecard[4]['hours']."'  ,     '".  $timecard[5]['track_date']."',  
			 
			 '".  $timecard[5]['hours']."'  ,  '".  $timecard[6]['track_date']."',    '".  $timecard[6]['hours']."',     '".$current_user_id."' )";
				

				$db->query($sql_tracker);
		}
	}
	else{
			echo	 $sql_tracker="UPDATE  `sp_timecard_tracker` SET  `timecard_id`='".$sp_Timeoff_TimeCard_id."',  `position`='".$i."',  `name`='".$_POST['timecard_type['.$i.']']."', `date_modified`= NOW(),  `modified_user_id`='".$current_user_id."',  `deleted`=0,  `assigned_user_id`='".$current_user_id."',  `track_date0` = '".  $timecard[0]['track_date']."', `hours0`= '".  $timecard[0]['hours']."',  `track_date1`= '".  $timecard[1]['track_date']."',  `hours1`= '".  $timecard[1]['hours']."',  `track_date2`='".  $timecard[2]['track_date']."',  `hours2`= '".  $timecard[2]['hours']."',  `track_date3`='".  $timecard[3]['track_date']."',  `hours3`= '".  $timecard[3]['hours']."',  `track_date4`='".  $timecard[4]['track_date']."',  `hours4`= '".  $timecard[4]['hours']."',  `track_date5`=  '".  $timecard[5]['track_date']."',  `hours5`= '".  $timecard[5]['hours']."',  `track_date6`= '".  $timecard[6]['track_date']."',  `hours6`= '".  $timecard[6]['hours']."',`user_id_c`= '".$current_user_id."' WHERE `id` = '".$_POST['timecard_tracker_id'][$i]."' ";
				

				$db->query($sql_tracker);
		//echo $_POST['timecard_tracker_id'][$i];die;
	}
		
}

 $sql_timecard="UPDATE sp_timeoff_timecard SET  `hours` = '".$total."' WHERE `id` = '".$sp_Timeoff_TimeCard_id."'" ;
$db->query($sql_timecard);
/* $sp_Timeoff_TimeCard->handleSave($total_hours, $timesheet_id, $current_user_id);

$sp_Timeoff_TimeCard->hours = $total_hours;
$sp_Timeoff_TimeCard->id = $timesheet_id;
$sp_Timeoff_TimeCard->save(); */
header("Location: index.php?module=sp_Timeoff_TimeCard&action=DetailView&record=".$sp_Timeoff_TimeCard_id);
exit;



/* foreach ($custom_project as $key => $value) {
	$key_parts = explode('_', $key);
	if(empty($value['0']) || empty($key_parts['1'])){
		continue;
	}
	$sql_res = $db->query("SELECT id from sp_timesheet_tracker where id='".$key_parts['1']."' AND track_date='".$key_parts['3']."'", true);
	$sql_row = $db->fetchByAssoc($sql_res);
	if(isset($sql_row['id']) && !empty($sql_row['id'])){
		$stt_res = $db->query("UPDATE sp_timesheet_tracker set hours=".$value['0']." where id = '".$sql_row['id']."'", true);
		$astt_res = $db->query("UPDATE sp_auto_timesheet_time_tracker set hours=".$value['0']." where id = '".$key_parts['2']."'", true);
	}else{
		$timesheet_tracker = BeanFactory::getBean('sp_timesheet_tracker');
		$timesheet_tracker->id = create_guid();
		$timesheet_tracker->new_with_id = true;
		$timesheet_tracker->name = "Timesheet Tracker";
		$timesheet_tracker->track_date = $key_parts['3'];
		$timesheet_tracker->user_id_c = $current_user->id;
		$timesheet_tracker->description = $value['1'];
		$timesheet_tracker->timesheet_id = $timesheet_id;
		$timesheet_tracker_id = $timesheet_tracker->save();

		$auto_timesheet_tracker = BeanFactory::getBean('sp_Auto_TimeSheet_Time_Tracker', $key_parts['2']);
		$auto_timesheet_tracker->manual_entry = true;
		$auto_timesheet_tracker->hours = $value['0'];
		$auto_timesheet_tracker->processed = false;
		$auto_timesheet_tracker->description = $value['1'];
		$auto_timesheet_tracker->sp_timesheet_tracker_id_c = $timesheet_tracker_id;
		$auto_timesheet_tracker->save();
	}
}

header("Location: index.php?module=sp_Timesheet&action=DetailView&record=".$timesheet_id);
exit;

*/

//Function to fix up PHP's messing up POST input containing dots, etc.
function getRealPOST() {
	$pairs = explode("&", file_get_contents("php://input"));
    $vars = array();
    foreach ($pairs as $pair) {
		$nv = explode("=", $pair);
		$real_name = $nv[0];
		$name = urldecode($nv[0]);
		
		if( strpos( $real_name, "%5B%5D" ) !== false ) {
			$name = substr($name, 0, -2);
			$real_name_count = count($vars[$name]);
			$vars[$name][$real_name_count] = urldecode($nv[1]);
		}
		else{
			 $value = urldecode($nv[1]);
			 $vars[$name] = $value;
		}
	}
    return $vars;
} 
?>