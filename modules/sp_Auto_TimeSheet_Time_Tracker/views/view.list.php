<?php
require_once('include/MVC/View/views/view.list.php');

class sp_Auto_TimeSheet_Time_TrackerViewList extends ViewList
{  
	public function __construct()
 	{
        parent::__construct();
	}
	public function display()
    {
		echo "</br>";
		global $current_user,$sugar_config;

		//////////////////
		global  $sugar_config;
		$siteUrl = $sugar_config['site_url'];
		require_once('modules/TimesheetNinja/license/OutfittersLicense.php');
		$validate_license = OutfittersLicense::isValid('TimesheetNinja');
		if($validate_license !== true) {
		if(is_admin($current_user)) {
			SugarApplication::appendErrorMessage('Timesheet is no longer active due to the following reason: '.$validate_license.' Users will have limited to no access until the issue has been addressed.');
		}
		echo '<p class="error">Timesheet is no longer active Please renew your subscription or check your license configuration.Will be redirect in seconds.....</p>';
		header("refresh:2;url=$siteUrl");
		//functionality may be altered here in response to the key failing to validate
		}
		echo "</br>";
		
		
		//////////////////
		
		
		
		parent::display();
	}
}
?>