<?php
$dictionary['User']['fields']['hourly_rate'] =  array (
      'name' => 'hourly_rate',
      'vname' => 'LBL_HOURLY_RATE',
      'type' => 'currency',
      'len' => '26,6',
      'merge_filter' => 'disabled',
      'audited' => 1,
	  'default' => '0',
    );
 ?>