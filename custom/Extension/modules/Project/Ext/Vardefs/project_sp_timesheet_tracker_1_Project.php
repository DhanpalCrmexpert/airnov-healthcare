<?php
// created: 2016-09-26 21:04:57
$dictionary["Project"]["fields"]["project_sp_timesheet_tracker_1"] = array (
  'name' => 'project_sp_timesheet_tracker_1',
  'type' => 'link',
  'relationship' => 'project_sp_timesheet_tracker_1',
  'source' => 'non-db',
  'module' => 'sp_timesheet_tracker',
  'bean_name' => 'sp_timesheet_tracker',
  'side' => 'right',
  'vname' => 'LBL_PROJECT_SP_TIMESHEET_TRACKER_1_FROM_SP_TIMESHEET_TRACKER_TITLE',
);
