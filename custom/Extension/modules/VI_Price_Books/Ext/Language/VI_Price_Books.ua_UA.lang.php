<?php
/*********************************************************************************
 * This file is part of package Price Books.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Price Books is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
$mod_strings['LBL_ACTIVE'] = 'Активний';
$mod_strings['LBL_PERCENTAGE_MARK'] = 'Відсоток відмітки';
$mod_strings['LBL_TYPE'] = 'Тип ціни';
$mod_strings['LBL_ROUND_OFF'] = 'Завершити до';
$mod_strings['LBL_ACCOUNT_NAME'] = "Ім'я облікового запису";
$mod_strings['LBL_ACCOUNT_ID'] = 'Ідентифікатор рахунку';
$mod_strings['LBL_ACCOUNTS'] = 'Рахунки';
$mod_strings['LBL_CONTACT_NAME'] = 'Ім’я контакту';
$mod_strings['LBL_CONTACT_ID'] = 'Ідентифікатор контакту';
$mod_strings['LBL_CONTACTS'] = 'Контакти';
$mod_strings['LBL_FLAT_PRICE'] = 'Ціна квартири';
$mod_strings['LBL_DIFFERENTIAL_PERCENTAGE'] = 'Диференціальний по (%)';
$mod_strings['LBL_DIFFERENTIAL_DOLLAR'] = 'Диференціальний за ($)';
$mod_strings['LBL_PRICE_BOOKS'] = 'Книги з цінами';
$mod_strings['LBL_NAME'] = "Ім'я";
$mod_strings['LBL_PRICE_TYPE'] = 'Тип ціни';
$mod_strings['LBL_ASSIGNED_TO'] = 'Призначений';
$mod_strings['LBL_NO_RECORDS'] = 'Записів не знайдено';
$mod_strings['LBL_MARK_UP'] = 'Відмітка (Додати)';
$mod_strings['LBL_MARK_DOWN'] = 'Позначити (Sub)';
$mod_strings['LBL_ADD_QUANTITY'] = 'Додати кількість';
$mod_strings['LBL_PRODUCT_NAME'] = 'Назва продукту';
$mod_strings['LBL_PRODUCT_PRICE'] = 'Ціна продукту';
$mod_strings['LBL_LIST_PRICE'] = 'Перелік цін';
$mod_strings['LBL_ADD_PRODUCT'] = 'Додати продукт';
$mod_strings['LBL_YES'] = 'Так';
$mod_strings['LBL_NO'] = 'Ні';
$mod_strings['LBL_SELECT_PRODUCT_MSG'] = "Будь ласка, виберіть Продукт перед натисканням на Книги цін !!!";
$mod_strings['LBL_SELECT_PRICEBOOK_MSG'] = "Виберіть лише одну прайс-книгу.";
$mod_strings['LBL_PRODUCT_VALIDATION'] = 'Виберіть інший товар. Цей продукт вже вибраний. ';
$mod_strings['LBL_QUANTITY_VALID_MSG'] = "Недійсне значення:";
$mod_strings['LBL_FROM_QUANTITY'] = "Від кількості";
$mod_strings['LBL_TO_QUANTITY'] = "До кількості";
$mod_strings['LBL_DIFFERENTIAL'] = "Диференціальний";
$mod_strings['LBL_PRICE_BOOKS_ACCESS'] = "Будь ласка, увімкніть функцію цінних книг для доступу до модуля цін книг.";
$mod_strings['LBL_CLICK_HERE'] = "Натисніть тут";
$mod_strings['LBL_ENABLE_LABEL'] = "для ввімкнення цього.";
$mod_strings['LBL_FROM_QUANTITY_VALID'] = "Будь ласка, введіть більше від значення кількості, ніж попереднє до значення кількості.";
$mod_strings['LBL_TO_QUANTITY_VALID'] = "Будь ласка, введіть величину більше, ніж від кількості.";
$mod_strings['LBL_PRODUCT_LIST_PRICE_VALID'] = "Будь ласка, додайте продукт та перелік цін на додавання кількості.";
$mod_strings['LBL_PRODCUT_PRICE_VALID'] = "Будь ласка, додайте ціну продукту до продукту.";
$mod_strings['LBL_LIST_PRICE_VALID'] = "Будь ласка, додайте перелік цін на товар.";
$mod_strings['LBL_QUANTITY_VALID_RANGE'] = "Будь ласка, додайте діапазон кількості для продукту.";
$mod_strings['LBL_PRODUCT_VALID'] = "Будь ласка, додайте продукт для додавання кількості.";
$mod_strings['LBL_PRODUCT_FIELDS_VALIDATION'] = "Будь ласка, додайте назву продукту та перелікову ціну для цінних книг.";
$mod_strings['LBL_QUANTITY_FIELD_VALIDATION'] = "Будь ласка, заповніть діапазон кількості для порожніх плит.";
$mod_strings['LBL_EDITVIEW_PANEL1'] = "Деталі цінних книг";
$mod_strings['LBL_DETAILVIEW_PANEL1'] = "Деталі цінних книг";
?> 