<?php
/*********************************************************************************
 * This file is part of package Price Books.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Price Books is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 *****************************a***************************************************/
$mod_strings['LBL_PRICE_BOOKS'] = "Catálogo de preços";
$mod_strings['LBL_PRICE_BOOKS_DESCRIPTION'] = "Gerenciar livros de preços de produtos para contatos / contas específicos.";
$mod_strings['LBL_ENABLE_PRICE_BOOKS'] = "Ativar recurso de catálogos de preços";
$mod_strings['LBL_PRICE_BOOKS_ACTIVATED'] = "Recurso de catálogos de preços ativado com sucesso.";
$mod_strings['LBL_PRICE_BOOKS_DEACTIVATED'] = "Recurso de catálogos de preços desativado com sucesso.";
$mod_strings['LBL_CLICK_HERE'] = "Clique aqui";
$mod_strings['LBL_MANAGE_PRICEBOOK'] = "Para gerenciar os catálogos de preços";
//license
$mod_strings['LBL_LICENCE_ACTIVE_LABEL'] = "VIPriceBooksLicenseAddon não está mais ativo devido ao seguinte motivo:";
$mod_strings['LBL_LICENCE_ISSUES'] = "Os usuários não terão acesso limitado até que o problema seja solucionado";
$mod_strings['LBL_CLICK_HERE'] = "Clique aqui";
$mod_strings['LBL_LICENCE_ACTIVE'] = "VIPriceBooksLicenseAddon não está mais ativo";
$mod_strings['LBL_RENEW_LICENCE'] = "Por favor, renove sua assinatura ou verifique sua configuração de licença.";
$mod_strings['LBL_UPDATE_LICENSE'] = "Atualizar licença";
?>