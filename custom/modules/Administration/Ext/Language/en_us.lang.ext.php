<?php 
 //WARNING: The contents of this file are auto-generated



$mod_strings['LBL_DTSENDGRID'] = 'SendGrid Configuration';
$mod_strings['LBL_DTSENDGRID_LICENSE_TITLE'] = 'License Configuration';
$mod_strings['LBL_DTSENDGRID_TITLE'] = 'License Configuration';
$mod_strings['LBL_DTSENDGRID_LICENSE'] = 'Manage and configure the license for this add-on';

$mod_strings['LBL_DTSENDGRID_CONFIGURATION'] = 'SendGrid Settings';
$mod_strings['LBL_DTSENDGRID_MESSAGE'] = 'Configure SendGrid API Keys';




$mod_strings['LBL_SAMPLELICENSEADDON'] = 'Timesheet Ninja Enterprise Monthly';
$mod_strings['LBL_SAMPLELICENSEADDON_LICENSE_TITLE'] = 'License Configuration';
$mod_strings['LBL_SAMPLELICENSEADDON_LICENSE'] = 'Manage and configure the license for this add-on';



$mod_strings['LBL_OUTLOOK_ADDIN_CONFIG_TITLE'] = 'Outlook Plugin Configuration';
$mod_strings['LBL_OUTLOOK_ADDIN'] = 'Configuration Settings for SuiteCRM Outlook Plugin';
$mod_strings['LBL_OUTLOOK_ADDIN_TITLE'] = 'SuiteCRM Outlook Plugin';
$mod_strings['LBL_OUTLOOK_ADDIN_TOOLS_DESC'] = 'Manage functionality and configuration for the Outlook Plugin.';
$mod_strings['LBL_OUTLOOK_ADDIN_DOWNLOAD_MANIFEST'] = 'Download Manifest';
$mod_strings['LBL_MICROSOFT_GRAPH_SETTINGS'] = 'Microsoft Graph Settings';
$mod_strings['LBL_MICROSOFT_GRAPH_TENANT_ID'] = 'Tenant ID';
$mod_strings['LBL_MICROSOFT_GRAPH_CLIENT_ID'] = 'Client ID';
$mod_strings['LBL_MICROSOFT_GRAPH_CLIENT_SECRET'] = 'Client Secret';
$mod_strings['LBL_OUTLOOK_ADDIN_LICENSE_HEADING'] = 'License Settings';
$mod_strings['LBL_OUTLOOK_ADDIN_LICENSE_KEY'] = 'Outlook Plugin License Key';
$mod_strings['LBL_OUTLOOK_ADDIN_BTN_VALIDATE_KEY'] = 'Validate';
$mod_strings['LBL_OUTLOOK_ADDIN_BTN_UPDATE_LICENSED'] = 'Update';
$mod_strings['LBL_OUTLOOK_ADDIN_SEARCH_SCHEDULERS'] = 'Schedulers';
$mod_strings['LBL_OUTLOOK_ADDIN_SEARCH_SCHEDULERS_HELP'] = 'The following Outlook Plugin schedulers were detected:';
$mod_strings['LBL_OUTLOOK_ADDIN_SEARCH_SCHEDULERS_LAST_RUN'] = 'last run:';
$mod_strings['LBL_OUTLOOK_ADDIN_SEARCH_SCHEDULERS_NEVER_RUN'] = 'This job has never run';
$mod_strings['LBL_OUTLOOK_ADDIN_SEARCH_SCHEDULERS_NOT_FOUND'] = 'No schedulers found. Consider creating one.';
$mod_strings['LBL_OUTLOOK_ADDIN_SEARCH_SCHEDULERS_DESC'] = 'The Outlook Plugin uses schedulers to synchronise data in the background.';
$mod_strings['LBL_OUTLOOK_ADDIN_SETTINGS'] = 'Outlook Add-in Settings';
$mod_strings['LBL_OUTLOOK_ADDIN_MODULES_SELECT'] = 'Select Modules for Manual Archive:';



$mod_strings['LBL_ANALYTICREPORTING'] = 'Analytic Reporting';
$mod_strings['LBL_ANALYTICREPORTING_LICENSE_TITLE'] = 'License Configuration';
$mod_strings['LBL_ANALYTICREPORTING_LICENSE'] = 'Manage and configure the license for Analytic Reporting';


/*********************************************************************************
 * This file is part of package Price Books.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Price Books is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
$mod_strings['LBL_PRICE_BOOKS'] = "Price Books";
$mod_strings['LBL_PRICE_BOOKS_DESCRIPTION'] = "Manage Price Books of Products for particular Contacts/Accounts.";
$mod_strings['LBL_ENABLE_PRICE_BOOKS'] = "Enable Price Books Feature";
$mod_strings['LBL_PRICE_BOOKS_ACTIVATED'] = "Price Books Feature Activated Successfully.";
$mod_strings['LBL_PRICE_BOOKS_DEACTIVATED']= "Price Books Feature Deactivated Successfully.";
$mod_strings['LBL_CLICK_HERE'] = "Click Here";
$mod_strings['LBL_MANAGE_PRICEBOOK'] = "To Manage Price Books";
//license
$mod_strings['LBL_LICENCE_ACTIVE_LABEL'] = "VIPriceBooksLicenseAddon is no longer active due to the following reason:";
$mod_strings['LBL_LICENCE_ISSUES'] = "Users will have limited to no access until the issue has been addressed";
$mod_strings['LBL_CLICK_HERE'] = "Click Here";
$mod_strings['LBL_LICENCE_ACTIVE'] = "VIPriceBooksLicenseAddon is no longer active";
$mod_strings['LBL_RENEW_LICENCE'] = "Please renew your subscription or check your license configuration.";
$mod_strings['LBL_UPDATE_LICENSE'] = "Update License";



$mod_strings['LBL_LINK_KANBAN_CONFIG_NAME'] = 'Kanban Config';
$mod_strings['LBL_LINK_KANBAN_CONFIG_DESCRIPTION'] = 'You can config kanban view for each modules';
$mod_strings['LBL_KANBAN_CONFIG_SECTION_HEADER'] = 'Kanban';
$mod_strings['LBL_KANBAN_CONFIG_SECTION_DESCRIPTION'] = 'Config Kanban View';

$mod_strings['LBL_KB_CHOOSE_MODULE'] = 'Choose Module';
$mod_strings['LBL_KB_CHOOSE_FIELD'] = 'Choose Field to View';
$mod_strings['LBL_KB_CHOOSE_OPTIONS'] = 'Choose Options to View';


$mod_strings['LBL_GMSYNCADDON'] = 'GrinMark Synchronizer Add-on';
$mod_strings['LBL_GMSYNCADDON_LICENSE_TITLE'] = 'License Configuration';
$mod_strings['LBL_GMSYNCADDON_LICENSE'] = 'Manage and configure the license for Outlook 365 AddOn';

?>