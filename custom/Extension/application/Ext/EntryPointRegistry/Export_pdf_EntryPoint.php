<?php 
	$entry_point_registry['export_pdf'] = array(
	    'file' => 'modules/sp_timesheet_tracker/export_pdf.php',
	    'auth' => false
	);
	
	$entry_point_registry['create_invoice'] = array(
	    'file' => 'modules/sp_timesheet_tracker/create_invoice.php',
	    'auth' => false
	);