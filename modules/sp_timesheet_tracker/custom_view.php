<?php

if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

if(isset($_GET['search']) && $_GET['search']){
	$table_html = get_html($_GET['date'],$_GET['user_id']);
	echo $table_html;
	exit;
}
if(!isset($_GET['popup'])){
	echo '<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>';
}
global $current_user;
$userBean = BeanFactory::getBean('Users');
$users = $userBean->get_full_list();

$user_options = "<option value='#'></option>";
foreach ($users as $key => $value) {
	if($value->id === $current_user->id){
		$user_options .= "<option value='{$value->id}' selected>{$value->full_name}</option>";
	}
	else{
		$user_options .= "<option value='{$value->id}'>{$value->full_name}</option>";
	}
}

$today = Date('Y-m-d',strtotime('today'));
$table_html = get_html($today,$current_user->id);

$start = Date('Y-m-d', strtotime('monday this week', $date_str));
$end = Date('Y-m-d', strtotime('sunday this week', $date_str));

$html = "<div class='main-body'>
			<div class='filter-panel'>
				<fieldset>
					<legend>Filters:</legend>
					<select name='user_list' id='user_list'>
						{$user_options}
					</select>
					<input type='date' class='date' name='date' id='date' value='{$today}' />
					<input type='button' name='search' id='search' value='Search'/>
				</fieldset>
			</div>
			<div class='data-panel'>
				<fieldset>
					<legend>Details:</legend>
					<div class='table-responsive'>
					<table class='data-table table' id='data-table'>
						{$table_html}
					</table></div>
				</fieldset>
			</div>
		 </div>";
echo $html;

?>
<style type="text/css">

	::-webkit-datetime-edit { padding: 1em; }
	::-webkit-datetime-edit-fields-wrapper { background: silver; }
	::-webkit-datetime-edit-text { color: red; padding: 0 0.3em; }
	::-webkit-inner-spin-button { display: none; }
	::-webkit-calendar-picker-indicator { background: #a5e8d6; }

	.data-table.table .thead-light th {
	    color: #495057;
	    background-color: #e9ecef;
	    border-color: #000000;
	}
	.data-table.table th, .data-table.table td {
		text-align: left;
	    padding: 0.75rem;
	    vertical-align: top;
	    border: 1px solid #000000 !important;
	}
	.date{
		border: 1px solid #a5e8d6;
	    border-radius: 5px;
	    min-height: 20px;
	}
</style>
<script type="text/javascript">
	$(document).ready(function(){
		$("body").on('click', "#search" ,function(){
			var date = $('#date').val();
			var user_id = $('#user_list').val();
			$.ajax({
				url:'./index.php?entryPoint=timesheet_custom_view&search=true',
				data:{'date':date, 'user_id':user_id},
				type:'GET',
				success:function(req_response){
					$("#data-table").html('');
					$("#data-table").html(req_response);
				}
			});
		});
	});

</script>

<?php
function get_html($date, $user_id){
	global $db;
	$user = BeanFactory::getBean('Users', $user_id);
	$user_id = $user->id;
	$date_str = strtotime($date);

	$start = Date('Y-m-d', strtotime('monday this week', $date_str));
	$end = Date('Y-m-d', strtotime('sunday this week', $date_str));
	$sql = "select track_date date, sum(hours) hours from sp_timesheet_tracker 
			where user_id_c = '{$user_id}'
			and track_date BETWEEN '{$start}' AND '{$end}'
			group by track_date
			order by track_date";

	$week_hours = array();
	while (strtotime($start) <= strtotime($end)) {
		$week_hours[$start] = array('name' => Date('D', strtotime($start))."</br>".Date('M d', strtotime($start)), 'hours'=>0);
		$start = Date('Y-m-d', strtotime('+1 day', strtotime($start)));
	}

	$result = $db->query($sql, true);
	while ($row = $db->fetchByAssoc($result)) {
		$week_hours[$row['date']]['hours'] = $row['hours'];
	}

	$header_html = "<thead class='thead-light'>
						<tr><th width='20%'>User Name</th>";
	$body_html = "<tbody>
						<tr><td>{$user->full_name}</td>";
	$total_hours = 0.0;
	foreach($week_hours as $day){
		$header_html .= "<th width='11%'>{$day['name']}</th>";
		$body_html .= "<td>{$day['hours']}</td>";
		$total_hours += number_format($day['hours'], 2);
	}

	$header_html .= "<th width='11%'>Total</th></tr></thead>";
	$body_html .= "<td>{$total_hours}</td></tr></tbody>";

	$html = $header_html.$body_html;

	return $html;
}
?>