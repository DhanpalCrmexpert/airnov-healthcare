<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

class sp_TimesheetLogicHook {


	function afterDelete(&$bean, $event, $arguments) {
		global $db;
		
		$timesheet_id = $bean->id;
		if(!empty($timesheet_id)){
			$query = "DELETE from sp_timecard_tracker where timecard_id = '$timesheet_id'";
			$db->query($query);
		}
	}
}
