<?php

class OpportunitiesLogicHooks
{
    
    function afterSave($bean, $event, $arguments)
    {       
        $this->calculateAccountPotentialImpact($bean);
        $this->calculateAccountSuccessRate($bean);
        $bean->ignore_update_c = true;
    }
    
    function beforeSave($bean, $event, $arguments)
    {
        $bean->stored_fetched_row_c = $bean->fetched_row;
    }

    function calculateAccountSuccessRate($bean)
    {
        if(($bean->stored_fetched_row_c['sales_stage'] != $bean->sales_stage) && $bean->sales_stage == 'Closed Won' && (!isset($bean->ignore_update_c) || $bean->ignore_update_c === false)){
            if(empty($bean->account_id)){
                return;
            }
            $account = BeanFactory::getBean('Accounts', $bean->account_id);
            $account->load_relationship('opportunities');
            $opportunities = $account->opportunities->getBeans();
            if(count($opportunities) == 0){
                return;
            }
            $account->success_rate_c=0;
            $total_won = 0;
            $total_opp = count($opportunities);
            foreach($opportunities as $opp){
                if($opp->sales_stage == 'Closed Won'){
                    $total_won++;
                }
            }
            if($total_won > 0){
                $account->success_rate_c = ($total_won/$total_opp)*100;
            } 
            $account->save();           
        } 
    }
    
    function calculateAccountPotentialImpact($bean)
    {
        if(($bean->stored_fetched_row_c['amount'] != $bean->amount || $bean->stored_fetched_row_c['date_closed'] != $bean->date_closed) && (!isset($bean->ignore_update_c) || $bean->ignore_update_c === false)){
            if(empty($bean->account_id)){
                return;
            }
            $account = BeanFactory::getBean('Accounts', $bean->account_id);
            $account->load_relationship('opportunities');
            $opportunities = $account->opportunities->getBeans();
            $account->potential_impact_usd_budget_nineteen_c=0;
            $account->potential_impact_usd_budget_twenty_c=0;
            $account->potential_impact_usd_budget_twentyone_c=0;
    
            foreach($opportunities as $opp){
                $closeDate = strtotime($opp->date_closed);
				$current_year=date("Y");
				$next_year=$current_year+1;
				$next_tonext__year=$current_year+2;
				if(date("Y", $closeDate) == $current_year){
                    $account->potential_impact_usd_budget_nineteen_c += $opp->amount;
                }
                else if(date("Y", $closeDate) == $next_year){
                    $account->potential_impact_usd_budget_twenty_c += $opp->amount;
                }
                else if(date("Y", $closeDate) == $next_tonext__year){
                    $account->potential_impact_usd_budget_twentyone_c += $opp->amount;
                }      
                /*if(date("Y", $closeDate) == '2019'){
                    $account->potential_impact_usd_budget_nineteen_c += $opp->amount;
                }
                else if(date("Y", $closeDate) == '2020'){
                    $account->potential_impact_usd_budget_twenty_c += $opp->amount;
                }
                else if(date("Y", $closeDate) == '2021'){
                    $account->potential_impact_usd_budget_twentyone_c += $opp->amount;
                }*/
            }
            $account->save();
        }       
    }    
}
