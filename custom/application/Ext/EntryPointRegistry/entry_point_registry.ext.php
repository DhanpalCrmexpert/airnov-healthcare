<?php 
 //WARNING: The contents of this file are auto-generated

 
	$entry_point_registry['timesheet_custom_view'] = array(
	    'file' => 'modules/sp_timesheet_tracker/custom_view.php',
	    'auth' => true
	);


/**
 * Copyright (c) SalesAgility Ltd.  All Rights Reserved.
 * Licensed under the GPL3 License.  See License in the project root
 * for license information.
 *
 * @package Outlook Addin for SuiteCRM
 * @copyright 2020 SalesAgility Ltd http://www.salesagility.com
 * @author SalesAgility <info@salesagility.com
 *
 */

$entry_point_registry['downloadOutlookManifest'] = [
    'file' => 'custom/include/SA_Outlook/admin/entrypoint/DownloadOutlookManifest.php',
    'auth' => true
];
$entry_point_registry['updateLicensedOutlookUsers'] = [
    'file' => 'custom/include/SA_Outlook/SuiteStore/entrypoint/UpdateLicensedOutlookUsers.php',
    'auth' => true
];
$entry_point_registry['validateOutlookKey'] = [
    'file' => 'custom/include/SA_Outlook/SuiteStore/entrypoint/ValidateOutlookKey.php',
    'auth' => true
];
$entry_point_registry['outlookAddinModules'] = [
    'file' => 'custom/include/SA_Outlook/admin/entrypoint/OutlookAddinModules.php',
    'auth' => false
];


	/*********************************************************************************
 * This file is part of package Price Books.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Price Books is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
	$entry_point_registry['VIPriceBooksConfigEnable'] = array(
        'file' => 'custom/VIPriceBooks/VIPriceBooksConfigEnable.php',
        'auth' => true
    );
    
	$entry_point_registry['VIPriceBooksAddProducts'] = array(
        'file' => 'custom/VIPriceBooks/VIPriceBooksAddProducts.php',
        'auth' => true
    );
	
	$entry_point_registry['VIPriceBooksGetData'] = array(
        'file' => 'custom/VIPriceBooks/VIPriceBooksGetData.php',
        'auth' => true
    );
	
	$entry_point_registry['VIPriceBooksGetUpdatedListPrice'] = array(
        'file' => 'custom/VIPriceBooks/VIPriceBooksGetUpdatedListPrice.php',
        'auth' => true
    );
	
    $entry_point_registry['VIPriceBooksGetProductPrice'] = array(
        'file' => 'custom/VIPriceBooks/VIPriceBooksGetProductPrice.php',
        'auth' => true
    );



$entry_point_registry['sendgrid_event'] = array(
    'file' => 'custom/modules/sg_SendGrid_Event/Ext/EntryPoint/SendGrid_Event.php',
    'auth' => false,
);




$entry_point_registry['downloadTemplate'] = array(
    'file' => 'modules/AnalyticReporting/entryPoints/downloadTemplate.php',
    'auth' => true
);
 
	$entry_point_registry['export_pdf'] = array(
	    'file' => 'modules/sp_timesheet_tracker/export_pdf.php',
	    'auth' => false
	);
	
	$entry_point_registry['create_invoice'] = array(
	    'file' => 'modules/sp_timesheet_tracker/create_invoice.php',
	    'auth' => false
	);

    $entry_point_registry['analyticReportWidget'] = array(
        'file' => 'modules/AnalyticReporting/entryPoints/widget.php',
        'auth' => false
    );

?>