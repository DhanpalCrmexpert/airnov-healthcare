<?php

class SendGridEmail extends Email
{
	public function send(
	SugarPHPMailer $mail = null,
        NonGmailSentFolderHandler $nonGmailSentFolder = null,
        InboundEmail $ie = null,
        Email $tempEmail = null,
        $check_notify = false,
        $options = "\\Seen")
	{
		global $mod_strings, $app_strings;
        global $current_user;
        global $sugar_config;
        global $locale;
        $this->clearTempEmailAtSend();
                
        //sendgrid send this id in custom code
        $sendgrid_uuid = $this->id;

        $OBCharset = $locale->getPrecedentPreference('default_email_charset');
        $mail = $mail ? $mail : new SugarPHPMailer();

        if (!$this->to_addrs_arr) {
            LoggerManager::getLogger()->error('"To" address(es) is not set or empty to sending email.');
            return false; // return false as error, to-address is required to sending an email
        }
        //sendgrid for saving to email address
        $to_mail_string="";
        $i=1;        
        foreach ((array)$this->to_addrs_arr as $addr_arr) {
            if($i==1)
            {
                $to_mail_string .= "{\"email\":\"{$addr_arr['email']}\"}";
            }
            else
            {
                $to_mail_string .= ",{\"email\":\"{$addr_arr['email']}\"}";
            } 
            $i++;
            if (empty($addr_arr['display'])) {
                if (!isset($addr_arr['email']) || !$addr_arr['email']) {
                    LoggerManager::getLogger()->error('"To" email address is missing!');
                } else {
                    $mail->AddAddress($addr_arr['email'], "");
                }
            } else {
                $mail->AddAddress(
                    $addr_arr['email'],
                    $locale->translateCharsetMIME(trim($addr_arr['display']), 'UTF-8', $OBCharset)
                );
            }
        }

        if (!$this->cc_addrs_arr) {
            LoggerManager::getLogger()->warn('"CC" address(es) is not set or empty to sending email.');
        }
        //sendgrid for saving cc email address
        $cc_mail_string="";
        $j=1;    
        foreach ((array)$this->cc_addrs_arr as $addr_arr) {
            if($j==1)
            {
                $cc_mail_string .= "{\"email\":\"{$addr_arr['email']}\"}";
            }
            else
            {
                $cc_mail_string .= ",{\"email\":\"{$addr_arr['email']}\"}";
            } 
            $j++;
            if (empty($addr_arr['display'])) {
                $mail->AddCC($addr_arr['email'], "");
            } else {
                $mail->AddCC(
                    $addr_arr['email'],
                    $locale->translateCharsetMIME(trim($addr_arr['display']), 'UTF-8', $OBCharset)
                );
            }
        }
        
        if (!$this->bcc_addrs_arr) {
            LoggerManager::getLogger()->warn('"BCC" address(es) is not set or empty to sending email.');
        }
        //sendgrid for saving cc email address
        $bcc_mail_string="{\"email\":\"{$current_user->email1}\"}";
        foreach ((array)$this->bcc_addrs_arr as $addr_arr) {            
            $bcc_mail_string .= ",{\"email\":\"{$addr_arr['email']}\"}";
            if (empty($addr_arr['display'])) {
                $mail->AddBCC($addr_arr['email'], "");
            } else {
                $mail->AddBCC(
                    $addr_arr['email'],
                    $locale->translateCharsetMIME(trim($addr_arr['display']), 'UTF-8', $OBCharset)
                );
            }
        }
        
        $ieId = $this->mailbox_id;
        $mail = $this->setMailer($mail, '', $ieId);

        if (($mail->oe->type === 'system') && (!isset($sugar_config['email_allow_send_as_user']) || (!$sugar_config['email_allow_send_as_user']))) {
            $mail->From =
            $sender =
            $ReplyToAddr = $mail->oe->smtp_from_addr;
            isValidEmailAddress($mail->From);
            $ReplyToName = $mail->oe->smtp_from_name;
        } else {

            // FROM ADDRESS
            if (!empty($this->from_addr)) {
                $mail->From = $this->from_addr;
                isValidEmailAddress($mail->From);
            } else {
                $mail->From = $current_user->getPreference('mail_fromaddress');
                isValidEmailAddress($mail->From);
                $this->from_addr = $mail->From;
                isValidEmailAddress($this->from_addr);
            }
            // FROM NAME
            if (!empty($this->from_name)) {
                $mail->FromName = $this->from_name;
            } elseif (!empty($this->from_addr_name)) {
                $mail->FromName = $this->from_addr_name;
            } else {
                $mail->FromName = $current_user->getPreference('mail_fromname');
                $this->from_name = $mail->FromName;
            }

            //Reply to information for case create and autoreply.
            if (!empty($this->reply_to_name)) {
                $ReplyToName = $this->reply_to_name;
            } else {
                $ReplyToName = $mail->FromName;
            }

            $sender = $mail->From;
            isValidEmailAddress($sender);
            if (!empty($this->reply_to_addr)) {
                $ReplyToAddr = $this->reply_to_addr;
            } else {
                $ReplyToAddr = $mail->From;
            }
            isValidEmailAddress($ReplyToAddr);
        }

        $mail->Sender = $sender; /* set Return-Path field in header to reduce spam score in emails sent via Sugar's Email module */
        isValidEmailAddress($mail->Sender);
        $mail->AddReplyTo($ReplyToAddr, $locale->translateCharsetMIME(trim($ReplyToName), 'UTF-8', $OBCharset));

        //$mail->Subject = html_entity_decode($this->name, ENT_QUOTES, 'UTF-8');
        $mail->Subject = $this->name;

        ///////////////////////////////////////////////////////////////////////
        ////	ATTACHMENTS
        if (isset($this->saved_attachments)) {
            $sendgrid_attachment_array = array();
            foreach ($this->saved_attachments as $note) {
                $mime_type = 'text/plain';
                if ($note->object_name == 'Note') {
                    if (!empty($note->file->temp_file_location) && is_file($note->file->temp_file_location)) { // brandy-new file upload/attachment
                        $file_location = "file://" . $note->file->temp_file_location;
                        $filename = $note->file->original_file_name;
                        $mime_type = $note->file->mime_type;
                    } else { // attachment coming from template/forward
                        $file_location = "upload://{$note->id}";
                        // cn: bug 9723 - documents from EmailTemplates sent with Doc Name, not file name.
                        $filename = !empty($note->filename) ? $note->filename : $note->name;
                        $mime_type = $note->file_mime_type;
                    }
                } elseif ($note->object_name == 'DocumentRevision') { // from Documents
                    $filePathName = $note->id;
                    // cn: bug 9723 - Emails with documents send GUID instead of Doc name
                    $filename = $note->getDocumentRevisionNameForDisplay();
                    $file_location = "upload://$note->id";
                    $mime_type = $note->file_mime_type;
                }

                // strip out the "Email attachment label if exists
                $filename = str_replace($mod_strings['LBL_EMAIL_ATTACHMENT'] . ': ', '', $filename);
                $file_ext = pathinfo($filename, PATHINFO_EXTENSION);
                //is attachment in our list of bad files extensions?  If so, append .txt to file location
                //check to see if this is a file with extension located in "badext"
                foreach ($sugar_config['upload_badext'] as $badExt) {
                    if (strtolower($file_ext) == strtolower($badExt)) {
                        //if found, then append with .txt to filename and break out of lookup
                        //this will make sure that the file goes out with right extension, but is stored
                        //as a text in db.
                        $file_location = $file_location . ".txt";
                        break; // no need to look for more
                    }
                }
                $mail->AddAttachment(
                    $file_location,
                    $locale->translateCharsetMIME(trim($filename), 'UTF-8', $OBCharset),
                    'base64',
                    $mime_type
                );

                // embedded Images
                if ($note->embed_flag == true) {
                    $cid = $filename;
                    $mail->AddEmbeddedImage($file_location, $cid, $filename, 'base64', $mime_type);
                }
                $sendgrid_attachment_array[$filename]['filename'] = $filename;
                $sendgrid_attachment_array[$filename]['file_location'] = $file_location;
                $sendgrid_attachment_array[$filename]['mime_type'] = $mime_type;                
            }
        } else {
            LoggerManager::getLogger()->fatal('Attachements not found');
        }
        ////	END ATTACHMENTS
        ///////////////////////////////////////////////////////////////////////

        $mail = $this->handleBody($mail);

        $GLOBALS['log']->debug('Email sending --------------------- ');

        ///////////////////////////////////////////////////////////////////////
        ////	I18N TRANSLATION
        $mail->prepForOutbound();
        ////	END I18N TRANSLATION
        ///////////////////////////////////////////////////////////////////////
        
        $validator = new EmailFromValidator();
        if (!$validator->isValid($this)) { 
            
            // if an email is invalid before sending, 
            // maybe at this point sould "return false;" because the email having 
            // invalid from address and/or name but we will trying to send it..
            // and we should log the problem at least:
            
            // (needs EmailFromValidation and EmailFromFixer.. everywhere where from name and/or from email address get a value)
            
            $errors = $validator->getErrorsAsText();
            $details = "Details:\n{$errors['messages']}\ncodes:{$errors['codes']}\n{$mail->ErrorInfo}";
            LoggerManager::getLogger()->error("Invalid email from address or name detected before sending. $details");
        }        

        if($current_user->SendGrid == 0)
        {            
            $mail_send = $mail->send();
        }
        else
        {            
            // Generated by curl-to-PHP: http://incarnate.github.io/curl-to-php/
            require_once('modules/sg_SendGrid_Event/license/DT_SendGrid_OutfittersLicense.php');
            $validate = DT_SendGrid_OutfittersLicense::isValid('sg_SendGrid_Event');
            $validate = 1;
            if ($validate != 1) {
                $GLOBALS['log']->fatal("************* Error in License Validation of SendGrid ************");
                return false;
            }
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, 'https://api.sendgrid.com/v3/mail/send');
            curl_setopt($ch, CURLOPT_HEADER, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);            

            //changes required: need to change dt_msg_id,type

            $sendgrid_body_content = explode(PHP_EOL,str_replace("\"", "'", $mail->Body));
            
            $body_line = 0;
            foreach ($sendgrid_body_content as $single_body_line)
            {     
                $single_body_line = trim($single_body_line);           
                if($body_line == 0)
                {                    
                    $sendgrid_body = ($mail->ContentType=="text/plain") ? "{$single_body_line}<br>" : "{$single_body_line}";
                }
                else
                {                    
                    $sendgrid_body .= ($mail->ContentType=="text/plain") ? "{$single_body_line}<br>" : "{$single_body_line}";
                }
                $body_line++;
            }                        

            $param = "\"personalizations\":[{\"to\":[{$to_mail_string}],\"subject\":\"{$mail->Subject}\"";
            if($cc_mail_string)
            {
                $param .= ",\"cc\":[{$cc_mail_string}]";
            }

            if($bcc_mail_string)
            {
                $param .= ",\"bcc\":[{$bcc_mail_string}]";   
            }
            $param .="}]";

            $param .=",\"from\":{\"email\":\"{$mail->From}\",\"name\":\"{$mail->FromName}\"},\"reply_to\":{\"email\":\"{$ReplyToAddr}\"}";

            if($sendgrid_attachment_array)
            {
                $param .= ",\"attachments\":[";
                $upload_file_count = 1;
                foreach ($sendgrid_attachment_array as $sendgrid_attachment_array_key => $sendgrid_attachment_array_value) {
                    //changes required: need to add file location other than upload
                    $upload_dir_file_path = $sugar_config['upload_dir'].end(explode("/", $sendgrid_attachment_array_value['file_location']));
                                        
                    $attachment = base64_encode(file_get_contents($upload_dir_file_path));
                    $filename = $sendgrid_attachment_array_value['filename'];
                    $sendgrid_mime_type = $sendgrid_attachment_array_value['mime_type'];
                    //attachment end
                    if($upload_file_count == 1)
                    {
                        $param .= "{\"content\":\"{$attachment}\",\"filename\":\"{$filename}\"}";
                    }
                    else
                    {
                        $param .= ",{\"content\":\"{$attachment}\",\"filename\":\"{$filename}\"}";   
                    }
                    $upload_file_count++;                    
                }
                $param .= "]";
            }            
            $site_url = $sugar_config['site_url'];
            $param .=",\"content\": [{\"type\": \"text/html\", \"value\": \"{$sendgrid_body}\"}],\"custom_args\":{\"dt_msg_id\":\"{$sendgrid_uuid}\",\"site_url\":\"{$site_url}\"}";

            curl_setopt($ch, CURLOPT_POSTFIELDS, "{{$param}}");

            curl_setopt($ch, CURLOPT_POST, 1);

            $sendgrid_admin = new Administration();
            $settings_sendgrid = $sendgrid_admin->retrieveSettings('sendgrid_config');
            $sendgrid_api_key = $settings_sendgrid->settings['sendgrid_config_sendgrid_api_key'];

            $headers = array();
            //changes required: need to get token from config_override or config table
            $headers[] = 'Authorization: Bearer '.$sendgrid_api_key.'';
            $headers[] = 'Content-Type: application/json';
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

            $result = curl_exec($ch);

            if (strpos($result, '202 Accepted') == false) {
                $GLOBALS['log']->fatal("curl_error===========".print_r(curl_error($ch)));                
                return false;
            }            

            $mail_send = $result;
            curl_close ($ch);            
        }

        if ($mail_send) {
            ///////////////////////////////////////////////////////////////////
            ////	INBOUND EMAIL HANDLING
            // mark replied            
            if (!empty($_REQUEST['inbound_email_id'])) {
                $ieId = $_REQUEST['inbound_email_id'];
                $this->createTempEmailAtSend($tempEmail);
                $this->getTempEmailAtSend()->status = 'replied';
                $ie = $ie ? $ie : new InboundEmail();
                $nonGmailSentFolder = $nonGmailSentFolder ? $nonGmailSentFolder : new NonGmailSentFolderHandler();
                if (!$ieMailId = $this->getTempEmailAtSend()->saveAndStoreInSentFolderIfNoGmail($ie, $ieId, $mail, $nonGmailSentFolder, $check_notify, $options)) {
                    LoggerManager::getLogger()->debug('IE Mail ID is ' . ($ieMailId === null ? 'null' : $ieMailId) . ' after save and store in non-gmail sent folder.');
                }
                if (!$this->getTempEmailAtSend()->save()) {
                    LoggerManager::getLogger()->warn('Email saving error: after save and store in non-gmail sent folder.');
                }
            }
            $GLOBALS['log']->debug(' --------------------- buh bye -- sent successful');
            ////	END INBOUND EMAIL HANDLING
            ///////////////////////////////////////////////////////////////////
            return true;
        }
        $GLOBALS['log']->debug($app_strings['LBL_EMAIL_ERROR_PREPEND'] . $mail->ErrorInfo);

        return false;
	}
}