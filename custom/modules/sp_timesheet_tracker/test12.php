<?php

global $db;
$where = " WHERE project.deleted=0 AND sp_timesheet_tracker.deleted=0 and users.deleted=0 AND  users.status='Active' ";	
	$sql = "SELECT 
						    project.name AS name,CONCAT(COALESCE(users.first_name, ''),' ',COALESCE(users.last_name, '')) AS user_name,
						    SUM(sp_timesheet_tracker.hours) AS total_hours
						FROM
						    sp_timesheet_tracker
						        JOIN
						    project ON sp_timesheet_tracker.project_id_c = project.id
							JOIN users on sp_timesheet_tracker.user_id_c = users.id
						{$where}
						GROUP BY project.name,sp_timesheet_tracker.user_id_c";
						
$sql_result = $db->query($sql, true);
$projArr = array();
while ($row = $db->fetchByAssoc($sql_result)) {
	$projArr[$row['name']][] = array('user_name'=>$row['user_name'],total_hours=>$row['total_hours']);
}
$returnArr = array();
foreach($projArr as $project_name=>$emp_data){
	$returnArr[] = array(
	'project_name'=>$project_name,
	'user_list' => $emp_data,
	);
}
echo json_encode($returnArr);
	