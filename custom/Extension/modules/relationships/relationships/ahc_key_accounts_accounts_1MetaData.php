<?php
// created: 2020-11-03 16:17:29
$dictionary["ahc_key_accounts_accounts_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'ahc_key_accounts_accounts_1' => 
    array (
      'lhs_module' => 'ahc_Key_Accounts',
      'lhs_table' => 'ahc_key_accounts',
      'lhs_key' => 'id',
      'rhs_module' => 'Accounts',
      'rhs_table' => 'accounts',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'ahc_key_accounts_accounts_1_c',
      'join_key_lhs' => 'ahc_key_accounts_accounts_1ahc_key_accounts_ida',
      'join_key_rhs' => 'ahc_key_accounts_accounts_1accounts_idb',
    ),
  ),
  'table' => 'ahc_key_accounts_accounts_1_c',
  'fields' => 
  array (
    0 => 
    array (
      'name' => 'id',
      'type' => 'varchar',
      'len' => 36,
    ),
    1 => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    2 => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'len' => '1',
      'default' => '0',
      'required' => true,
    ),
    3 => 
    array (
      'name' => 'ahc_key_accounts_accounts_1ahc_key_accounts_ida',
      'type' => 'varchar',
      'len' => 36,
    ),
    4 => 
    array (
      'name' => 'ahc_key_accounts_accounts_1accounts_idb',
      'type' => 'varchar',
      'len' => 36,
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'ahc_key_accounts_accounts_1spk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'ahc_key_accounts_accounts_1_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'ahc_key_accounts_accounts_1ahc_key_accounts_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'ahc_key_accounts_accounts_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'ahc_key_accounts_accounts_1accounts_idb',
      ),
    ),
  ),
);