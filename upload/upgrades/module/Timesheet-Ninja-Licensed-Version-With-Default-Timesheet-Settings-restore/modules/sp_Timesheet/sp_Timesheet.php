<?php
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.

 * SuiteCRM is an extension to SugarCRM Community Edition developed by Salesagility Ltd.
 * Copyright (C) 2011 - 2014 Salesagility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for  technical reasons, the Appropriate Legal Notices must
 * display the words  "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 ********************************************************************************/

/**
 * THIS CLASS IS FOR DEVELOPERS TO MAKE CUSTOMIZATIONS IN
 */
require_once('modules/sp_Timesheet/sp_Timesheet_sugar.php');
class sp_Timesheet extends sp_Timesheet_sugar {
	
	function __construct(){
		parent::__construct();
	}
	
	function handleSave(&$total_hours, $timesheet_id, $user_id){
		global $current_user, $db;
		
		// echo "<pre>";
		// print_r($_POST);
		// exit;
		$week_start = $_POST['week_start'];
		$week_end = $_POST['week_end'];
		// $user_id = $current_user->id;
		
		$autoTrackkedIds = $this->getAutoTrackedRelatedIDs($timesheet_id);
		
		$autoTrackkedIdsstr = '';
		if(!empty($autoTrackkedIds)){
			$autoTrackkedIdsstr = implode("', '", $autoTrackkedIds );
		}
		
		// START -- handle project timesheet relationship 
		$select_deletion_query = "select id, related_record_url, projecttask_id_c from sp_timesheet_tracker where track_date >= '$week_start' and deleted=0 and track_date <= '$week_end' and user_id_c = '$user_id' and timesheet_id = '$timesheet_id' and id not in ('$autoTrackkedIdsstr')";
		$rs_sel_pt = $db->query($select_deletion_query);
		while($row_pt = $db->fetchByAssoc($rs_sel_pt)){
			if($this->validateStatus($row_pt)){
				$pt_deletion_ids[] = $row_pt['id'];
			}
		}
		// echo "<pre>";
		// print_r($pt_deletion_ids);
		// exit;
		$ptDeletionIDSstr = '';
		if(!empty($pt_deletion_ids)){
			$ptDeletionIDSstr = implode("', '", $pt_deletion_ids );
		}
		

		
		
		$delete_pt_records = "delete from project_sp_timesheet_tracker_1_c where project_sp_timesheet_tracker_1sp_timesheet_tracker_idb in ('$ptDeletionIDSstr') and deleted = 0";
		$db->query($delete_pt_records);
		
		// END -- handle project timesheet relationship
		
		
		// delete account timesheet records
		$delete_at_records = "delete from accounts_sp_timesheet_tracker_1_c where accounts_sp_timesheet_tracker_1sp_timesheet_tracker_idb in ('$ptDeletionIDSstr') and deleted = 0";
		$db->query($delete_at_records);
		
		$project_time_arr = array();
		$account_time_arr = array();	

		foreach($_POST as $key=>$val){
			$key_arr = explode("_",$key);
			if(count($key_arr) < 3){
				continue;
			}
			else{
				if($key_arr[0] == "readonly"){
					$project_hours = $val[0];
					$total_hours = $total_hours + $project_hours;
					continue;
				}
				if($key_arr[0] != "project" && $key_arr[0] != "projecttask"){
					continue;
				}
				if(count($key_arr) == 3){
					
					
					$project_id  = $key_arr[1];
					$project_task_id  = '';
					$project_track_date  = $val[2];
					$project_description  = $val[1];
					if($project_description==$project_track_date){
					
					
					  $qry_find = "select id,description from sp_timesheet_tracker where (deleted=0 or soft_deleted=1) and track_date='".$project_track_date."' and parent_id='".$project_id."' and parent_type='Project' and user_id_c = '$user_id' order by date_entered desc";
				
		            $qry_find1 = $db->query($qry_find);
					if($qry_find1->num_rows>0){
						$res_find = $db->fetchByAssoc($qry_find1);
						if(!empty($res_find['description'])){
						$project_description = $res_find['description'];
						}
					}
		
				}
					$project_hours = $val[0];
				}
				else if(count($key_arr) == 4){
					$project_id  = $key_arr[1];
					$project_task_id  = $key_arr[2];
					$project_track_date  = $val[2];
					$project_description  = $val[1];
					echo $project_track_date;
					echo '<br>';
					echo $project_description;
				
					if($project_description==$project_track_date){
						
					
					   $qry_find = "select id,description from sp_timesheet_tracker where (deleted=0 or soft_deleted=1) and track_date='".$project_track_date."' and parent_id='".$project_id."' and projecttask_id_c='".$project_task_id."' and parent_type='Project' and user_id_c = '$user_id' order by date_entered desc";
					
					 
		            $qry_find1 = $db->query($qry_find);
					if($qry_find1->num_rows>0){
						$res_find = $db->fetchByAssoc($qry_find1);
						if(!empty($res_find['description'])){
							
						$project_description = $res_find['description'];
						echo 'hiiiiiiiiiiiiiiiiiii'.$project_description;
						}
					}
				}
			
					$project_hours = $val[0];
				}
	 	$deletion_query = "update  sp_timesheet_tracker set deleted=1,soft_deleted=1 where track_date >= '$week_start' and track_date <= '$week_end' and user_id_c = '$user_id' and timesheet_id = '$timesheet_id' and id not in ('$autoTrackkedIdsstr') and id in ('$ptDeletionIDSstr')";
	
		$db->query($deletion_query);
				if($project_hours > 0){
					$total_hours = $total_hours + $project_hours;
					$this->updateTimeSheetRecord($project_id, $project_task_id, $project_track_date, $project_hours, $user_id, $timesheet_id, $project_time_arr, $account_time_arr, $project_description);
				}
			}
		}
		// exit;
		// update project time
		foreach($project_time_arr as $proj_id=>$proj_time_arr){
			$project_time_total = $this->getWholeProjectTimeSheetEntries($proj_id);
			$update_project = "update project set total_hours_pt = '$project_time_total' where id = '$proj_id' and deleted = 0";
			$db->query($update_project);
		}
		
		// update account time
		foreach($account_time_arr as $acc_id=>$acc_time_arr){
			$account_time_total = $this->getWholeAccountTimeSheetEntries($acc_id);
			$update_account = "update accounts set total_hours_at = '$account_time_total' where id = '$acc_id' and deleted = 0";
			$db->query($update_account);
		}
		//check for manual entered time
		$check_time_sql ="SELECT sum(hours) hours
						FROM   sp_timesheet_tracker 
						WHERE  deleted = 0 
			     	  	AND track_date >= '{$week_start}' 
				       	AND track_date <= '{$week_end}' 
				       	AND id IN (
				       		SELECT sp_timesheet_tracker_id_c 
		                  	FROM   sp_auto_timesheet_time_tracker 
		 	                WHERE  manual_entry = 1 
	                        AND deleted = 0 
			                AND sp_timesheet_id_c = '{$timesheet_id}' 
						) 
						AND related_module IS NOT NULL 
						ORDER  BY track_date ASC ";
		$check_time_res = $db->query($check_time_sql, true);
		$check_time_row = $db->fetchByAssoc($check_time_res);
		$total_hours += (isset($check_time_row['hours'])) ? $check_time_row['hours'] : 0.0;
	}
	
	public function updateTimeSheetRecord($project_id, $project_task_id, $project_track_date, $project_hours, $user_id, $timesheet_id, &$project_time_arr, &$account_time_arr, $project_description){
		
		global $timedate, $db, $sugar_config, $current_user;
		$current_user_id = $user_id;
		
		if(!empty($timesheet_id)){
			$sp_timesheet_bean = BeanFactory::getBean("sp_Timesheet", $timesheet_id);
			$selected_module = $sp_timesheet_bean->module;
			$related_module = $sp_timesheet_bean->related_module;
		}
		else{
			$selected_module = $sugar_config['timesheet_module_'.$current_user_id];
			$related_module = $sugar_config['related_module_'.$current_user_id];
		}
		$tt_id = create_guid();
		$date_entered = $timedate->getInstance()->nowDb();
		$date_modified = $date_entered;
		
		if(empty($project_task_id)){
			$related_record_url = 'No Related Record';
		}
		else{
			
			$related_module_bean = BeanFactory::getBean($related_module, $project_task_id);
			$related_record_name = $related_module_bean->name;
			
			$related_record_url = '<a href="index.php?module='.$related_module.'&action=DetailView&record='.$project_task_id.'">'.$related_record_name.'</a>';
		}
		
		$query = "INSERT INTO `sp_timesheet_tracker`(id, date_entered, date_modified, modified_user_id, created_by, track_date, hours, project_id_c, projecttask_id_c, user_id_c, timesheet_id, parent_id , parent_type, related_record_url, description) VALUES('$tt_id', '$date_entered', '$date_modified', '$user_id', '$user_id', '$project_track_date', '$project_hours', '$project_id', '$project_task_id', '$user_id', '$timesheet_id', '$project_id', '$selected_module', '$related_record_url', '$project_description')";
		
		$db->query($query);
		
		
		// $update_hours_query = "UPDATE `sp_timesheet_tracker` set hours='$project_hours' where track_date='$project_track_date' and user_id_c='$user_id' and (project_id_c='$project_task_id' OR projecttask_id_c='$project_task_id')";
		// $db->query($update_hours_query);
		
		// handle project and timesheet tracker
		
		if($selected_module == "Project"){
			$pt_id = create_guid();
			$query_proj_timesheet = "INSERT into project_sp_timesheet_tracker_1_c (id, date_modified, deleted, project_sp_timesheet_tracker_1project_ida, project_sp_timesheet_tracker_1sp_timesheet_tracker_idb) VALUES ('$pt_id', '$date_entered', '0', '$project_id', '$tt_id')";
			$db->query($query_proj_timesheet);
			
			$project_time_arr[$project_id][] = $project_hours;
			$related_accounts = $this->getAccountsRelatedToProject($project_id);
		}
		
		if($selected_module == "Accounts" && $related_module == "Cases"){
			$account_id = $project_id;
			
			$at_id = create_guid();
			$query_account_timesheet = "INSERT into accounts_sp_timesheet_tracker_1_c (id, date_modified, deleted, accounts_sp_timesheet_tracker_1accounts_ida, accounts_sp_timesheet_tracker_1sp_timesheet_tracker_idb) VALUES ('$at_id', '$date_entered', '0', '$account_id', '$tt_id')";
			// echo "<br/>";
			$db->query($query_account_timesheet);
			
			$account_time_arr[$account_id][] = $project_hours;
			$related_accounts = array();
		}
		
		else if($selected_module == "Cases" && $related_module == "Accounts"){
			if(empty($project_task_id) && !empty($project_id)){
				$account_id = $this->getAccountIDFromCases($project_id);
			}
			else{
				$account_id = $project_task_id;
			}
			
			$at_id = create_guid();
			$query_account_timesheet = "INSERT into accounts_sp_timesheet_tracker_1_c (id, date_modified, deleted, accounts_sp_timesheet_tracker_1accounts_ida, accounts_sp_timesheet_tracker_1sp_timesheet_tracker_idb) VALUES ('$at_id', '$date_entered', '0', '$account_id', '$tt_id')";
			// echo "<br/>";
			$db->query($query_account_timesheet);
			
			$account_time_arr[$account_id][] = $project_hours;
			$related_accounts = array();
		}
		
		for($hh=0; $hh<count($related_accounts); $hh++){
			$at_id = create_guid();
			$account_id = $related_accounts[$hh];
			$query_account_timesheet = "INSERT into accounts_sp_timesheet_tracker_1_c (id, date_modified, deleted, accounts_sp_timesheet_tracker_1accounts_ida, accounts_sp_timesheet_tracker_1sp_timesheet_tracker_idb) VALUES ('$at_id', '$date_entered', '0', '$account_id', '$tt_id')";
			$db->query($query_account_timesheet);
			$account_time_arr[$account_id][] = $project_hours;
		}
	}
	
	public function getAccountIDFromCases($project_id){
		global $db;
		$account_id = '';
		$getAccountsQry = "Select account_id from cases where id = '$project_id' and deleted = 0";
		$resAccounts = $db->query($getAccountsQry);
		while($rowAccountId = $db->fetchByAssoc($resAccounts))
		{
			$account_id = $rowAccountId['account_id'];
		}
		return $account_id;
	}
	public function getAutoTrackedRelatedIDs($timesheet_id){
		global $db;
		$trackked_ids = array();
		$getAutoTimeSheetQry = "Select sp_timesheet_tracker_id_c from sp_auto_timesheet_time_tracker where deleted = 0 and processed = 0 and sp_timesheet_id_c = '$timesheet_id'";
		$resAutoTimeSheet = $db->query($getAutoTimeSheetQry);
		while($rowAutoTimeSheetId = $db->fetchByAssoc($resAutoTimeSheet))
		{
			$trackked_ids[] = $rowAutoTimeSheetId['sp_timesheet_tracker_id_c'];
		}
		return $trackked_ids;
	}
	
	public function getAccountsRelatedToProject($project_id){
		global $db;
		$account_ids = array();
		$getAccountsQry = "Select account_id from projects_accounts where deleted = 0 and project_id = '$project_id'";
		$resAccounts = $db->query($getAccountsQry);
		while($rowAccountId = $db->fetchByAssoc($resAccounts))
		{
			$account_ids[] = $rowAccountId['account_id'];
		}
		return $account_ids;
	}
	
	public function getWholeProjectTimeSheetEntries($project_id){
		global $db;
		$total_hours = 0;
		$getTimeSheetQry = "Select st.hours from sp_timesheet_tracker st INNER JOIN project_sp_timesheet_tracker_1_c pst on st.id = pst.project_sp_timesheet_tracker_1sp_timesheet_tracker_idb where pst.project_sp_timesheet_tracker_1project_ida = '$project_id' and st.deleted=0 and pst.deleted=0";
		$resTimeSheets = $db->query($getTimeSheetQry);
		while($rowTimeSheetId = $db->fetchByAssoc($resTimeSheets))
		{
			$total_hours = $total_hours + $rowTimeSheetId['hours'];
		}
		return $total_hours;
	}
	
	public function getWholeAccountTimeSheetEntries($account_id){
		global $db;
		$total_hours = 0;
		$getTimeSheetQry = "Select st.hours from sp_timesheet_tracker st INNER JOIN accounts_sp_timesheet_tracker_1_c ast on st.id = ast.accounts_sp_timesheet_tracker_1sp_timesheet_tracker_idb where ast.accounts_sp_timesheet_tracker_1accounts_ida = '$account_id' and st.deleted=0 and ast.deleted=0";
		$resTimeSheets = $db->query($getTimeSheetQry);
		while($rowTimeSheetId = $db->fetchByAssoc($resTimeSheets))
		{
			$total_hours = $total_hours + $rowTimeSheetId['hours'];
		}
		return $total_hours;
	}
	
	public function validateStatus($row_pt){
		global $db;
		
		$related_record_url = $row_pt["related_record_url"];
		$projecttask_id_c = $row_pt["projecttask_id_c"];
		
		if($related_record_url == "No Related Record" || empty($projecttask_id_c)){
			return true;
		}
		
		$related_record_arr = explode("?", $related_record_url);
		$related_record_arr2 = explode("&", $related_record_arr[1]);
		$related_record_arr3 = explode("=", $related_record_arr2[0]);
		
		$related_module = $related_record_arr3[1];
		
		if($related_module == "Project" || $related_module == "ProjectTask")
		{
			if($related_module == "Project"){
				$module_table_name = "project";
			}
			else{
				$module_table_name = "project_task";
			}
			
			$getProjectQry = "Select status from $module_table_name p where p.deleted = 0 and p.id = '$projecttask_id_c'";
			$resProject= $db->query($getProjectQry);
			while($rowProjectId = $db->fetchByAssoc($resProject))
			{
				$project_status = $rowProjectId['status'];
			}
			
			if($project_status == "Completed" || $project_status == "Closed" || $project_status == "On_Hold")
			{
				return false;
			}
		}
		return true;
	}
}
?>