<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2019-02-21 03:39:45
$dictionary['Contact']['fields']['jjwg_maps_lat_c']['inline_edit']=1;

 

 // created: 2020-11-17 16:13:41
$dictionary['Contact']['fields']['customer_number_c']['inline_edit']='';
$dictionary['Contact']['fields']['customer_number_c']['labelValue']='Customer Number';

 

 // created: 2019-02-21 03:39:45
$dictionary['Contact']['fields']['jjwg_maps_address_c']['inline_edit']=1;

 

 // created: 2020-11-05 14:13:58
$dictionary['Contact']['fields']['department_ahp_c']['inline_edit']='1';
$dictionary['Contact']['fields']['department_ahp_c']['labelValue']='Department';

 

 // created: 2020-11-19 04:25:02
$dictionary['Contact']['fields']['phone_work']['required']=true;
$dictionary['Contact']['fields']['phone_work']['inline_edit']=true;
$dictionary['Contact']['fields']['phone_work']['comments']='Work phone number of the contact';
$dictionary['Contact']['fields']['phone_work']['merge_filter']='disabled';

 

 // created: 2020-11-03 18:22:43
$dictionary['Contact']['fields']['last_name']['audited']=true;
$dictionary['Contact']['fields']['last_name']['inline_edit']=true;
$dictionary['Contact']['fields']['last_name']['comments']='Last name of the contact';
$dictionary['Contact']['fields']['last_name']['merge_filter']='disabled';

 

 // created: 2020-11-05 11:53:35
$dictionary['Contact']['fields']['preferred_communication_lang_c']['inline_edit']='1';
$dictionary['Contact']['fields']['preferred_communication_lang_c']['labelValue']='Preferred Communication Language';

 

 // created: 2020-11-05 12:45:41
$dictionary['Contact']['fields']['type_of_influencer_c']['inline_edit']='1';
$dictionary['Contact']['fields']['type_of_influencer_c']['labelValue']='Type of Influencer';

 

 // created: 2019-02-21 03:39:45
$dictionary['Contact']['fields']['jjwg_maps_geocode_status_c']['inline_edit']=1;

 

 // created: 2020-11-05 12:43:14
$dictionary['Contact']['fields']['personal_authority_c']['inline_edit']='1';
$dictionary['Contact']['fields']['personal_authority_c']['labelValue']='Personal Authority';

 

 // created: 2020-11-03 18:22:01
$dictionary['Contact']['fields']['title']['inline_edit']=true;
$dictionary['Contact']['fields']['title']['comments']='The title of the contact';
$dictionary['Contact']['fields']['title']['merge_filter']='disabled';

 

 // created: 2020-11-05 14:16:47
$dictionary['Contact']['fields']['salutation']['len']=100;
$dictionary['Contact']['fields']['salutation']['required']=true;
$dictionary['Contact']['fields']['salutation']['inline_edit']=true;
$dictionary['Contact']['fields']['salutation']['comments']='Contact salutation (e.g., Mr, Ms)';
$dictionary['Contact']['fields']['salutation']['merge_filter']='disabled';

 

 // created: 2020-11-03 18:22:24
$dictionary['Contact']['fields']['first_name']['required']=true;
$dictionary['Contact']['fields']['first_name']['audited']=true;
$dictionary['Contact']['fields']['first_name']['inline_edit']=true;
$dictionary['Contact']['fields']['first_name']['comments']='First name of the contact';
$dictionary['Contact']['fields']['first_name']['merge_filter']='disabled';

 

 // created: 2020-11-05 12:42:15
$dictionary['Contact']['fields']['relationship_quality_c']['inline_edit']='';
$dictionary['Contact']['fields']['relationship_quality_c']['labelValue']='Relationship quality';

 

 // created: 2020-11-03 18:24:16
$dictionary['Contact']['fields']['department']['inline_edit']=true;
$dictionary['Contact']['fields']['department']['comments']='The department of the contact';
$dictionary['Contact']['fields']['department']['merge_filter']='disabled';

 

/*********************************************************************************
 * This file is part of package Price Books.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Price Books is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/

/*
 * Relationship definition
 */
$dictionary['Contact']['relationships']['contact_vi_price_books'] = array(
    'lhs_module'        => 'Contacts',
    'lhs_table'         => 'contacts',
    'lhs_key'           => 'id',
    'rhs_module'        => 'VI_Price_Books',
    'rhs_table'         => 'vi_price_books',
    'rhs_key'           => 'contact_id',
    'relationship_type' => 'one-to-many',
);

$dictionary['Contact']['fields']['vi_price_books'] = array(
    'name'         => 'vi_price_books',
    'type'         => 'link',
    'relationship' => 'contact_vi_price_books',
    'module'       => 'VI_Price_Books',
    'bean_name'    => 'VI_Price_Books',
    'source'       => 'non-db',
    'vname'        => 'LBL_PRICE_BOOKS_LABEL',
);



 // created: 2020-11-03 18:22:58
$dictionary['Contact']['fields']['email1']['required']=true;
$dictionary['Contact']['fields']['email1']['inline_edit']=true;
$dictionary['Contact']['fields']['email1']['merge_filter']='disabled';

 

 // created: 2020-11-18 12:52:01
$dictionary['Contact']['fields']['customer_account_group_c']['inline_edit']='1';
$dictionary['Contact']['fields']['customer_account_group_c']['labelValue']='Customer Account Group';

 

 // created: 2019-02-21 03:39:45
$dictionary['Contact']['fields']['jjwg_maps_lng_c']['inline_edit']=1;

 
?>