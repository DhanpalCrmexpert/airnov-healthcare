<?php
/*********************************************************************************
 * This file is part of package Price Books.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Price Books is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
$mod_strings['LBL_PRICE_BOOKS'] = "Preisbücher";
$mod_strings['LBL_PRICE_BOOKS_DESCRIPTION'] = "Preisbücher von Produkten für bestimmte Kontakte / Konten verwalten.";
$mod_strings['LBL_ENABLE_PRICE_BOOKS'] = "Preisbuchfunktion aktivieren";
$mod_strings['LBL_PRICE_BOOKS_ACTIVATED'] = "Preisbuchfunktion erfolgreich aktiviert.";
$mod_strings['LBL_PRICE_BOOKS_DEACTIVATED'] = "Preisbuchfunktion erfolgreich deaktiviert.";
$mod_strings['LBL_CLICK_HERE'] = "Hier klicken";
$mod_strings['LBL_MANAGE_PRICEBOOK'] = "Preisbücher verwalten";
//license
$mod_strings['LBL_LICENCE_ACTIVE_LABEL'] = "VIPriceBooksLicenseAddon ist aus folgendem Grund nicht mehr aktiv:";
$mod_strings['LBL_LICENCE_ISSUES'] = "Benutzer haben bis zur Behebung des Problems keinen Zugriff mehr";
$mod_strings['LBL_CLICK_HERE'] = "Hier klicken";
$mod_strings['LBL_LICENCE_ACTIVE'] = "VIPriceBooksLicenseAddon ist nicht mehr aktiv";
$mod_strings['LBL_RENEW_LICENCE'] = "Bitte verlängern Sie Ihr Abonnement oder überprüfen Sie Ihre Lizenzkonfiguration.";
$mod_strings['LBL_UPDATE_LICENSE'] = "Lizenz aktualisieren";
?>