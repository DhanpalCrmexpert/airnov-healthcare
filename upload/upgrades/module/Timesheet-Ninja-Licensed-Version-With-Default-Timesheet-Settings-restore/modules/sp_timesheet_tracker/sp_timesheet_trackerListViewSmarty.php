<?php

require_once('include/ListView/ListViewSmarty.php');

class sp_timesheet_trackerListViewSmarty extends ListViewSmarty {

    /* function sp_timesheet_trackerListViewSmarty() {

        parent::ListViewSmarty();
    } */

    function buildExportLink($id = 'export_link') {

        global $app_strings;
        global $mod_strings;
        global $sugar_config;

		$script = "<a href='javascript:void(0)' id='export_listview_top' name='export_listview_top' onclick=\"return sListView.send_form(true, '{$_REQUEST['module']}', " .
                    "'index.php?entryPoint=export','{$app_strings['LBL_LISTVIEW_NO_SELECTED']}')\">{$mod_strings['LBL_EXPORT']}</a>";
					
        $script .= "<a href='javascript:void(0)' id='export_pdf' name='export_pdf' onclick=\"return sListView.send_form(true, '{$_REQUEST['module']}', " .
                    "'index.php?entryPoint=export_pdf','{$app_strings['LBL_LISTVIEW_NO_SELECTED']}')\">{$mod_strings['LBL_EXPORT_PDF']}</a>";
					
		$script .= "<a href='javascript:void(0)' id='create_invoice' name='create_invoice' onclick=\"return sListView.send_form(true, '{$_REQUEST['module']}', " .
                    "'index.php?entryPoint=create_invoice','{$app_strings['LBL_LISTVIEW_NO_SELECTED']}')\">{$mod_strings['LBL_CREATE_INVOICE']}</a>";			

        return $script;
    }

}

?>
