<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * This file is part of package Price Books.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Price Books is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
require_once('custom/VIPriceBooks/VIPriceBooksFunction.php');
class VIPriceBooksGetData{
    public function __construct(){
    	$this->getPriceBooksRecords();
    } 
    //Get Price Books Records For Invoice/Quotes
    public function getPriceBooksRecords(){
    	$accountId = $_REQUEST['accountId'];
		$contactId = $_REQUEST['contactId'];
        $productId = $_REQUEST['productId'];
        /*$count = $_REQUEST['counter'];
        $moduleName = $_REQUEST['modulename'];*/
        
        if($accountId != '' && $contactId !=''){
            $operator = 'AND';
            $priceBooksData = getPriceBooksRecordsForQuotesInvoice($operator,$accountId,$contactId,$productId);
        }else if($accountId != '' || $contactId != ''){
            $operator = 'OR';
            $priceBooksData = getPriceBooksRecordsForQuotesInvoice($operator,$accountId,$contactId,$productId);
        }elseif($accountId == '' && $contactId == ''){
            $priceBooksData = array();
        }
        
        $html = '<div class="modalBody">
                    <table id="priceTable">
                        <thead>
                            <tr class="listViewHeaders">
                                <td id="pricefirst_column"></td>
                                <td id="price_column"><b>'.translate('LBL_NAME','VI_Price_Books').'</b></td>
                                <td id="price_column"><b>'.translate('LBL_ACCOUNT_NAME','VI_Price_Books').'</b></td>
                                <td id="price_column"><b>'.translate('LBL_CONTACT_NAME','VI_Price_Books').'</b></td>
                                <td id="price_column"><b>'.translate('LBL_ASSIGNED_TO','VI_Price_Books').'</b></td>
                            </tr>
                        </thead>
                        <tbody>';
                            if(!empty($priceBooksData)){
                                foreach($priceBooksData as $key=>$value){
                                    $html .= '<tr><td id="checkboxRow" class="price_value_row"><input type="checkbox" class="listViewEntriesCheckBox" value="'.$value['id'].'" onclick="selectPriceBook()"></td><td class="price_value_row">'.$value['name'].'</td>';
                                    $accountBean = BeanFactory::getBean('Accounts',$value['account_id']);
                                    $html .= '<td class="price_value_row"><a href="?module=Accounts&action=DetailView&record='.$value['account_id'].'" target="_blank" class="relateModuleLink">'.$accountBean->name.'</a></td>';
                                    $contactBean = BeanFactory::getBean('Contacts',$value['contact_id']);
                                    $html .='<td class="price_value_row"><a href="?module=Accounts&action=DetailView&record='.$value['account_id'].'" target="_blank" class="relateModuleLink">'.$contactBean->name.'</a></td>';
                                    $tablename = 'users';
                                    $fieldNames = array('user_name');                
                                    $where = array('id'=>$value['assigned_user_id']);
                                    $selUserName = getPriceBooksRecord($tablename,$fieldNames,$where);
                                    $selRow = $GLOBALS['db']->fetchOne($selUserName);
                                    $username = $selRow['user_name'];
                                    $html .= '<td class="price_value_row">'.$username.'</td></tr>';
                                }//end of foreach
                            }else{
                                $html .= "<tr><td colspan='6'><center><h3>".translate('LBL_NO_RECORDS','VI_Price_Books')."</h3></center></td></tr>";
                            }
        $html .='</tbody></table></div>';
        echo $html; 
    }//end of function
}//end of class
new VIPriceBooksGetData();
?>