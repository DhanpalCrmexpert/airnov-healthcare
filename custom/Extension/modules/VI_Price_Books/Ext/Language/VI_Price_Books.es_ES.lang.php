<?php
/*********************************************************************************
 * This file is part of package Price Books.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Price Books is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
$mod_strings['LBL_ACTIVE'] = 'Activo';
$mod_strings['LBL_PERCENTAGE_MARK'] = 'Marca de porcentaje';
$mod_strings['LBL_TYPE'] = 'Tipo de precio';
$mod_strings['LBL_ROUND_OFF'] = 'Redondear a';
$mod_strings['LBL_ACCOUNT_NAME'] = 'Nombre de cuenta';
$mod_strings['LBL_ACCOUNT_ID'] = 'ID de cuenta';
$mod_strings['LBL_ACCOUNTS'] = 'Cuentas';
$mod_strings['LBL_CONTACT_NAME'] = 'Nombre de contacto';
$mod_strings['LBL_CONTACT_ID'] = 'ID de contacto';
$mod_strings['LBL_CONTACTS'] = 'Contactos';
$mod_strings['LBL_FLAT_PRICE'] = 'Precio fijo';
$mod_strings['LBL_DIFFERENTIAL_PERCENTAGE'] = 'Diferencial por (%)';
$mod_strings['LBL_DIFFERENTIAL_DOLLAR'] = 'Diferencial por ($)';
$mod_strings['LBL_PRICE_BOOKS'] = 'Libros de precios';
$mod_strings['LBL_NAME'] = 'Nombre';
$mod_strings['LBL_PRICE_TYPE'] = 'Tipo de precio';
$mod_strings['LBL_ASSIGNED_TO'] = 'Asignado a';
$mod_strings['LBL_NO_RECORDS'] = 'No se encontraron registros';
$mod_strings['LBL_MARK_UP'] = 'Marcado (Agregar)';
$mod_strings['LBL_MARK_DOWN'] = 'MarkDown (Sub)';
$mod_strings['LBL_ADD_QUANTITY'] = 'Agregar cantidad';
$mod_strings['LBL_PRODUCT_NAME'] = 'Nombre del producto';
$mod_strings['LBL_PRODUCT_PRICE'] = 'Precio del producto';
$mod_strings['LBL_LIST_PRICE'] = 'Precio de lista';
$mod_strings['LBL_ADD_PRODUCT'] = 'Agregar producto';
$mod_strings['LBL_YES'] = 'Sí';
$mod_strings['LBL_NO'] = 'No';
$mod_strings['LBL_SELECT_PRODUCT_MSG'] = "¡Seleccione el producto antes de hacer clic en los libros de precios!";
$mod_strings['LBL_SELECT_PRICEBOOK_MSG'] = "Seleccione solo un libro de precios";
$mod_strings['LBL_PRODUCT_VALIDATION'] = 'Seleccione otro producto. Este producto ya está seleccionado. ';
$mod_strings['LBL_QUANTITY_VALID_MSG'] = "Valor inválido:";
$mod_strings['LBL_FROM_QUANTITY'] = "De la cantidad";
$mod_strings['LBL_TO_QUANTITY'] = "A la cantidad";
$mod_strings['LBL_DIFFERENTIAL'] = "Diferencial";
$mod_strings['LBL_PRICE_BOOKS_ACCESS'] = "Habilite la función de libros de precios para acceder al módulo de libros de precios";
$mod_strings['LBL_CLICK_HERE'] = "Haga clic aquí";
$mod_strings['LBL_ENABLE_LABEL'] = "para Activarlo";
$mod_strings['LBL_FROM_QUANTITY_VALID'] = "Ingrese mayor valor de cantidad que anterior al valor de cantidad";
$mod_strings['LBL_TO_QUANTITY_VALID'] = "Ingrese un valor mayor a la cantidad que a partir del valor de la cantidad";
$mod_strings['LBL_PRODUCT_LIST_PRICE_VALID'] = "Agregue producto y precio de lista para agregar cantidad";
$mod_strings['LBL_PRODCUT_PRICE_VALID'] = "Agregue el precio del producto para el producto";
$mod_strings['LBL_LIST_PRICE_VALID'] = "Agregue el precio de lista para el producto";
$mod_strings['LBL_QUANTITY_VALID_RANGE'] = "Agregue rango de cantidad para el producto";
$mod_strings['LBL_PRODUCT_VALID'] = "Agregue producto para agregar cantidad";
$mod_strings['LBL_PRODUCT_FIELDS_VALIDATION'] = "Agregue nombre de producto y precio de lista para libros de precios";
$mod_strings['LBL_QUANTITY_FIELD_VALIDATION'] = "Complete el rango de cantidad para losas vacías";
$mod_strings['LBL_EDITVIEW_PANEL1'] = "Detalles de libros de precios";
$mod_strings['LBL_DETAILVIEW_PANEL1'] = "Detalles de libros de precios";
?> 