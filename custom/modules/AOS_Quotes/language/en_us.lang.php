<?php
// created: 2020-11-13 17:05:15
$mod_strings = array (
  'LBL_BILLING_ACCOUNT' => 'Customer',
  'LBL_ACCOUNTS' => 'Customers',
  'LBL_CREATE_OPPORTUNITY' => 'Convert Opportunity',
  'LBL_TARGET_YEAR' => 'Target Year',
  'LBL_ASSOCIATED_YEAR' => 'Associated Year',
);