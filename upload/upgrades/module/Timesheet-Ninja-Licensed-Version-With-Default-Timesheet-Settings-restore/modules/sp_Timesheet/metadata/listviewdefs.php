<?php
$module_name = 'sp_Timesheet';
$listViewDefs [$module_name] = 
array (
  'NAME' => 
  array (
    'width' => '32%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
  ),
  'TRACK_USER' => 
  array (
    'type' => 'relate',
    'studio' => 'visible',
    'label' => 'LBL_TRACK_USER',
    'id' => 'USER_ID_C',
    'link' => true,
    'width' => '10%',
    'default' => true,
  ),
  'TRACK_DATE_FROM' => 
  array (
    'type' => 'date',
    'label' => 'LBL_TRACK_DATE_FROM',
    'width' => '10%',
    'default' => true,
  ),
  'TRACK_DATE_TO' => 
  array (
    'type' => 'date',
    'label' => 'LBL_TRACK_DATE_TO',
    'width' => '10%',
    'default' => true,
  ),
 'HOURS' => 
  array (
    'type' => 'float',
    'label' => 'LBL_HOURS',
    'width' => '10%',
    'default' => true,
  ),
 'MODULE' => 
  array (
    'label' => 'LBL_MODULE',
    'width' => '10%',
    'default' => true,
  ),
 'RELATED_MODULE' => 
  array (
    'label' => 'LBL_RELATED_MODULE',
    'width' => '10%',
    'default' => true,
  ),
);
?>
