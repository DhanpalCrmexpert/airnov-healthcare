<?php
array_push($job_strings, 'send_Email_For_Auto_Track_Timings');

function send_Email_For_Auto_Track_Timings(){
		global $app_list_strings, $timedate, $current_user, $app_strings, $db, $sugar_config;
		
		$current_date_time = $timedate->get_gmt_db_datetime();
		
		// Prep time trackker Query
		$selectProjectTask = "SELECT * from sp_auto_timesheet_time_tracker WHERE processed = 0 and deleted = 0";
		
		$result = $db->query($selectProjectTask);

		while($row = $db->fetchByAssoc($result)){
			
			if(!empty($row['last_send_email_at'])){
				$last_send_email_at = $row['last_send_email_at'];
			}
			else{
				$last_send_email_at = $row['date_entered'];
			}
			
			$record_id = $row['id'];
			$current_user_id = $row['user_id_c'];
			$parent_type = $row['parent_type'];
			$parent_id = $row['parent_id'];
			
			$parent_module_bean = BeanFactory::getBean($parent_type, $parent_id);
			
			if(empty($parent_module_bean->id)){
				continue;
			}
			$record_name = $parent_module_bean->name;
			
			$mints = get_time_difference_in_emails($last_send_email_at, $current_date_time);
			
			if($mints >= 45){
				
				$user_obj = BeanFactory::getBean("Users", $current_user_id);
				$email_address = $user_obj->email1;
				if(empty($email_address)){
					continue;
				}
				
				$update_query = "UPDATE sp_auto_timesheet_time_tracker set last_send_email_at = '".$current_date_time."' where id = '".$record_id."'";
				
				$db->query($update_query);
				
				require_once('modules/EmailTemplates/EmailTemplate.php');
    	
				$objTemplate = new EmailTemplate();
				$objTemplate->retrieve_by_string_fields(array('name' => 'Timesheet - Auto Track Timer Notification'));
				
				$link = $sugar_config['site_url'].'/index.php?action=DetailView&module='.$parent_type.'&record=' . $parent_id;
				
				if (!empty($objTemplate->id)) {
					
					$subject = $objTemplate->subject;
					$subject = str_replace('{RELATED_RECORD}', $record_name, $subject);
					
					$body_html = $objTemplate->body_html;
					$body = $objTemplate->body;
				
					$body_html = str_replace('{LINK}', $link, $body_html);
					$body = str_replace('{LINK}', $link, $body);
					
					$body_html = str_replace('{CURRENT_USER}', $user_obj->name, $body_html);
					$body = str_replace('{CURRENT_USER}', $user_obj->name, $body);
					
					$body_html = str_replace('{RELATED_RECORD}', $record_name, $body_html);
					$body = str_replace('{RELATED_RECORD}', $record_name, $body);
				}
				else{
					$subject = "Timesheet Notification Reminder for Task ".$record_name;
					
					$body_html = "Hi ".$user_obj->name. ",<br/> <br/>
								
								You are auto tracking time for the Project task entitled '".$record_name."'.<br/>
								You can stop the timer by redirecting to this link: ".$link."  If your are still working on your task, feel free to continue working.<br/> <br/>
		
								Thank you.<br/> <br/>

								Timesheets Professional System.<br/> <br/>";
					$body = $body_html;
				}
				
				require_once("include/SugarPHPMailer.php");
				$note_msg=new SugarPHPMailer();
				$admin = new Administration();
				$admin->retrieveSettings();
				$note_msg->Subject = $subject;
				$note_msg->prepForOutbound();
				$note_msg->setMailerForSystem();
				
				$note_msg->IsHTML(true);
				$note_msg->From     = $admin->settings['notify_fromaddress'];
				$note_msg->FromName = $admin->settings['notify_fromname'];
				$note_msg->AddAddress($email_address);
				$note_msg->Body = from_html($body_html);
				$note_msg->AltBody = from_html($body);
				
				
				if (!$note_msg->Send()){
					 $GLOBALS['log']->info("Could not send email: " . $note_msg->ErrorInfo);
				}
			}
		}
		return true;
}

function get_time_difference_in_emails( $start, $end )
{
	$uts['start']      =    strtotime( $start );
	$uts['end']        =    strtotime( $end );
	
	if( $uts['start']!==-1 && $uts['end']!==-1 )
	{
		if( $uts['end'] >= $uts['start'] )
		{
			$diff    =    $uts['end'] - $uts['start'];
			$minutes=intval((floor($diff/60)));	
			return $minutes;
		}
		else
		{
			trigger_error( "Ending date/time is earlier than the start date/time", E_USER_WARNING );
		}
	}
	else
	{
		trigger_error( "Invalid date/time data detected", E_USER_WARNING );
	}
	return( false );
}

?>