function addTimerPopJS(timeS,parent_name,parent_type,parent_id,user_name,user_id, second, id, timerPause=false){
    var openDialogue = true;
    $(window).unload(function() {
      return "Handler for .unload() called.";
    });
    $("div[aria-describedby]").each(function(){
        if($(this).attr("aria-describedby") === "timerPopup"){
            openDialogue = false;
        }
    });
    var time = display_time(timeS);
    if (openDialogue === true) {
        html = "<div id='timerPopup' title='Timer Popup'><p>";
        html +="<span style='display:none;' id='popupTimerTime'>"+timeS+"</span>";
        html +="<span style='font-size:40px;' id='displayValueHours'>"+time.hours+"</span> Hour(s)";
        html +="<span style='font-size:40px;' id='displayValueMinutes'>"+time.minutes+"</span> Minute(s)";
        html +="<span style='font-size:40px;' id='displayValueSeconds'>"+second+"</span> Second(s)";
        html +="</p><hr style='margin-bottom:1'><p><b>Record:</b> "+parent_name+"</p>";
        html +="<p><b>User:</b> "+user_name+"</p>";
        html +="<span style='display:none' id='poupRecordType'>"+parent_type+"</span>";
        html +="<span style='display:none' id='poupRecordId'>"+parent_id+"</span>";
        html +="<span style='display:none' id='timeRecordId'>"+id+"</span></div>";
        $("#content").append(html);
        $( "#timerPopup" ).dialog({
            position: {at: 'right top'},
            height: "auto",
            width:"350px",
            buttons:[
                {
                    text: "Start",
                    icons: {
                        primary: "ui-icon-play"
                    },
                    click: function() {
                        var timeRecordId = $("#timeRecordId").text();
                        var timeSecond = $("#displayValueSeconds").text();
                        $.post("index.php?module=suh_timer_recording&action=updateTimerDB&sugar_body_only=true",
                        {
                            timeRecordId: timeRecordId,
                            second: timeSecond,
                            timerPause:false,
                        },
                        function(data, status){
                            startEngine(timeSecond);
                        });
                    },
                },
                {
                    text: "Pause",
                    icons: {
                        primary: "ui-icon-pause"
                    },
                    click: function() {
                        var timeRecordId = $("#timeRecordId").text();
                        var timeSecond = $("#displayValueSeconds").text();
                        $.post("index.php?module=suh_timer_recording&action=updateTimerDB&sugar_body_only=true",
                        {
                            timeRecordId: timeRecordId,
                            second: timeSecond,
                            timerPause:true,
                        },
                        function(data, status){
                            pauseEngine();
                        });
                    },
                },
                {
                    text: "End",
                    icons: {
                        primary: "ui-icon-stop"
                    },
                    click: function() {
                        var timeRecordId = $("#timeRecordId").text();
                        endTimer(timeRecordId);
                        // var timeSecond = $("#displayValueSeconds").text();
                        // $.post("index.php?module=sp_Timesheet&action=endAutoTrackTime&sugar_body_only=true",
                        // {
                        //     timer_record_id: timeRecordId,
                        // },
                        // function(data, status){
                        //     window.location.reload(true);
                        // });
                    }
                }
            ],
        });
    }

    if(timerPause == 0){
        startEngine(second);
    }
}


function updateDbAboutTimer(){
    var timeSpend = $("#popupTimerTime").text();
    var recordType = $("#poupRecordType").text();
    var recordId = $("#poupRecordId").text();
    var userId = $("#userId").text();
    var timeRecordId = $("#timeRecordId").text();
    $.post("index.php?module=suh_timer_recording&action=updateTimerDB&sugar_body_only=true",
    {
        timeSpend: timeSpend,
        recordType: recordType,
        recordId: recordId,
        timeRecordId: timeRecordId
    },
    function(data, status){
        console.log(data);
        if(trim(data) === '30mints'){
            if(!confirm("Do you want to continue with timer?")){
                endTimer(timeRecordId);
            }
        }
        if(trim(data) === '10mints'){
            // capture();
        }
    });
}

function capture() {
    html2canvas($('body'),{
        onrendered: function (canvas) {
            var imgString = canvas.toDataURL("image/png");
            window.open(imgString);
        }
    });
}

function updateDbAboutSecond(seconds){
    var recordType = $("#poupRecordType").text();
    var recordId = $("#poupRecordId").text();
    var userId = $("#userId").text();
    var timeRecordId = $("#timeRecordId").text();
    $.post("index.php?module=suh_timer_recording&action=updateTimerDB&sugar_body_only=true",
    {
        second: seconds,
        timeRecordId: timeRecordId
    },
    function(data, status){
        if(trim(data) !== undefined && (data.toLocaleLowerCase()=='ended' || data.toLocaleLowerCase()=='pause')){
            window.location.reload(true);
        }
    });
}
timer = new Timer();
function startEngine(second=0){
    var sec = second;
    timer.start({precision: 'seconds'});
    timer.addEventListener('secondsUpdated', function (e) {
        sec++;
        if(sec >= 60){
            var time = $("#popupTimerTime").text();
            if(time === null || time === undefined || time.length < 1 || time == "0"){
                time = 0;
            }
            time = +time + 1;
            var displaytime = display_time(time);
            $("#popupTimerTime").text(time);
            $("#displayValueHours").text(displaytime.hours);
            $("#displayValueMinutes").text(displaytime.minutes);
            updateDbAboutTimer();
            sec = 0;
        }
        if(sec%2 === 0){//store second after every three seconds
            updateDbAboutSecond(sec);
        }
        $("#displayValueSeconds").text(sec);
    });
}

function pauseEngine(){
    timer.stop();
}

function display_time(minutes){
    if(minutes === null || minutes === undefined || minutes.length < 1 || minutes == "0"){
        minutes = 0;
    }
    var hour = 0;
    if(minutes > 60){
        hour = parseInt(minutes/60);
        minutes = (minutes % 60 == 0) ? 0 : minutes%60;
    }

    return {'hours':hour, 'minutes':minutes};
}

function endTimer(timerRecordId){
    $.post("index.php?module=sp_Timesheet&action=endAutoTrackTime&sugar_body_only=true",
    {
        timer_record_id: timerRecordId,
    },
    function(data, status){
        window.location.reload(true);
    });
}