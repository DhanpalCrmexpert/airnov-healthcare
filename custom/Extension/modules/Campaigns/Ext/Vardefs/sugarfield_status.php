<?php
 // created: 2020-10-01 05:29:32
$dictionary['Campaign']['fields']['status']['default']='Planning';
$dictionary['Campaign']['fields']['status']['inline_edit']='';
$dictionary['Campaign']['fields']['status']['comments']='Status of the campaign';
$dictionary['Campaign']['fields']['status']['merge_filter']='disabled';
$dictionary['Campaign']['fields']['status']['massupdate']=0;

 ?>