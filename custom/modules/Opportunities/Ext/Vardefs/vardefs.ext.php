<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2020-09-01 11:04:27
$dictionary['Opportunity']['fields']['item_description_c'] = array(
    'name' => 'item_description_c',
    'vname' => 'LBL_ITEM_DESCRIPTION',
    'type' => 'text',
    'required' => false,
    'audited' => true,
    'reportable' => true,
);

$dictionary['Opportunity']['fields']['quantity_c'] = array(
    'name' => 'quantity_c',
    'vname' => 'LBL_QUANTITY_C',
    'type' => 'float',
    'required' => false,
    'audited' => true,
    'reportable' => true,
);

$dictionary['Opportunity']['fields']['npr_c'] = array(
    'name' => 'npr_c',
    'vname' => 'LBL_NPR',
    'type' => 'enum',
    'options' => 'yes_no_list',
    'required' => false,
    'audited' => true,
    'reportable' => true,
    'default' => '',
);

$dictionary['Opportunity']['fields']['nda_c'] = array(
    'name' => 'nda_c',
    'vname' => 'LBL_NDA',
    'type' => 'enum',
    'options' => 'yes_no_list',
    'required' => false,
    'audited' => true,
    'reportable' => true,
    'default' => '',
);

$dictionary['Opportunity']['fields']['potential_launch_c'] = array(
    'name' => 'potential_launch_c',
    'vname' => 'LBL_POTENTIAL_LAUNCH',
    'type' => 'date',
    'required' => false,
    'audited' => true,
    'reportable' => true,
);

$dictionary['Opportunity']['fields']['success_rate_c'] = array(
    'name' => 'success_rate_c',
    'vname' => 'LBL_SUCCESS_RATE',
    'type' => 'float',
    'required' => false,
    'audited' => true,
    'reportable' => true,
);

$dictionary['Opportunity']['fields']['production_site_c'] = array(
    'name' => 'production_site_c',
    'vname' => 'LBL_PRODUCTION_SITE',
    'type' => 'varchar',
    'required' => false,
    'audited' => true,
    'reportable' => true,
);



 // created: 2020-08-24 14:01:21
$dictionary['Opportunity']['fields']['sales_stage_amount_c']['inline_edit']='';
$dictionary['Opportunity']['fields']['sales_stage_amount_c']['labelValue']='Sales Stage Amount';

 

 // created: 2020-10-27 15:48:59
$dictionary['Opportunity']['fields']['sales_stage']['len']=100;
$dictionary['Opportunity']['fields']['sales_stage']['inline_edit']=true;
$dictionary['Opportunity']['fields']['sales_stage']['comments']='Indication of progression towards closure';
$dictionary['Opportunity']['fields']['sales_stage']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['sales_stage']['required']=false;

 

 // created: 2019-02-21 03:39:46
$dictionary['Opportunity']['fields']['jjwg_maps_lat_c']['inline_edit']=1;

 

 // created: 2020-11-06 08:31:32
$dictionary['Opportunity']['fields']['user_id_c']['inline_edit']=1;

 

 // created: 2020-11-06 08:35:11
$dictionary['Opportunity']['fields']['country_ship_to_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['country_ship_to_c']['labelValue']='Country (ship-to)';

 

 // created: 2019-02-21 03:39:46
$dictionary['Opportunity']['fields']['jjwg_maps_address_c']['inline_edit']=1;

 

 // created: 2020-11-06 08:40:09
$dictionary['Opportunity']['fields']['region_ship_to_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['region_ship_to_c']['labelValue']='Region (ship-to)';

 

 // created: 2020-11-06 08:44:41
$dictionary['Opportunity']['fields']['estimated_start_of_sales_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['estimated_start_of_sales_c']['labelValue']='Estimated Start of Sales';

 

 // created: 2020-11-06 09:21:00
$dictionary['Opportunity']['fields']['material_description_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['material_description_c']['labelValue']='Material Description';

 

 // created: 2020-11-06 09:46:18
$dictionary['Opportunity']['fields']['secondary_responsible_person_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['secondary_responsible_person_c']['labelValue']='Secondary Responsible Person';

 

 // created: 2020-10-27 15:49:16
$dictionary['Opportunity']['fields']['probability']['required']=true;
$dictionary['Opportunity']['fields']['probability']['inline_edit']=true;
$dictionary['Opportunity']['fields']['probability']['comments']='The probability of closure';
$dictionary['Opportunity']['fields']['probability']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['probability']['enable_range_search']=false;
$dictionary['Opportunity']['fields']['probability']['min']=0;
$dictionary['Opportunity']['fields']['probability']['max']=100;
$dictionary['Opportunity']['fields']['probability']['disable_num_format']='';

 

 // created: 2020-11-12 16:58:11
$dictionary['Opportunity']['fields']['competitor_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['competitor_c']['labelValue']='Competitor';

 

 // created: 2020-11-06 08:48:25
$dictionary['Opportunity']['fields']['customer_application_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['customer_application_c']['labelValue']='Customer application';

 

 // created: 2019-02-21 03:39:46
$dictionary['Opportunity']['fields']['jjwg_maps_geocode_status_c']['inline_edit']=1;

 

 // created: 2020-11-06 09:07:46
$dictionary['Opportunity']['fields']['currency_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['currency_c']['labelValue']='Currency';

 

 // created: 2020-11-06 09:20:44
$dictionary['Opportunity']['fields']['material_number_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['material_number_c']['labelValue']='Material Number';

 

 // created: 2020-11-06 08:47:51
$dictionary['Opportunity']['fields']['market_segment_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['market_segment_c']['labelValue']='Market Segment';

 

 // created: 2020-11-06 08:37:33
$dictionary['Opportunity']['fields']['opportunity_type']['len']=100;
$dictionary['Opportunity']['fields']['opportunity_type']['inline_edit']=true;
$dictionary['Opportunity']['fields']['opportunity_type']['comments']='Type of opportunity (ex: Existing, New)';
$dictionary['Opportunity']['fields']['opportunity_type']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['opportunity_type']['required']=true;

 

 // created: 2020-11-06 08:37:12
$dictionary['Opportunity']['fields']['product_segment_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['product_segment_c']['labelValue']='Product Segment';

 

 // created: 2020-11-06 08:32:59
$dictionary['Opportunity']['fields']['opportunity_status_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['opportunity_status_c']['labelValue']='Opportunity Status';

 

 // created: 2020-11-06 09:09:10
$dictionary['Opportunity']['fields']['topic_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['topic_c']['labelValue']='Topic';

 

 // created: 2019-02-21 03:39:46
$dictionary['Opportunity']['fields']['jjwg_maps_lng_c']['inline_edit']=1;

 
?>