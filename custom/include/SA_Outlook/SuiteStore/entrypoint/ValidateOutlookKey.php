<?php
/**
 * Copyright (c) SalesAgility Ltd.  All Rights Reserved.
 * Licensed under the GPL3 License.  See License in the project root
 * for license information.
 *
 * @package Outlook Addin for SuiteCRM
 * @copyright 2020 SalesAgility Ltd http://www.salesagility.com
 * @author SalesAgility <info@salesagility.com
 *
 */

if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

require_once 'custom/include/SA_Outlook/Outlook.php';
$key = $_REQUEST['sa_outlook_license_key'];

$outlook = new SalesAgility\Outlook\Outlook();
$validated = $outlook->validate($key);

$json = getJSONobj();
$result = $json::encode($validated);
print($result);
