<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * This file is part of package Price Books.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Price Books is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
require_once('modules/VIPriceBooksLicenseAddon/license/VIPriceBooksOutfittersLicense.php');
require_once('include/MVC/Controller/SugarController.php');
require_once('custom/VIPriceBooks/VIPriceBooksFunction.php');
class VIPriceBooksInvoiceQuotes{
	static $already_ran = false;
	function VIPriceBooksInvoiceQuotes($event,$arguments){
		if($_REQUEST['module'] != 'ModuleBuilder'){
			$tableName = 'config';
			$fieldNames = array('*');
			$where = array('name' => 'lic_price-books');    		
	  		
	  		$getLicenseCheckQuery = getPriceBooksRecord($tableName,$fieldNames,$where);
			$getLicenseCheckQueryResult = $GLOBALS['db']->fetchOne($getLicenseCheckQuery);
			$validate_license = VIPriceBooksOutfittersLicense::isValid('VIPriceBooksLicenseAddon');
			if(!empty($getLicenseCheckQueryResult && $validate_license == 1)){

				global $suitecrm_version;
				$randomNumber = rand();
				
				$tablename = "vi_enable_pricebooks";
			    $fieldNames = array('enable');
			    $where = array();

			    $selData = getPriceBooksRecord($tablename,$fieldNames,$where);
			    $resultData = $GLOBALS['db']->fetchOne($selData);
				$enable = $resultData['enable'];

				if($_REQUEST['module'] == "VI_Price_Books" && ($GLOBALS['app']->controller->action == 'EditView' || $GLOBALS['app']->controller->action == 'DetailView')){
					$action = $_REQUEST['action'];
					if(isset($_REQUEST['record'])){
						$record = $_REQUEST['record'];
					}else{
						$record = '';
					}
					
					if(isset($_REQUEST['return_action'])){
						$returnAction = $_REQUEST['return_action'];
					}else{
						$returnAction = '';
					}
					echo "<link rel='stylesheet' type='text/css' href='custom/include/VIPriceBooks/css/VIPriceBooksLogicHook.css'/>";

					echo '<script type="text/javascript"> 
                           var returnAction='."'".$returnAction."'".';
						   var record='."'".$record."'".'; 
						   var action = '."'".$action."'".';
						   var getPriceBookConfigValue = '."'".$enable."'".';

                           var script   = document.createElement("script");
                           script.type  = "text/javascript";
                           script.src   = "custom/include/VIPriceBooks/js/VIPriceBooksLogicHook.js?v='.$randomNumber.'";
                           document.body.appendChild(script);</script>';
						
					if($GLOBALS['app']->controller->action == 'EditView'){
						echo '<script type="text/javascript" src="custom/include/VIPriceBooks/js/VIPriceBooksEditViewCheckCondition.js?v='.$randomNumber.'"></script>';
					}
				}
				if(($_REQUEST['module'] == "AOS_Invoices" || $_REQUEST['module'] == "AOS_Quotes") && $GLOBALS['app']->controller->action == 'EditView'){
					if($enable == 1){
						$productValidation=translate('LBL_SELECT_PRODUCT_MSG','VI_Price_Books');
						$priceBooksMsg = translate('LBL_SELECT_PRICEBOOK_MSG','VI_Price_Books');
						$priceBooksLabel = translate('LBL_PRICE_BOOKS','VI_Price_Books');

						echo '<script type="text/javascript">
								var productValidation='."'".$productValidation."'".';
								var priceBooksMsg='."'".$priceBooksMsg."'".';
								var priceBooksLabel='."'".$priceBooksLabel."'".';
							</script>';
						echo '<script type="text/javascript" src="custom/include/VIPriceBooks/js/VIPriceBooksLineItems.js?v='.$randomNumber.'"></script>';

						$html = "<link rel='stylesheet' type='text/css' href='custom/include/VIPriceBooks/css/VIPriceBooksLineItems.css' />";
						$html .= '<div id="priceBookPopup" class="modal myModal fade in" tabindex="-1" role="dialog" style="display:none;">
		        					<div class="modal-dialog modal-lg">
			                    		<form name="PriceBooksPopupForm" id="PriceBooksPopupForm">
			                        		<div class="modalContent">
			                            		<div class="modal-header">
			                                		<button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="closePriceBooksPopup()"><span aria-hidden="true">×</span></button>
			                                		<h4 class="modal-title"><b>'.$priceBooksLabel.'</b></h4>
			                            		</div>
			                            		<div class="modal-body" id="searchList">
				                            		<div id="popup-loading" style="display:none;"></div>
					    						</div>
				                                <div class="modalFooter">';
					                                if($_REQUEST['module'] == "AOS_Invoices"){
					                                    $html .= '<input class="button" type="button" name="button" value="Add To Invoice" id="add_line_items" onclick="updateListPrice()">';
					                                }else if($_REQUEST['module'] == "AOS_Quotes"){
					                                    $html .= '<input class="button" type="button" name="button" value="Add To Quotes" id="add_line_items" onclick="updateListPrice()">';
					                                }  
													$html .= '<input class="button" type="button" name="button" value="CANCEL" id="btn_cancel" onclick="closePriceBooksPopup()">
						           				</div>
						           			</div>
						           		</form>
				           			</div>
				           		</div>';
				        echo $html;
					}
				}
			}
		}
	}
}