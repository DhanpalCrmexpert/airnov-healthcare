<?php
/*********************************************************************************
 * This file is part of package Price Books.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Price Books is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
require_once('custom/VIPriceBooks/VIPriceBooksFunction.php');
class Viewvi_pricebooksconfig extends SugarView {
    //Display Method 
    public function display(){
        global $mod_strings,$theme;
    
        $smarty = new Sugar_Smarty();

        $listViewLink = "?module=VI_Price_Books&action=ListView";
    
        $tablename = "vi_enable_pricebooks";
        $fieldNames = array('enable');
        $where = array();

        $selData = getPriceBooksRecord($tablename,$fieldNames,$where);
        $resultData = $GLOBALS['db']->fetchOne($selData);
     
        if(!empty($resultData)) {
           $enable = $resultData['enable'];
        }else{
            $enable = 0;
        }

        $url = "https://suitehelp.varianceinfotech.com";
        $helpBoxContent = getPriceBooksHelpBoxHtml($url);

        $smarty->assign('MOD',$mod_strings);
        $smarty->assign('THEME',$theme);
        $smarty->assign('LISTVIEWLINK',$listViewLink);
        $smarty->assign("ENABLE",$enable);
        $smarty->assign('HELPBOXCONTENT',$helpBoxContent);

        parent::display();
        $smarty->display('custom/modules/Administration/tpl/vi_pricebooksconfig.tpl');
    }//end of function
}//end of class
?>

