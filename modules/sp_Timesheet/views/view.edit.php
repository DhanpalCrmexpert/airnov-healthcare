<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

class sp_TimesheetViewEdit extends ViewEdit
{
	public function __construct() {
		parent::ViewEdit();
	}
	
    function display()
    {
		global $current_user, $sugar_config, $app_list_strings;
		
		$current_user_id = $current_user->id;
		$user_id = $current_user->id;
		$user_name = $current_user->name;
		
		$total_hours = 0;
		$week_start_date = date("Y-m-d");
		$result = $this->getDates($week_start_date);
		$dates = $this->getWeekDates($result['start'], $result['end']);
		//////////////////
		
		$siteUrl = $sugar_config['site_url'];
		require_once('modules/TimesheetNinja/license/OutfittersLicense.php');
		$validate_license = OutfittersLicense::isValid('TimesheetNinja');
		if($validate_license !== true) {
		if(is_admin($current_user)) {
			SugarApplication::appendErrorMessage('Timesheet is no longer active due to the following reason: '.$validate_license.' Users will have limited to no access until the issue has been addressed.');
		}
		echo '<p class="error">Timesheet is no longer active Please renew your subscription or check your license configuration.Will be redirect in seconds.....</p>';
		header("refresh:2;url=$siteUrl");
		//functionality may be altered here in response to the key failing to validate
		}
		echo "</br>";
		
		
		//////////////////
		if(!empty($this->bean->id)){
			$timeSheetDates = $this->getTimesheetDates($this->bean->id);
			$week_start_date = $timeSheetDates['track_date_from'];
			$result['start'] = date("Y-m-d",strtotime($timeSheetDates['track_date_from']));
			$result['end'] = date("Y-m-d",strtotime($timeSheetDates['track_date_to']));
			$dates = $this->getWeekDates($result['start'], $result['end']);
		}
		
		if(!empty($_REQUEST['week_start_date'])){
			$week_start_date = $_REQUEST['week_start_date'];
			$result = $this->getDates($week_start_date);
			$dates = $this->getWeekDates($result['start'], $result['end']);
		}
		
		$next_week_start_date = $this->getNextPreviousDate($week_start_date, "+7 day");
		
		$previous_week_start_date = $this->getNextPreviousDate($week_start_date, "-7 day");
		
		$json_dates = json_encode($dates);
		
		if(!empty($this->bean->id)){
			$selected_module = $this->bean->module;
			$related_module = $this->bean->related_module;
			$user_id = $this->bean->user_id_c;
			$current_user_id = $this->bean->user_id_c;
		}
		else{
			$selected_module = $sugar_config['timesheet_module_'.$current_user_id];
			$related_module = $sugar_config['related_module_'.$current_user_id];
		}
		
		$selected_module_lable = $app_list_strings['moduleList'][$selected_module];
		$related_module_lable = $app_list_strings['moduleList'][$related_module];

		$projects_list = $this->getProjectsList($result['start'], $result['end'], $user_id, $selected_module, $related_module);
		$projects = $projects_list['project_list'];
		$custom_projects = $projects_list['custom_array'];
		$temp_array = array();
		foreach ($custom_projects as $key => $value) {
			$key_parts = explode('_', $key);
			if(isset($temp_array[$key_parts['0']])){
				$temp_array[$key_parts['0']]['track_date'][$value['track_date']] = array('hours' => $value['hours']);
			}else{
				$temp_array[$key_parts['0']] = array(
					'related_module' => $value['related_module'],
            		'related_module_record' => $value['related_module_record'],
		            'tracker_id' => $value['tracker_id'],
		            'description' => $value['description'],
		            'auto_tracker_id' => $value['auto_tracker_id'],
		            'track_date' => array($value['track_date'] => array('hours' => $value['hours'])),
				);  
			}
		}
		
		if(empty($projects) && empty($custom_projects)){
			echo "Records are not assigned to you. Please assign some records to your self.";
			die();
		}
		
		echo "<input type='hidden' id='json_dates' name='json_dates' value='".$json_dates."' />";
		
		$title_header = '<strong style="color:red;">'.$user_name.': '.date('d F',strtotime($result['start'])).' -- '.date('d F',strtotime($result['end'])).'</strong>';
		
		$html = '
		<script src="modules/sp_Timesheet/js/timesheet.js"></script>
		
		<form action="index.php" method="POST" name="EditView" id="EditView">
		<table id="topLevelButtonsRow" width="100%" cellpadding="0" cellspacing="0" border="0" class="dcQuickEdit" >
		<tbody><tr>
		<td class="buttons">
		<input type="hidden" id="week_start" name="week_start" value="'.$result['start'].'" />
		<input type="hidden" id="week_end" name="week_end" value="'.$result['end'].'" />
		<input type="hidden" id="current_user_id" name="current_user_id" value="'.$current_user_id.'" />
		<input type="hidden" id="next_week_date" name="next_week_date" value="'.$next_week_start_date.'" />
		<input type="hidden" id="previous_week_date" name="previous_week_date" value="'.$previous_week_start_date.'" />
		<input type="hidden" name="module" value="sp_Timesheet">
		<input type="hidden" name="record" value="'.$this->bean->id.'">
		<input type="hidden" name="selected_module" value="'.$this->bean->module.'">
		<input type="hidden" name="related_module" value="'.$this->bean->related_module.'">
		<input type="hidden" name="related_module_relationship" value="'.$this->bean->related_module_relationship.'">
		<input type="hidden" name="isDuplicate" value="false">
		<input type="hidden" name="action">
		<input type="hidden" name="return_module" value="">
		<input type="hidden" name="return_action" value="">
		<input type="hidden" name="return_id" value="">
		<input type="hidden" name="module_tab"> 
		<input type="hidden" name="contact_role">
		<input type="hidden" name="offset" value="1">
		<!-- to be used for id for buttons with custom code in def files-->
		
		<div class="action_buttons">
		<input title="Save" accesskey="a" class="button primary" onclick="var _form = document.getElementById(\'EditView\'); _form.action.value=\'Save\'; if(check_form(\'EditView\'))SUGAR.ajaxUI.submitForm(_form);return false;" type="submit" name="button" value="Save" id="SAVE_HEADER">  
		
		<input title="Cancel [Alt+l]" accesskey="l" class="button" onclick="SUGAR.ajaxUI.loadContent(\'index.php?action=index&amp;module=sp_Timesheet\'); return false;" type="button" name="button" value="Cancel" id="CANCEL_HEADER">  
		
		<input title="Last Week" accesskey="a" class="button" type="button" name="previous_week" value="<< last week" id="previous_week"/>
		
		'.$title_header.'
		
		<input title="Next Week" accesskey="a" class="button" type="button" name="next_button" value="next week >>" id="next_week"/>
		
		<input disabled type="text" name="date_field" id="date_field" value="" />

		<img border="0" src="custom/themes/default/images/jscalendar.gif" alt="Enter Date" id="date_triggere" align="absmiddle" />
		
		<input title="Search" accesskey="a" class="button" type="button" name="custom_week" value="Jump" id="custom_week"/>
		
		<div class="clear"></div>
		<div id="selected_date"></div>
		</div>
		</td>
		
		
		<td align="right">
		</td>
		</tr>
		</tbody></table>
		<script language="javascript">
			var _form_id = "EditView";
			SUGAR.util.doWhen(function(){
				_form_id = (_form_id == "") ? "EditView" : _form_id;
				return document.getElementById(_form_id) != null;
			}, SUGAR.themes.actionMenu);
		</script>';

		$weekHtml = $html;
		$weekHtml .= "<table><tr><th></th>";
		
		$html .= '<table id="myTableDates" border=1 style="margin-left: 125px;">';
		$html .= '<tr>';
		$html .= '<td style="width: 30px;"></td>';
		for($k=0; $k<7; $k++){
			$quoted_date = $dates[$k];
			$quoted_date = "'$quoted_date'";
			// $html .= '<td style="width: 100px;">'.date("D m/d",strtotime($dates[$k])).'</td>';
			$html .= '<td style="width: 15%;"><a id="cstm_date_'.$k.'"  href="#" onclick="showHideTable('.$quoted_date.', '.$k.');">'.date("D m/d",strtotime($dates[$k])).'</a></td>';
			// $html .= '<td style="width: 30px;"></td>';
			$weekHtml .= '<th style="width: 75px;text-align: center;">'.date("D m/d",strtotime($dates[$k])).'</th>';
		}
		$weekHtml .= "<th style='width: 50px;text-align: center;'>Total</th><th  style='width: 50px;text-align: center;'>Project Hours</th></tr>";
		$html .= "</tr></table>";

		for($kk=0; $kk<7; $kk++){
			$html .= '<table id="mytable_'.$dates[$kk].'" style="border-collapse: separate;  border-spacing: 0 1em;">';
			
			$html .= '<tr>
			<td style="width: 275px;"></td>
			<td><b>Hours</b></td><td style="width: 30px;"></td>
			<td><b>Description</b></td><td style="width: 30px;"></td>
			</tr>
			';
			
			$found = true;
			$per_project_sum = 0;
			$total_items = 0;
			for($i=0; $i<count($projects); $i++){
				$total_items++;
				$project_name = $projects[$i]['project_name'];
				$project_id = $projects[$i]['project_id'];
				$quoted_project_id = "'$project_id'";
				$row_project_sum = 0;
				$field_name = 'project_'.$project_id.'_'.$dates[$kk];
				
				$kt_count = count($projects[$i]['project_time_sheet_tracker'][$field_name]);
				if((empty($this->bean->id) && $kt_count == 0) || empty($projects[$i]['project_time_sheet_tracker'][$field_name])){
					$kt_count = 1;
				}
	
				for($kt=0; $kt<$kt_count; $kt++){	
					$html .= '<tr id="saqib_'.$project_id.'">';
					$html .= '<td style="width: 275px; display: inline-block;"><b>'.$selected_module_lable.': </b> <a href="index.php?module='.$selected_module.'&action=DetailView&record='.$project_id.'" target="_blank">'.$project_name.'</a></td>';
					
					$rand_index = '';
					
					$quoted_field_name = $field_name;
					$quoted_field_name = "'$quoted_field_name'";
					
					$class_name = 'class_'.$project_id.'_'.$dates[$kk];
					$quoted_date = $dates[$kk];
					$quoted_date = "'$quoted_date'";
					
					$current_user_name = $current_user->name;
					$quoted_current_user = "'$current_user_name'";
					$quoted_project_name = $this->clean($project_name);
					$quoted_project_name = "'$quoted_project_name'";
					
					$html .= '<td><input class="'.$class_name.' hours_field" type="text" name="'.$field_name.'[]" id="'.$field_name.'" value="'.$projects[$i]['project_time_sheet_tracker'][$field_name][$kt]["hours"].'" style="width: 50px; height: 30px" /> </td>';
					$html .= '<td style="width: 30px;"></td>';
					
					$html .= '<td><input type="text" name="'.$field_name.'[]" id="description_'.$field_name.'" style="width: 250px; height: 30px" value="'.$projects[$i]['project_time_sheet_tracker'][$field_name][$kt]["description"].'" /> </td>';
					$html .= '<td style="width: 30px;"></td>';
					
					$html .= '<input type="hidden" name="'.$field_name.'[]" id="date_'.$field_name.'" value="'.$dates[$kk].'" />';
					
				}
				
				// Process Project Tasks
				for($pt=0; $pt<count($projects[$i]['project_tasks_list']); $pt++){
					$total_items++;
					$project_task_id = $projects[$i]['project_tasks_list'][$pt]['project_task_id'];
					$quoted_project_task_id = "'$project_task_id'";
					
					$field_name = 'projecttask_'.$project_id.'_'.$project_task_id.'_'.$dates[$kk];
						
					$kc_count = count($projects[$i]['project_time_sheet_tracker'][$field_name]);
					if((empty($this->bean->id) && $kc_count == 0 )|| empty($projects[$i]['project_time_sheet_tracker'][$field_name])){
						$kc_count = 1;
					}
					
					for($kc=0; $kc<$kc_count; $kc++){
						
						$quoted_field_name = $field_name;
						$quoted_field_name = "'$quoted_field_name'";
					
						$html .= '<tr>';
						$project_task_name = $projects[$i]['project_tasks_list'][$pt]['project_task_name'];
						$quoted_project_task_name = $this->clean($project_task_name);
						$quoted_project_task_name = "'$quoted_project_task_name'";
						
						$html .= '<td style="width: 275px;"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$related_module_lable.':</b> <a href="index.php?module='.$related_module.'&action=DetailView&record='.$project_task_id.'" target="_blank">'.$project_task_name.'</a></td>';
						$row_project_task_sum = 0;
					
						$readonly_mode = '';
						$readonly_post = '';
						
						if($this->checkIfTimeAutoTracked($this->bean->id, $project_id, $project_task_id, $dates[$kk], $current_user_id)){
							$readonly_mode = 'readonly="readonly"';
							$readonly_post = 'readonly_';
						}
						
						$quoted_date = $dates[$kk];
						$quoted_date = "'$quoted_date'";
						$class_name = 'pt_class_'.$dates[$kk].'_'.$project_id;
					
						$html .= '<td><input '.$readonly_mode.' class="'.$class_name.'" type="text" name="'.$readonly_post.$field_name.'[]" id="'.$field_name.'" value="'.$projects[$i]['project_time_sheet_tracker'][$field_name][$kc]["hours"].'" style="width: 50px; height: 30px" /> </td>';
						$html .= '<td style="width: 30px;"></td>';

						$html .= '<td><input type="text" name="'.$readonly_post.$field_name.'[]" id="description_'.$field_name.'" style="width: 250px; height: 30px" value="'.$projects[$i]['project_time_sheet_tracker'][$field_name][$kc]["description"].'" /> </td>';
						$html .= '<td style="width: 30px;"></td>';
	
						$html .= '<input type="hidden" name="'.$readonly_post.$field_name.'[]" id="date_'.$field_name.'" value="'.$dates[$kk].'" />';	
					}
				}

				$html .= '<tr ><td colspan=18><hr><td></tr>';
			}
			foreach ($temp_array as $key => $project) {
				$key_parts = explode('_', $key);
				
				$class_name = 'class_'.$key;
				$field_name = 'customproject_'.$project['tracker_id'].'_'.$project['auto_tracker_id'].'_'.$dates[$kk];
				$html .= "<tr>";
				$project_bean = BeanFactory::getBean($project['related_module'], $project['related_module_record']);
				if(isset($project_bean->id)){
					$html .= "<td><b>".$project['related_module'].": </b><a href='index.php?module=".$project['related_module']."&action=DetailView&record=".$project['related_module_record']."' target='_blank'>{$project_bean->name}</a></td>";
					$html .= '<td><input class="'.$class_name.'" type="text" name="'.$field_name.'[]" id="'.$field_name.'" value="'.$project['track_date'][$dates[$kk]]["hours"].'" style="width: 50px; height: 30px" /> </td>';
					$html .= '<td style="width: 30px;"></td>';

					$html .= '<td><input type="text" name="'.$field_name.'[]" id="description_'.$field_name.'" style="width: 250px; height: 30px" value="'.$project["description"].'" /> </td>';
					$html .= '<td style="width: 30px;"></td>';

					$html .= '<input type="hidden" name="'.$field_name.'[]" id="date_'.$field_name.'" value="'.$dates[$kk].'" />';
				}
				$html .= "<tr>";
			}
			$html .= "</table>";
	    }
		$html .= "</form>";

    

		for($i=0; $i<count($projects); $i++){
			$project_name = $projects[$i]['project_name'];
			$project_id = $projects[$i]['project_id'];

			$weekHtml .='<tr id="saqib_'.$project_id.'"  style="height: 60px" >';
			$weekHtml .= '<td style="width: 275px;padding-bottom: 2em;"><b>'.$selected_module_lable.': </b> <a href="index.php?module='.$selected_module.'&action=DetailView&record='.$project_id.'" target="_blank">'.$project_name.'</a></td>';
			$project_total = 0.0;
			for($kk=0; $kk<7; $kk++){
				$field_name = 'project_'.$project_id.'_'.$dates[$kk];
				$kt_count = count($projects[$i]['project_time_sheet_tracker'][$field_name]);
				if((empty($this->bean->id) && $kt_count == 0) || empty($projects[$i]['project_time_sheet_tracker'][$field_name])){
					$kt_count = 1;
				}
				// Showing weekly input fields for projects
				for($kt=0; $kt<$kt_count; $kt++){
					$class_name = 'class_'.$project_id;
					$weekHtml .= '<td style="vertical-align: top;text-align: center;"><input class="'.$class_name.' projects_hours" type="text" name="'.$field_name.'[]" id="'.$field_name.'" value="'.$projects[$i]['project_time_sheet_tracker'][$field_name][$kt]["hours"].'" style="width: 50px; height: 30px" /> </td>';
					$weekHtml .= '<input type="hidden" name="'.$field_name.'[]" id="date_'.$field_name.'" value="'.$dates[$kk].'" />';
					$weekHtml .= '<input type="hidden" name="'.$field_name.'[]" id="date_'.$field_name.'" value="'.$dates[$kk].'" />';
					$project_total += $projects[$i]['project_time_sheet_tracker'][$field_name][$kt]["hours"];
				}
			}
			$weekHtml .= '<td style="width: 70px; text-align:center;"><span class="'.$class_name.'_total_hours total_hours project_id_'.$project_id.'" style="width: 50px; height: 30px" id="'.$field_name.'_total_hours" >'.$project_total.'</span> </td>';

			$weekHtml .= '<td rowspan="1"><span class="'.$field_name.'_project_hours" style="width: 50px; height: 30px" id="project_'.$project_id.'_project_hours">'.$project_total.'</span> </td>';


			$weekHtml .='</tr>';
			for($pt=0; $pt<count($projects[$i]['project_tasks_list']); $pt++){
				$project_total = 0.0;
				$weekHtml .='<tr id="saqib_'.$project_id.'"  style="height: 60px">';
				$project_task_id = $projects[$i]['project_tasks_list'][$pt]['project_task_id'];
				$project_task_name = $projects[$i]['project_tasks_list'][$pt]['project_task_name'];
				$weekHtml .= '<td style="width: 275px;padding-bottom: 2em;"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$related_module_lable.':</b> <a href="index.php?module='.$related_module.'&action=DetailView&record='.$project_task_id.'" target="_blank">'.$project_task_name.'</a></td>';
				
				for($kk=0; $kk<7; $kk++){
					$field_name = 'projecttask_'.$project_id.'_'.$project_task_id.'_'.$dates[$kk];
					$kc_count = count($projects[$i]['project_time_sheet_tracker'][$field_name]);
					if((empty($this->bean->id) && $kc_count == 0 )|| empty($projects[$i]['project_time_sheet_tracker'][$field_name])){
						$kc_count = 1;
					}
					for($kc=0; $kc<$kc_count; $kc++){
						$class_name = 'pt_class_'.$project_task_id;
						$readonly_mode = '';
						$readonly_post = '';
						
						if($this->checkIfTimeAutoTracked($this->bean->id, $project_id, $project_task_id, $dates[$kk], $current_user_id)){
							$readonly_mode = 'readonly="readonly"';
							$readonly_post = 'readonly_';
						}
						//Showing project related tasks weekly time sheet fields
						$weekHtml .= '<td style="vertical-align: top;text-align: center;"><input '.$readonly_mode.' class="'.$class_name.' projects_hours" type="text" name="'.$readonly_post.$field_name.'[]" id="'.$field_name.'" value="'.$projects[$i]['project_time_sheet_tracker'][$field_name][$kc]["hours"].'" style="width: 50px; height:30px;" /> </td>';
						$weekHtml .= '<input type="hidden" name="'.$readonly_post.$field_name.'[]" id="date_'.$field_name.'" value="'.$dates[$kk].'" />';
						$weekHtml .= '<input type="hidden" name="'.$readonly_post.$field_name.'[]" id="date_'.$field_name.'" value="'.$dates[$kk].'" />';
						$project_total += $projects[$i]['project_time_sheet_tracker'][$field_name][$kc]["hours"];

					}

				}
				$weekHtml .= '<td style="width: 50px; height: 30px; text-align:center;"><span class="'.$class_name.'_total_hours total_hours project_id_'.$project_id.'"  id="'.$field_name.'_total_hours" >'.$project_total.'</span> </td>';
				$weekHtml .='</tr>';
			}
				$weekHtml .= '<tr ><td colspan=18><hr><td></tr>';
		}
		foreach ($temp_array as $project) {
			$total_items++;
			$weekHtml .= "<tr>";
			$project_bean = BeanFactory::getBean($project['related_module'], $project['related_module_record']);
			if(isset($project_bean->id)){
				$weekHtml .= "<td><b>".$project['related_module'].": </b><a href='index.php?module=".$project['related_module']."&action=DetailView&record=".$project['related_module_record']."' target='_blank'>{$project_bean->name}</a></td>";
			}
			$project_total = 0.0;
			$class_name = 'pt_class_'.$project['related_module_record'];
			foreach ($dates as $date) {
				$field_name = 'customproject_'.$project['tracker_id'].'_'.$project['auto_tracker_id'].'_'.$date;
				if(isset($project['track_date'][$date])){
					$weekHtml .= '<td><input class="'.$class_name.' projects_hours" type="text" name="'.$field_name.'[]" id="'.$field_name.'" value="'.$project['track_date'][$date]['hours'].'" style="width: 50px; height: 30px" /> </td>';
					$project_total += (float)$project['track_date'][$date]['hours'];
				}else{
					$weekHtml .= '<td><input class="'.$class_name.' projects_hours" type="text" name="'.$field_name.'[]" id="'.$field_name.'" value="" style="width: 50px; height: 30px" /> </td>';
				}
				$weekHtml.='<input type="hidden" name="'.$field_name.'[]" id="date_'.$field_name.'" value="'.$date.'"/>';
				$weekHtml.='<input type="hidden" name="'.$field_name.'[]" id="date_'.$field_name.'" value="'.$date.'"/>';
			}
			$weekHtml .= '<td><span class="'.$class_name.'_total_hours total_hours" style="width: 50px; height: 30px" id="'.$field_name.'_total_hours" >'.$project_total.'</span> </td>';
			$weekHtml .= '<td rowspan="'.$total_items.'"><span class="project_hours" style="width: 50px; height: 30px" id="project_hours">'.$this->bean->hours.'</span> </td>';
			$weekHtml .= "</tr>";
		}
		$weekHtml .= '</table></form>';

		if(isset($_REQUEST['week_view']) && $_REQUEST['week_view'] == 'true'){
			echo $weekHtml;
		}else{
			echo $html;
		}
		
	}
	
	function getProjectsList($start_date, $end_date, $user_id, $selected_module, $related_module){
		global $db, $current_user;
		
		$current_user_id = $user_id;
		$selected_module_bean = BeanFactory::getBean($selected_module);
		$module_table_name = $selected_module_bean->table_name;
		
		$status_where = "";
		if($selected_module == "Project" || $selected_module == "ProjectTask"){
			$status_where = "and status not in('Completed', 'Closed', 'On_Hold')";
		}
		
		$i = 0;
		$project_list = array();
		
		if($selected_module == "Project"){
			$getProjectQry = "Select DISTINCT p.id, p.name, p.date_entered from project p INNER JOIN project_task pt on pt.project_id = p.id where p.deleted = 0 and pt.deleted = 0 and p.status not in('Completed', 'Closed', 'On_Hold') and pt.assigned_user_id = '$current_user_id' ORDER BY p.date_entered DESC";
		}else if($selected_module == "Accounts"){
			$getProjectQry = "Select DISTINCT a.id, a.name from accounts a INNER JOIN cases c ON a.id = c.account_id where a.deleted = 0 and c.deleted = 0 and c.assigned_user_id = '$current_user_id' $status_where ";
		}else{
			$getProjectQry = "Select DISTINCT p.id, p.name from $module_table_name p where p.deleted = 0 and assigned_user_id = '$current_user_id' $status_where ";
		}
		$resProject= $db->query($getProjectQry);
		while($rowProjectId = $db->fetchByAssoc($resProject))
		{
			$project_list[$i]['project_id'] = $rowProjectId['id'];
			if(isset($rowProjectId['name'])){
				$project_list[$i]['project_name'] = $rowProjectId['name'];
			}
			else{
				$project_list[$i]['project_name'] = $rowProjectId['first_name']." ".$rowProjectId['last_name'];
			}
			$project_list[$i]['project_tasks_list'] = $this->getProjectTasksList($rowProjectId['id'], $selected_module, $related_module, $current_user_id);
			$related_tasks = $project_list[$i]['project_tasks_list'];
			$timeSheetResult = $this->getProjectTimeTracked($rowProjectId['id'], $start_date, $end_date, $user_id, $related_tasks);
			$project_list[$i]['project_time_sheet_tracker'] = $timeSheetResult['project_time_sheet_list'];
			$i++;
		}
		$custom_array = array();
		$timesheet_id_edit_qry = '';
		if(!empty($this->bean->id)){
			$timesheet_id_edit = $this->bean->id;
			$timesheet_id_edit_qry = " AND timesheet_id='$timesheet_id_edit' ";
		}
		$getCustomProjectTimeSheetQry = "Select * from sp_timesheet_tracker where deleted = 0 and track_date >= '$start_date' and track_date <= '$end_date' and user_id_c = '$user_id' {$timesheet_id_edit_qry} ORDER BY track_date ASC";
		$resCustomProjectTimeSheet = $db->query($getCustomProjectTimeSheetQry);
		while($rowCustomProjectTimeSheetId = $db->fetchByAssoc($resCustomProjectTimeSheet))
		{
			$temp_array = array();
			$project_task_id = $rowCustomProjectTimeSheetId['projecttask_id_c'];
			if(!empty($rowCustomProjectTimeSheetId['related_module'])){
				$temp_array = array(
					'description' => $rowCustomProjectTimeSheetId['description'],
					'hours' => $rowCustomProjectTimeSheetId['hours'],
					'track_date' => $rowCustomProjectTimeSheetId['track_date'],
					'related_module' => $rowCustomProjectTimeSheetId['related_module'],
					'related_module_record' => $project_task_id,
					'tracker_id' => $rowCustomProjectTimeSheetId['id'],
					'auto_tracker_id' => $rowCustomProjectTimeSheetId['sp_auto_timesheet_tracker_id'],
				);
				$custom_array[$project_task_id."_".$rowCustomProjectTimeSheetId['track_date']] = $temp_array;
			}

		}		


		return array('project_list' => $project_list, 'custom_array' => $custom_array);
	}
	
	function getProjectTimeTracked($project_id, $start_date, $end_date, $user_id, $related_tasks){
		global $db, $current_user, $sugar_config;
		$current_user_id = $current_user->id;
		if(!empty($this->bean->id)){
			$selected_module = $this->bean->module;
			$related_module = $this->bean->related_module;
			$user_id = $this->bean->user_id_c;
			$current_user_id = $this->bean->user_id_c;
		}
		else{
			$selected_module = $sugar_config['timesheet_module_'.$current_user_id];
			$related_module = $sugar_config['related_module_'.$current_user_id];
		}
		
		$related_task_ids = array();
		foreach($related_tasks as $key=>$related_tasks_sub_arr){
			$related_task_ids[] = $related_tasks_sub_arr['project_task_id'];
		}
		
		$related_task_ids_str = join("', '", $related_task_ids);
		
		$i = 0;
		$pre_index = '';
		$project_time_sheet_list = array();
		
		$timesheet_id_edit_qry = '';
		if(!empty($this->bean->id)){
			$timesheet_id_edit = $this->bean->id;
			$timesheet_id_edit_qry = " AND timesheet_id='$timesheet_id_edit' ";
		}
		
		$in_qry = "";
		if(!empty($related_task_ids_str)){
			$in_qry = " and (project_id_c in ('$related_task_ids_str') OR projecttask_id_c in ('$related_task_ids_str'))";
		}
		
		$getProjectTimeSheetQry = "Select * from sp_timesheet_tracker where deleted = 0 and (project_id_c='$project_id' OR projecttask_id_c='$project_id') and track_date >= '$start_date' and track_date <= '$end_date' and user_id_c = '$user_id' $timesheet_id_edit_qry ORDER BY track_date, projecttask_id_c ASC";
		$resProjectTimeSheet = $db->query($getProjectTimeSheetQry);
		while($rowProjectTimeSheetId = $db->fetchByAssoc($resProjectTimeSheet))
		{
			$project_id_c = $rowProjectTimeSheetId['project_id_c'];
			$project_task_id = $rowProjectTimeSheetId['projecttask_id_c'];
			$track_date = $rowProjectTimeSheetId['track_date'];
			$hours = $rowProjectTimeSheetId['hours'];
			$description = $rowProjectTimeSheetId['description'];
			$record_timesheet_id = $rowProjectTimeSheetId['timesheet_id'];
			
			if($record_timesheet_id == $timesheet_id_edit){
				if(empty($project_task_id)){
					$index = "project_". $project_id ."_". $track_date;
					if(!isset($project_time_sheet_list[$index])){
						$project_time_sheet_list[$index][0]["hours"] = $hours;
						$project_time_sheet_list[$index][0]["description"] = $description;
					}
				}
			}
			else{
				if(!empty($project_task_id)){
					$related_record_module = $this->getReleatedModule($rowProjectTimeSheetId);
					if($selected_module != $related_record_module){
						continue;
					}
				}
				$index = "project_". $project_id ."_". $track_date;
				if(!isset($project_time_sheet_list[$index])){
					$project_time_sheet_list[$index][0]["hours"] = $hours;
					$project_time_sheet_list[$index][0]["description"] = $description;
				}
			}
		}	
		
		$getProjectTimeSheetQry = "Select * from sp_timesheet_tracker where deleted = 0 $in_qry and track_date >= '$start_date' and track_date <= '$end_date' and user_id_c = '$user_id' $timesheet_id_edit_qry ORDER BY track_date, projecttask_id_c ASC";
		// exit;
		$resProjectTimeSheet = $db->query($getProjectTimeSheetQry);
		while($rowProjectTimeSheetId = $db->fetchByAssoc($resProjectTimeSheet))
		{
			$project_id_c = $rowProjectTimeSheetId['project_id_c'];
			$project_task_id = $rowProjectTimeSheetId['projecttask_id_c'];
			$track_date = $rowProjectTimeSheetId['track_date'];
			$hours = $rowProjectTimeSheetId['hours'];
			$description = $rowProjectTimeSheetId['description'];
			$record_timesheet_id = $rowProjectTimeSheetId['timesheet_id'];
			$parent_type = $rowProjectTimeSheetId['parent_type'];
			
			if(empty($this->bean->id)){
				if(empty($project_task_id)){
					$project_task_id = $project_id_c;
				}
				else{
					$related_record_module = $this->getReleatedModule($rowProjectTimeSheetId);
					if($related_module != $related_record_module){
						continue;
					}
				}
			}
			
			$index = "projecttask_". $project_id. "_". $project_task_id ."_". $track_date;
			if(!isset($project_time_sheet_list[$index])){
				$index1 = "projecttask_". $project_task_id. "_". $project_id_c ."_". $track_date;
				if(isset($project_time_sheet_list[$index1])){
					$index = $index1;
				}
				
				$project_time_sheet_list[$index][0]["hours"] = $hours;
				$project_time_sheet_list[$index][0]["description"] = $description;
			}
			
		}
		return array('project_time_sheet_list' => $project_time_sheet_list);
	}
	
	function getProjectTasksList($selected_module_id, $selected_module, $related_module, $current_user_id){
		global $db, $sugar_config, $current_user;
		
		$i = 0;
		$project_task_list = array();
		
		if(!empty($this->bean->id)){
			$relationship_name = $this->bean->related_module_relationship;
		}
		else{
			$relationship_name = $sugar_config['related_module_relationship_'.$current_user_id];
		}
		if(empty($relationship_name)){
			$relationship_name = 'cases';
		}
		$selected_module_bean = BeanFactory::getBean($selected_module, $selected_module_id);
		
		$selected_module_bean->load_relationship($relationship_name);
		$related_module_records = $selected_module_bean->$relationship_name->getBeans();
		foreach($related_module_records as $related_module_record){
			if($this->validateStatus($related_module_record, $related_module) && $related_module_record->assigned_user_id == $current_user_id){
				$project_task_list[$i]['project_task_id'] = $related_module_record->id;
				$project_task_list[$i]['project_task_name'] = $related_module_record->name;
				$i++;
			}
		}
		return $project_task_list;
	}
	
	public function validateStatus($related_module_record, $related_module){
		if($related_module == "Project" || $related_module == "ProjectTask")
		{
			if($related_module_record->status == "Completed" || $related_module_record->status == "Closed" || $related_module_record->status == "On_Hold")
			{
				return false;
			}
		}
		return true;
	}
	
	function getDates($date){
	  $week = date("W",strtotime($date));
	  $year = date("Y",strtotime($date));
	  $dto = new DateTime();
	  $result['start'] = $dto->setISODate($year, $week, 1)->format('Y-m-d');
	  $result['end'] = $dto->setISODate($year, $week, 7)->format('Y-m-d');
	  return $result;
	}
	
	public function getWeekDates($start_date, $end_date){
		$start_timestamp = strtotime($start_date);
		$end_timestamp   = strtotime($end_date);
		$dates = array();
		while ($start_timestamp <= $end_timestamp) {
			$dates[] = date('Y-m-d', $start_timestamp);
			$start_timestamp = strtotime('+1 day', $start_timestamp);
		}
		return $dates;
	}
	
	public function getNextPreviousDate($date, $how_much_days){
		$week_start_date = strtotime($date);
		$week_start_date = strtotime($how_much_days, $week_start_date);
		return $week_start_date = date('Y-m-d', $week_start_date);
	}
	
	public function checkIfTimeAutoTracked($timesheet_id, $project_id, $project_task_id, $date, $current_user_id){
		global $db, $timedate;
		$getAutoTimeSheetQry = "Select * from sp_auto_timesheet_time_tracker where deleted = 0 and processed = 0 and sp_timesheet_id_c = '$timesheet_id' and selected_module_id = '$project_id' and parent_id = '$project_task_id' and user_id_c = '$current_user_id'";
		$resAutoTimeSheet = $db->query($getAutoTimeSheetQry);
		while($rowAutoTimeSheetId = $db->fetchByAssoc($resAutoTimeSheet))
		{
			$start_date = $timedate->to_display_date($rowAutoTimeSheetId['start_date']);
			$start_date = date('Y-m-d', strtotime($start_date));
			
			if($start_date == $date){
				return true;
			}
		}
		return false;
	}
	
	public function clean($string) {
		$string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

		return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
	}
	
	public function getTimesheetDates($timesheet_id){
		global $db;
		
		$resultTimesheet = array();
		$query = "Select * from sp_timesheet t where t.deleted = 0 and t.id = '$timesheet_id'";
		
		$result= $db->query($query);
		while($row = $db->fetchByAssoc($result))
		{
			$resultTimesheet["track_date_from"] = $row["track_date_from"];	
			$resultTimesheet["track_date_to"] = $row["track_date_to"];	
		}
		return $resultTimesheet;
	}
	
	public function getReleatedModule($row_pt){
		global $db;
		
		$related_record_url = $row_pt["related_record_url"];
		$projecttask_id_c = $row_pt["projecttask_id_c"];
		
		if($related_record_url == "No Related Record" || empty($projecttask_id_c)){
			return 'true';
		}
		
		$related_record_arr = explode("?", $related_record_url);
		$related_record_arr2 = explode("&", $related_record_arr[1]);
		$related_record_arr3 = explode("=", $related_record_arr2[0]);
		
		return $related_module = $related_record_arr3[1];
	}
}
